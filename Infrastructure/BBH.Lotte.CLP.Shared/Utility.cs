﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Shared
{
    public class Utility
    {
        public const string strFormmatDatetime = "MM/dd/yyyy HH:mm:ss";
        public const string strFormmatDate = "MM/dd/yyyy";

        public static void WriteLog(string path, string message)
        {

            if (!File.Exists(path))
            {
                using (StreamWriter w = File.CreateText(path))
                {
                    Log(message, w);
                }
            }
            else
            {
                using (StreamWriter w = File.AppendText(path))
                {
                    Log(message, w);
                }
            }
        }
        private static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\n" + DateTime.Now.ToString());
            w.WriteLine(" {0}", logMessage);
            w.WriteLine("-----------------------------------");
        }

        public static string EncodeString(string value)
        {
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(value);
            encodedBytes = md5.ComputeHash(originalBytes);
            return BitConverter.ToString(encodedBytes);
        }
        public static string MaHoaMD5(string pass)
        {
            try
            {

                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] data = Encoding.UTF8.GetBytes(pass);
                data = md5.ComputeHash(data);
                StringBuilder buider = new StringBuilder();
                foreach (byte b in data)
                {
                    buider.Append(b.ToString("x2"));
                }
                return buider.ToString();
            }
            catch
            {
                return pass;
            }
        }
        public static string GenCode()
        {
            Random objRandom = new Random();
            string combination = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder captchaCode = new StringBuilder();

            for (int i = 0; i < 8; i++)
            {
                captchaCode.Append(combination[objRandom.Next(combination.Length)]);
            }
            return captchaCode.ToString();
        }

        #region Method Get Time CountDown
        /// <summary>
        
        /// Created date: 05.01.2018
        /// Description: Get Info DateTime Count Down Count Down
        /// </summary>
        public static void GetTimeCountDown(int intTimeClosed, ref int intHours, ref int intMinutes, ref int intSeconds)
        {
            intHours = 23;
            intMinutes = 0;
            intSeconds = 0;
            if (DateTime.Now.Hour < intTimeClosed)
            {
                intHours = intTimeClosed - DateTime.Now.Hour - 1;
            }
            else if (DateTime.Now.Hour >= intTimeClosed)
            {
                intHours = intHours - (DateTime.Now.Hour - intTimeClosed);
            }

            intMinutes = 59 - DateTime.Now.Minute;
            intSeconds = 59 - DateTime.Now.Second;

            if (intSeconds <= 0)
            {
                intSeconds = 59;
                intMinutes--;
            }

            if (intMinutes <= 0)
            {
                intMinutes = 59;
                intHours--;
            }

            if (intHours <= 0)
            {
                intHours = 0;
            }
        }

        #endregion

        #region Get Exchange BTC API
        public static async Task<List<Exchange>> GetExchangeAsync(string path, string strPathLog)
        {
            List<Exchange> lst = new List<Exchange>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    using (var response = await client.GetAsync(path, HttpCompletionOption.ResponseContentRead))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string str = await response.Content.ReadAsAsync<string>();

                            lst = JsonConvert.DeserializeObject<List<Exchange>>(str);
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                WriteLog(strPathLog, objEx.Message);
            }

            return lst;
        }
        #endregion

        #region DateTimeProcessor
        public static DateTime FromDateTime(string sdate)
        {
            DateTime date = DateTime.ParseExact(sdate, strFormmatDatetime, null);
            if (date == new DateTime()) date = new DateTime(1750, 1, 1, 0, 0, 0, 0);
            else date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, 0);
            return date;
        }

        public static DateTime? DateTimeParseFormat(DateTime? dtmReturnDefaul)
        {
            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = "dd/MM/yyyy";
            if (dtmReturnDefaul.HasValue)
            {
                dtmReturnDefaul = Convert.ToDateTime(dtmReturnDefaul.Value.ToString("dd/MM/yyyy"), dateInfo);
            }
            return dtmReturnDefaul;
        }

        #endregion
        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }
        public static string EncryptText(string input, string password)
        {
            byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            string result = Convert.ToBase64String(bytesEncrypted);

            return result;
        }

        public static string DecryptText(string input, string password)
        {
            byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            string result = Encoding.UTF8.GetString(bytesDecrypted);

            return result;
        }
    }

    public class Exchange
    {
        public int TypeCoinID { get; set; }
        public string CoinName { get; set; }
        public double Values { get; set; }
        public DateTime ExchangeDate { get; set; }
        public string Unit { get; set; }
        public string TokenCode { get; set; }
    }
}
