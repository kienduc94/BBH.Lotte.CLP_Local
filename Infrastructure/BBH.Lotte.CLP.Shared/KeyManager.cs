﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Shared
{
    /// <summary>
    
    /// Created date: 05.01.2018
    /// Description: Manager Info Keys (ViewData, TempData, Cache...)
    /// </summary>
    public class KeyManager
    {
        #region Keys ViewData
        public const string _HOUR_COUNT_DOWN = "HOUR_COUNT_DOWN";
        public const string _MINUTE_COUNT_DOWN = "MINUTE_COUNT_DOWN";
        public const string _SECOND_COUNT_DOWN = "SECOND_COUNT_DOWN";
        public const string _DATEANDTIME = "DATEANDTIME";
        public const string _ENDDATE = "ENDDATE";

        public const string _SUM_POINTS_BTC = "SUM_POINTS_BTC";
        public const string _NUMBERWINNER = "NUMBERWINNER";
        public const string _NUMBERMEGABALLID = "NUMBERMEGABALLID";
        #endregion

        #region Keys ConfigurationManage.AppSetting
        public const string PATH_LOG = "PathLog";
        public const string _TIME_CLOSED_COUNT_DOWN = "TimeClosedCountDown";
        public const string _TIME_OPENED_COUNT_DOWN = "TimeOpenCountDown";
        public const string _CHECK_LOCK_BOOKING = "CheckLockBooking";
        public const string _SITE_KEY_CAPTCHA = "SiteKey";
        public const string _SECREC_KEY_CAPTCHA = "SecrecKey";
        public const string _RIPPLE_API = "RippleApi";

        public const string _KEYCODESHA = "sha:KeyCode";
        public const string _HOSTSITESHA = "sha:HostSite";
        public const string _WSSENTMAILSERVICES = "wcf:WSSentmailServices";
        #endregion
        public const string SessionUser = "SessionUser";
        public const string TokenSession = "Token_";
        public const string ClientID = "ClientID";
        public const string SystemID = "SystemID";
        public const string WalletID = "WalletID";
        public const string RequestBy = "RequestBy";
        public const string AdminID = "wcf:UserAdminId";

        public const string FeeBTC = "FeeBTC";
        public const string FeeCarCoin = "FeeCarCoin";
        public const string TimeDaysCookies = "TimeDaysCookies";
        public const string MaxCountSendMail = "MaxCountSendMail";
        public const string TimeExpired = "TimeExpired";
        public const string DayOpen = "DayOpen";
        public const string NumberTicketFree = "NumberTicketFree";
        public const string FreeWalletAddress = "FreeWalletAddress";
        //public const string JactPotDefault = "JactPotDefault";

        public const string FreeWalletID = "wcf:freewallet";

        public const string FeeWalletID = "wcf:sysFeeTxn";
        public const string FeeWalletAddress = "FeeWalletAddress";

        public const string AwardWalletID = "wcf:sysAdward";
        public const string AwardWalletAddress = "AwardWalletAddress";

        public const string ProgressivePrizeID = "wcf:sysProgressivePrize";
        public const string ProgressivePrizeWalletAddress = "ProgressivePrizeWalletAddress";

        public const string JackPotID = "wcf:sysJackPot";
        public const string JackPotWalletAddress = "JackPotWalletAddress";

        //wcf:PriorityJackpot
        public const string PriorityJackpot = "wcf:PriorityJackpot";

        public const string FeePrizeID = "wcf:sysFeePrize";
        public const string FeePrizeWalletAddress = "FeePrizeWalletAddress";

        public const string SysAdminID = "wcf:sysAdmin";
        public const string SysAdminWalletAddress = "AdminWalletAddress";

        public const string LoanWalletID = "wcf:sysLoan";
        public const string LoanWalletAddress = "LoanWalletAddress";

        public const string sysMgmtID = "wcf:sysMgmt";
        public const string SysMgmtWalletAddress = "SysMgmtWalletAddress";


        public const string SysFeeTxnID = "wcf:sysFeeTxn";
        public const string SysFeeTxnWalletAddress = "SysFeeTxnWalletAddress";

        public const string sysGuaranteeFund_UserID = "wcf:sysGuaranteeFund_UserID";
        public const string sysGuaranteeFund_Address = "wcf:sysGuaranteeFund_Address";

        public const string PercentRevenueForFreeTicket = "wcf:PercentRevenueForFreeTicket";
        public const string PercentRevenueForAward = "wcf:PercentRevenueForAward";
        public const string PercentRevenueForOper = "wcf:PercentRevenueForOper";
        public const string PercentRevenueForBusinessActivity = "wcf:PercentRevenueForBusinessActivity";
        public const string PercentRevenueForJacpot = "wcf:PercentRevenueForJacpot";

        public const string PercentRevenueForGuaranteeFund = "wcf:PercentRevenueForGuaranteeFund";
        public const string PercentRevenueForJacpot60 = "wcf:PercentRevenueForJacpot60";

        public const string HoureStart = "HoureStart";
        public const string MinuteStart = "MinuteStart";
        public const string HourEnd = "HourEnd";
        public const string MinuteEnd = "MinuteEnd";

        public const string ApiMyCarCoin = "api:mycarcoin";
        public const string HashToken = "api:hashtoken";
        //public const string HashSalt = "api:hashsalt"; 
        public const string EmailDefault = "api:emaildefault";

        //api:RepeatTimes
        public const string RepeatTimes = "api:RepeatTimes";

        public const string Mobiles = "sms:mobiles";
        public const string AccountSid = "sms:accountSid";
        public const string AuthToken = "sms:authToken";

        public const string JackPotDefault = "JackpotDefault";
    }
}
