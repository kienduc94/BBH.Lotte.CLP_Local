﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BBH.Lotte.CLP.Shared
{
    [DataContract]
    [Serializable]
    public class ModelRSA
    {
        [DataMember]
        public string ParamRSAFirst { get; set; }
        [DataMember]
        public string ParamRSASecond { get; set; }
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        /// <summary>
        /// Initial ModelRSA One Parameter
        /// </summary>
        /// <param name="_ParamRSAFirst"></param>
        public ModelRSA(string _ParamRSAFirst)
        {
            this.ParamRSAFirst = _ParamRSAFirst;

           
        }
    }
}
