﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Shared
{
    public class SessionKey
    {
        public const string PATH_LOG = "PathLog";
        public const string LOGS = "Logs";
        public const string CONNECTSTRING = "ConnectionString";
        public const string TIMECLOSEDCOUNTDOWN = "TimeClosedCountDown"; 
        public const string TIMEOPENCOUNTDOWN = "TimeOpenCountDown";
        public const string KEYWAN2LOT = "KeyWan2Lot";
        public const string TIMECOOKIES = "TimeCookies";
        public const string USERNAME = "username";
        public const string USERNAMEADMIN = "UserName";
        public const string MEMBERINFOMATION = "MemberInfomation";
        public const string CHECKLOCKBOOKING = "CheckLockBooking";
        public const string NUMBERRECORDPAGE = "NumberRecordPage";
        public const string TOTALRECORDLOTTERYRESULT = "TotalRecordLotteryResult";
        public const string MEMBERID = "memberid";
        public const string EWALLET = "ewallet";
        public const string LOGIN = "Login";
        public const string EMAIL = "Email"; 
        public const string TIMEEXPIRED = "TimeExpired";
        public const string EMAILID = "EmailID";
        public const string PASSWORD = "Password";
        public const string LinkReference = "LinkReference";
        public const string CLIENTID = "ClientID";
        public const string TOTALRECORD = "TotalRecord";
        public const string NUMBERTICKET = "NumberTicket";
        public const string TICKET = "Ticket";
        public const string NUMBER49 = "Number49";
        public const string NUMBER26 = "Number26";
        public const string BTCADDRESS = "BTCAddress";
        public const string CARCOINADDRESS = "CarcoinAddress";
        public const string BTCPOINT = "BTCPoint";
        public const string CARCOINPOINT = "CarcoinPoint";
        public const string ISPASSWORD = "IsPassword";
        public const string RawNumber = "RawNumber";
        public const string NUMBERTICKETFREE = "NUMBERTICKETFREE";
        #region Login Member
        public const string _POINTS = "POINTS";
        public const string _FULLNAME = "FULLNAME";
        #endregion

    }
}
