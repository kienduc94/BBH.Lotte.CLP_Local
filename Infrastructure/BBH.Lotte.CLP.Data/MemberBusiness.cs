﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using BBH.Lotte.CLP.Domain;
using BBC.Core.Database;
using System.IO;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;

namespace BBH.Lotte.CLP.Data
{
    public class MemberBusiness : IMemberServices
    {
        public string pathLog = ConfigurationManager.AppSettings["PathLog"];
        //public static int InsertMember(MemberBO member, ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        int memberID = 0;
        //        string sql = "insert into member(UserName,Password,IsActive,IsDelete,CreateDate) values(@username,@password,@isActive,@isDelete,@createDate); SELECT SCOPE_IDENTITY()";
        //        SqlParameter[] pa = new SqlParameter[5];
        //        pa[0] = new SqlParameter("@username", member.UserName);
        //        pa[1] = new SqlParameter("@isActive", member.IsActive);
        //        pa[2] = new SqlParameter("@isDelete", member.IsDelete);
        //        pa[3] = new SqlParameter("@password", member.Password);
        //        pa[4] = new SqlParameter("@createDate", member.CreateDate);
        //        SqlCommand command = helper.GetCommand(sql, pa, false);
        //        memberID = Convert.ToInt32(command.ExecuteScalar());
        //        return memberID;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return 0;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}

        public bool InsertMemberInfomation(MemberInfomationBO member)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "insert into memberinfomation(MemberID,FullName,Email,Mobile,BirthDay,Address,Gender,Points,Avatar) values(@memberID,@fullname,@email,@mobile,@birthday,@addressMember,@gender,@points,@avatar)";
                SqlParameter[] pa = new SqlParameter[9];
                pa[0] = new SqlParameter("@memberID", member.MemberID);
                pa[1] = new SqlParameter("@fullname", member.FullName);
                pa[2] = new SqlParameter("@email", member.Email);

                pa[3] = new SqlParameter("@mobile", member.Mobile);

                pa[4] = new SqlParameter("@gender", member.Gender);
                if (member.Birthday != null)
                {
                    pa[5] = new SqlParameter("@birthday", member.Birthday);
                }
                else
                {
                    pa[5] = new SqlParameter("@birthday", DBNull.Value);
                }

                pa[6] = new SqlParameter("@avatar", member.Avatar);
                pa[7] = new SqlParameter("@addressMember", member.Address);

                pa[8] = new SqlParameter("@points", member.Points);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateMemberInfomation(MemberInfomationBO member, int memberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "update memberinfomation set FullName=@fullName,Email=@email,Mobile=@mobile,Gender=@gender,BirthDay=@birthday,Avatar=@avatar,Address=@addressMember where MemberID=@memberID";
                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@fullname", member.FullName);
                pa[2] = new SqlParameter("@email", member.Email);

                pa[3] = new SqlParameter("@mobile", member.Mobile);

                pa[4] = new SqlParameter("@gender", member.Gender);
                pa[5] = new SqlParameter("@birthday", member.Birthday);

                pa[6] = new SqlParameter("@avatar", member.Avatar);
                pa[7] = new SqlParameter("@addressMember", member.Address);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {

                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdatePasswordMember(string fullname, string password)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdatePasswordMember";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@fullname", fullname);
                pa[1] = new SqlParameter("@password", password);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdatePointsMember(int memberID, int points)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdatePointsMember";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@points", points);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool LockAndUnlockMember(int memberID, int isActive)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_LockAndUnlockMember";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@isActive", isActive);
                pa[1] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        public MemberInfomationBO GetMemberDetailByID(int memberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                MemberInfomationBO member = null;
                string sql = "select * from memberinfomation where MemberID=@memberID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    member = new MemberInfomationBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.UserName = reader["UserName"].ToString();
                    member.FullName = reader["FullName"].ToString();
                    member.Email = reader["Email"].ToString();
                    member.Mobile = reader["Mobile"].ToString();
                    member.Gender = int.Parse(reader["Gender"].ToString());
                    member.Avatar = reader["Avatar"].ToString();
                    member.Address = reader["Address"].ToString();
                    member.Birthday = DateTime.Parse(reader["Birthday"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());

                }
                return member;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public MemberInfomationBO GetMemberDetail(string email)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                MemberInfomationBO member = null;
                string sql = "select p.*, m.UserName from memberinfomation p left join member m on m.MemberID=p.MemberID where Email=@email";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@email", email);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    member = new MemberInfomationBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.UserName = reader["UserName"].ToString();
                    member.FullName = reader["FullName"].ToString();

                    member.Email = reader["Email"].ToString();


                    member.Mobile = reader["Mobile"].ToString();

                    member.Gender = int.Parse(reader["Gender"].ToString());
                    member.Avatar = reader["Avatar"].ToString();
                    member.Address = reader["Address"].ToString();

                    member.Birthday = DateTime.Parse(reader["Birthday"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());

                }
                return member;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool CheckUserNameExists(string userName)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "select UserName from member where UserName=@userName";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@userName", userName);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool CheckEmailExists(string email)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "CheckEmailExists";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@email", email);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<MemberBO> GetListMember(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                //  System.IO.File.WriteAllLines(@"C:\inetpub\wwwroot\cmslottery\Logs/member8.txt", new string[2] { start.ToString(), end.ToString() });

                List<MemberBO> lstMember = new List<MemberBO>();
                string sql = "SP_GetListMember";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberBO member = new MemberBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.E_Wallet = reader["E_Wallet"].ToString();
                    member.Email = reader["Email"].ToString();
                    member.FullName = reader["FullName"].ToString();
                    member.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());
                    member.IsActive = int.Parse(reader["IsActive"].ToString());
                    member.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstMember.Add(member);

                }

                //System.IO.File.WriteAllLines(@"C:\inetpub\wwwroot\cmslottery\Logs/member7.txt", new string[1] { (lstMember != null ? lstMember.Count().ToString() : "null") });
                return lstMember;
            }
            catch (Exception ex)
            {
                System.IO.File.WriteAllLines(@"C:\inetpub\wwwroot\cmslottery\Logs/member6.txt", new string[1] { ex.Message });
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<MemberBO> GetMemberByID(int memberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                MemberBO member = null;
                List<MemberBO> lstMember = new List<MemberBO>();
                //string sql = "SP_GetCompanyByMemberID";
                string sql = "select * from Member where MemberID=@memberID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    member = new MemberBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.E_Wallet = reader["E_Wallet"].ToString();
                    member.Email = reader["Email"].ToString();
                    member.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());
                    member.IsActive = int.Parse(reader["IsActive"].ToString());
                    lstMember.Add(member);
                }
                return lstMember;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<CompanyInformationBO> GetCompanyByMemberID(int memberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                CompanyInformationBO company = null;
                List<CompanyInformationBO> lstCompany = new List<CompanyInformationBO>();
                //string sql = "SP_GetCompanyByMemberID";
                string sql = "select * from CompanyInformation where MemberID=@memberID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    company = new CompanyInformationBO();
                    company.MemberID = int.Parse(reader["MemberID"].ToString());
                    company.CompanyName = reader["CompanyName"].ToString();
                    company.Phone = reader["Phone"].ToString();
                    company.Email = reader["Email"].ToString();
                    company.Address = reader["Address"].ToString();
                    company.Website = reader["Website"].ToString();
                    company.Description = reader["Description"].ToString();

                    lstCompany.Add(company);
                }
                return lstCompany;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<MemberBO> ListAllMember()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<MemberBO> lstMember = new List<MemberBO>();
                string sql = "SP_ListAllMember";

                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberBO member = new MemberBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.E_Wallet = reader["E_Wallet"].ToString();
                    member.Email = reader["Email"].ToString();
                    member.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());
                    member.IsActive = int.Parse(reader["IsActive"].ToString());
                    //member.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstMember.Add(member);

                }
                return lstMember;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<MemberBO> GetListMemberBySearch(string keyword, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<MemberBO> lstMember = new List<MemberBO>();
                string sql = "SP_SearchMemberByAdmin";//SP_SearchMemberNoPaging
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@keyword", keyword);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberBO member = new MemberBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.E_Wallet = reader["E_Wallet"].ToString();
                    member.Email = reader["Email"].ToString();
                    member.FullName = reader["FullName"].ToString();
                    member.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());
                    member.IsActive = int.Parse(reader["IsActive"].ToString());
                    member.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstMember.Add(member);

                }
                return lstMember;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public ObjResultMessage CountAllMember(ModelRSA objRsa, ref MemberBO objMember)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                
                string sql = "CountAllMember";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    objMember = new MemberBO();
                    objMember.TotalAllMember = int.Parse(reader["TotalAllMember"].ToString());
                    objMember.TotalNewMember = int.Parse(reader["TotalNewMember"].ToString());
                }
               
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "MemberBusiness -> CountAllMember : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utilitys.WriteLog(fileLog, ex.Message);
               
            }
            finally
            {
                helper.destroy();
            }
            return objResult;
        }

        public int CountMemberBySearch(string keyword)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");

            try
            {

                string sql = "SP_SearchMemberPaging";
                int count = 0;
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@keyword", keyword);

                SqlCommand cmd = helper.GetCommand(sql, pa, true);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    count = Convert.ToInt32(dr["Totalkeyword"].ToString());
                }
                return count;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
