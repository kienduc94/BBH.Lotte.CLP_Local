﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using BBH.Lotte.CLP.Domain;
using BBC.Core.Database;
using System.IO;
using BBH.Lotte.CLP.Domain.Interfaces;

namespace BBH.Lotte.CLP.Data
{
    public class GroupAdminBusiness : IGroupAdminServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public bool InsertGroupAdmin(GroupAdminBO admin)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                SqlParameter[] pa = new SqlParameter[2];
                string sql = "SP_InsertGroupAdmin";
                pa[0] = new SqlParameter("@groupName", admin.GroupName);
                pa[1] = new SqlParameter("@isActive", admin.IsActive);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception insert groupadmin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool CheckUserGroupAdminNameExists(string groupName)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckGroupAdminName";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@groupName", groupName);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception CheckGroupName admin : " + ex.Message); return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<GroupAdminBO> ListGroupAdmin()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<GroupAdminBO> list = new List<GroupAdminBO>();
                string sql = "SP_List_GroupAdmin";

                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    GroupAdminBO group = new GroupAdminBO();
                    group.GroupID = int.Parse(reader["GroupID"].ToString());
                    group.GroupName = reader["GroupName"].ToString();
                    group.IsActive = int.Parse(reader["IsActive"].ToString());
                    list.Add(group);
                }
                return list;
            }
            catch
            {
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public GroupAdminBO GetGroupAdminDetail(int groupID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                GroupAdminBO group = null;
                string sql = "SP_GetGroupAdminDetails";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@groupID", groupID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    group = new GroupAdminBO();
                    group.GroupID = int.Parse(reader["GroupID"].ToString());
                    group.GroupName = reader["GroupName"].ToString();
                    group.IsActive = int.Parse(reader["IsActive"].ToString());

                }
                return group;
            }
            catch
            {
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateStatusGroupAdmin(int groupID, int isActive)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_UpdateStatusGroupAdmin";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@groupID", groupID);
                pa[1] = new SqlParameter("@isActive", isActive);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception update status groupadmin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateGroupAdmin(int groupID, string groupName)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_UpdateGroupAdmin";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@groupID", groupID);
                pa[1] = new SqlParameter("@groupName", groupName);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception update groupadmin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
