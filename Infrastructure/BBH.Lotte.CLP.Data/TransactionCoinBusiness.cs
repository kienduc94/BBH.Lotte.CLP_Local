﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Data
{
    public class TransactionCoinBusiness : ITransactionCoinServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public IEnumerable<TransactionCoinBO> ListTransactionCoin()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionCoinBO> lstTransaction = new List<TransactionCoinBO>();
                string sql = "SP_ListTransactionCoin";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionCoinBO transaction = new TransactionCoinBO();

                    transaction.TransactionID = reader["TransactionID"].ToString();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    transaction.WalletAddressID = int.Parse(reader["WalletAddressID"].ToString());
                    transaction.ValueTransaction = float.Parse(reader["ValueTransaction"].ToString());
                    transaction.QRCode = reader["QRCode"].ToString();
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.WalletID = int.Parse(reader["WalletID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.Note = reader["Note"].ToString();
                    transaction.TransactionBitcoin = reader["TransactionBitcoin"].ToString();
                    transaction.TypeTransactionID = int.Parse(reader["TypeTransactionID"].ToString());

                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<TransactionCoinBO> ListTransactionCoinPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionCoinBO> lstTransaction = new List<TransactionCoinBO>();
                string sql = "SP_ListTransactionCoinPaging";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionCoinBO transaction = new TransactionCoinBO();

                    transaction.TransactionID = reader["TransactionID"].ToString();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    //transaction.WalletAddressID = int.Parse(reader["WalletAddressID"].ToString());
                    transaction.ValueTransaction = float.Parse(reader["ValueTransaction"].ToString());
                    transaction.QRCode = reader["QRCode"].ToString();
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    //transaction.WalletID = int.Parse(reader["WalletID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.Note = reader["Note"].ToString();
                    transaction.TransactionBitcoin = reader["TransactionBitcoin"].ToString();
                    transaction.TypeTransactionID = int.Parse(reader["TypeTransactionID"].ToString());
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());

                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<TransactionCoinBO> ListTransactionCoinBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int status, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionCoinBO> lstTransaction = new List<TransactionCoinBO>();
                string sql = "SP_ListTransactionCoinBySearchCMS";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@memberID", memberID);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                pa[5] = new SqlParameter("@status", status);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionCoinBO transaction = new TransactionCoinBO();
                    transaction.TransactionID = reader["TransactionID"].ToString();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    transaction.ValueTransaction = float.Parse(reader["ValueTransaction"].ToString());
                    transaction.QRCode = reader["QRCode"].ToString();
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.Note = reader["Note"].ToString();
                    //if (!string.IsNullOrEmpty(reader["TransactionBitcoin"].ToString()))
                    //{
                        transaction.TransactionBitcoin = reader["TransactionBitcoin"].ToString();
                    //}
                    transaction.TypeTransactionID = int.Parse(reader["TypeTransactionID"].ToString());
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());

                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateStatusTransaction(string transactionID, int status)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_ListTransactionCoinStatus";
                SqlParameter[] pa = new SqlParameter[2];

                pa[0] = new SqlParameter("@transactionID", transactionID);
                pa[1] = new SqlParameter("@status", status);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        public ObjResultMessage CountAllCoin(ModelRSA objRsa, ref TransactionCoinBO objTransaction)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                
                string sql = "CountAllCoin";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    objTransaction = new TransactionCoinBO();
                    objTransaction.TotalAllCoin = int.Parse(reader["TotalAllCoin"].ToString());
                    objTransaction.TotalNewCoin = int.Parse(reader["TotalNewCoin"].ToString());
                }
                
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "TransactionCoinBusiness -> CountAllCoin : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utilitys.WriteLog(fileLog, ex.Message);
                
            }
            finally
            {
                helper.destroy();
            }
            return objResult;
        }
    }
}
