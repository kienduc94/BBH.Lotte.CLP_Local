﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Data
{
    public class TransactionPackageBusiness : ITransactionPackageServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public IEnumerable<TransactionPackageBO> ListAllTransactionPackage(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPackageBO> lstTransaction = new List<TransactionPackageBO>();
                string sql = "SP_ListAllTransactionPackage";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPackageBO transaction = new TransactionPackageBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.PackageID = int.Parse(reader["PackageID"].ToString());
                    transaction.PackageName = reader["PackageName"].ToString();
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<TransactionPackageBO> ListAllTransactionPackageByMember(int memberID, DateTime fromDate, DateTime toDate, int status, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPackageBO> lstTransaction = new List<TransactionPackageBO>();
                string sql = "SP_ListTransactionPackageBySearchCMS";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@memberID", memberID);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                pa[5] = new SqlParameter("@status", status);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPackageBO transaction = new TransactionPackageBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.PackageID = int.Parse(reader["PackageID"].ToString());
                    transaction.PackageName = reader["PackageName"].ToString();
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        transaction.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool UpdateStatusTransaction(int transactionID, int status)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateStatusTransaction";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@status", status);
                pa[1] = new SqlParameter("@transactionID", transactionID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        public PackageBO GetPackageDetail(int packageID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                PackageBO package = null;
                string sql = "SP_GetPackageDetail";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@packageID", packageID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    package = new PackageBO();
                    package.PackageID = int.Parse(reader["PackageID"].ToString());

                    package.PackageName = reader["PackageName"].ToString();
                    package.PackageValue = double.Parse(reader["PackageValue"].ToString());
                    package.NumberExpire = int.Parse(reader["NumberExpire"].ToString());
                   

                }
                return package;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public TransactionPackageBO CountAllPackage()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                TransactionPackageBO package = null;
                string sql = "CountAllPackage";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    package = new TransactionPackageBO();
                    package.TotalAllPackage = int.Parse(reader["TotalAllPackage"].ToString());
                    package.TotalNewPackage = int.Parse(reader["TotalNewPackage"].ToString());
                }
                return package;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
