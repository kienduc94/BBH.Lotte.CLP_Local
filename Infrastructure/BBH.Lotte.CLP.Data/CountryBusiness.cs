﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;

namespace BBH.Lotte.CLP.Data
{
   public class CountryBusiness : ICountryServices
    {

        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public ObjResultMessage InsertCountry(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            CountryBO objCountry = Algorithm.DecryptionObjectRSA<CountryBO>(objRsa.ParamRSAFirst);
            if (objCountry == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "CountryBusiness -> InsertCountry : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_InsertCountry_CMS";
                    SqlParameter[] pa = new SqlParameter[6];
                     pa[0] = new SqlParameter("@IsActive", objCountry.IsActive);
                    pa[1] = new SqlParameter("@IsDeleted", objCountry.IsDeleted);
                    pa[2] = new SqlParameter("@CountryName", objCountry.CountryName);
                    pa[3] = new SqlParameter("@CreatedDate", objCountry.CreatedDate);
                    pa[4] = new SqlParameter("@CreatedUser", objCountry.CreatedUser);
                    pa[5] = new SqlParameter("@PhoneZipCode", objCountry.PhoneZipCode);

                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "CountryBusiness -> InsertCountry : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public IEnumerable<CountryBO> GetListCountry(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                Utilitys.WriteLog(fileLog, "Exception CheckAwardNameEnglish :5 ");
                List<CountryBO> lstAward = new List<CountryBO>();
                string sql = "SP_GetListCountry_CMS";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);

                Utilitys.WriteLog(fileLog, "Exception CheckAwardNameEnglish :3 ");
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Utilitys.WriteLog(fileLog, "Exception CheckAwardNameEnglish :1 ");
                    CountryBO country = new CountryBO();
                    country.CountryID = int.Parse(reader["CountryID"].ToString());
                    if (!string.IsNullOrEmpty(reader["CountryName"].ToString())) country.CountryName = reader["CountryName"].ToString();
                    if (!string.IsNullOrEmpty(reader["PhoneZipCode"].ToString())) country.PhoneZipCode = reader["PhoneZipCode"].ToString();
                    if (!string.IsNullOrEmpty(reader["IsActive"].ToString())) country.IsActive = int.Parse(reader["IsActive"].ToString());
                    if (!string.IsNullOrEmpty(reader["IsDeleted"].ToString())) country.IsDeleted = int.Parse(reader["IsDeleted"].ToString());
                    if (!string.IsNullOrEmpty(reader["CreatedUser"].ToString())) country.CreatedUser = reader["CreatedUser"].ToString();
                    if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString())) country.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["DeletedDate"].ToString())) country.DeletedDate = DateTime.Parse(reader["DeletedDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["DeletedUser"].ToString())) country.DeletedUser = reader["DeletedUser"].ToString();
                    if (!string.IsNullOrEmpty(reader["UpdatedDate"].ToString())) country.UpdatedDate = DateTime.Parse(reader["UpdatedDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["UpdatedUser"].ToString())) country.UpdatedUser = reader["UpdatedUser"].ToString();
                    if (!string.IsNullOrEmpty(reader["TOTALROWS"].ToString())) country.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstAward.Add(country);
                    Utilitys.WriteLog(fileLog, "Exception CheckAwardNameEnglish :2 ");
                }
                Utilitys.WriteLog(fileLog, "Exception CheckAwardNameEnglish :3 ");
                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception CheckAwardNameEnglish :4 ");
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public ObjResultMessage UpdateCountry(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            CountryBO objCountry = Algorithm.DecryptionObjectRSA<CountryBO>(objRsa.ParamRSAFirst);
            if (objCountry == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "CountryBusiness -> UpdateCountry : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_UpdateCountry_CMS";
                    SqlParameter[] pa = new SqlParameter[7];
                   
                    pa[0] = new SqlParameter("@CountryName", objCountry.CountryName);
                    pa[1] = new SqlParameter("@UpdatedDate", objCountry.UpdatedDate);
                    pa[2] = new SqlParameter("@UpdatedUser", objCountry.UpdatedUser);
                    pa[3] = new SqlParameter("@PhoneZipCode", objCountry.PhoneZipCode);
                    pa[4] = new SqlParameter("@CountryID", objCountry.CountryID);
                    pa[5] = new SqlParameter("@IsActive", objCountry.IsActive);
                    pa[6] = new SqlParameter("@IsDeleted", objCountry.IsDeleted);

                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "CountryBusiness -> UpdateCountry : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public ObjResultMessage LockAndUnlockCountry(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            CountryBO objCountry = Algorithm.DecryptionObjectRSA<CountryBO>(objRsa.ParamRSAFirst);
            if (objCountry == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "CountryBusiness -> LockAndUnlockCountry : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_LockAndUnlockCountry_CMS";
                    SqlParameter[] pa = new SqlParameter[2];
                    pa[0] = new SqlParameter("@IsActive", objCountry.IsActive);
                    pa[1] = new SqlParameter("@CountryID", objCountry.CountryID);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "CountryBusiness -> LockAndUnlockCountry : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public bool CheckCountryNameExists(int countryID, string countryName)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckCountryNameExits_CMS";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@countryName", countryName);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception CheckAwardNameEnglish : " + ex.Message); return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool CheckPhoneZipCodeExists(int countryID, string phoneZipCode)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckPhoneZipCodeExists_CMS";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@phoneZipCode", phoneZipCode);
                pa[1] = new SqlParameter("@countryID", countryID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception CheckPhoneZipCodeExists : " + ex.Message); return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<CountryBO> GetListCountryBySearch(string keyword, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<CountryBO> lstCountry = new List<CountryBO>();
                string sql = "SP_GetListCountryBySearch_CMS";//SP_SearchMemberNoPaging
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@Keyword", keyword);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CountryBO country = new CountryBO();
                    country.CountryID = int.Parse(reader["CountryID"].ToString());
                    if (!string.IsNullOrEmpty(reader["CountryName"].ToString())) country.CountryName = reader["CountryName"].ToString();
                    if (!string.IsNullOrEmpty(reader["PhoneZipCode"].ToString())) country.PhoneZipCode = reader["PhoneZipCode"].ToString();
                    if (!string.IsNullOrEmpty(reader["IsActive"].ToString())) country.IsActive = int.Parse(reader["IsActive"].ToString());
                    if (!string.IsNullOrEmpty(reader["IsDeleted"].ToString())) country.IsDeleted = int.Parse(reader["IsDeleted"].ToString());
                    if (!string.IsNullOrEmpty(reader["CreatedUser"].ToString())) country.CreatedUser = reader["CreatedUser"].ToString();
                    if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString())) country.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["DeletedDate"].ToString())) country.DeletedDate = DateTime.Parse(reader["DeletedDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["DeletedUser"].ToString())) country.DeletedUser = reader["DeletedUser"].ToString();
                    if (!string.IsNullOrEmpty(reader["UpdatedDate"].ToString())) country.UpdatedDate = DateTime.Parse(reader["UpdatedDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["UpdatedUser"].ToString())) country.UpdatedUser = reader["UpdatedUser"].ToString();
                    if (!string.IsNullOrEmpty(reader["TOTALROWS"].ToString())) country.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstCountry.Add(country);

                }
                return lstCountry;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

    }
}
