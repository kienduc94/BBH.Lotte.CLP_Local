﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Data
{
    public class FreeTicketBusiness: IFreeTicketServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public bool InsertFreeTicket(int intNumberTicket)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            bool rs = false;
            try
            {
                string sql = "SP_InsertFreeTicketDaily";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@NumberTicket", intNumberTicket);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
            }
            catch (Exception ex)
            {
                rs = false;
                Utilitys.WriteLog(fileLog, ex.Message);

            }
            finally
            {
                helper.destroy();
            }
            return rs;
        }
    }
}
