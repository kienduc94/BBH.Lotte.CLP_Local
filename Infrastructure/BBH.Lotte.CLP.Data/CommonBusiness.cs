﻿using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Data
{
    public class CommonBusiness: ICommonServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public ObjResultMessage CheckAuthenticate(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            bool bolIsResult = false;
            try
            {
                objResult = _CheckIPConfig(helper, objRsa, ref bolIsResult);

            }
            catch (Exception ex)
            {
               // Utilitys.WriteLog(fileLog, ex.Message);
            }
            finally
            {
                helper.destroy();
            }

            return objResult;
        }


        public ObjResultMessage _CheckIPConfig(Sqlhelper helper, ModelRSA objRsa, ref bool bolIsResult)
        {
            IPConfigBO objIPConfig = Algorithm.DecryptionObjectRSA<IPConfigBO>(objRsa.ParamRSAFirst);
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                string sql = "SP_CheckIPConfig_CMS";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@IPAddress", objIPConfig.IPAddress);
                pa[1] = new SqlParameter("@ServiceDomain", objIPConfig.ServiceDomain);
                pa[2] = new SqlParameter("@UserName", objIPConfig.UserName);
               // pa[3] = new SqlParameter("@Password", objIPConfig.Password);

                SqlCommand command = helper.GetCommand(sql, pa, true);

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    objResult.IsError = false;
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Notfound;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "CommonBusiness -> _CheckIPConfig : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
               // Utilitys.WriteLog(fileLog, ex.Message);

            }
            return objResult;
        }
    }
}
