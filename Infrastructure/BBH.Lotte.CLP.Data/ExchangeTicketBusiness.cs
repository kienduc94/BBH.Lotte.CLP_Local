﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBH.Lotte.CLP.Domain.Interfaces;

namespace BBH.Lotte.CLP.Data
{
    public class ExchangeTicketBusiness : IExchangeTicketServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public IEnumerable<ExchangeTicketBO> ListAllExchangeTicket()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<ExchangeTicketBO> lstTransaction = new List<ExchangeTicketBO>();
                string sql = "SP_ListAllExchangeNoPaging";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExchangeTicketBO exchangeTicket = new ExchangeTicketBO();

                    exchangeTicket.ExChangeID = int.Parse(reader["ExChangeID"].ToString());
                    exchangeTicket.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    exchangeTicket.CreateUser = reader["CreateUser"].ToString();
                    exchangeTicket.DeleteUser = reader["DeleteUser"].ToString();
                    exchangeTicket.DeleteDate = DateTime.Parse(reader["DeleteDate"].ToString());
                    exchangeTicket.IsActive = int.Parse(reader["IsActive"].ToString());
                    exchangeTicket.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    exchangeTicket.MemberID = int.Parse(reader["MemberID"].ToString());
                    exchangeTicket.PointValue = float.Parse(reader["PointValue"].ToString());
                    exchangeTicket.TicketNumber = int.Parse(reader["TicketNumber"].ToString());
                    exchangeTicket.UpdateDate = DateTime.Parse(reader["UpdateDate"].ToString());
                    exchangeTicket.UpdateUser = reader["UpdateUser"].ToString();
                    exchangeTicket.CoinID = reader["CoiID"].ToString();

                    lstTransaction.Add(exchangeTicket);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<ExchangeTicketBO> ListAllExchangeTicketPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<ExchangeTicketBO> lstTransaction = new List<ExchangeTicketBO>();
                string sql = "SP_ListAllExchangeTicket";

                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);

                SqlCommand command = helper.GetCommand(sql,pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExchangeTicketBO exchangeTicket = new ExchangeTicketBO();

                    exchangeTicket.ExChangeID = int.Parse(reader["ExChangeID"].ToString());
                    exchangeTicket.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    exchangeTicket.CreateUser = reader["CreateUser"].ToString();
                    exchangeTicket.DeleteUser = reader["DeleteUser"].ToString();
                    try
                    {
                        exchangeTicket.DeleteDate = DateTime.Parse(reader["DeleteDate"].ToString());
                    }
                    catch { }
                    exchangeTicket.IsActive = int.Parse(reader["IsActive"].ToString());
                    exchangeTicket.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    exchangeTicket.MemberID = int.Parse(reader["MemberID"].ToString());
                    exchangeTicket.PointValue = float.Parse(reader["PointValue"].ToString());
                    exchangeTicket.TicketNumber = int.Parse(reader["TicketNumber"].ToString());
                    try
                    {
                        exchangeTicket.UpdateDate = DateTime.Parse(reader["UpdateDate"].ToString());
                    }
                    catch { }
                    exchangeTicket.UpdateUser = reader["UpdateUser"].ToString();
                    exchangeTicket.CoinID = reader["CoinID"].ToString();
                    exchangeTicket.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());

                    lstTransaction.Add(exchangeTicket);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool LockAndUnlockExchangeTicket(int exchangeID, int isActive)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_LockAndUnlockExchangeTicket";
                SqlParameter[] pa = new SqlParameter[2];

                pa[0] = new SqlParameter("@exchangeID", exchangeID);
                pa[1] = new SqlParameter("@isActive", isActive);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateStatusExchangeTicket(int exchangeID, int status, DateTime deleteDate, string deleteUser)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateStatusExchangeTicket";
                SqlParameter[] pa = new SqlParameter[4];

                pa[0] = new SqlParameter("@exchangeID", exchangeID);
                pa[1] = new SqlParameter("@status", status);
                pa[2] = new SqlParameter("@deleteDate", deleteDate);
                pa[3] = new SqlParameter("@deleteUser", deleteUser);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<ExchangeTicketBO> ListExchangeTicketBySearch(DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<ExchangeTicketBO> lstTransaction = new List<ExchangeTicketBO>();
                string sql = "SP_ListAllExchangeBySearch";

                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExchangeTicketBO exchangeTicket = new ExchangeTicketBO();

                    exchangeTicket.ExChangeID = int.Parse(reader["ExChangeID"].ToString());
                    exchangeTicket.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    exchangeTicket.CreateUser = reader["CreateUser"].ToString();
                    exchangeTicket.DeleteUser = reader["DeleteUser"].ToString();
                    try
                    {
                        exchangeTicket.DeleteDate = DateTime.Parse(reader["DeleteDate"].ToString());
                    }
                    catch { }
                    exchangeTicket.IsActive = int.Parse(reader["IsActive"].ToString());
                    exchangeTicket.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    exchangeTicket.MemberID = int.Parse(reader["MemberID"].ToString());
                    exchangeTicket.PointValue = float.Parse(reader["PointValue"].ToString());
                    exchangeTicket.TicketNumber = int.Parse(reader["TicketNumber"].ToString());
                    try
                    {
                        exchangeTicket.UpdateDate = DateTime.Parse(reader["UpdateDate"].ToString());
                    }
                    catch { }
                    exchangeTicket.UpdateUser = reader["UpdateUser"].ToString();
                    exchangeTicket.CoinID = reader["CoinID"].ToString();

                    lstTransaction.Add(exchangeTicket);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool InsertExchangeTicket(ExchangeTicketBO exchangeTicket)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_InsertExchangeTicket";
                SqlParameter[] pa = new SqlParameter[12];

                pa[0] = new SqlParameter("@pointValue", exchangeTicket.PointValue);
                pa[1] = new SqlParameter("@ticketNumber", exchangeTicket.TicketNumber);
                pa[2] = new SqlParameter("@isActive", exchangeTicket.IsActive);
                pa[3] = new SqlParameter("@isDelete", exchangeTicket.IsDelete);
                pa[4] = new SqlParameter("@createDate", exchangeTicket.CreateDate);
                pa[5] = new SqlParameter("@createUser", exchangeTicket.CreateUser);
                pa[6] = new SqlParameter("@memberID", exchangeTicket.MemberID);
                pa[7] = new SqlParameter("@updateDate", exchangeTicket.UpdateDate);
                pa[8] = new SqlParameter("@updateUser", exchangeTicket.UpdateUser);
                pa[9] = new SqlParameter("@deleteDate", exchangeTicket.DeleteDate);
                pa[10] = new SqlParameter("@deleteUser", exchangeTicket.DeleteUser);
                pa[11] = new SqlParameter("@coinID", exchangeTicket.CoinID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateExchangeTicket(ExchangeTicketBO exchangeTicket)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateExchangeTicket";
                SqlParameter[] pa = new SqlParameter[7];

                pa[0] = new SqlParameter("@exchangeID", exchangeTicket.ExChangeID);
                pa[1] = new SqlParameter("@pointValue", exchangeTicket.PointValue);
                pa[2] = new SqlParameter("@ticketNumber", exchangeTicket.TicketNumber);              
                pa[3] = new SqlParameter("@createUser", exchangeTicket.CreateUser);
                pa[4] = new SqlParameter("@updateDate", exchangeTicket.UpdateDate);
                pa[5] = new SqlParameter("@updateUser", exchangeTicket.UpdateUser);
                pa[6] = new SqlParameter("@coinID", exchangeTicket.CoinID);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<CoinBO> ListAllCoin()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<CoinBO> lstCoin = new List<CoinBO>();
                string sql = "SP_ListAllCoin";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CoinBO coin = new CoinBO();

                    coin.CoidID = reader["CoinID"].ToString();
                    coin.CoinName = reader["CoinName"].ToString();
                    coin.IsActive = int.Parse(reader["IsActive"].ToString());

                    lstCoin.Add(coin);
                }
                return lstCoin;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool CheckCoinIDExists(string coinID, int exchangeID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckCoinIDExist";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@coinID", coinID);
                pa[1] = new SqlParameter("@exchangeID", exchangeID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception Check CoinID Exist : " + ex.Message); return false;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
