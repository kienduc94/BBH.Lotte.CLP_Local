﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Data
{
    public class SummaryBusiness : IBookingSummaryServices
    {
        public IEnumerable<BookingSummaryBO> ListTicketSummaryByPoint(int year, int isFreeTicket)
        {
           
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingSummaryBO> lstBooking = new List<BookingSummaryBO>();
                string sql = "SP_ListTicketByMonth_CMS";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@IsFreeTicket", isFreeTicket);
                pa[1] = new SqlParameter("@Year", year);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingSummaryBO objBooking = new BookingSummaryBO();
                    if (!string.IsNullOrEmpty(reader["TotalTicket"].ToString())) objBooking.TotalTicket = int.Parse(reader["TotalTicket"].ToString());
                    if (!string.IsNullOrEmpty(reader["MonthDate"].ToString())) objBooking.Month = int.Parse(reader["MonthDate"].ToString());
                    lstBooking.Add(objBooking);

                }
                return lstBooking;
            }
            catch (Exception ex)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("Exception ListTicketSummaryByPoint : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<BookingSummaryBO> ListTicketSummaryByFree(int year, int isFreeTicket)
        {

            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingSummaryBO> lstBooking = new List<BookingSummaryBO>();
                string sql = "SP_ListTicketByMonth_CMS";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@IsFreeTicket", isFreeTicket);
                pa[1] = new SqlParameter("@Year", year);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingSummaryBO objBooking = new BookingSummaryBO();
                    if (!string.IsNullOrEmpty(reader["TotalTicket"].ToString())) objBooking.TotalTicket = int.Parse(reader["TotalTicket"].ToString());
                    if (!string.IsNullOrEmpty(reader["MonthDate"].ToString())) objBooking.Month = int.Parse(reader["MonthDate"].ToString());
                    lstBooking.Add(objBooking);

                }
                return lstBooking;
            }
            catch (Exception ex)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("Exception ListTicketSummaryByFree : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


        public IEnumerable<PrizeBO> ListPrize()
        {

            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<PrizeBO> lstPrize = new List<PrizeBO>();
                string sql = "SP_ListSummaryPrize_CMS";
               
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    PrizeBO objPrize = new PrizeBO();
                    if (!string.IsNullOrEmpty(reader["AwardID"].ToString())) objPrize.AwardID = int.Parse(reader["AwardID"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardName"].ToString())) objPrize.AwardName = reader["AwardName"].ToString();
                    if (!string.IsNullOrEmpty(reader["awardvalues"].ToString())) objPrize.AwardValues = double.Parse(reader["awardvalues"].ToString());
                    lstPrize.Add(objPrize);

                }
                return lstPrize;
            }
            catch (Exception ex)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("Exception ListPrize : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

    }
}
