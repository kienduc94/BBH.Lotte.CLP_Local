﻿using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain.ObjDomain;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Data
{
    public class AwardWithdrawBusiness : IAwardWithdrawServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public ObjResultMessage ListAwardWithdraw(ModelRSA objRsa, ref ModelRSA objRsaReturn)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            List<AwardWithdrawBO> lstAwardWithdraw = new List<AwardWithdrawBO>();

            ObjSearchAwardWithdraw objSearch = Algorithm.DecryptionObjectRSA<ObjSearchAwardWithdraw>(objRsa.ParamRSAFirst);
            if (objSearch == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardWithdrawBusiness -> ListAwardWithdraw : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_ListAwardWithdraw_CMS";
                    SqlParameter[] pa = new SqlParameter[8];
                    pa[0] = new SqlParameter("@UserName", objSearch.RquestMemberID);
                    pa[1] = new SqlParameter("@FromDate", objSearch.RequestFromDate);
                    pa[2] = new SqlParameter("@ToDate", objSearch.RequestToDate);
                    pa[3] = new SqlParameter("@RequestStatus", objSearch.RequestStatus);
                    pa[4] = new SqlParameter("@AwardDate", objSearch.AwardDate);
                    pa[5] = new SqlParameter("@Start", objSearch.StartIndex);
                    pa[6] = new SqlParameter("@End", objSearch.EndIndex);
                    pa[7] = new SqlParameter("@RequestDate", objSearch.RequestDate);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        AwardWithdrawBO objAward = new AwardWithdrawBO();
                        objAward.RequestMemberID = reader["RequestMemberID"].ToString();
                        objAward.TransactionID = reader["TransactionID"].ToString();
                        objAward.RequestStatus = int.Parse(reader["RequestStatus"].ToString());

                        objAward.RequestWalletAddress = reader["RequestWalletAddress"].ToString();
                        objAward.ValuesWithdraw = double.Parse(reader["ValuesWithdraw"].ToString());
                        objAward.AwardDate = DateTime.Parse(reader["AwardDate"].ToString());
                        objAward.CoinIDWithdraw = reader["CoinIDWithdraw"].ToString();
                        objAward.RequestDate = DateTime.Parse(reader["RequestDate"].ToString());
                        objAward.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                        lstAwardWithdraw.Add(objAward);
                    }
                    if (lstAwardWithdraw != null && lstAwardWithdraw.Count() > 0)
                    {
                        string strParameterObj = Algorithm.EncryptionObjectRSA<List<AwardWithdrawBO>>(lstAwardWithdraw);
                        objRsaReturn = new ModelRSA(strParameterObj);
                    }

                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardWithdrawBusiness -> ListAwardWithdraw : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utility.WriteLog(pathLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public ObjResultMessage GetAwardWithdrawDetail(ModelRSA objRsa, ref ModelRSA objRsaReturn)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            AwardWithdrawBO objAward = null;
            AwardWithdrawBO objAwardPara = Algorithm.DecryptionObjectRSA<AwardWithdrawBO>(objRsa.ParamRSAFirst);
            if (objAwardPara == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardWithdrawBusiness -> GetAwardWithdrawDetail : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_GetAwardWithdrawDetail_CMS";
                    SqlParameter[] pa = new SqlParameter[1];
                    pa[0] = new SqlParameter("@TransactionID", objAwardPara.TransactionID);

                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        objAward = new AwardWithdrawBO();
                        objAward.RequestMemberID = reader["RequestMemberID"].ToString();
                        objAward.TransactionID = reader["TransactionID"].ToString();
                        if (!string.IsNullOrEmpty(reader["RequestStatus"].ToString()))
                        {
                            objAward.RequestStatus = int.Parse(reader["RequestStatus"].ToString());
                        }
                        objAward.RequestWalletAddress = reader["RequestWalletAddress"].ToString();
                        if (!string.IsNullOrEmpty(reader["ValuesWithdraw"].ToString()))
                        {
                            objAward.ValuesWithdraw = double.Parse(reader["ValuesWithdraw"].ToString());
                        }
                        if (!string.IsNullOrEmpty(reader["AwardDate"].ToString()))
                        {
                            objAward.AwardDate = DateTime.Parse(reader["AwardDate"].ToString());
                        }
                        if (!string.IsNullOrEmpty(reader["CoinIDWithdraw"].ToString()))
                        {
                            objAward.CoinIDWithdraw = reader["CoinIDWithdraw"].ToString();
                        }
                        if (!string.IsNullOrEmpty(reader["RequestDate"].ToString()))
                        {
                            objAward.RequestDate = DateTime.Parse(reader["RequestDate"].ToString());
                        }
                        if (!string.IsNullOrEmpty(reader["AdminIDApprove"].ToString()))
                        {
                            objAward.AdminIDApprove = reader["AdminIDApprove"].ToString();
                        }
                        if (!string.IsNullOrEmpty(reader["ApproveDate"].ToString()))
                        {
                            objAward.ApproveDate = DateTime.Parse(reader["ApproveDate"].ToString());
                        }
                    }
                    if (objAward != null)
                    {
                        string strParameterObj = Algorithm.EncryptionObjectRSA<AwardWithdrawBO>(objAward);
                        objRsaReturn = new ModelRSA(strParameterObj);
                    }

                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardWithdrawBusiness -> GetAwardWithdrawDetail : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utility.WriteLog(pathLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public ObjResultMessage UpdateStatusAwardWithdraw(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();

            AwardWithdrawBO objAward = Algorithm.DecryptionObjectRSA<AwardWithdrawBO>(objRsa.ParamRSAFirst);
            if (objAward == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardWithdrawBusiness -> UpdateStatusAwardWithdraw : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_UpdateStatusAwardWithdraw_CMS";
                    SqlParameter[] pa = new SqlParameter[4];
                    pa[0] = new SqlParameter("@TransactionID", objAward.TransactionID);
                    pa[1] = new SqlParameter("@RequestStatus", objAward.RequestStatus);
                    pa[2] = new SqlParameter("@UpdatedUser", objAward.UpdatedUser);
                    pa[3] = new SqlParameter("@AdminIDApprove", objAward.AdminIDApprove);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                    else
                    {
                        objResult.IsError = true;
                    }

                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardWithdrawBusiness -> UpdateStatusAwardWithdraw : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utility.WriteLog(pathLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }
    }
}
