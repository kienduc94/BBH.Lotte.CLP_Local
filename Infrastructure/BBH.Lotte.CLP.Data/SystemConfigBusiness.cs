﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBH.Lotte.CLP.Domain;
using BBC.Core.Database;
using System.Configuration;
using System.IO;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using BBC.Core.Common.Utils;
using System.Data.SqlClient;

namespace BBH.Lotte.CLP.Data
{
    public class SystemConfigBusiness : ISystemConfigServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public ObjResultMessage UpdateStatusActive(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            SystemConfigBO objsysConfig = Algorithm.DecryptionObjectRSA<SystemConfigBO>(objRsa.ParamRSAFirst);
            if (objsysConfig == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "SystemConfigBusiness -> UpdateStatusActive : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_UpdateStatusActive_CMS";
                    SqlParameter[] pa = new SqlParameter[2];
                    pa[0] = new SqlParameter("@isAutoLottery", objsysConfig.IsAutoLottery);
                    pa[1] = new SqlParameter("@configID", objsysConfig.ConfigID);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "SystemConfigBusiness -> UpdateStatusActive : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public IEnumerable<SystemConfigBO> GetListSystemConfig()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<SystemConfigBO> lstAward = new List<SystemConfigBO>();
                string sql = "SP_GetListSystemConfig_CMS";

                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SystemConfigBO system = new SystemConfigBO();
                    if (!Convert.IsDBNull(reader["ConfigID"])) system.ConfigID = int.Parse(reader["ConfigID"].ToString());
                    if (!Convert.IsDBNull(reader["IsAutoLottery"])) system.IsAutoLottery = int.Parse(reader["IsAutoLottery"].ToString());

                    if (!Convert.IsDBNull(reader["CreatedDate"])) system.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());

                    if (!Convert.IsDBNull(reader["CreatedUser"])) system.CreatedUser = reader["CreatedUser"].ToString();
                    if (!Convert.IsDBNull(reader["DeletedDate"])) system.DeletedDate = DateTime.Parse(reader["DeletedDate"].ToString());
                    if (!Convert.IsDBNull(reader["DeletedUser"])) system.DeletedUser = reader["DeletedUser"].ToString();
                    if (!Convert.IsDBNull(reader["UpdatedDate"])) system.UpdatedDate = DateTime.Parse(reader["UpdatedDate"].ToString());
                    if (!Convert.IsDBNull(reader["UpdatedUser"])) system.UpdatedUser = reader["UpdatedUser"].ToString();

                    lstAward.Add(system);
                }
                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
