﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Data
{
    public class ExchangePointBusiness : IExchangePointServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public IEnumerable<ExchangePointBO> ListAllExchangePoint()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<ExchangePointBO> lstTransaction = new List<ExchangePointBO>();
                string sql = "SP_ListAllExchangePoint";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExchangePointBO exchangePoint = new ExchangePointBO();

                    exchangePoint.ExChangeID = int.Parse(reader["ExChangeID"].ToString());
                    exchangePoint.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    exchangePoint.CreateUser = reader["CreateUser"].ToString();
                    exchangePoint.DeleteUser = reader["DeleteUser"].ToString();
                    exchangePoint.DeleteDate = DateTime.Parse(reader["DeleteDate"].ToString());
                    exchangePoint.IsActive = int.Parse(reader["IsActive"].ToString());
                    exchangePoint.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    exchangePoint.MemberID = int.Parse(reader["MemberID"].ToString());
                    exchangePoint.PointValue = float.Parse(reader["PointValue"].ToString());
                    exchangePoint.BitcoinValue = int.Parse(reader["BitcoinValue"].ToString());
                    exchangePoint.UpdateDate = DateTime.Parse(reader["UpdateDate"].ToString());
                    exchangePoint.UpdateUser = reader["UpdateUser"].ToString();

                    lstTransaction.Add(exchangePoint);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<ExchangePointBO> ListAllExchangePointPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<ExchangePointBO> lstTransaction = new List<ExchangePointBO>();
                string sql = "SP_ListAllExchangePointPaging";

                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExchangePointBO exchangePoint = new ExchangePointBO();

                    exchangePoint.ExChangeID = int.Parse(reader["ExChangeID"].ToString());
                    exchangePoint.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    exchangePoint.CreateUser = reader["CreateUser"].ToString();
                    exchangePoint.DeleteUser = reader["DeleteUser"].ToString();
                    //exchangePoint.DeleteDate = DateTime.Parse(reader["DeleteDate"].ToString());
                    exchangePoint.IsActive = int.Parse(reader["IsActive"].ToString());
                    exchangePoint.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    exchangePoint.MemberID = int.Parse(reader["MemberID"].ToString());
                    exchangePoint.PointValue = float.Parse(reader["PointValue"].ToString());
                    exchangePoint.BitcoinValue = float.Parse(reader["BitcoinValue"].ToString());
                    //exchangePoint.UpdateDate = DateTime.Parse(reader["UpdateDate"].ToString());
                    exchangePoint.UpdateUser = reader["UpdateUser"].ToString();

                    exchangePoint.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());

                    lstTransaction.Add(exchangePoint);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool LockAndUnlockExchangePoint(int exchangeID, int isActive)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_LockAndUnlockExchangePoint";
                SqlParameter[] pa = new SqlParameter[2];

                pa[0] = new SqlParameter("@exchangeID", exchangeID);
                pa[1] = new SqlParameter("@isActive", isActive);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateStatusExchangePoint(int exchangeID, int status, DateTime deleteDate, string deleteUser)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateStatusExchangePoint";
                SqlParameter[] pa = new SqlParameter[4];

                pa[0] = new SqlParameter("@exchangeID", exchangeID);
                pa[1] = new SqlParameter("@status", status);
                pa[2] = new SqlParameter("@deleteDate", deleteDate);
                pa[3] = new SqlParameter("@deleteUser", deleteUser);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<ExchangePointBO> ListExchangePointBySearch(DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<ExchangePointBO> lstTransaction = new List<ExchangePointBO>();
                string sql = "SP_ListAllExchangePointBySearch";

                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExchangePointBO exchangePoint = new ExchangePointBO();

                    exchangePoint.ExChangeID = int.Parse(reader["ExChangeID"].ToString());
                    exchangePoint.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    exchangePoint.CreateUser = reader["CreateUser"].ToString();
                    exchangePoint.DeleteUser = reader["DeleteUser"].ToString();
                    try
                    {
                        exchangePoint.DeleteDate = DateTime.Parse(reader["DeleteDate"].ToString());
                    }
                    catch { }
                    exchangePoint.IsActive = int.Parse(reader["IsActive"].ToString());
                    exchangePoint.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    exchangePoint.MemberID = int.Parse(reader["MemberID"].ToString());
                    exchangePoint.PointValue = float.Parse(reader["PointValue"].ToString());
                    exchangePoint.BitcoinValue = int.Parse(reader["BitcoinValue"].ToString());
                    try
                    {
                        exchangePoint.UpdateDate = DateTime.Parse(reader["UpdateDate"].ToString());
                    }
                    catch { }
                    exchangePoint.UpdateUser = reader["UpdateUser"].ToString();

                    lstTransaction.Add(exchangePoint);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool InsertExchangePoint(ExchangePointBO exchangePoint)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_InsertExchangePoint";
                SqlParameter[] pa = new SqlParameter[11];

                pa[0] = new SqlParameter("@pointValue", exchangePoint.PointValue);
                pa[1] = new SqlParameter("@bitcoinValue", exchangePoint.BitcoinValue);
                pa[2] = new SqlParameter("@isActive", exchangePoint.IsActive);
                pa[3] = new SqlParameter("@isDelete", exchangePoint.IsDelete);
                pa[4] = new SqlParameter("@createDate", exchangePoint.CreateDate);
                pa[5] = new SqlParameter("@createUser", exchangePoint.CreateUser);
                pa[6] = new SqlParameter("@memberID", exchangePoint.MemberID);
                pa[7] = new SqlParameter("@updateDate", exchangePoint.UpdateDate);
                pa[8] = new SqlParameter("@updateUser", exchangePoint.UpdateUser);
                pa[9] = new SqlParameter("@deleteDate", exchangePoint.DeleteDate);
                pa[10] = new SqlParameter("@deleteUser", exchangePoint.DeleteUser);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateExchangePoint(ExchangePointBO exchangePoint)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateExchangePoint";
                SqlParameter[] pa = new SqlParameter[6];

                pa[0] = new SqlParameter("@exchangeID", exchangePoint.ExChangeID);
                pa[1] = new SqlParameter("@pointValue", exchangePoint.PointValue);
                pa[2] = new SqlParameter("@bitcoinValue", exchangePoint.BitcoinValue);
                pa[3] = new SqlParameter("@createUser", exchangePoint.CreateUser);
                pa[4] = new SqlParameter("@updateDate", exchangePoint.UpdateDate);
                pa[5] = new SqlParameter("@updateUser", exchangePoint.UpdateUser);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

    }
}
