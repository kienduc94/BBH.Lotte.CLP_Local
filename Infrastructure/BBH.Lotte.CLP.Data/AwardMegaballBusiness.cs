﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBH.Lotte.CLP.Domain;
using BBC.Core.Database;
using System.Configuration;
using System.IO;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using BBC.Core.Common.Utils;
using System.Data;
using Newtonsoft.Json;

namespace BBH.Lotte.CLP.Data
{
    public class AwardMegaballBusiness : IAwardMegaballServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public ObjResultMessage InsertAwardMegaball(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            AwardMegaballBO awardMegaball = Algorithm.DecryptionObjectRSA<AwardMegaballBO>(objRsa.ParamRSAFirst);
            if (awardMegaball == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardMegaballBusiness -> InsertAwardMegaball : Null Object  ";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_InsertAwardMegaball";
                    SqlParameter[] pa = new SqlParameter[15];

                    pa[0] = new SqlParameter("@awardID", awardMegaball.AwardID);
                    pa[1] = new SqlParameter("@createDate", awardMegaball.CreateDate);
                    pa[2] = new SqlParameter("@isActive", awardMegaball.IsActive);
                    pa[3] = new SqlParameter("@isDelete", awardMegaball.IsDelete);
                    pa[4] = new SqlParameter("@priority", awardMegaball.Priority);

                    pa[5] = new SqlParameter("@firstNumber", awardMegaball.FirstNumber);
                    pa[6] = new SqlParameter("@secondNumber", awardMegaball.SecondNumber);
                    pa[7] = new SqlParameter("@thirdNumber", awardMegaball.ThirdNumber);
                    pa[8] = new SqlParameter("@fourthNumber", awardMegaball.FourthNumber);
                    pa[9] = new SqlParameter("@fivethNumber", awardMegaball.FivethNumber);
                    pa[10] = new SqlParameter("@extraNumber", awardMegaball.ExtraNumber);
                    pa[11] = new SqlParameter("@GenCode", awardMegaball.GenCode);
                    pa[12] = new SqlParameter("@jackPotDefault", awardMegaball.JackPot);
                    pa[13] = new SqlParameter("@DrawID", awardMegaball.DrawID);

                    SqlParameter objParamOut = new SqlParameter();
                    objParamOut.ParameterName = "@isExists";
                    objParamOut.DbType = System.Data.DbType.Int32;
                    objParamOut.Direction = System.Data.ParameterDirection.Output;
                    pa[14] = objParamOut;

                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();

                    var objOutput = command.Parameters["@isExists"];

                    if (row > 0 || (objOutput != null && Convert.ToInt32(objOutput.Value.ToString()) == 1))// System.Data.SqlClient.SqlParameter
                    {
                        objResult.IsError = false;

                        if (objOutput != null && Convert.ToInt32(objOutput.Value.ToString()) == 1)
                        {
                            objResult.MessageDetail = "CreateDate Award Megaball Exist";
                        }
                    }

                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardMegaballBusiness -> InsertAwardMegaball : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public ObjResultMessage UpdateAwardMegaball(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            AwardMegaballBO awardMegaball = Algorithm.DecryptionObjectRSA<AwardMegaballBO>(objRsa.ParamRSAFirst);
            if (awardMegaball == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardMegaballBusiness -> UpdateAwardMegaball : Null Object  ";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_UpdateAwardMegaball";
                    SqlParameter[] pa = new SqlParameter[10];
                    pa[0] = new SqlParameter("@numberID", awardMegaball.NumberID);
                    pa[1] = new SqlParameter("@awardID", awardMegaball.AwardID);
                    pa[2] = new SqlParameter("@firstNumber", awardMegaball.FirstNumber);
                    pa[3] = new SqlParameter("@secondNumber", awardMegaball.SecondNumber);
                    pa[4] = new SqlParameter("@thirdNumber", awardMegaball.ThirdNumber);
                    pa[5] = new SqlParameter("@fourthNumber", awardMegaball.FourthNumber);
                    pa[6] = new SqlParameter("@fivethNumber", awardMegaball.FivethNumber);
                    pa[7] = new SqlParameter("@extraNumber", awardMegaball.ExtraNumber);
                    pa[8] = new SqlParameter("@GenCode", awardMegaball.GenCode);
                    pa[9] = new SqlParameter("@jackPotDefault", awardMegaball.JackPot);

                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardMegaballBusiness -> UpdateAwardMegaball : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }

            }
            return objResult;
        }

        public ObjResultMessage LockAndUnlockAwardMegaball(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            AwardMegaballBO awardMegaball = Algorithm.DecryptionObjectRSA<AwardMegaballBO>(objRsa.ParamRSAFirst);
            if (awardMegaball == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardMegaballBusiness -> LockAndUnlockAwardMegaball : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_LockAndUnlockAwardMegaball";
                    SqlParameter[] pa = new SqlParameter[2];

                    pa[0] = new SqlParameter("@numberID", awardMegaball.NumberID);
                    pa[1] = new SqlParameter("@isActive", awardMegaball.IsActive);

                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }

                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardMegaballBusiness -> LockAndUnlockAwardMegaball : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public IEnumerable<AwardMegaballBO> GetListAwardMegaball(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardMegaballBO> lstAwardMegaball = new List<AwardMegaballBO>();
                string sql = "SP_GetListAwardMagaball";
                SqlParameter[] pa = new SqlParameter[2];

                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardMegaballBO number = new AwardMegaballBO();

                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.DrawID = int.Parse(reader["DrawID"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    number.IsActive = int.Parse(reader["IsActive"].ToString());
                    number.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    number.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    number.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    number.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    number.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    number.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    number.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());

                    number.AwardNameEnglish = reader["AwardNameEnglish"].ToString();
                    number.JackPot = decimal.Parse(reader["JackPot"].ToString());
                    number.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    number.TotalWon = decimal.Parse(reader["TotalWon"].ToString());

                    lstAwardMegaball.Add(number);

                }
                return lstAwardMegaball;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<AwardMegaballBO> ListAllAwardMegaballBySearch(DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardMegaballBO> lstTransaction = new List<AwardMegaballBO>();
                string sql = "SP_ListAwardMegaballBySearch";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);

                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardMegaballBO awardMegaball = new AwardMegaballBO();

                    awardMegaball.NumberID = int.Parse(reader["NumberID"].ToString());
                    awardMegaball.AwardID = int.Parse(reader["AwardID"].ToString());
                    awardMegaball.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    awardMegaball.IsActive = int.Parse(reader["IsActive"].ToString());
                    awardMegaball.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    awardMegaball.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    awardMegaball.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    awardMegaball.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    awardMegaball.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    awardMegaball.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    awardMegaball.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());

                    awardMegaball.AwardNameEnglish = reader["AwardNameEnglish"].ToString();
                    awardMegaball.JackPot = decimal.Parse(reader["JackPot"].ToString());
                    awardMegaball.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(awardMegaball);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool CheckNumberExit(int num1, int num2, int num3, int num4, int num5, int extranum)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckNumberExit";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@num1", num1);
                pa[1] = new SqlParameter("@num2", num2);
                pa[2] = new SqlParameter("@num3", num3);
                pa[3] = new SqlParameter("@num4", num4);
                pa[4] = new SqlParameter("@num5", num5);
                pa[5] = new SqlParameter("@extranum", extranum);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<AwardMegaballBO> GetListAwardNumberByDate(int start, int end, DateTime fromDate, DateTime toDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardMegaballBO> lstTransaction = new List<AwardMegaballBO>();
                string sql = "SP_GetListAwardMegaballResultByDate";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardMegaballBO awardMegaball = new AwardMegaballBO();

                    awardMegaball.NumberID = int.Parse(reader["NumberID"].ToString());
                    awardMegaball.AwardID = int.Parse(reader["AwardID"].ToString());
                    awardMegaball.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    awardMegaball.IsActive = int.Parse(reader["IsActive"].ToString());
                    awardMegaball.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    awardMegaball.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    awardMegaball.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    awardMegaball.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    awardMegaball.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    awardMegaball.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    awardMegaball.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());

                    awardMegaball.AwardNameEnglish = reader["AwardNameEnglish"].ToString();
                    awardMegaball.JackPot = decimal.Parse(reader["JackPot"].ToString());
                    //awardMegaball.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    // awardMegaball.ListWinnerMember = GetListWinner(helper, awardMegaball.NumberID);


                    lstTransaction.Add(awardMegaball);
                }
                reader.Close();

                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public ObjResultMessage CountAllAwardMegaball(ModelRSA objRsa, ref AwardMegaballBO objAwardMegaball)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_CountAllAwardMegaball";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    objAwardMegaball = new AwardMegaballBO();
                    objAwardMegaball.TotalAllAward = int.Parse(reader["TotalAllAward"].ToString());
                    objAwardMegaball.TotalNewAward = int.Parse(reader["TotalNewAward"].ToString());
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardMegaballBusiness -> CountAllAwardMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utilitys.WriteLog(fileLog, ex.Message);
            }
            finally
            {
                helper.destroy();
            }

            return objResult;
        }

        public List<WinnerBO> GetListWinner(int intNumberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<WinnerBO> lstAward = new List<WinnerBO>();
                string sql = "SP_GetListWinner";
                SqlParameter[] pa = new SqlParameter[1];
                //pa[0] = new SqlParameter("@start", start);
                //pa[1] = new SqlParameter("@end", end);
                pa[0] = new SqlParameter("@numberID", intNumberID);
                //pa[3] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    WinnerBO number = new WinnerBO();
                    number.BookingID = int.Parse(reader["BookingID"].ToString());

                    if (!Convert.IsDBNull(reader["FirstNumber"]))
                        number.FirstNumber = int.Parse(reader["FirstNumber"].ToString());

                    if (!Convert.IsDBNull(reader["SecondNumber"]))
                        number.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    if (!Convert.IsDBNull(reader["ThirdNumber"]))
                        number.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    if (!Convert.IsDBNull(reader["FourthNumber"]))
                        number.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    if (!Convert.IsDBNull(reader["FivethNumber"]))
                        number.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    if (!Convert.IsDBNull(reader["ExtraNumber"]))
                        number.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    number.Email = reader["Email"].ToString();

                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        number.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    number.JackPot = float.Parse(reader["JACKPOTWINNER"].ToString());
                    number.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    //number.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    number.AwardNameEnglish = reader["AwardNameEnglish"].ToString();
                    lstAward.Add(number);
                }

                reader.Close();

                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool CheckCreateDateExists(DateTime createDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckOpenDateAwardMegaball";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@createDate", createDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception CheckCreateDateExists : " + ex.Message); return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public AwardMegaballBO GetAwardMegabalByDrawID(int intDrawID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            AwardMegaballBO objAwardMegaballBO = new AwardMegaballBO();
            try
            {
                string sql = "SP_GetAwardMegabalByDrawID";
                SqlParameter[] pa = new SqlParameter[1];

                pa[0] = new SqlParameter("@DrawID", intDrawID);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    objAwardMegaballBO.NumberID = int.Parse(reader["NumberID"].ToString());
                    objAwardMegaballBO.AwardID = int.Parse(reader["AwardID"].ToString());
                    objAwardMegaballBO.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    objAwardMegaballBO.IsActive = int.Parse(reader["IsActive"].ToString());
                    objAwardMegaballBO.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    objAwardMegaballBO.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    objAwardMegaballBO.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    objAwardMegaballBO.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    objAwardMegaballBO.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    objAwardMegaballBO.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    objAwardMegaballBO.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    objAwardMegaballBO.JackPot = decimal.Parse(reader["JackPot"].ToString());
                    //objAwardMegaballBO.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    objAwardMegaballBO.DrawID = int.Parse(reader["DrawID"].ToString());
                }
                return objAwardMegaballBO;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public ObjResultMessage UpdateJackpotAwardMegaball(decimal decJackpot, int intDrawID, decimal totalwon)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                string sql = "SP_UpdateJackpotAwardMegaball";
                SqlParameter[] pa = new SqlParameter[3];

                pa[0] = new SqlParameter("@jackpot", decJackpot);
                pa[1] = new SqlParameter("@draw", intDrawID);
                pa[2] = new SqlParameter("@totalwon", totalwon);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    objResult.IsError = false;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardMegaballBusiness -> UpdateJackpotAwardMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utilitys.WriteLog(fileLog, ex.Message);

            }
            finally
            {
                helper.destroy();
            }
            return objResult;
        }

        public ObjResultMessage UpdateTotalWonAwardMegaball(Sqlhelper helper, int intDrawID, decimal totalwon)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                string sql = "SP_AwardMegaball_Upd";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@draw", intDrawID);
                pa[1] = new SqlParameter("@totalwon", totalwon);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    objResult.IsError = false;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardMegaballBusiness -> UpdateTotalWonAwardMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utilitys.WriteLog(fileLog, ex.Message);

                throw;
            }
            return objResult;
        }

        public ObjResultMessage InsertWinningNumber(TicketWinningBO objTicket, DateTime dtmCreatedDate, ref int intWinnerJackPotCount)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage(true, MessageResult.Error, "", DateTime.Now);
            BBC.CWallet.Share.BBCLoggerManager.Info("services.InsertListBooking: ");
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                //get list winning number in bookingmegaball
                List<TicketWinningBO> lstTicketWinning = GetListWinningNumber(helper, objTicket, dtmCreatedDate);
                if (lstTicketWinning != null && lstTicketWinning.Count > 0)
                {
                    lstTicketWinning = lstTicketWinning.OrderBy(c => c.Email).ToList();

                    int intPriorityConfig = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PriorityJackpot]);
                    var lstWinningJackpot = lstTicketWinning.Where(c => c.Priority == intPriorityConfig).ToList();
                    if (lstWinningJackpot != null)
                    {
                        //get total ticket winning JackPot
                        intWinnerJackPotCount = lstWinningJackpot.Count;
                    }

                    decimal dcJackpot = (intWinnerJackPotCount > 0 ? (objTicket.JackpotValue / intWinnerJackPotCount) : 0); //jackpot / total ticket winning JackPot

                    List<TicketWinningOut> lstOutput = new List<TicketWinningOut>();

                    foreach (TicketWinningBO obj in lstTicketWinning)
                    {
                        //insert table TicketWinning
                        if (obj.Priority == intPriorityConfig)
                        {
                            //If TicketWinning is JackPot
                            obj.AwardValue = dcJackpot;
                            obj.AwardFee = (intWinnerJackPotCount > 0 ? (obj.AwardFee / intWinnerJackPotCount) : 0);//Fee / total ticket winning Jackpot
                        }

                        int intTicketWinningID = InsertTicketWinning(helper, obj);
                        if (intTicketWinningID > 0)
                        {
                            //success
                            //if lstOutput don't have a record with this email
                            if (lstOutput.FirstOrDefault(c => c.email == obj.Email) == null)
                            {
                                decimal dcAwardValue = lstTicketWinning.Where(c => c.Email == obj.Email).Sum(c => (c.Priority == intPriorityConfig ? dcJackpot : c.AwardValue));
                                //Insert Table AwardMegaball to write log for BOS API
                                intTicketWinningID = InsertAwardMegaballLog(helper, obj.Email, dcAwardValue, dtmCreatedDate);
                                if (intTicketWinningID > 0)
                                {
                                    //success -> Add to List Output
                                    lstOutput.Add(new TicketWinningOut()
                                    {
                                        id = intTicketWinningID,
                                        email = obj.Email,
                                        value = dcAwardValue.ToString(),
                                        date_time = dtmCreatedDate.ToString("yyyy/MM/dd HH:mm:ss")
                                    }
                                                 );
                                }
                            }
                        }
                    }

                    if (lstOutput != null && lstOutput.Count > 0)
                    {
                        decimal totalwon = lstOutput.Sum(c => Convert.ToDecimal(c.value));

                        UpdateTotalWonAwardMegaball(helper, objTicket.DrawID, totalwon);

                        //success
                        objResult.IsError = false;
                        objResult.Result = MessageResult.Success;
                        objResult.MessageDetail = "Success";
                        objResult.ObjResultData = JsonConvert.SerializeObject(lstOutput);
                    }
                    else
                    {
                        objResult.MessageDetail = "Exists";
                    }
                }
            }
            catch (Exception ex)
            {
                objResult.MessageDetail = "AwardMegaballBusiness -> InsertWinningNumber: " + ex.Message;

                Utilitys.WriteLog(fileLog, "AwardMegaballBusiness -> InsertWinningNumber: " + ex.Message);
            }
            finally
            {
                helper.destroy();
            }

            return objResult;
        }

        public List<TicketWinningBO> GetListWinningNumber(Sqlhelper helper, TicketWinningBO objTicket, DateTime dtmCreatedDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            List<TicketWinningBO> lstTicketWinning = null;
            try
            {
                string sql = "SP_BookingMegaball_AwardValue_Sel";

                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@DrawID", objTicket.DrawID);
                pa[1] = new SqlParameter("@FirstNumber", objTicket.FirstNumberAward);
                pa[2] = new SqlParameter("@SecondNumber", objTicket.SecondNumberAward);
                pa[3] = new SqlParameter("@ThirdNumber", objTicket.ThirdNumberAward);
                pa[4] = new SqlParameter("@FourthNumber", objTicket.FourthNumberAward);
                pa[5] = new SqlParameter("@FivethNumber", objTicket.FivethNumberAward);
                pa[6] = new SqlParameter("@ExtraNumber", objTicket.ExtraNumberAward);
                pa[7] = new SqlParameter("@JackPotValue", objTicket.JackpotValue);

                SqlCommand command = helper.GetCommand(sql, pa, true);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (lstTicketWinning == null)
                        lstTicketWinning = new List<TicketWinningBO>();

                    TicketWinningBO objWinning = new TicketWinningBO();

                    if (!Convert.IsDBNull(reader["MemberID"])) objWinning.MemberID = Convert.ToInt32(reader["MemberID"]);
                    if (!Convert.IsDBNull(reader["BookingID"])) objWinning.BookingID = Convert.ToInt32(reader["BookingID"]);
                    if (!Convert.IsDBNull(reader["DrawID"])) objWinning.DrawID = Convert.ToInt32(reader["DrawID"]);
                    if (!Convert.IsDBNull(reader["BuyDate"])) objWinning.BuyDate = Convert.ToDateTime(reader["BuyDate"]);
                    if (!Convert.IsDBNull(reader["Email"])) objWinning.Email = Convert.ToString(reader["Email"]);
                    if (!Convert.IsDBNull(reader["AwardValue"])) objWinning.AwardValue = Convert.ToDecimal(reader["AwardValue"]);
                    if (!Convert.IsDBNull(reader["AwardID"])) objWinning.AwardID = Convert.ToInt32(reader["AwardID"]);
                    if (!Convert.IsDBNull(reader["NumberBallNormal"])) objWinning.NumberBallNormal = Convert.ToInt32(reader["NumberBallNormal"]);
                    if (!Convert.IsDBNull(reader["NumberBallGold"])) objWinning.NumberBallGold = Convert.ToInt32(reader["NumberBallGold"]);
                    if (!Convert.IsDBNull(reader["Priority"])) objWinning.Priority = Convert.ToInt32(reader["Priority"]);
                    if (!Convert.IsDBNull(reader["AwardFee"])) objWinning.AwardFee = Convert.ToDecimal(reader["AwardFee"]);

                    if (!Convert.IsDBNull(reader["FirstNumberBuy"])) objWinning.FirstNumberBuy = Convert.ToInt32(reader["FirstNumberBuy"]);
                    if (!Convert.IsDBNull(reader["SecondNumberBuy"])) objWinning.SecondNumberBuy = Convert.ToInt32(reader["SecondNumberBuy"]);
                    if (!Convert.IsDBNull(reader["ThirdNumberBuy"])) objWinning.ThirdNumberBuy = Convert.ToInt32(reader["ThirdNumberBuy"]);
                    if (!Convert.IsDBNull(reader["FourthNumberBuy"])) objWinning.FourthNumberBuy = Convert.ToInt32(reader["FourthNumberBuy"]);
                    if (!Convert.IsDBNull(reader["FivethNumberBuy"])) objWinning.FivethNumberBuy = Convert.ToInt32(reader["FivethNumberBuy"]);
                    if (!Convert.IsDBNull(reader["ExtraNumberBuy"])) objWinning.ExtraNumberBuy = Convert.ToInt32(reader["ExtraNumberBuy"]);

                    objWinning.FirstNumberAward = objTicket.FirstNumberAward;
                    objWinning.SecondNumberAward = objTicket.SecondNumberAward;
                    objWinning.ThirdNumberAward = objTicket.ThirdNumberAward;
                    objWinning.FourthNumberAward = objTicket.FourthNumberAward;
                    objWinning.FivethNumberAward = objTicket.FivethNumberAward;
                    objWinning.ExtraNumberAward = objTicket.ExtraNumberAward;
                    objWinning.AwardDate = objTicket.AwardDate;

                    lstTicketWinning.Add(objWinning);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                Utilitys.WriteLog(fileLog, "AwardMegaballBusiness -> GetListWinningNumber: " + objEx.Message);
            }

            return lstTicketWinning;
        }

        public int InsertTicketWinning(Sqlhelper helper, TicketWinningBO objTicket)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));

            int intResultID = 0;
            try
            {
                List<AwardBO> lstAward = new List<AwardBO>();
                string sql = "SP_TicketWinning_Add";

                SqlParameter[] pa = new SqlParameter[24];
                pa[0] = new SqlParameter("@BookingID", objTicket.BookingID);
                pa[1] = new SqlParameter("@DrawID", objTicket.DrawID);
                pa[2] = new SqlParameter("@BuyDate", objTicket.BuyDate);
                pa[3] = new SqlParameter("@AwardID", objTicket.AwardID);
                pa[4] = new SqlParameter("@FirstNumberBuy", objTicket.FirstNumberBuy);
                pa[5] = new SqlParameter("@SecondNumberBuy", objTicket.SecondNumberBuy);
                pa[6] = new SqlParameter("@ThirdNumberBuy", objTicket.ThirdNumberBuy);
                pa[7] = new SqlParameter("@FourthNumberBuy", objTicket.FourthNumberBuy);
                pa[8] = new SqlParameter("@FivethNumberBuy", objTicket.FivethNumberBuy);
                pa[9] = new SqlParameter("@ExtraNumberBuy", objTicket.ExtraNumberBuy);
                pa[10] = new SqlParameter("@FirstNumberAward", objTicket.FirstNumberAward);
                pa[11] = new SqlParameter("@SecondNumberAward", objTicket.SecondNumberAward);
                pa[12] = new SqlParameter("@ThirdNumberAward", objTicket.ThirdNumberAward);
                pa[13] = new SqlParameter("@FourthNumberAward", objTicket.FourthNumberAward);
                pa[14] = new SqlParameter("@FivethNumberAward", objTicket.FivethNumberAward);
                pa[15] = new SqlParameter("@ExtraNumberAward", objTicket.ExtraNumberAward);
                pa[16] = new SqlParameter("@NumberBallNormal", objTicket.NumberBallNormal);
                pa[17] = new SqlParameter("@NumberBallGold", objTicket.NumberBallGold);
                pa[18] = new SqlParameter("@AwardValue", objTicket.AwardValue);
                pa[19] = new SqlParameter("@AwardDate", objTicket.AwardDate);
                pa[20] = new SqlParameter("@MemberID", objTicket.MemberID);
                pa[21] = new SqlParameter("@Priority", objTicket.Priority);
                pa[22] = new SqlParameter("@AwardFee", objTicket.AwardFee);

                SqlParameter paraId = new SqlParameter();
                paraId.ParameterName = "@TicketWinningID";
                paraId.DbType = DbType.Int32;
                paraId.Direction = ParameterDirection.Output;
                pa[23] = paraId;

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int intQuery = command.ExecuteNonQuery();

                var objOutput = command.Parameters["@TicketWinningID"];
                if (intQuery > 0 && (objOutput != null && objOutput.Value != null))
                {
                    intResultID = Convert.ToInt32(objOutput.Value);
                }
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "AwardMegaballBusiness -> InsertTicketWinning: " + ex.Message);
                throw;
            }

            return intResultID;
        }

        public int InsertAwardMegaballLog(Sqlhelper helper, string strEmail, decimal dcValue, DateTime dtmCreatedDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));

            int intResultID = 0;
            try
            {
                List<AwardBO> lstAward = new List<AwardBO>();
                string sql = "SP_AwardMegaballLog_Add";

                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@Email", strEmail);
                pa[1] = new SqlParameter("@Value", dcValue);
                pa[2] = new SqlParameter("@CreatedDate", dtmCreatedDate);

                SqlParameter paraId = new SqlParameter();
                paraId.ParameterName = "@Id";
                paraId.DbType = DbType.Int32;
                paraId.Direction = ParameterDirection.Output;
                pa[3] = paraId;

                SqlCommand command = helper.GetCommand(sql, pa, true);

                int intQuery = command.ExecuteNonQuery();

                var objOutput = command.Parameters["@Id"];
                if (intQuery > 0 && (objOutput != null && objOutput.Value != null))
                {
                    intResultID = Convert.ToInt32(objOutput.Value);
                }
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "AwardMegaballBusiness -> InsertAwardMegaballLog: " + ex.Message);
                throw;
            }

            return intResultID;
        }

        public int UpdateAwardValue(decimal dcJackPotValue, int intDrawID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            int intResultID = 0;
            try
            {
                List<AwardBO> lstAward = new List<AwardBO>();
                string sql = "SP_Award_JackPotValue_Upd";

                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@JackPotValue", dcJackPotValue);
                pa[1] = new SqlParameter("@DrawID", intDrawID);

                SqlCommand command = helper.GetCommand(sql, pa, true);

                intResultID = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "AwardMegaballBusiness -> InsertAwardMegaballLog: " + ex.Message);
                throw;
            }

            return intResultID;
        }

        public int CheckJackPotWinnerByDrawID(TicketWinningBO objTicket)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            int intWinnerJack = 0;
            try
            {
                string sql = "SP_CheckJackPotWinner_Draw_Sel";

                SqlParameter[] pa = new SqlParameter[7];
                pa[0] = new SqlParameter("@DrawID", objTicket.DrawID);
                pa[1] = new SqlParameter("@FirstNumber", objTicket.FirstNumberAward);
                pa[2] = new SqlParameter("@SecondNumber", objTicket.SecondNumberAward);
                pa[3] = new SqlParameter("@ThirdNumber", objTicket.ThirdNumberAward);
                pa[4] = new SqlParameter("@FourthNumber", objTicket.FourthNumberAward);
                pa[5] = new SqlParameter("@FivethNumber", objTicket.FivethNumberAward);
                pa[6] = new SqlParameter("@ExtraNumber", objTicket.ExtraNumberAward);

                SqlCommand command = helper.GetCommand(sql, pa, true);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                   
                    if (!Convert.IsDBNull(reader["JackPotWinner"])) intWinnerJack = Convert.ToInt32(reader["JackPotWinner"]);
                }
                reader.Close();
            }
            catch (Exception objEx)
            {
                Utilitys.WriteLog(fileLog, "AwardMegaballBusiness -> CheckJackPotWinnerByDrawID: " + objEx.Message);
            }

            return intWinnerJack;
        }
    }
}
