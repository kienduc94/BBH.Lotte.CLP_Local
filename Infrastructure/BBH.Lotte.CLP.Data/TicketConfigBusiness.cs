﻿using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain.ObjDomain;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Data
{

    public class TicketConfigBusiness : ITicketConfigServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));

        public ObjResultMessage InsertTicketConfig(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();

            TicketConfigBO objTicketConfig = Algorithm.DecryptionObjectRSA<TicketConfigBO>(objRsa.ParamRSAFirst);
            if (objTicketConfig == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "TicketConfigBusiness -> InsertTicketConfig : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_InsertTicketConfig_CMS";
                    SqlParameter[] pa = new SqlParameter[8];
                    pa[0] = new SqlParameter("@ConfigName", objTicketConfig.ConfigName);
                    pa[1] = new SqlParameter("@NumberTicket", objTicketConfig.NumberTicket);
                    pa[2] = new SqlParameter("@CoinValues", objTicketConfig.CoinValues);
                    pa[3] = new SqlParameter("@CoinID", objTicketConfig.CoinID);
                    pa[4] = new SqlParameter("@IsActive", objTicketConfig.IsActive);
                    pa[5] = new SqlParameter("@CreatedUser", objTicketConfig.CreatedUser);
                    pa[6] = new SqlParameter("@CreatedDate", objTicketConfig.CreatedDate);
                    pa[7] = new SqlParameter("@IsDeleted", objTicketConfig.IsDeleted);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                    else
                    {
                        objResult.IsError = true;
                    }

                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "TicketConfigBusiness -> InsertTicketConfig : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utility.WriteLog(pathLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public ObjResultMessage UpdateTicketConfig(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();

            TicketConfigBO objTicketConfig = Algorithm.DecryptionObjectRSA<TicketConfigBO>(objRsa.ParamRSAFirst);
            if (objTicketConfig == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "TicketConfigBusiness -> UpdateTicketConfig : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_UpdateTicketConfig_CMS";
                    SqlParameter[] pa = new SqlParameter[6];
                    pa[0] = new SqlParameter("@ConfigName", objTicketConfig.ConfigName);
                    pa[1] = new SqlParameter("@NumberTicket", objTicketConfig.NumberTicket);
                    pa[2] = new SqlParameter("@CoinValues", objTicketConfig.CoinValues);
                    pa[3] = new SqlParameter("@CoinID", objTicketConfig.CoinID);
                    pa[4] = new SqlParameter("@ConfigID", objTicketConfig.ConfigID);
                    pa[5] = new SqlParameter("@UpdateDate", objTicketConfig.UpdatedDate);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                    else
                    {
                        objResult.IsError = true;
                    }

                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "TicketConfigBusiness -> UpdateTicketConfig : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utility.WriteLog(pathLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public ObjResultMessage DeleteTicketConfig(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();

            TicketConfigBO objTicketConfig = Algorithm.DecryptionObjectRSA<TicketConfigBO>(objRsa.ParamRSAFirst);
            if (objTicketConfig == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "TicketConfigBusiness -> DeleteTicketConfig : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_LockAndUnlockTicketConfig_CMS";
                    SqlParameter[] pa = new SqlParameter[2];

                    pa[0] = new SqlParameter("@ConfigID", objTicketConfig.ConfigID);
                    pa[1] = new SqlParameter("@DeletedUser", objTicketConfig.DeletedUser);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                    else
                    {
                        objResult.IsError = true;
                    }

                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "TicketConfigBusiness -> DeleteTicketConfig : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utility.WriteLog(pathLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }


        public IEnumerable<TicketConfigBO> ListTicketConfig()
        {

            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            List<TicketConfigBO> lstTicket = new List<TicketConfigBO>();

            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_ListTicketConfig";
                
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TicketConfigBO objTicketConfig = new TicketConfigBO();
                    objTicketConfig.ConfigID = int.Parse(reader["ConfigID"].ToString());
                    objTicketConfig.ConfigName = reader["ConfigName"].ToString();
                    objTicketConfig.CoinValues = decimal.Parse(reader["CoinValues"].ToString());

                    objTicketConfig.CoinID = reader["CoinID"].ToString();
                    objTicketConfig.IsActive = int.Parse(reader["IsActive"].ToString());
                    objTicketConfig.NumberTicket = int.Parse(reader["NumberTicket"].ToString());
                    objTicketConfig.IsDeleted = int.Parse(reader["IsDeleted"].ToString());
                    objTicketConfig.CreatedUser = reader["CreatedUser"].ToString();
                    objTicketConfig.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                    lstTicket.Add(objTicketConfig);
                }
                return lstTicket;

            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }


        }

        public ObjResultMessage GetTicketConfigDetail(ModelRSA objRsa, ref TicketConfigBO objTicketConfig)
        {

            ObjResultMessage objResult = new ObjResultMessage();
            TicketConfigBO objTicketConfigPara = Algorithm.DecryptionObjectRSA<TicketConfigBO>(objRsa.ParamRSAFirst);
            if (objTicketConfigPara == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "TicketConfigBusiness -> GetTicketConfigDetail : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_GetTicketConfigDetail_CMS";
                    SqlParameter[] pa = new SqlParameter[1];
                    pa[0] = new SqlParameter("@TicketConfigID", objTicketConfigPara.ConfigID);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        objTicketConfig = new TicketConfigBO();
                        objTicketConfig.ConfigID = int.Parse(reader["ConfigID"].ToString());
                        objTicketConfig.ConfigName = reader["ConfigName"].ToString();
                        objTicketConfig.CoinValues = decimal.Parse(reader["CoinValues"].ToString());
                        objTicketConfig.CoinID = reader["CoinID"].ToString();
                        objTicketConfig.NumberTicket = int.Parse(reader["NumberTicket"].ToString());
                        objTicketConfig.IsDeleted = int.Parse(reader["IsDeleted"].ToString());
                        objTicketConfig.CreatedUser = reader["CreatedUser"].ToString();
                        objTicketConfig.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                    }
                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "TicketConfigBusiness -> GetTicketConfigDetail : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utility.WriteLog(pathLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }

            return objResult;
        }
        public ObjResultMessage LockAndUnlockConfigTicket(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            TicketConfigBO objConfigTicket = Algorithm.DecryptionObjectRSA<TicketConfigBO>(objRsa.ParamRSAFirst);
            if (objConfigTicket == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "ConfigTicket -> LockAndUnlockConfigTicket : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_LockAndLockConfigTicket_CMS";
                    SqlParameter[] pa = new SqlParameter[2];
                    pa[0] = new SqlParameter("@isActive", objConfigTicket.IsActive);
                    pa[1] = new SqlParameter("@configID", objConfigTicket.ConfigID);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "ConfigTicket -> LockAndUnlockConfigTicket : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public bool CheckCoinID(int configID, string coinID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckCoinID";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@coinID", coinID);
                pa[1] = new SqlParameter("@configID", configID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception CheckConfigName : " + ex.Message); return false;
            }
            finally
            {
                helper.destroy();
            }
        }

    }
}
