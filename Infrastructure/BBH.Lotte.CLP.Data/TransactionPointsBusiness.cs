﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Data
{
    public class TransactionPointsBusiness : ITransactionPointsServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public IEnumerable<TransactionPointsBO> ListTransactionPoint()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPointsBO> lstTransaction = new List<TransactionPointsBO>();
                string sql = "SP_ListTransactionPoint";
                SqlCommand command = helper.GetCommandNonParameter(sql,true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPointsBO transaction = new TransactionPointsBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.Points = double.Parse(reader["Points"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<TransactionPointsBO> ListTransactionPointPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPointsBO> lstTransaction = new List<TransactionPointsBO>();
                string sql = "SP_ListTransactionPointPage";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPointsBO transaction = new TransactionPointsBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.Points = double.Parse(reader["Points"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<TransactionPointsBO> ListTransactionPointBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int status, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPointsBO> lstTransaction = new List<TransactionPointsBO>();
                string sql = "SP_ListTransactionPointBySearchCMS";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@memberID", memberID);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                pa[5] = new SqlParameter("@status", status);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPointsBO transaction = new TransactionPointsBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.Points = double.Parse(reader["Points"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        transaction.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateStatusTransaction(int transactionID, int status)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_ListTransactionPointStatus";
                SqlParameter[] pa = new SqlParameter[2];
                
                pa[0] = new SqlParameter("@transactionID", transactionID);
                pa[1] = new SqlParameter("@status", status);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        public TransactionPointsBO CountAllPoints()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                TransactionPointsBO package = null;
                string sql = "CountAllPoints";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    package = new TransactionPointsBO();
                    package.TotalAllPoint = int.Parse(reader["TotalAllPoints"].ToString());
                    package.TotalNewPoint = int.Parse(reader["TotalNewPoints"].ToString());
                }
                return package;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

    }
}
