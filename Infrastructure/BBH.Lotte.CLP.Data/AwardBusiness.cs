﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBH.Lotte.CLP.Domain;
using BBC.Core.Database;
using System.Configuration;
using System.IO;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using BBC.Core.Common.Utils;

namespace BBH.Lotte.CLP.Data
{
    public class AwardBusiness : IAwardServices
    {

        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public ObjResultMessage InsertAward(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            AwardBO objAward = Algorithm.DecryptionObjectRSA<AwardBO>(objRsa.ParamRSAFirst);
            if (objAward == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardBusiness -> InsertAward : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_InsertAward";
                    SqlParameter[] pa = new SqlParameter[8];
                    //pa[0] = new SqlParameter("@awardName", award.AwardName);
                    pa[0] = new SqlParameter("@isActive", objAward.IsActive);
                    pa[1] = new SqlParameter("@isDelete", objAward.IsDelete);
                    pa[2] = new SqlParameter("@awardValue", objAward.AwardValue);
                    pa[3] = new SqlParameter("@createDate", objAward.CreateDate);
                    pa[4] = new SqlParameter("@priority", objAward.Priority);
                    pa[5] = new SqlParameter("@awardNameEnglish", objAward.AwardNameEnglish);
                    pa[6] = new SqlParameter("@awardFee", objAward.AwardFee);
                    pa[7] = new SqlParameter("@AwardPercent", objAward.AwardPercent);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardBusiness -> InsertAward : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public ObjResultMessage UpdateAward(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            AwardBO objAward = Algorithm.DecryptionObjectRSA<AwardBO>(objRsa.ParamRSAFirst);
            if (objAward == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardBusiness -> UpdateAward : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_UpdateAward";
                    SqlParameter[] pa = new SqlParameter[5];
                    //pa[0] = new SqlParameter("@awardName", award.AwardName);
                    //pa[0] = new SqlParameter("@awardValue", award.AwardValue);
                    pa[0] = new SqlParameter("@awardID", objAward.AwardID);
                    pa[1] = new SqlParameter("@awardNameEnglish", objAward.AwardNameEnglish);
                    pa[2] = new SqlParameter("@awardValue", objAward.AwardValue);
                    pa[3] = new SqlParameter("@awardFee", (objAward.AwardFee.ToString().Length > 10 ? objAward.AwardFee.ToString().Substring(0, 10) : objAward.AwardFee.ToString()));
                    pa[4] = new SqlParameter("@AwardPercent", objAward.AwardPercent);


                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardBusiness -> UpdateAward : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public ObjResultMessage LockAndUnlockAward(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            AwardBO objAward = Algorithm.DecryptionObjectRSA<AwardBO>(objRsa.ParamRSAFirst);
            if (objAward == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardBusiness -> LockAndUnlockAward : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_LockAndUnlockAward";
                    SqlParameter[] pa = new SqlParameter[2];
                    pa[0] = new SqlParameter("@isActive", objAward.IsActive);
                    pa[1] = new SqlParameter("@awardID", objAward.AwardID);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardBusiness -> LockAndUnlockAward : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

        public IEnumerable<AwardBO> GetListAward(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardBO> lstAward = new List<AwardBO>();
                string sql = "SP_GetListAward";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardBO number = new AwardBO();
                    if (!string.IsNullOrEmpty(reader["AwardID"].ToString())) number.AwardID = int.Parse(reader["AwardID"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardName"].ToString())) number.AwardName = reader["AwardName"].ToString();


                    if (!string.IsNullOrEmpty(reader["AwardNameEnglish"].ToString())) number.AwardNameEnglish = reader["AwardNameEnglish"].ToString();

                    if (!string.IsNullOrEmpty(reader["IsActive"].ToString())) number.IsActive = int.Parse(reader["IsActive"].ToString());

                    if (!string.IsNullOrEmpty(reader["AwardValue"].ToString())) number.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    if (!string.IsNullOrEmpty(reader["Priority"].ToString())) number.Priority = int.Parse(reader["Priority"].ToString());
                    if (!string.IsNullOrEmpty(reader["CreateDate"].ToString())) number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["TOTALROWS"].ToString())) number.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardPercent"].ToString())) number.AwardPercent = float.Parse(reader["AwardPercent"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardFee"].ToString())) number.AwardFee = decimal.Parse(reader["AwardFee"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardFee"].ToString())) number.AwardFee = decimal.Parse(reader["AwardFee"].ToString());

                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public AwardBO GetListAwardDetail(int awardID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                AwardBO number = new AwardBO();
                string sql = "SP_GetListAwardDetail_CMS";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@awardID", awardID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!string.IsNullOrEmpty(reader["AwardID"].ToString())) number.AwardID = int.Parse(reader["AwardID"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardName"].ToString())) number.AwardName = reader["AwardName"].ToString();
                    if (!string.IsNullOrEmpty(reader["AwardNameEnglish"].ToString())) number.AwardNameEnglish = reader["AwardNameEnglish"].ToString();
                    if (!string.IsNullOrEmpty(reader["IsActive"].ToString())) number.IsActive = int.Parse(reader["IsActive"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardValue"].ToString())) number.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    if (!string.IsNullOrEmpty(reader["Priority"].ToString())) number.Priority = int.Parse(reader["Priority"].ToString());
                    if (!string.IsNullOrEmpty(reader["CreateDate"].ToString())) number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardPercent"].ToString())) number.AwardPercent = float.Parse(reader["AwardPercent"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardFee"].ToString())) number.AwardFee = decimal.Parse(reader["AwardFee"].ToString());
                }
                return number;
             }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool CheckAwardNameEnglishExists(int awardID, string awardNameEnglish)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckAwardNameEnglish";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@awardNameEnglish", awardNameEnglish);
                pa[1] = new SqlParameter("@AwardID", awardID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception CheckAwardNameEnglish : " + ex.Message); return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<AwardNumberBO> GetListAwardNumber(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardNumberBO> lstAward = new List<AwardNumberBO>();
                string sql = "SP_GetListAwardNumber";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardNumberBO number = new AwardNumberBO();
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.AwardName = reader["AwardName"].ToString();
                    if (!string.IsNullOrEmpty(reader["AwardNameEnglish"].ToString()))
                    {
                        number.AwardNameEnglish = reader["AwardNameEnglish"].ToString();
                    }
                    number.IsActive = int.Parse(reader["IsActive"].ToString());


                    number.NumberValue = reader["NumberValue"].ToString();
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool InsertAwardNumber(AwardNumberBO award)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_InsertAwardNumber";
                SqlParameter[] pa = new SqlParameter[8];

                pa[0] = new SqlParameter("@isActive", award.IsActive);
                pa[1] = new SqlParameter("@isDelete", award.IsDelete);
                pa[2] = new SqlParameter("@awardID", award.AwardID);
                pa[3] = new SqlParameter("@createDate", award.CreateDate);
                pa[4] = new SqlParameter("@priority", award.Priority);
                pa[5] = new SqlParameter("@numberValue", award.NumberValue);
                pa[6] = new SqlParameter("@stationName", award.StationName);
                pa[7] = new SqlParameter("@stationNameEnglish", award.StationNameEnglish);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateAwardNumber(AwardNumberBO award)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateAwardNumber";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@numberID", award.NumberID);
                pa[1] = new SqlParameter("@awardID", award.AwardID);
                pa[2] = new SqlParameter("@numberValue", award.NumberValue);
                pa[3] = new SqlParameter("@stationName", award.StationName);
                pa[4] = new SqlParameter("@stationNameEnglish", award.StationNameEnglish);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public ObjResultMessage LockAndUnlockAwardNumber(ModelRSA objRsa)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            ObjResultMessage objResult = new ObjResultMessage();
            AwardMegaballBO objAward = Algorithm.DecryptionObjectRSA<AwardMegaballBO>(objRsa.ParamRSAFirst);
            if (objAward == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardBusiness -> LockAndUnlockAwardNumber : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_LockAndUnlockAwardNumber";
                    SqlParameter[] pa = new SqlParameter[2];
                    pa[0] = new SqlParameter("@numberID", objAward.NumberID);
                    pa[1] = new SqlParameter("@isActive", objAward.IsActive);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }

                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardBusiness -> LockAndUnlockAwardNumber : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    Utilitys.WriteLog(fileLog, ex.Message);

                }
                finally
                {
                    helper.destroy();
                }
            }

            return objResult;

        }

        public IEnumerable<AwardNumberBO> GetListAwardNumberByDate(int start, int end, DateTime fromDate, DateTime toDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardNumberBO> lstAward = new List<AwardNumberBO>();
                string sql = "SP_GetListAwardNumberByDate";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardNumberBO number = new AwardNumberBO();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.NumberValue = reader["NumberValue"].ToString();
                    number.AwardName = reader["AwardName"].ToString();
                    if (!string.IsNullOrEmpty(reader["AwardNameEnglish"].ToString()))
                    {
                        number.AwardNameEnglish = reader["AwardNameEnglish"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["StationNameEnglish"].ToString()))
                    {
                        number.StationNameEnglish = reader["StationNameEnglish"].ToString();
                    }
                    number.StationName = reader["StationName"].ToString();
                    number.IsActive = int.Parse(reader["IsActive"].ToString());
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<WinnerBO> GetListWinner(int start, int end, DateTime fromDate, DateTime toDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<WinnerBO> lstAward = new List<WinnerBO>();
                string sql = "SP_GetListWinner";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    WinnerBO number = new WinnerBO();
                    number.BookingID = int.Parse(reader["BookingID"].ToString());
                    number.MemberID = int.Parse(reader["MemberID"].ToString());
                    if (!Convert.IsDBNull(reader["FirstNumber"]))
                        number.FirstNumber = int.Parse(reader["FirstNumber"].ToString());

                    if (!Convert.IsDBNull(reader["SecondNumber"]))
                        number.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    if (!Convert.IsDBNull(reader["ThirdNumber"]))
                        number.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    if (!Convert.IsDBNull(reader["FourthNumber"]))
                        number.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    if (!Convert.IsDBNull(reader["FivethNumber"]))
                        number.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    if (!Convert.IsDBNull(reader["ExtraNumber"]))
                        number.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    number.Email = reader["Email"].ToString();

                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        number.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    number.OpenDate = DateTime.Parse(reader["CreateDate"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
