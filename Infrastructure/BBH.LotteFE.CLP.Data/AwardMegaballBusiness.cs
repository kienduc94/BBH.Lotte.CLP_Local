﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Data
{
    public class AwardMegaballBusiness : IAwardMegaballServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public IEnumerable<AwardMegaball> GetListAward(int start, int end, DateTime fromDate, DateTime toDate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardMegaball> lstAward = new List<AwardMegaball>();
                string sql = "SP_GetListAwardMegaballFE";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardMegaball number = new AwardMegaball();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                 
                    number.AwardName = reader["AwardName"].ToString();
                    number.AwardValue = double.Parse(reader["AwardValue"].ToString());
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());

                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());

                    number.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    number.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    number.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    number.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    number.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    number.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    number.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    number.TotalWon = decimal.Parse(reader["TotalWon"].ToString());
                    number.JackPot = decimal.Parse(reader["JackPot"].ToString());
                    number.DrawID = int.Parse(reader["DrawID"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<AwardMegaball> GetAllListAward()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardMegaball> lstAward = new List<AwardMegaball>();
                string sql = "SP_GetListAwardMegaball_FE";
                
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardMegaball number = new AwardMegaball();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());

                    //number.AwardName = reader["AwardName"].ToString();
                    //number.AwardValue = double.Parse(reader["AwardValue"].ToString());
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());

                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());

                    number.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    number.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    number.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    number.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    number.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    number.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    number.TotalWon = decimal.Parse(reader["TotalWon"].ToString());
                    number.JackPot = decimal.Parse(reader["JackPot"].ToString());
                    number.DrawID = int.Parse(reader["DrawID"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        /// <summary>
        /// Created date: 15.01.2018
        /// Description: Load thông tin xổ số gần nhất
        /// </summary>
        /// <returns></returns>
        public AwardMegaball GetLotteryResult(int intMemberID, ref double dblPoint)
        {
            Sqlhelper objHelper = new Sqlhelper("", "ConnectionString");
            try
            {
                AwardMegaball objAward = new AwardMegaball();
                string strQuery = "SP_AWARDMEGABALL_SELRESULT";
                SqlCommand objCommand = objHelper.GetCommandNonParameter(strQuery, true);
                SqlDataReader reader = objCommand.ExecuteReader();
                if (reader.Read())
                {
                    if(!Convert.IsDBNull(reader["AwardID"]))
                        objAward.AwardID = int.Parse(reader["AwardID"].ToString());

                    if (!Convert.IsDBNull(reader["AwardName"]))
                        objAward.AwardName = reader["AwardName"].ToString().Trim();

                    if (!Convert.IsDBNull(reader["AwardValue"]))
                        objAward.AwardValue = double.Parse(reader["AwardValue"].ToString());

                    if (!Convert.IsDBNull(reader["NumberID"]))
                        objAward.NumberID = int.Parse(reader["NumberID"].ToString());

                    if (!Convert.IsDBNull(reader["Priority"]))
                        objAward.Priority = int.Parse(reader["Priority"].ToString());

                    if (!Convert.IsDBNull(reader["CreateDate"]))
                        objAward.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());

                    if (!Convert.IsDBNull(reader["FirstNumber"]))
                        objAward.FirstNumber = int.Parse(reader["FirstNumber"].ToString());

                    if (!Convert.IsDBNull(reader["SecondNumber"]))
                        objAward.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    if (!Convert.IsDBNull(reader["ThirdNumber"]))
                        objAward.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    if (!Convert.IsDBNull(reader["FourthNumber"]))
                        objAward.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    if (!Convert.IsDBNull(reader["FivethNumber"]))
                        objAward.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    if (!Convert.IsDBNull(reader["ExtraNumber"]))
                        objAward.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());

                    if (!Convert.IsDBNull(reader["JackPot"]))
                        objAward.JackPot = decimal.Parse(reader["JackPot"].ToString());

                }

                reader.Close();

                dblPoint = GetPointByMemberID(objHelper, intMemberID);

                return objAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                objHelper.destroy();
            }
        }
        /// <summary>
        /// Created date: 02.03.2018
        /// Description: Load Point By MemberID
        /// </summary>
        /// <returns></returns>
        public double GetPointByMemberID(Sqlhelper objHelper, int intMemberID)
        {
            try
            {
                double dblPoint = 0;
                string strQuery = "SP_MEMBER_SELPOINT";
                SqlParameter[] arrParam = new SqlParameter[1];
                arrParam[0] = new SqlParameter("@MemberID", intMemberID);

                SqlCommand objCommand = objHelper.GetCommand(strQuery, arrParam, true);
                SqlDataReader reader = objCommand.ExecuteReader();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader["Points"]))
                        dblPoint = Convert.ToDouble(reader["Points"].ToString());
                }

                reader.Close();
                
                return dblPoint;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                throw;
            }
        }
        public IEnumerable<AwardMegaball> GetListAwardByDay(DateTime fromDate, DateTime toDate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardMegaball> lstAward = new List<AwardMegaball>();
                string sql = "SP_GetListAwardByDayFE";
                SqlParameter[] pa = new SqlParameter[2];
              
                pa[0] = new SqlParameter("@fromDate", fromDate);
                pa[1] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardMegaball number = new AwardMegaball();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());

                    number.AwardName = reader["AwardName"].ToString();
                    number.AwardValue = double.Parse(reader["AwardValue"].ToString());
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());

                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());

                    number.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    number.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    number.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    number.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    number.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    number.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<AwardMegaball> GetListAwardOrderByDate()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardMegaball> lstAward = new List<AwardMegaball>();
                string sql = "SP_GetListAwardOrderByDateFE";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardMegaball number = new AwardMegaball();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.AwardName = reader["AwardName"].ToString();
                    number.AwardValue = double.Parse(reader["AwardValue"].ToString());
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    number.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    number.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    number.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    number.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    number.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    number.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    number.GenCode = reader["GenCode"].ToString();
                    number.DrawID = int.Parse(reader["DrawID"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<AwardMegaball> GetListAwardByDate(DateTime date)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardMegaball> lstAward = new List<AwardMegaball>();
                string sql = "SP_GetListAwardByDate";

                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@date", date);

                SqlCommand command = helper.GetCommand(sql, pa, true);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardMegaball number = new AwardMegaball();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.AwardName = reader["AwardName"].ToString();
                    number.AwardValue = double.Parse(reader["AwardValue"].ToString());
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    number.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    number.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    number.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    number.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    number.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    number.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    number.GenCode = reader["GenCode"].ToString();
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<AwardMegaball> GetListAwardMegaball(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardMegaball> lstAwardMegaball = new List<AwardMegaball>();
                string sql = "SP_GetListAwardMagaball";
                SqlParameter[] pa = new SqlParameter[2];

                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardMegaball number = new AwardMegaball();

                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    number.IsActive = int.Parse(reader["IsActive"].ToString());
                    number.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    number.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    number.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    number.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    number.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    number.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    number.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());

                    number.JackPot = decimal.Parse(reader["JackPot"].ToString());
                    number.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    number.DrawID = int.Parse(reader["DrawID"].ToString());
                    number.TotalWon = decimal.Parse(reader["TotalWon"].ToString());
                    lstAwardMegaball.Add(number);

                }
                return lstAwardMegaball;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public AwardMegaball GetAwardMegaballByID(int intNumberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                AwardMegaball number = new AwardMegaball();
                string sql = "SP_GetAwardMagaballByID";
                SqlParameter[] pa = new SqlParameter[1];

                pa[0] = new SqlParameter("@NumberID", intNumberID);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    number.IsActive = int.Parse(reader["IsActive"].ToString());
                    number.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    number.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    number.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    number.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    number.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    number.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    number.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    number.JackPot = decimal.Parse(reader["JackPot"].ToString());
                    number.GenCode = reader["GenCode"].ToString();
                    number.DrawID = int.Parse(reader["DrawID"].ToString());
                }
                return number;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public AwardMegaball GetAwardMegabalByDrawID(int intDrawID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            AwardMegaball objAwardMegaballBO = new AwardMegaball();
            try
            {
                string sql = "SP_GetAwardMegabalByDrawID";
                SqlParameter[] pa = new SqlParameter[1];

                pa[0] = new SqlParameter("@DrawID", intDrawID);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    objAwardMegaballBO.NumberID = int.Parse(reader["NumberID"].ToString());
                    objAwardMegaballBO.AwardID = int.Parse(reader["AwardID"].ToString());
                    objAwardMegaballBO.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    objAwardMegaballBO.IsActive = int.Parse(reader["IsActive"].ToString());
                    objAwardMegaballBO.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    objAwardMegaballBO.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    objAwardMegaballBO.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    objAwardMegaballBO.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    objAwardMegaballBO.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    objAwardMegaballBO.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    objAwardMegaballBO.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    objAwardMegaballBO.JackPot = decimal.Parse(reader["JackPot"].ToString());
                    //objAwardMegaballBO.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    objAwardMegaballBO.DrawID = int.Parse(reader["DrawID"].ToString());
                }
                return objAwardMegaballBO;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

    }
}
