﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Data
{
    public class TransactionCoinBusiness : ICoinServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public IEnumerable<TransactionCoinBO> ListTransactionCoinPaging(string coinID, int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionCoinBO> lstTransaction = new List<TransactionCoinBO>();
                string sql = "SP_ListTransactionCoinID";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);

                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionCoinBO transaction = new TransactionCoinBO();

                    transaction.TransactionID = reader["TransactionID"].ToString();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    transaction.WalletAddressID = reader["WalletAddressID"].ToString();
                    transaction.ValueTransaction = float.Parse(reader["ValueTransaction"].ToString());
                    transaction.QRCode = reader["QRCode"].ToString();
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.WalletID = reader["WalletID"].ToString();

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.Note = reader["Note"].ToString();
                    transaction.TransactionBitcoin = reader["TransactionBitcoin"].ToString();
                    transaction.TypeTransactionID = int.Parse(reader["TypeTransactionID"].ToString());
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());

                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<TransactionCoinBO> ListTransactionCoinWalletBySearch(string coinID, int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionCoinBO> lstTransaction = new List<TransactionCoinBO>();
                string sql = "SP_ListTransactionWalletCoinFE";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                pa[5] = new SqlParameter("@coinID", coinID);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionCoinBO transaction = new TransactionCoinBO();
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    transaction.CoinID = reader["CoinID"].ToString();
                    transaction.Note = reader["Note"].ToString();
                    transaction.QRCode = reader["QRCode"].ToString();
                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionBitcoin = reader["TransactionBitcoin"].ToString();
                    transaction.TransactionID = reader["TransactionID"].ToString();
                    transaction.TypeTransactionID = int.Parse(reader["TypeTransactionID"].ToString());
                    transaction.ValueTransaction = double.Parse(reader["ValueTransaction"].ToString());
                    transaction.WalletAddressID = reader["WalletAddressID"].ToString();
                    transaction.WalletID = reader["WalletID"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

    }
}
