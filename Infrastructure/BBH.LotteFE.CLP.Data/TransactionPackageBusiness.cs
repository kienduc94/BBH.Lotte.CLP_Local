﻿
using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Data
{
    public class TransactionPackageBusiness : IPackageServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public bool InsertTransactionPackage(TransactionPackageBO transaction)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_InsertTransactionPackageFE";
                SqlParameter[] pa = new SqlParameter[7];
                pa[0] = new SqlParameter("@memberID", transaction.MemberID);
                pa[1] = new SqlParameter("@packageID", transaction.PackageID);
                pa[2] = new SqlParameter("@status", transaction.Status);
                pa[3] = new SqlParameter("@createDate", transaction.CreateDate);
                pa[4] = new SqlParameter("@expireDate", transaction.ExpireDate);
                pa[5] = new SqlParameter("@transactionValue", transaction.TransactionValue);
                pa[6] = new SqlParameter("@note", transaction.Note);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<TransactionPackageBO> ListTransactionPackageByMember(int memberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPackageBO> lstTransaction = new List<TransactionPackageBO>();
                string sql = "SP_ListTransactionPackageByMemberFE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPackageBO transaction = new TransactionPackageBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.PackageID = int.Parse(reader["PackageID"].ToString());
                    transaction.PackageName = reader["PackageName"].ToString();
                    transaction.TransactionValue = reader["TransactionCode"].ToString();
                    transaction.Note = reader["Note"].ToString();
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


        public IEnumerable<TransactionPackageBO> ListTransactionPackageByMember(int memberID, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPackageBO> lstTransaction = new List<TransactionPackageBO>();
                string sql = "SP_ListTransactionPackageByMember_FE";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPackageBO transaction = new TransactionPackageBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.PackageID = int.Parse(reader["PackageID"].ToString());
                    transaction.PackageName = reader["PackageName"].ToString();
                    transaction.PackageNameEnglish = reader["PackageNameEnglish"].ToString();
                    transaction.TransactionValue = reader["TransactionCode"].ToString();
                    transaction.Note = reader["Note"].ToString();
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<TransactionPackageBO> ListTransactionPackageBySearch(int memberID, DateTime fromDate, DateTime toDate, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPackageBO> lstTransaction = new List<TransactionPackageBO>();
                string sql = "SP_ListTransactionPackageBySearchFrontEnd";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPackageBO transaction = new TransactionPackageBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.PackageID = int.Parse(reader["PackageID"].ToString());
                    transaction.PackageName = reader["PackageName"].ToString();
                    transaction.PackageNameEnglish = reader["PackageNameEnglish"].ToString();
                    transaction.TransactionValue = reader["TransactionCode"].ToString();
                    transaction.Note = reader["Note"].ToString();
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


        public IEnumerable<TransactionPackageBO> ListTransactionPackageActiveByMember(int memberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPackageBO> lstTransaction = new List<TransactionPackageBO>();
                string sql = "SP_ListTransactionPackageActiveByMemberFE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPackageBO transaction = new TransactionPackageBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.PackageID = int.Parse(reader["PackageID"].ToString());
                    transaction.TransactionValue = reader["TransactionCode"].ToString();
                    transaction.Note = reader["Note"].ToString();
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool CheckExpirePackage(int memberID, DateTime dateNow)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckExpirePackageFE ";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@dateNow", dateNow);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;

                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        public IEnumerable<PackageBO> ListAllPackage()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<PackageBO> lstPackage = new List<PackageBO>();
                string sql = "SP_ListAllPackageFE";
                SqlCommand command = helper.GetCommandNonParameter(sql, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    PackageBO package = new PackageBO();
                    package.PackageID = int.Parse(reader["PackageID"].ToString());

                    package.PackageName = reader["PackageName"].ToString();
                    package.PackageValue = double.Parse(reader["PackageValue"].ToString());
                    package.NumberExpire = int.Parse(reader["NumberExpire"].ToString());
                    package.PackageNameEnglish = reader["PackageNameEnglish"].ToString();
                    lstPackage.Add(package);

                }
                return lstPackage;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


    }
}
