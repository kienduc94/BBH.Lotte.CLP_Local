﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Data
{
    public class AdminBusiness
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public static int InsertAdmin(AdminBO admin)
        {
            //string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                int adminID = 0;
                SqlParameter[] pa = new SqlParameter[5];
                string sql = "insert into admin(UserName,Password,Hashkey,IsVisible,CreateDate) values(@userName,@pass,@hashKey,@isVisible,@createDate); SELECT SCOPE_IDENTITY()";
                pa[0] = new SqlParameter("@userName", admin.UserName);
                pa[1] = new SqlParameter("@pass", admin.Password);
                pa[2] = new SqlParameter("@hashKey", admin.Hashkey);
                pa[3] = new SqlParameter("@isVisible", admin.IsVisible);
                pa[4] = new SqlParameter("@createDate", admin.CreateDate);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                adminID = Convert.ToInt32(command.ExecuteScalar());
                return adminID;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Exception insert admin : " + ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool LoginManagerAccount(string username, string password)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                bool rs = false;
                string sql = "select UserName,Password from admin where UserName=@userName and Password=@pass and IsVisible=1";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@userName", username);
                pa[1] = new SqlParameter("@pass", password);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Exception login admin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static List<AdminBO> ListAllAdmin()
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {

                List<AdminBO> list = new List<AdminBO>();
                string sql = "select * from admin";

                SqlCommand command = helper.GetCommandNonParameter(sql, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AdminBO admin = new AdminBO();
                    admin.AdminID = int.Parse(reader["AdminID"].ToString());
                    admin.UserName = reader["UserName"].ToString();
                    admin.Password = reader["Password"].ToString();
                    admin.Hashkey = reader["HashKey"].ToString();
                    admin.IsVisible = int.Parse(reader["IsVisible"].ToString());
                    list.Add(admin);
                }
                return list;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Exception get list admind : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static AdminBO GerAdminDetail(string username)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {

                AdminBO admin = null;
                string sql = "select * from admin where UserName=@userName";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@userName", username);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    admin = new AdminBO();
                    admin.AdminID = int.Parse(reader["AdminID"].ToString());
                    admin.UserName = reader["UserName"].ToString();
                    admin.Password = reader["Password"].ToString();
                    admin.Hashkey = reader["HashKey"].ToString();
                    admin.IsVisible = int.Parse(reader["IsVisible"].ToString());
                }
                return admin;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Exception Get admindetail : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static AdminBO GerAdminDetail(int adminID)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {

                AdminBO admin = null;
                string sql = "select * from admin where AdminID=@adminID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@adminID", adminID);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    admin = new AdminBO();
                    admin.AdminID = int.Parse(reader["AdminID"].ToString());
                    admin.UserName = reader["UserName"].ToString();
                    admin.Password = reader["Password"].ToString();
                    admin.Hashkey = reader["HashKey"].ToString();
                    admin.IsVisible = int.Parse(reader["IsVisible"].ToString());
                }
                return admin;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Exception get admindetail : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static int GetManagerIDNewest()
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {

                string sql = "select top 1 AdminID from admin order by AdminID desc";
                SqlCommand command = helper.GetCommandNonParameter(sql, false);
                SqlDataReader dr = command.ExecuteReader();
                int managerID = 0;
                if (dr.Read())
                {
                    managerID = int.Parse(dr["AdminID"].ToString());
                }
                return managerID;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Exception get managerid newest : " + ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdateStatusAdmin(int adminID, int visible)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                string sql = "update admin set IsVisible=@isVisible where AdminID=@adminID";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@adminID", adminID);
                pa[1] = new SqlParameter("@isVisible", visible);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Exception update status admin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdateUserNameAndPasswordAdmin(int adminID, string userName, string password)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                string sql = "update admin set UserName=@userName,Password=@password where AdminID=@adminID";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@adminID", adminID);
                pa[1] = new SqlParameter("@userName", userName);
                pa[2] = new SqlParameter("@password", password);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Exception update username and password admin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdateUserNamedAdmin(int adminID, string userName)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                string sql = "update admin set UserName=@userName where AdminID=@adminID";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@adminID", adminID);
                pa[1] = new SqlParameter("@userName", userName);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Exception update username admin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool InsertAccessRight(AccessRightBO access)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                bool rs = false;
                string sql = "insert into accessright(GroupID,AdminID) values(@groupID,@adminID)";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@adminID", access.AdminID);
                pa[1] = new SqlParameter("@groupID", access.GroupID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch
            {
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool DeleteAccessRight(int adminID)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                bool rs = false;
                string sql = "delete from accessright where AdminID=@adminID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@adminID", adminID);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch
            {
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static List<AccessRightBO> ListAccessRightByManagerID(int adminID)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                List<AccessRightBO> list = new List<AccessRightBO>();
                string sql = "select *from accessright where AdminID=@adminID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@adminID", adminID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AccessRightBO access = new AccessRightBO();
                    access.AdminID = int.Parse(reader["AdminID"].ToString());
                    access.GroupID = int.Parse(reader["GroupID"].ToString());
                    list.Add(access);
                }
                return list;
            }
            catch
            {
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static List<GroupAdminBO> ListGroupAdmin()
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                List<GroupAdminBO> list = new List<GroupAdminBO>();
                string sql = "select *from groupadmin";

                SqlCommand command = helper.GetCommandNonParameter(sql, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    GroupAdminBO group = new GroupAdminBO();
                    group.GroupID = int.Parse(reader["GroupID"].ToString());
                    group.GroupName = reader["GroupName"].ToString();
                    group.IsActive = int.Parse(reader["IsActive"].ToString());
                    list.Add(group);
                }
                return list;
            }
            catch
            {
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static GroupAdminBO GetGroupAdminDetail(int groupID)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                GroupAdminBO group = null;
                string sql = "select *from groupadmin where GroupID=@groupID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@groupID", groupID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    group = new GroupAdminBO();
                    group.GroupID = int.Parse(reader["GroupID"].ToString());
                    group.GroupName = reader["GroupName"].ToString();
                    group.IsActive = int.Parse(reader["IsActive"].ToString());

                }
                return group;
            }
            catch
            {
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


        public static bool InsertAdminfomation(AdminInfomationBO member)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                bool rs = false;
                string sql = "insert into admininfomation(AdminID,FullName,Email,Mobile,Address,BirthDay,Gender,Avatar) values(@adminID,@fullname,@email,@mobile,@addressMember,@birthday,@gender,@avatar)";
                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@adminID", member.AdminID);
                pa[1] = new SqlParameter("@fullname", member.FullName);
                pa[2] = new SqlParameter("@email", member.Email);

                pa[3] = new SqlParameter("@mobile", member.Mobile);

                pa[4] = new SqlParameter("@gender", member.Gender);
                if (member.Birthday != null)
                {
                    pa[5] = new SqlParameter("@birthday", member.Birthday);
                }
                else
                {
                    pa[5] = new SqlParameter("@birthday", DBNull.Value);
                }

                pa[6] = new SqlParameter("@avatar", member.Avatar);
                pa[7] = new SqlParameter("@addressMember", member.Address);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdateAdminInfomation(AdminInfomationBO member, int adminID)
        {
            Sqlhelper helper = new Sqlhelper("");
            try
            {
                bool rs = false;
                string sql = "update admininfomation set FullName=@fullName,Email=@email,Mobile=@mobile,Gender=@gender,BirthDay=@birthday,Avatar=@avatar,Address=@addressMember where AdminID=@adminID";
                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@adminID", adminID);
                pa[1] = new SqlParameter("@fullname", member.FullName);
                pa[2] = new SqlParameter("@email", member.Email);

                pa[3] = new SqlParameter("@mobile", member.Mobile);

                pa[4] = new SqlParameter("@gender", member.Gender);
                pa[5] = new SqlParameter("@birthday", member.Birthday);

                pa[6] = new SqlParameter("@avatar", member.Avatar);
                pa[7] = new SqlParameter("@addressMember", member.Address);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {

                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
