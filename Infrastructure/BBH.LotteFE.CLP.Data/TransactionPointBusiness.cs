﻿
using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Data
{
    public class TransactionPointBusiness : IPointsServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public bool InsertTransactionPoint(TransactionPointsBO transaction)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_InsertTransactionPointfe";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@memberID", transaction.MemberID);
                pa[1] = new SqlParameter("@points", transaction.Points);
                pa[2] = new SqlParameter("@status", transaction.Status);
                pa[3] = new SqlParameter("@createDate", transaction.CreateDate);
                pa[4] = new SqlParameter("@transactionCode", transaction.TransactionCode);
                pa[5] = new SqlParameter("@note", transaction.Note);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        //public IEnumerable<TransactionPointsBO> ListTransactionPointByMember(int memberID)
        //{
        //    Sqlhelper helper = new Sqlhelper("", "ConnectionString");
        //    try
        //    {
        //        List<TransactionPointsBO> lstTransaction = new List<TransactionPointsBO>();
        //        string sql = "select tp.*,m.Email,m.E_Wallet from transactionpoint tp left join member m on tp.MemberID=m.MemberID where tp.MemberID=@memberID  order by CreateDate DESC";
        //        SqlParameter[] pa = new SqlParameter[1];
        //        pa[0] = new SqlParameter("@memberID", memberID);
        //        SqlCommand command = helper.GetCommand(sql,pa, false);
        //        SqlDataReader reader = command.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            TransactionPointsBO transaction = new TransactionPointsBO();
        //            transaction.MemberID = int.Parse(reader["MemberID"].ToString());

        //            transaction.Status = int.Parse(reader["Status"].ToString());
        //            transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
        //            transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
        //            transaction.Points = double.Parse(reader["Points"].ToString());
        //            transaction.TransactionCode = reader["TransactionCode"].ToString();
        //            transaction.Note = reader["Note"].ToString();
        //            transaction.Email = reader["Email"].ToString();
        //            transaction.E_Wallet = reader["E_Wallet"].ToString();
        //            lstTransaction.Add(transaction);

        //        }
        //        return lstTransaction;
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteErrorLog.WriteLogsToFileText(ex.Message);
        //        return null;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}
        public IEnumerable<TransactionPointsBO> ListTransactionPointByMember(int memberID, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPointsBO> lstTransaction = new List<TransactionPointsBO>();
                string sql = "SP_ListTransactionPointByMemberFE";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPointsBO transaction = new TransactionPointsBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.Points = double.Parse(reader["Points"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.Note = reader["Note"].ToString();
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<TransactionPointsBO> ListTransactionPointBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionPointsBO> lstTransaction = new List<TransactionPointsBO>();
                string sql = "SP_ListTransactionPointBySearchFrontEnd";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionPointsBO transaction = new TransactionPointsBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionID = int.Parse(reader["TransactionID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.Points = double.Parse(reader["Points"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.Note = reader["Note"].ToString();
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public ExchangePointBO ListExchangePointByMember(int memberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                ExchangePointBO objExchangePointBO = new ExchangePointBO();
                string sql = "SP_ListExchangePointByMember";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    objExchangePointBO.MemberID = int.Parse(reader["MemberID"].ToString());
                    objExchangePointBO.ExchangeID = int.Parse(reader["ExchangeID"].ToString());
                    objExchangePointBO.PointValue = float.Parse(reader["PointValue"].ToString());
                    objExchangePointBO.BitcoinValue = float.Parse(reader["BitcoinValue"].ToString());
                    break;
                }
                return objExchangePointBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public ExchangeTicketBO ListExchangeTicketByMember(int memberID, string strCoinID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                ExchangeTicketBO objExchangeTicketBO = null;
                string sql = "SP_ListExchangeTicketByMemberFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@CoinID", strCoinID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (objExchangeTicketBO == null)
                        objExchangeTicketBO = new ExchangeTicketBO();

                    objExchangeTicketBO.MemberID = int.Parse(reader["MemberID"].ToString());
                    objExchangeTicketBO.ExchangeID = int.Parse(reader["ExchangeID"].ToString());
                    objExchangeTicketBO.PointValue = float.Parse(reader["PointValue"].ToString());
                    objExchangeTicketBO.TicketNumber = float.Parse(reader["TicketNumber"].ToString());
                    objExchangeTicketBO.CoinID = reader["CoinID"].ToString();
                    break;
                }
                return objExchangeTicketBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool InsertTransactionCoin(TransactionCoinBO transaction)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_InsertTransactionCoinFE";
                SqlParameter[] pa = new SqlParameter[13];
                pa[0] = new SqlParameter("@TransactionID", transaction.TransactionID);
                pa[1] = new SqlParameter("@WalletAddressID", transaction.WalletAddressID);
                pa[2] = new SqlParameter("@MemberID", transaction.MemberID);
                pa[3] = new SqlParameter("@ValueTransaction", transaction.ValueTransaction);
                pa[4] = new SqlParameter("@QRCode", transaction.QRCode);
                pa[5] = new SqlParameter("@CreateDate", transaction.CreateDate);
                pa[6] = new SqlParameter("@ExpireDate", transaction.ExpireDate);
                pa[7] = new SqlParameter("@Status", transaction.Status);
                pa[8] = new SqlParameter("@Note", transaction.Note);
                pa[9] = new SqlParameter("@WalletID", transaction.WalletID);
                pa[10] = new SqlParameter("@TypeTransactionID", transaction.TypeTransactionID);
                pa[11] = new SqlParameter("@TransactionBitcoin", transaction.TransactionBitcoin);
                pa[12] = new SqlParameter("@CoinID", transaction.CoinID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        /// <summary>

        /// Edited date: 12.01.2018
        /// Description: Thay DateTime -> DateTime?
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<TransactionCoinBO> ListTransactionWalletBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionCoinBO> lstTransaction = new List<TransactionCoinBO>();
                string sql = "SP_ListTransactionWalletBySearchFrontEnd";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionCoinBO transaction = new TransactionCoinBO();
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    transaction.Note = reader["Note"].ToString();
                    transaction.QRCode = reader["QRCode"].ToString();
                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionBitcoin = reader["TransactionBitcoin"].ToString();
                    transaction.TransactionID = reader["TransactionID"].ToString();
                    transaction.TypeTransactionID = int.Parse(reader["TypeTransactionID"].ToString());
                    transaction.ValueTransaction = double.Parse(reader["ValueTransaction"].ToString());
                    transaction.WalletAddressID = reader["WalletAddressID"].ToString();
                    transaction.WalletID = reader["WalletID"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<TransactionCoinBO> ListTransactionWalletByMember(int memberID, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TransactionCoinBO> lstTransaction = new List<TransactionCoinBO>();
                string sql = "SP_ListTransactionWalletByMemberFE";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionCoinBO transaction = new TransactionCoinBO();
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.ExpireDate = DateTime.Parse(reader["ExpireDate"].ToString());
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    transaction.Note = reader["Note"].ToString();
                    transaction.QRCode = reader["QRCode"].ToString();
                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionBitcoin = reader["TransactionBitcoin"].ToString();
                    transaction.TransactionID = reader["TransactionID"].ToString();
                    transaction.TypeTransactionID = int.Parse(reader["TypeTransactionID"].ToString());
                    transaction.ValueTransaction = double.Parse(reader["ValueTransaction"].ToString());
                    transaction.WalletAddressID = reader["WalletAddressID"].ToString();
                    transaction.WalletID = reader["WalletID"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool CheckExistTransactionBitcoin(string strTransactionID, int intMemberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckExistTransactionBitcoinFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@TransactionBitcoin", strTransactionID);
                pa[1] = new SqlParameter("@MemberID", intMemberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    rs = true;
                    break;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        /// <summary>

        /// Created date: 11.01.2018
        /// Description: Load Info Point Wallet Address
        /// </summary>
        /// <param name="intMemberID"></param>
        /// <returns></returns>
        public MemberWalletBO LoadInfoPointWallet(int intMemberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            MemberWalletBO objWallet = new MemberWalletBO();
            try
            {
                string sql = "SP_Member_Wallet_SELPOINT";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@MemberID", intMemberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader["Points"]))
                        objWallet.Points = Convert.ToDouble(reader["Points"]);
                    if (!Convert.IsDBNull(reader["WalletAddress"]))
                        objWallet.WalletAddress = Convert.ToString(reader["WalletAddress"]);
                    if (!Convert.IsDBNull(reader["RippleAddress"]))
                        objWallet.RippleAddress = Convert.ToString(reader["RippleAddress"]).Trim();
                    if (!Convert.IsDBNull(reader["SerectKeyRipple"]))
                        objWallet.SerectKeyRipple = Convert.ToString(reader["SerectKeyRipple"]).Trim();
                }
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message);
            }
            finally
            {
                helper.destroy();
            }

            return objWallet;
        }
    }
}
