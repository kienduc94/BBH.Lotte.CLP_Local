﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using BBH.LotteFE.CLP.Domain;
using BBC.Core.Database;
using BBH.LotteFE.CLP.Domain.Interfaces;
using BBC.Core.Common.Log;

namespace BBH.LotteFE.CLP.Data
{
    public class AwardBusiness : IAwardServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public bool InsertAward(AwardBO award)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_InsertAwardFE";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@awardName", award.AwardName);
                pa[1] = new SqlParameter("@isActive", award.IsActive);
                pa[2] = new SqlParameter("@isDelete", award.IsDelete);
                pa[3] = new SqlParameter("@awardValue", award.AwardValue);
                pa[4] = new SqlParameter("@createDate", award.CreateDate);
                pa[5] = new SqlParameter("@priority", award.Priority);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool UpdateAward(AwardBO award)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateAwardFE";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@awardName", award.AwardName);
                pa[1] = new SqlParameter("@awardValue", award.AwardValue);
                pa[2] = new SqlParameter("@awardID", award.AwardID);
               
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool LockAndUnlockAward(int awardID, int isActive)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_LockAndUnlockAwardFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@isActive", isActive);
                pa[1] = new SqlParameter("@awardID", awardID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<AwardBO> GetListAward(int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardBO> lstAward = new List<AwardBO>();
                string sql = "SP_GetListAwardFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardBO number = new AwardBO();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.AwardName = reader["AwardName"].ToString();
                    number.IsActive = int.Parse(reader["IsActive"].ToString());


                    number.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool InsertAwardNumber(AwardNumberBO award)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_InsertAwardNumberFE";
                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@numberID", award.NumberID);
                pa[1] = new SqlParameter("@isActive", award.IsActive);
                pa[2] = new SqlParameter("@isDelete", award.IsDelete);
                pa[3] = new SqlParameter("@awardID", award.AwardID);
                pa[4] = new SqlParameter("@createDate", award.CreateDate);
                pa[5] = new SqlParameter("@priority", award.Priority);
                pa[6] = new SqlParameter("@numberValue", award.NumberValue);
                pa[7] = new SqlParameter("@stationName", award.StationName);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<AwardNumberBO> GetListAwardByDate(int start, int end, DateTime fromDate, DateTime toDate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardNumberBO> lstAward = new List<AwardNumberBO>();
                string sql = "SP_GetListAwardByDateFE";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardNumberBO number = new AwardNumberBO();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.NumberValue = reader["NumberValue"].ToString();
                    number.AwardName = reader["AwardName"].ToString();
                    number.AwardValue = double.Parse(reader["AwardValue"].ToString());
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());
                   
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    number.StationName = reader["StationName"].ToString();
                    number.StationNameEnglish = reader["StationNameEnglish"].ToString();
                    
                    lstAward.Add(number);
                    
                }
                return lstAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<AwardBO> GetAllListAward()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardBO> lstAward = new List<AwardBO>();
                string sql = "SP_GetAllAwardFE";
                
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardBO number = new AwardBO();
                    if (!string.IsNullOrEmpty(reader["AwardID"].ToString())) number.AwardID = int.Parse(reader["AwardID"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardName"].ToString())) number.AwardName = reader["AwardName"].ToString();
                    if (!string.IsNullOrEmpty(reader["AwardValue"].ToString())) number.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    if (!string.IsNullOrEmpty(reader["Priority"].ToString())) number.Priority = int.Parse(reader["Priority"].ToString());
                    if (!string.IsNullOrEmpty(reader["CreateDate"].ToString())) number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardNameEnglish"].ToString())) number.AwardNameEnglish = reader["AwardNameEnglish"].ToString();
                    if (!string.IsNullOrEmpty(reader["NumberBallNormal"].ToString())) number.NumberBallNormal = int.Parse(reader["NumberBallNormal"].ToString());
                    if (!string.IsNullOrEmpty(reader["NumberBallGold"].ToString())) number.NumberBallGold = int.Parse(reader["NumberBallGold"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardFee"].ToString())) number.AwardFee = decimal.Parse(reader["AwardFee"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardPercent"].ToString())) number.AwardPercent = float.Parse(reader["AwardPercent"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
