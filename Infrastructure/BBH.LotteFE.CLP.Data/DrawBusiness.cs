﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace BBH.LotteFE.CLP.Data
{
    public class DrawBusiness: IDrawServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public DrawBO GetDrawIDByDate(DateTime dtTime)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<DrawBO> lstDrawBO = new List<DrawBO>();
                string sql = "SP_GetDrawIDByDate";

                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@date", dtTime);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                DrawBO objDrawBO = new DrawBO();
                if (reader.Read())
                {
                    objDrawBO.DrawID = int.Parse(reader["DrawID"].ToString());
                    objDrawBO.StartDate = DateTime.Parse(reader["StartDate"].ToString());
                    objDrawBO.EndDate = DateTime.Parse(reader["EndDate"].ToString());
                    objDrawBO.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                }
                return objDrawBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public ObjResultMessage InsertDraw(DrawBO objDraw)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<DrawBO> lstDrawBO = new List<DrawBO>();
                string sql = "SP_InsertDraw";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@StartDate", objDraw.StartDate);
                pa[1] = new SqlParameter("@EndDate", objDraw.EndDate);
                pa[2] = new SqlParameter("@CreatedDate", objDraw.CreatedDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    objResult.IsError = false;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "BookingMegaballBusiness -> InsertBooking : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            finally
            {
                helper.destroy();
            }
            return objResult;
        }
        public DrawBO GetNewestDrawID()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<DrawBO> lstDrawBO = new List<DrawBO>();
                string sql = "SP_GetNewestDrawID";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                DrawBO objDrawBO = new DrawBO();
                if (reader.Read())
                {
                    objDrawBO.DrawID = int.Parse(reader["DrawID"].ToString());
                    objDrawBO.StartDate = DateTime.Parse(reader["StartDate"].ToString());
                    objDrawBO.EndDate = DateTime.Parse(reader["EndDate"].ToString());
                    objDrawBO.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                }
                return objDrawBO;
            }
            catch (Exception ex)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
