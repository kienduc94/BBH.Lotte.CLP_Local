﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Data;
using System.IO;
using BBC.Core.Database;
using System.Data.SqlClient;
using BBH.LotteFE.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using BBC.Core.Common.Utils;
using BBC.Core.Common.Log;

namespace BBH.LotteFE.CLP.Data
{
    public class AwardWithdrawBusiness : IAwardWithdrawServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public ObjResultMessage InsertAwardWithdraw(AwardWithdrawBO awardWithdraw)
        {
            ObjResultMessage objResult = new ObjResultMessage();

            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_InsertAwardWithdraw";
                SqlParameter[] pa = new SqlParameter[10];
                pa[0] = new SqlParameter("@TransactionID", awardWithdraw.TransactionID);
                pa[1] = new SqlParameter("@RequestWalletAddress", awardWithdraw.RequestWalletAddress);
                pa[2] = new SqlParameter("@RequestMemberID", awardWithdraw.RequestMemberID);

                pa[3] = new SqlParameter("@CoinIDWithdraw", awardWithdraw.CoinIDWithdraw);

                pa[4] = new SqlParameter("@RequestStatus", awardWithdraw.RequestStatus);

                pa[5] = new SqlParameter("@ValuesWithdraw", awardWithdraw.ValuesWithdraw);
                pa[6] = new SqlParameter("@RequestDate", awardWithdraw.RequestDate);

                pa[7] = new SqlParameter("@IsDeleted", awardWithdraw.IsDeleted);
                pa[8] = new SqlParameter("@AwardDate", awardWithdraw.AwardDate);
                pa[9] = new SqlParameter("@BookingID", awardWithdraw.BookingID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    objResult.IsError = false;
                }
                else
                {
                    objResult.IsError = true;
                }

            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardWithdrawBusiness -> InsertAwardWithdraw : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            finally
            {
                helper.destroy();
            }
            return objResult;
        }

        public ObjResultMessage GetAwardWithdrawDetail(string requestMemberID, DateTime awardDate, int bookingID, ref AwardWithdrawBO objAward)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {

                string sql = "SP_GetAwardWithdrawDetail_FE";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@RequestMemberID", requestMemberID);
                pa[1] = new SqlParameter("@AwardDate", awardDate);
                pa[2] = new SqlParameter("@BookingID", bookingID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    objAward = new AwardWithdrawBO();
                    objAward.TransactionID = reader["TransactionID"].ToString();
                    objAward.RequestMemberID = reader["RequestMemberID"].ToString();
                    objAward.ValuesWithdraw = decimal.Parse(reader["ValuesWithdraw"].ToString());
                    objAward.RequestWalletAddress = reader["RequestWalletAddress"].ToString();
                    objAward.RequestDate = DateTime.Parse(reader["RequestDate"].ToString());
                    objAward.RequestStatus = int.Parse(reader["RequestStatus"].ToString());

                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardWithdrawBusiness -> GetAwardWithdrawDetail : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            finally
            {
                helper.destroy();
            }
            return objResult;
        }

        public IEnumerable<AwardWithdrawBO> GetAllListAward()
        {
           // ObjResultMessage objResult = new ObjResultMessage();
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardWithdrawBO> lstAward = new List<AwardWithdrawBO>();
                string sql = "SP_GetAllAwardWithDraw_FE";

                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardWithdrawBO objAward = new AwardWithdrawBO();
                    objAward.TransactionID = reader["TransactionID"].ToString();
                    objAward.RequestMemberID = reader["RequestMemberID"].ToString();
                    objAward.ValuesWithdraw = decimal.Parse(reader["ValuesWithdraw"].ToString());
                    objAward.RequestWalletAddress = reader["RequestWalletAddress"].ToString();
                    objAward.RequestDate = DateTime.Parse(reader["RequestDate"].ToString());
                    objAward.RequestStatus = int.Parse(reader["RequestStatus"].ToString());

                    lstAward.Add(objAward);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }

            finally
            {
                helper.destroy();
            }
        }

        public ObjResultMessage UpdateStatusAwardWithdraw(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();

            AwardWithdrawBO objAward = Algorithm.DecryptionObjectRSA<AwardWithdrawBO>(objRsa.ParamRSAFirst);
            if (objAward == null)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "AwardWithdrawBusiness -> UpdateStatusAwardWithdraw : Null Object";
                objResult.MessageDate = DateTime.Now;
            }
            else
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    string sql = "SP_UpdateStatusAwardWithdraw_CMS";
                    SqlParameter[] pa = new SqlParameter[4];
                    pa[0] = new SqlParameter("@TransactionID", objAward.TransactionID);
                    pa[1] = new SqlParameter("@RequestStatus", objAward.RequestStatus);
                    pa[2] = new SqlParameter("@UpdatedUser", objAward.UpdatedUser);
                    pa[3] = new SqlParameter("@AdminIDApprove", objAward.AdminIDApprove);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        objResult.IsError = false;
                    }
                    else
                    {
                        objResult.IsError = true;
                    }

                }
                catch (Exception ex)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "AwardWithdrawBusiness -> UpdateStatusAwardWithdraw : " + ex.Message;
                    objResult.MessageDate = DateTime.Now;
                    WriteErrorLog.WriteLogsToFileText(ex.Message);
                }
                finally
                {
                    helper.destroy();
                }
            }
            return objResult;
        }

    }
}
