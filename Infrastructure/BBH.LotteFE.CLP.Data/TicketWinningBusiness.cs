﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Data
{
    public class TicketWinningBusiness : ITicketWinningServices
    {
       
        public TiketWinningBO GetListTicketWinningByBookingID(int BookingID, decimal awardvalue, decimal awardfee, int priority, int intDrawID, int intMemberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_GetListTicketWinningByBookingID";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@BookingID", BookingID);
                pa[1] = new SqlParameter("@awardvalue", awardvalue);
                pa[2] = new SqlParameter("@awardfee", awardfee);
                pa[3] = new SqlParameter("@priority", priority);
                pa[4] = new SqlParameter("@DrawID", intDrawID);
                pa[5] = new SqlParameter("@MemberID", intMemberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                TiketWinningBO objTiketWinningBO = new TiketWinningBO(); ;
                if (reader.Read())
                {
                    objTiketWinningBO.BookingID = int.Parse(reader["BookingID"].ToString());
                    objTiketWinningBO.DrawID = int.Parse(reader["DrawID"].ToString());
                    objTiketWinningBO.MemberID = int.Parse(reader["MemberID"].ToString());
                    objTiketWinningBO.Priority = int.Parse(reader["Priority"].ToString());
                    objTiketWinningBO.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    objTiketWinningBO.AwardFee = decimal.Parse(reader["AwardFee"].ToString());
                }
                return objTiketWinningBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<TiketWinningBO> GetListTicketWinningByMemberID(int intMemberID, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TiketWinningBO> lstTiketWinningBO = new List<TiketWinningBO>();
                string sql = "SP_GetListTicketWinningByMemberID";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@memberID", intMemberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                TiketWinningBO objTiketWinningBO;
                while (reader.Read())
                {
                    objTiketWinningBO = new TiketWinningBO();
                    objTiketWinningBO.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    objTiketWinningBO.BookingID = int.Parse(reader["BookingID"].ToString());
                    objTiketWinningBO.DateBuy = DateTime.Parse(reader["BuyDate"].ToString());
                    objTiketWinningBO.DrawID = int.Parse(reader["DrawID"].ToString());
                    objTiketWinningBO.ExtraNumberAward = int.Parse(reader["ExtraNumberAward"].ToString());
                    objTiketWinningBO.ExtraNumberBuy = int.Parse(reader["ExtraNumberBuy"].ToString());
                    objTiketWinningBO.FirstNumberAward = int.Parse(reader["FirstNumberAward"].ToString());
                    objTiketWinningBO.FirstNumberBuy = int.Parse(reader["FirstNumberBuy"].ToString());
                    objTiketWinningBO.FivethNumberAward = int.Parse(reader["FivethNumberAward"].ToString());
                    objTiketWinningBO.FivethNumberBuy = int.Parse(reader["FivethNumberBuy"].ToString());
                    objTiketWinningBO.FourthNumberAward = int.Parse(reader["FourthNumberAward"].ToString());
                    objTiketWinningBO.FourthNumberBuy = int.Parse(reader["FourthNumberBuy"].ToString());
                    objTiketWinningBO.MemberID = int.Parse(reader["MemberID"].ToString());
                    objTiketWinningBO.NumberBallGold = int.Parse(reader["NumberBallGold"].ToString());
                    objTiketWinningBO.NumberBallNormal = int.Parse(reader["NumberBallNormal"].ToString());
                    objTiketWinningBO.Priority = int.Parse(reader["Priority"].ToString());
                    objTiketWinningBO.SecondNumberAward = int.Parse(reader["SecondNumberAward"].ToString());
                    objTiketWinningBO.SecondNumberBuy = int.Parse(reader["SecondNumberBuy"].ToString());
                    objTiketWinningBO.ThirdNumberAward = int.Parse(reader["ThirdNumberAward"].ToString());
                    objTiketWinningBO.ThirdNumberBuy = int.Parse(reader["ThirdNumberBuy"].ToString());
                    objTiketWinningBO.TicketWinningID = int.Parse(reader["TicketWinningID"].ToString());
                    objTiketWinningBO.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    objTiketWinningBO.AwardDate = DateTime.Parse(reader["AwardDate"].ToString());
                    objTiketWinningBO.AwardFee = decimal.Parse(reader["AwardFee"].ToString());
                    objTiketWinningBO.AwardWithdrawStatus = int.Parse(reader["AwardWithdrawStatus"].ToString());
                    objTiketWinningBO.TotalRecord = int.Parse(reader["TotalRecord"].ToString());
                    lstTiketWinningBO.Add(objTiketWinningBO);
                }
                return lstTiketWinningBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<TiketWinningBO> GetListTicketWinningBySearch(int intMemberID, int start, int end, DateTime? startdate, DateTime? enddate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TiketWinningBO> lstTiketWinningBO = new List<TiketWinningBO>();
                string sql = "SP_GetListTicketWinningBySearch";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@memberID", intMemberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                pa[3] = new SqlParameter("@startdate", startdate);
                pa[4] = new SqlParameter("@enddate", enddate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                TiketWinningBO objTiketWinningBO;
                while (reader.Read())
                {
                    objTiketWinningBO = new TiketWinningBO();
                    objTiketWinningBO.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    objTiketWinningBO.BookingID = int.Parse(reader["BookingID"].ToString());
                    objTiketWinningBO.DateBuy = DateTime.Parse(reader["BuyDate"].ToString());
                    objTiketWinningBO.DrawID = int.Parse(reader["DrawID"].ToString());
                    objTiketWinningBO.ExtraNumberAward = int.Parse(reader["ExtraNumberAward"].ToString());
                    objTiketWinningBO.ExtraNumberBuy = int.Parse(reader["ExtraNumberBuy"].ToString());
                    objTiketWinningBO.FirstNumberAward = int.Parse(reader["FirstNumberAward"].ToString());
                    objTiketWinningBO.FirstNumberBuy = int.Parse(reader["FirstNumberBuy"].ToString());
                    objTiketWinningBO.FivethNumberAward = int.Parse(reader["FivethNumberAward"].ToString());
                    objTiketWinningBO.FivethNumberBuy = int.Parse(reader["FivethNumberBuy"].ToString());
                    objTiketWinningBO.FourthNumberAward = int.Parse(reader["FourthNumberAward"].ToString());
                    objTiketWinningBO.FourthNumberBuy = int.Parse(reader["FourthNumberBuy"].ToString());
                    objTiketWinningBO.MemberID = int.Parse(reader["MemberID"].ToString());
                    objTiketWinningBO.NumberBallGold = int.Parse(reader["NumberBallGold"].ToString());
                    objTiketWinningBO.NumberBallNormal = int.Parse(reader["NumberBallNormal"].ToString());
                    objTiketWinningBO.Priority = int.Parse(reader["Priority"].ToString());
                    objTiketWinningBO.SecondNumberAward = int.Parse(reader["SecondNumberAward"].ToString());
                    objTiketWinningBO.SecondNumberBuy = int.Parse(reader["SecondNumberBuy"].ToString());
                    objTiketWinningBO.ThirdNumberAward = int.Parse(reader["ThirdNumberAward"].ToString());
                    objTiketWinningBO.ThirdNumberBuy = int.Parse(reader["ThirdNumberBuy"].ToString());
                    objTiketWinningBO.TicketWinningID = int.Parse(reader["TicketWinningID"].ToString());
                    objTiketWinningBO.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    objTiketWinningBO.AwardDate = DateTime.Parse(reader["AwardDate"].ToString());
                    objTiketWinningBO.AwardFee = decimal.Parse(reader["AwardFee"].ToString());
                    objTiketWinningBO.AwardWithdrawStatus = int.Parse(reader["AwardWithdrawStatus"].ToString());
                    objTiketWinningBO.TotalRecord = int.Parse(reader["TotalRecord"].ToString());
                    lstTiketWinningBO.Add(objTiketWinningBO);
                }
                return lstTiketWinningBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
