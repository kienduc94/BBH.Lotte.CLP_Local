﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using BBH.LotteFE.CLP.Domain;
using BBC.Core.Database;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System.Data;
using NBitcoin;
using BBC.Core.Common.Log;

namespace BBH.LotteFE.CLP.Data
{
    public class MemberBusiness : IMemberServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        private Network objNetwork = Network.TestNet;

        /// <summary>
        /// Edited date: 16.01.2018
        /// Description: Change InsertMember & bolIsResult
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public bool InsertMember(MemberBO member, ref int intMemberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            bool bolIsResult = false;
            try
            {
                InsertMember(helper, ref member, ref bolIsResult);
                intMemberID = member.MemberID;

            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            finally
            {
                helper.destroy();
            }

            return bolIsResult;
        }
        public bool InsertMemberWallet(MemberWalletBO member)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            bool bolIsError = false;
            try
            {
                bolIsError = InsertMemberWallet(helper, member);
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            finally
            {
                helper.destroy();
            }

            return bolIsError;
        }

        /// <summary>
        
        /// Created date: 16.01.2018
        /// Description: InsertMemberWallet (Tách hàm riêng để ko cần mở helper lại)
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        public bool InsertMemberWallet(Sqlhelper helper, MemberWalletBO member)
        {
            bool bolIsResult = false;//true: success
            try
            {
                string sql = "SP_InsertMemberWalletFE";
                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@IndexWallet", member.IndexWallet);
                pa[1] = new SqlParameter("@IsActive", member.IsActive);
                pa[2] = new SqlParameter("@IsDelete", member.IsDelete);
                pa[3] = new SqlParameter("@MemberID", member.MemberID);
                pa[4] = new SqlParameter("@NumberCoin", member.NumberCoin);
                pa[5] = new SqlParameter("@WalletAddress", member.WalletAddress);
                pa[6] = new SqlParameter("@RippleAddress", member.RippleAddress);
                pa[7] = new SqlParameter("@SerectKeyRipple", member.SerectKeyRipple);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    bolIsResult = true;
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                throw;
            }

            return bolIsResult;
        }

        public bool UpdatePasswordMember(string email, string password)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdatePasswordMemberFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@email", email);
                pa[1] = new SqlParameter("@password", password);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool UpdateNewPasswordMember(string email, string oldpassword, string newpassword)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateNewPasswordMemberFE";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@email", email);
                pa[1] = new SqlParameter("@password", oldpassword);
                pa[2] = new SqlParameter("@newpassword", newpassword);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool LockAndUnlockMember(int memberID, int isActive)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_LockAndUnlockMemberFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@isActive", isActive);
                pa[1] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public MemberBO GetMemberDetailByID(int memberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                MemberBO member = null;
                string sql = "SP_GetMemberDetailByIDFE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    member = new MemberBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.Email = reader["Email"].ToString();
                    member.E_Wallet = reader["E_Wallet"].ToString();
                    member.Email = reader["Email"].ToString();
                    member.IsActive = int.Parse(reader["IsActive"].ToString());
                    member.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());
                    member.LinkReference = reader["LinkReference"].ToString();
                    member.NumberTicketFree = int.Parse(reader["NumberTicketFree"].ToString());
                }
                return member;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public MemberBO GetMemberDetailByEmail(string email)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                MemberBO member = null;
                string sql = "SP_GetMemberDetailByEmailFE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@email", email);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    member = new MemberBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.Email = reader["Email"].ToString();
                    member.E_Wallet = reader["E_Wallet"].ToString();
                    member.Email = reader["Email"].ToString();
                    member.IsActive = int.Parse(reader["IsActive"].ToString());
                    member.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());
                    member.Password = reader["Password"].ToString();
                    if (!Convert.IsDBNull(reader["LinkReference"]))
                    {
                        member.LinkReference = reader["LinkReference"].ToString();
                    }
                    member.NumberTicketFree = int.Parse(reader["NumberTicketFree"].ToString());
                    if(!Convert.IsDBNull(reader["TotalSmsCodeInput"]))
                    {
                        member.TotalSmsCodeInput = int.Parse(reader["TotalSmsCodeInput"].ToString());
                    }
                    if (!Convert.IsDBNull(reader["ExpireSmSCode"]))
                    {
                        member.ExpireSmSCode = DateTime.Parse(reader["ExpireSmSCode"].ToString());
                    }
                    if (!Convert.IsDBNull(reader["SmsCode"]))
                    {
                        member.SmsCode = reader["SmsCode"].ToString();
                    }
                     
                    if (!Convert.IsDBNull(reader["Mobile"]))
                    {
                        member.Mobile = reader["Mobile"].ToString();
                    }
                    if (!Convert.IsDBNull(reader["IsIndentifySms"]))
                    {
                        member.IsIndentifySms = int.Parse(reader["IsIndentifySms"].ToString());
                    }
                    //them fullName
                    if (!Convert.IsDBNull(reader["FullName"]))
                        member.FullName = Convert.ToString(reader["FullName"]).Trim();
                    //if(!Convert.IsDBNull(reader["WalletAddress"]))
                    //    member.WalletAddress = Convert.ToString(reader["WalletAddress"]).Trim();
                    //if (!Convert.IsDBNull(reader["RippleAddress"]))
                    //    member.RippleAddress = Convert.ToString(reader["RippleAddress"]).Trim();
                }
                return member;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public double GetPointsMember(int memberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                double points = 0;
                string sql = "SP_GetPointsMemberFE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {

                    points = double.Parse(reader["Points"].ToString());

                }
                return points;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }
        public MemberBO LoginMember(string email, string password)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                MemberBO member = null;
                string sql = "SP_LoginMemberFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@email", email);
                pa[1] = new SqlParameter("@password", password);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    member = new MemberBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.Email = reader["Email"].ToString();
                    member.E_Wallet = reader["E_Wallet"].ToString();
                    member.Email = reader["Email"].ToString();
                    member.IsActive = int.Parse(reader["IsActive"].ToString());
                    member.IsDelete = int.Parse(reader["IsDelete"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());
                    member.Password = reader["Password"].ToString();
                    member.LinkReference = reader["LinkReference"].ToString();
                    member.NumberTicketFree = int.Parse(reader["NumberTicketFree"].ToString());
                    if (!Convert.IsDBNull(reader["FullName"]))
                    {
                        member.FullName = Convert.ToString(reader["FullName"]);
                    }
                }
                return member;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool CheckUserNameExists(string userName)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "select UserName from member where UserName=@userName";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@userName", userName);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool CheckE_WalletExists(string e_wallet)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckE_WalletExistsFE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@e_wallet", e_wallet);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool CheckEmailExists(string email)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckEmailExistsFE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@email", email);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<MemberInfomationBO> GetListMember()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<MemberInfomationBO> lstMember = new List<MemberInfomationBO>();
                string sql = "select m.UserName,m.IsActive,p.* from memberinfomation p left join member m on m.MemberID=p.MemberID order by m.MemberID DESC";
                //SqlParameter[] pa = new SqlParameter[1];
                //pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommandNonParameter(sql, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberInfomationBO member = new MemberInfomationBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.UserName = reader["UserName"].ToString();
                    member.FullName = reader["FullName"].ToString();
                    //member.ProvinceID = int.Parse(reader["ProvinceID"].ToString());
                    member.Email = reader["Email"].ToString();
                    //member.DistrictID = int.Parse(reader["DistrictID"].ToString());
                    //member.Phone = reader["Phone"].ToString();
                    //member.WardID = int.Parse(reader["WardID"].ToString());
                    member.IsActive = int.Parse(reader["IsActive"].ToString());
                    member.Gender = int.Parse(reader["Gender"].ToString());
                    member.Avatar = reader["Avatar"].ToString();
                    member.Address = reader["Address"].ToString();
                    member.Mobile = reader["Mobile"].ToString();
                    member.Birthday = DateTime.Parse(reader["Birthday"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());
                    lstMember.Add(member);

                }
                return lstMember;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<MemberInfomationBO> GetListMemberInfomation(int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<MemberInfomationBO> lstMember = new List<MemberInfomationBO>();
                string sql = "select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.MemberID DESC) as Row,m.MemberID,mb.UserName,m.FullName,m.Avatar,m.Mobile,m.Email,m.Points from memberinfomation m left join member mb on m.MemberID=n.MemberID where group by m.MemberID,mb.UserName,m.FullName,m.Avatar,m.Mobile,m.Email,m.Points) as Products  where Row>=@start and Row<=@end";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberInfomationBO member = new MemberInfomationBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.UserName = reader["UserName"].ToString();
                    member.FullName = reader["FullName"].ToString();

                    member.Email = reader["Email"].ToString();

                    member.Avatar = reader["Avatar"].ToString();
                    member.Mobile = reader["Mobile"].ToString();
                    member.Points = double.Parse(reader["Points"].ToString());
                    lstMember.Add(member);

                }
                return lstMember;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool UpdatePointsMember(int memberID, int points)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdatePointsMemberFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@points", points);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public int CountAllMember()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                int count = 0;
                string sql = "select count(MemberID) as totalMember from memberprofile";
                //SqlParameter[] pa = new SqlParameter[1];
                //pa[0] = new SqlParameter("@isActive", isActive);
                SqlCommand command = helper.GetCommandNonParameter(sql, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    count = int.Parse(reader["totalMember"].ToString());

                }
                return count;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }
        public MemberInfomationBO GetMemberDetail(string email)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                MemberInfomationBO member = null;
                string sql = "select p.*, m.UserName from memberinfomation p left join member m on m.MemberID=p.MemberID where Email=@email";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@email", email);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    member = new MemberInfomationBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.UserName = reader["UserName"].ToString();
                    member.FullName = reader["FullName"].ToString();

                    member.Email = reader["Email"].ToString();


                    member.Mobile = reader["Mobile"].ToString();

                    member.Gender = int.Parse(reader["Gender"].ToString());
                    member.Avatar = reader["Avatar"].ToString();
                    member.Address = reader["Address"].ToString();

                    member.Birthday = DateTime.Parse(reader["Birthday"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());

                }
                return member;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool UpdatePointsMemberFE(int memberID, double points)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdatePointsMember_FE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@points", points);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool CheckExistsMemberByFBID(Sqlhelper helper, string strFBUserID, ref int intMemberID)
        {
            bool bolIsExists = false;
            try
            {
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@Email", strFBUserID);
                SqlCommand command = helper.GetCommand("SP_MEMBER_SEL_CHECKMEM", pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader["MemberID"]))
                        intMemberID = Convert.ToInt32(reader["MemberID"]);

                    bolIsExists = true;
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                throw;
            }

            return bolIsExists;
        }

        /// <summary>
        
        /// Created date: 16.01.2018
        /// Description: Insert Member After Login FB Success
        /// </summary>
        /// <param name="strFBUserID"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        public int InsertMemberAfterLoginFB(ref MemberBO objMember)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                if (objMember != null)
                {
                    int intMemberID = 0;

                    bool bolIsExistsTemp = true;
                    bool bolIsExists = CheckExistsMemberByFBID(helper, objMember.Email, ref intMemberID);
                    if (!bolIsExists)
                    {
                        //Not Exists Member With FBUserID
                        bolIsExistsTemp = false;

                        //Insert member
                        objMember.IsActive = 1;
                        objMember.CreateDate = DateTime.Now;

                        InsertMember(helper, ref objMember, ref bolIsExists);
                        if (!bolIsExists)
                            return 4;//register insert member fails

                        intMemberID = objMember.MemberID;
                    }

                    //exists in table member
                    if (intMemberID > 0)
                    {
                        //if (!bolIsExistsTemp)
                        //{
                        //    //= false: not exists merberid in method CheckExistsMemberByFBID
                        //    //Insert member wallet
                        //    bool bolIsRegister = InsertMemberWalletByMemberID(helper, intMemberID, strMasterKeyWan2Lot, strRippleAddress, strSerectKeyRipple);
                        //    if (!bolIsRegister)
                        //        return 4;//register insert member wallet fail
                        //}

                        //memberID > 0: get object to set session login
                        objMember = LoadMemberByMemberID(helper, intMemberID);

                        if (objMember != null)
                        {
                            if (string.IsNullOrEmpty(objMember.Password))
                            {
                                //password is null
                                return 3;
                            }
                        }
                    }
                }
                else
                {
                    //FBUserID Is Null
                    return 2;
                }

                return 0;//success
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return 1;
            }
            finally
            {
                helper.destroy();
            }
        }

        /// <summary>
        
        /// Created date: 16.01.2018
        /// Description: Insert Member
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="member"></param>
        /// <param name="bolIsResult"></param>
        public void InsertMember(Sqlhelper helper, ref MemberBO objMember, ref bool bolIsResult)
        {
            try
            {
                string sql = "SP_InsertMemberFE";
                SqlParameter[] pa = new SqlParameter[16];
                pa[0] = new SqlParameter("@email", objMember.Email);
                pa[1] = new SqlParameter("@isActive", objMember.IsActive);
                pa[2] = new SqlParameter("@isDelete", objMember.IsDelete);
                pa[3] = new SqlParameter("@password", objMember.Password);
                pa[4] = new SqlParameter("@createDate", objMember.CreateDate);
                pa[5] = new SqlParameter("@points", objMember.Points);
                pa[6] = new SqlParameter("@fullName", objMember.FullName);
                pa[7] = new SqlParameter("@mobile", objMember.Mobile);
                pa[8] = new SqlParameter("@avatar", objMember.Avatar);
                pa[9] = new SqlParameter("@gender", objMember.Gender);
                pa[10] = new SqlParameter("@birthday", objMember.Birthday);
                pa[11] = new SqlParameter("@IsUserType", objMember.IsUserType);
                pa[12] = new SqlParameter("@UserTypeID", objMember.UserTypeID);
                pa[13] = new SqlParameter("@LinkLogin", objMember.LinkLogin == null ? "" : objMember.LinkLogin);
                pa[14] = new SqlParameter("@NumberTicketFree", objMember.NumberTicketFree);
                pa[15] = new SqlParameter("@LinkReference", objMember.LinkReference == null ? "" : objMember.LinkReference);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlParameter outputParam = command.Parameters.Add("@outMemberID", SqlDbType.Int);
                outputParam.Direction = ParameterDirection.Output;

                int row = command.ExecuteNonQuery();

                if (row > 0)
                {
                    objMember.MemberID = (outputParam.Value != null ? Convert.ToInt32(outputParam.Value) : 0);

                    //IsResult: result success
                    bolIsResult = true;
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                throw;
            }
        }

        /// <summary>
        
        /// Created date: 16.01.2018
        /// Description: Check Exists Member By FBUserID
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="strFBUserID"></param>
        /// <returns></returns>
        public MemberBO LoadMemberByMemberID(Sqlhelper helper, int intMemberID)
        {
            MemberBO objMember = null;
            try
            {
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@MemberID", intMemberID);
                SqlCommand command = helper.GetCommand("SP_MEMBER_SELBYMEMID", pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    if (objMember == null)
                        objMember = new MemberBO();

                    if (!Convert.IsDBNull(reader["MemberID"]))
                        objMember.MemberID = int.Parse(reader["MemberID"].ToString());

                    if (!Convert.IsDBNull(reader["Email"]))
                        objMember.Email = reader["Email"].ToString();

                    if (!Convert.IsDBNull(reader["E_Wallet"]))
                        objMember.E_Wallet = reader["E_Wallet"].ToString();

                    if (!Convert.IsDBNull(reader["Password"]))
                        objMember.Password = reader["Password"].ToString();

                    if (!Convert.IsDBNull(reader["Points"]))
                        objMember.Points = double.Parse(reader["Points"].ToString());

                    if (!Convert.IsDBNull(reader["FullName"]))
                        objMember.FullName = Convert.ToString(reader["FullName"]).Trim();

                    if (!Convert.IsDBNull(reader["IsUserType"]))
                        objMember.IsUserType = Convert.ToInt32(reader["IsUserType"]);

                    if (!Convert.IsDBNull(reader["UserTypeID"]))
                        objMember.UserTypeID = Convert.ToString(reader["UserTypeID"]);

                    if (!Convert.IsDBNull(reader["LinkReference"]))
                        objMember.LinkReference = Convert.ToString(reader["LinkReference"]);

                    if (!Convert.IsDBNull(reader["NumberTicketFree"]))
                        objMember.NumberTicketFree = Convert.ToInt32(reader["NumberTicketFree"]);

                    if (!Convert.IsDBNull(reader["Points"]))
                        objMember.Points = Convert.ToDouble(reader["Points"].ToString());

                }

                reader.Close();
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message);
                throw;
            }

            return objMember;
        }

        public bool InsertMemberWalletByMemberID(Sqlhelper helper, int intMemberID, string strMasterKeyWan2Lot, string strRippleAddress, string strSerectKeyRipple)
        {
            bool bolIsResult = false;
            try
            {
                RandomUtils.Random = new UnsecureRandom();
                string strClientExtKey = strMasterKeyWan2Lot;
                ExtKey masterPubKey = new BitcoinExtKey(strClientExtKey, objNetwork);
                ExtKey pubkey = masterPubKey.Derive(intMemberID, hardened: true);

                MemberWalletBO objMemberWallet = new MemberWalletBO();
                objMemberWallet.IndexWallet = intMemberID;
                objMemberWallet.IsActive = 1;
                objMemberWallet.IsDelete = 0;
                objMemberWallet.MemberID = intMemberID;
                objMemberWallet.NumberCoin = 0;
                objMemberWallet.WalletAddress = pubkey.PrivateKey.PubKey.GetAddress(objNetwork).ToString();
                objMemberWallet.RippleAddress = strRippleAddress;
                objMemberWallet.SerectKeyRipple = strSerectKeyRipple;

                bolIsResult = InsertMemberWallet(helper, objMemberWallet);
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message);
                throw;
            }

            return bolIsResult;
        }

        public bool DeleteMember(int intMemberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_Member_DEL";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", intMemberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool ResetPasswordMember(string strEmail)
        {
            bool bolIsResult = false;
            try
            {
                Sqlhelper helper = new Sqlhelper("", "ConnectionString");
                try
                {
                    bool rs = false;
                    string sql = "SP_Member_ResetPassword";
                    SqlParameter[] pa = new SqlParameter[1];
                    pa[0] = new SqlParameter("@email", strEmail);
                    SqlCommand command = helper.GetCommand(sql, pa, true);
                    int row = command.ExecuteNonQuery();
                    if (row > 0)
                    {
                        rs = true;
                    }
                    return rs;
                }
                catch (Exception ex)
                {
                    WriteErrorLog.WriteLogsToFileText(ex.Message);
                    return false;
                }
                finally
                {
                    helper.destroy();
                }
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message);
                throw;
            }

            return bolIsResult;
        }

        public IEnumerable<LinkResetPassword> GetListLinkResetPassword(DateTime dtCreatedDate, string strEmail)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<LinkResetPassword> lstobjLinkResetPassword = new List<LinkResetPassword>();
                string sql = "SP_GetListLinkResetPasswordFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@Date", dtCreatedDate);
                pa[1] = new SqlParameter("@Email", strEmail);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    LinkResetPassword objLinkResetPassword = new LinkResetPassword();
                    objLinkResetPassword.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                    objLinkResetPassword.Email = reader["Email"].ToString();
                    objLinkResetPassword.ExpireLink = DateTime.Parse(reader["ExpireLink"].ToString());
                    objLinkResetPassword.IPAddress = reader["IPAddress"].ToString();
                    objLinkResetPassword.LinkReset = reader["LinkReset"].ToString();
                    objLinkResetPassword.NumberSend = int.Parse(reader["NumberSend"].ToString());
                    objLinkResetPassword.Status = int.Parse(reader["Status"].ToString());
                    lstobjLinkResetPassword.Add(objLinkResetPassword);

                }
                return lstobjLinkResetPassword;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool InsertLinkResetPassword(LinkResetPassword objLinkResetPassword)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_InsertLinkResetPasswordFE";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@Email", objLinkResetPassword.Email);
                pa[1] = new SqlParameter("@ExpireLink", objLinkResetPassword.ExpireLink);
                pa[2] = new SqlParameter("@IPAddress", objLinkResetPassword.IPAddress);
                pa[3] = new SqlParameter("@LinkReset", objLinkResetPassword.LinkReset);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    //IsResult: result success
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                throw;
            }
        }

        public LinkResetPassword GetDetailLinkResetPassword(string strLinhReset)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            LinkResetPassword objLinkResetPassword = new LinkResetPassword();
            try
            {
                string sql = "SP_GetDetailLinkResetPasswordFE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@LinkReset", strLinhReset);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    objLinkResetPassword.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                    objLinkResetPassword.Email = reader["Email"].ToString();
                    objLinkResetPassword.ExpireLink = DateTime.Parse(reader["ExpireLink"].ToString());
                    objLinkResetPassword.IPAddress = reader["IPAddress"].ToString();
                    objLinkResetPassword.LinkReset = reader["LinkReset"].ToString();
                    objLinkResetPassword.NumberSend = int.Parse(reader["NumberSend"].ToString());
                    objLinkResetPassword.Status = int.Parse(reader["Status"].ToString());

                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                throw;
            }
            return objLinkResetPassword;
        }

        public bool InsertMemberReference(MemberReferenceBO objMemberReferenceBO)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_InsertMemberReferenceFE";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@MemberID", objMemberReferenceBO.MemberID);
                 pa[1] = new SqlParameter("@LinkReference", objMemberReferenceBO.LinkReference);
                pa[2] = new SqlParameter("@MemberIDReference", objMemberReferenceBO.MemberIDReference);
                pa[3] = new SqlParameter("@LockID", objMemberReferenceBO.LockID);
                 pa[4] = new SqlParameter("@Status", objMemberReferenceBO.Status);
                 pa[5] = new SqlParameter("@Amount", objMemberReferenceBO.Amount);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                throw;
            }
        }

        public bool UpdateFreeTicketMember(int intMemberID, int intNumberTicket)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateFreeTicketMemberFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@memberID", intMemberID);
                pa[1] = new SqlParameter("@numberticketfree", intNumberTicket);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<MemberReferenceBO> GetListMemberReferenceByMemberID(int intMemberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<MemberReferenceBO> lstMemberReferenceBO = new List<MemberReferenceBO>();
                string sql = "SP_ListMemberReferenceFEByMemberID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@MemberID", intMemberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                    objMemberReferenceBO.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                    objMemberReferenceBO.LinkReference = reader["LinkReference"].ToString();
                    objMemberReferenceBO.MemberID = int.Parse(reader["MemberID"].ToString());
                    objMemberReferenceBO.MemberIDReference = int.Parse(reader["MemberIDReference"].ToString());
                    objMemberReferenceBO.LockID = reader["LockID"].ToString();
                    objMemberReferenceBO.Status = int.Parse(reader["Status"].ToString());
                    objMemberReferenceBO.Amount = decimal.Parse(reader["Amount"].ToString());
                    lstMemberReferenceBO.Add(objMemberReferenceBO);
                }
                return lstMemberReferenceBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public MemberReferenceBO GetMemberReferenceByMemberID(int intMemberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                string sql = "SP_ListMemberReferenceFEByMemberID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@MemberID", intMemberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    
                    objMemberReferenceBO.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                    objMemberReferenceBO.LinkReference = reader["LinkReference"].ToString();
                    objMemberReferenceBO.MemberID = int.Parse(reader["MemberID"].ToString());
                    objMemberReferenceBO.MemberIDReference = int.Parse(reader["MemberIDReference"].ToString());
                    objMemberReferenceBO.LockID = reader["LockID"].ToString();
                    objMemberReferenceBO.Status = int.Parse(reader["Status"].ToString());
                    objMemberReferenceBO.Amount = decimal.Parse(reader["Amount"].ToString());
                }
                return objMemberReferenceBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<MemberReferenceBO> GetDetailMemberReferenceByIDRef(int intMemberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<MemberReferenceBO> lstMemberReferenceBO = new List<MemberReferenceBO>();
                string sql = "SP_ListDetailsByMemberIDRef";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@MemberID", intMemberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                    objMemberReferenceBO.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                    objMemberReferenceBO.LinkReference = reader["LinkReference"].ToString();
                    objMemberReferenceBO.MemberID = int.Parse(reader["MemberID"].ToString());
                    objMemberReferenceBO.MemberIDReference = int.Parse(reader["MemberIDReference"].ToString());
                    objMemberReferenceBO.LockID = reader["LockID"].ToString();
                    objMemberReferenceBO.Status = int.Parse(reader["Status"].ToString());
                    objMemberReferenceBO.Amount = decimal.Parse(reader["Amount"].ToString());

                    lstMemberReferenceBO.Add(objMemberReferenceBO);
                }
                return lstMemberReferenceBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateMemberReference(MemberReferenceBO objMemberReferenceBO)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_UpdateMemberReferenceFE";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@MemberID", objMemberReferenceBO.MemberID);
                pa[1] = new SqlParameter("@MemberIDReference", objMemberReferenceBO.MemberIDReference);
                pa[2] = new SqlParameter("@LockID", objMemberReferenceBO.LockID);
                pa[3] = new SqlParameter("@Status", objMemberReferenceBO.Status);
                pa[4] = new SqlParameter("@Amount", objMemberReferenceBO.Amount);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                throw;
            }
        }

        public bool CheckMobileExists(string mobile)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckMobileExist";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@mobile", mobile);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool CheckMobileMemberExists(string mobile,string email)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckMobileMemberExist";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@mobile", mobile);
                pa[1] = new SqlParameter("@email", email);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool UpdateInformationMember(MemberBO member, string email)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateInformationMember_FE";
                SqlParameter[] pa = new SqlParameter[6];

                pa[0] = new SqlParameter("@email", email);
                pa[1] = new SqlParameter("@mobile", member.Mobile);
                pa[2] = new SqlParameter("@smsCode", member.SmsCode);
                pa[3] = new SqlParameter("@isIndentifySms", member.IsIndentifySms);
                pa[4] = new SqlParameter("@expireSmSCode", member.ExpireSmSCode);
                pa[5] = new SqlParameter("@totalSmsCodeInput", member.TotalSmsCodeInput);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public int GetTotalSSNSMember(string email,int totalsSMS)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_GetTotalsSMSMember_FE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@email", email);
                pa[2] = new SqlParameter("@totalsSMS", totalsSMS);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                     totalsSMS = int.Parse(reader["TotalSmsCodeInput"].ToString());

                }
                return totalsSMS;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<CountryBO> GetListCountry(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<CountryBO> lstAward = new List<CountryBO>();
                string sql = "SP_GetListCountry_CMS";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CountryBO country = new CountryBO();
                    country.CountryID = int.Parse(reader["CountryID"].ToString());
                    if (!string.IsNullOrEmpty(reader["CountryName"].ToString())) country.CountryName = reader["CountryName"].ToString();
                    if (!string.IsNullOrEmpty(reader["PhoneZipCode"].ToString())) country.PhoneZipCode = reader["PhoneZipCode"].ToString();
                    if (!string.IsNullOrEmpty(reader["IsActive"].ToString())) country.IsActive = int.Parse(reader["IsActive"].ToString());
                    if (!string.IsNullOrEmpty(reader["IsDeleted"].ToString())) country.IsDeleted = int.Parse(reader["IsDeleted"].ToString());
                    if (!string.IsNullOrEmpty(reader["CreatedUser"].ToString())) country.CreatedUser = reader["CreatedUser"].ToString();
                    if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString())) country.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["DeletedDate"].ToString())) country.DeletedDate = DateTime.Parse(reader["DeletedDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["DeletedUser"].ToString())) country.DeletedUser = reader["DeletedUser"].ToString();
                    if (!string.IsNullOrEmpty(reader["UpdatedDate"].ToString())) country.UpdatedDate = DateTime.Parse(reader["UpdatedDate"].ToString());
                    if (!string.IsNullOrEmpty(reader["UpdatedUser"].ToString())) country.UpdatedUser = reader["UpdatedUser"].ToString();
                    if (!string.IsNullOrEmpty(reader["TOTALROWS"].ToString())) country.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstAward.Add(country);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
