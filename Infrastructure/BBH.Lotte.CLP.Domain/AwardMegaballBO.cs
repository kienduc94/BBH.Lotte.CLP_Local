﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    public class AwardMegaballBO
    {
        public int NumberID { get; set; }

        public int AwardID {get;set;}

        public DateTime CreateDate { get; set; }

        public int IsActive { get; set; }
         public int Priority { get; set; }
        public int IsDelete { get; set; }

        public int FirstNumber { get; set; }

        public int SecondNumber { get; set; }

        public int ThirdNumber { get; set; }

        public int FourthNumber { get; set; }

        public int FivethNumber { get; set; }

        public int ExtraNumber { get; set; }

        public string NumberWiner { get; set; }

        public int TotalRecord { get; set; }

        public int TotalAllAward { get; set; }

        public int TotalNewAward { get; set; }
        public string AwardNameEnglish { get; set; }
        public string GenCode { get; set; }
        public decimal JackPot { get; set; }
        public List<WinnerBO> ListWinnerMember { get; set; }
        public int DrawID { get; set; }
        public decimal TotalWon { get; set; }
    }
}
