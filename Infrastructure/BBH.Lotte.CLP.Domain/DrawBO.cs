﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class DrawBO
    {
        [DataMember]
        public int DrawID { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }


        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}
