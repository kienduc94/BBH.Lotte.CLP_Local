﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    public class AccessRightBO
    {
        public int GroupID { get; set; }
        public int AdminID { get; set; }
    }
}
