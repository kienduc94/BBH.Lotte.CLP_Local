﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    public class BookingSummaryBO
    {
        public int Month { get; set; }

        public int TotalTicket { get; set; }

        public int TotalTicketFree { get; set; }
    }
}
