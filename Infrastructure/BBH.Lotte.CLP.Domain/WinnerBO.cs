﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Domain
{
    public class WinnerBO
    {
        public int MemberID { get; set; }
        public string Email { get; set; }
        public string E_Wallet { get; set; }
        //public string NumberValue { get; set; }
        public int FirstNumber { get; set; }

        public int SecondNumber { get; set; }

        public int ThirdNumber { get; set; }

        public int FourthNumber { get; set; }

        public int FivethNumber { get; set; }

        public int ExtraNumber { get; set; }

        public string NumberWiner { get; set; }

        public int TotalRecord { get; set; }
        public string TransactionCode { get; set; }
        public int TotalRecordWinner { get; set; }
        public DateTime OpenDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string AwardNameEnglish { get; set; }
        public int BookingID { get; set; }
        public float JackPot { get; set; }
    }
}
