﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    public class MemberNumberBO
    {
        public int ID { get; set; }
        public int MemberID { get; set; }
        public int NumberID { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }

    }
}
