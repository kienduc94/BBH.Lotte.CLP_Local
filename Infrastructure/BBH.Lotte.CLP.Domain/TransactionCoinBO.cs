﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    public class TransactionCoinBO
    {
        public string TransactionID { get; set; }
        public int WalletAddressID { get; set; }
        public int MemberID { get; set; }
        public float ValueTransaction { get; set; }
        public string QRCode { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public int Status { get; set; }
        public string Note { get; set; }
        public int WalletID { get; set; }
        public int TypeTransactionID { get; set; }
        public string TransactionBitcoin { get; set; }
        public int TotalRecord { get; set; }
        public int TotalAllCoin { get; set; }

        public int TotalNewCoin { get; set; }
    }
}
