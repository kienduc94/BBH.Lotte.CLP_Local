﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class TicketWinningBO
    {
        [DataMember]
        public int TicketWinningID { get; set; }

        [DataMember]
        public int BookingID { get; set; }

        [DataMember]
        public int DrawID { get; set; }

        [DataMember]
        public DateTime BuyDate { get; set; }

        [DataMember]
        public int AwardID { get; set; }

        [DataMember]
        public int FirstNumberBuy { get; set; }

        [DataMember]
        public int SecondNumberBuy { get; set; }

        [DataMember]
        public int ThirdNumberBuy { get; set; }

        [DataMember]
        public int FourthNumberBuy { get; set; }

        [DataMember]
        public int FivethNumberBuy { get; set; }

        [DataMember]
        public int ExtraNumberBuy { get; set; }

        [DataMember]
        public int FirstNumberAward { get; set; }

        [DataMember]
        public int SecondNumberAward { get; set; }

        [DataMember]
        public int ThirdNumberAward { get; set; }

        [DataMember]
        public int FourthNumberAward { get; set; }

        [DataMember]
        public int FivethNumberAward { get; set; }

        [DataMember]
        public int ExtraNumberAward { get; set; }

        [DataMember]
        public int NumberBallGold { get; set; }

        [DataMember]
        public int NumberBallNormal { get; set; }

        [DataMember]
        public decimal AwardValue { get; set; }

        [DataMember]
        public DateTime AwardDate { get; set; }

        [DataMember]
        public int MemberID { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public decimal AwardFee { get; set; }

        [DataMember]
        public decimal JackpotValue { get; set; }

        [DataMember]
        public int WalletID;

        [DataMember]
        public short ClientID;

        [DataMember]
        public short SystemID;

        [DataMember]
        public string SysJackpot_UserID;

        [DataMember]
        public string SysJackpot_Address;

        [DataMember]
        public string SysAward_UserID;

        [DataMember]
        public string SysLoan_UserID;

        [DataMember]
        public string SysLoan_Address;

        [DataMember]
        public string SysGuaranteeFund_UserID;

        [DataMember]
        public string SysGuaranteeFund_Address;
    }

    public class TicketWinningOut
    {
        public int id { get; set; }
        public string email { get; set; }
        public string value { get; set; }
        public string date_time { get; set; }
    }
}
