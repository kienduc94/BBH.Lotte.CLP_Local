﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Domain
{
    public class TransactionPackageBO
    {
        public int TransactionID { get; set; }
        public int MemberID { get; set; }

        public int PackageID { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ExpireDate { get; set; }

        public int Status { get; set; }

        public string PackageName { get; set; }

        public string Email { get; set; }

        public string E_Wallet { get; set; }

        public int TotalRecord { get; set; }

        public string Note { get; set; }

        public string NoteEnglish { get; set; }

        public int TotalAllPackage { get; set; }

        public int TotalNewPackage { get; set; }

        public string TransactionCode { get; set; }
    }
}
