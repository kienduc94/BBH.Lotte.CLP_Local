﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    public class CoinBO
    {
        public string CoidID { get; set; }
        public string CoinName { get; set; }
        public int IsActive { get; set; }
    }
}
