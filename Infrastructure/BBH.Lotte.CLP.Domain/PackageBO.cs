﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Domain
{
    public class PackageBO
    {
        public int PackageID { get; set; }
        public string PackageName { get; set; }

        public double PackageValue { get; set; }

        public DateTime CreateDate { get; set; }

        public int NumberExpire { get; set; }

        public int IsActive { get; set; }

        public int Priority { get; set; }

        public string PackageNameEnglish { get; set; }
    }
}
