﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    public class TransactionPointsBO
    {
        public int TransactionID { get; set; }
        public int MemberID { get; set; }
        public double Points { get; set; }
        public DateTime CreateDate { get; set; }
        public int Status { get; set; }
        public string Email { get; set; }
        public string E_Wallet { get; set; }

        public int TotalRecord { get; set; }

        public string Note { get; set; }
        public string NoteEnglish { get; set; }

        public int TotalAllPoint { get; set; }

        public int TotalNewPoint { get; set; }

        public string TransactionCode { get; set; }
    }
}
