﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class AdminBO
    {
        [DataMember]
        public int AdminID { get; set; }

         [DataMember]
        public string UserName { get; set; }

         [DataMember]
        public string Password { get; set; }

         [DataMember]
        public string Hashkey { get; set; }

         [DataMember]
        public int IsActive { get; set; }

         [DataMember]
        public DateTime CreateDate { get; set; }

         [DataMember]
        public string FullName { get; set; }

         [DataMember]
        public string Email { get; set; }

         [DataMember]
        public string Mobile { get; set; }

         [DataMember]
        public string Address { get; set; }

         [DataMember]
        public string Avatar { get; set; }

         [DataMember]
        public int IsDelete { get; set; }

         [DataMember]
        public int GroupID { get; set; }

        [DataMember]
        public int TotalRecord { get; set; }

        [DataMember]
        public string GroupName { get; set; }

         [DataMember]
        public AdminInfomationBO AdminInfomation { get; set; }

        
    }
}
