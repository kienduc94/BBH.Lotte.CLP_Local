﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    public class NumberGeneralBO
    {
        public int NumberID { get; set; }
        public string NumberValue { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }
        public int Priority { get; set; }

    }
}
