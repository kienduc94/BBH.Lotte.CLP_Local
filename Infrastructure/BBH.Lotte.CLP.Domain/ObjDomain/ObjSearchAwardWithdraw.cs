﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain.ObjDomain
{
    public class ObjSearchAwardWithdraw
    {
        public string RquestMemberID { get; set; }

        public DateTime RequestFromDate { get; set; }

        public DateTime RequestToDate { get; set; }

        public DateTime AwardDate { get; set; }
        public DateTime RequestDate { get; set; }
        public int RequestStatus { get; set; }

        public int StartIndex { get; set; }

        public int EndIndex { get; set; }
    }
}
