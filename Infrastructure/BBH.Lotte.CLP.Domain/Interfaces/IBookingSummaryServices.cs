﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IBookingSummaryServices
    {
        [OperationContract]
        IEnumerable<BookingSummaryBO> ListTicketSummaryByPoint(int year, int isFreeTicket);

        [OperationContract]
        IEnumerable<BookingSummaryBO> ListTicketSummaryByFree(int year, int isFreeTicket);

        [OperationContract]
        IEnumerable<PrizeBO> ListPrize();
    }
}
