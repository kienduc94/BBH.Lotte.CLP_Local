﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IMemberServices
    {
        [OperationContract]
        bool InsertMemberInfomation(MemberInfomationBO member);

        [OperationContract]
        bool UpdateMemberInfomation(MemberInfomationBO member, int memberID);

        [OperationContract]
        bool UpdatePasswordMember(string userName, string password);

        [OperationContract]
        bool UpdatePointsMember(int memberID, int points);

        [OperationContract]
        bool LockAndUnlockMember(int memberID, int isActive);

        [OperationContract]
        MemberInfomationBO GetMemberDetailByID(int memberID);

        [OperationContract]
        MemberInfomationBO GetMemberDetail(string email);

        [OperationContract]
        bool CheckUserNameExists(string userName);

        [OperationContract]
        bool CheckEmailExists(string email);

        [OperationContract]
        IEnumerable<MemberBO> GetListMember(int start, int end);

        [OperationContract]
        IEnumerable<MemberBO> ListAllMember();

        [OperationContract]
        IEnumerable<MemberBO> GetListMemberBySearch(string keyword, DateTime? fromDate, DateTime? toDate, int start, int end);

        [OperationContract]
        IEnumerable<CompanyInformationBO> GetCompanyByMemberID(int memberID);

        [OperationContract]
        ObjResultMessage CountAllMember(ModelRSA objRsa, ref MemberBO objMember);

        [OperationContract]
        int CountMemberBySearch(string keyword);

        [OperationContract]
        IEnumerable<MemberBO> GetMemberByID(int memberID);

  
    }
}
