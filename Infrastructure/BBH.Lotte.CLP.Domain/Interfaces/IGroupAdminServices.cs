﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IGroupAdminSvc" in both code and config file together.
    [ServiceContract]
    public interface IGroupAdminServices
    {
        [OperationContract]
        bool InsertGroupAdmin(GroupAdminBO admin);

        [OperationContract]
        IEnumerable<GroupAdminBO> ListGroupAdmin();

        [OperationContract]
        GroupAdminBO GetGroupAdminDetail(int groupID);

        [OperationContract]
        bool UpdateStatusGroupAdmin(int groupID, int isActive);

        [OperationContract]
        bool UpdateGroupAdmin(int groupID, string groupName);

        [OperationContract]
        bool CheckUserGroupAdminNameExists(string groupName);

    }
}
