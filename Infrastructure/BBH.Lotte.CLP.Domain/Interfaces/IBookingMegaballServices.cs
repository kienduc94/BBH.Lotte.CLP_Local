﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBookingMegaballServices" in both code and config file together.
    [ServiceContract]
    public interface IBookingMegaballServices
    {
        [OperationContract]
        IEnumerable<BookingMegaballBO> ListAllBookingMegaball();

        [OperationContract]
        IEnumerable<BookingMegaballBO> ListAllBookingPage(int start, int end);

        [OperationContract]
        IEnumerable<BookingMegaballBO> ListAllBookingBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int status, int start, int end);

        [OperationContract]
        ObjResultMessage CountAllBookingMegaball(ModelRSA objRsa, ref BookingMegaballBO objBooking);

        [OperationContract]
        bool UpdateStatusBookingMegaball(int bookingID, int status);

        [OperationContract]
        IEnumerable<BookingMegaballBO> GetListMemberBySearch(string keyword, DateTime fromDate, DateTime toDate, int start, int end);

        [OperationContract]
        IEnumerable<BookingMegaballBO> ListAllBookingMegaballByDate(DateTime dtDate);
        [OperationContract]
        IEnumerable<BookingMegaballBO> ListAllBookingMegaballByDraw(int intDrawID);
        [OperationContract]
        decimal GetAwardNotJackpot(string strNumberCheck, string strExtraNumber, int intDrawID);
    }

}
