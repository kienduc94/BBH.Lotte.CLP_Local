﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface ITicketConfigServices
    {
        [OperationContract]
        ObjResultMessage InsertTicketConfig(ModelRSA objRsa);

        [OperationContract]
        ObjResultMessage UpdateTicketConfig(ModelRSA objRsa);

        [OperationContract]
        IEnumerable<TicketConfigBO> ListTicketConfig();
        [OperationContract]
        ObjResultMessage DeleteTicketConfig(ModelRSA objRsa);

        //[OperationContract]
        //ObjResultMessage ListTicketConfig(ModelRSA objRsa, ref List<TicketConfigBO> lstTicketConfig);

        [OperationContract]
        ObjResultMessage GetTicketConfigDetail(ModelRSA objRsa, ref TicketConfigBO objTicketConfig);
        [OperationContract]
        ObjResultMessage LockAndUnlockConfigTicket(ModelRSA objRsa);
        [OperationContract]
        bool CheckCoinID(int configID, string coinID);
    }
}
