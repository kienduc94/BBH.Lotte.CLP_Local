﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IBookingServices
    {
        [OperationContract]
        IEnumerable<BookingBO> ListAllBooking();

        [OperationContract]
        IEnumerable<BookingBO> ListAllBookingPaging(int start, int end);

        [OperationContract]
        IEnumerable<BookingBO> ListAllBookingBySearch(int memberID, DateTime fromDate, DateTime toDate, int status, int start, int end);

        [OperationContract]
        bool UpdateStatusBooking(int bookingID, int status);

        [OperationContract]
        BookingBO CountAllBooking();

    }
}
