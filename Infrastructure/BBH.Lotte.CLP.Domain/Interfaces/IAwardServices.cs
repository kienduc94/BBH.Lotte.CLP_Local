﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IAwardServices
    {
        [OperationContract]
        ObjResultMessage InsertAward(ModelRSA objRsa);

        [OperationContract]
        ObjResultMessage UpdateAward(ModelRSA objRsa);

        [OperationContract]
        ObjResultMessage LockAndUnlockAward(ModelRSA objRsa);

        [OperationContract]
        bool CheckAwardNameEnglishExists(int awardID, string awardNameEnglish);

        [OperationContract]
        IEnumerable<AwardBO> GetListAward(int start, int end);

        [OperationContract]
        IEnumerable<AwardNumberBO> GetListAwardNumber(int start, int end);

        [OperationContract]
        bool InsertAwardNumber(AwardNumberBO award);

        [OperationContract]
        bool UpdateAwardNumber(AwardNumberBO award);

        [OperationContract]
        IEnumerable<AwardNumberBO> GetListAwardNumberByDate(int start, int end, DateTime fromDate, DateTime toDate);

        [OperationContract]
        ObjResultMessage LockAndUnlockAwardNumber(ModelRSA objRsa);

        [OperationContract]
        IEnumerable<WinnerBO> GetListWinner(int start, int end, DateTime fromDate, DateTime toDate);

        [OperationContract]
        AwardBO GetListAwardDetail(int awardID);
    }
}
