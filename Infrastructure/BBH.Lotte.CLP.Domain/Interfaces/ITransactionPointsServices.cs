﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITransactionPointsSvc" in both code and config file together.
    [ServiceContract]
    public interface ITransactionPointsServices
    {
        [OperationContract]
        IEnumerable<TransactionPointsBO> ListTransactionPoint();

        [OperationContract]
        IEnumerable<TransactionPointsBO> ListTransactionPointPaging(int start, int end);

        [OperationContract]
        IEnumerable<TransactionPointsBO> ListTransactionPointBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int status, int start, int end);

        [OperationContract]
        bool UpdateStatusTransaction(int transactionID, int status);

        [OperationContract]
        TransactionPointsBO CountAllPoints();

    }
}
