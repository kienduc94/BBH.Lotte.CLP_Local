﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface ISystemConfigServices
    {
        [OperationContract]
        ObjResultMessage UpdateStatusActive(ModelRSA objRsa);

        [OperationContract]
        IEnumerable<SystemConfigBO> GetListSystemConfig();
    }
}
