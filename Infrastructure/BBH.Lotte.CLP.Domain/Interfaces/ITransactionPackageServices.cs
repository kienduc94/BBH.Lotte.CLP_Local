﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITransactionPackageSvc" in both code and config file together.
    [ServiceContract]
    public interface ITransactionPackageServices
    {
        [OperationContract]
        IEnumerable<TransactionPackageBO> ListAllTransactionPackage(int start, int end);

        [OperationContract]
        IEnumerable<TransactionPackageBO> ListAllTransactionPackageByMember(int memberID, DateTime fromDate, DateTime toDate, int status, int start, int end);

        [OperationContract]
        bool UpdateStatusTransaction(int transactionID, int status);

        [OperationContract]
        PackageBO GetPackageDetail(int packageID);

        [OperationContract]
        TransactionPackageBO CountAllPackage();

    }
}
