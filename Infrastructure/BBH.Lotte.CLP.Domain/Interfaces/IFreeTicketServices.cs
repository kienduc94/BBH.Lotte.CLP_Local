﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IFreeTicketServices
    {
        [OperationContract]
        bool InsertFreeTicket(int intNumberTicket);
    }
}
