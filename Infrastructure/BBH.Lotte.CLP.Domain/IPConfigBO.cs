﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class IPConfigBO
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string IPAddress { get; set; }

        [DataMember]
        public string ServiceDomain { get; set; }

        [DataMember]
        public int IsActive { get; set; }

        [DataMember]
        public int IsDeleted { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}
