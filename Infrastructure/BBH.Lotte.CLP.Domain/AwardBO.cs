﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    public class AwardBO
    {
        public int AwardID { get; set; }
        public string AwardName { get; set; }
        public decimal AwardValue { get; set; }
        public DateTime CreateDate { get; set; }
        public int Priority { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }
        public int TotalRecord { get; set; }
        public string AwardNameEnglish { get; set; }
        public decimal AwardFee { get; set; }
        public int NumberBallGold { get; set; }
        public int NumberBallNormal { get; set; }
        public float AwardPercent { get; set; }
    }
}
