﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Domain
{
    public class ExchangeTicketBO
    {
        public int ExChangeID { get; set; }
        public float PointValue {get;set;}
        public int TicketNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
        public int MemberID { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public DateTime DeleteDate { get; set; }
        public string DeleteUser { get; set; }
        public int TotalRecord { get; set; }
        public string FullNane { get; set; }
        public string CoinID { get; set; }
        public string Email { get; set; }
    }
}
