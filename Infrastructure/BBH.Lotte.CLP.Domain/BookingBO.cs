﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Domain
{
    public class BookingBO
    {
        public int BookingID { get; set; }
        public int MemberID { get; set; }

        public string NumberValue { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }

        public DateTime OpenDate { get; set; }

        public int Status { get; set; }

        public string Email { get; set; }

        public string E_Wallet { get; set; }

        public int TotalRecord { get; set; }

        public string Note { get; set; }
        public string NoteEnglish { get; set; }

        public int TotalAllBooking { get; set; }

        public int TotalNewBooking { get; set; }

        public string TransactionCode { get; set; }
    }
}
