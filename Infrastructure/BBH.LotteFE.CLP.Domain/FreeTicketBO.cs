﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class FreeTicketBO
    {
        [DataMember]
        public int FreeTicketID { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public int Total { get; set; }

        [DataMember]
        public int IsActive { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}
