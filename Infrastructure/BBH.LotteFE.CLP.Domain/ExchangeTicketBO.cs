﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class ExchangeTicketBO
    {
        [DataMember]
        public int ExchangeID { get; set; }
        [DataMember]
        public float PointValue { get; set; }
        [DataMember]
        public float TicketNumber { get; set; }
        [DataMember]
        public int MemberID { get; set; }
        [DataMember]
        public int IsActive { get; set; }
        [DataMember]
        public int IsDelete { get; set; }
        [DataMember]
        public string CreateUser { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]
        public DateTime UpdateDate { get; set; }
        [DataMember]
        public string UpdateUser { get; set; }
        [DataMember]
        public DateTime DeleteDate { get; set; }
        [DataMember]
        public string DeleteUser { get; set; }
        [DataMember]
        public string CoinID { get; set; }
    }
}
