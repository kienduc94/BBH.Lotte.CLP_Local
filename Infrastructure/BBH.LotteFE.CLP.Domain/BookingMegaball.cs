﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BBH.LotteFE.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class BookingMegaball
    {
        [DataMember]
        public int BookingID { get; set; }
        [DataMember]

        public int MemberID { get; set; }
        [DataMember]

        public DateTime CreateDate { get; set; }
        [DataMember]

        public DateTime OpenDate { get; set; }
        [DataMember]

        public int Status { get; set; }
        [DataMember]

        public string TransactionCode { get; set; }
        [DataMember]

        public string Note { get; set; }
        [DataMember]

        public string NoteEnglish { get; set; }
        [DataMember]

        public int FirstNumber { get; set; }
        [DataMember]

        public int SecondNumber { get; set; }
        [DataMember]

        public int ThirdNumber { get; set; }
        [DataMember]

        public int FourthNumber { get; set; }
        [DataMember]

        public int FivethNumber { get; set; }
        [DataMember]

        public int ExtraNumber { get; set; }
        [DataMember]

        public int Quantity { get; set; }
        [DataMember]

        public string Email { get; set; }
        [DataMember]

        public string Bitcoin_Wallet { get; set; }
        [DataMember]

        public int TotalRecord { get; set; }
        [DataMember]

        public double ValuePoint { get; set; }
        [DataMember]
        public int AwardWithdrawStatus { get; set; }

        [DataMember]
        public int DrawID { get; set; }

        [DataMember]
        public int IsFreeTicket { get; set; }

        [DataMember]
        public double ValuesWithdraw { get; set; }
    }
}
