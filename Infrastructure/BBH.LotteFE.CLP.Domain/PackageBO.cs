﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class PackageBO
    {
        [DataMember]
        public int PackageID { get; set; }
        [DataMember]
        public string PackageName { get; set; }
        [DataMember]
        public double PackageValue { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]
        public int NumberExpire { get; set; }
        [DataMember]
        public int IsActive { get; set; }
        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public string PackageNameEnglish { get; set; }

    }
}
