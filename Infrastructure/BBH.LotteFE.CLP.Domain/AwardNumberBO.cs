﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class AwardNumberBO
    {
        [DataMember]
        public int NumberID { get; set; }
        [DataMember]
        public int AwardID { get; set; }
        [DataMember]
        public string NumberValue { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]
        public int IsActive { get; set; }
        [DataMember]
        public int IsDelete { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string AwardName { get; set; }
        [DataMember]
        public double AwardValue { get; set; }
        [DataMember]
        public string StationName { get; set; }

        [DataMember]
        public string AwardNameEnglish { get; set; }

        [DataMember]
        public string StationNameEnglish { get; set; }

    }
}
