﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class BookingBO
    {
        [DataMember]
        public int BookingID { get; set; }
        [DataMember]
        public int MemberID { get; set; }
        [DataMember]
        public string NumberValue { get; set; }
        [DataMember]
        public int Quantity { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]

        public DateTime OpenDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string E_Wallet { get; set; }
        [DataMember]
        public string TransactionCode { get; set; }

        [DataMember]
        public int TotalRecord { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public string NoteEnglish { get; set; }

    }
}
