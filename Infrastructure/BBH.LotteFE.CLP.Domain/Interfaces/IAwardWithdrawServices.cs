﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IAwardWithdrawServices
    {
        [OperationContract]
        ObjResultMessage InsertAwardWithdraw(AwardWithdrawBO awardWithdraw);

        [OperationContract]
        ObjResultMessage GetAwardWithdrawDetail(string requestMemberID, DateTime awardDate,int bookingID, ref AwardWithdrawBO objAwardWithdraw);

        [OperationContract]
         IEnumerable<AwardWithdrawBO> GetAllListAward();

        [OperationContract]
        ObjResultMessage UpdateStatusAwardWithdraw(ModelRSA objRsa);
    }
}
