﻿using BBC.Core.Database;
using BBH.Lotte.CLP.Shared;
using BBH.LotteFE.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.LotteFE.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IBookingMegaballServices
    {
        [OperationContract]
        ObjResultMessage InsertBooking(BookingMegaball booking);

        [OperationContract]
        IEnumerable<BookingMegaball> ListTransactionBookingByMember(int memberID, int start, int end);

        [OperationContract]
        IEnumerable<BookingMegaball> ListTransactionBookingByMemberPaging(int memberID, int start, int end);

        [OperationContract]
        IEnumerable<BookingMegaball> ListTransactionBookingBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end);

        [OperationContract]
        IEnumerable<BookingMegaball> ListTransactionBookingByDate(DateTime fromDate, DateTime toDate);

        [OperationContract]
        bool UpdateStatusBooking(int bookingID, int status);

        [OperationContract]
        int CountMemberBuyNumber(int memberID, int firstNumber, int secondNumber, int thirdNumber, int fourthNumber, int fivethNumber, DateTime fromDate, DateTime toDate);
        [OperationContract]
        IEnumerable<BookingMegaball> ListTransactionBookingOrderByDate(int start, int end);
        [OperationContract]
        ObjResultMessage InsertListBooking(ModelRSA objRsa, double doublePoint, int intNumberID = 0, double dblTotalJackPot = 0);
        [OperationContract]
        float SumValuePoint();
        [OperationContract]
        IEnumerable<BookingMegaball> ListBookingByAwardDate(int intAwardNumber);
        [OperationContract]
        IEnumerable<BookingMegaball> ListBookingMegaballByDate(DateTime dtmDate);

        [OperationContract]
         List<BookingMegaball> ListTransactionBookingWinnigByMemberSearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end);
        [OperationContract]
        IEnumerable<BookingMegaball> ListAllBookingMegaballByDraw(int intDrawID);

        [OperationContract]
        IEnumerable<BookingMegaball> ListBookingByDraw(int intDrawID, int start, int end);

        [OperationContract]
         List<BookingMegaball> ListAllBooking();
        [OperationContract]
         IEnumerable<AwardBO> GetAllListAward();

        [OperationContract]
        IEnumerable<BookingMegaball> SP_ListBookingByDraw_FE(int intDrawID);

        [OperationContract]
        int InsertTicketNumberTimeLog(string strEmail, int intTicketNumber, DateTime dtmCreatedDate);
    }
}
