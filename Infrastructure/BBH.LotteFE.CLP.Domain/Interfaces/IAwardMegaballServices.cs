﻿using BBH.LotteFE.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.LotteFE.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IAwardMegaballServices
    {
        [OperationContract]
        IEnumerable<AwardMegaball> GetListAward(int start, int end, DateTime fromDate, DateTime toDate);
        [OperationContract]
        IEnumerable<AwardMegaball> GetListAwardOrderByDate();
        [OperationContract]
        IEnumerable<AwardMegaball> GetListAwardByDay(DateTime fromDate, DateTime toDate);

        /// <summary>
        /// Created date: 15.01.2018
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        AwardMegaball GetLotteryResult(int intMemberID, ref double dblPoint);
        [OperationContract]
        IEnumerable<AwardMegaball> GetAllListAward();
        [OperationContract]
        IEnumerable<AwardMegaball> GetListAwardByDate(DateTime date);
        [OperationContract]
        IEnumerable<AwardMegaball> GetListAwardMegaball(int start, int end);
        [OperationContract]
        AwardMegaball GetAwardMegaballByID(int intNumberID);
        [OperationContract]
        AwardMegaball GetAwardMegabalByDrawID(int intDrawID);
    }
}
