﻿using BBH.LotteFE.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.LotteFE.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IMemberServices
    {
        [OperationContract]
        bool InsertMember(MemberBO objMemberBO, ref int intMemberID);
        [OperationContract]
        bool UpdatePasswordMember(string email, string password);
        [OperationContract]
        bool UpdateNewPasswordMember(string email, string oldpassword, string newpassword);

        [OperationContract]
        bool LockAndUnlockMember(int memberID, int isActive);

        [OperationContract]
        MemberBO GetMemberDetailByID(int memberID);

        [OperationContract]
        MemberBO GetMemberDetailByEmail(string strEmail);

        [OperationContract]
        double GetPointsMember(int memberID);

        [OperationContract]
        MemberBO LoginMember(string userName, string password);

        [OperationContract]
        bool CheckUserNameExists(string strUserName);

        [OperationContract]
        bool CheckMobileExists(string mobile);

        [OperationContract]
        bool CheckMobileMemberExists(string mobile, string email);

        [OperationContract]
        bool CheckE_WalletExists(string strE_Wallet);

        [OperationContract]
        bool CheckEmailExists(string strEmail);

        [OperationContract]
        IEnumerable<MemberInfomationBO> GetListMember();

        [OperationContract]
        IEnumerable<MemberInfomationBO> GetListMemberInfomation(int start, int end);

        [OperationContract]
        bool UpdatePointsMember(int memberID, int points);

        [OperationContract]
        int CountAllMember();

        [OperationContract]
        MemberInfomationBO GetMemberDetail(string email);

        [OperationContract]
        bool InsertMemberWallet(MemberWalletBO objMemberWalletBO);

        [OperationContract]
        bool UpdatePointsMemberFE(int memberID, double points);

        [OperationContract]
        int InsertMemberAfterLoginFB(ref MemberBO objMember);

        [OperationContract]
        bool DeleteMember(int intMemberID);

        [OperationContract]
        bool ResetPasswordMember(string strEmail);

        [OperationContract]
        IEnumerable<LinkResetPassword> GetListLinkResetPassword(DateTime dtCreatedDate, string strEmail);

        [OperationContract]
        bool InsertLinkResetPassword(LinkResetPassword objLinkResetPassword);

        [OperationContract]
        LinkResetPassword GetDetailLinkResetPassword(string strLinkReset);

        [OperationContract]
        bool InsertMemberReference(MemberReferenceBO objMemberReferenceBO);

        //[OperationContract]
        //MemberReferenceBO GetDetailMemberReferenceBO(string strLinkReset);

        [OperationContract]
        bool UpdateFreeTicketMember(int intMemberID, int intNumberTicket);

        [OperationContract]
        IEnumerable<MemberReferenceBO> GetListMemberReferenceByMemberID(int intMemberID);
        [OperationContract]
        IEnumerable<MemberReferenceBO> GetDetailMemberReferenceByIDRef(int intMemberIDRef);
        [OperationContract]
        bool UpdateMemberReference(MemberReferenceBO objMemberReferenceBO);

        [OperationContract]
        bool UpdateInformationMember(MemberBO member, string email);

        [OperationContract]
        int GetTotalSSNSMember(string email, int totalsSMS);

        [OperationContract]
        MemberReferenceBO GetMemberReferenceByMemberID(int intMemberID);

        [OperationContract]
        IEnumerable<CountryBO> GetListCountry(int start, int end);
    }
}
