﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface ITicketConfigServices
    {
        [OperationContract]
        IEnumerable<TicketConfigBO> GetListTicketConfig();
    }
}
