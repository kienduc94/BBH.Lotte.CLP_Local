﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface ITicketWinningServices
    {
        [OperationContract]
        IEnumerable<TiketWinningBO> GetListTicketWinningByMemberID(int intMemberID, int start, int end);
        [OperationContract]
        IEnumerable<TiketWinningBO> GetListTicketWinningBySearch(int intMemberID, int start, int end,DateTime? startdate, DateTime? enddate);
        [OperationContract]
        TiketWinningBO GetListTicketWinningByBookingID(int BookingID, decimal awardvalue, decimal awardfee, int proprity, int intDrawID, int intMemberID);
    }
}
