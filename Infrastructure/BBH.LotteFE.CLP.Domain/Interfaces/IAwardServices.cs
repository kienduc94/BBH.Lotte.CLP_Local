﻿using BBH.LotteFE.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.LotteFE.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IAwardServices
    {
        [OperationContract]
        bool InsertAward(AwardBO award);

        [OperationContract]
        bool UpdateAward(AwardBO award);

        [OperationContract]
        bool LockAndUnlockAward(int awardID, int isActive);

        [OperationContract]
        IEnumerable<AwardBO> GetListAward(int start, int end);

        [OperationContract]
        bool InsertAwardNumber(AwardNumberBO award);

        [OperationContract]
        IEnumerable<AwardNumberBO> GetListAwardByDate(int start, int end, DateTime fromDate, DateTime toDate);

        [OperationContract]
        IEnumerable<AwardBO> GetAllListAward();

    }
}
