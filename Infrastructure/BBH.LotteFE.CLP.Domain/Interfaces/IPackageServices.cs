﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.LotteFE.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IPackageServices
    {
        [OperationContract]
        bool InsertTransactionPackage(TransactionPackageBO transaction);

        //IEnumerable<TransactionPackageBO> ListTransactionPackageByMember(int memberID);
        [OperationContract]
        IEnumerable<TransactionPackageBO> ListTransactionPackageByMember(int memberID, int start, int end);
        [OperationContract]
        IEnumerable<TransactionPackageBO> ListTransactionPackageBySearch(int memberID, DateTime fromDate, DateTime toDate, int start, int end);
        [OperationContract]
        IEnumerable<TransactionPackageBO> ListTransactionPackageActiveByMember(int memberID);
        [OperationContract]
        bool CheckExpirePackage(int memberID, DateTime dateNow);
        [OperationContract]
        IEnumerable<PackageBO> ListAllPackage();
    }
}
