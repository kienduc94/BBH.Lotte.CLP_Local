﻿using BBH.LotteFE.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.LotteFE.CLP.Domain.Interfaces
{
    [ServiceContract]
    public interface IPointsServices
    {
        [OperationContract]
        bool InsertTransactionPoint(TransactionPointsBO transaction);



        [OperationContract]
        IEnumerable<TransactionPointsBO> ListTransactionPointByMember(int memberID, int start, int end);

        [OperationContract]
        IEnumerable<TransactionPointsBO> ListTransactionPointBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end);
        
        [OperationContract]
        ExchangePointBO ListExchangePointByMember(int memberID);
        [OperationContract]
        ExchangeTicketBO ListExchangeTicketByMember(int memberID, string strCoinID);
        [OperationContract]
        bool InsertTransactionCoin(TransactionCoinBO transaction); 
        /// <summary>
        
        /// Edited date: 12.01.2018
        /// Description: Thay DateTime -> DateTime?
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        [OperationContract]
        IEnumerable<TransactionCoinBO> ListTransactionWalletBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end);
        [OperationContract]
        IEnumerable<TransactionCoinBO> ListTransactionWalletByMember(int memberID, int start, int end);
        [OperationContract]
        bool CheckExistTransactionBitcoin(string strTransactionID, int intMemberID);

        /// <summary>
        
        /// Created date: 11.01.2018
        /// Description: Load Info Point Wallet Address
        /// </summary>
        /// <param name="intMemberID"></param>
        /// <returns></returns>
        [OperationContract]
        MemberWalletBO LoadInfoPointWallet(int intMemberID);
    }
}
