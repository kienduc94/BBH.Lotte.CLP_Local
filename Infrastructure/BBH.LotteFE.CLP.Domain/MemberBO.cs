﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Domain
{
    /// <summary>
   
    /// Edited date: 18.01.2018
    /// Description: IsUserType && UserTypeID
    /// </summary>
    [DataContract]
    [Serializable]
    public class MemberBO
    {
        [DataMember]
        public int MemberID { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string E_Wallet { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]
        public int IsActive { get; set; }
        [DataMember]
        public int IsDelete { get; set; }
        [DataMember]
        public double Points { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public int Gender { get; set; }
        [DataMember]
        public DateTime? Birthday { get; set; }
        [DataMember]
        public int IsCompany { get; set; }
        //[DataMember]
        //public string WalletAddress { get; set; }
        //[DataMember]
        //public string RippleAddress { get; set; }
        //[DataMember]
        //public string SerectKeyRipple { get; set; }

        /// <summary>
        /// 0: Login normal system RippleLot, 1: Login with facebook, 2: Login with google+
        /// </summary>
        [DataMember]
        public int IsUserType { get; set; }

        /// <summary>
        /// FacebookID or GoogleID
        /// </summary>
        [DataMember]
        public string UserTypeID { get; set; }
        [DataMember]
        public string LinkLogin { get; set; }
        [DataMember]
        public int NumberTicketFree { get; set; }
        [DataMember]
        public string LinkReference { get; set; }

        [DataMember]
        public string SmsCode { get; set; }

        [DataMember]
        public int IsIndentifySms { get; set; }
        [DataMember]
        public DateTime ExpireSmSCode { get; set; }
        [DataMember]
        public int TotalSmsCodeInput { get; set; }
    }
}
