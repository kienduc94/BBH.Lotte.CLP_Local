﻿using System;
using System.Runtime.Serialization;
namespace BBH.LotteFE.CLP.Domain
{
    [DataContract]
    [Serializable]
    public class MemberWalletBO
    {
        [DataMember]
        public int WalletID { get; set; }
        [DataMember]
        public int MemberID { get; set; }
        [DataMember]
        public string MasterKey { get; set; }
        [DataMember]
        public int IndexWallet { get; set; }
        [DataMember]
        public float NumberCoin { get; set; }
        [DataMember]
        public int IsActive { get; set; }
        [DataMember]
        public string CreateUser { get; set; }
        [DataMember]
        public DateTime UpdateDate { get; set; }
        [DataMember]
        public string UpdateUser { get; set; }
        [DataMember]
        public DateTime DeleteDate { get; set; }
        [DataMember]
        public string DeleteUser { get; set; }
        [DataMember]
        public int IsDelete { get; set; }
        [DataMember]
        public string WalletAddress { get; set; }
        [DataMember]
        public string RippleAddress { get; set; }
        [DataMember]
        public string SerectKeyRipple { get; set; }

        [DataMember]
        public double Points { get; set; }
    }
}
