﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.LotteFE.CLP.Domain
{
    public class AwardMegaball
    {
        public int NumberID { get; set; }
        public int AwardID { get; set; }
        public DateTime? CreateDate { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }

        public int FirstNumber { get; set; }

        public int SecondNumber { get; set; }

        public int ThirdNumber { get; set; }

        public int FourthNumber { get; set; }
        public int FivethNumber { get; set; }
        public int ExtraNumber { get; set; }

        public string AwardName { get; set; }

        public double AwardValue { get; set; }
        public int Priority { get; set; }
        public string GenCode { get; set; }
        public decimal JackPot { get; set; }
        public int TotalRecord { get; set; }
        public int DrawID { get; set; }
        public decimal TotalWon { get; set; }
    }
}
