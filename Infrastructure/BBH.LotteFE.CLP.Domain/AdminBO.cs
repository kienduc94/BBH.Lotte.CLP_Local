﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Domain
{
    public class AdminBO
    {
        public int AdminID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Hashkey { get; set; }
        public int IsVisible { get; set; }
        public DateTime CreateDate { get; set; }
        public AdminInfomationBO AdminInfomation { get; set; }
    }
}
