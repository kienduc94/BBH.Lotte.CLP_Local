/*
 Navicat Premium Data Transfer

 Source Server         : 151.106.53.251 (beta)
 Source Server Type    : MySQL
 Source Server Version : 50560
 Source Host           : 151.106.53.251:3306
 Source Schema         : bbccryptocurrencywallet

 Target Server Type    : MySQL
 Target Server Version : 50560
 File Encoding         : 65001

 Date: 27/07/2018 11:15:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `UserID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Total` decimal(18, 5) NULL DEFAULT 0.00000,
  `Available` decimal(18, 5) NULL DEFAULT 0.00000,
  `InBlockChain` decimal(18, 5) NULL DEFAULT 0.00000,
  `InLock` decimal(18, 5) NULL DEFAULT 0.00000,
  `InInternalDeposit` decimal(18, 5) NULL DEFAULT 0.00000,
  `InWithdraw` decimal(18, 5) NULL DEFAULT 0.00000
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `AddressId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SystemId` smallint(6) NOT NULL,
  `ClientId` smallint(6) NOT NULL,
  `UserId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CurrencyCode` smallint(6) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UpdatedDate` datetime NULL DEFAULT NULL,
  `UpdatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Total` decimal(18, 10) NULL DEFAULT 0.0000000000,
  `Available` decimal(18, 10) NULL DEFAULT 0.0000000000,
  `InBlockChain` decimal(18, 5) NULL DEFAULT 0.00000,
  `InLock` decimal(18, 5) NULL DEFAULT 0.00000,
  `InInternalDeposit` decimal(18, 5) NULL DEFAULT 0.00000,
  `InWithdraw` decimal(18, 5) NULL DEFAULT 0.00000,
  `Status` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '1: Active, 2: Locked, 3: TemporaryUnlock, 4: TemporaryLock',
  PRIMARY KEY (`AddressId`, `SystemId`) USING BTREE,
  INDEX `FK_Address_User`(`UserId`, `SystemId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('mfgsJiTPiRgwpxacpwitdWcrWg2iDKZV4H', 1, 1, 'huy36391@gmail.com', NULL, 1, '2018-07-22 15:18:33', 'huy36391@gmail.com', NULL, NULL, 0.8984000000, 0.8984000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mfsVevnjUyTQrSaNrzBbWUYNA1XyZZFT9Y', 1, 1, 'dinhthihanh07@gmail.com', NULL, 1, '2018-07-24 10:17:29', 'dinhthihanh07@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mfXkcp1CgC1eaqgqQKEEbyCUcagWeJGmik', 1, 1, 'quanghaupro79@gmail.com', NULL, 1, '2018-07-21 00:28:20', 'quanghaupro79@gmail.com', NULL, NULL, 0.9980000000, 0.9980000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mgCRmgjRxvHJwMgYnvcR3w5JQzmxktPuks', 1, 1, 'doanthithunguyet4@gmail.com', NULL, 1, '2018-07-23 14:04:35', 'doanthithunguyet4@gmail.com', NULL, NULL, 0.9986000000, 0.9986000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mgcywtxYCRe2gHCqk5vtAiBbttbvQE35v4', 1, 1, 'neyhai2012@gmail.com', NULL, 1, '2018-07-18 13:01:26', 'neyhai2012@gmail.com', NULL, NULL, 0.9980000000, 0.9980000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mgM4r76W8BRiYQMYezwScFZaLALTuQnW43', 1, 1, 'tuanjapan6789@gmail.com', NULL, 1, '2018-07-18 09:40:36', 'tuanjapan6789@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mgXrncAbs1XSzmYAvUFoRfQs9kC5p3euuY', 1, 1, 'sysJackPot', 'Address sysJackPot', 1, '2018-07-02 04:14:02', 'administrator', NULL, NULL, 0.0000000000, 0.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mgy4XqXpVyw8M2Hy2jg6P34tcADWpvMm2y', 1, 1, 'sysProgressivePrize', 'Address sysProgressivePrize', 1, '2018-07-02 04:06:36', 'administrator', NULL, NULL, 0.1462600000, 0.1462600000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mhJSB7P9vjKjdkKK7ZHPQoZqK81NEuPxhu', 1, 1, 'trandinhson96@gmail.com', NULL, 1, '2018-07-20 14:56:51', 'trandinhson96@gmail.com', NULL, NULL, 0.9958000000, 0.9958000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mhKifZCU5jj8zPjp369X8UihwH7yPrSdtW', 1, 1, 'userBTC', 'Address userBTC', 1, '2018-07-02 04:14:49', 'administrator', NULL, NULL, 0.0000000000, 0.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mhkMLyTauJgmjSptZSzYoMSX79mFjkyCV3', 1, 1, 'buiducquyen1983@gmail.com', NULL, 1, '2018-07-25 02:42:11', 'buiducquyen1983@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mi9c8BMi9QUgQ8wdfJFEp7VWU7wKHHEZho', 1, 1, 'sysFeeTxn', 'Address Fee Txn', 1, '2018-06-29 07:29:51', 'administrator', NULL, NULL, 0.0015345100, 0.0015345100, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('miAKb3EcGahwLKWW3y3b7YJfjvHydYHbX4', 1, 1, 'vuthanghy89@gmail.com', NULL, 1, '2018-07-18 07:01:49', 'vuthanghy89@gmail.com', NULL, NULL, 0.9994000000, 0.9994000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mibuYaFxrJzUDLQGwBoGcXKBPaNnfxYZYY', 1, 1, 'xuanan10@gmail.com', NULL, 1, '2018-07-17 04:37:22', 'xuanan10@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mibwvjcvTkWvovddD6NKoqpDjqanoCLSvN', 1, 1, 'manhthuyen1997@gmail.com', NULL, 1, '2018-07-17 02:11:47', 'manhthuyen1997@gmail.com', NULL, NULL, 0.9950000000, 0.9950000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mid7hc2rzzqYyo2Uh2gsrBsxpDYbFSh3KT', 1, 1, 'huynhminhduc041097@gmail.com', NULL, 1, '2018-07-19 03:49:30', 'huynhminhduc041097@gmail.com', NULL, NULL, 12.0098800000, 12.0098800000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('miJ1kLumayBhPmNguzpZy376thUCaSFx2P', 1, 1, 'dany.alliat@gmail.com', NULL, 1, '2018-07-17 01:46:54', 'dany.alliat@gmail.com', NULL, NULL, 0.9384000000, 0.9384000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', 1, 1, 'vinhnq@geovietnam.com', NULL, 1, '2018-07-21 09:53:00', 'vinhnq@geovietnam.com', NULL, NULL, 0.9654000000, 0.9654000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('miVcyRgJaLHWuYTiMoUGVgtat8EiA8u9gM', 1, 1, 'hoang0977671567@gmail.com', NULL, 1, '2018-07-18 12:44:44', 'hoang0977671567@gmail.com', NULL, NULL, 0.9994000000, 0.9994000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mjBw15J6dUmwmedGHGBwurk1b8rdVixcp2', 1, 1, 'kavantrant93@gmail.com', NULL, 1, '2018-07-26 02:39:37', 'kavantrant93@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mjiDpwaiGZS4eAKYq1VSzz4N8nbezoFvJo', 1, 1, 'luong.trungnhan2812@gmail.com', NULL, 1, '2018-07-16 03:29:27', 'luong.trungnhan2812@gmail.com', '2018-07-18 02:42:09', 'BTCNetwork', 1.1343750000, 1.1343750000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mjjrrJVjwMfkWheMcZSf1HdQijnSN9BjcX', 1, 1, 'phuongtn6888@gmail.com', NULL, 1, '2018-07-17 09:01:21', 'phuongtn6888@gmail.com', NULL, NULL, 0.9988000000, 0.9988000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mjKv2FhU3s8Xz8SWyh2c3AMCTZdvrvaMNJ', 1, 1, 'duonghaison1970@gmail.com', NULL, 1, '2018-07-17 08:02:09', 'duonghaison1970@gmail.com', NULL, NULL, 0.9990000000, 0.9990000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mjoef5D4L4dkQXmhvNBYszHa9Kseetp8sc', 1, 1, 'tripledxxl@gmail.com', NULL, 1, '2018-07-18 20:45:16', 'tripledxxl@gmail.com', NULL, NULL, 0.9978000000, 0.9978000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mk5LQSdNBuiqmRFnyG1j1HMFTyHKMb5kcS', 1, 1, 'nguyenvantuy0204@gmail.com', NULL, 1, '2018-07-22 10:00:27', 'nguyenvantuy0204@gmail.com', NULL, NULL, 0.9920000000, 0.9920000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', 1, 1, 'trungngan.fca@gmail.com', NULL, 1, '2018-07-20 08:01:18', 'trungngan.fca@gmail.com', NULL, NULL, 0.8922000000, 0.8922000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mkBwwtpHspnavYXgSVmKZWB3WfY5uiYvwu', 1, 1, 'ogunyemi1409@gmail.com', NULL, 1, '2018-07-15 03:10:01', 'ogunyemi1409@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mkFAtr3tYY6DWJraWVtV2U28geA1CFEX3X', 1, 1, 'viethungpdu90@gmail.com', NULL, 1, '2018-07-17 02:32:44', 'viethungpdu90@gmail.com', NULL, NULL, 0.8790000000, 0.8790000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mkkusAPvz6HR4pbWKdLE8bjDxqrMQNT3zs', 1, 1, 'cuarangmuoi27@gmail.com', NULL, 1, '2018-07-17 06:17:11', 'cuarangmuoi27@gmail.com', NULL, NULL, 0.9174000000, 0.9174000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', 1, 1, 'fredhlordze103@gmail.com', NULL, 1, '2018-07-16 22:20:07', 'fredhlordze103@gmail.com', NULL, NULL, 0.9882000000, 0.9882000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mkQqxpTWWyUVjJvRAhmvm3t63ioj75mjMP', 1, 1, 'thaingocthoai1234@gmail.com', NULL, 1, '2018-07-18 07:37:38', 'thaingocthoai1234@gmail.com', NULL, NULL, 0.9786000000, 0.9786000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mktssYk5cNk8ytzx3k1gM4DAYK5ATEwKn2', 1, 1, 'duy.duong@bigbrothers.gold', NULL, 1, '2018-07-15 01:31:56', 'duy.duong@bigbrothers.gold', '2018-07-16 04:49:31', 'BTCNetwork', 1.0021484300, 1.0021484300, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mm8jdfeXT1PNrTF99LpDog13oLgB6E9JHp', 1, 1, 'buidunghn83@gmail.com', NULL, 1, '2018-07-18 15:31:58', 'buidunghn83@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mmaqE3i7T2B8aQtrDKajse5vuiHBuPNevu', 1, 1, 'phuctd222@gmail.com', NULL, 1, '2018-07-23 11:20:43', 'phuctd222@gmail.com', NULL, NULL, 0.9995000000, 0.9995000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', 1, 1, 'nghiabtc888@gmail.com', NULL, 1, '2018-07-26 17:15:50', 'nghiabtc888@gmail.com', NULL, NULL, 0.5142000000, 0.5142000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mmrGM9Djrqi5mjHBPikxNm5VUn3APtwBoq', 1, 1, 'thanhlam6618@gmail.com', NULL, 1, '2018-07-18 13:38:34', 'thanhlam6618@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mmroEqY7mWr3ohCKQZNJ6PL3Hk77AEz5h5', 1, 1, 'nguyenle130690@gmail.com', NULL, 1, '2018-07-19 15:54:00', 'nguyenle130690@gmail.com', NULL, NULL, 1.5065000000, 1.5065000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mn4nCP7fxfJyZtaYVfVxN7tVASQKoVnN1E', 1, 1, 'ryanchau2016@gmail.com', NULL, 1, '2018-07-21 10:14:43', 'ryanchau2016@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mnEXMijjAudS9jbG6AZF5hK9CMnwhi8Gnu', 1, 1, 'mrkhanh831@gmail.com', NULL, 1, '2018-07-20 16:00:46', 'mrkhanh831@gmail.com', NULL, NULL, 0.9976000000, 0.9976000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mnfe7BigPEgTGaurWSHPqLNykBBKHqyxrA', 1, 1, 'hieplandcoin.tn@gmail.com', NULL, 1, '2018-07-17 08:58:16', 'hieplandcoin.tn@gmail.com', NULL, NULL, 0.9972000000, 0.9972000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mnRWTbEWcRQ3jCHWNbx9SCgBLf4QiEhyX1', 1, 1, 'pardo422@gmail.com', NULL, 1, '2018-07-26 11:31:54', 'pardo422@gmail.com', NULL, NULL, 0.0000000000, 0.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mnvEpNbvjFKBcKWSsakAHDYuwGHLVqrvzM', 1, 1, 'minhquan060917@gmail.com', NULL, 1, '2018-07-19 19:03:59', 'minhquan060917@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mnYQtciF6NFG4oAFR24KfuDTw8KwxRQf3u', 1, 1, 'phim.ngo@bigbrothers.gold', NULL, 1, '2018-07-16 02:11:25', 'phim.ngo@bigbrothers.gold', NULL, NULL, 0.9992000000, 0.9992000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mnZq9TCKeAmd1gSCS543k3BEdmWfjdZoLa', 1, 1, 'kienduc94@gmail.com', NULL, 1, '2018-07-14 05:27:18', 'kienduc94@gmail.com', '2018-07-14 07:29:00', 'BTCNetwork', 0.9025971300, 0.9025937500, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mo2PmS175o6Ri2PKR6WMEkfpupoMFZGNbp', 1, 1, 'giangitman@gmail.com', NULL, 1, '2018-07-23 08:42:26', 'giangitman@gmail.com', NULL, NULL, 0.9996000000, 0.9996000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('moHQynVxdQW6dvno6Lb5HwSuvCvqCsZ8JR', 1, 1, 'fsuphihung@gmail.com', NULL, 1, '2018-07-17 00:02:22', 'fsuphihung@gmail.com', NULL, NULL, 0.9988000000, 0.9988000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('moKCSBMxWE86Qtojno9Mza4ghaeTgucS5P', 1, 1, 'nhan.luong@bigbrothers.gold', NULL, 1, '2018-07-16 06:14:46', 'nhan.luong@bigbrothers.gold', NULL, NULL, 0.9770000000, 0.9770000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', 1, 1, 'chuyengiatrimunpuskin@gmail.com', NULL, 1, '2018-07-20 17:48:02', 'chuyengiatrimunpuskin@gmail.com', NULL, NULL, 0.7343000000, 0.7343000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('moQF9NczNegDDFKsfFM8hRrsr8Rm7GchfW', 1, 1, 'nguyenlytruongson@gmail.com', NULL, 1, '2018-07-14 06:32:09', 'nguyenlytruongson@gmail.com', NULL, NULL, 0.9813000000, 0.9813000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('moRXLioFXZWiZEXQsp93feAWPuuCtX5wW5', 1, 1, 'bhoangson86@gmail.com', NULL, 1, '2018-07-19 15:40:13', 'bhoangson86@gmail.com', NULL, NULL, 0.9994000000, 0.9994000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mp7k5DQvtxavh1DU2ScMCrRTqBQeZYQQRJ', 1, 1, 'thanhhungt31@gmail.com', NULL, 1, '2018-07-17 13:16:03', 'thanhhungt31@gmail.com', NULL, NULL, 0.9984000000, 0.9984000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mp8Ffun3KMxmrfEwusa9EWUcmz1P7c8CZN', 1, 1, 'phamdung1080@gmail.com', NULL, 1, '2018-07-20 09:34:36', 'phamdung1080@gmail.com', NULL, NULL, 0.9980000000, 0.9980000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mpcXH5rkRjJh3kidfBAXGTygk9kVkzXsxt', 1, 1, 'manhhainguyen.fnc@gmail.com', NULL, 1, '2018-07-26 08:20:22', 'manhhainguyen.fnc@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', 1, 1, 'hathanhpc20@gmail.com', NULL, 1, '2018-07-13 11:01:25', 'hathanhpc20@gmail.com', '2018-07-14 06:27:22', 'accouting', 0.7029067800, 0.7029000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mpQRw8opEvw56g7ynd8H8eALatho6RKi3y', 1, 1, 'k.duchappy@gmail.com', NULL, 1, '2018-07-14 07:13:31', 'k.duchappy@gmail.com', NULL, NULL, 0.9994000000, 0.9994000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mpXHwF5MKv3UGtdgUFpQQgPjXQ4WRmwuaf', 1, 1, 'tuyendinhhb@gmail.com', NULL, 1, '2018-07-20 09:38:12', 'tuyendinhhb@gmail.com', NULL, NULL, 0.9968000000, 0.9968000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mq4FKuVT3oKmyAf5qRCbahuSzHCg84TXXq', 1, 1, 'ngonhaidang93boxing@gmail.com', NULL, 1, '2018-07-18 11:40:36', 'ngonhaidang93boxing@gmail.com', NULL, NULL, 0.9960000000, 0.9960000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mq6oeWGQpg7wcNKycxaN8VrWkKmybHsn6S', 1, 1, 'nghiemductrung0612@gmail.com', NULL, 1, '2018-07-17 12:29:38', 'nghiemductrung0612@gmail.com', NULL, NULL, 0.9990000000, 0.9990000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mqJtYUfFTEojpGDFWnbtbAjQsAE5w2aBas', 1, 1, 'danielmokobi35@gmail.com', NULL, 1, '2018-07-24 10:14:04', 'danielmokobi35@gmail.com', NULL, NULL, 0.9988000000, 0.9988000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mqpYrS6fK9Uq28PZN9RUXqAc8hoY2y7RZY', 1, 1, 'nguyenly796@gmail.com', NULL, 1, '2018-07-18 10:11:38', 'nguyenly796@gmail.com', NULL, NULL, 0.9670000000, 0.9670000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mrDSBfoQ5Vh7hRJ2sHNzAuF53WpQLejUtZ', 1, 1, 'trungnhan.luong@gmail.com', NULL, 1, '2018-07-14 02:37:16', 'trungnhan.luong@gmail.com', NULL, NULL, 0.9796000000, 0.9796000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mrJcbMVwz7vFaA5xipD9bVVa1Nx51kdHf7', 1, 1, 'dohieuthao1994@gmail.com', NULL, 1, '2018-07-22 06:25:13', 'dohieuthao1994@gmail.com', NULL, NULL, 0.9994000000, 0.9994000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mrnLYNuX6kkWfUTtMt2NuMhNCPj4rxLjoc', 1, 1, 'khoivk86@gmail.com', NULL, 1, '2018-07-19 11:38:57', 'khoivk86@gmail.com', NULL, NULL, 0.9962000000, 0.9962000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mrNZSck1VT33joNUkR3rjtXDk7dtWRZF6j', 1, 1, 'cuarangmuoi25@gmail.com', NULL, 1, '2018-07-22 02:08:11', 'cuarangmuoi25@gmail.com', NULL, NULL, 0.9966000000, 0.9966000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mrqPSNNwFni5y7chK8MiiPZAwYfgYUnZUH', 1, 1, 'truongyen01@gmail.com', NULL, 1, '2018-07-19 01:22:18', 'truongyen01@gmail.com', NULL, NULL, 0.9976000000, 0.9976000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('ms2dQUkaXkqPPRkrc8Kpuw9YAgBU3CT5Gh', 1, 1, 'leanhdung1434@gmail.com', NULL, 1, '2018-07-22 07:32:32', 'leanhdung1434@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('ms4kgAkd9DVbBebE2NdQrLQQDBU3RJSMee', 1, 1, 'quangnguyen0293@gmail.com', NULL, 1, '2018-07-18 08:31:59', 'quangnguyen0293@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('ms7c7TycFwM2Wv9yANA9xxMF8wqkZgRoAr', 1, 1, 'mr.lehieu1@gmail.com', NULL, 1, '2018-07-18 01:48:15', 'mr.lehieu1@gmail.com', NULL, NULL, 0.9847000000, 0.9847000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('msaesNmz1mXN2EXDA5MV9eKpPHXtP1bGpH', 1, 1, 'Luongthuthuy.2992.dn@gmail.com', NULL, 1, '2018-07-18 12:21:27', 'Luongthuthuy.2992.dn@gmail.com', NULL, NULL, 0.9974000000, 0.9974000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('msDWos1eehr4X9BYj59g38hcX13mAuTXoY', 1, 1, 'bui.hoangdungtn@gmail.com', NULL, 1, '2018-07-17 04:19:08', 'bui.hoangdungtn@gmail.com', NULL, NULL, 0.9987000000, 0.9987000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('msjWH5tVJ1uwQeCprisGXXQUHJFmEPaRH6', 1, 1, 'liketocdo.com@gmail.com', NULL, 1, '2018-07-27 03:15:35', 'liketocdo.com@gmail.com', NULL, NULL, 0.9988000000, 0.9988000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', 1, 1, 'lethanhdat0111@gmail.com', NULL, 1, '2018-07-17 03:31:52', 'lethanhdat0111@gmail.com', NULL, NULL, 0.9964000000, 0.9964000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('msPJGc8ShngtGnmhyKqBNk7e9Dn4SGmoY3', 1, 1, 'thanhha12196@gmail.com', NULL, 1, '2018-07-13 11:07:46', 'thanhha12196@gmail.com', '2018-07-26 03:35:33', 'accouting', 0.9000553300, 0.9000000000, 0.00000, 0.00000, 0.00000, 0.00006, 1);
INSERT INTO `address` VALUES ('msQT2FGD6u1NDJxRTFhvVkdGjNCXY2MZC9', 1, 1, 'mvd081812@gmail.com', NULL, 1, '2018-07-22 05:58:22', 'mvd081812@gmail.com', NULL, NULL, 0.0000000000, 0.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mswEf6G69Akr2vwsjfWxQFrL66d7TFkSJK', 1, 1, 'globallook2020@gmail.com', NULL, 1, '2018-07-24 20:31:19', 'globallook2020@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', 1, 1, 'HIEU.PLAZA@GMAIL.COM', NULL, 1, '2018-07-17 00:00:55', 'HIEU.PLAZA@GMAIL.COM', NULL, NULL, 0.8114000000, 0.8114000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', 1, 1, 'sysLoan', 'Address sysLoan', 1, '2018-07-02 04:08:38', 'administrator', NULL, NULL, -134.1844603200, -134.1844603200, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mtR2yyXzsGpAiHh5Whki97TM5VERg6ZhS2', 1, 1, 'sanakiho236@gmail.com', NULL, 1, '2018-07-17 03:57:26', 'sanakiho236@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mtUKsTUk7XjMMP31SnPHqKToRgQa3PPbw9', 1, 1, 'giangbinh33@gmail.com', NULL, 1, '2018-07-18 10:56:27', 'giangbinh33@gmail.com', NULL, NULL, 0.9986000000, 0.9986000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mu2sMJouXK655NEuLSe6Gwgc86TLdgRujy', 1, 1, 'langtutomboyls@gmail.com', NULL, 1, '2018-07-18 09:01:02', 'langtutomboyls@gmail.com', NULL, NULL, 0.9990000000, 0.9990000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mu8mDyUjwi4uon6Ldkc4Ghkb3ZLTEEQQQM', 1, 1, 'pecoi1988@gmail.com', NULL, 1, '2018-07-20 13:35:30', 'pecoi1988@gmail.com', NULL, NULL, 0.9994000000, 0.9994000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('muCJpNeEmpuzkDQg5VarRDDuaWcVHyVSYx', 1, 1, 'buingochuynh7@gmail.com', NULL, 1, '2018-07-18 12:11:56', 'buingochuynh7@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('muQHbeLQMJfiUB2UTwBrB5u4PRqYqmwWBz', 1, 1, 'ndtrung84@gmail.com', NULL, 1, '2018-07-16 16:29:10', 'ndtrung84@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mv1DPXGU7zYJcBqTSMcyBPSdwQ9S3h8GYo', 1, 1, 'buitanphat1991@gmail.com', NULL, 1, '2018-07-13 11:01:43', 'buitanphat1991@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mv2rZiawxhzBqtqB6nw3wnUgr9rdh4bhmi', 1, 1, 'yocoin.vn@gmail.com', NULL, 1, '2018-07-15 12:59:18', 'yocoin.vn@gmail.com', NULL, NULL, 0.9930000000, 0.9930000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mvF21EmtQHiBt94ZTj7rhtfkVyWuiNYwGa', 1, 1, 'Tranducduy87@gmail.com', NULL, 1, '2018-07-18 07:12:32', 'Tranducduy87@gmail.com', NULL, NULL, 0.9990000000, 0.9990000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mvNJ1zbvAstPXvoxRZWQUqE92Y5yn2DFQx', 1, 1, 'dattrinhvo@gmail.com', NULL, 1, '2018-07-22 12:56:46', 'dattrinhvo@gmail.com', NULL, NULL, 0.9984000000, 0.9984000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mvsZ8hCsfwwrPngyRJX3fE3xoMUJxJB4GM', 1, 1, 'mrtran.coin@gmail.com', NULL, 1, '2018-07-18 09:46:49', 'mrtran.coin@gmail.com', NULL, NULL, 0.0000000000, 0.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mvTEB96KPhPMLxE4Wvr9ewLzLtA2m76CR7', 1, 1, 'son.ly@bigbrothers.gold', NULL, 1, '2018-07-15 01:33:53', 'son.ly@bigbrothers.gold', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mvWA7PLPHJqvgXA3ieArziaLAJEDPNbBEd', 1, 1, 'sysMgmt', 'Address sysMgmt', 1, '2018-07-02 04:04:40', 'administrator', NULL, NULL, 0.7243880000, 0.7243880000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mvWmRUkCLULt9zWAKYnpRywPRMTZyXADiv', 1, 1, 'shandypham2017@gmail.com', NULL, 1, '2018-07-18 07:30:00', 'shandypham2017@gmail.com', NULL, NULL, 0.9994000000, 0.9994000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mvzn6Na5dkgcFkn5pnfPu2SzRjf8q18Vam', 1, 1, 'hoangsonbtc68@gmail.com', NULL, 1, '2018-07-19 15:59:32', 'hoangsonbtc68@gmail.com', NULL, NULL, 0.9990000000, 0.9990000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mw7aHBci3CnzRRVhUCoA6G8nGwxtnz24oG', 1, 1, 'carcoin12345@gmail.com', NULL, 1, '2018-07-14 07:18:26', 'carcoin12345@gmail.com', NULL, NULL, 0.0000000000, 0.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mw7pruDBaxH2KDmrn4Dvew7Zq9br7y32pk', 1, 1, 'buithanhhoa0309@gmail.com', NULL, 1, '2018-07-19 07:42:52', 'buithanhhoa0309@gmail.com', NULL, NULL, 0.9990000000, 0.9990000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mw8ERtRN9KoBjXGqS6Uet2RFZRHfcAi4qT', 1, 1, 'trungdung280686@gmail.com', NULL, 1, '2018-07-17 14:08:34', 'trungdung280686@gmail.com', NULL, NULL, 0.9500000000, 0.9500000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mw9GRzGNDz9ha5cHrdziX2imLzpAnHLhCS', 1, 1, 'hvu04122000@gmail.com', NULL, 1, '2018-07-18 07:33:13', 'hvu04122000@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mwbd29Gd3LhdtZRTDmFy9R1MJdFuWAAWLN', 1, 1, 'mrnam.nt2494@gmail.com', NULL, 1, '2018-07-18 15:34:14', 'mrnam.nt2494@gmail.com', NULL, NULL, 0.9926000000, 0.9926000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mwchJhdQGENRtuBmzfB6Wsk6uiMo6VvKEo', 1, 1, 'namgialong4646@gmail.com', NULL, 1, '2018-07-18 10:27:50', 'namgialong4646@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mwgZ6vZAFNxpqEzpvZEakfPDB8Rb6h6Tmh', 1, 1, 'cscjackpot@gmail.com', NULL, 1, '2018-07-18 04:35:06', 'cscjackpot@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mwnVKakRYEProt7aCn3863BHSLT2pegoub', 1, 1, 'quytu379@gmail.com', NULL, 1, '2018-07-18 07:40:26', 'quytu379@gmail.com', NULL, NULL, 0.9996000000, 0.9996000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mwuUuoicFsLeRhrdRUj1AfGZwuwWXQctSm', 1, 1, 'manhnshd92@gmail.com', NULL, 1, '2018-07-18 22:32:42', 'manhnshd92@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mwvpqr4WvY7GAnTyCVPfr3p1GBKHt3Bbwj', 1, 1, 'userFree', 'Address userFree', 1, '2018-07-02 04:15:28', 'administrator', '2018-07-02 07:01:27', 'administrator', 0.0000000000, 0.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mx6TwFZpG3AY5XPhFzrPhorT6pyhhnApTz', 1, 1, 'nvn3007@gmail.com', NULL, 1, '2018-07-19 10:27:15', 'nvn3007@gmail.com', NULL, NULL, 0.9958000000, 0.9958000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mx7R4shzfvhNdE7UVHd7ciR7VF2hQ2arLe', 1, 1, 'nguyenhongsonhn91@gmail.com', NULL, 1, '2018-07-23 13:37:38', 'nguyenhongsonhn91@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mxCvsswMMhsFRoLkXbJX9axgH2LfsuXCYL', 1, 1, 'bades1509@yahoo.com', NULL, 1, '2018-07-19 03:14:24', 'bades1509@yahoo.com', NULL, NULL, 0.9990000000, 0.9990000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', 1, 1, 'myphamquyenvinh@gmail.com', NULL, 1, '2018-07-20 11:19:57', 'myphamquyenvinh@gmail.com', NULL, NULL, 0.9877000000, 0.9877000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mxSqRd7z17gQ7zRYbkZ9oJa7EkJ3TFDrEZ', 1, 1, 'tanhoaxa2004@gmail.com', NULL, 1, '2018-07-17 01:41:47', 'tanhoaxa2004@gmail.com', NULL, NULL, 0.9396000000, 0.9396000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('my9NWRYv3hDDyhyzpXLzJtY6MepBPRQNWp', 1, 1, 'tranthien585@gmail.com', NULL, 1, '2018-07-18 07:51:58', 'tranthien585@gmail.com', NULL, NULL, 0.9994000000, 0.9994000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('myBQdCpcASat8E4ow1ujc8D5VcuyoZmaHW', 1, 1, 'phat.bui@bigbrothers.gold', NULL, 1, '2018-07-13 11:09:30', 'phat.bui@bigbrothers.gold', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('myChTCnhaCRuYZKyPZS5fVSf3JRY9e1yTc', 1, 1, 'daisy.k@cscjackpot.com', NULL, 1, '2018-07-15 19:30:19', 'daisy.k@cscjackpot.com', NULL, NULL, 0.9506000000, 0.9506000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('myg3xMiVbLGHg9VJaXQx7aLvp9nqsNPXNU', 1, 1, 'khanhdinh8686@gmail.com', NULL, 1, '2018-07-22 09:20:29', 'khanhdinh8686@gmail.com', NULL, NULL, 0.9938000000, 0.9938000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('myumuAuRpbgK8CMQio38DtxKVNH6ZmqyHE', 1, 1, 'binhnguyentyphu@gmail.com', NULL, 1, '2018-07-18 09:54:41', 'binhnguyentyphu@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mzaciJaJMGTkHXwE2WKYutpWXoi6tHnP39', 1, 1, 'cuong0981148176@gmail.com', NULL, 1, '2018-07-21 08:36:44', 'cuong0981148176@gmail.com', NULL, NULL, 0.9988000000, 0.9988000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mzc1Z4M263WWh2pSG79v7besoTzN73a2jL', 1, 1, 'maiduynhat1987@gmail.com', NULL, 1, '2018-07-18 11:13:05', 'maiduynhat1987@gmail.com', NULL, NULL, 0.9994000000, 0.9994000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mzhrB3siKL2jw1QQWSR7vB4iTst9e7kfxZ', 1, 1, 'haquynhanhlsls@gmail.com', NULL, 1, '2018-07-18 11:24:12', 'haquynhanhlsls@gmail.com', NULL, NULL, 0.9800000000, 0.9800000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mzR5JTq2BFxZtzHT8482vhi86FuPwj7X1M', 1, 1, 'lethanh239@icloud.com', NULL, 1, '2018-07-18 13:57:09', 'lethanh239@icloud.com', NULL, NULL, 0.9581000000, 0.9581000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('mzspFYoc7N2ZfWnHU3nZJQRZDcb1PXjSjJ', 1, 1, 'sucmanhcuabanguoi@gmail.com', NULL, 1, '2018-07-19 11:45:34', 'sucmanhcuabanguoi@gmail.com', NULL, NULL, 0.9976000000, 0.9976000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n129xQ5YhvPYnLQ12dhb3er7DiPr1bovMy', 1, 1, 'longlucky69@gmail.com', NULL, 1, '2018-07-19 04:03:01', 'longlucky69@gmail.com', NULL, NULL, 0.9966000000, 0.9966000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n15yYUxvE3Pqpfm5nzHyoajGXk8TG8icAa', 1, 1, 'toan8181@gmail.com', NULL, 1, '2018-07-15 12:48:04', 'toan8181@gmail.com', NULL, NULL, 0.9988000000, 0.9988000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n1BV2rAbvcXPSZXJBt22QnYQATThqwEqtM', 1, 1, 'hoangbaocoin@gmail.com', NULL, 1, '2018-07-18 12:15:40', 'hoangbaocoin@gmail.com', NULL, NULL, 0.9780000000, 0.9780000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n1C4bF2NBjGLf6BdfgPND7r2ooNMgKPUsg', 1, 1, 'thanh.ha@bigbrothers.gold', NULL, 1, '2018-07-14 07:17:17', 'thanh.ha@bigbrothers.gold', NULL, NULL, 0.0000000000, 0.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n1fuXXppMLZpiHA1G4HX8N4XnHUr9kjKy2', 1, 1, 'ngphuongthanh86@gmail.com', NULL, 1, '2018-07-21 18:22:56', 'ngphuongthanh86@gmail.com', NULL, NULL, 0.9988000000, 0.9988000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n1jpPi46oLjhEiB3RPnS5G44VCEgQmp7t9', 1, 1, 'eduvietthai123@gmail.com', NULL, 1, '2018-07-18 07:13:54', 'eduvietthai123@gmail.com', NULL, NULL, 0.9976000000, 0.9976000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n1qiFCkcZ9wXzQvKHQoCRM9o4fRLWsGXyq', 1, 1, 'nguyendinhhai932@gmail.com', NULL, 1, '2018-07-19 08:44:01', 'nguyendinhhai932@gmail.com', NULL, NULL, 0.9988000000, 0.9988000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', 1, 1, 'sysAdmin', 'administrator', 1, '2018-06-19 10:26:51', 'administrator', NULL, NULL, 0.8152400000, 0.8152400000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', 1, 1, 'sysAdward', 'Address Sys Adward', 1, '2018-06-29 07:28:07', 'administrator', NULL, NULL, 0.0001203200, 0.0001203200, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n1tmwes3VAWpA6RVz4TMnLuknaYZejiPRz', 1, 1, 'sysFeePrize', 'Address Fee Prize', 1, '2018-06-29 07:28:59', 'administrator', NULL, NULL, 0.0166000000, 0.0166000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n1UnCTDemH4GK7EiuKAnUi3BKTttZKhHHs', 1, 1, 'carcoin123@gmail.com', NULL, 1, '2018-07-14 02:59:09', 'carcoin123@gmail.com', NULL, NULL, 0.9944000000, 0.9944000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n24HcLLXidAxejG7uyjNRuByrVDi8ZBkLY', 1, 1, 'suk1262@naver.com', NULL, 1, '2018-07-18 14:07:08', 'suk1262@naver.com', NULL, NULL, 0.9996000000, 0.9996000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n2FvbiCxUqR4jCWUJPipnzceggGPhasn5D', 1, 1, 'sysBTC', 'Address sysBTC', 1, '2018-07-02 04:09:34', 'administrator', NULL, NULL, 0.0000000000, 0.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n2mjES8L7zVrY8f2zcXW9r8fTSWGmKPLvS', 1, 1, 'cscjackpot.testing@gmail.com', NULL, 1, '2018-07-18 04:07:05', 'cscjackpot.testing@gmail.com', NULL, NULL, 0.9994000000, 0.9994000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n2Not5EqwnycuZGdXH6BLoF5awXh6gR1Je', 1, 1, 'duongngocphuoc2008@gmai.com', NULL, 1, '2018-07-18 08:14:39', 'duongngocphuoc2008@gmai.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n2NqcRBUe3SSPenm6bkyuH3D5UPuAdjxQB', 1, 1, 'lecautoananh@gmail.com', NULL, 1, '2018-07-18 10:42:24', 'lecautoananh@gmail.com', NULL, NULL, 0.9996000000, 0.9996000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', 1, 1, 'sysFreeTicket', 'freewallet.administrator', 1, '2018-06-19 10:28:03', 'administrator', '2018-07-02 07:14:12', 'BTCNetwork', 0.2213720000, 0.2177720000, 0.00000, 0.00360, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n3cVpJ13iJqXoSN2aAamFS8T9wGfrrRVJK', 1, 1, 'phamlehoa2310@gmail.com', NULL, 1, '2018-07-17 09:16:56', 'phamlehoa2310@gmail.com', NULL, NULL, 0.9996000000, 0.9996000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n3GdF5TEsn8aGrd1zJv4EbosmRmCmqGKpU', 1, 1, 'zon.zip.9@gmail.com', NULL, 1, '2018-07-18 15:21:08', 'zon.zip.9@gmail.com', NULL, NULL, 0.9684000000, 0.9684000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n3ugUwnPpbFSXCqSKpD5Ni1G94T2mW6vd8', 1, 1, 'dothicuc1970@gmail.com', NULL, 1, '2018-07-25 05:51:36', 'dothicuc1970@gmail.com', NULL, NULL, 1.0000000000, 1.0000000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n3uiYVX2EdJv7JrS1UXLEsEcLXvHmqWDed', 1, 1, 'Vothiyenoanh2468@gmail.com', NULL, 1, '2018-07-18 18:29:44', 'Vothiyenoanh2468@gmail.com', NULL, NULL, 0.9998000000, 0.9998000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', 1, 1, 'kimthithuy17061983@gmail.com', NULL, 1, '2018-07-17 02:48:56', 'kimthithuy17061983@gmail.com', NULL, NULL, 0.9960000000, 0.9960000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', 1, 1, 'hoangbhvd@gmail.com', NULL, 1, '2018-07-17 02:15:48', 'hoangbhvd@gmail.com', NULL, NULL, 0.0821000000, 0.0821000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n4fAJYGPrYqtaR8wqYmsHnnsniFZWP89rS', 1, 1, 'nguyentuanhung2828@gmail.com', NULL, 1, '2018-07-21 08:30:02', 'nguyentuanhung2828@gmail.com', NULL, NULL, 0.9382000000, 0.9382000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);
INSERT INTO `address` VALUES ('n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', 1, 1, 'vuhoan99@gmail.com', NULL, 1, '2018-07-18 13:02:35', 'vuhoan99@gmail.com', NULL, NULL, 0.9524000000, 0.9524000000, 0.00000, 0.00000, 0.00000, 0.00000, 1);

-- ----------------------------
-- Table structure for addressevents
-- ----------------------------
DROP TABLE IF EXISTS `addressevents`;
CREATE TABLE `addressevents`  (
  `EventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `EventDescription` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventData` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventData1` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventData2` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventSource` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EventType` smallint(6) NULL DEFAULT 0 COMMENT '1: Create, 2: Lock, 3: Unlock',
  `AddressId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ClientId` smallint(6) NOT NULL,
  `SystemId` smallint(6) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`EventId`) USING BTREE,
  INDEX `FK_AddressEvents_Address`(`AddressId`, `SystemId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of addressevents
-- ----------------------------
INSERT INTO `addressevents` VALUES (2, 'Lock Address', 'Ly do Lock Address', 'administrator', NULL, NULL, 2, 'mrbAUcPN14spvaTDpRoy8FWRYFMq3rE8wG', 1, 1, '2018-06-13 16:37:05');
INSERT INTO `addressevents` VALUES (3, 'unlock address', 'ly do unlock address', NULL, 'administrator', NULL, 3, 'mrbAUcPN14spvaTDpRoy8FWRYFMq3rE8wG', 1, 1, '2018-06-13 16:37:43');

-- ----------------------------
-- Table structure for providerdetail
-- ----------------------------
DROP TABLE IF EXISTS `providerdetail`;
CREATE TABLE `providerdetail`  (
  `WalletId` bigint(20) NOT NULL DEFAULT 0,
  `Key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `Value` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `CreatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CreatedDate` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`WalletId`, `Key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of providerdetail
-- ----------------------------
INSERT INTO `providerdetail` VALUES (1, 'Address', 'mwTFe8P7GeyDhPMzdCyEV7Fuh4ZksMUovk', 'administrator', '2018-06-12 17:52:49');
INSERT INTO `providerdetail` VALUES (1, 'ApiKey', 'zSzzRsTPT99lGLMK', 'administrator', '2018-06-12 17:53:00');
INSERT INTO `providerdetail` VALUES (1, 'ApiSecret', 'qjqL6IbxERhlJEP2TXRmBjqiSv66VZlt', 'administrator', '2018-06-12 17:53:00');
INSERT INTO `providerdetail` VALUES (1, 'PrivateKey', 'cPLAY6EUohFdmeAr8BVgsKFYhp8akXpZHpyUt436pcpn5VEiwP89', 'administrator', '2018-06-12 17:53:00');
INSERT INTO `providerdetail` VALUES (29, 'Address', 'mwTFe8P7GeyDhPMzdCyEV7Fuh4ZksMUovk', 'administrator', '2018-06-12 17:52:49');
INSERT INTO `providerdetail` VALUES (29, 'ApiKey', 'zSzzRsTPT99lGLMK', 'administrator', '2018-06-12 17:53:00');
INSERT INTO `providerdetail` VALUES (29, 'ApiSecret', 'qjqL6IbxERhlJEP2TXRmBjqiSv66VZlt', 'administrator', '2018-06-12 17:53:00');
INSERT INTO `providerdetail` VALUES (29, 'PrivateKey', 'cPLAY6EUohFdmeAr8BVgsKFYhp8akXpZHpyUt436pcpn5VEiwP89', 'administrator', '2018-06-12 17:53:00');

-- ----------------------------
-- Table structure for transaction
-- ----------------------------
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction`  (
  `TransactionID` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `TransactionType` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0: None, 1: Deposit, 2: TransferToMasterAddress, 3: Transfer, 4: Return, 5: Withdraw, 6: Lock, 7: Unlock',
  `FromClientId` smallint(6) NULL DEFAULT NULL COMMENT 'Transfer transaction \'\'FromClientID\'\' to \'\'ToClientID\'\' another',
  `ToClientId` smallint(6) NULL DEFAULT NULL,
  `FromSystemId` smallint(6) NULL DEFAULT NULL COMMENT 'Transfer transaction \'\'FromSystemID\'\' to \'\'ToSystemID\'\' another',
  `ToSystemId` smallint(6) NULL DEFAULT NULL COMMENT 'Transfer transaction \'\'FromSystemID\'\' to \'\'ToSystemID\'\' another',
  `FromUserId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Transfer transaction \'\'FromUserID\'\' to \'\'ToUserID\'\' another',
  `ToUserId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CurrencyCode` smallint(6) NULL DEFAULT NULL,
  `Amount` decimal(18, 10) NULL DEFAULT 0.0000000000,
  `Description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CreatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CreatedDate` datetime NULL DEFAULT NULL,
  `UpdatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `UpdatedDate` datetime NULL DEFAULT NULL,
  `CompletedDate` datetime NULL DEFAULT NULL,
  `Status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0: None, 1: Pending, 2: Processing, 3: Successfully, 4: Failed',
  `Data1` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'TransactionType: 5 - Withdraw: ToAddress, else: FromAddress',
  `Data2` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Tx',
  `Fee` decimal(18, 5) NULL DEFAULT 0.00000,
  `FeeBlockChain` decimal(18, 10) NULL DEFAULT 0.0000000000,
  `Data` date NULL DEFAULT NULL,
  `Confirmation` bigint(20) NULL DEFAULT 0,
  PRIMARY KEY (`TransactionID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of transaction
-- ----------------------------
INSERT INTO `transaction` VALUES ('00510ad4-87d0-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-15 01:40:06', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('00601f87-8db5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-22 13:41:54', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0060c8d0-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:20:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0063ac90-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:20:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('00d2372f-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:26:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('00d4a1ce-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:26:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('01176361-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:47:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('012785cd-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:47:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('01329dab-8fc9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-25 05:10:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0134e204-8fc9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-25 05:10:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0184870b-90d8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysAdward', 1, 0.0568000000, 'RevenueForAward(Award) (40 %)', 'LotteryCarcoin', '2018-07-26 13:29:59', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('01896780-90d8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysProgressivePrize', 1, 0.0126400000, 'RevenueForProgressivePrize(ProgressivePrize)', 'LotteryCarcoin', '2018-07-26 13:29:59', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0190ccee-90d8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysJackPot', 1, 0.0347200000, 'RevenueForJackpot(Jackpot) (20 %)', 'LotteryCarcoin', '2018-07-26 13:29:59', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('019a7427-90d8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysMgmt', 1, 0.0347200000, 'RevenueForSysMgmt(Oper) (18 %)', 'LotteryCarcoin', '2018-07-26 13:29:59', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('01b59727-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:51:30', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('01dd6798-89cb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungdung280686@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'trungdung280686@gmail.com', '2018-07-17 14:09:23', NULL, NULL, NULL, 3, 'mw8ERtRN9KoBjXGqS6Uet2RFZRHfcAi4qT', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('01fc4944-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:44:21', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0238c24d-90fa-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:33:23', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('02426157-8e20-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:27:53', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0244f60d-8e20-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:27:53', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('02f01950-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'viethungpdu90@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'viethungpdu90@gmail.com', '2018-07-18 06:37:13', NULL, NULL, NULL, 3, 'mkFAtr3tYY6DWJraWVtV2U28geA1CFEX3X', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0365170f-914b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-27 03:13:14', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('03b0ed62-9119-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'quanghaupro79@gmail.com', 'sysAdmin', 1, 0.0008000000, 'Payment ticket lottery', 'quanghaupro79@gmail.com', '2018-07-26 21:15:19', NULL, NULL, NULL, 3, 'mfXkcp1CgC1eaqgqQKEEbyCUcagWeJGmik', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('049197a4-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0198000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:40:36', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('04bab896-87de-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'ogunyemi1409@gmail.com', NULL, 1, 0.9985000000, 'Lottery With Draw', 'ogunyemi1409@gmail.com', '2018-07-15 03:20:26', NULL, NULL, NULL, 1, '1CSDoLYNw7GxnSSZQXx37DcDEWy2hfFGjt', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('052d7ac3-8e97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-23 16:39:48', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('058f750d-8a5e-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'shandypham2017@gmail.com', NULL, 1, 0.9995000000, 'Lottery With Draw', 'shandypham2017@gmail.com', '2018-07-18 07:41:43', NULL, NULL, NULL, 1, '1FHo2jihhE9cZGWH69U27DXDtFciPuXGZP', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('05acf5d0-9142-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:52', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('05af0f73-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:26:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('05b21327-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:26:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('05b3e4ef-90f5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0010000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-26 16:57:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('05f5e150-8b30-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyendinhhai932@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyendinhhai932@gmail.com', '2018-07-19 08:44:58', NULL, NULL, NULL, 3, 'n1qiFCkcZ9wXzQvKHQoCRM9o4fRLWsGXyq', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('062fca39-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:51:37', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0705e0bb-8fbb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuarangmuoi25@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'cuarangmuoi25@gmail.com', '2018-07-25 03:30:01', NULL, NULL, NULL, 3, 'mrNZSck1VT33joNUkR3rjtXDk7dtWRZF6j', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0715802a-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:44:29', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('07be6464-914d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 03:27:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('081a003b-89a9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-17 10:06:11', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0832bc52-9142-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('083b280b-90f5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0010000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-26 16:57:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('083bdb7d-8aa0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'mrnam.nt2494@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: mrnam.nt2494@gmail.com', 'LotteryCarcoin', '2018-07-18 15:34:14', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0873470b-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'viethungpdu90@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'viethungpdu90@gmail.com', '2018-07-18 06:37:22', NULL, NULL, NULL, 3, 'mkFAtr3tYY6DWJraWVtV2U28geA1CFEX3X', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('08aa3379-8f84-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:56:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('08adcb10-8f84-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:56:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('08c8af92-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:12:05', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('08cb97d1-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0008000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:12:05', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('08e18cec-8b6a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'bhoangson86@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: bhoangson86@gmail.com', 'LotteryCarcoin', '2018-07-19 15:40:13', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('090ffe3f-8a8b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'neyhai2012@gmail.com', 'sysAdmin', 1, 0.0008000000, 'Payment ticket lottery', 'neyhai2012@gmail.com', '2018-07-18 13:03:56', NULL, NULL, NULL, 3, 'mgcywtxYCRe2gHCqk5vtAiBbttbvQE35v4', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0931f5b4-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:29:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('094b3b8e-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:27:50', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('094dd0ff-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:27:50', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('09a5551e-9142-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('09b4f98a-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:48:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('09b7e5c1-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:48:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0a206150-8a40-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'cscjackpot.testing@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: cscjackpot.testing@gmail.com', 'LotteryCarcoin', '2018-07-18 04:07:06', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0a4dfbbe-901e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:18:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0a506a10-901e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:18:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0a77ae75-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:26:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0a7a611f-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:26:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0a8bfb32-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:51:45', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0a8d0ba1-8989-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'cuarangmuoi27@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: cuarangmuoi27@gmail.com', 'LotteryCarcoin', '2018-07-17 06:17:11', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0adb40f1-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:44:36', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0afae9a8-914d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 03:27:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0b0d0a14-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:32:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0b10395e-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:32:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0b62e0ff-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:11:39', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0b685c64-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:11:39', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0b8b1102-8972-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanhdat0111@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'lethanhdat0111@gmail.com', '2018-07-17 03:32:34', NULL, NULL, NULL, 3, 'msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0b8c2051-8a65-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'quangnguyen0293@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: quangnguyen0293@gmail.com', 'LotteryCarcoin', '2018-07-18 08:31:59', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0c194d67-9142-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:09:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0c2d5488-8fc9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-25 05:10:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0c3092ba-8fc9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-25 05:10:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0c345ec6-8bf4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0018000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-20 08:08:10', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0c46e738-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:29', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0c4930c3-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:29', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0cc2ed70-8798-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0002680000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-14 18:59:35', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0ce5d9bd-8e97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-23 16:40:01', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0da8d6aa-90f5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0010000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-26 16:57:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0dc0ada5-90f9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:26:32', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0e476ffc-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:13:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0e583308-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:13:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0e7d3259-8b6e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'bhoangson86@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'bhoangson86@gmail.com', '2018-07-19 16:09:01', NULL, NULL, NULL, 3, 'moRXLioFXZWiZEXQsp93feAWPuuCtX5wW5', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0eac52cb-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'viethungpdu90@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'viethungpdu90@gmail.com', '2018-07-18 06:37:33', NULL, NULL, NULL, 3, 'mkFAtr3tYY6DWJraWVtV2U28geA1CFEX3X', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0f2e08da-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:51:52', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0f2f568a-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:27:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0f31b522-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:27:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0f465543-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:44:43', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0f95cc3c-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:32:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0f9884a7-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:32:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0fa76273-8bf6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tripledxxl@gmail.com', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'tripledxxl@gmail.com', '2018-07-20 08:22:34', NULL, NULL, NULL, 3, 'mjoef5D4L4dkQXmhvNBYszHa9Kseetp8sc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0fc217c7-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0002000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0fc7558f-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('0fecfed6-8bfb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanhdat0111@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'lethanhdat0111@gmail.com', '2018-07-20 08:58:22', NULL, NULL, NULL, 3, 'msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('103d84de-8db5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-22 13:42:20', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('103e4763-8efa-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:28:47', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('10a19fa0-8d96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nguyenvantuy0204@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nguyenvantuy0204@gmail.com', 'LotteryCarcoin', '2018-07-22 10:00:27', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('10a78fac-8c37-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'mrkhanh831@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'mrkhanh831@gmail.com', '2018-07-20 16:07:53', NULL, NULL, NULL, 3, 'mnEXMijjAudS9jbG6AZF5hK9CMnwhi8Gnu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('11108f91-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1112f621-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('11262a48-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:48:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('11291096-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:48:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1142a061-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:12:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('11457c80-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:12:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('118b6728-8afb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'thaingocthoai1234@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'thaingocthoai1234@gmail.com', '2018-07-19 02:25:54', NULL, NULL, NULL, 3, 'mkQqxpTWWyUVjJvRAhmvm3t63ioj75mjMP', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('124179c1-8c36-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'mrkhanh831@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: mrkhanh831@gmail.com', 'LotteryCarcoin', '2018-07-20 16:00:46', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('126b616d-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:44:48', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('12b2a407-89bd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nghiemductrung0612@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nghiemductrung0612@gmail.com', 'LotteryCarcoin', '2018-07-17 12:29:39', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('130ee997-914d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 03:27:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('13418a63-914d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 03:27:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('13529d76-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('13562447-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('13a05446-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('13a28be3-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1410555d-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:52:01', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('146583a7-8e97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-23 16:40:14', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('14bb0ef1-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('14bde3bb-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('14fe985f-8db4-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'lethanh239@icloud.com', NULL, 1, 0.4995000000, 'Lottery With Draw', 'lethanh239@icloud.com', '2018-07-22 13:35:19', NULL, NULL, NULL, 1, '1M8VNub1g9rVRuhUT8gGx35HeLeSTRSZcJ', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('153ea291-8f84-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:56:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('15423c57-8f84-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:56:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('156222ae-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'viethungpdu90@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'viethungpdu90@gmail.com', '2018-07-18 06:37:44', NULL, NULL, NULL, 3, 'mkFAtr3tYY6DWJraWVtV2U28geA1CFEX3X', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('15b4f568-914d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 03:28:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('160777ca-868c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'hathanhpc20@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: hathanhpc20@gmail.com', 'LotteryCarcoin', '2018-07-13 11:01:25', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1664d5ac-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:44:55', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('166b7f57-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:11:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('166e0db8-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:11:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('166f7493-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:27:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1671f207-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:27:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('16ae253d-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:30:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('16af32ea-894b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 22:53:43', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('16eb4e80-8a8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-18 13:32:57', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('171ca80d-8bf3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'trungngan.fca@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: trungngan.fca@gmail.com', 'LotteryCarcoin', '2018-07-20 08:01:18', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('171cb154-90c1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'duonghaison1970@gmail.com', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'duonghaison1970@gmail.com', '2018-07-26 10:45:56', NULL, NULL, NULL, 3, 'mjKv2FhU3s8Xz8SWyh2c3AMCTZdvrvaMNJ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('171e35fa-914d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 03:28:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1762db6e-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('17659920-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('176bcf99-8d54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'cuarangmuoi25@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: cuarangmuoi25@gmail.com', 'LotteryCarcoin', '2018-07-22 02:08:11', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('17798cf1-8afa-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'suk1262@naver.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'suk1262@naver.com', '2018-07-19 02:18:54', NULL, NULL, NULL, 3, 'n24HcLLXidAxejG7uyjNRuByrVDi8ZBkLY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('17c06d31-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('17c27d6e-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('17de68a9-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('17e0bf73-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('17f1e090-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:48:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('17f850f2-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:48:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('18271e3c-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:52:07', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('184ce2e9-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:49', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('184fd0f6-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:49', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('18744d84-8bfb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanhdat0111@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'lethanhdat0111@gmail.com', '2018-07-20 08:58:37', NULL, NULL, NULL, 3, 'msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('188bfed7-90f5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0010000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-26 16:58:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('188cf11e-8d13-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'ngphuongthanh86@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: ngphuongthanh86@gmail.com', 'LotteryCarcoin', '2018-07-21 18:22:56', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('18c12bc1-897b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'xuanan10@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: xuanan10@gmail.com', 'LotteryCarcoin', '2018-07-17 04:37:22', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('19841cc9-9054-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-25 21:45:45', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('19941833-8b30-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyendinhhai932@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyendinhhai932@gmail.com', '2018-07-19 08:45:31', NULL, NULL, NULL, 3, 'n1qiFCkcZ9wXzQvKHQoCRM9o4fRLWsGXyq', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('19ac4de8-8736-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 07:18:26', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('19b7dfc5-914d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 03:28:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('19eb442c-8a85-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'Luongthuthuy.2992.dn@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: Luongthuthuy.2992.dn@gmail.com', 'LotteryCarcoin', '2018-07-18 12:21:27', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1a486c65-9142-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:09:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1a4afe1c-8a69-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'langtutomboyls@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: langtutomboyls@gmail.com', 'LotteryCarcoin', '2018-07-18 09:01:02', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1a5e6d66-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:45:02', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1a6a84f5-8a7d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'haquynhanhlsls@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: haquynhanhlsls@gmail.com', 'LotteryCarcoin', '2018-07-18 11:24:12', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1a7592e4-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:21:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1a78ca8d-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:21:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1ab67abd-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:27:26', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1ab8e0c6-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:27:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b0ea983-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:30:18', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b12d379-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b12daae-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'quanghaupro79@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 21:30:18', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b15d26e-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b2f259c-8e6b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'phuctd222@gmail.com', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'phuctd222@gmail.com', '2018-07-23 11:25:27', NULL, NULL, NULL, 3, 'mmaqE3i7T2B8aQtrDKajse5vuiHBuPNevu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b3a2125-87e6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/14/2018 7:00:00 PM', '2018-07-15 04:18:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b3cbc2e-87e6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'nguyenlytruongson@gmail.com', 1, 0.0019000000, 'Total Amount Receive', '7/14/2018 7:00:00 PM', '2018-07-15 04:18:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b4c6850-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b4f7f5d-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b76f53e-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1b7989ff-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1caf5095-888b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0005840000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-15 23:59:29', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1cc9d9ce-8e97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-23 16:40:28', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1ccebf3a-89cb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungdung280686@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'trungdung280686@gmail.com', '2018-07-17 14:10:08', NULL, NULL, NULL, 3, 'mw8ERtRN9KoBjXGqS6Uet2RFZRHfcAi4qT', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1ce2a254-8c1c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-20 12:54:57', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1d122f4b-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:52:16', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1d5a2753-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1d5c1dcc-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1d6956cc-9142-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:09:32', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1da9c37d-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:45:07', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1dad41d5-8cf2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-21 14:26:51', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1dc4bb55-8d86-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trandinhson96@gmail.com', 'sysAdmin', 1, 0.0018000000, 'Payment ticket lottery', 'trandinhson96@gmail.com', '2018-07-22 08:06:17', NULL, NULL, NULL, 3, 'mhJSB7P9vjKjdkKK7ZHPQoZqK81NEuPxhu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1e0c92ec-8db6-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'huynhminhduc041097@gmail.com', NULL, 1, 0.9399000000, 'Lottery With Draw', 'huynhminhduc041097@gmail.com', '2018-07-22 13:49:53', NULL, NULL, NULL, 1, '3JfdAijjheKG2KxYNhK2sghEbTi6d1q5KM', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1e1317c8-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:48:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1e156db9-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:48:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1e750bdd-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1e798415-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1f04f525-894f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0036000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 23:22:35', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1f1debef-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:27:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1f214895-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:27:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1f4cab0f-8989-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuarangmuoi27@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'cuarangmuoi27@gmail.com', '2018-07-17 06:17:46', NULL, NULL, NULL, 3, 'mkkusAPvz6HR4pbWKdLE8bjDxqrMQNT3zs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1f521399-8b0d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'daisy.k@cscjackpot.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'daisy.k@cscjackpot.com', '2018-07-19 04:35:08', NULL, NULL, NULL, 3, 'myChTCnhaCRuYZKyPZS5fVSf3JRY9e1yTc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1f76022c-8c00-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'phamdung1080@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: phamdung1080@gmail.com', 'LotteryCarcoin', '2018-07-20 09:34:36', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1f8312ec-90f2-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nguyenle130690@gmail.com', NULL, 1, 1.4995000000, 'Lottery With Draw', 'nguyenle130690@gmail.com', '2018-07-26 16:36:56', NULL, NULL, NULL, 1, '3AyonV65ecTHd8uynQ931j45jJo9YQKu8t', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1f9f0e29-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('1fa13b98-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('20302115-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:28', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('203220fd-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:28', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('20405652-8721-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 04:48:17', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2047d6ac-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('204a98b6-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0010000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('20521ee3-868c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'buitanphat1991@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: buitanphat1991@gmail.com', 'LotteryCarcoin', '2018-07-13 11:01:43', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('20d35d04-8d53-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-22 02:01:18', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('20ebf481-87dd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'ogunyemi1409@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'ogunyemi1409@gmail.com', '2018-07-15 03:14:04', NULL, NULL, NULL, 3, 'mkBwwtpHspnavYXgSVmKZWB3WfY5uiYvwu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('210279fd-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:45:13', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('21e354d3-87cf-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'son.ly@bigbrothers.gold', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: son.ly@bigbrothers.gold', 'LotteryCarcoin', '2018-07-15 01:33:53', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2204a701-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:52:24', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2225b61c-8f80-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysMgmt', 1, 0.1546920000, 'RevenueForSysMgmt(Oper) (18 %)', 'LotteryCarcoin', '2018-07-24 20:28:26', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('22d3e147-8dd7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'khanhdinh8686@gmail.com', 'sysAdmin', 1, 0.0022000000, 'Payment ticket lottery', 'khanhdinh8686@gmail.com', '2018-07-22 17:46:14', NULL, NULL, NULL, 3, 'myg3xMiVbLGHg9VJaXQx7aLvp9nqsNPXNU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('22fc7c75-8e97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-23 16:40:38', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('231590c3-8a5a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'eduvietthai123@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: eduvietthai123@gmail.com', 'LotteryCarcoin', '2018-07-18 07:13:54', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('23540730-8bfb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanhdat0111@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'lethanhdat0111@gmail.com', '2018-07-20 08:58:55', NULL, NULL, NULL, 3, 'msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('237cca80-8bf4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-20 08:08:49', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('237cfb1a-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'viethungpdu90@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'viethungpdu90@gmail.com', '2018-07-18 06:38:07', NULL, NULL, NULL, 3, 'mkFAtr3tYY6DWJraWVtV2U28geA1CFEX3X', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('23832e1c-8b49-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'khoivk86@gmail.com', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'khoivk86@gmail.com', '2018-07-19 11:44:45', NULL, NULL, NULL, 3, 'mrnLYNuX6kkWfUTtMt2NuMhNCPj4rxLjoc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('239cfa8e-8e6a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'daisy.k@cscjackpot.com', 'sysAdmin', 1, 0.0080000000, 'Payment ticket lottery', 'daisy.k@cscjackpot.com', '2018-07-23 11:18:32', NULL, NULL, NULL, 3, 'myChTCnhaCRuYZKyPZS5fVSf3JRY9e1yTc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('239d9d1a-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:27:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('23a0b6bd-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:27:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('23b7a52b-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0002000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:08', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('23ba13af-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:08', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('23d5b1de-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:21:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('23d8f11c-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:21:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2450ecd6-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:45:18', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('245a4b5e-8c2d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'trandinhson96@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: trandinhson96@gmail.com', 'LotteryCarcoin', '2018-07-20 14:56:51', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('249cd56e-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('249f2aef-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('24c84826-8b6d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenle130690@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyenle130690@gmail.com', '2018-07-19 16:02:29', NULL, NULL, NULL, 3, 'mmroEqY7mWr3ohCKQZNJ6PL3Hk77AEz5h5', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('25324587-8f80-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysAdward', 1, 0.2705000000, 'RevenueForAward(Award) (40 %)', 'LotteryCarcoin', '2018-07-24 20:28:32', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('253498a7-8f80-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysProgressivePrize', 1, 0.0732600000, 'RevenueForProgressivePrize(ProgressivePrize)', 'LotteryCarcoin', '2018-07-24 20:28:32', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('25378a94-8f80-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysJackPot', 1, 0.1718800000, 'RevenueForJackpot(Jackpot) (20 %)', 'LotteryCarcoin', '2018-07-24 20:28:32', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2539b1c2-8f80-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysMgmt', 1, 0.1718800000, 'RevenueForSysMgmt(Oper) (18 %)', 'LotteryCarcoin', '2018-07-24 20:28:32', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('256892b1-880b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenlytruongson@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nguyenlytruongson@gmail.com', '2018-07-15 08:43:28', NULL, NULL, NULL, 3, 'moQF9NczNegDDFKsfFM8hRrsr8Rm7GchfW', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('25af43bc-8af5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'mrnam.nt2494@gmail.com', 'sysAdmin', 1, 0.0044000000, 'Payment ticket lottery', 'mrnam.nt2494@gmail.com', '2018-07-19 01:43:31', NULL, NULL, NULL, 3, 'mwbd29Gd3LhdtZRTDmFy9R1MJdFuWAAWLN', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('25edf079-8979-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanhdat0111@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'lethanhdat0111@gmail.com', '2018-07-17 04:23:25', NULL, NULL, NULL, 3, 'msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2645e131-9142-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:09:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2679fb9e-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('267c33b6-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('26a4515b-89a2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'phamlehoa2310@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: phamlehoa2310@gmail.com', 'LotteryCarcoin', '2018-07-17 09:16:56', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('26f038d0-907d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'kavantrant93@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: kavantrant93@gmail.com', 'LotteryCarcoin', '2018-07-26 02:39:37', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('27069b4d-8a89-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'hoang0977671567@gmail.com', NULL, 1, 0.9895000000, 'Lottery With Draw', 'hoang0977671567@gmail.com', '2018-07-18 12:50:27', NULL, NULL, NULL, 1, '1JVHn8qgYiidDFnTWrpcVKzCseqy9m9MGE', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2743c5ac-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:30:38', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2765e243-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2767f2be-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('27a70c42-8971-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'viethungpdu90@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'viethungpdu90@gmail.com', '2018-07-17 03:26:12', NULL, NULL, NULL, 3, 'mkFAtr3tYY6DWJraWVtV2U28geA1CFEX3X', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('27b19ad8-8b40-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'manhthuyen1997@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'manhthuyen1997@gmail.com', '2018-07-19 10:40:26', NULL, NULL, NULL, 1, '12qnHtY1WuKhF4DkU89EcvrL6hG8tCXVgq', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('27e4e25e-8ccc-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-21 09:55:07', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2802f268-8f9d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 23:56:12', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2805dba5-8f9d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0008000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 23:56:12', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('282f064c-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:12:28', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2832e91c-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:12:28', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2838ee5a-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:45:25', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('283d2416-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:21:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('283fd9cf-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:21:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('284192a1-8db6-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'huynhminhduc041097@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'huynhminhduc041097@gmail.com', '2018-07-22 13:50:10', NULL, NULL, NULL, 1, '3JfdAijjheKG2KxYNhK2sghEbTi6d1q5KM', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('284bc46c-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('284e569c-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:38', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('286a4845-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('286db0ef-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0019000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('288a5a87-8a5c-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'eduvietthai123@gmail.com', NULL, 1, 0.9895000000, 'Lottery With Draw', 'eduvietthai123@gmail.com', '2018-07-18 07:28:22', NULL, NULL, NULL, 1, '3CZjLuqs3CHbP7dEfPHxnhwceSk1Z5Jxdr', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('28c436c9-90f5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysJackPot', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-26 16:58:40', NULL, NULL, NULL, 3, 'mgXrncAbs1XSzmYAvUFoRfQs9kC5p3euuY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('28c7e750-90f5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysJackPot', 'huynhminhduc041097@gmail.com', 1, 10.5065800000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-26 16:58:40', NULL, NULL, NULL, 3, 'mgXrncAbs1XSzmYAvUFoRfQs9kC5p3euuY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('28e48c21-8737-11e8-8c19-00163ec5394d', 1, 0, 1, 0, 1, NULL, 'kienduc94@gmail.com', 1, 0.0085937500, NULL, 'BTCNetwork', '2018-07-14 07:26:01', 'BTCNetwork', '2018-07-14 07:32:46', NULL, 3, 'mtixQWdPfuBbKxJ8cDNbcQbYdiHRNAPEotmu2VUWJ2gNbv8XgyimVoBYGQBWTmhegqkj', '7a54450f206c7f07e1323e763fc9469da660c4cb9175f7a92a0463967a822059', 0.00000, 0.0000000000, NULL, 2);
INSERT INTO `transaction` VALUES ('29baf59a-8e97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-23 16:40:49', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('29f90118-8bf4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-20 08:09:00', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2a0eda36-8d18-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0021360000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-21 18:59:13', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2a1b6933-8a69-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'thaingocthoai1234@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'thaingocthoai1234@gmail.com', '2018-07-18 09:01:29', NULL, NULL, NULL, 3, 'mkQqxpTWWyUVjJvRAhmvm3t63ioj75mjMP', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2a2cc1f6-9092-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-26 05:10:02', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2a2d09b8-896d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'viethungpdu90@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'viethungpdu90@gmail.com', '2018-07-17 02:57:38', NULL, NULL, NULL, 3, 'mkFAtr3tYY6DWJraWVtV2U28geA1CFEX3X', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2a4ca428-8c01-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tuyendinhhb@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'tuyendinhhb@gmail.com', '2018-07-20 09:42:04', NULL, NULL, NULL, 3, 'mpXHwF5MKv3UGtdgUFpQQgPjXQ4WRmwuaf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2a984c31-8750-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 10:25:01', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2ac7431b-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:52:39', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2aee8e84-8d96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenvantuy0204@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyenvantuy0204@gmail.com', '2018-07-22 10:01:11', NULL, NULL, NULL, 3, 'mk5LQSdNBuiqmRFnyG1j1HMFTyHKMb5kcS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2b0f1926-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2b11d6f8-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2b85fef4-9142-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:09:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2be03efd-894b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 22:54:18', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2cd2904a-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2cd57abd-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2d3ab32b-8726-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 05:24:27', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2d44c5c0-8d9e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'zon.zip.9@gmail.com', 'sysAdmin', 1, 0.0100000000, 'Payment ticket lottery', 'zon.zip.9@gmail.com', '2018-07-22 10:58:31', NULL, NULL, NULL, 3, 'n3GdF5TEsn8aGrd1zJv4EbosmRmCmqGKpU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2d73392a-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:50', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2d754f65-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:28:50', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2d9e0dbf-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:45:34', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2e185fa4-8989-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuarangmuoi27@gmail.com', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'cuarangmuoi27@gmail.com', '2018-07-17 06:18:11', NULL, NULL, NULL, 3, 'mkkusAPvz6HR4pbWKdLE8bjDxqrMQNT3zs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2e1a21eb-9142-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:10:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2e71f564-8728-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'kienduc94@gmail.com', NULL, 1, 0.0995000000, 'Lottery With Draw', 'kienduc94@gmail.com', '2018-07-14 05:38:48', 'accouting', '2018-07-14 05:39:31', NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', '19ae1db7741342bd4c58509b37b3093a2ff0dcb0f8f12de64dfa2e11dacc969d', 0.00050, 0.0000033800, NULL, 0);
INSERT INTO `transaction` VALUES ('2ee537df-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:49', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2ee85b41-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:49', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2f0851ea-8af2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'truongyen01@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: truongyen01@gmail.com', 'LotteryCarcoin', '2018-07-19 01:22:18', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2f797386-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:52:47', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2f8285e7-914d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-27 03:28:47', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2f9eeb67-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:56:07', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2fc7cf3a-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:12:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('2fcb6d23-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:12:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('30137f05-8e84-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'doanthithunguyet4@gmail.com', NULL, 1, 0.9975000000, 'Lottery With Draw', 'doanthithunguyet4@gmail.com', '2018-07-23 14:25:00', NULL, NULL, NULL, 1, '3JJoRStLxWE19kRegDAxCabKk4Vvn8pwWZ', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('304e1112-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:28:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3056e289-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:28:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('309d545e-872e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungnhan.luong@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'trungnhan.luong@gmail.com', '2018-07-14 06:21:48', NULL, NULL, NULL, 3, 'mrDSBfoQ5Vh7hRJ2sHNzAuF53WpQLejUtZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('30a1805c-8a5e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'shandypham2017@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'shandypham2017@gmail.com', '2018-07-18 07:42:55', NULL, NULL, NULL, 3, 'mvWmRUkCLULt9zWAKYnpRywPRMTZyXADiv', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('30f28b79-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:30:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('313dc225-8979-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanhdat0111@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'lethanhdat0111@gmail.com', '2018-07-17 04:23:44', NULL, NULL, NULL, 3, 'msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('315501f3-8e97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-23 16:41:02', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('31a7f327-898a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuarangmuoi27@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'cuarangmuoi27@gmail.com', '2018-07-17 06:25:26', NULL, NULL, NULL, 3, 'mkkusAPvz6HR4pbWKdLE8bjDxqrMQNT3zs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('31b259a2-8a5a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'Tranducduy87@gmail.com', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'Tranducduy87@gmail.com', '2018-07-18 07:14:19', NULL, NULL, NULL, 3, 'mvF21EmtQHiBt94ZTj7rhtfkVyWuiNYwGa', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('31cb2901-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('31ce7ccc-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('32256312-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0002000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:32', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('32287620-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:32', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3241859c-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('324605e2-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('326265d7-8bf4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0060000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-20 08:09:14', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('328da541-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:30:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('32a33c74-90f8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'khanhdinh8686@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'khanhdinh8686@gmail.com', '2018-07-26 17:20:25', NULL, NULL, NULL, 3, 'myg3xMiVbLGHg9VJaXQx7aLvp9nqsNPXNU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('32c59ea7-8c1b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-20 12:48:25', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('32d4d263-8f9d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 23:56:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('32d7d1fd-8f9d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0008000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 23:56:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('33195f52-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('331b9f35-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:14:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3396100a-8b34-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyendinhhai932@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyendinhhai932@gmail.com', '2018-07-19 09:14:52', NULL, NULL, NULL, 3, 'n1qiFCkcZ9wXzQvKHQoCRM9o4fRLWsGXyq', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('33dbfefb-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('33de8190-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:33:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('341351f1-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:52:54', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('344f658f-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0002000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('34539bb9-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3456c010-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0198000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:45:45', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('345ab0f2-8cc1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'cuong0981148176@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: cuong0981148176@gmail.com', 'LotteryCarcoin', '2018-07-21 08:36:44', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('345f95e2-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:29:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3461cde4-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:29:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('34fa2ce9-8830-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'yocoin.vn@gmail.com', 'sysAdmin', 1, 0.0028000000, 'Payment ticket lottery', 'yocoin.vn@gmail.com', '2018-07-15 13:08:46', NULL, NULL, NULL, 3, 'mv2rZiawxhzBqtqB6nw3wnUgr9rdh4bhmi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3527a7c2-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:28:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('352a3c72-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:28:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('35b7bf8e-8a5e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-18 07:43:04', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('35ffcc8b-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('36025fc7-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('363b015c-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:12:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('363ebf55-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:12:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('365433f2-8b40-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'manhthuyen1997@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'manhthuyen1997@gmail.com', '2018-07-19 10:40:51', NULL, NULL, NULL, 1, '12qnHtY1WuKhF4DkU89EcvrL6hG8tCXVgq', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('36609692-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('36679e6f-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('36a162c8-8b0a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'longlucky69@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'longlucky69@gmail.com', '2018-07-19 04:14:19', NULL, NULL, NULL, 3, 'n129xQ5YhvPYnLQ12dhb3er7DiPr1bovMy', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('36a9d5fa-8a89-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'hoang0977671567@gmail.com', NULL, 1, 0.9895000000, 'Lottery With Draw', 'hoang0977671567@gmail.com', '2018-07-18 12:50:53', NULL, NULL, NULL, 1, '1JVHn8qgYiidDFnTWrpcVKzCseqy9m9MGE', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('36be3962-868d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'phat.bui@bigbrothers.gold', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: phat.bui@bigbrothers.gold', 'LotteryCarcoin', '2018-07-13 11:09:30', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('36cbdde7-8a89-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'hoang0977671567@gmail.com', NULL, 1, 0.9895000000, 'Lottery With Draw', 'hoang0977671567@gmail.com', '2018-07-18 12:50:54', NULL, NULL, NULL, 1, '1JVHn8qgYiidDFnTWrpcVKzCseqy9m9MGE', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('37171db9-8cf1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-21 14:20:24', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('372971c2-8b30-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyendinhhai932@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'nguyendinhhai932@gmail.com', '2018-07-19 08:46:20', NULL, NULL, NULL, 3, 'n1qiFCkcZ9wXzQvKHQoCRM9o4fRLWsGXyq', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('37c0fd8b-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:34:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('37c50400-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:34:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('37d6973d-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0196000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:56:21', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('37da5866-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:50:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('37dc9086-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:50:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('37f9a171-8968-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-17 02:22:14', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('385e1c6e-894e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 23:16:08', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('387bba7f-8bf3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-20 08:02:14', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('38aec466-8e97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-23 16:41:14', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('38ec542c-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:45:53', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('392a20f2-89fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysMgmt', 1, 0.0552960000, 'RevenueForSysMgmt(Oper) (18 %)', 'LotteryCarcoin', '2018-07-17 20:08:51', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3969b69c-8998-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-17 08:05:52', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('398af727-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:31:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('39a00d44-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('39a2c9f9-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('39d0b1e9-8a79-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'giangbinh33@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: giangbinh33@gmail.com', 'LotteryCarcoin', '2018-07-18 10:56:27', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('39fa292b-8e0b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0047880000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-22 23:59:07', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3a0bf491-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:53:04', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3a619f28-89fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysAdward', 1, 0.2457600000, 'RevenueForAward(Award) (40 %)', 'LotteryCarcoin', '2018-07-17 20:08:53', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3a64b178-89fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysFeePrize', 'sysAdward', 1, 0.0004000000, 'Tranfer -> from: sysFeePrize to: sysAdward', 'LotteryCarcoin', '2018-07-17 20:08:53', NULL, NULL, NULL, 3, 'n1tmwes3VAWpA6RVz4TMnLuknaYZejiPRz', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3a678a7e-89fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysFeeTxn', 'sysAdward', 1, 0.0004000000, 'Tranfer -> from: sysFeeTxn to: sysAdward', 'LotteryCarcoin', '2018-07-17 20:08:53', NULL, NULL, NULL, 3, 'mi9c8BMi9QUgQ8wdfJFEp7VWU7wKHHEZho', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3a6a894e-89fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'sysAdward', 1, 0.1463501600, 'Tranfer -> from: sysLoan to: sysAdward', 'LotteryCarcoin', '2018-07-17 20:08:53', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3a85fcc8-8a75-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'namgialong4646@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: namgialong4646@gmail.com', 'LotteryCarcoin', '2018-07-18 10:27:50', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3aa58301-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:31:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3acf83bb-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:29', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3ad229ff-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:29', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3b0303a8-8a90-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'thanhlam6618@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'thanhlam6618@gmail.com', '2018-07-18 13:41:07', NULL, NULL, NULL, 3, 'mmrGM9Djrqi5mjHBPikxNm5VUn3APtwBoq', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3b1bcc86-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3b1e87ee-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3b4ef84d-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:34:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3b52e918-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:34:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3bc775fd-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:50:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3bc96282-8db5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huynhminhduc041097@gmail.com', 'sysAdmin', 1, 0.0060000000, 'Payment ticket lottery', 'huynhminhduc041097@gmail.com', '2018-07-22 13:43:33', NULL, NULL, NULL, 3, 'mid7hc2rzzqYyo2Uh2gsrBsxpDYbFSh3KT', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3bc9ddf1-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:50:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3beb5eb1-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:42:09', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3c0b4224-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3c0e357c-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3c2157eb-8bf4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-20 08:09:30', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3c533943-89a3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'phamlehoa2310@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'phamlehoa2310@gmail.com', '2018-07-17 09:24:41', NULL, NULL, NULL, 3, 'n3cVpJ13iJqXoSN2aAamFS8T9wGfrrRVJK', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3ca6b1e3-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:45:59', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3cf72d97-914d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-27 03:29:09', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3d0859b1-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:31:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3d105cbc-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:31:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3d23a834-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:56:30', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3d994fb7-8a89-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'hoang0977671567@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'hoang0977671567@gmail.com', '2018-07-18 12:51:05', NULL, NULL, NULL, 1, '1JVHn8qgYiidDFnTWrpcVKzCseqy9m9MGE', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3da93327-8a89-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'hoang0977671567@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'hoang0977671567@gmail.com', '2018-07-18 12:51:05', NULL, NULL, NULL, 1, '1JVHn8qgYiidDFnTWrpcVKzCseqy9m9MGE', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3ddf5b04-9092-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-26 05:10:35', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3e6607d7-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3e692ee1-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3ef36d0f-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3ef57264-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:50:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3ef6374b-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0008000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3ef77a4e-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:50:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3f7abadb-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:53:13', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3f8efa63-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:55', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3f9200a8-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:38:55', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3f9212dd-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:17', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3f94d410-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:17', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3fa8fd5f-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0120000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:42:15', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3fbfebca-8ccc-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-21 09:55:47', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('3fc1845c-897b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'xuanan10@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'xuanan10@gmail.com', '2018-07-17 04:38:27', NULL, NULL, NULL, 3, 'mibuYaFxrJzUDLQGwBoGcXKBPaNnfxYZYY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4083d96a-894b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 22:54:53', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('40ad1769-8d54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuarangmuoi25@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'cuarangmuoi25@gmail.com', '2018-07-22 02:09:20', NULL, NULL, NULL, 3, 'mrNZSck1VT33joNUkR3rjtXDk7dtWRZF6j', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('40bbff58-8b49-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'sucmanhcuabanguoi@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: sucmanhcuabanguoi@gmail.com', 'LotteryCarcoin', '2018-07-19 11:45:34', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('410a750e-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:46:07', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4122cc04-8b34-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyendinhhai932@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyendinhhai932@gmail.com', '2018-07-19 09:15:15', NULL, NULL, NULL, 3, 'n1qiFCkcZ9wXzQvKHQoCRM9o4fRLWsGXyq', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('41d747d8-8af5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'mrnam.nt2494@gmail.com', 'sysAdmin', 1, 0.0028000000, 'Payment ticket lottery', 'mrnam.nt2494@gmail.com', '2018-07-19 01:44:18', NULL, NULL, NULL, 3, 'mwbd29Gd3LhdtZRTDmFy9R1MJdFuWAAWLN', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('429640c6-8db5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huynhminhduc041097@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'huynhminhduc041097@gmail.com', '2018-07-22 13:43:45', NULL, NULL, NULL, 3, 'mid7hc2rzzqYyo2Uh2gsrBsxpDYbFSh3KT', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('42b258a8-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:28:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('42b4e76e-9006-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:28:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('42d6c8d2-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:29:26', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('42d8c67d-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0005000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:29:26', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('433437c5-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:50:55', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('433699cc-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:50:55', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('43394f84-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('433c8dad-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('433e68d1-8998-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-17 08:06:09', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4341f942-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('43446682-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('43766ed2-8a77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'lecautoananh@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: lecautoananh@gmail.com', 'LotteryCarcoin', '2018-07-18 10:42:24', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4383a3a7-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:42:22', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4396e88b-90f8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'khanhdinh8686@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'khanhdinh8686@gmail.com', '2018-07-26 17:20:53', NULL, NULL, NULL, 3, 'myg3xMiVbLGHg9VJaXQx7aLvp9nqsNPXNU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('43ecf22e-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:56:41', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('443b1959-8d9d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'zon.zip.9@gmail.com', 'sysAdmin', 1, 0.0100000000, 'Payment ticket lottery', 'zon.zip.9@gmail.com', '2018-07-22 10:52:00', NULL, NULL, NULL, 3, 'n3GdF5TEsn8aGrd1zJv4EbosmRmCmqGKpU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('44ae7bad-8cc0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nguyentuanhung2828@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nguyentuanhung2828@gmail.com', 'LotteryCarcoin', '2018-07-21 08:30:02', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('44dd741e-868c-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-13 11:02:44', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('44f229a0-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:46:13', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('44fdc79d-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('45008468-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('45233a23-8a93-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanh239@icloud.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'lethanh239@icloud.com', '2018-07-18 14:02:53', NULL, NULL, NULL, 3, 'mzR5JTq2BFxZtzHT8482vhi86FuPwj7X1M', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('45a6b955-898a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuarangmuoi27@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'cuarangmuoi27@gmail.com', '2018-07-17 06:26:00', NULL, NULL, NULL, 3, 'mkkusAPvz6HR4pbWKdLE8bjDxqrMQNT3zs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('45eb5847-8955-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fsuphihung@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'fsuphihung@gmail.com', '2018-07-17 00:06:37', NULL, NULL, NULL, 3, 'moHQynVxdQW6dvno6Lb5HwSuvCvqCsZ8JR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('46c02d2f-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:51:01', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('46c25f63-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:51:01', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('46e28179-87a1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysMgmt', 1, 0.0064800000, 'RevenueForSysMgmt(Oper) (18 %)', 'LotteryCarcoin', '2018-07-14 20:05:38', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('46ef5fcc-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:29', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('46f1eb11-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:29', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('47170591-8a73-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenly796@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyenly796@gmail.com', '2018-07-18 10:13:52', NULL, NULL, NULL, 3, 'mqpYrS6fK9Uq28PZN9RUXqAc8hoY2y7RZY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('471b489f-87a1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysFeeTxn', 'sysAdward', 1, 0.0000000000, 'Tranfer -> from: sysFeeTxn to: sysAdward', 'LotteryCarcoin', '2018-07-14 20:05:38', NULL, NULL, NULL, 3, 'mi9c8BMi9QUgQ8wdfJFEp7VWU7wKHHEZho', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('471de59a-87a1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'sysAdward', 1, 0.0381101600, 'Tranfer -> from: sysLoan to: sysAdward', 'LotteryCarcoin', '2018-07-14 20:05:38', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4728cfcc-8a89-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'hoang0977671567@gmail.com', NULL, 1, 0.9895000000, 'Lottery With Draw', 'hoang0977671567@gmail.com', '2018-07-18 12:51:21', NULL, NULL, NULL, 1, '1JVHn8qgYiidDFnTWrpcVKzCseqy9m9MGE', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('477991f2-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:42:28', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('47e15a7a-891f-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'toan8181@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'toan8181@gmail.com', '2018-07-16 17:40:07', NULL, NULL, NULL, 1, '174gzsMyvMbuxzhWptG6izCLWsAorQSDTF', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('47f1727e-894e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 23:16:34', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('481cc321-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('481fa1b8-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4822e854-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:39:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4826a7c6-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:39:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('482f5437-8728-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kienduc94@gmail.com', 'sysFeeTxn', 1, 0.0004966200, 'Lottery With Draw', 'accouting', '2018-07-14 05:39:31', NULL, NULL, NULL, 3, 'mnZq9TCKeAmd1gSCS543k3BEdmWfjdZoLa', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('48497933-8998-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-17 08:06:17', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('484aef60-8db5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huynhminhduc041097@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'huynhminhduc041097@gmail.com', '2018-07-22 13:43:54', NULL, NULL, NULL, 3, 'mid7hc2rzzqYyo2Uh2gsrBsxpDYbFSh3KT', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4852ce2b-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:46:19', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('48624af2-8c01-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tuyendinhhb@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'tuyendinhhb@gmail.com', '2018-07-20 09:42:54', NULL, NULL, NULL, 3, 'mpXHwF5MKv3UGtdgUFpQQgPjXQ4WRmwuaf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('48932c10-8963-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'dany.alliat@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: dany.alliat@gmail.com', 'LotteryCarcoin', '2018-07-17 01:46:54', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('48f7b218-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:46:20', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4944de99-8830-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'yocoin.vn@gmail.com', 'sysAdmin', 1, 0.0038000000, 'Payment ticket lottery', 'yocoin.vn@gmail.com', '2018-07-15 13:09:20', NULL, NULL, NULL, 3, 'mv2rZiawxhzBqtqB6nw3wnUgr9rdh4bhmi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('49a3fc21-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:13:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('49a8ad2e-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:13:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('49b2c3d3-8cc0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyentuanhung2828@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyentuanhung2828@gmail.com', '2018-07-21 08:30:10', NULL, NULL, NULL, 3, 'n4fAJYGPrYqtaR8wqYmsHnnsniFZWP89rS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('49cae949-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:34:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('49cef36b-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:34:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('49de9802-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:29:38', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('49e0e4ef-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0010000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:29:38', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4a0c180c-8c2d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trandinhson96@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'trandinhson96@gmail.com', '2018-07-20 14:57:55', NULL, NULL, NULL, 3, 'mhJSB7P9vjKjdkKK7ZHPQoZqK81NEuPxhu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4a222c80-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:51:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4a24671b-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:51:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4ac09fa6-8a84-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'hoangbaocoin@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: hoangbaocoin@gmail.com', 'LotteryCarcoin', '2018-07-18 12:15:40', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4b2581b8-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0196000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:42:34', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4b9569d6-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4b99e190-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4bf2b041-90f8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'khanhdinh8686@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'khanhdinh8686@gmail.com', '2018-07-26 17:21:07', NULL, NULL, NULL, 3, 'myg3xMiVbLGHg9VJaXQx7aLvp9nqsNPXNU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4c510009-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0178000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:56:55', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4c6941e2-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4c6c1344-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:13:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4c700214-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:46:26', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4c77b31b-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4c7b183d-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4c8664cb-8af2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'truongyen01@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'truongyen01@gmail.com', '2018-07-19 01:23:07', NULL, NULL, NULL, 3, 'mrqPSNNwFni5y7chK8MiiPZAwYfgYUnZUH', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4c8dd8dd-8e8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 15:44:32', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4ca75b00-8e8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'bui.hoangdungtn@gmail.com', 1, 0.0008000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 15:44:32', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4cc4cd9a-8f2a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'danielmokobi35@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: danielmokobi35@gmail.com', 'LotteryCarcoin', '2018-07-24 10:14:04', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4ccd3e73-8998-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-17 08:06:25', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4cd738a7-8d96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenvantuy0204@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nguyenvantuy0204@gmail.com', '2018-07-22 10:02:08', NULL, NULL, NULL, 3, 'mk5LQSdNBuiqmRFnyG1j1HMFTyHKMb5kcS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4cff337e-8b09-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'ogunyemi1409@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'ogunyemi1409@gmail.com', '2018-07-19 04:07:47', NULL, NULL, NULL, 3, 'mkBwwtpHspnavYXgSVmKZWB3WfY5uiYvwu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4d30beab-8b07-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huynhminhduc041097@gmail.com', 'sysAdmin', 1, 0.0030000000, 'Payment ticket lottery', 'huynhminhduc041097@gmail.com', '2018-07-19 03:53:28', NULL, NULL, NULL, 3, 'mid7hc2rzzqYyo2Uh2gsrBsxpDYbFSh3KT', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4d724f71-8fc5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 04:43:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4d7450c1-8fc5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'bui.hoangdungtn@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 04:43:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4d8b72d2-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:51:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4d8d8738-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:51:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4dd33439-8720-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 04:42:24', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4e981ec3-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:34:42', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4e9ae4cc-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:34:42', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4f04d96a-8954-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0008320000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-16 23:59:43', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4f0e5cb5-8f26-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nhan.luong@bigbrothers.gold', 'sysAdmin', 1, 0.0022000000, 'Payment ticket lottery', 'nhan.luong@bigbrothers.gold', '2018-07-24 09:45:30', NULL, NULL, NULL, 3, 'moKCSBMxWE86Qtojno9Mza4ghaeTgucS5P', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4f21d7d3-8aa1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'zon.zip.9@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: zon.zip.9@gmail.com', 'LotteryCarcoin', '2018-07-18 15:43:22', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4f5efb29-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:42:42', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4fbc32f5-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:29:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4fbe98c9-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0010000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:29:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('4fc296d4-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:31:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('500c440b-8c7d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'quanghaupro79@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'quanghaupro79@gmail.com', '2018-07-21 00:30:44', NULL, NULL, NULL, 3, 'mfXkcp1CgC1eaqgqQKEEbyCUcagWeJGmik', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5012348e-8b3e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nvn3007@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nvn3007@gmail.com', 'LotteryCarcoin', '2018-07-19 10:27:15', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('50192a82-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:46:32', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('50704f93-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5072bb46-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('50a9fc46-9086-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 03:45:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('50af0e43-9086-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'daisy.k@cscjackpot.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 03:45:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('50b1190a-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:13:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('50b4faf5-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:13:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('50c04f34-8fac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-25 01:44:42', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('50e62229-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:55', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('50e92b27-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:22:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('510f7b69-8bf8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-20 08:38:43', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('511a5d9f-882d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'toan8181@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: toan8181@gmail.com', 'LotteryCarcoin', '2018-07-15 12:48:05', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('512f3758-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:51:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5131874d-8db6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:51:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('51684e06-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:32:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('516af58b-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:32:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('51cce710-8967-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'hoangbhvd@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: hoangbhvd@gmail.com', 'LotteryCarcoin', '2018-07-17 02:15:48', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('51ce5775-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0188000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:57:05', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('51d3422c-8b0f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'longlucky69@gmail.com', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'longlucky69@gmail.com', '2018-07-19 04:50:52', NULL, NULL, NULL, 3, 'n129xQ5YhvPYnLQ12dhb3er7DiPr1bovMy', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('521a11b9-8a7d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'haquynhanhlsls@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'haquynhanhlsls@gmail.com', '2018-07-18 11:25:46', NULL, NULL, NULL, 3, 'mzhrB3siKL2jw1QQWSR7vB4iTst9e7kfxZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('52259da5-8e8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 15:44:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('522944fc-8e8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'bui.hoangdungtn@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 15:44:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('52acef50-8e7b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-23 13:21:32', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('52b7507b-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:11:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('52cd1681-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:11:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5327fda3-90f8-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nghiabtc888@gmail.com', NULL, 1, 0.9795000000, 'Lottery With Draw', 'nghiabtc888@gmail.com', '2018-07-26 17:21:19', NULL, NULL, NULL, 1, '17Hv9ZYkzTkkFPLe7yvz1GN1CJ2q7C 11w4', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5355dc06-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:46:37', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5375d970-8fc5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 04:43:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('53780908-8fc5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'bui.hoangdungtn@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 04:43:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('539d7c5e-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:42:49', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('53efb518-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:50:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('53f25d5c-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:50:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5433d508-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:52', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5436d817-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:52', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('54500ff1-8a85-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'Luongthuthuy.2992.dn@gmail.com', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'Luongthuthuy.2992.dn@gmail.com', '2018-07-18 12:23:05', NULL, NULL, NULL, 3, 'msaesNmz1mXN2EXDA5MV9eKpPHXtP1bGpH', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('548de4be-8b48-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'khoivk86@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: khoivk86@gmail.com', 'LotteryCarcoin', '2018-07-19 11:38:58', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('54d9f6a9-8998-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-17 08:06:38', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('54de0ab9-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:34:52', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('54e129bb-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:34:52', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5532b270-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('55384131-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('556c2a55-8e54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'giangitman@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: giangitman@gmail.com', 'LotteryCarcoin', '2018-07-23 08:42:26', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('55ae1afa-8dac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 12:39:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('55b0b089-8dac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'daisy.k@cscjackpot.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 12:39:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5603a642-8e81-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'doanthithunguyet4@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: doanthithunguyet4@gmail.com', 'LotteryCarcoin', '2018-07-23 14:04:35', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('561755dc-8e8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 15:44:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('561ac7ef-8e8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'bui.hoangdungtn@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 15:44:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('568f7be0-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:46:43', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('568ffeaf-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('56927800-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0199000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('569bc8d6-8cef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-21 14:06:58', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('56c4bc91-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:32:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('56c77f56-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:32:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('56fde74f-8aa1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-18 15:43:36', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('575f8441-8a57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0150000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:53:54', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('57846620-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:42:55', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5785561d-914b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'liketocdo.com@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: liketocdo.com@gmail.com', 'LotteryCarcoin', '2018-07-27 03:15:35', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('578fc377-8e6a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyentuanhung2828@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nguyentuanhung2828@gmail.com', '2018-07-23 11:19:59', NULL, NULL, NULL, 3, 'n4fAJYGPrYqtaR8wqYmsHnnsniFZWP89rS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('57c6dbd3-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:13:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('57cbaa5c-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:13:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('57ce3eec-868e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'thanhha12196@gmail.com', 'sysAdmin', 1, 0.0120000000, 'Payment ticket lottery', 'thanhha12196@gmail.com', '2018-07-13 11:17:35', NULL, NULL, NULL, 3, 'msPJGc8ShngtGnmhyKqBNk7e9Dn4SGmoY3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5821e94a-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('58243932-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:15:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5824a656-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0182000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:57:15', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('582b5b9f-8fb4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'buiducquyen1983@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: buiducquyen1983@gmail.com', 'LotteryCarcoin', '2018-07-25 02:42:11', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('58304da1-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5833a909-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('58ac1bfe-896c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-17 02:51:47', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('58e63816-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('58e8cbba-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('595e9326-8b27-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'buithanhhoa0309@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: buithanhhoa0309@gmail.com', 'LotteryCarcoin', '2018-07-19 07:42:52', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('598ab023-8a45-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hieplandcoin.tn@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hieplandcoin.tn@gmail.com', '2018-07-18 04:45:06', NULL, NULL, NULL, 3, 'mnfe7BigPEgTGaurWSHPqLNykBBKHqyxrA', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('59e80448-8a45-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hieplandcoin.tn@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hieplandcoin.tn@gmail.com', '2018-07-18 04:45:07', NULL, NULL, NULL, 3, 'mnfe7BigPEgTGaurWSHPqLNykBBKHqyxrA', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('59f07879-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:46:48', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5a376f32-911b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:32:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5a7f545f-8a88-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'hoang0977671567@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: hoang0977671567@gmail.com', 'LotteryCarcoin', '2018-07-18 12:44:44', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5a94b096-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5a9a16f7-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5aea6797-8c23-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'myphamquyenvinh@gmail.com', NULL, 1, 0.9985000000, 'Lottery With Draw', 'myphamquyenvinh@gmail.com', '2018-07-20 13:46:48', NULL, NULL, NULL, 1, '36d1th98Vcff47DNWTRrK79X4CVeKNw4q8', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5b032f37-8af2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'truongyen01@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'truongyen01@gmail.com', '2018-07-19 01:23:32', NULL, NULL, NULL, 3, 'mrqPSNNwFni5y7chK8MiiPZAwYfgYUnZUH', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5b15cf73-8e8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 15:44:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5b1a2d31-8e8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'bui.hoangdungtn@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 15:44:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5b42b257-8d5b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 03:00:12', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5b510a90-8d5b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0199000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 03:00:12', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5b6f410c-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:39:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5b72c93e-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0019000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:39:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5bc6e235-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:43:02', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5bc8e0ec-8b07-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huynhminhduc041097@gmail.com', 'sysAdmin', 1, 0.0018000000, 'Payment ticket lottery', 'huynhminhduc041097@gmail.com', '2018-07-19 03:53:52', NULL, NULL, NULL, 3, 'mid7hc2rzzqYyo2Uh2gsrBsxpDYbFSh3KT', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5c1c7236-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5c213a09-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5c41bc41-8a33-11e8-8c19-00163ec5394d', 1, 0, 1, 0, 1, NULL, 'luong.trungnhan2812@gmail.com', 1, 1.1000000000, NULL, 'BTCNetwork', '2018-07-18 02:36:20', 'BTCNetwork', '2018-07-18 02:55:16', NULL, 3, 'n1UVmpTjLTcJ2usec28D3sYWqzNhtf5aTf', '02ea92f01f7b25d4023025cf68ae32e1198b1f3e4f034d6888f3f675965a36ea', 0.00000, 0.0000000000, NULL, 2);
INSERT INTO `transaction` VALUES ('5c52b26e-868f-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-13 11:24:52', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5cfd7a35-8dac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 12:40:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5d004952-8dac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'daisy.k@cscjackpot.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 12:40:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5d209c17-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:46:54', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5dccd8d1-8830-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'yocoin.vn@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'yocoin.vn@gmail.com', '2018-07-15 13:09:54', NULL, NULL, NULL, 3, 'mv2rZiawxhzBqtqB6nw3wnUgr9rdh4bhmi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5e593591-8915-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'ndtrung84@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: ndtrung84@gmail.com', 'LotteryCarcoin', '2018-07-16 16:29:10', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5e7a7274-8cc1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuong0981148176@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'cuong0981148176@gmail.com', '2018-07-21 08:37:54', NULL, NULL, NULL, 3, 'mzaciJaJMGTkHXwE2WKYutpWXoi6tHnP39', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5e7bfa7f-8c00-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'phamdung1080@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'phamdung1080@gmail.com', '2018-07-20 09:36:22', NULL, NULL, NULL, 3, 'mp8Ffun3KMxmrfEwusa9EWUcmz1P7c8CZN', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5e9a383f-8fc5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-25 04:44:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5e9c504b-8d97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenvantuy0204@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nguyenvantuy0204@gmail.com', '2018-07-22 10:09:47', NULL, NULL, NULL, 3, 'mk5LQSdNBuiqmRFnyG1j1HMFTyHKMb5kcS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5e9dba75-8fc5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'bui.hoangdungtn@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-25 04:44:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5eb9f92b-8a98-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-18 14:39:23', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5ec4088d-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:29', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5ec651ee-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:29', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5edbca52-8e6a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyentuanhung2828@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nguyentuanhung2828@gmail.com', '2018-07-23 11:20:11', NULL, NULL, NULL, 3, 'n4fAJYGPrYqtaR8wqYmsHnnsniFZWP89rS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5ee5e185-8955-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fsuphihung@gmail.com', 'sysAdmin', 1, 0.0008000000, 'Payment ticket lottery', 'fsuphihung@gmail.com', '2018-07-17 00:07:19', NULL, NULL, NULL, 3, 'moHQynVxdQW6dvno6Lb5HwSuvCvqCsZ8JR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5efe7f58-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:39:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5f011c13-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:39:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5f1d546d-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:50:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5f200a44-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:50:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5f562393-8c22-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'pecoi1988@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'pecoi1988@gmail.com', '2018-07-20 13:39:46', NULL, NULL, NULL, 3, 'mu8mDyUjwi4uon6Ldkc4Ghkb3ZLTEEQQQM', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5f964829-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:57:28', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5f99e307-894b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 22:55:45', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5fb9447b-8a5f-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'Tranducduy87@gmail.com', NULL, 1, 0.9895000000, 'Lottery With Draw', 'Tranducduy87@gmail.com', '2018-07-18 07:51:24', NULL, NULL, NULL, 1, 'mvF21EmtQHiBt94ZTj7rhtfkVyWuiNYwGa', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5ff1d3b0-8ef9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:23:51', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('5ff4a6d9-8a85-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'Luongthuthuy.2992.dn@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'Luongthuthuy.2992.dn@gmail.com', '2018-07-18 12:23:25', NULL, NULL, NULL, 3, 'msaesNmz1mXN2EXDA5MV9eKpPHXtP1bGpH', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6008eae3-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:30:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('600b8409-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0008000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:30:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('603b7831-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:17', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('603e2569-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:17', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6118b6af-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:40:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('611c09f9-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:40:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('61415bf8-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:47:01', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6187a8a2-8e37-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanhdat0111@gmail.com', 'sysAdmin', 1, 0.0016000000, 'Payment ticket lottery', 'lethanhdat0111@gmail.com', '2018-07-23 05:15:11', NULL, NULL, NULL, 3, 'msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('618e698d-8dac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 12:40:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('619140dd-8dac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'daisy.k@cscjackpot.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 12:40:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6218aef3-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:32:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('621bae1a-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:32:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6287c7f8-8a5c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'shandypham2017@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: shandypham2017@gmail.com', 'LotteryCarcoin', '2018-07-18 07:30:00', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('628eb9b7-8992-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuarangmuoi27@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'cuarangmuoi27@gmail.com', '2018-07-17 07:24:04', NULL, NULL, NULL, 3, 'mkkusAPvz6HR4pbWKdLE8bjDxqrMQNT3zs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('62dbe6b0-8a73-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenly796@gmail.com', 'sysAdmin', 1, 0.0008000000, 'Payment ticket lottery', 'nguyenly796@gmail.com', '2018-07-18 10:14:39', NULL, NULL, NULL, 3, 'mqpYrS6fK9Uq28PZN9RUXqAc8hoY2y7RZY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('630a4f60-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:39:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('630f06e3-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:39:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('63a665f4-8d7c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'dohieuthao1994@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'dohieuthao1994@gmail.com', '2018-07-22 06:56:39', NULL, NULL, NULL, 3, 'mrJcbMVwz7vFaA5xipD9bVVa1Nx51kdHf7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('63a8b291-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:38', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('63ab6044-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:38', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('63aff5af-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('63b2cb02-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('63bd8d6f-8ae4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'thaingocthoai1234@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'thaingocthoai1234@gmail.com', '2018-07-18 23:43:33', NULL, NULL, NULL, 3, 'mkQqxpTWWyUVjJvRAhmvm3t63ioj75mjMP', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('63c052c2-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:35:17', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('63c33137-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:35:17', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6442d358-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('64454017-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('647cbe1b-8fc5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-25 04:44:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('647ff5cb-8fc5-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'bui.hoangdungtn@gmail.com', 1, 0.0008000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-25 04:44:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('64bdb348-8ef9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:23:59', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('64f861b0-8a7f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'ngonhaidang93boxing@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: ngonhaidang93boxing@gmail.com', 'LotteryCarcoin', '2018-07-18 11:40:36', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('658c7cee-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:16:51', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6592594b-8946-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'fredhlordze103@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: fredhlordze103@gmail.com', 'LotteryCarcoin', '2018-07-16 22:20:07', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('65e4d7cc-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:57:38', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('664b8bda-8dc3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tuyendinhhb@gmail.com', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'tuyendinhhb@gmail.com', '2018-07-22 15:24:58', NULL, NULL, NULL, 3, 'mpXHwF5MKv3UGtdgUFpQQgPjXQ4WRmwuaf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6674df85-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:30:26', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('66773e63-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:30:26', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('66a61d48-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('66a8c8a0-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('672651d3-8d81-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'leanhdung1434@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: leanhdung1434@gmail.com', 'LotteryCarcoin', '2018-07-22 07:32:32', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6765abcb-8af9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-19 02:13:59', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('67da4785-8d54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuarangmuoi25@gmail.com', 'sysAdmin', 1, 0.0024000000, 'Payment ticket lottery', 'cuarangmuoi25@gmail.com', '2018-07-22 02:10:26', NULL, NULL, NULL, 3, 'mrNZSck1VT33joNUkR3rjtXDk7dtWRZF6j', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('67f303bf-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:35:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('67f70a1d-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:35:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('68302072-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('68327ec4-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('683d7656-8f2b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'mr.lehieu1@gmail.com', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'mr.lehieu1@gmail.com', '2018-07-24 10:22:00', NULL, NULL, NULL, 3, 'ms7c7TycFwM2Wv9yANA9xxMF8wqkZgRoAr', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('68566895-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('685b7885-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('686e4351-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('68738cbe-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6898074a-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:47:13', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('68b498ab-8ef9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:24:06', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('68f98a7b-8dac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 12:40:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('68fc29b8-8dac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'daisy.k@cscjackpot.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 12:40:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('691da9cf-8cc1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuong0981148176@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'cuong0981148176@gmail.com', '2018-07-21 08:38:12', NULL, NULL, NULL, 3, 'mzaciJaJMGTkHXwE2WKYutpWXoi6tHnP39', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('69650e1d-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:05', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('696884e1-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:05', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('69a34c27-8f2a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'danielmokobi35@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'danielmokobi35@gmail.com', '2018-07-24 10:14:53', NULL, NULL, NULL, 3, 'mqJtYUfFTEojpGDFWnbtbAjQsAE5w2aBas', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('69a48f41-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:05', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('69a7f01b-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:05', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('69bd0d25-8735-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'k.duchappy@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: k.duchappy@gmail.com', 'LotteryCarcoin', '2018-07-14 07:13:31', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('69c12287-8b27-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'buithanhhoa0309@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'buithanhhoa0309@gmail.com', '2018-07-19 07:43:20', NULL, NULL, NULL, 3, 'mw7pruDBaxH2KDmrn4Dvew7Zq9br7y32pk', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6a765647-8f2a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'danielmokobi35@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'danielmokobi35@gmail.com', '2018-07-24 10:14:54', NULL, NULL, NULL, 3, 'mqJtYUfFTEojpGDFWnbtbAjQsAE5w2aBas', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6af4bce6-8b3f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'manhthuyen1997@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'manhthuyen1997@gmail.com', '2018-07-19 10:35:10', NULL, NULL, NULL, 3, 'mibwvjcvTkWvovddD6NKoqpDjqanoCLSvN', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6b632433-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:14:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6b65dc12-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:14:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6b90fde3-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0002000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:32:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6b94417c-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:32:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6b9b5ca4-8a65-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'quangnguyen0293@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'quangnguyen0293@gmail.com', '2018-07-18 08:34:40', NULL, NULL, NULL, 3, 'ms4kgAkd9DVbBebE2NdQrLQQDBU3RJSMee', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6bf2faa7-8bf3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-20 08:03:41', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6c24c202-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6c2730d1-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6c53061e-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:35:32', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6c55e4bd-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0010000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:35:32', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6c88c169-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:52', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6c8c03b5-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:14:52', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6cae617d-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:17:03', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6d19591a-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6d1bf9f5-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6d2ce5ff-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:30:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6d2f1a9c-8db3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:30:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6daca6db-8fa8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'truongyen01@gmail.com', 'sysAdmin', 1, 0.0018000000, 'Payment ticket lottery', 'truongyen01@gmail.com', '2018-07-25 01:16:53', NULL, NULL, NULL, 3, 'mrqPSNNwFni5y7chK8MiiPZAwYfgYUnZUH', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6dcaa206-8ef9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:24:14', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6dcffbcf-8ed4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0105840000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-23 23:59:23', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6df5f90f-89a0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hieplandcoin.tn@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'hieplandcoin.tn@gmail.com', '2018-07-17 09:04:36', NULL, NULL, NULL, 3, 'mnfe7BigPEgTGaurWSHPqLNykBBKHqyxrA', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6e142aee-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6e16903c-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0019000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6e2a66df-8a25-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'kimthithuy17061983@gmail.com', NULL, 1, 0.9985000000, 'Lottery With Draw', 'kimthithuy17061983@gmail.com', '2018-07-18 00:56:37', NULL, NULL, NULL, 1, '15poUDHnUZ2fQWDDbtPiR6we93Q4i2W9Yt', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6e40ab46-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:47:22', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6eb13fd9-9119-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'quanghaupro79@gmail.com', NULL, 1, 0.9895000000, 'Lottery With Draw', 'quanghaupro79@gmail.com', '2018-07-26 21:18:19', NULL, NULL, NULL, 1, '3LyEJ2dZrjQHvtbsV2FTx5aQMvV48sohzZ', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6f02c587-8aa1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-18 15:44:16', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6f265a04-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:10:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6f298313-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'nguyenle130690@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:10:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6f3e361d-8e3f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'quanghaupro79@gmail.com', 'sysAdmin', 1, 0.0008000000, 'Payment ticket lottery', 'quanghaupro79@gmail.com', '2018-07-23 06:12:50', NULL, NULL, NULL, 3, 'mfXkcp1CgC1eaqgqQKEEbyCUcagWeJGmik', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6fa17b0a-8dc9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangsonbtc68@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangsonbtc68@gmail.com', '2018-07-22 16:08:10', NULL, NULL, NULL, 3, 'mvzn6Na5dkgcFkn5pnfPu2SzRjf8q18Vam', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('6fd0b70c-88a8-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-16 03:29:24', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('702cf4d2-8cc0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyentuanhung2828@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyentuanhung2828@gmail.com', '2018-07-21 08:31:15', NULL, NULL, NULL, 3, 'n4fAJYGPrYqtaR8wqYmsHnnsniFZWP89rS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('70dc47c7-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('70dfea9c-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('710c5253-882f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'toan8181@gmail.com', 'sysAdmin', 1, 0.0012000000, 'Payment ticket lottery', 'toan8181@gmail.com', '2018-07-15 13:03:17', NULL, NULL, NULL, 3, 'n15yYUxvE3Pqpfm5nzHyoajGXk8TG8icAa', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('715362d5-88b3-11e8-8c19-00163ec5394d', 1, 0, 1, 0, 1, NULL, 'duy.duong@bigbrothers.gold', 1, 0.0021484300, NULL, 'BTCNetwork', '2018-07-16 04:48:11', 'BTCNetwork', '2018-07-16 04:52:43', NULL, 3, 'mwDuarjHaYqoniyPbLzjymx1YgaDMkeLQp', '5a31ad8e471c429bc6ebbccd1235d387b15c7bac0fa2ac5e87205ecf0c3a011b', 0.00000, 0.0000000000, NULL, 2);
INSERT INTO `transaction` VALUES ('7161f318-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:14:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('71655329-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:14:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('71f80870-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('71fa9c30-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('720282c8-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:17:12', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('720fbdfd-8d4b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-22 01:06:18', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7212357e-8e6a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'phuctd222@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: phuctd222@gmail.com', 'LotteryCarcoin', '2018-07-23 11:20:43', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('72177bcd-8d4b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-22 01:06:18', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7291b4d2-8a58-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'vuthanghy89@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: vuthanghy89@gmail.com', 'LotteryCarcoin', '2018-07-18 07:01:49', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('72dc752d-8d97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenvantuy0204@gmail.com', 'sysAdmin', 1, 0.0012000000, 'Payment ticket lottery', 'nguyenvantuy0204@gmail.com', '2018-07-22 10:10:21', NULL, NULL, NULL, 3, 'mk5LQSdNBuiqmRFnyG1j1HMFTyHKMb5kcS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('738e1244-8964-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'dany.alliat@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'dany.alliat@gmail.com', '2018-07-17 01:55:16', NULL, NULL, NULL, 3, 'miJ1kLumayBhPmNguzpZy376thUCaSFx2P', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('73a4fb18-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('73a8b812-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:23:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('73fac389-8a5d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'thaingocthoai1234@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: thaingocthoai1234@gmail.com', 'LotteryCarcoin', '2018-07-18 07:37:38', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('740af002-8a33-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 02:37:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('74103ea7-8a33-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'bui.hoangdungtn@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 02:37:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('74147f01-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:47:32', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7431e2ca-8a5f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'tranthien585@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: tranthien585@gmail.com', 'LotteryCarcoin', '2018-07-18 07:51:58', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('75039693-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7506d174-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('75396ca3-90f0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0010000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-26 16:25:01', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('757adcad-8b3f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'manhthuyen1997@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'manhthuyen1997@gmail.com', '2018-07-19 10:35:27', NULL, NULL, NULL, 3, 'mibwvjcvTkWvovddD6NKoqpDjqanoCLSvN', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('75a4e65b-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:17:18', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('75c38e6e-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:51:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('75c5b2a4-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:51:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('76464c1b-89bd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiemductrung0612@gmail.com', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'nghiemductrung0612@gmail.com', '2018-07-17 12:32:26', NULL, NULL, NULL, 3, 'mq6oeWGQpg7wcNKycxaN8VrWkKmybHsn6S', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('766228c9-8e6e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanhdat0111@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'lethanhdat0111@gmail.com', '2018-07-23 11:49:29', NULL, NULL, NULL, 3, 'msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('77103aa2-8cc1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuong0981148176@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'cuong0981148176@gmail.com', '2018-07-21 08:38:36', NULL, NULL, NULL, 3, 'mzaciJaJMGTkHXwE2WKYutpWXoi6tHnP39', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('77a31871-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:14:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('77a640bf-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:14:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('77bb7d09-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:58:08', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('77d1cc13-8e34-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenle130690@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyenle130690@gmail.com', '2018-07-23 04:54:20', NULL, NULL, NULL, 3, 'mmroEqY7mWr3ohCKQZNJ6PL3Hk77AEz5h5', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('78412df8-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7843fab1-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0079000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7854a1ef-8a92-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'lethanh239@icloud.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: lethanh239@icloud.com', 'LotteryCarcoin', '2018-07-18 13:57:09', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7863fe4d-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:24:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7866dd67-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:24:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('789c9be2-8fb7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-25 03:04:34', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('78df3cb3-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:43:51', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('78ee6496-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:41:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('78f11483-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:41:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('78f24fd9-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:35:53', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('78f5d3e5-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:35:53', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7910fbdb-8fb3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungnhan.luong@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'trungnhan.luong@gmail.com', '2018-07-25 02:35:57', NULL, NULL, NULL, 3, 'mrDSBfoQ5Vh7hRJ2sHNzAuF53WpQLejUtZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('79713f55-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7974a97c-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('797f4581-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7981c74a-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:12:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7997ff0f-8cc0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyentuanhung2828@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyentuanhung2828@gmail.com', '2018-07-21 08:31:30', NULL, NULL, NULL, 3, 'n4fAJYGPrYqtaR8wqYmsHnnsniFZWP89rS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('79989d4e-8b3f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nvn3007@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nvn3007@gmail.com', '2018-07-19 10:35:34', NULL, NULL, NULL, 3, 'mx6TwFZpG3AY5XPhFzrPhorT6pyhhnApTz', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('79c3770e-89fe-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'ogunyemi1409@gmail.com', NULL, 1, 0.9975000000, 'Lottery With Draw', 'ogunyemi1409@gmail.com', '2018-07-17 20:17:46', NULL, NULL, NULL, 1, '1CSDoLYNw7GxnSSZQXx37DcDEWy2hfFGjt', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('79d9e726-8cfb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-21 15:33:51', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('79e6f5ab-8b3f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nvn3007@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nvn3007@gmail.com', '2018-07-19 10:35:35', NULL, NULL, NULL, 3, 'mx6TwFZpG3AY5XPhFzrPhorT6pyhhnApTz', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7a18a47c-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:17:25', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7a3ffdc4-8954-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'HIEU.PLAZA@GMAIL.COM', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: HIEU.PLAZA@GMAIL.COM', 'LotteryCarcoin', '2018-07-17 00:00:55', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7aa7f1a2-914b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'liketocdo.com@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'liketocdo.com@gmail.com', '2018-07-27 03:16:34', NULL, NULL, NULL, 3, 'msjWH5tVJ1uwQeCprisGXXQUHJFmEPaRH6', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7b36ec7a-8a33-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 02:37:12', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7b398b78-8a33-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'bui.hoangdungtn@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 02:37:12', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7b5def28-8946-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 22:20:44', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7b66d229-90f0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0010000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-26 16:25:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7b7213bc-8d90-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'khanhdinh8686@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: khanhdinh8686@gmail.com', 'LotteryCarcoin', '2018-07-22 09:20:29', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7ba309c7-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:51:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7bab7adb-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:51:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7bdb5d38-8acb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'tripledxxl@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: tripledxxl@gmail.com', 'LotteryCarcoin', '2018-07-18 20:45:16', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7c26420b-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7c28d87d-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7c658f98-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:24:08', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7c68cf81-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:24:08', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7ca1a9bb-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:58:16', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7ca7cc17-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0002000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:28', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7caa7556-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:28', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7cb4e9b4-8af9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'suk1262@naver.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'suk1262@naver.com', '2018-07-19 02:14:35', NULL, NULL, NULL, 3, 'n24HcLLXidAxejG7uyjNRuByrVDi8ZBkLY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7cc87596-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:35:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7ccc8e51-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:35:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7d05f9f5-8a73-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenly796@gmail.com', 'sysAdmin', 1, 0.0120000000, 'Payment ticket lottery', 'nguyenly796@gmail.com', '2018-07-18 10:15:23', NULL, NULL, NULL, 3, 'mqpYrS6fK9Uq28PZN9RUXqAc8hoY2y7RZY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7d449af1-8963-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'dany.alliat@gmail.com', 'sysAdmin', 1, 0.0016000000, 'Payment ticket lottery', 'dany.alliat@gmail.com', '2018-07-17 01:48:23', NULL, NULL, NULL, 3, 'miJ1kLumayBhPmNguzpZy376thUCaSFx2P', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7d44cdaa-894b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 22:56:35', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7d5aacf2-8972-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanhdat0111@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'lethanhdat0111@gmail.com', '2018-07-17 03:35:45', NULL, NULL, NULL, 3, 'msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7d73ca35-8cef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-21 14:08:03', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7d7ef2f5-8d4d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-22 01:20:56', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7d8ab5e4-8d4b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-22 01:06:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7d8cd685-8d4b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-22 01:06:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7da00e5d-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:21', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7da2b81f-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:21', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7deaace2-8ada-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'manhnshd92@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: manhnshd92@gmail.com', 'LotteryCarcoin', '2018-07-18 22:32:42', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7dfb87ab-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:17:32', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7e0e45e7-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:13:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7e11f1cb-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:13:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7e1ab3f3-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:47:49', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7e7692b4-8e34-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenle130690@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyenle130690@gmail.com', '2018-07-23 04:54:31', NULL, NULL, NULL, 3, 'mmroEqY7mWr3ohCKQZNJ6PL3Hk77AEz5h5', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7e88b457-8e2e-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'vinhnq@geovietnam.com', NULL, 1, 0.1995000000, 'Lottery With Draw', 'vinhnq@geovietnam.com', '2018-07-23 04:11:35', NULL, NULL, NULL, 1, '3Ftb23PhqaoaW5Uzn6nfXXEHJMY4PKfjYf', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7ea532e8-8d13-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'ngphuongthanh86@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'ngphuongthanh86@gmail.com', '2018-07-21 18:25:47', NULL, NULL, NULL, 3, 'n1fuXXppMLZpiHA1G4HX8N4XnHUr9kjKy2', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7ebf91a2-90fb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0192000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:44:01', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7f86c476-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:42', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7f8964b8-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:42', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('7f9e06a5-8bfd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nhan.luong@bigbrothers.gold', 'sysAdmin', 1, 0.0008000000, 'Payment ticket lottery', 'nhan.luong@bigbrothers.gold', '2018-07-20 09:15:49', NULL, NULL, NULL, 3, 'moKCSBMxWE86Qtojno9Mza4ghaeTgucS5P', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('800523ba-8e81-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'doanthithunguyet4@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'doanthithunguyet4@gmail.com', '2018-07-23 14:05:45', NULL, NULL, NULL, 3, 'mgCRmgjRxvHJwMgYnvcR3w5JQzmxktPuks', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('801d362f-8a25-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'kimthithuy17061983@gmail.com', NULL, 1, 0.9985000000, 'Lottery With Draw', 'kimthithuy17061983@gmail.com', '2018-07-18 00:57:07', NULL, NULL, NULL, 1, '15poUDHnUZ2fQWDDbtPiR6we93Q4i2W9Yt', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('80210182-8c97-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-21 03:38:12', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('80235dab-8b86-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'minhquan060917@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: minhquan060917@gmail.com', 'LotteryCarcoin', '2018-07-19 19:03:59', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('805ec842-90f0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0010000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-26 16:25:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('806ceb1b-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:24:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('80702bde-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:24:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8098471e-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('809c8e0a-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('80ba1b8d-8cf6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-21 14:58:15', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('80cd7e88-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('80d06eeb-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('80d61ca0-8dc2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'huy36391@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: huy36391@gmail.com', 'LotteryCarcoin', '2018-07-22 15:18:33', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8200b3be-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:17:39', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8226f041-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:13:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('822a03ec-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:13:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('82565326-8d54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-22 02:11:11', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8277a0e2-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0192000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:58:26', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('828eb854-8865-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'daisy.k@cscjackpot.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: daisy.k@cscjackpot.com', 'LotteryCarcoin', '2018-07-15 19:30:19', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('82a6dd55-89f3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0053120000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-17 18:59:19', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('82b60633-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:51:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('82b88523-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:51:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('82da029a-90aa-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-26 08:04:19', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('82dae323-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('82deb7d4-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8338ffdc-8b27-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'buithanhhoa0309@gmail.com', 'sysAdmin', 1, 0.0008000000, 'Payment ticket lottery', 'buithanhhoa0309@gmail.com', '2018-07-19 07:44:02', NULL, NULL, NULL, 3, 'mw7pruDBaxH2KDmrn4Dvew7Zq9br7y32pk', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('839672b5-8bf7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-20 08:32:58', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('84368e1d-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('84392268-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8456b5af-8dca-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'khoivk86@gmail.com', 'sysAdmin', 1, 0.0018000000, 'Payment ticket lottery', 'khoivk86@gmail.com', '2018-07-22 16:15:55', NULL, NULL, NULL, 3, 'mrnLYNuX6kkWfUTtMt2NuMhNCPj4rxLjoc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('84a6332a-8975-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'sanakiho236@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: sanakiho236@gmail.com', 'LotteryCarcoin', '2018-07-17 03:57:26', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('84b553fe-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:48:00', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('84f7145d-90ea-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanh239@icloud.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'lethanh239@icloud.com', '2018-07-26 15:42:30', NULL, NULL, NULL, 3, 'mzR5JTq2BFxZtzHT8482vhi86FuPwj7X1M', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('850a1ce3-8d4c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-22 01:13:59', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('857637c7-8faf-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tanhoaxa2004@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'tanhoaxa2004@gmail.com', '2018-07-25 02:07:39', NULL, NULL, NULL, 3, 'mxSqRd7z17gQ7zRYbkZ9oJa7EkJ3TFDrEZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('85d83636-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:13:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('85daa145-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:13:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('85e93da2-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:53', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('85eb8b04-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0079000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:53', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('863615f9-8e34-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenle130690@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyenle130690@gmail.com', '2018-07-23 04:54:44', NULL, NULL, NULL, 3, 'mmroEqY7mWr3ohCKQZNJ6PL3Hk77AEz5h5', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('864b25f7-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:17:46', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('86a9137c-8b49-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sucmanhcuabanguoi@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'sucmanhcuabanguoi@gmail.com', '2018-07-19 11:47:31', NULL, NULL, NULL, 3, 'mzspFYoc7N2ZfWnHU3nZJQRZDcb1PXjSjJ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('86ec803b-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8701b1c1-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('87216e42-90f8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:22:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8724ae46-90f8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:22:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('872bfafb-89a0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hieplandcoin.tn@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'hieplandcoin.tn@gmail.com', '2018-07-17 09:05:19', NULL, NULL, NULL, 3, 'mnfe7BigPEgTGaurWSHPqLNykBBKHqyxrA', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('87c558a6-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('87c9f4f4-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('87f5931d-88bf-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-16 06:14:43', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8809fddc-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0192000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:58:36', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('888282b0-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:17:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('88854530-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:17:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8891a258-90a8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-26 07:50:09', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('89362184-8f80-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'globallook2020@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: globallook2020@gmail.com', 'LotteryCarcoin', '2018-07-24 20:31:19', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('893d03fd-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('89401ab9-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:40:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('89ced18d-88bf-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nhan.luong@bigbrothers.gold', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nhan.luong@bigbrothers.gold', 'LotteryCarcoin', '2018-07-16 06:14:46', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('89cf2870-8b10-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'longlucky69@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'longlucky69@gmail.com', '2018-07-19 04:59:35', NULL, NULL, NULL, 3, 'n129xQ5YhvPYnLQ12dhb3er7DiPr1bovMy', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8a0ece95-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:48:09', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8a1d3bc6-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 03:13:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8a1f7d9d-8eef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 03:13:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8aa0e668-8a77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lecautoananh@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'lecautoananh@gmail.com', '2018-07-18 10:44:23', NULL, NULL, NULL, 3, 'n2NqcRBUe3SSPenm6bkyuH3D5UPuAdjxQB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8ac8c878-889d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'phim.ngo@bigbrothers.gold', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: phim.ngo@bigbrothers.gold', 'LotteryCarcoin', '2018-07-16 02:11:25', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8afc0fdb-87c6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0002000000, 'Payment fee Withdraw', '7/14/2018 7:00:00 PM', '2018-07-15 00:32:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8b02e16f-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8b057907-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8b0b39f3-87c6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hathanhpc20@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/14/2018 7:00:00 PM', '2018-07-15 00:32:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8b48a281-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8b4b4282-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8b99f86f-8a88-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoang0977671567@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoang0977671567@gmail.com', '2018-07-18 12:46:06', NULL, NULL, NULL, 3, 'miVcyRgJaLHWuYTiMoUGVgtat8EiA8u9gM', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8b9f7749-899f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'hieplandcoin.tn@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: hieplandcoin.tn@gmail.com', 'LotteryCarcoin', '2018-07-17 08:58:17', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8bb209f3-8db4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-22 13:38:38', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8bc833c4-8af2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'truongyen01@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'truongyen01@gmail.com', '2018-07-19 01:24:53', NULL, NULL, NULL, 3, 'mrqPSNNwFni5y7chK8MiiPZAwYfgYUnZUH', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8c298c71-8aa6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0060000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-18 16:20:52', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8c35e4e9-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:17:26', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8c38ffc0-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:17:26', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8c758471-8bf7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-20 08:33:13', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8c8a198a-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:17:56', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8c945e4c-8978-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'bui.hoangdungtn@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: bui.hoangdungtn@gmail.com', 'LotteryCarcoin', '2018-07-17 04:19:08', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8cb1b774-8ab8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'Vothiyenoanh2468@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: Vothiyenoanh2468@gmail.com', 'LotteryCarcoin', '2018-07-18 18:29:44', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8cf5ff3c-8a7b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'maiduynhat1987@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: maiduynhat1987@gmail.com', 'LotteryCarcoin', '2018-07-18 11:13:05', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8cfe4812-8e34-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenle130690@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyenle130690@gmail.com', '2018-07-23 04:54:56', NULL, NULL, NULL, 3, 'mmroEqY7mWr3ohCKQZNJ6PL3Hk77AEz5h5', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8d5294c6-868d-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'hathanhpc20@gmail.com', NULL, 1, 0.1885000000, 'Lottery With Draw', 'hathanhpc20@gmail.com', '2018-07-13 11:11:55', 'accouting', '2018-07-14 02:43:00', NULL, 3, 'msPJGc8ShngtGnmhyKqBNk7e9Dn4SGmoY3', 'ddc7ce7b6cbe76a5ff2d0da2b9cb2aa5143dd196b8bee1776aa61ed1348ded2f', 0.00050, 0.0000033900, NULL, 0);
INSERT INTO `transaction` VALUES ('8de901c2-8faf-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tanhoaxa2004@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'tanhoaxa2004@gmail.com', '2018-07-25 02:07:54', NULL, NULL, NULL, 3, 'mxSqRd7z17gQ7zRYbkZ9oJa7EkJ3TFDrEZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8e0cdac1-90f8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:22:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8e0f7dcb-90f8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:22:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8e0fa880-9119-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'quanghaupro79@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'quanghaupro79@gmail.com', '2018-07-26 21:19:12', NULL, NULL, NULL, 1, '3LyEJ2dZrjQHvtbsV2FTx5aQMvV48sohzZ', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8e4dd8f0-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8e52c7ee-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:33:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8e7e260d-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0194000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:58:46', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8e7f59de-8a84-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbaocoin@gmail.com', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'hoangbaocoin@gmail.com', '2018-07-18 12:17:33', NULL, NULL, NULL, 3, 'n1BV2rAbvcXPSZXJBt22QnYQATThqwEqtM', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8e95b972-89c3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'thanhhungt31@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: thanhhungt31@gmail.com', 'LotteryCarcoin', '2018-07-17 13:16:03', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8ea2f066-8a69-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'langtutomboyls@gmail.com', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'langtutomboyls@gmail.com', '2018-07-18 09:04:17', NULL, NULL, NULL, 3, 'mu2sMJouXK655NEuLSe6Gwgc86TLdgRujy', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8ee7d261-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nghiabtc888@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nghiabtc888@gmail.com', 'LotteryCarcoin', '2018-07-26 17:15:50', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8f06c93d-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:08', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8f094843-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:08', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8f255a82-8cef-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-21 14:08:33', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8f27e137-90a8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0084000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-26 07:50:20', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8f87a63b-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:48:18', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8fa2428c-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8fa49877-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('8fb4d589-87cf-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-15 01:36:57', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('900187ce-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:52:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('90054a7a-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:52:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('90357e09-87dc-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'ogunyemi1409@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: ogunyemi1409@gmail.com', 'LotteryCarcoin', '2018-07-15 03:10:01', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('909b6718-894c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0050000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 23:04:17', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('90c1a637-8e83-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'doanthithunguyet4@gmail.com', NULL, 1, 0.9975000000, 'Lottery With Draw', 'doanthithunguyet4@gmail.com', '2018-07-23 14:20:32', NULL, NULL, NULL, 1, '3JJoRStLxWE19kRegDAxCabKk4Vvn8pwWZ', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('90c7413f-8d9d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenvantuy0204@gmail.com', 'sysAdmin', 1, 0.0024000000, 'Payment ticket lottery', 'nguyenvantuy0204@gmail.com', '2018-07-22 10:54:08', NULL, NULL, NULL, 3, 'mk5LQSdNBuiqmRFnyG1j1HMFTyHKMb5kcS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('90df644c-8735-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'k.duchappy@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'k.duchappy@gmail.com', '2018-07-14 07:14:36', NULL, NULL, NULL, 3, 'mpQRw8opEvw56g7ynd8H8eALatho6RKi3y', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('90f30f0e-8954-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'HIEU.PLAZA@GMAIL.COM', '2018-07-17 00:01:33', NULL, NULL, NULL, 3, 'mt6G6Ef8P78UQ1hC1YHBa9sEWKpwTdJpx7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9151cf63-8962-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'tanhoaxa2004@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: tanhoaxa2004@gmail.com', 'LotteryCarcoin', '2018-07-17 01:41:47', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9211c315-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 02:17:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9214658d-8e1e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 02:17:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9234e795-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:18:06', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('92404baa-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:52:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9242ea48-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:52:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9243815d-8e7d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nguyenhongsonhn91@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nguyenhongsonhn91@gmail.com', 'LotteryCarcoin', '2018-07-23 13:37:38', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('928dff8c-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('92904f04-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('931b00d7-8972-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanhdat0111@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'lethanhdat0111@gmail.com', '2018-07-17 03:36:22', NULL, NULL, NULL, 3, 'msMDcxboM1HNhgEUrJqf6efTgNKtXopmFR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('934dae9c-8726-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'kienduc94@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: kienduc94@gmail.com', 'LotteryCarcoin', '2018-07-14 05:27:18', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('93a37569-8e34-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenle130690@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyenle130690@gmail.com', '2018-07-23 04:55:07', NULL, NULL, NULL, 3, 'mmroEqY7mWr3ohCKQZNJ6PL3Hk77AEz5h5', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('93d06886-8b41-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'thanhhungt31@gmail.com', 'sysAdmin', 1, 0.0014000000, 'Payment ticket lottery', 'thanhhungt31@gmail.com', '2018-07-19 10:50:37', NULL, NULL, NULL, 3, 'mp7k5DQvtxavh1DU2ScMCrRTqBQeZYQQRJ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('93d5eb22-8b30-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nguyendinhhai932@gmail.com', NULL, 1, 0.9987000000, 'Lottery With Draw', 'nguyendinhhai932@gmail.com', '2018-07-19 08:48:56', NULL, NULL, NULL, 1, '3FXdxvjY6qPfaQdEwPKzrdgQuVb6bntsEt', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('93f3bf84-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:15:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('94775735-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:39', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9479a4bc-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:39', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('94a0ab2b-8915-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'ndtrung84@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'ndtrung84@gmail.com', '2018-07-16 16:30:41', NULL, NULL, NULL, 3, 'muQHbeLQMJfiUB2UTwBrB5u4PRqYqmwWBz', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('94cdaec1-8e31-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'huy36391@gmail.com', NULL, 1, 0.9095000000, 'Lottery With Draw', 'huy36391@gmail.com', '2018-07-23 04:33:40', NULL, NULL, NULL, 1, '3BMEXmSyqy4E1MmEtwecgETV1q4D2pF4ZP', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9568ad4a-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:01', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('958b9410-8fb3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huy36391@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'huy36391@gmail.com', '2018-07-25 02:36:44', NULL, NULL, NULL, 3, 'mfgsJiTPiRgwpxacpwitdWcrWg2iDKZV4H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9592fd8e-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:58:58', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('95a51812-8d54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-22 02:11:43', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('95c0e5cc-8dbe-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'mrkhanh831@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'mrkhanh831@gmail.com', '2018-07-22 14:50:30', NULL, NULL, NULL, 3, 'mnEXMijjAudS9jbG6AZF5hK9CMnwhi8Gnu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('95c6a870-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:48:29', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('96136ae3-868c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0022000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-13 11:05:00', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('96208fc8-8f52-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0188000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-24 15:02:27', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('962635da-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('962cce12-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('962f16ad-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0019000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('970a723c-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('970ecca8-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:18:14', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('97b3ca86-8a5d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'thaingocthoai1234@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'thaingocthoai1234@gmail.com', '2018-07-18 07:38:38', NULL, NULL, NULL, 3, 'mkQqxpTWWyUVjJvRAhmvm3t63ioj75mjMP', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('98168a15-8c1c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-20 12:58:24', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9829e823-8b1c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'quytu379@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'quytu379@gmail.com', '2018-07-19 06:25:53', NULL, NULL, NULL, 3, 'mwnVKakRYEProt7aCn3863BHSLT2pegoub', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('988222ae-8b0f-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'zon.zip.9@gmail.com', NULL, 1, 0.4995000000, 'Lottery With Draw', 'zon.zip.9@gmail.com', '2018-07-19 04:52:50', NULL, NULL, NULL, 1, '39HMNEZ38WZvwCbZ8siuS4P1Ce4hw2WKZy', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('98b31684-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('98b567f5-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('98cb0383-8a70-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'binhnguyentyphu@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: binhnguyentyphu@gmail.com', 'LotteryCarcoin', '2018-07-18 09:54:41', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('98d5d251-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:15:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('98d81539-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0008000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:15:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('994a90ed-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:25', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('994d24dc-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:25', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9982c961-8e82-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'doanthithunguyet4@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'doanthithunguyet4@gmail.com', '2018-07-23 14:13:37', NULL, NULL, NULL, 3, 'mgCRmgjRxvHJwMgYnvcR3w5JQzmxktPuks', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('99eec17f-89c3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'thanhhungt31@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'thanhhungt31@gmail.com', '2018-07-17 13:16:22', NULL, NULL, NULL, 3, 'mp7k5DQvtxavh1DU2ScMCrRTqBQeZYQQRJ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('99f9573b-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('99fc3c85-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9a4a205c-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:42:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9a4d327f-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:42:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9a54dc17-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0196000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:59:06', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9a8e1e90-8fa2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-25 00:35:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9aa16a4b-8fa2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-25 00:35:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9add85d9-87dd-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'ogunyemi1409@gmail.com', NULL, 1, 0.9986000000, 'Lottery With Draw', 'ogunyemi1409@gmail.com', '2018-07-15 03:17:29', NULL, NULL, NULL, 1, '1CSDoLYNw7GxnSSZQXx37DcDEWy2hfFGjt', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9b339f04-8cc0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyentuanhung2828@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nguyentuanhung2828@gmail.com', '2018-07-21 08:32:27', NULL, NULL, NULL, 3, 'n4fAJYGPrYqtaR8wqYmsHnnsniFZWP89rS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9b5e5e2a-8fb3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huy36391@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'huy36391@gmail.com', '2018-07-25 02:36:54', NULL, NULL, NULL, 3, 'mfgsJiTPiRgwpxacpwitdWcrWg2iDKZV4H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9b919807-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:18:22', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9c162115-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:48:39', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9c38e9e5-911c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:41:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9c6e396a-8faf-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tanhoaxa2004@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'tanhoaxa2004@gmail.com', '2018-07-25 02:08:18', NULL, NULL, NULL, 3, 'mxSqRd7z17gQ7zRYbkZ9oJa7EkJ3TFDrEZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9cd54b7d-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:53', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9cd7b943-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:53', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9d0e596c-868c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0088000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-13 11:05:12', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9d782821-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:54:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9d7d2015-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:54:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9e3240eb-8963-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'dany.alliat@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'dany.alliat@gmail.com', '2018-07-17 01:49:18', NULL, NULL, NULL, 3, 'miJ1kLumayBhPmNguzpZy376thUCaSFx2P', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9f115dbd-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:17', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9f13cead-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:17', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9f586ffd-870f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysFeeTxn', 1, 0.0004966100, 'Lottery With Draw', 'accouting', '2018-07-14 02:43:00', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9f5a85b3-8b02-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'sanakiho236@gmail.com', NULL, 1, 0.9993000000, 'Lottery With Draw', 'sanakiho236@gmail.com', '2018-07-19 03:19:58', NULL, NULL, NULL, 1, '12o9dvMCXHLp8ptxF8C9tzUac2c3PTzgXD', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9f837c5d-8d5c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-22 03:09:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9f85fd52-8d5c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-22 03:09:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9f9cffec-8a62-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'duongngocphuoc2008@gmai.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: duongngocphuoc2008@gmai.com', 'LotteryCarcoin', '2018-07-18 08:14:39', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9faf54c5-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0190000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:59:15', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('9ff194d4-8b07-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huynhminhduc041097@gmail.com', 'sysAdmin', 1, 0.0036000000, 'Payment ticket lottery', 'huynhminhduc041097@gmail.com', '2018-07-19 03:55:47', NULL, NULL, NULL, 3, 'mid7hc2rzzqYyo2Uh2gsrBsxpDYbFSh3KT', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a023c249-8e31-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'huy36391@gmail.com', NULL, 1, 0.9095000000, 'Lottery With Draw', 'huy36391@gmail.com', '2018-07-23 04:33:59', NULL, NULL, NULL, 1, '3BMEXmSyqy4E1MmEtwecgETV1q4D2pF4ZP', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a02afaa4-90aa-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0168000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-26 08:05:08', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a04c8131-8c00-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'tuyendinhhb@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: tuyendinhhb@gmail.com', 'LotteryCarcoin', '2018-07-20 09:38:12', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a050fe04-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a054262e-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a09e8e78-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a0a1269b-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:36:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a1380519-8a6e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'tuanjapan6789@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: tuanjapan6789@gmail.com', 'LotteryCarcoin', '2018-07-18 09:40:36', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a1540298-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:48:48', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a1aa94dd-8fa2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:35:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a1ad6f54-8fa2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:35:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a1e5035e-8b10-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'longlucky69@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'longlucky69@gmail.com', '2018-07-19 05:00:15', NULL, NULL, NULL, 3, 'n129xQ5YhvPYnLQ12dhb3er7DiPr1bovMy', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a20f82a0-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:42:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a2131a72-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:42:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a264d0e5-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:53:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a2679849-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:53:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a2687b18-9026-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 16:20:18', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a26922bd-8dc2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huy36391@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'huy36391@gmail.com', '2018-07-22 15:19:29', NULL, NULL, NULL, 3, 'mfgsJiTPiRgwpxacpwitdWcrWg2iDKZV4H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a26a54dc-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:54:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a26d28e0-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:54:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a274c502-9026-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'chuyengiatrimunpuskin@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 16:20:18', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a2be3170-872f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nguyenlytruongson@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nguyenlytruongson@gmail.com', 'LotteryCarcoin', '2018-07-14 06:32:09', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a2cbe040-8b49-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'khoivk86@gmail.com', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'khoivk86@gmail.com', '2018-07-19 11:48:18', NULL, NULL, NULL, 3, 'mrnLYNuX6kkWfUTtMt2NuMhNCPj4rxLjoc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a2e2110e-8b08-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'longlucky69@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: longlucky69@gmail.com', 'LotteryCarcoin', '2018-07-19 04:03:01', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a38705ab-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:42', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a38a5de7-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:42', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a3f206bf-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:25', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a4a2bbcf-8a2c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'mr.lehieu1@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: mr.lehieu1@gmail.com', 'LotteryCarcoin', '2018-07-18 01:48:15', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a4aad936-8b30-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nguyendinhhai932@gmail.com', NULL, 1, 0.9987000000, 'Lottery With Draw', 'nguyendinhhai932@gmail.com', '2018-07-19 08:49:24', NULL, NULL, NULL, 1, '3FXdxvjY6qPfaQdEwPKzrdgQuVb6bntsEt', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a4bba2ca-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a4beb730-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a52c789e-8db4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-22 13:39:21', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a52f14a1-8db4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-22 13:39:21', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a53afda6-8ef8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0198000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:18:38', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a5512188-90aa-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-26 08:05:16', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a6030c05-8f52-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0198000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-24 15:02:54', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a62f3915-8a8b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-18 13:08:20', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a660ea39-8faf-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tanhoaxa2004@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'tanhoaxa2004@gmail.com', '2018-07-25 02:08:35', NULL, NULL, NULL, 3, 'mxSqRd7z17gQ7zRYbkZ9oJa7EkJ3TFDrEZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a6866c3c-9152-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'daisy.k@cscjackpot.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'daisy.k@cscjackpot.com', '2018-07-27 04:07:54', NULL, NULL, NULL, 3, 'myChTCnhaCRuYZKyPZS5fVSf3JRY9e1yTc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a6ce0a04-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a6d094dd-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a704339a-8963-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'dany.alliat@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'dany.alliat@gmail.com', '2018-07-17 01:49:33', NULL, NULL, NULL, 3, 'miJ1kLumayBhPmNguzpZy376thUCaSFx2P', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a72b17ca-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a72d6b3b-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0039000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a7304c56-8a33-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'bui.hoangdungtn@gmail.com', 'sysAdmin', 1, 0.0012000000, 'Payment ticket lottery', 'bui.hoangdungtn@gmail.com', '2018-07-18 02:38:26', NULL, NULL, NULL, 3, 'msDWos1eehr4X9BYj59g38hcX13mAuTXoY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a73ff221-8b07-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huynhminhduc041097@gmail.com', 'sysAdmin', 1, 0.0052000000, 'Payment ticket lottery', 'huynhminhduc041097@gmail.com', '2018-07-19 03:55:59', NULL, NULL, NULL, 3, 'mid7hc2rzzqYyo2Uh2gsrBsxpDYbFSh3KT', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a7937680-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:48:59', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a796f26d-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:16:01', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a799b3c2-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:16:01', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a7d579f8-882f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'yocoin.vn@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'yocoin.vn@gmail.com', '2018-07-15 13:04:49', NULL, NULL, NULL, 3, 'mv2rZiawxhzBqtqB6nw3wnUgr9rdh4bhmi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a7e7a549-9026-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 16:20:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a7f20a59-9026-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'chuyengiatrimunpuskin@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 16:20:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a878586b-8d13-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'ngphuongthanh86@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'ngphuongthanh86@gmail.com', '2018-07-21 18:26:57', NULL, NULL, NULL, 3, 'n1fuXXppMLZpiHA1G4HX8N4XnHUr9kjKy2', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a92387fc-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a926c7c0-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0008000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a9f60324-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('a9f95224-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('aa5969b0-8a2f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'phim.ngo@bigbrothers.gold', 'sysAdmin', 1, 0.0008000000, 'Payment ticket lottery', 'phim.ngo@bigbrothers.gold', '2018-07-18 02:09:53', NULL, NULL, NULL, 3, 'mnYQtciF6NFG4oAFR24KfuDTw8KwxRQf3u', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('aa813911-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('aa83d006-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('aa8f2292-8e31-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'huy36391@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'huy36391@gmail.com', '2018-07-23 04:34:17', NULL, NULL, NULL, 1, '3BMEXmSyqy4E1MmEtwecgETV1q4D2pF4ZP', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('aadd118e-8a7f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'ngonhaidang93boxing@gmail.com', 'sysAdmin', 1, 0.0040000000, 'Payment ticket lottery', 'ngonhaidang93boxing@gmail.com', '2018-07-18 11:42:33', NULL, NULL, NULL, 3, 'mq4FKuVT3oKmyAf5qRCbahuSzHCg84TXXq', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ab2dcf68-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:38', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ab31c336-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:38', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ab4a979d-8728-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'hathanhpc20@gmail.com', NULL, 1, 0.7995000000, 'Lottery With Draw', 'hathanhpc20@gmail.com', '2018-07-14 05:42:17', NULL, NULL, NULL, 1, 'mnZq9TCKeAmd1gSCS543k3BEdmWfjdZoLa', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ab6c98f8-90aa-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-26 08:05:27', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ab7b6b06-90f8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:23:48', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ab9f546e-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:25:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('aba364a4-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:25:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('abbeb303-8a40-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cscjackpot.testing@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'cscjackpot.testing@gmail.com', '2018-07-18 04:11:37', NULL, NULL, NULL, 3, 'n2mjES8L7zVrY8f2zcXW9r8fTSWGmKPLvS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ac2dac30-8713-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'carcoin123@gmail.com', 'sysAdmin', 1, 0.0044000000, 'Payment ticket lottery', 'carcoin123@gmail.com', '2018-07-14 03:11:59', NULL, NULL, NULL, 3, 'n1UnCTDemH4GK7EiuKAnUi3BKTttZKhHHs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ac76adb3-8d5c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-22 03:09:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ac78d27b-8d5c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-22 03:09:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ac7d82a0-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:49:07', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('aca04c21-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('aca4fb1b-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:41:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ad3d8fd5-8e5f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-23 10:03:38', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ad95283f-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:21', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ad972d66-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:21', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('adb4c21f-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:53:49', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('adb77bb9-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:53:49', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('adda259e-9026-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 16:20:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('addde6a5-9026-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'chuyengiatrimunpuskin@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 16:20:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ade7b86a-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:16:12', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('adead01a-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('adead10c-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:16:12', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('aded72ca-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0199000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ae0e076a-8954-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'fsuphihung@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: fsuphihung@gmail.com', 'LotteryCarcoin', '2018-07-17 00:02:22', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ae51f74b-8d5b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 03:02:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ae544a73-8d5b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0199000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 03:02:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('af3e3534-8acb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tripledxxl@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'tripledxxl@gmail.com', '2018-07-18 20:46:43', NULL, NULL, NULL, 3, 'mjoef5D4L4dkQXmhvNBYszHa9Kseetp8sc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('af585ff5-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('af5b2b41-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0060000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('af68ee50-8d5b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 03:02:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('af6b4329-8d5b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0199000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 03:02:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('af9b6a90-8a8a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'neyhai2012@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: neyhai2012@gmail.com', 'LotteryCarcoin', '2018-07-18 13:01:26', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('afb27d68-8b49-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sucmanhcuabanguoi@gmail.com', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'sucmanhcuabanguoi@gmail.com', '2018-07-19 11:48:40', NULL, NULL, NULL, 3, 'mzspFYoc7N2ZfWnHU3nZJQRZDcb1PXjSjJ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('afc0d9fa-8d90-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'khanhdinh8686@gmail.com', 'sysAdmin', 1, 0.0016000000, 'Payment ticket lottery', 'khanhdinh8686@gmail.com', '2018-07-22 09:21:57', NULL, NULL, NULL, 3, 'myg3xMiVbLGHg9VJaXQx7aLvp9nqsNPXNU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b0099432-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0002000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:42:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b00f51be-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:42:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b101896f-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:55:32', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b12b54c9-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:49:15', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b146781d-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b14928a7-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b2241373-8f73-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0066040000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-24 18:59:28', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b23ab1dc-8dae-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'dattrinhvo@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: dattrinhvo@gmail.com', 'LotteryCarcoin', '2018-07-22 12:56:46', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b29fe881-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b2a2bc31-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.4999000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b3337484-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:42:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b3368091-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:42:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b42195d7-8b10-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'longlucky69@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'longlucky69@gmail.com', '2018-07-19 05:00:46', NULL, NULL, NULL, 3, 'n129xQ5YhvPYnLQ12dhb3er7DiPr1bovMy', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b432cdaf-8dc9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangsonbtc68@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'hoangsonbtc68@gmail.com', '2018-07-22 16:10:05', NULL, NULL, NULL, 3, 'mvzn6Na5dkgcFkn5pnfPu2SzRjf8q18Vam', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b44e8fa3-8997-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'duonghaison1970@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: duonghaison1970@gmail.com', 'LotteryCarcoin', '2018-07-17 08:02:09', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b4af3a46-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:16:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b4b27e32-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:16:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b4f65708-8abc-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0244200000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-18 18:59:30', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b54698ca-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b5493fa5-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b56b24fe-8a5f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tranthien585@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'tranthien585@gmail.com', '2018-07-18 07:53:47', NULL, NULL, NULL, 3, 'my9NWRYv3hDDyhyzpXLzJtY6MepBPRQNWp', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b581dbe1-8d4e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-22 01:29:39', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b5de4c0e-8b09-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'longlucky69@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'longlucky69@gmail.com', '2018-07-19 04:10:42', NULL, NULL, NULL, 3, 'n129xQ5YhvPYnLQ12dhb3er7DiPr1bovMy', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b5e5f14e-88b0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-16 04:28:38', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b6084131-8718-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 03:48:03', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b67d7ccf-8b32-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbaocoin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbaocoin@gmail.com', '2018-07-19 09:04:13', NULL, NULL, NULL, 3, 'n1BV2rAbvcXPSZXJBt22QnYQATThqwEqtM', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b68ed85b-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:12:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b691616f-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'nguyenle130690@gmail.com', 1, 0.0060000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:12:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b6979735-914b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'liketocdo.com@gmail.com', 'sysAdmin', 1, 0.0008000000, 'Payment ticket lottery', 'liketocdo.com@gmail.com', '2018-07-27 03:18:14', NULL, NULL, NULL, 3, 'msjWH5tVJ1uwQeCprisGXXQUHJFmEPaRH6', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b69ab568-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:54:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b69e230c-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:54:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b6bb4445-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0198000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:49:24', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b6fced2d-8fa2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:35:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b7004708-8fa2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:35:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b756c42d-8da7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 12:06:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b76659c0-8da7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 12:06:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b76db43a-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:43:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b77122e6-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0008000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:43:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b7717368-8a9f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'buidunghn83@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: buidunghn83@gmail.com', 'LotteryCarcoin', '2018-07-18 15:31:58', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b773592b-8b5d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0050000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-19 14:12:03', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b782176f-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 17:16:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b79f5af4-8946-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 22:22:25', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b7a160e5-872d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'carcoin123@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'carcoin123@gmail.com', '2018-07-14 06:18:25', NULL, NULL, NULL, 3, 'n1UnCTDemH4GK7EiuKAnUi3BKTttZKhHHs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b8b0b923-8a2c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'mr.lehieu1@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'mr.lehieu1@gmail.com', '2018-07-18 01:48:48', NULL, NULL, NULL, 3, 'ms7c7TycFwM2Wv9yANA9xxMF8wqkZgRoAr', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b934d443-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:54:08', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b938511e-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:54:08', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b9605dc1-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b9622dce-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b9711f41-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b97338dc-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0005000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b98c3c92-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:25:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b9926bbe-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:25:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b9cadea0-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b9d1a72a-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0019000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:10', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b9e3fb67-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:55:47', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b9f4aa88-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('b9f7e717-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ba3f7678-8e8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'bui.hoangdungtn@gmail.com', 'sysAdmin', 1, 0.0024000000, 'Payment ticket lottery', 'bui.hoangdungtn@gmail.com', '2018-07-23 15:47:36', NULL, NULL, NULL, 3, 'msDWos1eehr4X9BYj59g38hcX13mAuTXoY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ba8b8c4b-87dc-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'ogunyemi1409@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'ogunyemi1409@gmail.com', '2018-07-15 03:11:12', NULL, NULL, NULL, 3, 'mkBwwtpHspnavYXgSVmKZWB3WfY5uiYvwu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('badc5284-9066-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0010880000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-25 23:59:07', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bb20b380-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:49:31', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bb25c853-8e69-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'daisy.k@cscjackpot.com', 'sysAdmin', 1, 0.0018000000, 'Payment ticket lottery', 'daisy.k@cscjackpot.com', '2018-07-23 11:15:37', NULL, NULL, NULL, 3, 'myChTCnhaCRuYZKyPZS5fVSf3JRY9e1yTc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bb39b8fe-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:16:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bb3b74fe-8dc2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huy36391@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'huy36391@gmail.com', '2018-07-22 15:20:11', NULL, NULL, NULL, 3, 'mfgsJiTPiRgwpxacpwitdWcrWg2iDKZV4H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bb3cf66f-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:16:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bb4e9eb2-8d4c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'myphamquyenvinh@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'myphamquyenvinh@gmail.com', '2018-07-22 01:15:30', NULL, NULL, NULL, 3, 'mxJvbkrVfyRe55LuCkdWe66hQh3xnJzQkD', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bb738246-89fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0002000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-17 20:12:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bb73a299-8b6c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'hoangsonbtc68@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: hoangsonbtc68@gmail.com', 'LotteryCarcoin', '2018-07-19 15:59:32', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bb7bcdd1-89fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'ogunyemi1409@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-17 20:12:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bb8d849a-8962-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tanhoaxa2004@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'tanhoaxa2004@gmail.com', '2018-07-17 01:42:58', NULL, NULL, NULL, 3, 'mxSqRd7z17gQ7zRYbkZ9oJa7EkJ3TFDrEZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bbb94154-8dc2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huy36391@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'huy36391@gmail.com', '2018-07-22 15:20:12', NULL, NULL, NULL, 3, 'mfgsJiTPiRgwpxacpwitdWcrWg2iDKZV4H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bbd89aa1-90f7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 17:17:06', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bc0da1f5-8da7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 12:06:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bc103ae7-8da7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 12:06:56', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bcb003be-8d55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 02:19:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bcb266cd-8d55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 02:19:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bcdd0fd0-8fa2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:36:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bcdfc3a0-8fa2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:36:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bd4667ce-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bd49de63-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bdc47892-8a73-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenly796@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nguyenly796@gmail.com', '2018-07-18 10:17:11', NULL, NULL, NULL, 3, 'mqpYrS6fK9Uq28PZN9RUXqAc8hoY2y7RZY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bdf77575-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bdf9ff6b-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('be621726-8f52-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0198000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-24 15:03:35', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('be7074f0-90f0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0010000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-26 16:27:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bf135737-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bf1683d1-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bf641ac2-8b06-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'huynhminhduc041097@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: huynhminhduc041097@gmail.com', 'LotteryCarcoin', '2018-07-19 03:49:30', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bfa61de2-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:49:39', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bfaa9a2a-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-26 16:12:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('bfafd381-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'nguyenle130690@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-26 16:12:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c03050f3-872d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'carcoin123@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'carcoin123@gmail.com', '2018-07-14 06:18:40', NULL, NULL, NULL, 3, 'n1UnCTDemH4GK7EiuKAnUi3BKTttZKhHHs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c0728c3b-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:55:58', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c093bf12-8d4e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanh239@icloud.com', 'sysAdmin', 1, 0.0100000000, 'Payment ticket lottery', 'lethanh239@icloud.com', '2018-07-22 01:29:58', NULL, NULL, NULL, 3, 'mzR5JTq2BFxZtzHT8482vhi86FuPwj7X1M', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c0f296fe-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:25:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c0f4ea53-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:25:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c0f6a0d6-90ac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'manhhainguyen.fnc@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: manhhainguyen.fnc@gmail.com', 'LotteryCarcoin', '2018-07-26 08:20:22', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c107ebdb-8aa1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-18 15:46:34', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c1919b2a-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:16:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c1953215-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c1953855-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0008000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:16:45', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c1988e36-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c1a8913b-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:55', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c1aaf601-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0010000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:37:55', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c1dacf89-8b3e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nvn3007@gmail.com', 'sysAdmin', 1, 0.0022000000, 'Payment ticket lottery', 'nvn3007@gmail.com', '2018-07-19 10:30:26', NULL, NULL, NULL, 3, 'mx6TwFZpG3AY5XPhFzrPhorT6pyhhnApTz', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c23e89c6-8fb3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huy36391@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'huy36391@gmail.com', '2018-07-25 02:37:59', NULL, NULL, NULL, 3, 'mfgsJiTPiRgwpxacpwitdWcrWg2iDKZV4H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c25be32e-8966-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'manhthuyen1997@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: manhthuyen1997@gmail.com', 'LotteryCarcoin', '2018-07-17 02:11:47', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c26dfa56-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 20:23:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c26fe252-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:43:28', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c274bd8f-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:43:28', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c27de4ec-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 20:23:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c29cce7b-8969-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'viethungpdu90@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: viethungpdu90@gmail.com', 'LotteryCarcoin', '2018-07-17 02:33:16', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c2dff532-8d5d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'zon.zip.9@gmail.com', 'sysAdmin', 1, 0.0110000000, 'Payment ticket lottery', 'zon.zip.9@gmail.com', '2018-07-22 03:17:24', NULL, NULL, NULL, 3, 'n3GdF5TEsn8aGrd1zJv4EbosmRmCmqGKpU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c3b16134-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:07:01', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c3d05faf-8dc2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huy36391@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'huy36391@gmail.com', '2018-07-22 15:20:25', NULL, NULL, NULL, 3, 'mfgsJiTPiRgwpxacpwitdWcrWg2iDKZV4H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c40574b8-8da7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 12:07:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c408c17f-8da7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 12:07:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c49167a2-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:49:47', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c4a1c3c1-8baf-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0262560000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-19 23:59:24', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c4a2c576-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:25', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c4a58193-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:25', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c526af65-8a83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'buingochuynh7@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: buingochuynh7@gmail.com', 'LotteryCarcoin', '2018-07-18 12:11:56', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c555d49e-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:53', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c5583e41-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:53', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c5cf09a0-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:56:07', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c5fef211-8a70-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'binhnguyentyphu@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'binhnguyentyphu@gmail.com', '2018-07-18 09:55:57', NULL, NULL, NULL, 3, 'myumuAuRpbgK8CMQio38DtxKVNH6ZmqyHE', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c632d10a-8c00-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tuyendinhhb@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'tuyendinhhb@gmail.com', '2018-07-20 09:39:16', NULL, NULL, NULL, 3, 'mpXHwF5MKv3UGtdgUFpQQgPjXQ4WRmwuaf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c64de806-8ab8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'Vothiyenoanh2468@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'Vothiyenoanh2468@gmail.com', '2018-07-18 18:31:21', NULL, NULL, NULL, 3, 'n3uiYVX2EdJv7JrS1UXLEsEcLXvHmqWDed', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6626163-8db4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-22 13:40:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6648a26-8db4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-22 13:40:16', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c695a08a-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:09:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6a36182-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:09:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6ae09f3-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-26 16:12:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6b0cf1f-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'nguyenle130690@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-26 16:12:58', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6b3e88d-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:54:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6b6b4d4-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:54:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6bd1495-8f2a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'dinhthihanh07@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: dinhthihanh07@gmail.com', 'LotteryCarcoin', '2018-07-24 10:17:29', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6d9fcda-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:38:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6ddea10-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:38:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6ec1996-9026-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 16:21:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c6f1d192-9026-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'chuyengiatrimunpuskin@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 16:21:19', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c70e3022-8c21-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'pecoi1988@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: pecoi1988@gmail.com', 'LotteryCarcoin', '2018-07-20 13:35:30', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c7241a6d-86f8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0004600000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-13 23:59:28', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c7ad988a-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c7b0ed40-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c7df28a0-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 20:23:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c80e8044-8dbe-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'mrkhanh831@gmail.com', 'sysAdmin', 1, 0.0018000000, 'Payment ticket lottery', 'mrkhanh831@gmail.com', '2018-07-22 14:51:54', NULL, NULL, NULL, 3, 'mnEXMijjAudS9jbG6AZF5hK9CMnwhi8Gnu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c8847de5-8f3a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nhan.luong@bigbrothers.gold', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nhan.luong@bigbrothers.gold', '2018-07-24 12:12:04', NULL, NULL, NULL, 3, 'moKCSBMxWE86Qtojno9Mza4ghaeTgucS5P', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c88ec8a4-90c7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hieplandcoin.tn@gmail.com', 'sysAdmin', 1, 0.0008000000, 'Payment ticket lottery', 'hieplandcoin.tn@gmail.com', '2018-07-26 11:33:51', NULL, NULL, NULL, 3, 'mnfe7BigPEgTGaurWSHPqLNykBBKHqyxrA', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c8a67f1f-8978-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'bui.hoangdungtn@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'bui.hoangdungtn@gmail.com', '2018-07-17 04:20:49', NULL, NULL, NULL, 3, 'msDWos1eehr4X9BYj59g38hcX13mAuTXoY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c8ad78c3-8e24-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 03:02:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c8b6d218-8e24-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 03:02:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c8d654b6-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c8dbcb40-90f4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'huynhminhduc041097@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 16:55:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c972e693-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:49:55', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c9ef8455-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:43:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('c9f23be2-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:43:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ca182adb-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 20:23:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ca396c7a-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:26:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ca3c0c2a-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:26:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ca408da4-8aa2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'zon.zip.9@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'zon.zip.9@gmail.com', '2018-07-18 15:53:59', NULL, NULL, NULL, 3, 'n3GdF5TEsn8aGrd1zJv4EbosmRmCmqGKpU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ca4c1268-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:56:15', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ca98736c-8e2e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-23 04:13:42', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cabb1c4a-8dc2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'huy36391@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'huy36391@gmail.com', '2018-07-22 15:20:37', NULL, NULL, NULL, 3, 'mfgsJiTPiRgwpxacpwitdWcrWg2iDKZV4H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cae8d6da-8cc1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuong0981148176@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'cuong0981148176@gmail.com', '2018-07-21 08:40:56', NULL, NULL, NULL, 3, 'mzaciJaJMGTkHXwE2WKYutpWXoi6tHnP39', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cb7926a7-8728-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'hathanhpc20@gmail.com', NULL, 1, 0.6995000000, 'Lottery With Draw', 'hathanhpc20@gmail.com', '2018-07-14 05:43:11', NULL, NULL, NULL, 1, 'mnZq9TCKeAmd1gSCS543k3BEdmWfjdZoLa', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cc35b3f2-8726-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 05:28:53', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cc712e7b-9026-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 16:21:29', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cc75477c-9026-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'chuyengiatrimunpuskin@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 16:21:29', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cc8eee6c-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:38', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cc917952-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:38', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cc9d0c22-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 20:23:41', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cca6237a-8f52-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungngan.fca@gmail.com', 'sysAdmin', 1, 0.0196000000, 'Payment ticket lottery', 'trungngan.fca@gmail.com', '2018-07-24 15:03:59', NULL, NULL, NULL, 3, 'mk7rwujJEyE4rtPphGNDkwHqWHaYHuAkH9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ccbbf5b9-8a5a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'eduvietthai123@gmail.com', 'sysAdmin', 1, 0.0024000000, 'Payment ticket lottery', 'eduvietthai123@gmail.com', '2018-07-18 07:18:39', NULL, NULL, NULL, 3, 'n1jpPi46oLjhEiB3RPnS5G44VCEgQmp7t9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cd34f0ca-8e24-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 03:02:12', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cd37b6f7-8e24-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 03:02:12', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cd9111e0-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cd93b08f-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:43', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cdca4532-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:56:21', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ce31b289-8aa1-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-18 15:46:56', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ce4d1917-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:26:26', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ce4ff3cb-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:26:26', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ce86b26b-8fce-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'dothicuc1970@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: dothicuc1970@gmail.com', 'LotteryCarcoin', '2018-07-25 05:51:36', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ce9b299f-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:09:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ce9e7d31-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:09:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cec9206c-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:38:17', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cecb7769-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0010000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:38:17', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ceed6cbe-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:50:05', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ceef5765-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 20:23:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cfd07f45-9084-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'thanhha12196@gmail.com', NULL, 1, 0.0875000000, 'Lottery With Draw', 'thanhha12196@gmail.com', '2018-07-26 03:34:27', 'accouting', '2018-07-26 03:35:38', NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', '99fc1e9ea291e8771005cb82614057b1f559122f105c47b2df94e37e5acc8139', 0.00050, 0.0000553300, NULL, 0);
INSERT INTO `transaction` VALUES ('cfd47745-8e57-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'giangitman@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'giangitman@gmail.com', '2018-07-23 09:07:20', NULL, NULL, NULL, 3, 'mo2PmS175o6Ri2PKR6WMEkfpupoMFZGNbp', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cffc622a-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:54:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('cfffcc67-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:54:47', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d007435d-871b-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 04:10:15', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d0a79998-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:43:52', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d0aa644d-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:43:52', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d0c162b0-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:49', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d0c645d7-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:49', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d13b9454-8975-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sanakiho236@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'sanakiho236@gmail.com', '2018-07-17 03:59:34', NULL, NULL, NULL, 3, 'mtR2yyXzsGpAiHh5Whki97TM5VERg6ZhS2', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d13eda7f-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:07:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d141c708-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-27 02:07:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d1c5d675-8ef9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:27:02', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d217c4ae-914b-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'vuhoan99@gmail.com', NULL, 1, 0.0995000000, 'Lottery With Draw', 'vuhoan99@gmail.com', '2018-07-27 03:19:01', NULL, NULL, NULL, 1, '12JnCqT7EGR5TfoT3G19jVUjSQuq1Tmrju', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d21e12e9-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d2220c07-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d24cd68d-870e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'trungnhan.luong@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: trungnhan.luong@gmail.com', 'LotteryCarcoin', '2018-07-14 02:37:16', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d27d35f0-872e-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'hathanhpc20@gmail.com', NULL, 1, 0.0095000000, 'Lottery With Draw', 'hathanhpc20@gmail.com', '2018-07-14 06:26:20', 'accouting', '2018-07-14 06:27:26', NULL, 3, 'mnZq9TCKeAmd1gSCS543k3BEdmWfjdZoLa', 'd4a584a82b59502b995b1d2ed1cc4f9217658886e079326cb7ca662eaf6aa39f', 0.00050, 0.0000033900, NULL, 0);
INSERT INTO `transaction` VALUES ('d2ac9af8-8a83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'buingochuynh7@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'buingochuynh7@gmail.com', '2018-07-18 12:12:18', NULL, NULL, NULL, 3, 'muCJpNeEmpuzkDQg5VarRDDuaWcVHyVSYx', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d2c0f56e-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 16:38:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d2c45dfd-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 16:38:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d2d875a2-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:56:29', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d35ca006-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 02:26:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d35fb6c7-8e1f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 02:26:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d365d0bd-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:50:12', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d38b0b12-89a0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'phuongtn6888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'phuongtn6888@gmail.com', '2018-07-17 09:07:27', NULL, NULL, NULL, 3, 'mjjrrJVjwMfkWheMcZSf1HdQijnSN9BjcX', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d3ae8e80-8da7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'mr.lehieu1@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'mr.lehieu1@gmail.com', '2018-07-22 12:07:35', NULL, NULL, NULL, 3, 'ms7c7TycFwM2Wv9yANA9xxMF8wqkZgRoAr', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d4126cd3-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d4151788-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0019000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:35:54', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d4cc2ad4-8e81-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'doanthithunguyet4@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'doanthithunguyet4@gmail.com', '2018-07-23 14:08:07', NULL, NULL, NULL, 3, 'mgCRmgjRxvHJwMgYnvcR3w5JQzmxktPuks', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d55f55fa-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:10:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d5636b5f-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:10:09', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d5e1c2d3-8a5c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'hvu04122000@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: hvu04122000@gmail.com', 'LotteryCarcoin', '2018-07-18 07:33:13', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d5ea2367-8d7b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'dohieuthao1994@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'dohieuthao1994@gmail.com', '2018-07-22 06:52:41', NULL, NULL, NULL, 3, 'mrJcbMVwz7vFaA5xipD9bVVa1Nx51kdHf7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d5eea6ca-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:07:32', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d5f3ae99-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-27 02:07:32', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d66edc82-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:56:35', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d67935e4-8ef9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-24 04:27:10', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d6e95405-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:44:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d6eca0ed-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:44:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d70a82dc-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 16:38:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d70d6b10-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 16:38:31', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d7217fe9-8c0e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'myphamquyenvinh@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: myphamquyenvinh@gmail.com', 'LotteryCarcoin', '2018-07-20 11:19:57', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d779493c-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:17:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d77c3130-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:17:22', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d7ac4da0-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d7b056d4-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 23:53:57', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d7b6907e-8b25-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'mr.lehieu1@gmail.com', 'sysAdmin', 1, 0.0020000000, 'Payment ticket lottery', 'mr.lehieu1@gmail.com', '2018-07-19 07:32:05', NULL, NULL, NULL, 3, 'ms7c7TycFwM2Wv9yANA9xxMF8wqkZgRoAr', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d7d39d56-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:50:20', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d7dc9bd8-8b01-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'bades1509@yahoo.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: bades1509@yahoo.com', 'LotteryCarcoin', '2018-07-19 03:14:24', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d7e96312-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 20:24:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d7ed25eb-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 20:24:00', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d7f0978d-8a5d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'quytu379@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: quytu379@gmail.com', 'LotteryCarcoin', '2018-07-18 07:40:26', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d8172542-8727-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'kienduc94@gmail.com', NULL, 1, 0.9995000000, 'Lottery With Draw', 'kienduc94@gmail.com', '2018-07-14 05:36:23', NULL, NULL, NULL, 1, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d8ca408f-8a8a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'vuhoan99@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: vuhoan99@gmail.com', 'LotteryCarcoin', '2018-07-18 13:02:35', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d8f060f3-90fa-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nghiabtc888@gmail.com', NULL, 1, 0.7995000000, 'Lottery With Draw', 'nghiabtc888@gmail.com', '2018-07-26 17:39:23', NULL, NULL, NULL, 1, '3BMEX1QHRrAErvkjY1DtXAAkCD1yAr dQWY', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d95311b4-8a7a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'giangbinh33@gmail.com', 'sysAdmin', 1, 0.0014000000, 'Payment ticket lottery', 'giangbinh33@gmail.com', '2018-07-18 11:08:04', NULL, NULL, NULL, 3, 'mtUKsTUk7XjMMP31SnPHqKToRgQa3PPbw9', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('d99e46d9-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:56:41', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('da1b5bac-8e2e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0040000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-23 04:14:08', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dad6a1ec-8946-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fredhlordze103@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'fredhlordze103@gmail.com', '2018-07-16 22:23:24', NULL, NULL, NULL, 3, 'mkMi4pnv4R8q9ZPGiP8wwScKSS44fuiFrB', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('daf78c5e-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 16:38:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dafa34f6-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 16:38:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('db1491ea-897a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'bui.hoangdungtn@gmail.com', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'bui.hoangdungtn@gmail.com', '2018-07-17 04:35:38', NULL, NULL, NULL, 3, 'msDWos1eehr4X9BYj59g38hcX13mAuTXoY', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('db5219a2-8b3e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nvn3007@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nvn3007@gmail.com', '2018-07-19 10:31:09', NULL, NULL, NULL, 3, 'mx6TwFZpG3AY5XPhFzrPhorT6pyhhnApTz', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('db5c82da-8cd8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyentuanhung2828@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nguyentuanhung2828@gmail.com', '2018-07-21 11:26:02', NULL, NULL, NULL, 3, 'n4fAJYGPrYqtaR8wqYmsHnnsniFZWP89rS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dbd303bc-8ccb-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'vinhnq@geovietnam.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: vinhnq@geovietnam.com', 'LotteryCarcoin', '2018-07-21 09:53:00', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dc35cf5b-87ce-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'duy.duong@bigbrothers.gold', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: duy.duong@bigbrothers.gold', 'LotteryCarcoin', '2018-07-15 01:31:56', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dc543ec4-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 20:24:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dc56b446-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 20:24:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dccfdefe-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:50:28', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dcdefb0b-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:10:21', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dce1ee58-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:10:21', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dce725ef-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 23:54:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dcea1909-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 23:54:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dd0671c1-8d22-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysMgmt', 1, 0.2700720000, 'RevenueForSysMgmt(Oper) (18 %)', 'LotteryCarcoin', '2018-07-21 20:15:48', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dd3a8e53-8ba8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuhoan99@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'vuhoan99@gmail.com', '2018-07-19 23:09:58', NULL, NULL, NULL, 3, 'n4FZvTaXSRrM6wo6XaVRaJ5brqevH8GJfi', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dd784a5e-8969-11e8-8c19-00163ec5394d', 1, 0, 1, 0, 1, NULL, 'luong.trungnhan2812@gmail.com', 1, 0.0343750000, NULL, 'BTCNetwork', '2018-07-17 02:34:01', 'BTCNetwork', '2018-07-17 02:57:01', NULL, 3, 'mjYmxvnDQheEAo9fbiuw9hSEFcyBkH3A4p', '073fda23e0db54121cb53a67cec32793810b41a7f329aaf8cf1216ff4714cc61', 0.00000, 0.0000000000, NULL, 2);
INSERT INTO `transaction` VALUES ('dd815f7c-90fd-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0194000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 18:00:59', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dd929034-8a93-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'suk1262@naver.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: suk1262@naver.com', 'LotteryCarcoin', '2018-07-18 14:07:08', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ddd6ad85-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:44:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ddd9ef16-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:44:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('de074e1c-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-26 16:13:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('de09baf8-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'nguyenle130690@gmail.com', 1, 0.4999000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-26 16:13:37', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('de406cb0-8a92-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'lethanh239@icloud.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'lethanh239@icloud.com', '2018-07-18 14:00:00', NULL, NULL, NULL, 3, 'mzR5JTq2BFxZtzHT8482vhi86FuPwj7X1M', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('de413f29-8b09-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'ogunyemi1409@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'ogunyemi1409@gmail.com', '2018-07-19 04:11:50', NULL, NULL, NULL, 3, 'mkBwwtpHspnavYXgSVmKZWB3WfY5uiYvwu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('de47ceaa-87c6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'hathanhpc20@gmail.com', '2018-07-15 00:34:43', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('de85ce13-9083-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 03:27:42', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('de8866b4-9083-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'phuctd222@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 03:27:42', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('de9bd8b8-90f8-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nghiabtc888@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'nghiabtc888@gmail.com', '2018-07-26 17:25:13', NULL, NULL, NULL, 1, '3BMEX1QHRrAErvkjY1DtXAAkCD1yAr dQWY', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('deca0a76-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 16:38:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('decd5ebf-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 16:38:44', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dedeafaf-87c9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/14/2018 7:00:00 PM', '2018-07-15 00:56:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dee0f382-87c9-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hathanhpc20@gmail.com', 1, 0.0019000000, 'Total Amount Receive', '7/14/2018 7:00:00 PM', '2018-07-15 00:56:13', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('df709ac6-8a8f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'thanhlam6618@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: thanhlam6618@gmail.com', 'LotteryCarcoin', '2018-07-18 13:38:34', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dfa203af-8b6e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenle130690@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nguyenle130690@gmail.com', '2018-07-19 16:14:52', NULL, NULL, NULL, 3, 'mmroEqY7mWr3ohCKQZNJ6PL3Hk77AEz5h5', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('dfa84ec5-8e5f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-23 10:05:03', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e018ab20-911a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:28:39', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e01c1fcd-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:56:51', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e03c2be0-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 20:24:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e03fbe86-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-26 20:24:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e1200d38-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:55:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e1234731-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:55:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e1560e0f-8711-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'carcoin123@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: carcoin123@gmail.com', 'LotteryCarcoin', '2018-07-14 02:59:09', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e162d619-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:07:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e16c717c-89a2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hieplandcoin.tn@gmail.com', 'sysAdmin', 1, 0.0004000000, 'Payment ticket lottery', 'hieplandcoin.tn@gmail.com', '2018-07-17 09:22:09', NULL, NULL, NULL, 3, 'mnfe7BigPEgTGaurWSHPqLNykBBKHqyxrA', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e1980955-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:43:26', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e198dc48-8fc8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-25 05:09:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e19b70a9-8fc8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-25 05:09:11', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e203e578-90f1-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nguyenle130690@gmail.com', NULL, 1, 0.4995000000, 'Lottery With Draw', 'nguyenle130690@gmail.com', '2018-07-26 16:35:13', NULL, NULL, NULL, 1, '3AyonV65ecTHd8uynQ931j45jJo9YQKu8t', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e20fc33d-8d22-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysAdward', 1, 0.5398000000, 'RevenueForAward(Award) (40 %)', 'LotteryCarcoin', '2018-07-21 20:15:56', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e212f1c3-8d22-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysProgressivePrize', 1, 0.0603600000, 'RevenueForProgressivePrize(ProgressivePrize)', 'LotteryCarcoin', '2018-07-21 20:15:56', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e2165539-8d22-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysJackPot', 1, 0.3000800000, 'RevenueForJackpot(Jackpot) (20 %)', 'LotteryCarcoin', '2018-07-21 20:15:56', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e229db86-8a8a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'neyhai2012@gmail.com', 'sysAdmin', 1, 0.0012000000, 'Payment ticket lottery', 'neyhai2012@gmail.com', '2018-07-18 13:02:51', NULL, NULL, NULL, 3, 'mgcywtxYCRe2gHCqk5vtAiBbttbvQE35v4', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e268f576-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-24 23:54:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e26b5d06-8f9c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'mr.lehieu1@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-24 23:54:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e26d4a3b-8e95-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-23 16:31:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e27e6167-882e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'yocoin.vn@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: yocoin.vn@gmail.com', 'LotteryCarcoin', '2018-07-15 12:59:18', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e2801ebb-8e95-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0005000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-23 16:31:40', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e2b5a665-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 16:38:50', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e2b8c65d-8e96-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0005000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 16:38:50', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e367bd87-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:44:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e36a4ee3-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:44:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e3c96569-90fa-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nghiabtc888@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'nghiabtc888@gmail.com', '2018-07-26 17:39:41', NULL, NULL, NULL, 1, '3BMEX1QHRrAErvkjY1DtXAAkCD1yAr dQWY', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e3dac588-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:56:58', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e441ebbc-8b2f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nguyendinhhai932@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nguyendinhhai932@gmail.com', 'LotteryCarcoin', '2018-07-19 08:44:01', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e45054f9-89ca-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'trungdung280686@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: trungdung280686@gmail.com', 'LotteryCarcoin', '2018-07-17 14:08:34', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e469f76c-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:10:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e46e6432-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:10:34', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e47cba0c-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 20:24:21', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e4a0933f-8cce-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'ryanchau2016@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: ryanchau2016@gmail.com', 'LotteryCarcoin', '2018-07-21 10:14:43', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e5002274-912f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0101680000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-26 23:59:06', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e5349703-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-26 16:13:49', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e537a15f-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'nguyenle130690@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-26 16:13:49', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e59d4259-89a2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'phamlehoa2310@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'phamlehoa2310@gmail.com', '2018-07-17 09:22:16', NULL, NULL, NULL, 3, 'n3cVpJ13iJqXoSN2aAamFS8T9wGfrrRVJK', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e5d389e2-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:43:34', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e5ee1c42-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:50:43', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e621290c-8c78-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysFreeTicket', 1, 0.0278720000, 'RevenueFreeTicketDaily (2 %)', 'LotteryCarcoin', '2018-07-20 23:59:09', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e658989b-9111-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 20:24:24', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e6a9f563-8fc8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-25 05:09:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e6ad6a65-8fc8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0005000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-25 05:09:20', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e6c4a333-87de-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'ogunyemi1409@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'ogunyemi1409@gmail.com', '2018-07-15 03:26:45', NULL, NULL, NULL, 3, 'mkBwwtpHspnavYXgSVmKZWB3WfY5uiYvwu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e7bfab53-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:57:04', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e7fed7b8-8dae-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'dattrinhvo@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'dattrinhvo@gmail.com', '2018-07-22 12:58:16', NULL, NULL, NULL, 3, 'mvNJ1zbvAstPXvoxRZWQUqE92Y5yn2DFQx', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e7ffb6e9-8d85-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trandinhson96@gmail.com', 'sysAdmin', 1, 0.0018000000, 'Payment ticket lottery', 'trandinhson96@gmail.com', '2018-07-22 08:04:47', NULL, NULL, NULL, 3, 'mhJSB7P9vjKjdkKK7ZHPQoZqK81NEuPxhu', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e8235562-8c00-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'tuyendinhhb@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'tuyendinhhb@gmail.com', '2018-07-20 09:40:13', NULL, NULL, NULL, 3, 'mpXHwF5MKv3UGtdgUFpQQgPjXQ4WRmwuaf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e82ff252-8e91-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenvantuy0204@gmail.com', 'sysAdmin', 1, 0.0030000000, 'Payment ticket lottery', 'nguyenvantuy0204@gmail.com', '2018-07-23 16:03:12', NULL, NULL, NULL, 3, 'mk5LQSdNBuiqmRFnyG1j1HMFTyHKMb5kcS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e87cc5c5-8d89-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'manhthuyen1997@gmail.com', 'sysAdmin', 1, 0.0034000000, 'Payment ticket lottery', 'manhthuyen1997@gmail.com', '2018-07-22 08:33:25', NULL, NULL, NULL, 3, 'mibwvjcvTkWvovddD6NKoqpDjqanoCLSvN', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e8b3a9bb-8cd8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyentuanhung2828@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'nguyentuanhung2828@gmail.com', '2018-07-21 11:26:25', NULL, NULL, NULL, 3, 'n4fAJYGPrYqtaR8wqYmsHnnsniFZWP89rS', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e8b45833-8cee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'chuyengiatrimunpuskin@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: chuyengiatrimunpuskin@gmail.com', 'LotteryCarcoin', '2018-07-21 14:03:54', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e8cc03ff-8e95-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-23 16:31:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e8cffd2b-8e95-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-23 16:31:51', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e8f048e6-872d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kienduc94@gmail.com', 'sysAdmin', 1, 0.0060000000, 'Payment ticket lottery', 'kienduc94@gmail.com', '2018-07-14 06:19:48', NULL, NULL, NULL, 3, 'mnZq9TCKeAmd1gSCS543k3BEdmWfjdZoLa', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e8fbb4bb-8730-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenlytruongson@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'nguyenlytruongson@gmail.com', '2018-07-14 06:41:17', NULL, NULL, NULL, 3, 'moQF9NczNegDDFKsfFM8hRrsr8Rm7GchfW', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('e9c4467e-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:43:40', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ea2ccfdb-8b02-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'sanakiho236@gmail.com', NULL, 1, 0.9993000000, 'Lottery With Draw', 'sanakiho236@gmail.com', '2018-07-19 03:22:04', NULL, NULL, NULL, 1, '12o9dvMCXHLp8ptxF8C9tzUac2c3PTzgXD', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ea6b56b7-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ea6f3799-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:06', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ea8b5bc4-8d52-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-22 01:59:46', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ea92f36e-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ea931748-8b6c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangsonbtc68@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'hoangsonbtc68@gmail.com', '2018-07-19 16:00:51', NULL, NULL, NULL, 3, 'mvzn6Na5dkgcFkn5pnfPu2SzRjf8q18Vam', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ea96d3f6-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ead32090-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 00:44:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ead5a7f2-8fa3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0008000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 00:44:35', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('eadc00e3-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-26 16:13:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('eadfd9bc-90ee-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'nguyenle130690@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-26 16:13:59', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('eaefdaf9-8b3e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nvn3007@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nvn3007@gmail.com', '2018-07-19 10:31:35', NULL, NULL, NULL, 3, 'mx6TwFZpG3AY5XPhFzrPhorT6pyhhnApTz', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('eb474c03-8966-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'manhthuyen1997@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'manhthuyen1997@gmail.com', '2018-07-17 02:12:56', NULL, NULL, NULL, 3, 'mibwvjcvTkWvovddD6NKoqpDjqanoCLSvN', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('eb6dcf94-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:50:52', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ebd938c1-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:10:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ebdcf32e-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:10:46', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ec2a9ef1-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:57:12', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ec9b162e-8fc8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-25 05:09:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ec9d94cd-8fc8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-25 05:09:30', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ecc70706-8b01-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'bades1509@yahoo.com', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'bades1509@yahoo.com', '2018-07-19 03:14:59', NULL, NULL, NULL, 3, 'mxCvsswMMhsFRoLkXbJX9axgH2LfsuXCYL', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ed0b62be-8db4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vinhnq@geovietnam.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'vinhnq@geovietnam.com', '2018-07-22 13:41:21', NULL, NULL, NULL, 3, 'misL2toCwcJjAzPn9SifwMYPVfuAYHiMxn', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ed3238d6-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:43:46', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ed54d585-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:27:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ed5766ef-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0010000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:27:03', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ed840c2e-911a-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-26 21:29:01', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('edc36ee9-8967-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'manhthuyen1997@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'manhthuyen1997@gmail.com', '2018-07-17 02:20:09', NULL, NULL, NULL, 3, 'mibwvjcvTkWvovddD6NKoqpDjqanoCLSvN', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ee0108ef-8af4-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'mrnam.nt2494@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'mrnam.nt2494@gmail.com', '2018-07-19 01:41:57', NULL, NULL, NULL, 3, 'mwbd29Gd3LhdtZRTDmFy9R1MJdFuWAAWLN', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ef5aaa88-8e95-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:32:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ef5e6c12-8e95-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:32:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('efc2faec-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('efc60081-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:15', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f01da0ad-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:57:18', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f033e10e-8eda-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'dattrinhvo@gmail.com', 'sysAdmin', 1, 0.0010000000, 'Payment ticket lottery', 'dattrinhvo@gmail.com', '2018-07-24 00:45:59', NULL, NULL, NULL, 3, 'mvNJ1zbvAstPXvoxRZWQUqE92Y5yn2DFQx', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f0693899-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:51:01', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f0969723-8735-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 07:17:17', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f102ca0e-8a7b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'maiduynhat1987@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'maiduynhat1987@gmail.com', '2018-07-18 11:15:53', NULL, NULL, NULL, 3, 'mzc1Z4M263WWh2pSG79v7besoTzN73a2jL', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f10a2caf-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:18:04', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f10d8448-901d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:18:05', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f1aa9215-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'viethungpdu90@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'viethungpdu90@gmail.com', '2018-07-18 06:36:44', NULL, NULL, NULL, 3, 'mkFAtr3tYY6DWJraWVtV2U28geA1CFEX3X', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f1b2c730-8996-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungnhan.luong@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'trungnhan.luong@gmail.com', '2018-07-17 07:56:42', NULL, NULL, NULL, 3, 'mrDSBfoQ5Vh7hRJ2sHNzAuF53WpQLejUtZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f204c3b2-8cce-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'ryanchau2016@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'ryanchau2016@gmail.com', '2018-07-21 10:15:05', NULL, NULL, NULL, 3, 'mn4nCP7fxfJyZtaYVfVxN7tVASQKoVnN1E', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f211914b-90fa-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nghiabtc888@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'nghiabtc888@gmail.com', '2018-07-26 17:40:05', NULL, NULL, NULL, 1, '3BMEX1QHRrAErvkjY1DtXAAkCD1yAr dQWY', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f2490316-8a59-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'Tranducduy87@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: Tranducduy87@gmail.com', 'LotteryCarcoin', '2018-07-18 07:12:32', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f261e2df-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:43:55', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f29d6524-8971-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'lethanhdat0111@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: lethanhdat0111@gmail.com', 'LotteryCarcoin', '2018-07-17 03:31:52', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f308c3b3-8e95-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/21/2018 7:00:00 PM', '2018-07-23 16:32:08', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f30e899b-8e95-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0005000000, 'Total Amount Receive', '7/21/2018 7:00:00 PM', '2018-07-23 16:32:08', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f33c0c68-896b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'kimthithuy17061983@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: kimthithuy17061983@gmail.com', 'LotteryCarcoin', '2018-07-17 02:48:57', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f33e972a-90fa-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nghiabtc888@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'nghiabtc888@gmail.com', '2018-07-26 17:40:07', NULL, NULL, NULL, 1, '3BMEX1QHRrAErvkjY1DtXAAkCD1yAr dQWY', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f341ddaa-89ca-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'trungdung280686@gmail.com', 'sysAdmin', 1, 0.0100000000, 'Payment ticket lottery', 'trungdung280686@gmail.com', '2018-07-17 14:08:59', NULL, NULL, NULL, 3, 'mw8ERtRN9KoBjXGqS6Uet2RFZRHfcAi4qT', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f3823362-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:57:24', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f3c7b4e2-8724-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 05:15:41', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f3e35830-8a43-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'cscjackpot@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: cscjackpot@gmail.com', 'LotteryCarcoin', '2018-07-18 04:35:06', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f40821d0-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:27:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f40a9a58-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:27:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f445bf94-8b71-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nguyenle130690@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'nguyenle130690@gmail.com', '2018-07-19 16:36:55', NULL, NULL, NULL, 3, 'mmroEqY7mWr3ohCKQZNJ6PL3Hk77AEz5h5', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f4d5b274-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:11:01', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f4d8ef9c-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:11:01', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f5abd462-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:51:10', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f5d6ee04-8b6b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nguyenle130690@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nguyenle130690@gmail.com', 'LotteryCarcoin', '2018-07-19 15:54:00', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f6bcff7a-8cf3-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'chuyengiatrimunpuskin@gmail.com', '2018-07-21 14:40:05', NULL, NULL, NULL, 3, 'moKze3aJ1VBepgBCEy84QKSkU5CtKDhnRZ', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f6e62690-8a72-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'nguyenly796@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: nguyenly796@gmail.com', 'LotteryCarcoin', '2018-07-18 10:11:38', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f6e630ea-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:44:02', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f74c4efa-8a59-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'vuthanghy89@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'vuthanghy89@gmail.com', '2018-07-18 07:12:41', NULL, NULL, NULL, 3, 'miAKb3EcGahwLKWW3y3b7YJfjvHydYHbX4', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f7538b2e-8d7b-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'dohieuthao1994@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'dohieuthao1994@gmail.com', '2018-07-22 06:53:37', NULL, NULL, NULL, 3, 'mrJcbMVwz7vFaA5xipD9bVVa1Nx51kdHf7', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f76e4d9e-8fc8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-25 05:09:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f770e6e0-8fc8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-25 05:09:48', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f77a0469-9005-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:26:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f78c8396-9005-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0004000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:26:27', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f7c957ba-8867-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'daisy.k@cscjackpot.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'daisy.k@cscjackpot.com', '2018-07-15 19:47:55', NULL, NULL, NULL, 3, 'myChTCnhaCRuYZKyPZS5fVSf3JRY9e1yTc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f7d68b6d-8e77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-23 12:57:31', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f7fb3e65-8726-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 05:30:07', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f801720d-8726-11e8-8c19-00163ec5394d', 6, 1, 0, 1, 0, 'sysFreeTicket', NULL, 1, 0.0002000000, 'Lock for free ticket', 'LotteryCarcoin', '2018-07-14 05:30:07', NULL, NULL, NULL, 2, 'n2yzJDbvieSqoQmEx5aSZky5gBWtp6D5gf', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f865f254-90fa-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'nghiabtc888@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'nghiabtc888@gmail.com', '2018-07-26 17:40:16', NULL, NULL, NULL, 1, '3BMEX1QHRrAErvkjY1DtXAAkCD1yAr dQWY', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f86e9284-8ef6-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'daisy.k@cscjackpot.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'daisy.k@cscjackpot.com', '2018-07-24 04:06:38', NULL, NULL, NULL, 3, 'myChTCnhaCRuYZKyPZS5fVSf3JRY9e1yTc', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f8a584f3-8e90-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'cuarangmuoi27@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'cuarangmuoi27@gmail.com', '2018-07-23 15:56:30', NULL, NULL, NULL, 3, 'mkkusAPvz6HR4pbWKdLE8bjDxqrMQNT3zs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f8f62968-868c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'thanhha12196@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: thanhha12196@gmail.com', 'LotteryCarcoin', '2018-07-13 11:07:46', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f8fc0293-8a5d-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'shandypham2017@gmail.com', NULL, 1, 0.4995000000, 'Lottery With Draw', 'shandypham2017@gmail.com', '2018-07-18 07:41:22', NULL, NULL, NULL, 1, '1FHo2jihhE9cZGWH69U27DXDtFciPuXGZP', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f939ac50-8a23-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'kimthithuy17061983@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'kimthithuy17061983@gmail.com', '2018-07-18 00:46:11', NULL, NULL, NULL, 3, 'n42G1H3rbrPH5RMtnwj6Unwe94fdkygV3H', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f9439c2d-8a5d-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'quytu379@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'quytu379@gmail.com', '2018-07-18 07:41:22', NULL, NULL, NULL, 3, 'mwnVKakRYEProt7aCn3863BHSLT2pegoub', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f9769c05-899f-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'phuongtn6888@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: phuongtn6888@gmail.com', 'LotteryCarcoin', '2018-07-17 09:01:21', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f9a59937-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-22 13:27:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f9a8071c-8db2-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'lethanh239@icloud.com', 1, 0.0004000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-22 13:27:23', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f9c0f287-872e-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hathanhpc20@gmail.com', 'sysFeeTxn', 1, 0.0004966100, 'Lottery With Draw', 'accouting', '2018-07-14 06:27:26', NULL, NULL, NULL, 3, 'mpDL7BQkawvFiaGbvM5j5bmezF1wtq9shU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('f9c29869-8c7c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'quanghaupro79@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: quanghaupro79@gmail.com', 'LotteryCarcoin', '2018-07-21 00:28:20', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fa4d2994-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fa513198-9141-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'myphamquyenvinh@gmail.com', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-27 02:08:33', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fa67dd24-89a0-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'phuongtn6888@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'phuongtn6888@gmail.com', '2018-07-17 09:08:32', NULL, NULL, NULL, 3, 'mjjrrJVjwMfkWheMcZSf1HdQijnSN9BjcX', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fa6c0bc8-9084-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'thanhha12196@gmail.com', 'sysFeeTxn', 1, 0.0004446700, 'Lottery With Draw', 'accouting', '2018-07-26 03:35:38', NULL, NULL, NULL, 3, 'msPJGc8ShngtGnmhyKqBNk7e9Dn4SGmoY3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fa9c03fe-90ac-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'manhhainguyen.fnc@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'manhhainguyen.fnc@gmail.com', '2018-07-26 08:21:59', NULL, NULL, NULL, 3, 'mpcXH5rkRjJh3kidfBAXGTygk9kVkzXsxt', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fac9c210-9105-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0194000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 18:59:04', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fb72475d-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:44:10', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fb75b016-8a56-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:51:19', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fbaa4488-8c1c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'phamdung1080@gmail.com', 'sysAdmin', 1, 0.0018000000, 'Payment ticket lottery', 'phamdung1080@gmail.com', '2018-07-20 13:01:11', NULL, NULL, NULL, 3, 'mp8Ffun3KMxmrfEwusa9EWUcmz1P7c8CZN', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fbe31d86-8fc8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/19/2018 7:00:00 PM', '2018-07-25 05:09:55', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fbe62d22-8fc8-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0002000000, 'Total Amount Receive', '7/19/2018 7:00:00 PM', '2018-07-25 05:09:55', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fc6b75c1-8732-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'kienduc94@gmail.com', NULL, 1, 0.0995000000, 'Lottery With Draw', 'kienduc94@gmail.com', '2018-07-14 06:56:08', NULL, NULL, NULL, 1, 'mnZq9TCKeAmd1gSCS543k3BEdmWfjdZoLa', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fc6c25c5-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fc6ffdfc-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0019000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:02', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fc7361ee-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 15:11:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fc78a432-901c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0002000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 15:11:14', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fc78f5d6-914c-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-27 03:27:21', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fc7903a0-9005-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-25 12:26:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fc7b9b83-9005-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'HIEU.PLAZA@GMAIL.COM', 1, 0.0010000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-25 12:26:36', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fcf379d6-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'viethungpdu90@gmail.com', 'sysAdmin', 1, 0.0200000000, 'Payment ticket lottery', 'viethungpdu90@gmail.com', '2018-07-18 06:37:03', NULL, NULL, NULL, 3, 'mkFAtr3tYY6DWJraWVtV2U28geA1CFEX3X', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('fd4514e5-8963-11e8-8c19-00163ec5394d', 5, 1, 0, 1, 0, 'dany.alliat@gmail.com', NULL, 1, 0.8995000000, 'Lottery With Draw', 'dany.alliat@gmail.com', '2018-07-17 01:51:57', NULL, NULL, NULL, 1, '18zv8Lro5Aqrtp8E46PktRYsge9WN3YaZF', '', 0.00050, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('feb0f8c8-8a55-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'hoangbhvd@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'hoangbhvd@gmail.com', '2018-07-18 06:44:15', NULL, NULL, NULL, 3, 'n485kp3vR8H6DW5WG5GEWmz67J3ACwNsWs', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ff6c391f-8d77-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysLoan', 'dohieuthao1994@gmail.com', 1, 1.0000000000, 'Tranfer -> from: sysLoan to: dohieuthao1994@gmail.com', 'LotteryCarcoin', '2018-07-22 06:25:13', NULL, NULL, NULL, 3, 'mtoc3Mgxxeki8FG2pHkeeT84HA8r6TGcxd', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ff8f8b7d-8954-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'fsuphihung@gmail.com', 'sysAdmin', 1, 0.0002000000, 'Payment ticket lottery', 'fsuphihung@gmail.com', '2018-07-17 00:04:39', NULL, NULL, NULL, 3, 'moHQynVxdQW6dvno6Lb5HwSuvCvqCsZ8JR', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ffb73310-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0001000000, 'Payment fee Withdraw', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ffba22fe-8a54-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'hoangbhvd@gmail.com', 1, 0.0003000000, 'Total Amount Receive', '7/17/2018 7:00:00 PM', '2018-07-18 06:37:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ffc00aa2-9105-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'nghiabtc888@gmail.com', 'sysAdmin', 1, 0.0190000000, 'Payment ticket lottery', 'nghiabtc888@gmail.com', '2018-07-26 18:59:12', NULL, NULL, NULL, 3, 'mmat8WRik6z1SBQPcEHumoG1zNmi1nhHn3', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ffc84f13-8d90-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'khanhdinh8686@gmail.com', 'sysAdmin', 1, 0.0006000000, 'Payment ticket lottery', 'khanhdinh8686@gmail.com', '2018-07-22 09:24:11', NULL, NULL, NULL, 3, 'myg3xMiVbLGHg9VJaXQx7aLvp9nqsNPXNU', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ffd0db71-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'sysFeePrize', 1, 0.0000000000, 'Payment fee Withdraw', '7/24/2018 7:00:00 PM', '2018-07-24 20:56:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ffd49c8c-8f83-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdward', 'vinhnq@geovietnam.com', 1, 0.0005000000, 'Total Amount Receive', '7/24/2018 7:00:00 PM', '2018-07-24 20:56:07', NULL, NULL, NULL, 3, 'n1qXecHp6FxaEJssGSwZaNC1i1BUUtgX2G', NULL, 0.00000, 0.0000000000, NULL, 0);
INSERT INTO `transaction` VALUES ('ffdcae39-90d7-11e8-8c19-00163ec5394d', 3, 1, 1, 1, 1, 'sysAdmin', 'sysMgmt', 1, 0.0312480000, 'RevenueForSysMgmt(Oper) (18 %)', 'LotteryCarcoin', '2018-07-26 13:29:56', NULL, NULL, NULL, 3, 'n1qpa442Svnffxiwu1caQAVgyL94EG5UdS', NULL, 0.00000, 0.0000000000, NULL, 0);

-- ----------------------------
-- Table structure for transactionevents
-- ----------------------------
DROP TABLE IF EXISTS `transactionevents`;
CREATE TABLE `transactionevents`  (
  `EventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `EventDescription` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventData` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Reason',
  `EventData1` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'LockBy',
  `EventData2` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'UnLockBy',
  `EventData3` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AdditionalData',
  `EventSource` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EventType` smallint(6) NULL DEFAULT 0 COMMENT '1: Create, 2: Lock, 3: Unlock',
  `TransactionID` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ClientId` smallint(6) NOT NULL,
  `UserID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `Amount` decimal(18, 0) NULL DEFAULT 0,
  PRIMARY KEY (`EventId`) USING BTREE,
  INDEX `FK_TransactionEvents_Transaction`(`TransactionID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for transactionlog
-- ----------------------------
DROP TABLE IF EXISTS `transactionlog`;
CREATE TABLE `transactionlog`  (
  `Id` bigint(20) NOT NULL,
  `TransactionId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SourceLog` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Step` tinyint(3) UNSIGNED NOT NULL,
  `SubStep` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LogData` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `CreatedDate` datetime NOT NULL,
  `ClientId` smallint(6) NOT NULL,
  `SystemId` smallint(6) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `FK_TransactionLog_Transaction`(`TransactionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `UserId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SystemId` smallint(6) NOT NULL,
  `ClientId` smallint(6) NOT NULL,
  `WalletId` bigint(20) NOT NULL,
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PhoneNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DailyWithdrawLimit` decimal(18, 5) NULL DEFAULT NULL,
  `WithdrawAddressWhiteListEnable` tinyint(4) NULL DEFAULT NULL,
  `WithdrawOnDay` decimal(18, 5) NULL DEFAULT 0.00000,
  `WithdrawDate` datetime NULL DEFAULT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UpdatedDate` datetime NULL DEFAULT NULL,
  `UpdatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Status` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '1: Active, 2: Locked, 3: TemporaryUnlock, 4: TemporaryLock',
  PRIMARY KEY (`UserId`, `SystemId`) USING BTREE,
  INDEX `FK_User_Wallet`(`WalletId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('bades1509@yahoo.com', 1, 1, 1, 'bades1509@yahoo.com', 'bades1509@yahoo.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 03:13:59', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('bhoangson86@gmail.com', 1, 1, 1, 'bhoangson86@gmail.com', 'bhoangson86@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 15:39:55', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('binhnguyentyphu@gmail.com', 1, 1, 1, 'binhnguyentyphu@gmail.com', 'binhnguyentyphu@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 09:54:15', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('bui.hoangdungtn@gmail.com', 1, 1, 1, 'bui.hoangdungtn@gmail.com', 'bui.hoangdungtn@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 04:18:35', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('buiducquyen1983@gmail.com', 1, 1, 1, 'buiducquyen1983@gmail.com', 'buiducquyen1983@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-25 02:41:29', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('buidunghn83@gmail.com', 1, 1, 1, 'buidunghn83@gmail.com', 'buidunghn83@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 15:31:01', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('buingochuynh7@gmail.com', 1, 1, 1, 'buingochuynh7@gmail.com', 'buingochuynh7@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 12:11:25', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('buingochuynh@gmail.com', 1, 1, 1, 'buingochuynh@gmail.com', 'buingochuynh@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 12:03:54', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('buitanphat1991@gmail.com', 1, 1, 1, 'buitanphat1991@gmail.com', 'buitanphat1991@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-13 11:01:31', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('buithanhhoa0309@gmail.com', 1, 1, 1, 'buithanhhoa0309@gmail.com', 'buithanhhoa0309@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 07:42:40', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('carcoin12345@gmail.com', 1, 1, 1, 'carcoin12345@gmail.com', 'carcoin12345@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-14 07:18:18', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('carcoin123@gmail.com', 1, 1, 1, 'carcoin123@gmail.com', 'carcoin123@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-14 02:57:23', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('carcoin@gmail.com', 1, 1, 1, 'carcoin@gmail.com', 'carcoin@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-14 02:56:56', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('chuyengiatrimunpuskin@gmail.com', 1, 1, 1, 'chuyengiatrimunpuskin@gmail.com', 'chuyengiatrimunpuskin@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-20 17:43:00', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('cscjackpot.testing@gmail.com', 1, 1, 1, 'cscjackpot.testing@gmail.com', 'cscjackpot.testing@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 03:01:19', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('cscjackpot@gmail.com', 1, 1, 1, 'cscjackpot@gmail.com', 'cscjackpot@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 04:34:48', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('cuarangmuoi25@gmail.com', 1, 1, 1, 'cuarangmuoi25@gmail.com', 'cuarangmuoi25@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-22 02:06:51', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('cuarangmuoi27@gmail.com', 1, 1, 1, 'cuarangmuoi27@gmail.com', 'cuarangmuoi27@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 06:16:08', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('cuong0981148176@gmail.com', 1, 1, 1, 'cuong0981148176@gmail.com', 'cuong0981148176@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-21 08:36:03', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('daisy.k@cscjackpot.com', 1, 1, 1, 'daisy.k@cscjackpot.com', 'daisy.k@cscjackpot.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-15 19:24:25', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('danielmokobi35@gmail.com', 1, 1, 1, 'danielmokobi35@gmail.com', 'danielmokobi35@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-24 09:01:01', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('dany.alliart@gmail.com', 1, 1, 1, 'dany.alliart@gmail.com', 'dany.alliart@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-25 03:01:11', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('dany.alliat@gmail.com', 1, 1, 1, 'dany.alliat@gmail.com', 'dany.alliat@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 01:44:57', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('dattrinhvo@gmail.com', 1, 1, 1, 'dattrinhvo@gmail.com', 'dattrinhvo@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-22 12:56:03', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('dinhthihanh07@gmail.com', 1, 1, 1, 'dinhthihanh07@gmail.com', 'dinhthihanh07@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-24 10:16:06', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('doanthithunguyet4@gmail.com', 1, 1, 1, 'doanthithunguyet4@gmail.com', 'doanthithunguyet4@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-23 14:03:40', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('dohieuthao1994@gmail.com', 1, 1, 1, 'dohieuthao1994@gmail.com', 'dohieuthao1994@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-22 06:17:42', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('doiminoduong2008@gmail.com', 1, 1, 1, 'doiminoduong2008@gmail.com', 'doiminoduong2008@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:22:14', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('dongluan1988@gmail.com', 1, 1, 1, 'dongluan1988@gmail.com', 'dongluan1988@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 03:11:19', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('dothicuc1970@gmail.com', 1, 1, 1, 'dothicuc1970@gmail.com', 'dothicuc1970@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-25 05:51:13', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('dungnt.cococo@gmail.com', 1, 1, 1, 'dungnt.cococo@gmail.com', 'dungnt.cococo@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 06:58:10', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('duonghaison1970@gmail.com', 1, 1, 1, 'duonghaison1970@gmail.com', 'duonghaison1970@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 08:01:41', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('duongngocphuoc2008@gmai.com', 1, 1, 1, 'duongngocphuoc2008@gmai.com', 'duongngocphuoc2008@gmai.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:37:46', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('duy.duong@bigbrothers.gold', 1, 1, 1, 'duy.duong@bigbrothers.gold', 'duy.duong@bigbrothers.gold', '', 20.00000, NULL, 0.00000, NULL, '2018-07-15 01:31:33', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('eduvietthai123@gmail.com', 1, 1, 1, 'eduvietthai123@gmail.com', 'eduvietthai123@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:13:02', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('fredhlordze103@gmail.com', 1, 1, 1, 'fredhlordze103@gmail.com', 'fredhlordze103@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-16 22:18:04', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('fsuphihung@gmail.com', 1, 1, 1, 'fsuphihung@gmail.com', 'fsuphihung@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 00:00:54', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('giangbinh33@gmail.com', 1, 1, 1, 'giangbinh33@gmail.com', 'giangbinh33@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 10:55:23', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('giangitman@gmail.com', 1, 1, 1, 'giangitman@gmail.com', 'giangitman@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-23 08:40:30', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('globallook2020@gmail.com', 1, 1, 1, 'globallook2020@gmail.com', 'globallook2020@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-24 20:30:41', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('globallook2030@gmail.com', 1, 1, 1, 'globallook2030@gmail.com', 'globallook2030@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 21:32:40', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('hanhnguyen051492@gmail.com', 1, 1, 1, 'hanhnguyen051492@gmail.com', 'hanhnguyen051492@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-20 13:33:17', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('haquynhanhlsls@gmail.com', 1, 1, 1, 'haquynhanhlsls@gmail.com', 'haquynhanhlsls@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 11:23:59', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('hathanhpc20@gmail.com', 1, 1, 1, 'hathanhpc20@gmail.com', 'hathanhpc20@gmail.com', '', 20.00000, NULL, 4.00000, '2018-07-14 06:27:22', '2018-07-13 11:01:12', 'LotteryCarcoin', '2018-07-14 06:27:22', 'accouting', 1);
INSERT INTO `user` VALUES ('hiepdaoduy77@gmail.com', 1, 1, 1, 'hiepdaoduy77@gmail.com', 'hiepdaoduy77@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 08:50:24', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('hieplandcoin.tn@gmail.com', 1, 1, 1, 'hieplandcoin.tn@gmail.com', 'hieplandcoin.tn@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 08:57:55', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('HIEU.PLAZA@GMAIL.COM', 1, 1, 1, 'HIEU.PLAZA@GMAIL.COM', 'HIEU.PLAZA@GMAIL.COM', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 00:00:45', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('hoang0977671567@gmail.com', 1, 1, 1, 'hoang0977671567@gmail.com', 'hoang0977671567@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 12:44:19', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('hoangbaocoin@gmail.com', 1, 1, 1, 'hoangbaocoin@gmail.com', 'hoangbaocoin@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 12:15:28', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('hoangbhvd@gmail.com', 1, 1, 1, 'hoangbhvd@gmail.com', 'hoangbhvd@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 02:13:19', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('hoangsonbtc68@gmail.com', 1, 1, 1, 'hoangsonbtc68@gmail.com', 'hoangsonbtc68@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 15:54:30', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('hohoangcuongmessi@gmail.com', 1, 1, 1, 'hohoangcuongmessi@gmail.com', 'hohoangcuongmessi@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 01:34:14', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('huy36391@gmail.com', 1, 1, 1, 'huy36391@gmail.com', 'huy36391@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-22 15:17:59', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('huynhminhduc041097@gmail.com', 1, 1, 1, 'huynhminhduc041097@gmail.com', 'huynhminhduc041097@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 03:47:52', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('hvu04122000@gmail.com', 1, 1, 1, 'hvu04122000@gmail.com', 'hvu04122000@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:32:54', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('k.duchappy@gmail.com', 1, 1, 1, 'k.duchappy@gmail.com', 'k.duchappy@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-14 07:07:09', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('kavantrant93@gmail.com', 1, 1, 1, 'kavantrant93@gmail.com', 'kavantrant93@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-26 02:35:00', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('khanhdinh8686@gmail.com', 1, 1, 1, 'khanhdinh8686@gmail.com', 'khanhdinh8686@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-22 09:20:00', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('khoivk86@gmail.com', 1, 1, 1, 'khoivk86@gmail.com', 'khoivk86@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 11:36:48', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('kienduc94@gmail.com', 1, 1, 1, 'kienduc94@gmail.com', 'kienduc94@gmail.com', '', 20.00000, NULL, 2.00000, '2018-07-14 05:39:25', '2018-07-14 05:26:06', 'LotteryCarcoin', '2018-07-14 05:39:25', 'accouting', 1);
INSERT INTO `user` VALUES ('kimthithuy17061983@gmail.com', 1, 1, 1, 'kimthithuy17061983@gmail.com', 'kimthithuy17061983@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 02:47:46', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('langtutomboyls@gmail.com', 1, 1, 1, 'langtutomboyls@gmail.com', 'langtutomboyls@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 09:00:29', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('leanhdung1434@gmail.com', 1, 1, 1, 'leanhdung1434@gmail.com', 'leanhdung1434@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-22 07:31:58', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('lecautoananh@gmail.com', 1, 1, 1, 'lecautoananh@gmail.com', 'lecautoananh@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 10:31:31', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('lethanh239@icloud.com', 1, 1, 1, 'lethanh239@icloud.com', 'lethanh239@icloud.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 13:56:51', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('lethanhdat0111@gmail.com', 1, 1, 1, 'lethanhdat0111@gmail.com', 'lethanhdat0111@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 03:30:56', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('liketocdo.com@gmail.com', 1, 1, 1, 'liketocdo.com@gmail.com', 'liketocdo.com@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-27 03:14:13', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('longlucky69@gmail.com', 1, 1, 1, 'longlucky69@gmail.com', 'longlucky69@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 04:02:37', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('luong.trungnhan2812@gmail.com', 1, 1, 1, 'luong.trungnhan2812@gmail.com', 'luong.trungnhan2812@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-16 03:22:58', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('Luongthuthuy.2992.dn@gmail.com', 1, 1, 1, 'Luongthuthuy.2992.dn@gmail.com', 'Luongthuthuy.2992.dn@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 12:21:03', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('maiduynhat1987@gmail.com', 1, 1, 1, 'maiduynhat1987@gmail.com', 'maiduynhat1987@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 11:12:31', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('manhhainguyen.fnc@gmail.com', 1, 1, 1, 'manhhainguyen.fnc@gmail.com', 'manhhainguyen.fnc@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-26 08:19:15', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('manhnshd92@gmail.com', 1, 1, 1, 'manhnshd92@gmail.com', 'manhnshd92@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 22:32:17', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('manhthuyen1997@gmail.com', 1, 1, 1, 'manhthuyen1997@gmail.com', 'manhthuyen1997@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 02:11:16', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('minhquan060917@gmail.com', 1, 1, 1, 'minhquan060917@gmail.com', 'minhquan060917@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 19:03:33', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('mr.lehieu1@gmail.com', 1, 1, 1, 'mr.lehieu1@gmail.com', 'mr.lehieu1@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 01:47:22', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('mrkhanh831@gmail.com', 1, 1, 1, 'mrkhanh831@gmail.com', 'mrkhanh831@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 07:47:28', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('mrnam.nt2494@gmail.com', 1, 1, 1, 'mrnam.nt2494@gmail.com', 'mrnam.nt2494@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 15:33:36', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('mrtran.coin@gmail.com', 1, 1, 1, 'mrtran.coin@gmail.com', 'mrtran.coin@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-16 23:53:22', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('mvd081812@gmail.com', 1, 1, 1, 'mvd081812@gmail.com', 'mvd081812@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-22 05:53:43', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('myphamquyenvinh@gmail.com', 1, 1, 1, 'myphamquyenvinh@gmail.com', 'myphamquyenvinh@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-20 10:53:09', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('namgialong4646@gmail.com', 1, 1, 1, 'namgialong4646@gmail.com', 'namgialong4646@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 10:27:41', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('ndtrung84@gmail.com', 1, 1, 1, 'ndtrung84@gmail.com', 'ndtrung84@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-16 16:28:49', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nductrung90@gmail.com', 1, 1, 1, 'nductrung90@gmail.com', 'nductrung90@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 12:41:06', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('neyhai2012@gmail.com', 1, 1, 1, 'neyhai2012@gmail.com', 'neyhai2012@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 12:58:32', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nghiabtc888@gmail.com', 1, 1, 1, 'nghiabtc888@gmail.com', 'nghiabtc888@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-26 17:15:05', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nghiemductrung0612@gmail.com', 1, 1, 1, 'nghiemductrung0612@gmail.com', 'nghiemductrung0612@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 12:29:13', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('ngonhaidang93boxing@gmail.com', 1, 1, 1, 'ngonhaidang93boxing@gmail.com', 'ngonhaidang93boxing@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 11:35:10', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('ngphuongthanh86@gmail.com', 1, 1, 1, 'ngphuongthanh86@gmail.com', 'ngphuongthanh86@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-21 18:22:30', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nguyendinhhai932@gmail.com', 1, 1, 1, 'nguyendinhhai932@gmail.com', 'nguyendinhhai932@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 08:43:30', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nguyenhongsonhn91@gmail.com', 1, 1, 1, 'nguyenhongsonhn91@gmail.com', 'nguyenhongsonhn91@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-23 13:37:17', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nguyenle130690@gmail.com', 1, 1, 1, 'nguyenle130690@gmail.com', 'nguyenle130690@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 15:53:16', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nguyenly796@gmail.com', 1, 1, 1, 'nguyenly796@gmail.com', 'nguyenly796@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 10:10:49', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nguyenlytruongson@gmail.com', 1, 1, 1, 'nguyenlytruongson@gmail.com', 'nguyenlytruongson@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-14 06:31:16', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nguyentuanhung2828@gmail.com', 1, 1, 1, 'nguyentuanhung2828@gmail.com', 'nguyentuanhung2828@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-21 08:29:45', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nguyenvantuy0204@gmail.com', 1, 1, 1, 'nguyenvantuy0204@gmail.com', 'nguyenvantuy0204@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-22 10:00:02', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nhan.luong@bigbrothers.gold', 1, 1, 1, 'nhan.luong@bigbrothers.gold', 'nhan.luong@bigbrothers.gold', '', 20.00000, NULL, 0.00000, NULL, '2018-07-16 03:31:54', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nhattuan543345@gmail.com', 1, 1, 1, 'nhattuan543345@gmail.com', 'nhattuan543345@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 08:41:51', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('nvn3007@gmail.com', 1, 1, 1, 'nvn3007@gmail.com', 'nvn3007@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 10:26:41', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('ogunyemi1409@gmail.com', 1, 1, 1, 'ogunyemi1409@gmail.com', 'ogunyemi1409@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-15 03:09:23', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('pardo422@gmail.com', 1, 1, 1, 'pardo422@gmail.com', 'pardo422@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-26 11:11:33', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('pecoi1988@gmail.com', 1, 1, 1, 'pecoi1988@gmail.com', 'pecoi1988@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-20 13:30:20', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('phamdung1080@gmail.com', 1, 1, 1, 'phamdung1080@gmail.com', 'phamdung1080@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-20 09:33:42', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('phamlehoa2310@gmail.com', 1, 1, 1, 'phamlehoa2310@gmail.com', 'phamlehoa2310@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 09:16:30', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('phat.bui@bigbrothers.gold', 1, 1, 1, 'phat.bui@bigbrothers.gold', 'phat.bui@bigbrothers.gold', '', 20.00000, NULL, 0.00000, NULL, '2018-07-13 11:08:37', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('phim.ngo@bigbrothers.gold', 1, 1, 1, 'phim.ngo@bigbrothers.gold', 'phim.ngo@bigbrothers.gold', '', 20.00000, NULL, 0.00000, NULL, '2018-07-16 02:06:52', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('phuctd222@gmail.com', 1, 1, 1, 'phuctd222@gmail.com', 'phuctd222@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-23 11:19:56', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('phuongtn6888@gmail.com', 1, 1, 1, 'phuongtn6888@gmail.com', 'phuongtn6888@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 08:59:26', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('quanghaupro79@gmail.com', 1, 1, 1, 'quanghaupro79@gmail.com', 'quanghaupro79@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-21 00:25:08', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('quangnguyen0293@gmail.com', 1, 1, 1, 'quangnguyen0293@gmail.com', 'quangnguyen0293@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 08:31:23', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('quytu379@gmail.com', 1, 1, 1, 'quytu379@gmail.com', 'quytu379@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:40:10', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('ryanchau2016@gmail.com', 1, 1, 1, 'ryanchau2016@gmail.com', 'ryanchau2016@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-21 10:14:30', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sanakiho236@gmail.com', 1, 1, 1, 'sanakiho236@gmail.com', 'sanakiho236@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 03:57:03', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('shandypham2017@gmail.com', 1, 1, 1, 'shandypham2017@gmail.com', 'shandypham2017@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:29:39', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('shandyphamqngai2018@gmail.com', 1, 1, 1, 'shandyphamqngai2018@gmail.com', 'shandyphamqngai2018@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:28:10', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('son.ly@bigbrothers.gold', 1, 1, 1, 'son.ly@bigbrothers.gold', 'son.ly@bigbrothers.gold', '', 20.00000, NULL, 0.00000, NULL, '2018-07-15 01:33:15', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sucmanhcuabanguoi@gmail.com', 1, 1, 1, 'sucmanhcuabanguoi@gmail.com', 'sucmanhcuabanguoi@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 11:45:04', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('suk1262@naver.com', 1, 1, 1, 'suk1262@naver.com', 'suk1262@naver.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 14:06:50', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sysAdmin', 1, 1, 1, 'sysAdmin', 'sysAdmin@address', NULL, NULL, NULL, 0.00000, NULL, '2018-06-15 15:39:46', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sysAdward', 1, 1, 1, 'sysAdward', 'sysAdward@address', NULL, 20.00000, NULL, 0.00000, NULL, '2018-06-29 07:27:43', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sysBTC', 1, 1, 1, 'sysBTC', 'sysBTC@address', NULL, 20.00000, NULL, 0.00000, NULL, '2018-07-02 04:09:21', 'administrator', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sysFeePrize', 1, 1, 1, 'sysFeePrize', 'sysFeePrize@address', NULL, 20.00000, NULL, 0.00000, NULL, '2018-06-29 07:28:41', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sysFeeTxn', 1, 1, 1, 'sysFeeTxn', 'sysFeeTxn@address', NULL, 20.00000, NULL, 0.00000, NULL, '2018-06-29 07:29:33', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sysFreeTicket', 1, 1, 1, 'sysFreeTicket', 'sysFreeTicket@address', NULL, NULL, NULL, 0.00000, NULL, '2018-06-15 15:39:07', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sysJackPot', 1, 1, 1, 'sysJackPot', 'sysJackPot@address', NULL, 20.00000, NULL, 0.00000, NULL, '2018-07-02 04:13:49', 'administrator', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sysLoan', 1, 1, 1, 'sysLoan', 'sysLoan@address', NULL, 20.00000, NULL, 0.00000, NULL, '2018-07-02 04:08:20', 'administrator', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sysMgmt', 1, 1, 1, 'sysMgmt', 'sysMgmt@address', NULL, 20.00000, NULL, 0.00000, NULL, '2018-07-02 04:01:56', 'administrator', NULL, NULL, 1);
INSERT INTO `user` VALUES ('sysProgressivePrize', 1, 1, 1, 'sysProgressivePrize', 'sysProgressivePrize@address', NULL, 20.00000, NULL, 0.00000, NULL, '2018-07-02 04:06:26', 'administrator', NULL, NULL, 1);
INSERT INTO `user` VALUES ('tanhoaxa2004@gmail.com', 1, 1, 1, 'tanhoaxa2004@gmail.com', 'tanhoaxa2004@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 01:41:20', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('tathung1689@gmail.com', 1, 1, 1, 'tathung1689@gmail.com', 'tathung1689@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-25 06:36:30', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('thaingocthoai1234@gmail.com', 1, 1, 1, 'thaingocthoai1234@gmail.com', 'thaingocthoai1234@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:35:30', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('thanh.ha@bigbrothers.gold', 1, 1, 1, 'thanh.ha@bigbrothers.gold', 'thanh.ha@bigbrothers.gold', '', 20.00000, NULL, 0.00000, NULL, '2018-07-14 07:17:03', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('thanhha12196@gmail.com', 1, 1, 1, 'thanhha12196@gmail.com', 'thanhha12196@gmail.com', '', 20.00000, NULL, 2.00000, '2018-07-26 03:35:33', '2018-07-13 11:07:25', 'LotteryCarcoin', '2018-07-26 03:35:33', 'accouting', 1);
INSERT INTO `user` VALUES ('thanhhungt31@gmail.com', 1, 1, 1, 'thanhhungt31@gmail.com', 'thanhhungt31@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 13:14:45', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('thanhlam6618@gmail.com', 1, 1, 1, 'thanhlam6618@gmail.com', 'thanhlam6618@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 13:35:37', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('thienbui1987@gmail.com', 1, 1, 1, 'thienbui1987@gmail.com', 'thienbui1987@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-14 07:15:39', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('thu.nguyen@cryptolending.org', 1, 1, 1, 'thu.nguyen@cryptolending.org', 'thu.nguyen@cryptolending.org', '', 20.00000, NULL, 0.00000, NULL, '2018-07-15 19:29:21', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('toan8181@gmail.com', 1, 1, 1, 'toan8181@gmail.com', 'toan8181@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-15 12:47:12', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('trandinhson96@gmail.com', 1, 1, 1, 'trandinhson96@gmail.com', 'trandinhson96@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-20 14:56:07', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('Tranducduy87@gmail.com', 1, 1, 1, 'Tranducduy87@gmail.com', 'Tranducduy87@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:11:57', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('tranthien585@gmail.com', 1, 1, 1, 'tranthien585@gmail.com', 'tranthien585@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:51:00', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('tripledxxl@gmail.com', 1, 1, 1, 'tripledxxl@gmail.com', 'tripledxxl@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 20:39:22', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('trungdung280686@gmail.com', 1, 1, 1, 'trungdung280686@gmail.com', 'trungdung280686@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 14:05:11', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('trungngan.fca@gmail.com', 1, 1, 1, 'trungngan.fca@gmail.com', 'trungngan.fca@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-20 08:00:58', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('trungnhan.luong@gmail.com', 1, 1, 1, 'trungnhan.luong@gmail.com', 'trungnhan.luong@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-14 02:36:07', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('truongyen01@gmail.com', 1, 1, 1, 'truongyen01@gmail.com', 'truongyen01@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-19 01:21:47', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('tuanjapan6789@gmail.com', 1, 1, 1, 'tuanjapan6789@gmail.com', 'tuanjapan6789@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 09:40:21', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('tuyendinhhb@gmail.com', 1, 1, 1, 'tuyendinhhb@gmail.com', 'tuyendinhhb@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-20 09:37:41', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('userBTC', 1, 1, 1, 'userBTC', 'userBTC@address', NULL, 20.00000, NULL, 0.00000, NULL, '2018-07-02 04:14:38', 'administrator', NULL, NULL, 1);
INSERT INTO `user` VALUES ('userFree', 1, 1, 1, 'userFree', 'userFree@address', NULL, 20.00000, NULL, 3.00000, '2018-07-02 07:01:27', '2018-07-02 04:15:17', 'administrator', '2018-07-02 07:01:27', 'administrator', 1);
INSERT INTO `user` VALUES ('viethungpdu90@gmail.com', 1, 1, 1, 'viethungpdu90@gmail.com', 'viethungpdu90@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 02:32:08', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('vinhnq@geovietnam.com', 1, 1, 1, 'vinhnq@geovietnam.com', 'vinhnq@geovietnam.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-21 09:52:38', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('Vothiyenoanh2468@gmail.com', 1, 1, 1, 'Vothiyenoanh2468@gmail.com', 'Vothiyenoanh2468@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 18:25:22', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('vuhoan99@gmail.com', 1, 1, 1, 'vuhoan99@gmail.com', 'vuhoan99@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 12:54:52', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('vuthanghy89@gmail.com', 1, 1, 1, 'vuthanghy89@gmail.com', 'vuthanghy89@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 07:01:33', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('xuanan10@gmail.com', 1, 1, 1, 'xuanan10@gmail.com', 'xuanan10@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-17 04:37:15', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('yocoin.vn@gmail.com', 1, 1, 1, 'yocoin.vn@gmail.com', 'yocoin.vn@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-15 12:54:10', 'LotteryCarcoin', NULL, NULL, 1);
INSERT INTO `user` VALUES ('zon.zip.9@gmail.com', 1, 1, 1, 'zon.zip.9@gmail.com', 'zon.zip.9@gmail.com', '', 20.00000, NULL, 0.00000, NULL, '2018-07-18 15:14:51', 'LotteryCarcoin', NULL, NULL, 1);

-- ----------------------------
-- Table structure for userevents
-- ----------------------------
DROP TABLE IF EXISTS `userevents`;
CREATE TABLE `userevents`  (
  `EventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `EventDescription` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventData` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventData1` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventData2` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventSource` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EventType` smallint(6) NULL DEFAULT 0 COMMENT '1: Create, 2: Lock, 3: Unlock',
  `UserId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ClientId` smallint(6) NOT NULL,
  `SystemId` smallint(6) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`EventId`) USING BTREE,
  INDEX `FK_UserEvents_User`(`UserId`, `SystemId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of userevents
-- ----------------------------
INSERT INTO `userevents` VALUES (2, 'lock user', 'ly do lock', 'administrator', NULL, NULL, 2, 'kienduc94@gmail.com', 1, 1, '2018-06-13 15:02:43');
INSERT INTO `userevents` VALUES (3, 'unlock user', 'ly do unlock', NULL, 'administrator', NULL, 3, 'kienduc94@gmail.com', 1, 1, '2018-06-13 15:03:31');

-- ----------------------------
-- Table structure for wallet
-- ----------------------------
DROP TABLE IF EXISTS `wallet`;
CREATE TABLE `wallet`  (
  `WalletId` bigint(20) NOT NULL AUTO_INCREMENT,
  `ClientId` smallint(6) NOT NULL,
  `SystemId` smallint(6) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UpdatedDate` datetime NULL DEFAULT NULL,
  `UpdatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1: Active, 2: Locked, 3: TemporaryUnlock, 4: TemporaryLock',
  `ProviderCode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DailyWithdrawLimit` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`WalletId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wallet
-- ----------------------------
INSERT INTO `wallet` VALUES (1, 1, 1, '2018-06-12 17:52:27', 'administrator', '2018-06-13 14:13:08', 'administrator', 1, 'BBC', 20);
INSERT INTO `wallet` VALUES (2, 1, 1, '2018-06-12 17:52:27', 'administrator', NULL, NULL, 1, 'BBC', 15);
INSERT INTO `wallet` VALUES (3, 1, 1, '2018-06-13 10:25:29', 'administrator', NULL, NULL, 1, 'BBC', 15);

-- ----------------------------
-- Table structure for walletevents
-- ----------------------------
DROP TABLE IF EXISTS `walletevents`;
CREATE TABLE `walletevents`  (
  `EventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `EventDescription` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventData` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventData1` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventData2` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EventSource` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EventType` smallint(6) NULL DEFAULT 0 COMMENT '1: Create, 2: Lock, 3: Unlock',
  `WalletId` bigint(20) NOT NULL,
  `ClientId` smallint(6) NOT NULL,
  `SystemId` smallint(6) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`EventId`) USING BTREE,
  INDEX `FK_WalletEvents_Wallet`(`WalletId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of walletevents
-- ----------------------------
INSERT INTO `walletevents` VALUES (2, 'ly do lock wallet', 'lock wallet', 'administrator', NULL, NULL, 2, 28, 1, 1, '2018-06-13 14:12:22');
INSERT INTO `walletevents` VALUES (3, 'unlock', 'ly do unlock wallet', NULL, 'administrator', NULL, 3, 28, 1, 1, '2018-06-13 14:13:08');
INSERT INTO `walletevents` VALUES (4, 'ly do lock wallet', 'lock wallet', 'administrator', NULL, NULL, 2, 17, 1, 1, '2018-06-13 14:28:53');

-- ----------------------------
-- Table structure for withdrawaddresswhitelist
-- ----------------------------
DROP TABLE IF EXISTS `withdrawaddresswhitelist`;
CREATE TABLE `withdrawaddresswhitelist`  (
  `ClientId` smallint(6) NOT NULL,
  `SystemId` smallint(6) NOT NULL,
  `UserId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UpdatedDate` datetime NULL DEFAULT NULL,
  `UpdatedBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CurrencyId` smallint(6) NOT NULL,
  `RecordId` bigint(20) NOT NULL,
  PRIMARY KEY (`RecordId`) USING BTREE,
  INDEX `FK_WithdrawAddressWhiteList_User`(`UserId`, `SystemId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for withdrawqueue
-- ----------------------------
DROP TABLE IF EXISTS `withdrawqueue`;
CREATE TABLE `withdrawqueue`  (
  `QueueId` bigint(20) NOT NULL,
  `TransactionIds` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Status` tinyint(3) UNSIGNED NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CompletedDate` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`QueueId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Function structure for F_ADDRESS_READY_WITHDRAW_UPDAVAIL_FE
-- ----------------------------
DROP FUNCTION IF EXISTS `F_ADDRESS_READY_WITHDRAW_UPDAVAIL_FE`;
delimiter ;;
CREATE FUNCTION `F_ADDRESS_READY_WITHDRAW_UPDAVAIL_FE`(p_AmountReal decimal(18, 10),
	p_CreatedBy VARCHAR(100),
	p_FromUserId varchar(100),
	p_CurrencyCode int,
	p_FromSystemId int,
	p_FromClientId int,
	p_Status int)
 RETURNS int(20)
  READS SQL DATA 
BEGIN	
		Update Address
		Set Address.InWithdraw = Address.InWithdraw + p_AmountReal,
				Address.Available = (Address.Available - p_AmountReal),
				Address.UpdatedBy = p_CreatedBy,
				Address.UpdatedDate = UTC_TIMESTAMP()
		Where Address.UserID =  p_FromUserId
				And Address.CurrencyCode = p_CurrencyCode
				And Address.SystemID = p_FromSystemId
				And Address.ClientID = p_FromClientId
				And p_Status = 5; /*5: ReadyToProcess*/
		
		Set @v_RowCount = ROW_COUNT();
		
		RETURN @v_RowCount;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for F_TRANSACTION_CHECK_ENOUGHBALANCE_FE
-- ----------------------------
DROP FUNCTION IF EXISTS `F_TRANSACTION_CHECK_ENOUGHBALANCE_FE`;
delimiter ;;
CREATE FUNCTION `F_TRANSACTION_CHECK_ENOUGHBALANCE_FE`(p_FromUserId varchar(100),
	p_CurrencyCode int,
	p_FromSystemId int,
	p_FromClientId int,
	p_AmountReal decimal(18, 10))
 RETURNS int(20)
  READS SQL DATA 
BEGIN	
     /*0: not enough available, 1: enough available for withdraw*/
		SET @v_IsEnoughBalance = 0;
	
		Select 1 iNTO @v_IsEnoughBalance
		From Address
		Where Address.UserID =  p_FromUserId
			And Address.CurrencyCode = p_CurrencyCode
			And Address.SystemID = p_FromSystemId
			And Address.ClientID = p_FromClientId
			And (
						p_AmountReal >= 0
						And (Address.Available - p_AmountReal) >= 0
					);
		
		RETURN @v_IsEnoughBalance;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for F_TRANSACTION_SRH_TOTALROWS_FE
-- ----------------------------
DROP FUNCTION IF EXISTS `F_TRANSACTION_SRH_TOTALROWS_FE`;
delimiter ;;
CREATE FUNCTION `F_TRANSACTION_SRH_TOTALROWS_FE`(p_UserId varchar(100) /* = NULL */,
	p_WalletId int /* = -1 */,
	p_CurrencyCode int /* = 0 */,
	p_FromDate DateTime /* = NULL */,
	p_ToDate DateTime /* = NULL */,
	p_TransactionType int,
	p_ClientID int,
	p_SystemID int,
	p_Status int)
 RETURNS int(20)
  READS SQL DATA 
BEGIN	
	SET @v_TotalRows = 0;
	
		Select Count(*)  INTO @v_TotalRows
			From Transaction
			Where (
							(
								Transaction.TransactionType != 1
								And Transaction.FromClientID = p_ClientID
							)
							Or 
							(
								Transaction.TransactionType = 1
								And Transaction.ToClientId = p_ClientID
							)
						)
						And (
									(
										Transaction.TransactionType != 1
										And Transaction.FromSystemId = p_SystemID
									)
									Or 
									(
										Transaction.TransactionType = 1
										And Transaction.ToSystemId = p_SystemID
									)
								)
						And (
									p_TransactionType = 0 
									Or Transaction.TransactionType = p_TransactionType
								)
						And (
									p_FromDate IS NULL
									Or DATE(Transaction.CreatedDate) >= DATE(p_FromDate)
								)
						And (
									p_ToDate IS NULL
									Or DATE(Transaction.CreatedDate) <= DATE(p_ToDate)
								)
						And (
									p_UserId = '-1'
									Or p_UserId = ''
									Or p_UserId IS NULL
									Or (
											Exists(
															Select 1
															From User
															Where User.UserID = p_UserId
																	And User.SystemID = p_SystemID
																	And User.ClientID = p_ClientID
																	And (
																				(
																					Transaction.TransactionType != 1
																					And Transaction.FromUserID = User.UserID
																				)
																				Or 
																				(
																					Transaction.TransactionType = 1
																					And Transaction.ToUserID = User.UserID
																				)
																			)
														)
										)
								)
						And (
									p_WalletId <= 0
									Or Exists(
														Select 1
														From User
														Where User.WalletID = p_WalletId
																And User.SystemID = p_SystemID
																And User.ClientID = p_ClientID
																And (
																				(
																					Transaction.TransactionType != 1
																					And Transaction.FromUserID = User.UserID
																				)
																				Or 
																				(
																					Transaction.TransactionType = 1
																					And Transaction.ToUserID = User.UserID
																				)
																			)
																And Exists(
																						Select 1
																						From Wallet
																						Where Wallet.WalletID = User.WalletID
																					)
														)
									)
						 And (
									p_CurrencyCode <= 0
									Or (
											Transaction.CurrencyCode = p_CurrencyCode
										)
									)
						And (
									p_Status <= 0
									Or Transaction.Status = p_Status
						     );
		
	RETURN @v_TotalRows;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for F_USER_WITHDRAWONDAY_UPD_FE
-- ----------------------------
DROP FUNCTION IF EXISTS `F_USER_WITHDRAWONDAY_UPD_FE`;
delimiter ;;
CREATE FUNCTION `F_USER_WITHDRAWONDAY_UPD_FE`(p_WithdrawOnDay decimal(18, 5),
	p_CreatedBy VARCHAR(100),
	p_FromUserId varchar(100),
	p_FromSystemId int,
	p_FromClientId int)
 RETURNS int(20)
  READS SQL DATA 
BEGIN	
		Update `User`
		Set `User`.WithdrawOnDay = p_WithdrawOnDay + 1,
				`User`.WithdrawDate = UTC_TIMESTAMP(),
				`User`.UpdatedBy = p_CreatedBy,
				`User`.UpdatedDate = UTC_TIMESTAMP()
		Where `User`.UserID =  p_FromUserId
				And `User`.SystemID = p_FromSystemId
				And `User`.ClientID = p_FromClientId;
		
		Set @v_RowCount = ROW_COUNT();
		
		RETURN @v_RowCount;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_AddressEvents_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_AddressEvents_Add`;
delimiter ;;
CREATE PROCEDURE `SP_AddressEvents_Add`(p_AddressID varchar(100),
	p_ClientID int,
	p_SystemID int,
	p_EventData longtext /* = NULL */,
	p_EventData1 longtext /* = NULL */,
	p_EventDescription longtext /* = NULL */,
	p_Status int)
Begin
  
   Declare v_CreatedDate datetime;
	  DECLARE ISERROR INT;
	 
	 DECLARE _ROLLBACK INT DEFAULT 0;
	 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET _ROLLBACK = 1;
	 START TRANSACTION;
	 
		Set v_CreatedDate = UTC_TIMESTAMP();
		
		IF(p_Status = 2 Or p_Status = 4) 
			Then
				-- @Status Lock: 2: Locked, 4: TemporaryLock
				Insert into AddressEvents(EventData,
										EventData1,
										EventType,
										EventDescription,
										AddressId,
										ClientId,
										SystemId,
										CreatedDate)
				Values (p_EventData,
						p_EventData1,
						2,
						p_EventDescription,
						p_AddressID,
						p_ClientID,
						p_SystemID,
						v_CreatedDate);
		Else
				-- @Status Unlock: 1: Active, 3: TemporaryUnlock
				Insert into AddressEvents(EventData,
										EventData2,
										EventType,
										EventDescription,
										AddressId,
										ClientId,
										SystemId,
										CreatedDate)
				Values (p_EventData,
						p_EventData1,
						3,
						p_EventDescription,
						p_AddressID,
						p_ClientID,
						p_SystemID,
						v_CreatedDate);
			End if;

			Set @p_EventID = LAST_INSERT_ID();

		-- if insert success -> update status in table Wallet
		Update Address
		set Address.Status = p_Status,
			Address.UpdatedBy = p_EventData1,
			Address.UpdatedDate = v_CreatedDate
		Where Address.AddressID = p_AddressID
		      AND Address.SystemID = p_SystemID;

	SET ISERROR = _ROLLBACK;
	IF
		ISERROR = 1 THEN
		Set @p_EventID = 0;
		ROLLBACK;
	ELSE COMMIT;
	END IF;
	
	Select @p_EventID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_AddressEvents_Sel
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_AddressEvents_Sel`;
delimiter ;;
CREATE PROCEDURE `SP_AddressEvents_Sel`(p_intEventID int)
Begin
	Select AddressEvents.EventId,
	       AddressEvents.AddressId,
		   AddressEvents.ClientId,
		   AddressEvents.SystemId,
		   AddressEvents.EventData,
		   AddressEvents.EventData1,
		   AddressEvents.EventData2,
		   AddressEvents.EventDescription,
		   AddressEvents.EventSource,
		   AddressEvents.EventType,
		   AddressEvents.CREATEdDate
	From AddressEvents
	Where AddressEvents.EventId = p_intEventID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_AddressEvents_SRH
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_AddressEvents_SRH`;
delimiter ;;
CREATE PROCEDURE `SP_AddressEvents_SRH`(p_AddressID varchar(100) /* = Null */,
	p_ClientID int,
	p_SystemID int,
	p_FromIndex int /* = 0 */,
	p_Limit int)
Begin
  SET @v_Row = 0;
	
	Select AddrEv.EventId,
	       AddrEv.AddressId,
		   AddrEv.ClientId,
		   AddrEv.SystemId,
		   AddrEv.EventData,
		   AddrEv.EventData1,
		   AddrEv.EventData2,
		   AddrEv.EventDescription,
		   AddrEv.EventSource,
		   AddrEv.EventType,
		   AddrEv.CreatedDate,
		   AddrEv.RowNum,
		   AddrEv.TotalRecord
	From (
			Select AddressEvents.EventId,
				   AddressEvents.AddressId,
				   AddressEvents.ClientId,
				   AddressEvents.SystemId,
				   AddressEvents.EventData,
				   AddressEvents.EventData1,
				   AddressEvents.EventData2,
				   AddressEvents.EventDescription,
				   AddressEvents.EventSource,
				   AddressEvents.EventType,
				   AddressEvents.CreatedDate,
				   (@v_Row := @v_Row + 1) AS RowNum,
				   (
							Select Count(*)
							From AddressEvents
							Where (
											p_AddressID = '-1'
											Or p_AddressID = ''
											Or p_AddressID IS NULL
											Or AddressEvents.AddressId = p_AddressID
										)
										And AddressEvents.ClientId = p_ClientID
										And AddressEvents.SystemId = p_SystemID
					 ) TotalRecord
			From AddressEvents
			Where (
							p_AddressID = '-1'
							Or p_AddressID = ''
							Or p_AddressID IS NULL
							Or AddressEvents.AddressId = p_AddressID
						)
						And AddressEvents.ClientId = p_ClientID
						And AddressEvents.SystemId = p_SystemID
		) AddrEv
	Where AddrEv.RowNum > (p_FromIndex * p_Limit)
	      And AddrEv.RowNum <= ((p_FromIndex + 1) * p_Limit);
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Address_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Address_Add`;
delimiter ;;
CREATE PROCEDURE `SP_Address_Add`(p_ClientID int,
	p_SystemID int,
	p_UserID varchar(100),
	p_Name nvarchar(200),
	p_CurrencyCode smallint,
	p_strAddress varchar(100),
	p_CREATEdBy nvarchar(100),
	out p_intIsExists smallint)
Begin
	Set p_intIsExists = 0;
	Begin
		Select 1 Into p_intIsExists
		From Address
		Where (
						Address.AddressID = p_strAddress
						Or (
									Address.UserID =  p_UserID
									And Address.CurrencyCode = p_CurrencyCode
							 )
					)
					And Address.SystemID = p_SystemID;
	End;

	If(p_intIsExists = 0)
		Then
			Insert into Address(AddressId,
													SystemID,
													ClientID,
													UserID,
													Name,
													CurrencyCode,
													CREATEdDate,
													CREATEdBy)
			Values (p_strAddress,
							p_SystemID,
							p_ClientID,
							p_UserID,
							p_Name,
							p_CurrencyCode,
							UTC_TIMESTAMP(),
							p_CREATEdBy);
		End if;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Address_Sel
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Address_Sel`;
delimiter ;;
CREATE PROCEDURE `SP_Address_Sel`(p_CurrencyCode smallint,
	p_UserID varchar(100),
	p_ClientID int,
	p_SystemID int)
Begin
	Select Address.ClientID,
		   Address.SystemID,
		   Address.AddressID,
		   Address.UserID,
		   Address.Name,
		   Address.CurrencyCode,
		   Address.Total,
		   Address.Available,
		   Address.InBlockChain,
		   Address.InLock,
		   Address.InInternalDeposit,
		   Address.InWithdraw,
		   Address.Status
	From Address
	Where Address.UserID =  p_UserID
				And Address.CurrencyCode = p_CurrencyCode
	      And Address.SystemID = p_SystemID
				And Address.ClientID = p_ClientID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Address_SRH
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Address_SRH`;
delimiter ;;
CREATE PROCEDURE `SP_Address_SRH`(p_strUserID varchar(100) /* = NULL */,
	p_ClientID int /* = 0 */,
	p_SystemID int /* = 0 */,
	p_FromIndex int /* = 0 */,
	p_Limit int)
Begin
  SET @v_Row = 0;
	
	Select Addr.ClientID,
		   Addr.SystemID,
		   Addr.AddressID,
		   Addr.UserID,
		   Addr.Name,
		   Addr.CurrencyCode,
		   Addr.Total,
		   Addr.Available,
		   Addr.InBlockChain,
		   Addr.InLock,
		   Addr.InInternalDeposit,
		   Addr.InWithdraw,
		   Addr.Status,
		   Addr.RowNum,
		   Addr.TotalRecord
	From (
			Select Address.ClientID,
				   Address.SystemID,
				   Address.AddressID,
				   Address.UserID,
				   Address.Name,
				   Address.CurrencyCode,
				   Address.Total,
				   Address.Available,
				   Address.InBlockChain,
				   Address.InLock,
				   Address.InInternalDeposit,
				   Address.InWithdraw,
				   Address.Status,
				   (@v_Row := @v_Row + 1) AS RowNum,
				   (
							Select Count(*)
							From Address
							Where (
											p_SystemID <= 0
											Or Address.SystemID = p_SystemID
										)
										And (
													p_ClientID <= 0
													Or Address.ClientID = p_ClientID
												)
										And (
													p_strUserID = '-1'
													Or p_strUserID = ''
													Or p_strUserID IS NULL
													Or (
															Exists(
																			Select 1
																			From User
																			Where User.UserID = p_strUserID
																					And User.SystemID = p_SystemID
																					And User.ClientID = p_ClientID
																					And Address.UserID = User.UserID
																)
														)
												)
					 ) TotalRecord
			From Address
			Where (
							p_SystemID <= 0
							Or Address.SystemID = p_SystemID
						)
						And (
									p_ClientID <= 0
									Or Address.ClientID = p_ClientID
								)
						And (
									p_strUserID = '-1'
									Or p_strUserID = ''
									Or p_strUserID IS NULL
									Or ( 
											Exists(
															Select 1
															From User
															Where User.UserID = p_strUserID
																	And User.SystemID = p_SystemID
																	And User.ClientID = p_ClientID
																	And Address.UserID = User.UserID
												)
										)
								)
		) Addr
	Where (
					p_FromIndex < 0
					Or (
							Addr.RowNum > (p_FromIndex * p_Limit)
							And Addr.RowNum <= ((p_FromIndex + 1) * p_Limit)
						 )
				);
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_ProviderDetail_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_ProviderDetail_Add`;
delimiter ;;
CREATE PROCEDURE `SP_ProviderDetail_Add`(p_WalletId int,
	p_Key varchar(100),
	p_Value longtext,
	p_RequestedBy varchar(100))
Begin
  /*SET autocommit = 1;*/
	
	Insert into ProviderDetail(
															WalletId,
															`Key`,
															`Value`,
															CREATEdBy,
															CREATEdDate
														)
	Values (p_WalletId,
					p_Key,
					p_Value,
					p_RequestedBy,
					UTC_TIMESTAMP());
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_ProviderDetail_Sel
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_ProviderDetail_Sel`;
delimiter ;;
CREATE PROCEDURE `SP_ProviderDetail_Sel`(p_WalletId int)
Begin
	Select ProviderDetail.`Key`,
		   ProviderDetail.Value
	From ProviderDetail
	Where ProviderDetail.WalletId = p_WalletId;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Provider_Sel
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Provider_Sel`;
delimiter ;;
CREATE PROCEDURE `SP_Provider_Sel`(p_ProviderID int)
Begin
	Select Provider.ProviderID,
		   Provider.ProviderName,
		   Provider.`Key`,
		   Provider.Value
	From Provider
	Where Provider.ProviderID = p_ProviderID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_TransactionEvents_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_TransactionEvents_Add`;
delimiter ;;
CREATE PROCEDURE `SP_TransactionEvents_Add`(p_TransactionID varchar(100),
	p_ClientID int,
	p_UserID varchar(100),
	p_Amount decimal(18, 0),
	p_EventData longtext /* = NULL */,
	p_EventData1 longtext /* = NULL */,
	p_EventData3 longtext /* = NULL */,
	p_EventDescription longtext /* = NULL */,
	p_Status int)
Begin
   Declare v_CREATEdDate DateTime;
	 DECLARE ISERROR INT;
	 DECLARE _ROLLBACK INT DEFAULT 0;
	 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET _ROLLBACK = 1;
	 START TRANSACTION;

		Set v_CREATEdDate = UTC_TIMESTAMP();

		IF(p_Status = 2 Or p_Status = 4) 
			Then
				-- @Status Lock: 2: Locked, 4: TemporaryLock
				Insert into TransactionEvents(EventData,
										EventData1,
										EventData3,
										EventType,
										TransactionId,
										ClientId,
										UserID,
										Amount,
										CREATEdDate)
				Values (p_EventData,
						p_EventData1,
						p_EventData3,
						2,
						p_TransactionID,
						p_ClientID,
						p_UserID,
						p_Amount,
						v_CREATEdDate);
		Else
				-- @Status Unlock: 1: Active, 3: TemporaryUnlock
				Insert into TransactionEvents(EventData,
										EventData2,
										EventData3,
										EventType,
										EventDescription,
										TransactionId,
										ClientId,
										UserID,
										Amount,
										CREATEdDate)
				Values (p_EventData,
						p_EventData1,
						p_EventData3,
						3,
						p_EventDescription,
						p_TransactionID,
						p_ClientID,
						p_UserID,
						p_Amount,
						v_CREATEdDate);
			End if;

		-- if insert success -> update status in table Wallet
		Update Transaction
		set Transaction.Status = p_Status,
			Transaction.UpdatedBy = p_EventData1,
			Transaction.UpdatedDate = v_CREATEdDate
		Where Transaction.TransactionID = p_TransactionID;

		Select v_CREATEdDate;
	SET ISERROR = _ROLLBACK;
	IF
		ISERROR = 1 THEN 
		Set v_CREATEdDate = NULL;
		ROLLBACK;
	ELSE COMMIT;
	END IF;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_TransactionEvents_Sel
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_TransactionEvents_Sel`;
delimiter ;;
CREATE PROCEDURE `SP_TransactionEvents_Sel`(p_TransactionID varchar(100),
	p_ClientID int,
	p_UserID varchar(100),
	p_Status int)
Begin
  SET @v_Row = 0;
	
	Select Trans.EventId,
		   Trans.EventData,
		   Trans.EventData1,
		   Trans.EventData2,
		   Trans.EventData3,
		   Trans.TransactionID,
		   Trans.EventDescription,
		   Trans.Amount,
		   Trans.EventType
	From (
			Select TransactionEvents.EventId,
				   TransactionEvents.EventData,
				   TransactionEvents.EventData1,
				   TransactionEvents.EventData2,
				   TransactionEvents.EventData3,
				   TransactionEvents.TransactionID,
				   TransactionEvents.EventDescription,
				   TransactionEvents.Amount,
				   TransactionEvents.EventType,
				   (@v_Row := @v_Row + 1) AS RowNumber
			From TransactionEvents
			Where TransactionEvents.TransactionID = p_TransactionID
				  And TransactionEvents.ClientId = p_ClientID
				  And TransactionEvents.UserID = p_UserID
				  And (
						(
							(
								p_Status = 1
								Or p_Status = 3
							)
							And TransactionEvents.EventType = 3
						)
						Or 
						(
							(
								p_Status = 2
								Or p_Status = 4
							)
							And TransactionEvents.EventType = 2
						)
					  )
		) Trans
	Where Trans.RowNumber = 1;

End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Transaction_Deposit_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Transaction_Deposit_Add`;
delimiter ;;
CREATE PROCEDURE `SP_Transaction_Deposit_Add`(p_TransactionTypeID int,
	p_ToClientId int,
	p_ToUserId varchar(100),
	p_ToSystemId int,
	p_Data1 varchar(100),
	p_CurrencyCode int /* = 0 */,
	p_Amount decimal(18, 10),
	p_Description varchar(100) /* = NULL */,
	p_CREATEdBy nvarchar(100),
	p_Data2 varchar(100) /* = NULL */,
	out p_TransactionID varchar(100))
Begin
	Set p_TransactionID = NULL;-- NULL If To User Not Exists In Table [User] And SystemID

	Set @v_IsExistsToUser = 0;
	Begin
		Select @v_IsExistsToUser := 1
		From User
		Where User.UserID =  p_ToUserId
				And User.SystemID = p_ToSystemId;
	End;

	Set @IsExistsHashTx = 0;
	Begin
		Select @IsExistsHashTx := 1
		From `transaction`
		Where transaction.Data2 = p_Data2;
	End;
	
	IF(@v_IsExistsToUser = 1 And @IsExistsHashTx = 0) 
		Then
			/*@TransactionTypeID: 1: Deposit*/
			Set p_TransactionID = UUID();

			Insert into Transaction(TransactionID,
								TransactionType,
								FromClientId,
								ToClientId,
								FromSystemId,
								ToSystemId,
								FromUserId,
								ToUserId,
								CurrencyCode,
								Amount,
								Description,
								CREATEdBy,
								CREATEdDate, 
								Status,
								Data1,
								Data2,
								Fee)
			Values (p_TransactionID,
					p_TransactionTypeID,
					0,
					p_ToClientId,
					0,
					p_ToSystemId,
					NULL,
					p_ToUserId,
					p_CurrencyCode,
					p_Amount,
					p_Description,
					p_CREATEdBy,
					UTC_TIMESTAMP(),
					2,
					p_Data1,
					p_Data2,
					0);
		End if;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Transaction_Deposit_Upd
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Transaction_Deposit_Upd`;
delimiter ;;
CREATE PROCEDURE `SP_Transaction_Deposit_Upd`(p_TransactionID varchar(100),
	p_ClientId int,
	p_SystemId int,
	p_Status int, 
	p_Confirmation bigint,
	p_UpdatedBy varchar(100))
Begin
	Update Transaction
	Set Transaction.Confirmation = p_Confirmation,
	    Transaction.UpdatedBy = p_UpdatedBy,
		Transaction.UpdatedDate = UTC_TIMESTAMP()
	Where Transaction.TransactionID = p_TransactionID
	      And Transaction.ToClientID = p_ClientId
		  And Transaction.ToSystemID = p_SystemId;
	
	If(ROW_COUNT() > 0)
		Then
				Update Transaction
				Set Transaction.Status = p_Status,
						Transaction.UpdatedBy = p_UpdatedBy,
					Transaction.UpdatedDate = UTC_TIMESTAMP()
				Where Transaction.TransactionID = p_TransactionID
							And Transaction.ToClientID = p_ClientId
						And Transaction.ToSystemID = p_SystemId
						And Transaction.Status != 3; -- Status not be successfully;

				If(ROW_COUNT() > 0 And p_Status = 3)
					Then
						/*Update success and TransactionStatus = 3: Successfully*/
						Set @v_Amount = 0;
						Set @v_ToUserId = NULL;
						Set @v_CurrencyCode = 0;

						Begin
							Select @v_Amount := Transaction.Amount,
									 @v_ToUserId := Transaction.ToUserID,
									 @v_CurrencyCode := Transaction.CurrencyCode
							From Transaction
							Where Transaction.TransactionID = p_TransactionID
									And Transaction.ToClientID = p_ClientId
									And Transaction.ToSystemID = p_SystemId
									And Transaction.TransactionType = 1
									AND Transaction.Status = 3; -- Deposit: 1
						End;

						Update Address
						Set Address.Available = Address.Available + @v_Amount,
							Address.Total = Address.Total + @v_Amount,
							Address.UpdatedBy = p_UpdatedBy,
							Address.UpdatedDate = UTC_TIMESTAMP()
						Where Address.UserID =  @v_ToUserId
									And Address.CurrencyCode = @v_CurrencyCode
								And Address.ClientID = p_ClientId
								And Address.SystemID = p_SystemId;
					End if;
		End If;
	
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Transaction_InternalDeposit_Upd
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Transaction_InternalDeposit_Upd`;
delimiter ;;
CREATE PROCEDURE `SP_Transaction_InternalDeposit_Upd`(p_TransactionID varchar(100),
	p_ClientId int,
	p_SystemId int,
	p_Status int, 
	p_Confirmation bigint,
	p_UpdatedBy varchar(100))
Begin
	Update Transaction
	Set Transaction.Status = p_Status,
		Transaction.Confirmation = p_Confirmation,
	    Transaction.UpdatedBy = p_UpdatedBy,
		Transaction.UpdatedDate = UTC_TIMESTAMP()
	Where Transaction.TransactionID = p_TransactionID
	      And Transaction.FromClientID = p_ClientId
		  And Transaction.FromSystemID = p_SystemId
		  And Transaction.Status != 3 -- Status not be successfully;
		  And Transaction.TransactionType = 2; -- InternalDeposit/TransferToMasterAddr: 2

	If(FOUND_ROWS() > 0 And p_Status = 3)
		Then
			/*Update success and TransactionStatus = 3: Successfully*/
			Set @v_Amount = 0;
			Set @v_FromUserId = NULL;
			Set @v_CurrencyCode = 0;

			Begin
				Select @v_Amount = Transaction.Amount,
					   @v_FromUserId = Transaction.FromUserID,
					   @v_CurrencyCode = Transaction.CurrencyCode
				From Transaction
				Where Transaction.TransactionID = p_TransactionID
					  And Transaction.FromClientID = p_ClientId
					  And Transaction.FromSystemID = p_SystemId
					  And Transaction.TransactionType = 2; -- InternalDeposit/TransferToMasterAddr: 2
			End;

			Update Address
			Set Address.InInternalDeposit = Case
										  When (Address.InInternalDeposit - v_Amount) >= 0 then (Address.InInternalDeposit - v_Amount)
										  Else 0
									   End,
				Address.InBlockChai = Case
											When (Address.InBlockChain - v_Amount) >= 0 then (Address.InBlockChain - v_Amount)
											Else 0
										  End
			Where Address.UserID =  v_FromUserId
			      And Address.CurrencyCode = v_CurrencyCode
				  And Address.ClientID = p_ClientId
				  And Address.SystemID = p_SystemId;
		End if;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Transaction_Lock_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Transaction_Lock_Add`;
delimiter ;;
CREATE PROCEDURE `SP_Transaction_Lock_Add`(p_TransactionTypeID int,
	p_FromClientId int,
	p_FromSystemId int,
	p_Data1 varchar(100),
	p_FromUserId varchar(100) /* = NULL */,
	p_CurrencyCode int /* = 0 */,
	p_Amount decimal(18, 10),
	p_Description varchar(100) /* = NULL */,
	p_CREATEdBy nvarchar(100),
	out p_TransactionID varchar(100))
Begin
	Set p_TransactionID = NULL;-- NULL If To User Not Exists In Table [User] And SystemID

	Set @v_IsExistsFromUser = 0;
	Begin
		Select @v_IsExistsFromUser := 1
		From User
		Where User.UserID =  p_FromUserId
			  And User.SystemID = p_FromSystemId;
	End;

	IF(@v_IsExistsFromUser = 1) 
		Then
			Set @v_Available = 0;
			Set @v_InLock = 0;
					
			IF(p_TransactionTypeID = 6) 
				Then
					-- TransactionType = 6: Lock
					-- --• Available -= k
					-- --• InLock += k
					Select @v_Available := Case	
																	When ((Address.Available - p_Amount) >= 0) then (Address.Available - p_Amount)
																	When ((Address.Available - p_Amount) < 0) then -1
																 End As Available,
								 @v_InLock := Case	
																When (Address.Available < p_Amount) then Address.InLock + Address.Available -- Lock
																When (Address.Available >= p_Amount) then Address.InLock + p_Amount -- Lock
															 End As InLock
						From Address
						Where Address.AddressID = p_Data1
									And Address.UserID =  p_FromUserId
								And Address.CurrencyCode = p_CurrencyCode
								And Address.SystemID = p_FromSystemId
								And Address.ClientID = p_FromClientId;
			ELSEIF (p_TransactionTypeID = 7) 
				Then
					-- TransactionType = 7: Unlock
					-- --• Available  += k
					-- --• InLock -=k
					Select @v_Available := Case	
																	 When (Address.InLock < p_Amount) then Address.Available + Address.InLock -- UnLock
																	 When (Address.InLock >= p_Amount) then Address.Available + p_Amount -- UnLock
																 End As Available,
								 @v_InLock := Case	
																When ((Address.InLock - p_Amount) >= 0) then (Address.InLock - p_Amount)
																When ((Address.InLock - p_Amount) < 0) then -1
															 End As InLock
						From Address
						Where Address.AddressID = p_Data1
									And Address.UserID =  p_FromUserId
								And Address.CurrencyCode = p_CurrencyCode
								And Address.SystemID = p_FromSystemId
								And Address.ClientID = p_FromClientId;
			END IF;

			Update Address
			Set Address.Available = @v_Available,
				Address.InLock = @v_InLock
			Where Address.AddressID = p_Data1
					And Address.UserID =  p_FromUserId
					And Address.CurrencyCode = p_CurrencyCode
					And Address.SystemID = p_FromSystemId
					And Address.ClientID = p_FromClientId
					And (
							(
								p_TransactionTypeID = 6
								And @v_Available >= 0
							)
							Or
							(
								p_TransactionTypeID = 7 -- UnLock
								And @v_InLock >= 0
							)
						);

			IF(FOUND_ROWS() > 0)
				Then
				    Set p_TransactionID = UUID();

					Insert into Transaction(TransactionID,
										TransactionType,
										FromClientId,
										ToClientId,
										FromSystemId,
										ToSystemId,
										FromUserId,
										ToUserId,
										CurrencyCode,
										Amount,
										Description,
										CREATEdBy,
										CREATEdDate, 
										Status,
										Data1)
					Values (p_TransactionID,
							p_TransactionTypeID,
							p_FromClientId,
							0,
							p_FromSystemId,
							0,
							p_FromUserId,
							NULL,
							p_CurrencyCode,
							p_Amount,
							p_Description,
							p_CREATEdBy,
							UTC_TIMESTAMP(),
							2,
							p_Data1);
				End if;
		End if;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Transaction_Sel
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Transaction_Sel`;
delimiter ;;
CREATE PROCEDURE `SP_Transaction_Sel`(p_TransactionID varchar(100),
	p_FromClientID int,
	p_FromSystemID int)
Begin
	Select Transaction.TransactionID,
		   Transaction.TransactionType,
		   Transaction.FromClientID,
		   Transaction.FromSystemID,
		   Transaction.FromUserID,
		   Transaction.ToClientID,
		   Transaction.ToSystemID,
		   Transaction.ToUserID,
		   Transaction.CurrencyCode,
		   Transaction.Amount,
		   Transaction.Description,
		   Transaction.Status,
		   Transaction.CREATEdBy,
		   Transaction.CREATEdDate,
		   Transaction.CompletedDate,
		   Case 
			  When Transaction.TransactionType = 5 then (
															Select Address.AddressID
															From Address
															Where Address.UserID =  Transaction.FromUserID
															      And Address.CurrencyCode = Transaction.CurrencyCode
																  And Address.SystemID = p_FromSystemID
																  And Address.ClientID = p_FromClientID
														  )
			  Else Transaction.Data1
		   End As FromAddress,
		   Case 
			  When (Transaction.TransactionType = 1 
					or Transaction.TransactionType = 3) then (
																Select Address.AddressID
																From Address
																Where Address.UserID =  Transaction.ToUserID
																	  And Address.CurrencyCode = Transaction.CurrencyCode
																	  And Address.SystemID = p_FromSystemID
																	  And Address.ClientID = p_FromClientID
															  )
			  Else Transaction.Data1
		   End As ToAddress,
		   Transaction.Data2 Tx,
		   Transaction.Fee
	From Transaction
	Where Transaction.TransactionID = p_TransactionID
		  And (
				(
					Transaction.TransactionType = 1
					And Transaction.ToClientID = p_FromClientID
					And Transaction.ToSystemID = p_FromSystemID
				)
				Or
				(
					Transaction.FromClientID = p_FromClientID
					And Transaction.FromSystemID = p_FromSystemID
				)
			  );
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Transaction_SRH
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Transaction_SRH`;
delimiter ;;
CREATE PROCEDURE `SP_Transaction_SRH`(p_UserId varchar(100) /* = NULL */,
	p_WalletId int /* = -1 */,
	p_CurrencyCode int /* = 0 */,
	p_FromDate DateTime /* = NULL */,
	p_ToDate DateTime /* = NULL */,
	p_TransactionType int,
	p_ClientID int,
	p_SystemID int,
	p_FromIndex int /* = 0 */,
	p_Limit int,
	p_Status int)
Begin
  SET @v_Row = 0;
	
	Select Trans.TransactionID,
		   Trans.TransactionType,
		   Trans.FromClientId,
		   Trans.FromSystemId,
		   Trans.FromUserId,
		   Trans.ToClientId,
		   Trans.ToSystemId,
		   Trans.ToUserId,
		   Trans.CurrencyCode,
		   Trans.Amount,
		   Trans.Description,
		   Trans.Status,
		   Trans.CreatedBy,
		   Trans.CreatedDate,
		   Trans.CompletedDate,
		   Trans.FromAddress,
		   Trans.ToAddress,
		   Trans.Tx,
		   Trans.Fee,
		   Trans.RowNum,
		   Trans.TotalRecord
	From (
			Select Transaction.TransactionID,
				   Transaction.TransactionType,
				   Transaction.FromClientID,
				   Transaction.FromSystemID,
				   Transaction.FromUserID,
				   Transaction.ToClientID,
				   Transaction.ToSystemID,
				   Transaction.ToUserID,
				   Transaction.CurrencyCode,
				   Transaction.Amount,
				   Transaction.Description,
				   Transaction.Status,
				   Transaction.CreatedBy,
				   Transaction.CreatedDate,
				   Transaction.CompletedDate,
				   Case 
					  When Transaction.TransactionType = 5 then (
																	Select Address.AddressID
																	From Address
																	Where Address.UserID = Transaction.FromUserID
																		  And Address.CurrencyCode = Transaction.CurrencyCode
																		  And Address.SystemID = p_SystemID
																		  And Address.ClientID = p_ClientID
																  )
					  Else Transaction.Data1
				   End As FromAddress,
				   Case 
					  When (Transaction.TransactionType = 1 
									or Transaction.TransactionType = 3) then (
																															Select Address.AddressID
																															From Address
																															Where Address.UserID = Transaction.ToUserID
																																	And Address.CurrencyCode = Transaction.CurrencyCode
																																	And Address.SystemID = p_SystemID
																																	And Address.ClientID = p_ClientID
																														)
					  Else Transaction.Data1
				   End As ToAddress,
		       Transaction.Data2 Tx,
				   Transaction.Fee,
				   (@v_Row := @v_Row + 1) AS RowNum,
				   F_TRANSACTION_SRH_TOTALROWS_FE(p_UserId, p_WalletId, 
																					p_CurrencyCode, p_FromDate, 
																					p_ToDate, p_TransactionType, 
																					p_ClientID, p_SystemID, p_Status) AS TotalRecord
			From Transaction
			Where (
							(
								Transaction.TransactionType != 1
								And Transaction.FromClientID = p_ClientID
							)
							Or 
							(
								Transaction.TransactionType = 1
								And Transaction.ToClientId = p_ClientID
							)
						)
						And (
									(
										Transaction.TransactionType != 1
										And Transaction.FromSystemId = p_SystemID
									)
									Or 
									(
										Transaction.TransactionType = 1
										And Transaction.ToSystemId = p_SystemID
									)
								)
						And (
									p_TransactionType = 0 
									Or Transaction.TransactionType = p_TransactionType
								)
						And (
									p_FromDate IS NULL
									Or DATE(Transaction.CreatedDate) >= DATE(p_FromDate)
								)
						And (
									p_ToDate IS NULL
									Or DATE(Transaction.CreatedDate) <= DATE(p_ToDate)
								)
						And (
									p_UserId = '-1'
									Or p_UserId = ''
									Or p_UserId IS NULL
									Or (
											Exists(
															Select 1
															From User
															Where User.UserID = p_UserId
																	And User.SystemID = p_SystemID
																	And User.ClientID = p_ClientID
																	And (
																				(
																					Transaction.TransactionType != 1
																					And Transaction.FromUserID = User.UserID
																				)
																				Or 
																				(
																					Transaction.TransactionType = 1
																					And Transaction.ToUserID = User.UserID
																				)
																			)
														)
										)
								)
						And (
									p_WalletId <= 0
									Or Exists(
														Select 1
														From User
														Where User.WalletID = p_WalletId
																And User.SystemID = p_SystemID
																And User.ClientID = p_ClientID
																And (
																				(
																					Transaction.TransactionType != 1
																					And Transaction.FromUserID = User.UserID
																				)
																				Or 
																				(
																					Transaction.TransactionType = 1
																					And Transaction.ToUserID = User.UserID
																				)
																			)
																And Exists(
																						Select 1
																						From Wallet
																						Where Wallet.WalletID = User.WalletID
																					)
														)
									)
						 And (
									p_CurrencyCode <= 0
									Or (
											Transaction.CurrencyCode = p_CurrencyCode
										)
									)
						And (
									p_Status <= 0
									Or Transaction.Status = p_Status
						     )
						ORDER BY Transaction.CreatedDate DESC
		) Trans
	Where (
			p_FromIndex < 0
			Or (
					Trans.RowNum > (p_FromIndex * p_Limit)
					And Trans.RowNum <= ((p_FromIndex + 1) * p_Limit)
			   )
		  );
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Transaction_Transfer_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Transaction_Transfer_Add`;
delimiter ;;
CREATE PROCEDURE `SP_Transaction_Transfer_Add`(p_TransactionTypeID int,
	p_FromClientId int,
	p_FromSystemId int,
	p_FromUserId varchar(100),
	p_Data1 varchar(100),
	p_ToClientId int,
	p_ToSystemId int,
	p_ToUserId varchar(100),
	p_CurrencyCode int /* = 0 */,
	p_Amount decimal(18, 10),
	p_Description varchar(100) /* = NULL */,
	p_CREATEdBy nvarchar(100),
	p_IsCheckBalance SMALLINT,
	out p_TransactionID varchar(100))
Begin
	Set p_TransactionID = NULL;-- NULL If To User Not Exists In Table [User] And SystemID

	Set @v_IsExistsFromUser = 0;
	Begin
		Select @v_IsExistsFromUser := 1
		From User
		Where User.UserID =  p_FromUserId
			  And User.SystemID = p_FromSystemId;
	End;

	/*Set @v_IsExistsToUser = 1;
	If(p_TransactionTypeID = 3 and p_ToUserId != NULL and p_ToSystemId != NULL)
	Then*/
		Set @v_IsExistsToUser = 0;
		Begin
			Select @v_IsExistsToUser := 1
			From User
			Where User.UserID =  p_ToUserId
				  And User.SystemID = p_ToSystemId;
		End;
	/*End if;*/

	IF(@v_IsExistsFromUser = 1 and @v_IsExistsToUser = 1) 
		Then
			Set @v_Available = -1;
			Set @v_Total = -1;
			
			-- TransactionType = 3: Transfer
			Begin
				Select @v_Available := (Available - p_Amount),
					   @v_Total := (Total - p_Amount)
				From Address FromAddr
				Where FromAddr.AddressId = p_Data1
						And FromAddr.SystemId = p_FromSystemId
						And FromAddr.ClientId = p_FromClientId
						And FromAddr.UserId = p_FromUserId;
			End;

			/*enough available in address*/
			If(p_IsCheckBalance = 0 Or (@v_Available >= 0 And @v_Total >= 0)) Then
					-- •	FromAddress.Available -= Amount,
					-- • FromAddress.Total -= Amount
					Update Address
					Set Address.Available = @v_Available,
						Address.Total = @v_Total
					Where Address.AddressID = p_Data1
							And Address.SystemID = p_FromSystemId
							And Address.ClientID = p_FromClientId
							And Address.UserID =  p_FromUserId;
							
					-- •	ToAddress.Available += Amount,
					-- • ToAddress.Total += Amount
					IF(ROW_COUNT() > 0)
						Then
							Update Address
							Set Address.Available = (Address.Available + p_Amount),
								Address.Total = (Address.Total + p_Amount)
							Where Address.CurrencyCode = p_CurrencyCode
									And Address.UserID =  p_ToUserId
									And Address.SystemID = p_ToSystemId
									And Address.ClientID = p_ToClientId;
						End if;
						
					IF(ROW_COUNT() > 0)
						Then
							/*@TransactionTypeID: 1: Deposit*/
								Set p_TransactionID = UUID();

							Insert into Transaction(TransactionID,
												TransactionType,
												FromClientId,
												ToClientId,
												FromSystemId,
												ToSystemId,
												FromUserId,
												ToUserId,
												CurrencyCode,
												Amount,
												Description,
												CREATEdBy,
												CREATEdDate, 
												Status,
												Data1)
							Values (p_TransactionID,
									p_TransactionTypeID,
									p_FromClientId,
									p_ToClientId,
									p_FromSystemId,
									p_ToSystemId,
									p_FromUserId,
									p_ToUserId,
									p_CurrencyCode,
									p_Amount,
									p_Description,
									p_CREATEdBy,
									UTC_TIMESTAMP(),
									3,
									p_Data1);
						End if;
				Else
						/*not enough available in address*/
						Set p_TransactionID = '-2';
				End if;
	End If;
			

			

			
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Transaction_Withdraw_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Transaction_Withdraw_Add`;
delimiter ;;
CREATE PROCEDURE `SP_Transaction_Withdraw_Add`(p_TransactionTypeID int,
	p_FromClientId int,
	p_FromUserId varchar(100) /* = NULL */,
	p_FromSystemId int,
	p_Data1 varchar(100),
	p_CurrencyCode int /* = 0 */,
	p_Amount decimal(18, 10),
	p_Description varchar(100) /* = NULL */,
	p_CreatedBy VARCHAR(100),
	p_Data2 varchar(100) /* = NULL */,
	p_Fee decimal(18, 10),
	p_Status int,
	p_TransactionIDCur varchar(100),
	/*p_FeeBlockchain decimal(18, 5), /* = 0 */
	out p_TransactionID varchar(100))
Begin
	Set p_TransactionID = NULL;-- NULL If To User Not Exists In Table [User] And SystemID

	Set @v_IsExistsUser = 0;
	Set @v_DailyWithdrawLimit = -1;-- -1: Not Exists User (@TransactionID = NULL)
	Set @v_WithdrawOnDay = 0;
	Begin
		Select @v_DailyWithdrawLimit := User.DailyWithdrawLimit
		From User
		Where User.UserID =  p_FromUserId
			  And User.SystemID = p_FromSystemId;
	End;

	Begin
		Select @v_IsExistsUser := 1,
					 @v_WithdrawOnDay := Case
																	When (DATE(`User`.WithdrawDate) = DATE(UTC_TIMESTAMP())) then `User`.WithdrawOnDay
																	Else 0
																End
		From `User`
		Where `User`.UserID =  p_FromUserId
			  And `User`.SystemID = p_FromSystemId
			  And (
							`User`.DailyWithdrawLimit > 0
							And (
									`User`.WithdrawDate Is Null
									Or (DATE(`User`.WithdrawDate) != DATE(UTC_TIMESTAMP()))
									Or (
										 DATE(`User`.WithdrawDate) = DATE(UTC_TIMESTAMP())
										 And `User`.WithdrawOnDay < `User`.DailyWithdrawLimit
										 )
								)
				   );
	End;

	IF(@v_IsExistsUser = 1) /* And (p_Fee >= p_FeeBlockchain)*/
		Then
			Set @v_AmountReal = p_Amount;
			
			If(p_Fee >= 0 And p_Status != 5)
				Then
					Set @v_AmountReal = (p_Amount - p_Fee);/*(p_Amount - (p_Fee - p_FeeBlockchain))*/
				End if;
			
			/*Begin
				Select @v_IsEnoughBalance := 1
				From Address
				Where Address.UserID =  p_FromUserId
					And Address.CurrencyCode = p_CurrencyCode
					And Address.SystemID = p_FromSystemId
					And Address.ClientID = p_FromClientId
					And (
								@v_AmountReal >= 0
								And (Address.Available - @v_AmountReal) >= 0
							);
			End;*/
			
			/*0: not enough available, 1: enough available for withdraw*/
			Set @v_IsEnoughBalance = F_TRANSACTION_CHECK_ENOUGHBALANCE_FE(p_FromUserId, 
																																		p_CurrencyCode, 
																																		p_FromSystemId, 
																																		p_FromClientId, 
																																		@v_AmountReal); 
			If(@v_IsEnoughBalance = 1)
				Then
				    /*If this address has enough available when @v_IsEnoughBalance = 1, else: @v_IsEnoughBalance = 0*/
						
						/*Update Address Available and InWithdraw when p_Status = 5: ReadyToProcess*/
						Set @v_RowCount = F_ADDRESS_READY_WITHDRAW_UPDAVAIL_FE(@v_AmountReal, 
																																	 p_CreatedBy, 
																																	 p_FromUserId, 
																																	 p_CurrencyCode, 
																																	 p_FromSystemId, 
																																	 p_FromClientId, p_Status);
						
						/*Update Address
						Set Address.InWithdraw = Address.InWithdraw + @v_AmountReal,
							Address.Available = (Address.Available - @v_AmountReal),
							Address.UpdatedBy = p_CreatedBy,
							Address.UpdatedDate = UTC_TIMESTAMP()
						Where Address.UserID =  p_FromUserId
								And Address.CurrencyCode = p_CurrencyCode
								And Address.SystemID = p_FromSystemId
								And Address.ClientID = p_FromClientId
								And p_Status = 5; 5: ReadyToProcess*/
								
						IF(@v_RowCount > 0 or p_Status != 5) 
							Then
								Set @v_Status = 1;
								If (p_Status = 5) Then 
									Set @v_Status = p_Status;
								End If;
								
								/*Case: Accounting Confirm -> Update Status Transaction Pending -> ReadyToProcess*/
								Update Transaction
								Set Status = @v_Status,
										UpdatedBy = p_CreatedBy,
										UpdatedDate = UTC_TIMESTAMP()
								Where p_TransactionIDCur Is Not null 
								      And TransactionID = p_TransactionIDCur;
								
								If (ROW_COUNT() <= 0) Then
								    Set p_TransactionID = UUID();
								    /*Update transaction fail -> Insert New Transaction*/
										Insert into Transaction(TransactionID,
													TransactionType,
													FromClientId,
													ToClientId,
													FromSystemId,
													ToSystemId,
													FromUserId,
													ToUserId,
													CurrencyCode,
													Amount,
													Description,
													CREATEdBy,
													CREATEdDate, 
													Status,
													Data1,
													Data2,
													Fee)
									Values (p_TransactionID,
											p_TransactionTypeID, 
											p_FromClientId,
											0,
											p_FromSystemId,
											0,
											p_FromUserId,
											NULL,
											p_CurrencyCode,
											@v_AmountReal,
											p_Description,
											p_CreatedBy,
											UTC_TIMESTAMP(),
											@v_Status,
											p_Data1,
											p_Data2,
											p_Fee);
								Else
										/*FOUND_ROWS() > 0: Update Success*/
										Set p_TransactionID = p_TransactionIDCur;
								End If;
								
							End if;
							
						IF(ROW_COUNT() > 0 and p_Status = 5)
							Then
							    Set @v_WithdrawOnday = F_USER_WITHDRAWONDAY_UPD_FE((@v_WithdrawOnDay + 1),
																														p_CreatedBy, p_FromUserId, p_FromSystemId, p_FromClientId);
									/*Update `User`
									Set `User`.WithdrawOnDay = @v_WithdrawOnDay + 1,
										`User`.WithdrawDate = UTC_TIMESTAMP(),
										`User`.UpdatedBy = p_CreatedBy,
										`User`.UpdatedDate = UTC_TIMESTAMP()
									Where `User`.UserID =  p_FromUserId
											And `User`.SystemID = p_FromSystemId
											And `User`.ClientID = p_FromClientId;*/
							End if;
				Else
						Set p_TransactionID = '-2'; /*not enough balance available*/
				End If;
	Else 
			If(@v_DailyWithdrawLimit >= 0)
				Then
					Set p_TransactionID = '-1';
				End if;
		End if;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Transaction_Withdraw_Upd
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Transaction_Withdraw_Upd`;
delimiter ;;
CREATE PROCEDURE `SP_Transaction_Withdraw_Upd`(p_TransactionID varchar(100),
	p_ClientId int,
	p_SystemId int,
	p_Status int, 
	p_Confirmation bigint,
	p_UpdatedBy varchar(100),
	p_TxId longtext,
	p_FeeBlockChain decimal(18, 10))
Begin
		Update Address
		Set Address.InWithdraw = Address.InWithdraw + p_FeeBlockChain,
				Address.Available = Case
															When (Address.Available - p_FeeBlockChain) >= 0 then (Address.Available - p_FeeBlockChain)
															Else 0
														End
		Where Exists(
									Select 1
									From Transaction
									Where Transaction.TransactionID = p_TransactionID
										And Transaction.FromClientID = p_ClientId
										And Transaction.FromSystemID = p_SystemId
										And Transaction.FromUserID = Address.UserID
										And Transaction.CurrencyCode = Address.CurrencyCode
										And Transaction.TransactionType = 5 -- Withdraw: 5
								)
					And Address.ClientID = p_ClientId
					And Address.SystemID = p_SystemId;
		
		IF(ROW_COUNT() > 0)
			Then
					Update Transaction
					Set Transaction.Status = p_Status,
						Transaction.Confirmation = p_Confirmation,
							Transaction.UpdatedBy = p_UpdatedBy,
						Transaction.UpdatedDate = UTC_TIMESTAMP(),
						Transaction.Data2 = p_TxId,
						Transaction.FeeBlockChain = p_FeeBlockChain
						/*Transaction.Amount = Transaction.Amount  + p_FeeBlockChain*/
					Where Transaction.TransactionID = p_TransactionID
								And Transaction.FromClientID = p_ClientId
							And Transaction.FromSystemID = p_SystemId
							And Transaction.Status != 3; -- Status not be successfully;

					If(ROW_COUNT() > 0 And p_Status = 3)
						Then
							/*Update success and TransactionStatus = 3: Successfully*/
							Set @v_Amount = 0;
							Set @v_FromUserId = NULL;
							Set @v_CurrencyCode = 0;

							Begin
								Select @v_Amount := Transaction.Amount,
										 @v_FromUserId := Transaction.FromUserID,
										 @v_CurrencyCode := Transaction.CurrencyCode
								From Transaction
								Where Transaction.TransactionID = p_TransactionID
										And Transaction.FromClientID = p_ClientId
										And Transaction.FromSystemID = p_SystemId
										And Transaction.TransactionType = 5; -- Withdraw: 5
							End;

							Update Address
							Set Address.InWithdraw = Case
																				When (Address.InWithdraw - @v_Amount) >= 0 then (Address.InWithdraw - @v_Amount)
																				Else 0
																			 End,
								Address.Total = Case
																	When (Address.Total - @v_Amount) >= 0 then (Address.Total - @v_Amount)
																	Else 0
																End
							Where Address.UserID = @v_FromUserId
										And Address.CurrencyCode = @v_CurrencyCode
									And Address.ClientID = p_ClientId
									And Address.SystemID = p_SystemId;
						End if;
			End If;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_UserEvents_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_UserEvents_Add`;
delimiter ;;
CREATE PROCEDURE `SP_UserEvents_Add`(p_UserID varchar(100),
	p_ClientID int,
	p_SystemID int,
	p_EventData longtext,
	p_EventData1 longtext,
	p_EventDescription longtext /* = NULL */,
	p_Status int)
Begin
	 DECLARE ISERROR INT;
	 Declare v_CREATEdDate DateTime;
	 
	 DECLARE _ROLLBACK INT DEFAULT 0;
	 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET _ROLLBACK = 1;
	 START TRANSACTION;

		
		Set v_CREATEdDate = UTC_TIMESTAMP();

		IF(p_Status = 2 Or p_Status = 4) 
			Then
				-- @Status Lock: 2: Locked, 4: TemporaryLock
				Insert into UserEvents(EventData,
										EventData1,
										EventType,
										EventDescription,
										UserId,
										ClientId,
										SystemId,
										CREATEdDate)
				Values (p_EventData,
						p_EventData1,
						2,
						p_EventDescription,
						p_UserID,
						p_ClientID,
						p_SystemID,
						v_CREATEdDate);
		Else
				-- @Status Unlock: 1: Active, 3: TemporaryUnlock
				Insert into UserEvents(EventData,
										EventData2,
										EventType,
										EventDescription,
										UserId,
										ClientId,
										SystemId,
										CREATEdDate)
				Values (p_EventData,
						p_EventData1,
						3,
						p_EventDescription,
						p_UserID,
						p_ClientID,
						p_SystemID,
						v_CREATEdDate);
			End if;

		Set @p_EventID = LAST_INSERT_ID();
		
		-- if insert success -> update status in table Wallet
		Update User
		set User.Status = p_Status,
			User.UpdatedBy = p_EventData1,
			User.UpdatedDate = v_CREATEdDate
		Where User.UserID =  p_UserID
		      AND User.SystemID = p_SystemID;

	SET ISERROR = _ROLLBACK;
	IF
		ISERROR = 1 THEN
		Set @p_EventID = 0;
		ROLLBACK;
	ELSE COMMIT;
	END IF;
	
	Select @p_EventID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_UserEvents_Sel
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_UserEvents_Sel`;
delimiter ;;
CREATE PROCEDURE `SP_UserEvents_Sel`(p_intEventID int)
Begin
	Select UserEvents.EventId,
	       UserEvents.UserId,
		   UserEvents.ClientId,
		   UserEvents.SystemId,
		   UserEvents.EventData,
		   UserEvents.EventData1,
		   UserEvents.EventData2,
		   UserEvents.EventDescription,
		   UserEvents.EventSource,
		   UserEvents.EventType,
		   UserEvents.CREATEdDate
	From UserEvents
	Where UserEvents.EventId = p_intEventID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_UserEvents_SRH
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_UserEvents_SRH`;
delimiter ;;
CREATE PROCEDURE `SP_UserEvents_SRH`(p_UserID varchar(100) /* = Null */,
	p_ClientID int,
	p_SystemID int,
	p_FromIndex int /* = 0 */,
	p_Limit int)
Begin
  SET @v_Row = 0;
	
 	Select US.EventId,
	       US.UserId,
		   US.ClientId,
		   US.SystemId,
		   US.EventData,
		   US.EventData1,
		   US.EventData2,
		   US.EventDescription,
		   US.EventSource,
		   US.EventType,
		   US.CREATEdDate,
		   US.RowNum,
			US.TotalRecord
	From (
			Select UserEvents.EventId,
				   UserEvents.UserId,
				   UserEvents.ClientId,
				   UserEvents.SystemId,
				   UserEvents.EventData,
				   UserEvents.EventData1,
				   UserEvents.EventData2,
				   UserEvents.EventDescription,
				   UserEvents.EventSource,
				   UserEvents.EventType,
				   UserEvents.CREATEdDate,
				   (@v_Row := @v_Row + 1) AS RowNum,
				   (
							Select Count(*)
							From UserEvents
							Where (
											p_UserID = '-1'
											Or p_UserID = ''
											Or p_UserID IS NULL
											Or UserEvents.UserId = p_UserID
										)
										And UserEvents.ClientId = p_ClientID
										And UserEvents.SystemId = p_SystemID
					 ) TotalRecord
			From UserEvents
			Where (
							p_UserID = '-1'
							Or p_UserID = ''
							Or p_UserID IS NULL
							Or UserEvents.UserId = p_UserID
						)
						And UserEvents.ClientId = p_ClientID
						And UserEvents.SystemId = p_SystemID
		) US
	Where US.RowNum > (p_FromIndex * p_Limit)
	      And US.RowNum <= ((p_FromIndex + 1) * p_Limit);
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_User_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_User_Add`;
delimiter ;;
CREATE PROCEDURE `SP_User_Add`(p_ClientID int,
	p_SystemID int,
	p_WalletID int,
	p_UserID varchar(100),
	p_Name nvarchar(200) /* = NULL */,
	p_Email varchar(100) /* = NULL */,
	p_PhoneNumber varchar(20) /* = NULL */,
	p_RequestedBy nvarchar(100),
	p_DailyWithdrawLimit int /* = 0 */,
	out p_intIsExists smallint)
Begin
	Set p_intIsExists = 0;
	Begin
		Select 1 Into p_intIsExists
		From User
		Where User.UserID = p_UserID
		      And User.SystemID = p_SystemID;
	End;

	If(p_intIsExists = 0)
		Then
			Insert into User(UserId,
					SystemId,
					ClientId,
					WalletId,
					Name,
					Email,
					PhoneNumber,
					DailyWithdrawLimit,
					CreatedBy,
					CreatedDate)
			Values (p_UserID,
					p_SystemID,
					p_ClientID,
					p_WalletID,
					p_Name,
					p_Email,
					p_PhoneNumber,
					p_DailyWithdrawLimit,
					p_RequestedBy,
					UTC_TIMESTAMP());
		End if;
	

End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_User_Sel
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_User_Sel`;
delimiter ;;
CREATE PROCEDURE `SP_User_Sel`(p_ClientID int,
	p_SystemID int,
	p_UserID varchar(100))
Begin
	Select User.UserID,
		   User.SystemID,
		   User.ClientID,
		   User.WalletID,
		   User.Name,
		   User.Email,
		   User.PhoneNumber,
		   User.Status,
		   User.DailyWithdrawLimit
	From User
	Where User.UserID =  p_UserID
	      And User.SystemID = p_SystemID
		  And User.ClientID = p_ClientID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_User_SRH
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_User_SRH`;
delimiter ;;
CREATE PROCEDURE `SP_User_SRH`(p_ClientID int,
	p_SystemID int,
	p_FromIndex int /* = 0 */,
	p_Limit int)
Begin
  SET @v_Row = 0;
	
	Select US.UserId,
		   US.SystemId,
		   US.ClientId,
		   US.WalletId,
		   US.Name,
		   US.Email,
		   US.PhoneNumber,
		   US.Status,
		   US.RowNum,
		   US.TotalRecord
	From (
					Select User.UserID,
							 User.SystemID,
							 User.ClientID,
							 User.WalletID,
							 User.Name,
							 User.Email,
							 User.PhoneNumber,
							 User.Status,
							 (@v_Row := @v_Row + 1) AS RowNum,
							 (
								Select Count(*)
								From User
								Where User.SystemID = p_SystemID
										And User.ClientID = p_ClientID
							 ) TotalRecord
					From User
					Where User.SystemID = p_SystemID
							And User.ClientID = p_ClientID
		) US
	Where US.RowNum > (p_FromIndex * p_Limit)
	      And US.RowNum <= ((p_FromIndex + 1) * p_Limit);
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_User_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_User_UPD`;
delimiter ;;
CREATE PROCEDURE `SP_User_UPD`(p_ClientID int,
	p_SystemID int,
	p_WalletID int,
	p_UserID varchar(100),
	p_Name varchar(200) /* = NULL */,
	p_Email varchar(100) /* = NULL */,
	p_PhoneNumber varchar(20) /* = NULL */,
	p_DailyWithdrawLimit int /* = 0 */,
	p_RequestedBy varchar(100))
Begin
	Update User
	Set User.ClientID = p_ClientID,
	    User.WalletID = p_WalletID,
		User.Name = p_Name,
		User.Email = p_Email,
		User.PhoneNumber = p_PhoneNumber,
		User.DailyWithdrawLimit = p_DailyWithdrawLimit,
		User.UpdatedBy = p_RequestedBy,
		User.UpdatedDate = UTC_TIMESTAMP()
	Where User.UserID =  p_UserID
	      And User.SystemID = p_SystemID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_WalletEvents_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_WalletEvents_Add`;
delimiter ;;
CREATE PROCEDURE `SP_WalletEvents_Add`(p_WalletID int,
	p_ClientID int,
	p_SystemID int,
	p_EventData longtext /* = NULL */,
	p_EventData1 longtext /* = NULL */,
	p_EventDescription longtext /* = NULL */,
	p_Status int)
Begin
	Declare v_CreatedDate datetime;
	  DECLARE ISERROR INT;
	 
	 DECLARE _ROLLBACK INT DEFAULT 0;
	 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET _ROLLBACK = 1;
	 START TRANSACTION; 
	 
		Set v_CREATEdDate = UTC_TIMESTAMP();

		IF(p_Status = 2 Or p_Status = 4) 
			Then
				-- @Status Lock: 2: Locked, 4: TemporaryLock
				Insert into WalletEvents(EventData,
										EventData1,
										EventType,
										EventDescription,
										WalletId,
										ClientId,
										SystemId,
										CREATEdDate)
				Values (p_EventData,
						p_EventData1,
						2,
						p_EventDescription,
						p_WalletID,
						p_ClientID,
						p_SystemID,
						v_CREATEdDate);
		Else
				-- @Status Unlock: 1: Active, 3: TemporaryUnlock
				Insert into WalletEvents(EventData,
										EventData2,
										EventType,
										EventDescription,
										WalletId,
										ClientId,
										SystemId,
										CREATEdDate)
				Values (p_EventData,
						p_EventData1,
						3,
						p_EventDescription,
						p_WalletID,
						p_ClientID,
						p_SystemID,
						v_CREATEdDate);
			End if;

		Set @p_EventID = LAST_INSERT_ID();

		-- if insert success -> update status in table Wallet
		Update Wallet
		set Wallet.Status = p_Status,
			Wallet.UpdatedBy = p_EventData1,
			Wallet.UpdatedDate = v_CREATEdDate
		Where Wallet.WalletId = p_WalletID;

	SET ISERROR = _ROLLBACK;
	IF
		ISERROR = 1 THEN
		Set @p_EventID = 0;
		ROLLBACK;
	ELSE COMMIT;
	END IF;
	
	Select @p_EventID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_WalletEvents_Sel
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_WalletEvents_Sel`;
delimiter ;;
CREATE PROCEDURE `SP_WalletEvents_Sel`(p_intEventID int)
Begin
	Select WalletEvents.EventId,
	       WalletEvents.WalletId,
		   WalletEvents.ClientId,
		   WalletEvents.SystemId,
		   WalletEvents.EventData,
		   WalletEvents.EventData1,
		   WalletEvents.EventData2,
		   WalletEvents.EventDescription,
		   WalletEvents.EventSource,
		   WalletEvents.EventType,
		   WalletEvents.CREATEdDate
	From WalletEvents
	Where WalletEvents.EventId = p_intEventID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_WalletEvents_SRH
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_WalletEvents_SRH`;
delimiter ;;
CREATE PROCEDURE `SP_WalletEvents_SRH`(p_WalletID int,
	p_ClientID int,
	p_SystemID int,
	p_FromIndex int /* = 0 */,
	p_Limit int)
Begin
  SET @v_Row = 0;
	
	Select  WL.EventId,
			WL.WalletId,
			WL.ClientId,
			WL.SystemId,
			WL.EventData,
			WL.EventData1,
			WL.EventData2,
			WL.EventDescription,
			WL.EventSource,
			WL.EventType,
			WL.CreatedDate,
			WL.RowNum,
			WL.TotalRecord
	From (
			Select WalletEvents.EventId,
				   WalletEvents.WalletId,
				   WalletEvents.ClientId,
				   WalletEvents.SystemId,
				   WalletEvents.EventData,
				   WalletEvents.EventData1,
				   WalletEvents.EventData2,
				   WalletEvents.EventDescription,
				   WalletEvents.EventSource,
				   WalletEvents.EventType,
				   WalletEvents.CreatedDate,
				   (@v_Row := @v_Row + 1) AS RowNum,
					 (
							Select Count(*)
							From WalletEvents
							Where (
											p_WalletID <= 0 
											Or WalletEvents.WalletId = p_WalletID
										)
										And WalletEvents.ClientId = p_ClientID
										And WalletEvents.SystemId = p_SystemID
					 ) As TotalRecord
			From WalletEvents
			Where (
							p_WalletID <= 0 
							Or WalletEvents.WalletId = p_WalletID
						)
						And WalletEvents.ClientId = p_ClientID
						And WalletEvents.SystemId = p_SystemID
		) WL
	Where WL.RowNum > (p_FromIndex * p_Limit)
	      And WL.RowNum <= ((p_FromIndex + 1) * p_Limit);
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Wallet_Add
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Wallet_Add`;
delimiter ;;
CREATE PROCEDURE `SP_Wallet_Add`(p_ClientID int,
	p_SystemID int,
	p_RequestedBy varchar(100),
	p_ProviderCode varchar(100),
	p_DailyWithdrawLimit int,
	out p_CREATEdDate DateTime,
	out p_WalletID int)
Begin
    Set p_CREATEdDate = UTC_TIMESTAMP();

	Insert into Wallet(ClientId, 
					   SystemId, 
					   CREATEdDate, 
					   CREATEdBy,
					   ProviderCode,
						 DailyWithdrawLimit)
	Values (p_ClientID,
			p_SystemID,
			p_CREATEdDate,
			p_RequestedBy,
			p_ProviderCode,
			p_DailyWithdrawLimit);

   Set p_WalletID = LAST_INSERT_ID();
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Wallet_Sel
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Wallet_Sel`;
delimiter ;;
CREATE PROCEDURE `SP_Wallet_Sel`(p_ClientID int,
	p_SystemID int,
	p_WalletId int)
Begin
	Select Wallet.WalletId,
	       Wallet.ClientId,
		   Wallet.SystemId,
		   Wallet.Status,
		   Wallet.CREATEdDate,
		   Wallet.ProviderCode,
		   Wallet.DailyWithdrawLimit,
		   Wallet.CREATEdBy,
		   Wallet.UpdatedBy,
		   Wallet.UpdatedDate
	From Wallet
	Where Wallet.WalletId = p_WalletId
	      And Wallet.ClientId = p_ClientID
		  And Wallet.SystemId = p_SystemID;
End
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SP_Wallet_SRH
-- ----------------------------
DROP PROCEDURE IF EXISTS `SP_Wallet_SRH`;
delimiter ;;
CREATE PROCEDURE `SP_Wallet_SRH`(p_ClientID int,
	p_SystemID int,
	p_FromIndex int /* = 0 */,
	p_Limit int)
Begin
  SET @v_Row = 0;
	
	Select WL.WalletId,
			WL.ClientId,
			WL.SystemId,
			WL.Status,
			WL.CREATEdDate,
			WL.ProviderCode,
			WL.CREATEdBy,
			WL.UpdatedBy,
			WL.UpdatedDate,
			WL.RowNum,
			WL.TotalRecord
	From (
			Select Wallet.WalletId,
				   Wallet.ClientId,
				   Wallet.SystemId,
				   Wallet.Status,
				   Wallet.CREATEdDate,
				   Wallet.ProviderCode,
				   Wallet.CREATEdBy,
				   Wallet.UpdatedBy,
				   Wallet.UpdatedDate,
				   (@v_Row := @v_Row + 1) AS RowNum,
					 (
							Select Count(*)
								From Wallet
								Where Wallet.ClientId = p_ClientID
										And Wallet.SystemId = p_SystemID
					 ) As TotalRecord
			From Wallet
			Where Wallet.ClientId = p_ClientID
				  And Wallet.SystemId = p_SystemID
		) WL
	Where WL.RowNum > (p_FromIndex * p_Limit)
	      And WL.RowNum <= ((p_FromIndex + 1) * p_Limit);
End
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
