USE [LotteryCLP]
GO
/****** Object:  User [dev]    Script Date: 7/27/2018 11:02:14 AM ******/
CREATE USER [dev] FOR LOGIN [dev] WITH DEFAULT_SCHEMA=[dev]
GO
/****** Object:  Schema [dev]    Script Date: 7/27/2018 11:02:14 AM ******/
CREATE SCHEMA [dev]
GO
/****** Object:  StoredProcedure [dbo].[Admin_Sel]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Admin_Sel]   
    @UserName nvarchar(50),   
    @PassWord nvarchar(50)   
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Admin  
    WHERE UserName = @UserName AND Password = @PassWord  ;

GO
/****** Object:  StoredProcedure [dbo].[Award_Insert]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Award_Insert]   
    @AwardName nvarchar(50) = null,
    @AwardValue float,
    @IsActive int,
    @IsDelete int,
    @Priority int
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Award(AwardName,AwardValue,IsActive,IsDelete,Priority) values(@AwardName,@AwardValue,@IsActive,@IsDelete,@Priority) 
    
END

GO
/****** Object:  StoredProcedure [dbo].[AwardNumber_Insert]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AwardNumber_Insert]   
    @AwardID int,
    @CreateDate datetime,
    @IsActive int,
    @IsDelete int,
    @NumberValue nvarchar(50) = null,
    @Priority int
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into AwardNumber(AwardID,CreateDate,IsActive,IsDelete,NumberValue,Priority) values(@AwardID,@CreateDate,@IsActive,@IsDelete,@NumberValue,@Priority) 
END

GO
/****** Object:  StoredProcedure [dbo].[AwardNumber_Sel]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AwardNumber_Sel]   
    @fromDate datetime,
    @toDate datetime
AS
BEGIN 
     SET NOCOUNT ON 
		SELECT ROW_NUMBER() OVER (ORDER BY awn.Priority ASC) as Row,awn.*,aw.AwardName,aw.AwardValue from awardnumber awn left join award aw on awn.AwardID=aw.AwardID where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0
END

GO
/****** Object:  StoredProcedure [dbo].[Booking_Insert]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Booking_Insert]   
    @MemberID int,
    @NumberValue nvarchar(50) = null,
    @Quantity int,
    @Status int,
    @OpenDate datetime,
    @CreateDate datetime,
    @TransactionCode nvarchar(50)
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Booking(MemberID,NumberValue,Quantity,Status,OpenDate,CreateDate,TransactionCode) values(@MemberID,@NumberValue,@Quantity,@Status,@OpenDate, @CreateDate, @TransactionCode) 
    update Member set Points = ((select Points from Member where MemberID = @MemberID) - @Quantity) where MemberID = @MemberID
END

GO
/****** Object:  StoredProcedure [dbo].[Booking_Sel]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Booking_Sel]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select CreateDate, BookingID, MemberID, NumberValue, OpenDate, Quantity, Status,TransactionCode from Booking where MemberID = @MemberID order by CreateDate desc
END

GO
/****** Object:  StoredProcedure [dbo].[CheckEmailExists]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[CheckEmailExists]
(
	@email nvarchar(250)
)
 as begin
		select Email from member where Email=@email
 end

GO
/****** Object:  StoredProcedure [dbo].[CountAllBooking]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllBooking]
as
begin
select COUNT(b.BookingID) as TotalAllBooking,
dbo.CountNewBooking() as TotalNewBooking
 from Booking b;
end;

GO
/****** Object:  StoredProcedure [dbo].[CountAllCoin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[CountAllCoin]
as
begin
select COUNT(c.TransactionID) as TotalAllCoin,
dbo.CountNewCoin() as TotalNewCoin
 from TransactionCoin c;
end;

GO
/****** Object:  StoredProcedure [dbo].[CountAllMember]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllMember]
as
begin
select COUNT(m.MemberID) as TotalAllMember,
dbo.CountNewMember() as TotalNewMember
 from Member m
where m.IsActive=1 
and m.IsDelete=0;
end;

GO
/****** Object:  StoredProcedure [dbo].[CountAllPackage]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllPackage]
as
begin
select COUNT(p.TransactionID) as TotalAllPackage,
dbo.CountNewPackage() as TotalNewPackage
 from TransactionPackage p;
end;

GO
/****** Object:  StoredProcedure [dbo].[CountAllPoints]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllPoints]
as
begin
select COUNT(p.TransactionID) as TotalAllPoints,
dbo.CountNewPoint() as TotalNewPoints
 from TransactionPoint p;
end;

GO
/****** Object:  StoredProcedure [dbo].[Member_Check]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_Check]   
    @Email nvarchar(50),
    @E_Wallet nvarchar(50)
AS   
    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE E_Wallet = @E_Wallet or Email = @Email;

GO
/****** Object:  StoredProcedure [dbo].[Member_E_Wallet_Check]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Member_E_Wallet_Check]   
    @UserName nvarchar(50)
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE E_Wallet = @UserName ;

GO
/****** Object:  StoredProcedure [dbo].[Member_E_Wallet_Sel]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_E_Wallet_Sel]   
    @UserName nvarchar(50),   
    @PassWord nvarchar(50)   
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE E_Wallet = @UserName AND Password = @PassWord and IsActive = 1 and IsDelete = 0 ;

GO
/****** Object:  StoredProcedure [dbo].[Member_Email_Check]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Member_Email_Check]   
    @UserName nvarchar(50)
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE Email = @UserName ;

GO
/****** Object:  StoredProcedure [dbo].[Member_Email_Sel]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_Email_Sel]   
    @UserName nvarchar(50),   
    @PassWord nvarchar(50)   
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE Password = @PassWord and IsActive = 1 and IsDelete = 0 and Email = @UserName or E_Wallet = @UserName;

GO
/****** Object:  StoredProcedure [dbo].[Member_Insert]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_Insert]   
    @Email nvarchar(50) = null,
    @E_Wallet nvarchar(50) = null,
    @Password nvarchar(50) = null,
    @IsActive int,
    @IsDelete int,
    @Points float,
    @CreateDate datetime
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Member(Email,E_Wallet,Password,IsActive,IsDelete,Points,CreateDate) values(@Email,@E_Wallet,@Password,@IsActive,@IsDelete,@Points,@CreateDate) 
    
END

GO
/****** Object:  StoredProcedure [dbo].[Member_Update]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Member_Update] 
	@MemberID int,  
    @Email nvarchar(50) = null,
    @E_Wallet nvarchar(50) = null,
    @Password nvarchar(50) = null,
    @IsActive int,
    @IsDelete int,
    @Points float
AS 
BEGIN 
     SET NOCOUNT ON 
    update Member
	set Email = @Email,
	E_Wallet = @E_Wallet,
	Password = @Password,
	IsActive = @IsActive,
    IsDelete = @IsDelete,
    Points = @Points
    where MemberID = @MemberID
END

GO
/****** Object:  StoredProcedure [dbo].[Package_Insert]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Package_Insert]   
    @PackageName nvarchar(50) = null,
    @PackageValue float,
    @CreateDate datetime,
    @IsActive int,
    @Priority int,
    @NumberExpire int
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Package(PackageName,PackageValue,CreateDate,IsActive,Priority,NumberExpire) values(@PackageName,@PackageValue,@CreateDate,@IsActive,@Priority,@NumberExpire) 
END

GO
/****** Object:  StoredProcedure [dbo].[Package_Sel_All]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Package_Sel_All]   
AS   
    SET NOCOUNT ON;  
    SELECT * 
    FROM Package  
    WHERE IsActive = 1 
    order by Priority asc ;

GO
/****** Object:  StoredProcedure [dbo].[SP_AWARDMEGABALL_SELRESULT]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_AWARDMEGABALL_SELRESULT]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 15.01.2018
	DESCRIPTION: LOAD THÔNG TIN KẾT QUẢ XỔ SỐ GẦN NHẤT
*/
AS BEGIN 
	SELECT AWN.*,
		   AW.AWARDNAME,
		   AW.AWARDVALUE 
	FROM AWARDMEGABALL AWN 
	LEFT JOIN AWARD AW 
			ON AWN.AWARDID = AW.AWARDID 
	WHERE AWN.ISDELETE = 0
	      --AND AWN.ISACTIVE = 1
    ORDER BY AWN.CreateDate DESC;
END

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckAwardNameEnglish]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckAwardNameEnglish]
 (
	@awardNameEnglish nvarchar(250),
	@AwardID int
 )
 as begin 
		select * 
		from Award 
		where AwardNameEnglish=@awardNameEnglish and IsActive=1 and IsDelete=0 and AwardID!=@AwardID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckCoinIDExist]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_CheckCoinIDExist]
(
	@coinID nchar(10),
	@exchangeID int
)
as begin 
	select * from ExchangeTicket where CoinID=@coinID and (@exchangeID=0 or ExchangeID!=@exchangeID) and IsActive =1 and IsDelete =0
end

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckE_WalletExistsFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckE_WalletExistsFE]
	(
@e_wallet nvarchar(250)
)
as begin 
	select E_Wallet from member where E_Wallet=@e_wallet
end

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckEmailAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckEmailAdmin]
  (
	@email nvarchar(50)
 )
 as begin 
		select Email from Admin where Email=@email
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckEmailExistsFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckEmailExistsFE]
(
@email nvarchar(250)
)
as begin 
	select Email from member where Email=@email
end

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckExistTransactionBitcoinFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckExistTransactionBitcoinFE]
(
	@TransactionBitcoin nvarchar(500),
	@MemberID int
)
as begin
	select * 
	from TransactionCoin 
	where TransactionBitcoin = @TransactionBitcoin
	      and MemberID = @MemberID;
end

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckExpirePackageFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckExpirePackageFE]
(
@memberID int,
@dateNow datetime
)
as begin 
	select * from TransactionPackage p where MemberID=@memberID and Status=1 and p.ExpireDate>=@dateNow
end

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckGroupAdminName]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckGroupAdminName] 
 (
	@groupName  nvarchar(250)
 )
 as begin 
	select GroupName from GroupAdmin where GroupName = @groupName 
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckIPConfig_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SP_CheckIPConfig_CMS]
(
	@IPAddress nvarchar(50),
	@ServiceDomain nvarchar(250),
	@UserName nvarchar(250)
	
)
as
begin
select * from IPConfig ip, [Admin] m  
where 
--ip.IPAddress=@IPAddress
--and 
ip.ServiceDomain=@ServiceDomain
and ip.IsActive=1
and ip.IsDeleted=0
and m.UserName=@UserName 

and m.IsActive=1 
and m.IsDelete=0;

end;

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckIPConfig_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_CheckIPConfig_FE]
(
	@IPAddress nvarchar(50),
	@ServiceDomain nvarchar(250),
	@UserName nvarchar(250)
	
)
as
begin
select * from IPConfig ip, Member m  
where 
--ip.IPAddress=@IPAddress
--and 
ip.ServiceDomain=@ServiceDomain
and ip.IsActive=1
and ip.IsDeleted=0
and m.Email=@UserName 

and m.IsActive=1 
and m.IsDelete=0;

end;

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckNumberExit]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckNumberExit]
 (
	@num1 int,
	@num2 int,
	@num3 int,
	@num4 int,
	@num5 int,
	@extranum int
 )
 as begin
	select FirstNumber, SecondNumber, ThirdNumber, FourthNumber, FivethNumber, ExtraNumber from AwardMegaball
	where FirstNumber=@num1 or SecondNumber=@num2 or ThirdNumber=@num3 or FourthNumber=@num4 or FivethNumber=@num5 or ExtraNumber=@extranum
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckOpenDateAwardMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckOpenDateAwardMegaball]
 (
	@createDate datetime
 )
 as begin
		select * from AwardMegaball where convert(nvarchar(250),CreateDate,103) = CONVERT(nvarchar(250),@createDate,103) 
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_CheckUserNameAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckUserNameAdmin]
   (
	@userName nvarchar(50)
 )
 as begin 
		select UserName from Admin where UserName=@userName
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_CountAllAwardMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_CountAllAwardMegaball]
as begin 
	select COUNT(aw.AwardID) as TotalAllAward,
	guest.fc_CountNewAward() as TotalNewAward 
	from AwardMegaball aw
	where aw.IsActive=1 and aw.IsDelete=0;
end

GO
/****** Object:  StoredProcedure [dbo].[SP_CountAllBookingMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountAllBookingMegaball]
as begin
	select COUNT(b.BookingID) as TotalAllBookingMegaball,
	dbo.CountNewBookingMegaball() as TotalNewBookingMegaball
	from BookingMegaball b;
end

GO
/****** Object:  StoredProcedure [dbo].[SP_CountMember]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountMember](@curdate datetime)
  as begin 
  select count(MemberID) as TotalCount from BookingMegaball where CreateDate=@curdate
  end

GO
/****** Object:  StoredProcedure [dbo].[SP_CountMemberBuyNumber_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountMemberBuyNumber_FE]
(
	@fromDate datetime,
	@toDate datetime,
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fiveNumber int,
	@memberID int


)
as begin 
	select count(BookingID) as TotalRecord from bookingmegaball 
	where CreateDate between @fromDate and @toDate and FirstNumber=@firstNumber and SecondNumber=@secondNumber and ThirdNumber=@thirdNumber and FourthNumber=@fourthNumber and FivethNumber=@fiveNumber and MemberID not in(@memberID)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_CountMemberBuyNumberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountMemberBuyNumberFE]
(
	@fromDate datetime,
	@toDate datetime,
	@numberValue int,
	@memberID int
)
as begin 
	select count(BookingID) as TotalRecord from booking where CreateDate between @fromDate and @toDate and NumberValue=@numberValue and MemberID not in(@memberID)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteAccessRight]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_DeleteAccessRight]
(@adminID int)
as begin
		delete from accessright where AdminID=@adminID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteTypeNews_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_DeleteTypeNews_CMS]
(
	@IsDelete smallint,
	@DeleteDate datetime,
	@DeleteUser nvarchar(250),
	@TypeNewsID int
)
as
begin
update TypeNews
set
	IsDelete=@IsDelete,
	
	DeleteDate=@DeleteDate,
	DeleteUser=@DeleteUser
	where TypeNewsID=@TypeNewsID;

end;

GO
/****** Object:  StoredProcedure [dbo].[SP_GerAdminDetail]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GerAdminDetail](@userName nvarchar(50))
as begin
		select * from admin where UserName=@userName
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GerAdminDetailByID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GerAdminDetailByID](@adminID nvarchar(50))
as begin
		select * from admin where AdminID=@adminID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAwardFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_GetAllAwardFE]

as begin 
	select *from award where IsActive = 1 and IsDelete = 0
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardMagaballByID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_GetAwardMagaballByID]
(
	@NumberID int
)
as begin 
	select *from AwardMegaball where NumberID = @NumberID and IsActive = 1 and IsDelete = 0
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardMegabalByDrawID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetAwardMegabalByDrawID]
  (
	@DrawID int
 )
 as begin
		
		select * from AwardMegaball where DrawID=@DrawID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardWithdrawDetail_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_GetAwardWithdrawDetail_CMS]
(
	@TransactionID nvarchar(250)
)
as
begin
select * from AwardWithdraw
where TransactionID=@TransactionID
and IsDeleted=0;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardWithdrawDetail_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GetAwardWithdrawDetail_FE]
(
	@RequestMemberID nvarchar(250),
	@AwardDate datetime,
	@BookingID int
)
as
begin
select * from AwardWithdraw
where convert(varchar(10), AwardDate, 101)=convert(varchar(10), @AwardDate, 101)
and RequestMemberID=@RequestMemberID
and IsDeleted=0
and (@BookingID=-1 or BookingID=@BookingID);
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_GetCompanyByMemberID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetCompanyByMemberID]
(
	@memberID int
)
as begin 
	select * from CompanyInfomation where MemberID=@memberID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetDetailLinkResetPasswordFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GetDetailLinkResetPasswordFE]
(
	@LinkReset nvarchar(1000)
)
as
begin
	SELECT *
	FROM LinkResetPassword
	WHERE LinkReset = @LinkReset and status = 0 and ExpireLink >= SYSDATETIME();
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_GetDrawIDByDate]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetDrawIDByDate]
(
	@date datetime
)
as begin 
	select * from draw where @date >= startdate and @date <= enddate order by createddate desc
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetFreeTicketDailyActive]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetFreeTicketDailyActive]
as begin
		select * from FreeTicketDaily where IsActive=1
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetGroupAdminDetail]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetGroupAdminDetail]
(@groupID int)
as begin
		select *from groupadmin where GroupID=@groupID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetGroupAdminDetails]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetGroupAdminDetails]
(@groupID int)
as begin
		select *from groupadmin where GroupID=@groupID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAward]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAward]
 (
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY AwardID DESC)
		 as Row,sum(1) over() as TOTALROWS,
		  aw.* from award aw) as Products  where Row>=@start and Row<=@end
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardByDate]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardByDate]
(
	@date datetime
 )
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.Createdate desc) as Row,awn.*,aw.AwardName,aw.AwardValue 
	from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID 
	 where awn.IsActive=1 and awn.IsDelete=0 and Convert(varchar,awn.CreateDate,103) = CONVERT(varchar,@date, 103) ) as Products
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardByDateFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardByDateFE]
(
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int

)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.Priority ASC) as Row,awn.*,aw.AwardName,aw.AwardValue from awardnumber awn left join award aw on awn.AwardID=aw.AwardID 
	where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0 ) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardByDayFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardByDayFE]
(
	@fromDate datetime,
	@toDate datetime

)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.Priority ASC) as Row,awn.*,aw.AwardName,aw.AwardValue from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID
	 where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0 ) as Products
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardFE]
(
@start int,
@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY AwardID DESC) as Row,aw.* from award aw) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMagaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardMagaball]
  (
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY NumberID DESC)as row, sum(1) over() as TOTALROWS
		 ,aw.*,a.AwardNameEnglish from awardmegaball aw left join  Award a on aw.AwardID=a.AwardID where aw.IsActive=1 and aw.IsDelete=0) as Products  where Row>=@start and Row<=@end 
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMegaballFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardMegaballFE]
(
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.NumberID desc) as Row, sum(1) over() as TOTALROWS,awn.*,aw.AwardName,aw.AwardValue from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID 
	where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0 ) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMegaballResultByDate]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardMegaballResultByDate]
 (
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,
		aw.*,a.AwardName,a.AwardNameEnglish from awardmegaball aw left join award a on aw.AwardID=a.AwardID 
		where aw.CreateDate between @fromDate and @toDate ) as Products  where Row>=@start and Row<=@end and IsActive=1 and IsDelete=0
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardNumber]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardNumber]
 (
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,aw.*,a.AwardName,a.AwardNameEnglish
		 from awardnumber aw left join award a on aw.AwardID=a.AwardID) as Products  where Row>=@start and Row<=@end
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardNumberByDate]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardNumberByDate]
 (
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,aw.*,a.AwardName,a.AwardNameEnglish from awardnumber aw left join award a on aw.AwardID=a.AwardID 
		where aw.CreateDate between @fromDate and @toDate ) as Products  where Row>=@start and Row<=@end
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardOrderByDateFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardOrderByDateFE]

as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.Createdate desc) as Row,awn.*,aw.AwardName,aw.AwardValue 
	from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID 
	 where awn.IsActive=1 and awn.IsDelete=0 ) as Products
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListBookingByRaw]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListBookingByRaw]
 (
	@Raw int
 )
 as begin

		SELECT BookingMegaball.*,Member.Email
		from BookingMegaball
		join Member 
		on BookingMegaball.MemberID = Member.MemberID
		where Convert(varchar,BookingMegaball.OpenDate,103) in (select Convert(varchar,AwardMegaball.CreateDate,103) from AwardMegaball where AwardMegaball.NumberID = @Raw)
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListCountry_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListCountry_CMS]
(
		@start int,
		@end int
)
as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY c.CreatedDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, c.* 
		from Country c where IsActive=1 and IsDeleted=0) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListLinkResetPasswordFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListLinkResetPasswordFE]
(
	@Date datetime,
	@Email nvarchar(250)
)

 as begin
		select *from LinkResetPassword 
		where CONVERT(nvarchar(250),CreatedDate, 103) = CONVERT(nvarchar(250),@Date, 103) and Email = @Email
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListMember]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListMember]
(
	@start int,
	@end int
)

 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.MemberID DESC) as Row,SUM(1) OVER()AS TOTALROWS,m.* from  member m ) as Products
		  where Row>=@start and Row<=@end
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetListTicketWinningByMemberID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetListTicketWinningByMemberID]   
	@MemberID int,
	@start int,
	@end int
AS 
BEGIN 
     SET NOCOUNT ON 
    select *from (SELECT ROW_NUMBER() OVER (ORDER BY tw.buydate DESC) as row, sum(1) over() as TotalRecord,tw.*,
	dbo.GetAwardWithdrawRequestStatus(mb.Email,tw.BookingID,tw.AwardDate) as AwardWithdrawStatus
	from TicketWinning tw
	join Member mb on mb.MemberID = tw.MemberID
	where tw.MemberID = @MemberID) as Products  where row>=@start and row<=@end
	
END


GO
/****** Object:  StoredProcedure [dbo].[SP_GetListTicketWinningBySearch]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetListTicketWinningBySearch]   
	@memberID int,
	@start int,
	@end int,
	@startdate datetime,
	@enddate datetime
AS 
BEGIN 
     SET NOCOUNT ON 
    select *from (SELECT ROW_NUMBER() OVER (ORDER BY tw.buydate DESC) as row, sum(1) over() as TotalRecord,tw.*,
	dbo.GetAwardWithdrawRequestStatus(mb.Email,tw.BookingID,tw.AwardDate) as AwardWithdrawStatus
	from TicketWinning tw
	join Member mb on mb.MemberID = tw.MemberID
	where (@memberID=0 or tw.MemberID = @memberID) and (tw.BuyDate between @startdate and @enddate)) as Products  where row>=@start and row<=@end
	
END


GO
/****** Object:  StoredProcedure [dbo].[SP_GetListWinner]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListWinner]
 (
	--@fromDate datetime
	--@toDate datetime,
	--@start int,
	--@end int
	@numberID int
 )
 as begin 
     DECLARE @firstNumber int,
	         @SecondNumber int,
			 @ThirdNumber int,
			 @FourthNumber int,
			 @FivethNumber int,
			 @ExtraNumber int,
			 @AWARDDATENOW datetime,
			 @AWARDDATEPREV datetime = NULL,
			 @JACKPOTWINNER FLOAT,
			 @AwardNameEnglish nvarchar(250);
	BEGIN  
	    --GET RESULT AWARD WITH CREATEDDATE LAST
		SELECT TOP 1 @AWARDDATENOW = AWARDMEGABALL.CREATEDATE,
		             @FirstNumber = AWARDMEGABALL.FirstNumber,
					 @SecondNumber = AWARDMEGABALL.SecondNumber,
					 @ThirdNumber = AWARDMEGABALL.ThirdNumber,
					 @FourthNumber = AWARDMEGABALL.FourthNumber,
					 @FivethNumber = AWARDMEGABALL.FivethNumber,
					 @ExtraNumber = AWARDMEGABALL.ExtraNumber,
					 @JACKPOTWINNER = AWARDMEGABALL.JACKPOTWINNER,
					 @AwardNameEnglish = Award.AwardNameEnglish
		FROM AWARDMEGABALL
		JOIN Award
		  ON Award.AwardID = AWARDMEGABALL.AwardID
		WHERE AWARDMEGABALL.ISDELETE = 0
		      AND AWARDMEGABALL.ISACTIVE = 1
			  AND AwardMegaball.NumberID = @numberID;

		--GET AWARD DATE PREVIOUS NOT WON
		SELECT TOP 1 @AWARDDATEPREV = CREATEDATE
		FROM AWARDMEGABALL
		WHERE AWARDMEGABALL.ISDELETE = 0
		      AND AWARDMEGABALL.ISACTIVE = 1
			  AND AWARDMEGABALL.NumberID != @numberID
			  AND AWARDMEGABALL.CreateDate < @AWARDDATENOW
			  ORDER BY CREATEDATE DESC;
	END;

	SELECT BOOKINGMEGABALL.BookingID,
	       MEMBER.Email,
		   BookingMegaball.TransactionCode,
		   BOOKINGMEGABALL.FirstNumber,
		   BOOKINGMEGABALL.SecondNumber,
		   BOOKINGMEGABALL.ThirdNumber,
		   BOOKINGMEGABALL.FourthNumber,
		   BOOKINGMEGABALL.FivethNumber,
		   BOOKINGMEGABALL.ExtraNumber, 
		   @JACKPOTWINNER JACKPOTWINNER,
		   BOOKINGMEGABALL.CreateDate,
		   @AWARDDATENOW OpenDate,
		   @AwardNameEnglish AwardNameEnglish
	FROM Member
	JOIN BOOKINGMEGABALL
	  ON BOOKINGMEGABALL.MemberID = Member.MemberID
	WHERE ((@AWARDDATEPREV is null 
	        and BOOKINGMEGABALL.CreateDate < @AWARDDATENOW
	        )
		    or (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREV and @AWARDDATENOW))
		and (
				BOOKINGMEGABALL.FirstNumber = @firstNumber 
				Or BOOKINGMEGABALL.FirstNumber = @secondNumber
				Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
				Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
				Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
				Or BOOKINGMEGABALL.FirstNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.SecondNumber = @firstNumber 
				Or BOOKINGMEGABALL.SecondNumber = @secondNumber
				Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
				Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
				Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
				Or BOOKINGMEGABALL.SecondNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.ThirdNumber = @firstNumber 
				Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
				Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
				Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
				Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
				Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.FourthNumber = @firstNumber 
				Or BOOKINGMEGABALL.FourthNumber = @secondNumber
				Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
				Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
				Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
				Or BOOKINGMEGABALL.FourthNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.FivethNumber = @firstNumber 
				Or BOOKINGMEGABALL.FivethNumber = @secondNumber
				Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
				Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
				Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
				Or BOOKINGMEGABALL.FivethNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.ExtraNumber = @firstNumber 
				Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
				Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
				Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
				Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
				Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
			)
		--and BOOKINGMEGABALL.FirstNumber = @firstNumber
		--AND BOOKINGMEGABALL.SecondNumber = @secondNumber
		--AND BOOKINGMEGABALL.ThirdNumber = @thirdNumber
		--AND BOOKINGMEGABALL.FourthNumber = @fourthNumber
		--AND BOOKINGMEGABALL.FivethNumber = @fivethNumber
		--AND BOOKINGMEGABALL.ExtraNumber = @extraNumber

		--select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.BookingID ASC) 
		--				as Row,SUM(1) OVER()AS TOTALROWS,
		--					m.Email,b.TransactionCode,b.BookingID,b.OpenDate,b.CreateDate,b.FirstNumber,
		--					b.SecondNumber,b.ThirdNumber,b.FourthNumber,b.FivethNumber,
		--					b.ExtraNumber,aw.AwardNameEnglish,a.JackPot
		--				from Member m 
		--				left join BookingMegaball b on m.MemberID=b.MemberID
		--				left join AwardMegaball a 
		--					   on (a.FirstNumber=b.FirstNumber and a.SecondNumber=b.SecondNumber and a.ThirdNumber=b.ThirdNumber
		--					 and a.FourthNumber=a.FourthNumber
		--					and a.FivethNumber=b.FivethNumber and a.ExtraNumber=b.ExtraNumber) 
		--				left join Award aw 
		--				       on a.AwardID=aw.AwardID
		--				where a.IsActive=1 and a.IsDelete=0 and b.Status=1
		--				 --and DATEADD(DD, DATEDIFF(DD, 0, a.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, b.OpenDate), 0)
		--				 and a.CreateDate between @fromDate and @toDate )
		--   as Products  where Row>=@start and Row<=@end
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetManagerIDNewest]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetManagerIDNewest]
as begin
		select top 1 AdminID from admin order by AdminID desc
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetMemberDetailByEmailFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetMemberDetailByEmailFE]
(
@email nvarchar(250)
)
as begin 
	select * from member where Email=@email
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetMemberDetailByIDFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetMemberDetailByIDFE]
(
@memberID int
)
as begin 
	select * from member where MemberID=@memberID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetNewestDrawID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetNewestDrawID]
as begin 
	select top 1 * from draw  order by createddate desc, DrawID asc
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetPackageDetail]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetPackageDetail]
(
	@packageID int
)
as begin 
	select * from package where PackageID=@packageID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GetPointsMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetPointsMemberFE]
(
@memberID int
)
as begin 
	select Points from member where MemberID=@memberID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAccessRight]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAccessRight]
(
	@groupID int,
	@adminID int
)
as begin
		insert into accessright(GroupID,AdminID) values(@groupID,@adminID)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_InsertAdmin]
(
	@userName nvarchar(50),
	@pass nvarchar(250),
	@hashKey nvarchar(250),
	@isActive smallint,
	@createDate datetime,
	@fullName nvarchar(250),
	@email nvarchar(250),
	@mobile char(50),
	@address nvarchar(250),
	@avatar nvarchar(250),
	@isDelete smallint
)
as
begin
	insert into admin(UserName,Password,Hashkey,IsActive,CreateDate,FullName,Email,Mobile,Address,Avatar,IsDelete)
				 values(@userName,@pass,@hashKey,@isActive,@createDate,@fullName,@email,@mobile,@address,@avatar,@isDelete);
				  SELECT SCOPE_IDENTITY()

end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAward]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertAward]
(
	--@awardName nvarchar(250),
	@awardValue decimal(18,10),
	@createDate datetime,
	@priority int,
	@isActive smallint,
	@isDelete smallint,
	@awardNameEnglish nvarchar(250),
	@awardFee decimal(18,10),
	@AwardPercent decimal(18,10)
)
as begin 
		insert into award(AwardName,AwardValue,CreateDate,Priority,IsActive,IsDelete,AwardNameEnglish,AwardFee,AwardPercent)
		 values('',@awardValue,@createDate,@priority,@isActive,@isDelete,@awardNameEnglish,@awardFee,@AwardPercent)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAwardFE]
(
	@awardName nvarchar(250),
	@awardValue float,
	@createDate datetime,
	@priority smallint,
	@isDelete smallint,
	@isActive smallint

)
as begin 
	insert into award(AwardName,AwardValue,CreateDate,Priority,IsActive,IsDelete) values(@awardName,@awardValue,@createDate,@priority,@isActive,@isDelete)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertAwardMegaball]
/*
	DESCRIPTION: @AWARDDATEPREVIOUS, @JACKPOTPREVIOUS, @TOTALJACKPOT
*/
(
	@awardID int,
	@createDate datetime,
	@isActive smallint,
	@isDelete smallint,
	@priority smallint,
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fivethNumber int,
	@extraNumber int,
	@genCode nvarchar(500),
	@jackPotDefault float,
	@DrawID int,
	@isExists int out
)
as begin
    DECLARE @AWARDDATEPREVIOUS DATETIME = null,
	        @JACKPOTPREVIOUS FLOAT = @JACKPOTDEFAULT,
	        @TOTALJACKPOT FLOAT = @JACKPOTDEFAULT,
			@JACKPOTWINNER FLOAT = 0;

    Set @isExists = 0;
	BEGIN
		SELECT @isExists = 1 
		FROM AWARDMEGABALL
		WHERE Convert(nvarchar, AWARDMEGABALL.CreateDate, 103) = Convert(nvarchar, @createDate, 103);
	END;

	IF(@isExists = 0) 
	BEGIN
			BEGIN  
				--GET RESULT AWARD WITH CREATEDDATE LAST
				SELECT TOP 1 @AWARDDATEPREVIOUS = CREATEDATE,
							 @JACKPOTPREVIOUS = AWARDMEGABALL.JackPot
				FROM AWARDMEGABALL
				WHERE AWARDMEGABALL.ISDELETE = 0
					  AND AWARDMEGABALL.ISACTIVE = 1
					  ORDER BY CREATEDATE DESC;
			END;

			BEGIN  
				SELECT @TOTALJACKPOT = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
				FROM BOOKINGMEGABALL
				WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
					  AND NOT EXISTS (SELECT 1
								  FROM BOOKINGMEGABALL
								  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
										and (
											   BOOKINGMEGABALL.FirstNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
											   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.SecondNumber = @firstNumber 
											   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
											   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
											   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
											   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
											   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
											   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.FourthNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
											   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.FivethNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
											   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
											   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
											));
			END

			IF(@TOTALJACKPOT IS NULL)
			BEGIN
				--ELSE IF NOW HAVE LEAST 1 MEMBER WINNER -> TOTAL JACKPOT WILL BE DEFAULT (CONFIG SOURCES) 
				--@JACKPOTWINNER: Total jackpot won -> save history
			   BEGIN  
					SELECT @JACKPOTWINNER = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
					FROM BOOKINGMEGABALL
					WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
						  AND EXISTS (SELECT 1
									  FROM BOOKINGMEGABALL
									  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
											and (
											   BOOKINGMEGABALL.FirstNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
											   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
											)
											and (
												   BOOKINGMEGABALL.SecondNumber = @firstNumber 
												   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
												   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
												   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
												   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
												   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
												   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.FourthNumber = @firstNumber 
												   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
												   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
												   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
												   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
												   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.FivethNumber = @firstNumber 
												   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
												   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
												   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
												   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
												   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
												   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
												));
				END
				--set @JACKPOTWINNER = @JACKPOTPREVIOUS;
			   SET @TOTALJACKPOT = @jackPotDefault;
			END
			--ELSE
			--BEGIN
			--   --IF NOT ANY LEAST 1 MEMBER WINNER -> TOTAL = DEFAULT (CONFIG) + JACTPOT PREVIOUS (IF HAVE)
			--   SET @TOTALJACKPOT = @JACKPOTPREVIOUS;
			--END;
			--UPDATE AWARDMEGABALL
			--SET AWARDMEGABALL.AWARDID = @awardID,
			--    AWARDMEGABALL.CreateDate = @createDate,
			--	AWARDMEGABALL.IsActive = @isActive,
			--	AWARDMEGABALL.IsDelete = @isDelete,
			--	AWARDMEGABALL.Priority = @priority,
			--	AWARDMEGABALL.FirstNumber = @firstNumber,
			--	AWARDMEGABALL.SecondNumber = @secondNumber,
			--	AWARDMEGABALL.ThirdNumber = @thirdNumber,
			--	AWARDMEGABALL.FourthNumber = @fourthNumber,
			--	AWARDMEGABALL.FivethNumber = @fivethNumber,
			--	AWARDMEGABALL.ExtraNumber = @extraNumber,
			--	AWARDMEGABALL.GenCode = @genCode,
			--	AWARDMEGABALL.JackPot = @TOTALJACKPOT,
			--	AWARDMEGABALL.JACKPOTWINNER = @JACKPOTWINNER
		 --   WHERE AWARDMEGABALL.IsDelete = 0
			--	  AND AWARDMEGABALL.IsActive = 0;
    
			--IF(@@ROWCOUNT = 0)
			--BEGIN
				INSERT 
				INTO AWARDMEGABALL(AWARDID,
								   CREATEDATE,
								   ISACTIVE,
								   ISDELETE,
								   PRIORITY,
								   FIRSTNUMBER,
								   SECONDNUMBER,
								   THIRDNUMBER,
								   FOURTHNUMBER,
								   FIVETHNUMBER,
								   EXTRANUMBER,
								   GENCODE,
								   JACKPOT, 
								   JackPotWinner,
								   DrawID)
					VALUES(@AWARDID,
						   @CREATEDATE,
						   @ISACTIVE,
						   @ISDELETE,
						   @PRIORITY,
						   @FIRSTNUMBER,
						   @SECONDNUMBER,
						   @THIRDNUMBER,
						   @FOURTHNUMBER,
						   @FIVETHNUMBER,
						   @EXTRANUMBER,
						   @GENCODE,
						   @TOTALJACKPOT,
						   @JACKPOTWINNER,
						   @DrawID)
			--END;
	END;
	
	
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardNumber]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAwardNumber]
 (
	@awardID int,
	@numberValue float,
	@createDate datetime,
	@isActive smallint,
	@isDelete smallint,
	@priority int,
	@stationName nvarchar(250),
	@stationNameEnglish nvarchar(250)
 )
 as begin 
		insert into awardnumber(AwardID,NumberValue,CreateDate,IsActive,IsDelete,Priority,StationName,StationNameEnglish)
		 values(@awardID,@numberValue,@createDate,@isActive,@isDelete,@priority,@stationName,@stationNameEnglish)
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardNumberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAwardNumberFE]
(
	@numberID int,
	@awardID int,
	@createDate datetime,
	@priority smallint,
	@isDelete smallint,
	@isActive smallint,
	@numberValue int,
	@stationName nvarchar(250)
)
as begin 
	insert into awardnumber(NumberID,AwardID,NumberValue,CreateDate,IsActive,IsDelete,Priority,StationName) values(@numberID,@awardID,@numberValue,@createDate,@isActive,@isDelete,@priority,@stationName)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertBooking_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertBooking_FE]
(
	@memberID int,
	@createDate datetime,
	@openDate datetime,
	@status int,
	@transactionCode char(100),
	@note nvarchar(2500),
	@noteEnglish nvarchar(2500),
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fivethNumber int,
	@extraNumber int,
	@quantity int,
	@ValuePoint numeric(18,10),
	@DrawID int,
	@IsFreeTicket int

)
as begin 
	insert into BookingMegaball(MemberID,CreateDate,OpenDate,Status,TransactionCode,Note,NoteEnglish,FirstNumber,SecondNumber,ThirdNumber,FourthNumber,FivethNumber,ExtraNumber,Quantity,ValuePoint,DrawID,IsFreeTicket)
	 values(@memberID,@createDate,@openDate,@status,@transactionCode,@note,@noteEnglish,@firstNumber,@secondNumber,@thirdNumber,@fourthNumber,@fivethNumber,@extraNumber,@quantity,@ValuePoint,@DrawID,@IsFreeTicket)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertBookingFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertBookingFE]
(
	@memberID int,
	@numberValue int,
	@quantity int,
	@createDate datetime,
	@openDate datetime,
	@status int,
	@transactionCode char(100)
)
as begin 
	insert into booking(MemberID,NumberValue,Quantity,CreateDate,OpenDate,Status,TransactionCode) values(@memberID,@numberValue,@quantity,@createDate,@openDate,@status,@transactionCode)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertDraw]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_InsertDraw]
(
--@AWARDNAME NVARCHAR(250),
@STARTDATE DATETIME,
@ENDDATE DATETIME,
@CREATEDDATE DATETIME
)
AS BEGIN 
declare @ISEXISTS INT;
SET @ISEXISTS = 0;
BEGIN
	SELECT @ISEXISTS = 1 
	FROM DRAW
	WHERE CONVERT(NVARCHAR, DRAW.CREATEDDATE, 103) = CONVERT(NVARCHAR, @CREATEDDATE, 103);
END;
	IF(@ISEXISTS = 0) 
	BEGIN
	INSERT INTO DRAW(STARTDATE,ENDDATE,CREATEDDATE)
		VALUES(@STARTDATE,@ENDDATE,@CREATEDDATE)
		END;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertExchangePoint]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertExchangePoint]
(
	@pointValue float,
	@bitcoinValue float,
	@isActive smallint,
	@isDelete smallint,
	@createDate datetime,
	@createUser nvarchar(250),
	@memberID int,
	@updateDate datetime,
	@updateUser nvarchar(250),
	@deleteDate date,
	@deleteUser nvarchar(250)
)
as begin
	insert into ExchangePoint(PointValue,BitcoinValue,IsActive,IsDelete,CreateDate,CreateUser,MemberID,UpdateDate,UpdateUser,DeleteDate,DeleteUser)
	values(@pointValue,@bitcoinValue,@isActive,@isDelete,@createDate,@createUser,@memberID,@updateDate,@updateUser,@deleteDate,@deleteUser)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertExchangeTicket]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertExchangeTicket]
(
	@pointValue float,
	@ticketNumber int,
	@isActive smallint,
	@isDelete smallint,
	@createDate datetime,
	@createUser nvarchar(250),
	@memberID int,
	@updateDate datetime,
	@updateUser nvarchar(250),
	@deleteDate date,
	@deleteUser nvarchar(250),
	@coinID char(10)
)
as begin
	insert into ExchangeTicket(PointValue,TicketNumber,IsActive,IsDelete,CreateDate,CreateUser,MemberID,UpdateDate,UpdateUser,DeleteDate,DeleteUser,CoinID)
	values(@pointValue,@ticketNumber,@isActive,@isDelete,@createDate,@createUser,@memberID,@updateDate,@updateUser,@deleteDate,@deleteUser,@coinID)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertFreeTicketDaily]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_InsertFreeTicketDaily] (@NumberTicket int)
AS
BEGIN
  DECLARE @ISEXISTS int;
  DECLARE @TotalTicket int;
  DECLARE @AmountFreeTicket int;
  SET @ISEXISTS = 0;
  SET @TotalTicket = 0;
  BEGIN
    SELECT
      @ISEXISTS = 1
    FROM FreeTicketDaily
    WHERE CONVERT(nvarchar, FreeTicketDaily.Date, 103) = CONVERT(nvarchar, SYSDATETIME(), 103)
    AND CONVERT(nvarchar, FreeTicketDaily.CreatedDate, 103) = CONVERT(nvarchar, SYSDATETIME(), 103)
  END;
  IF (@ISEXISTS = 0)
  BEGIN
    SET @AmountFreeTicket = (SELECT
      Amount
    FROM StoreFreeTicket)
    IF (@AmountFreeTicket > 0)
    BEGIN
      IF (@AmountFreeTicket >= @NumberTicket)
      BEGIN
        SET @TotalTicket = (SELECT
          Total
        FROM FreeTicketDaily
        WHERE IsActive = 1);

        UPDATE FreeTicketDaily
        SET IsActive = 0
        WHERE IsActive = 1;

        INSERT INTO FreeTicketDaily (Date, Total, IsActive, CreatedDate)
          VALUES (SYSDATETIME(), (@TotalTicket + @NumberTicket), 1, SYSDATETIME())

        UPDATE StoreFreeTicket
        SET amount = (@AmountFreeTicket - @NumberTicket)

      END;
      ELSE
      BEGIN
        UPDATE FreeTicketDaily
        SET IsActive = 0
        WHERE IsActive = 1;

        INSERT INTO FreeTicketDaily (Date, Total, IsActive, CreatedDate)
          VALUES (SYSDATETIME(), (@TotalTicket + @AmountFreeTicket), 1, SYSDATETIME())

        UPDATE StoreFreeTicket
        SET amount = 0
      END;
    END;
  END;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertGroupAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertGroupAdmin]
(
	@groupName nvarchar(250),
	@isActive smallint
)
as begin
		insert into groupadmin(GroupName,IsActive) values(@groupName,@isActive)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertLinkResetPasswordFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertLinkResetPasswordFE]
(
	@Email nvarchar(250),
	@LinkReset nvarchar(1000),
	@ExpireLink nvarchar(1000),
	@IPAddress nvarchar(50)
)
as begin 
		update LinkResetPassword set Status = 1 where CreatedDate <= SYSDATETIME() and Email = @Email
		insert into LinkResetPassword(Email,LinkReset,Status,NumberSend,CreatedDate,ExpireLink,IPAddress)
		 values(@Email,@LinkReset,0,1,SYSDATETIME(),@ExpireLink,@IPAddress)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertMemberFE]
/*
	Edited By: Lý Kiến Đức
	Edited date: 16.01.2018
	Description: Bổ sung field @FBUserID
*/
(
	@email nvarchar(250),
	@password nvarchar(250) = null,
	@isActive smallint,
	@isDelete smallint,
	@points numeric(18,10),
	@createDate datetime = null,
	@fullName nvarchar(250),
	@mobile char(15) = null,
	@avatar nvarchar(250) = null,
	@gender smallint,
	@birthday datetime = null,
	@IsUserType smallint = 0,
	@UserTypeID varchar(100) = null,
	@LinkLogin nvarchar(250),
	@NumberTicketFree int,
	@LinkReference nvarchar(250),
	@outMemberID int OUTPUT

)
as begin 
	insert into member(Email, Password, IsActive, IsDelete, Points, CreateDate, FullName, Mobile, Avatar, Gender, Birthday, IsUserType, UserTypeID, LinkLogin, NumberTicketFree, LinkReference)
	 values(@email, @password, @isActive, @isDelete, @points, @createDate, @fullName, @mobile, @avatar, @gender, @birthday, @IsUserType, @UserTypeID, @LinkLogin,@NumberTicketFree, @LinkReference)

	 SELECT @outMemberID = SCOPE_IDENTITY() 
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertMemberReferenceFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertMemberReferenceFE]
(
	@MemberID int,
	@LinkReference nvarchar(1000),
	@MemberIDReference int,
	@LockID nvarchar(1000),
	@Status int,
	@Amount decimal(18,5)
)
as begin 
	insert into MemberReference(MemberID, CreatedDate, LinkReference, MemberIDReference, LockID, Status, Amount)
	 values(@MemberID, SYSDATETIME(), @LinkReference, @MemberIDReference, @LockID, @Status, @Amount)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertMemberWalletFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertMemberWalletFE]
(
	@IndexWallet int,
	@IsActive smallint,
	@IsDelete smallint,
	@MemberID int,
 	@NumberCoin float,
	@WalletAddress nvarchar(250),
	@RippleAddress nvarchar(250),
	@SerectKeyRipple nvarchar(250)
)
as begin 
	insert into Member_Wallet(IndexWallet,IsActive,IsDelete,MemberID,NumberCoin,WalletAddress,RippleAddress,SerectKeyRipple) values(@IndexWallet,@IsActive,@IsDelete,@MemberID,@NumberCoin,@WalletAddress,@RippleAddress,@SerectKeyRipple)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTicketWinning]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_InsertTicketWinning]   
    @AwardNumber nvarchar(50),
	@BookingID int,
	@CreatedDate datetime,
	@DrawID int,
	@ExtraNumber nvarchar(50),
	@FirstNumber nvarchar(50),
	@FivethNumber nvarchar(50),
	@FourthNumber nvarchar(50),
	@MemberID int,
	@SecondNumber nvarchar(50),
	@ThirdNumber nvarchar(50),
	@Value decimal(18,5)
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into TicketWinning(AwardNumber,BookingID,CreatedDate,DrawID,ExtraNumber,FirstNumber,FivethNumber,FourthNumber,MemberID,SecondNumber,ThirdNumber,Value) values(@AwardNumber,@BookingID,@CreatedDate,@DrawID,@ExtraNumber,@FirstNumber,@FivethNumber,@FourthNumber,@MemberID,@SecondNumber,@ThirdNumber,@Value) 
END

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTransactionCoinFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertTransactionCoinFE]
(
	@MemberID int,
	@WalletAddressID char(150),
	@ValueTransaction numeric(18, 10),
	@QRCode  nvarchar(500),
	@CreateDate datetime,
	@ExpireDate datetime,
	@Status smallint,
	@Note nvarchar(2500),
	@WalletID nvarchar(500),
	@TypeTransactionID int,
	@TransactionBitcoin nvarchar(500),
	@TransactionID char(150),
	@CoinID nchar(10)
)
as begin
	insert into TransactionCoin(TransactionID,WalletAddressID,MemberID,ValueTransaction,QRCode,CreateDate,ExpireDate,Status,Note,WalletID,TypeTransactionID,TransactionBitcoin,CoinID) 
                                values(@TransactionID,@WalletAddressID,@MemberID,@ValueTransaction,@QRCode,@CreateDate,@ExpireDate,@Status,@Note,@WalletID,@TypeTransactionID,@TransactionBitcoin,@CoinID)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTransactionPackageFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertTransactionPackageFE]
(
	@memberID int,
	@packageID int,
	@createDate datetime,
	@expireDate datetime, 
	@status int,
	@transactionValue  nvarchar(250),
	@note nvarchar(3500)

)
as begin 
	insert into transactionpackage(MemberID,PackageID,CreateDate,ExpireDate,Status,TransactionCode,Note) values(@memberID,@packageID,@createDate,@expireDate,@status,@transactionValue,@note)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTransactionPointfe]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertTransactionPointfe]
(
	@memberID int,
	@points int,
	@createDate datetime,
	@status int,
	@transactionCode nvarchar(500),
	@note nvarchar(2500)

)
as begin 
	insert into transactionpoint(MemberID,Points,CreateDate,Status,TransactionCode,Note) values(@memberID,@points,@createDate,@status,@transactionCode,@note)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_List_GroupAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_List_GroupAdmin]
as begin
		select *from groupadmin
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAccessRightByManagerID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAccessRightByManagerID]
(@adminID int)
as begin
		select *from accessright where AdminID=@adminID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllAdmin]
as begin
		select a.*,ac.GroupID,g.GroupName
		 from Admin a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID;

end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllAdminPaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllAdminPaging]
(   @start int,
	@end int)
as begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY a.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,a.*,g.GroupName,g.GroupID
		 from Admin a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID)
		 as Products  where Row>=@start and Row<=@end

end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBooking]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBooking]
as begin
		select b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID  order by b.CreateDate DESC
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaball]
as begin
		select b.*,m.Email from BookingMegaball b left join Member m on b.MemberID=m.MemberID order by b.CreateDate DESC 
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballByDate]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaballByDate]
(
		@date datetime
)
as begin
		select b.*,m.Email 
		from BookingMegaball b 
		left join Member m on b.MemberID=m.MemberID 
		where CONVERT(nvarchar(250),b.CreateDate, 103) = CONVERT(nvarchar(250), @date, 103)
		order by b.CreateDate DESC 
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballByDraw]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllBookingMegaballByDraw]
(
		@DrawID int
)
as begin
		select b.*,m.Email 
		from BookingMegaball b 
		left join Member m on b.MemberID=m.MemberID 
		where b.DrawID = @DrawID
		order by b.CreateDate DESC 
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballPaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaballPaging]
(
	@start int,
	@end int
)
as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, b.*,m.Email 
		from BookingMegaball b left join member m on b.MemberID=m.MemberID) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingPaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingPaging]
(
		@start int,
		@end int
)
as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet 
		from booking b left join member m on b.MemberID=m.MemberID) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllCoin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_ListAllCoin]
as begin
	select * from Coin where IsActive =1
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllCompanypaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllCompanypaging]
 (
	@start int,
	@end int
)

 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY c.MemberID  DESC) as Row,SUM(1) OVER()AS TOTALROWS,c.*, m.FullName from  CompanyInfomation c left join Member m on c.MemberID=m.MemberID
		) as Products
		  where Row>=@start and Row<=@end
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangeBySearch]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllExchangeBySearch]
(
	@start int,
	@end int,	
	@fromDate datetime = null,
	@toDate datetime=null
)
as begin 
	select * from (Select ROW_NUMBER() over(Order by ex.CreateDate DESC)as Row,SUM(1) over() as TOTALROWS,
	ex.* from ExchangeTicket ex where @fromDate=null or ex.CreateDate between @fromDate and @toDate)
	as Products where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangeNoPaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangeNoPaging]
as begin
	select * from ExchangeTicket order by CreateDate DESC
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangePoint]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangePoint]
as begin
	select * from ExchangePoint order by CreateDate desc
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangePointBySearch]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllExchangePointBySearch]
(
	@start int,
	@end int,	
	@fromDate datetime =null,
	@toDate datetime =null
)
as begin 
	select * from (Select ROW_NUMBER() over(Order by ex.CreateDate DESC)as Row,SUM(1) over() as TOTALROWS,
	ex.* from ExchangePoint ex where @fromDate=null or ex.CreateDate between @fromDate and @toDate)
	as Products where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangePointPaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangePointPaging]
(
	@start int,
	@end int
)
as 
begin
	
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY ex.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, ex.*,ad.UserName from ExchangePoint ex left join Admin ad on ex.CreateUser= ad.UserName) as Products 
	 where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangeTicket]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangeTicket]
(
	@start int,
	@end int
)
as 
begin
	
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY ex.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, ex.*,ad.UserName from ExchangeTicket ex left join Admin ad on ex.CreateUser= ad.UserName) as Products 
	 where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllMember]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllMember]
 as begin
	Select Email from Member order by CreateDate DESC
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllPackageFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllPackageFE]

as begin 
	select * from package where IsActive=1 order by Priority ASC
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllTransactionPackage]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllTransactionPackage]
(
	@start int,
	@end int
)
as begin 
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, tp.*,p.PackageName,m.Email,m.E_Wallet from transactionpackage tp left join package p on tp.PackageID=p.PackageID left join member m on tp.MemberID=m.MemberID) as Products
		  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAwardMegaballBySearch]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAwardMegaballBySearch]
(
	@start int,
	@end int,	
	@fromDate datetime = null,
	@toDate datetime = null
	--@awardNameEnglish nvarchar(250)
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY a.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
a.*,aw.AwardNameEnglish from AwardMegaball a left join Award aw on a.AwardID=aw.AwardID
where @fromDate=null or a.CreateDate between @fromDate and @toDate
) as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAwardWithdraw_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListAwardWithdraw_CMS]
(
	@UserName nvarchar(250),
	@FromDate datetime,
	@ToDate datetime,
	@RequestStatus smallint,
	@AwardDate datetime,
	@Start int,
	@End int
)
as
begin
select *from 
(
SELECT ROW_NUMBER() OVER (ORDER BY aw.RequestDate desc)as row, 
sum(1) over() as TOTALROWS,
		aw.AdminIDApprove,
		aw.ApproveDate,
		aw.ApproveNote,
		aw.AwardDate,
		aw.CoinIDWithdraw,
		aw.IsDeleted,
		aw.RequestDate,
		aw.RequestMemberID,
		aw.RequestStatus,
		aw.RequestWalletAddress,
		aw.TransactionID,
		aw.ValuesWithdraw
		from AwardWithdraw aw 
		where
		(@UserName is null or @UserName='' or aw.RequestMemberID=@UserName)
		and (@FromDate='01/01/1990' or (aw.RequestDate between @FromDate and @ToDate))
		and (@RequestStatus=-1 or aw.RequestStatus=@RequestStatus)
		and (@AwardDate='01/01/1990' or aw.AwardDate =@AwardDate)
		) as awardresult  
		where Row>=@Start and Row<=@End
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingBySearchCMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ListBookingBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)
and (@status=-1 or b.Status=@status)
) as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingBySearchFrontEnd]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ListBookingBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballByDate]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_ListBookingMegaballByDate]
(
@Date datetime
)
as begin
		select * 
		from BookingMegaball
		where Convert(varchar,OpenDate,103) = CONVERT(varchar, @Date, 103)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballBySearchCMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListBookingMegaballBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime = null,
	--@fromDate datetime,
	--@toDate datetime,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email from BookingMegaball b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
--and (b.CreateDate between @fromDate and @toDate)
--and (@fromDate= '01/01/1990 12:01:00' or b.CreateDate between @fromDate and @toDate)
and (@fromDate= null or b.CreateDate between @fromDate and @toDate)
and (@status=-1 or b.Status=@status)
) as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballBySearchFrontEnd]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListBookingMegaballBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email from BookingMegaball b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
and (@fromDate IS NULL or b.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListDetailsByMemberIDRef]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListDetailsByMemberIDRef]
(
	@MemberID int
)
as begin 
	select *from MemberReference where MemberIDReference = @MemberID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListExchangePointByMember]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListExchangePointByMember]
(
@memberID int
)
as begin 
	select *from ExchangePoint where memberID = @memberID and isdelete = 0 and isactive = 1
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListExchangeTicketByMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListExchangeTicketByMemberFE]
(
	@memberID int,
	@CoinID nchar(10)
)
as begin
	select *from ExchangeTicket where memberID = @memberID and isdelete = 0 and isactive = 1 and CoinID = @CoinID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListGroupAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListGroupAdmin]
as begin
		select *from groupadmin
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListMember_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ListMember_FE]
(
	@start int,
	@end int
)
as
begin
select *from 
(SELECT ROW_NUMBER() OVER (ORDER BY m.MemberID DESC) as Row,
m.MemberID,
mb.RippleAddress,
m.FullName,
m.Avatar,
m.Mobile,
m.Email,
m.Points,
mb.NumberCoin,
mb.IndexWallet,
mb.WalletAddress
from Member m left join Member_Wallet mb on m.MemberID=mb.MemberID where m.IsDelete=0 
group by m.MemberID,
mb.RippleAddress,
m.FullName,
m.Avatar,
m.Mobile,
m.Email,
m.Points,
mb.NumberCoin,
mb.IndexWallet,
mb.WalletAddress) as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListMemberReferenceFEByMemberID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListMemberReferenceFEByMemberID]
(
	@MemberID int
)
as begin 
	select *from MemberReference where MemberID = @MemberID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTicketConfig]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTicketConfig]

as begin 
	select *from TicketConfig 
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTicketWinningByMemberID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTicketWinningByMemberID]
(
	@memberID int
)
as begin
		select BM.*
		from 
		BookingMegaball BM
		join AwardMegaball AM
		on BM.DrawID = am.DrawID
		where BM.MemberID = @memberID;
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByDate_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingByDate_FE]
(
	@fromDate datetime,
	@toDate datetime
)
as begin 
	select b.*,m.Email,m.E_Wallet 
	from bookingmegaball b 
	left join member m 
	       on b.MemberID=m.MemberID 
    where (b.CreateDate between @fromDate and @toDate) 
	      and b.Status=1 
    order by b.CreateDate DESC
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByDateFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingByDateFE]
(
	@fromDate datetime,
	@toDate datetime
)
as begin 
	select Distinct b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID where b.CreateDate between @fromDate and @toDate and b.Status=1 order by b.CreateDate DESC
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionBookingByMemberFE]
(
	@memberID int
)
as begin 
	select b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID where b.MemberID=@memberID  order by CreateDate DESC
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByMemberPagingFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingByMemberPagingFE]
(
	@memberID int,
	@start int,
	@end int
)	
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet 
	from bookingmegaball b left join member m on b.MemberID=m.MemberID left join AwardMegaball aw on b.CreateDate=aw.CreateDate
	 where b.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingMegaballByMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingMegaballByMemberFE]
(
	@memberID int,
	@start int,
	@end int
)
as begin 
	select * from(SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,
	 b.*,m.Email,m.E_Wallet from BookingMegaball b left join member m on b.MemberID=m.MemberID where b.MemberID=@memberID and b.Status = 1  
	 ) as product
	where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingOrderByDateFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingOrderByDateFE]
(
	@start int,
	@end int
)
as begin 
	select * from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet
	 from bookingmegaball b left join member m on b.MemberID=m.MemberID) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionCoin]
as begin 
	select tc.*,m.Email from TransactionCoin tc left join member m on tc.MemberID=m.MemberID  order by tc.CreateDate DESC	
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinBySearchCMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionCoinBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime =null,
	@toDate datetime=null,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tc.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tc.*,m.Email
from TransactionCoin tc left join member m on tc.MemberID=m.MemberID 
where (@memberID=0 or tc.MemberID=@memberID)
and (@status=-1 or tc.Status=@status)
and (@fromDate=null or tc.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinPaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionCoinPaging]
(
	@start int,
	@end int
)
as begin 
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY tc.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, tc.* from TransactionCoin tc) as Products
		  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinStatus]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionCoinStatus]
(
	@status int,
	@transactionID char(150)
)
as begin 
		update TransactionCoin set Status=@status where TransactionID=@transactionID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageActiveByMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPackageActiveByMemberFE]
(
@memberID int
)
as begin 
	select top 1 * from TransactionPackage where MemberID=@memberID and Status=1 order by ExpireDate desc
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageByMember_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPackageByMember_FE]
(
@end int,
@start int,
@memberID int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS,tp.*,p.PackageName,p.PackageNameEnglish,m.Email,m.E_Wallet from transactionpackage tp left join package p on tp.PackageID=p.PackageID left join member m on tp.MemberID=m.MemberID
	 where tp.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageByMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPackageByMemberFE]
(
@memberID int
)
as begin 
	select tp.*,p.PackageName,m.Email,m.E_Wallet from transactionpackage tp left join package p on tp.PackageID=p.PackageID left join member m on tp.MemberID=m.MemberID where tp.MemberID=@memberID  order by CreateDate DESC
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageBySearchCMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPackageBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,p.PackageName,p.PackageNameEnglish, m.Email,m.E_Wallet 
from transactionpackage tp left join package p on tp.PackageID=p.PackageID 
left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@status=-1 or tp.Status=@status)
and (@fromDate='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageBySearchFrontEnd]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPackageBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,p.PackageName,p.PackageNameEnglish,m.Email,m.E_Wallet 
from transactionpackage tp left join package p on tp.PackageID=p.PackageID 
left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@fromDate='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPoint]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPoint]
as begin 
	select tp.*,m.Email,m.E_Wallet from transactionpoint tp left join member m on tp.MemberID=m.MemberID  order by tp.CreateDate DESC	
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointByMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPointByMemberFE]
(
	@memberID int,
	@start int,
	@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, tp.*,m.Email,m.E_Wallet from transactionpoint tp left join member m on tp.MemberID=m.MemberID 
	where tp.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointBySearchCMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPointBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime =null,
	@toDate datetime=null,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,m.Email,m.E_Wallet 
from transactionpoint tp left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@status=-1 or tp.Status=@status)
and (@fromDate=null or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointBySearchFrontEnd]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPointBySearchFrontEnd]
/*
	EDITED BY: LÝ KIẾN ĐỨC
	EDITED DATE: 05.03.2018
	DESCRIPTION: CHANGE @FROMDATE = '1/1/1990 12:00:00 AM' -> @FROMDATE IS NULL
*/
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime
	
)
as
begin
--select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
--tp.*,m.Email,m.E_Wallet 
--from transactionpoint tp left join member m on tp.MemberID=m.MemberID 
--where (@memberID=0 or tp.MemberID=@memberID)

--and (@fromDate='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
--) as Products  where Row>=@start and Row<=@end

	SELECT *
	FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY TP.CREATEDATE DESC) AS ROW,
			       SUM(1) OVER()AS TOTALROWS, 
				   TP.*,
				   M.EMAIL,
				   M.E_WALLET 
			FROM TRANSACTIONPOINT TP 
			LEFT JOIN MEMBER M 
			       ON TP.MEMBERID = M.MEMBERID 
			WHERE (@MEMBERID = 0 OR TP.MEMBERID = @MEMBERID)
				   AND (@FROMDATE IS NULL 
					    OR TP.CREATEDATE BETWEEN @FROMDATE AND @TODATE)
	) AS PRODUCTS  
	WHERE ROW >= @START 
		  AND ROW <= @END
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointPage]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionPointPage]
(
	@start int,
	@end int
)
as begin 
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, tp.*,m.Email,m.E_Wallet from transactionpoint tp left join member m on tp.MemberID=m.MemberID) as Products
		  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointStatus]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPointStatus]
(
	@status int,
	@transactionID int
)
as begin 
		update transactionpoint set Status=@status where TransactionID=@transactionID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionsBookingByMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionsBookingByMemberFE]
(
	@memberID int,
	@start int,
	@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID
	 where b.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionWalletByMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionWalletByMemberFE]
(
@memberID int,
@start int,
@end int
)
as begin
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, tp.*,m.Email,m.E_Wallet from TransactionCoin tp left join member m on tp.MemberID=m.MemberID 
	where tp.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionWalletBySearchFrontEnd]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionWalletBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,m.Email,m.E_Wallet 
from TransactionCoin tp left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@fromDate IS NULL or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAward]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAward]
 (
	@awardID int,
	@isActive smallint
 )
 as begin
		update award set IsActive=@isActive where AwardID=@awardID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAwardFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAwardFE]
(
	@isActive smallint,
	@awardID int
)
as begin 
	update award set IsActive=@isActive where AwardID=@awardID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAwardMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAwardMegaball]
 (
	@numberID int,
	@isActive smallint
 )
 as begin
		update AwardMegaball set IsActive=@isActive where NumberID=@numberID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAwardNumber]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAwardNumber]
 (
	@isActive smallint,
	@numberID int
 )
 as begin 
		update awardnumber set IsActive=@isActive where NumberID=@numberID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockExchangePoint]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockExchangePoint]
 (
 @exchangeID int,
 @isActive smallint
 )
 as begin
	update ExchangePoint set IsActive=@isActive where ExchangeID=@exchangeID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockExchangeTicket]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockExchangeTicket]
 (
 @exchangeID int,
 @isActive smallint
 )
 as begin
	update ExchangeTicket set IsActive=@isActive where ExchangeID=@exchangeID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockMember]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LockAndUnlockMember]
(
	@isActive smallint,
	@memberID int
)
as begin 

		update member set IsActive=@isActive where MemberID=@memberID and IsActive = 0
end

GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LockAndUnlockMemberFE]
(
	@isActive smallint,
	@memberID int
)
as begin 
	update member set IsActive=@isActive where MemberID=@memberID and IsActive = 0
end

GO
/****** Object:  StoredProcedure [dbo].[SP_LoginAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_LoginAdmin]
(
	@userName nvarchar(250),
	@pass nvarchar(250)
)
as 
begin
select UserName,Password,ac.GroupID from admin a left join AccessRight ac on a.AdminID=ac.AdminID 
where UserName=@userName 
and Password=@pass 
and IsActive=1 
and IsDelete=0;
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_LoginManagerAccount]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LoginManagerAccount]
(
	@userName nvarchar(50),
	@pass nvarchar(250)
)
as begin
        select UserName,Password,ac.GroupID from admin a left join AccessRight ac on a.AdminID=ac.AdminID
		 where UserName=@userName and Password=@pass and IsActive=1 and IsDelete=0;
end

GO
/****** Object:  StoredProcedure [dbo].[SP_LoginMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LoginMemberFE]
(
	@email nvarchar(250),
	@password nvarchar(250)


)
as begin 
	select m.* from member m where Email=@email and Password=@password and m.IsActive=1 and m.IsDelete=0
end

GO
/****** Object:  StoredProcedure [dbo].[SP_Member_DEL]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_Member_DEL]   
    @memberID int
AS   
    delete Member where MemberID = @memberID

GO
/****** Object:  StoredProcedure [dbo].[SP_Member_ResetPassword]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Member_ResetPassword]   
    @Email nvarchar(250)
AS   
    Update Member set Password = '' where Email = @Email

GO
/****** Object:  StoredProcedure [dbo].[SP_MEMBER_SEL_CHECKMEM]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_MEMBER_SEL_CHECKMEM]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 16.01.2018
	DESCRIPTION: CHECK MEMBER FACEBOOK EXISTS
*/
(
	@Email nvarchar(250)
)
AS BEGIN
	SELECT M.MemberID
	FROM MEMBER M
	WHERE M.IsDelete = 0
	      AND M.Email = @Email;
END;

GO
/****** Object:  StoredProcedure [dbo].[SP_MEMBER_SELBYMEMID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_MEMBER_SELBYMEMID]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 16.01.2018
	DESCRIPTION: Load Info Member By MemberID
*/
(
	@MemberID int
)
AS BEGIN
	SELECT M.MemberID,
	       M.Email,
		   M.E_Wallet,
		   M.Password,
		   M.Points,
		   M.FullName,
		   M.IsUserType,
		   M.UserTypeID,
		   --M_W.WalletAddress,
		   --M_W.RippleAddress,
		   --M_W.SerectKeyRipple,
		   M.Points,
		   M.NumberTicketFree,
		   M.LinkReference
	FROM MEMBER M
	--JOIN Member_Wallet M_W
	--  ON M_W.MemberID = M.MemberID
	WHERE M.IsDelete = 0
	      AND M.MemberID = @MemberID;
END;

GO
/****** Object:  StoredProcedure [dbo].[SP_MEMBER_SELPOINT]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[SP_MEMBER_SELPOINT]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 02.03.2018
	DESCRIPTION: LOAD POINT BY MEMBERID
*/
(
	@MemberID int
)
AS BEGIN 
	SELECT MEMBER.Points
	FROM MEMBER
	WHERE MEMBER.MemberID = @MemberID;
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Member_Wallet_SELPOINT]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_Member_Wallet_SELPOINT]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 11.01.2018
	DESCRIPTION: LOAD INFO POINT AND ADDRESS BY MEMBERID
*/
(
	@MemberID int
)
as
begin
	SELECT MEM.Points,
	       MW.WalletAddress,
		   MW.RippleAddress,
		   MW.SerectKeyRipple
	FROM Member_Wallet MW
	JOIN Member MEM
	  ON MEM.MemberID = MW.MemberID
	WHERE MW.MemberID = @memberID;
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_SearchCompanyByKeyword]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_SearchCompanyByKeyword]
(
	@Keyword nvarchar(400)
)

as
begin

select * from CompanyInfomation c

where ((@Keyword is null) or (@Keyword='0') or (c.CompanyName like '%' + @Keyword + '%') or c.Email like '%' + @Keyword + '%' )

end;

GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberByAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_SearchMemberByAdmin]
(
	@Keyword nvarchar(400),
	@start int,
	@end int,
	@fromDate datetime = null,
	@toDate datetime = null
)

as
begin

--select * from Member m

--where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,m.* from Member m
where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
and (@fromDate= null or m.CreateDate between @fromDate and @toDate))
as Products  where Row>=@start and Row<=@end
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberByBookingMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_SearchMemberByBookingMegaball]
(
	@Keyword nvarchar(400),
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
	)
as begin 
select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,b.*,m.Email 
	 from BookingMegaball b left join Member m on b.MemberID=m.MemberID
	-- where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	--) as product
	--where Row>=@start and Row<=@end
    where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)) as product
	where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberNoPaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_SearchMemberNoPaging]
(
	@Keyword nvarchar(400))
as begin 
	select * from Member m

    where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
end

GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberPaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_SearchMemberPaging]
(
	@keyword nvarchar(250)
)	
as
begin
select COUNT(Email) as Totalkeyword from Member where Email like N'%'+@keyword+'%';
end

GO
/****** Object:  StoredProcedure [dbo].[SP_SumValuePointByMemberID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_SumValuePointByMemberID]
	 (
		@memberID int
	 )
	 as begin 
		 select sum(ValuePoint) as TotalSum from bookingmegaball where MemberID=@memberID
	end

GO
/****** Object:  StoredProcedure [dbo].[SP_SumValuePoints]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_SumValuePoints]
  as begin
	select Sum(ValuePoint) as TotalCount from BookingMegaball 
  end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateActive]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateActive]
(
	@email nvarchar(250)
)
as begin 
	update member set IsActive=1 where Email=@email
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAdmin]
(
	@adminID int,
	@fullName nvarchar(250),
	@email nvarchar(250),
	@mobile char(50),
	@address nvarchar(250)
)
as begin
		update admin set FullName=@fullName,Email=@email,Mobile=@mobile,Address=@address where AdminID=@adminID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAward]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAward]
(
	@awardID int,
	--@awardName nvarchar(250),
	--@awardValue float,
	@awardNameEnglish nvarchar(250),
	@awardValue decimal(18,10),
	@awardFee decimal(18,10),
	@AwardPercent decimal(18,10)
)
as begin 
		update award set AwardNameEnglish=@awardNameEnglish, AwardValue =@awardValue, AwardFee=@awardFee ,AwardPercent=@AwardPercent
		 where AwardID=@awardID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAwardFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateAwardFE]
(
	@awardName nvarchar(250),
	@awardValue float,
	@awardID int

)
as begin 
	update award set AwardName=@awardName,AwardValue=@awardValue where AwardID=@awardID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAwardMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAwardMegaball]
 (
	@numberID int,
	@awardID int,	
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fivethNumber int,
	@extraNumber int,
	@genCode nvarchar(500),
	@jackPotDefault float
 )
 as begin
	  DECLARE @AWARDDATEPREVIOUS DATETIME = null,
	        @JACKPOTPREVIOUS FLOAT = @JACKPOTDEFAULT,
	        @TOTALJACKPOT FLOAT = @JACKPOTDEFAULT,
			@JACKPOTWINNER FLOAT = 0;
	BEGIN  
	    --GET RESULT AWARD WITH CREATEDDATE LAST
		SELECT TOP 1 @AWARDDATEPREVIOUS = CREATEDATE,
		             @JACKPOTPREVIOUS = AWARDMEGABALL.JackPot
		FROM AWARDMEGABALL
		WHERE AWARDMEGABALL.ISDELETE = 0
		      AND AWARDMEGABALL.ISACTIVE = 1
			  ORDER BY CREATEDATE DESC;
	END;

	BEGIN  
		SELECT @TOTALJACKPOT = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
		FROM BOOKINGMEGABALL
		WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
		      AND NOT EXISTS (SELECT 1
			              FROM BOOKINGMEGABALL
						  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
								and (
									   BOOKINGMEGABALL.FirstNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
									   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.SecondNumber = @firstNumber 
									   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
									   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
									   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
									   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
									   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
									   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.FourthNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
									   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.FivethNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
									   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
									   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
									));
	END

	IF(@TOTALJACKPOT IS NULL)
	BEGIN
		--ELSE IF NOW HAVE LEAST 1 MEMBER WINNER -> TOTAL JACKPOT WILL BE DEFAULT (CONFIG SOURCES) 
		--@JACKPOTWINNER: Total jackpot won -> save history
	   BEGIN  
			SELECT @JACKPOTWINNER = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
			FROM BOOKINGMEGABALL
			WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
				  AND EXISTS (SELECT 1
							  FROM BOOKINGMEGABALL
							  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
									and (
									   BOOKINGMEGABALL.FirstNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
									   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
									)
									and (
										   BOOKINGMEGABALL.SecondNumber = @firstNumber 
										   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
										   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
										   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
										   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
										   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
										   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.FourthNumber = @firstNumber 
										   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
										   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
										   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
										   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
										   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.FivethNumber = @firstNumber 
										   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
										   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
										   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
										   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
										   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
										   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
										));
		END
		--set @JACKPOTWINNER = @JACKPOTPREVIOUS;
	   SET @TOTALJACKPOT = @jackPotDefault;
	END

	update AwardMegaball 
	set AwardID = @awardID,
	    FirstNumber = @firstNumber,
		SecondNumber = @secondNumber,
		ThirdNumber = @thirdNumber,
		FourthNumber = @fourthNumber,
		FivethNumber = @fivethNumber,
		ExtraNumber = @extraNumber, 
		GenCode = @genCode, 
		JackPot = @TOTALJACKPOT,
		JACKPOTWINNER = @JACKPOTWINNER
	where NumberID=@numberID

 end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAwardNumber]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateAwardNumber]
 (
	@numberID int,
	@awardID int,
	@numberValue float,
	@stationName nvarchar(250),
	@stationNameEnglish nvarchar(250)

 )
 as begin  
		update awardnumber set AwardID=@awardID,NumberValue=@numberValue,StationName=@stationName,StationNameEnglish=@stationNameEnglish
		 where NumberID=@numberID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateExchangePoint]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateExchangePoint]
 (
	@exchangeID int,
	@pointValue float,
	@bitcoinValue float,
	@createUser nvarchar(250),
	@updateUser nvarchar(250),
	@updateDate datetime
	
 )
 as begin
	update ExchangePoint set PointValue=@pointValue, BitcoinValue=@bitcoinValue, CreateUser=@createUser, UpdateUser=@updateUser,UpdateDate=@updateDate
	where ExchangeID=@exchangeID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateExchangeTicket]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateExchangeTicket]
 (
	@exchangeID int,
	@pointValue float,
	@ticketNumber int,
	@createUser nvarchar(250),
	@updateUser nvarchar(250),
	@updateDate datetime,
	@coinID char(10)
	
 )
 as begin
	update ExchangeTicket set PointValue=@pointValue, TicketNumber=@ticketNumber, CreateUser=@createUser, UpdateDate=@updateDate,UpdateUser=@updateUser,CoinID=@coinID
	where ExchangeID=@exchangeID
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateFreeTicketMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateFreeTicketMemberFE]
(
	@numberticketfree int,
	@memberID int
)
as begin
		update member set NumberTicketFree=@numberticketfree where MemberID=@memberID;
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateGroupAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateGroupAdmin]
(
		@groupName nvarchar(250),
		@groupID int
)
as begin 
		update groupadmin set GroupName=@groupName where GroupID=@groupID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateJackpotAwardMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateJackpotAwardMegaball]
 (
	@jackpot decimal(18,5)
 )
 as begin
	update AwardMegaball 
	set 
		JackPot = @jackpot
	where NumberID=(select top 1 AwardMegaball.NumberID from AwardMegaball order by NumberID desc, CreateDate asc)
 end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateMemberReferenceFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateMemberReferenceFE]
(
	@MemberID int,
	@LockID nvarchar(1000),
	@MemberIDReference varchar(1000),
	@Status int,
	@Amount decimal(18,5)
)
as begin 
		declare @TotalNumberTicket int;
		set @TotalNumberTicket = 0;
		set @TotalNumberTicket = (select total from FreeTicketDaily where IsActive = 1);

		update MemberReference set LockID = @LockID, Status = @Status,Amount=@Amount
		where MemberID = @MemberID and MemberIDReference = @MemberIDReference

		if(@Status = 0)
		begin
			if(@TotalNumberTicket > 0)
				begin
					update FreeTicketDaily set Total =@TotalNumberTicket - 1 where IsActive = 1;
				end;
		end;
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateNewPasswordMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateNewPasswordMemberFE]
(
	@newpassword nvarchar(250),
	@email nvarchar(250),
	@password nvarchar(250)
)
as begin 
	update member set Password=@newpassword where Email=@email and Password=@password
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePasswordMember]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePasswordMember]
(
	@password nvarchar(250),
	@fullname nvarchar(250)
)
as begin
		update member set Password=@password where Fullname=@fullname
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePasswordMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePasswordMemberFE]
(
	@password nvarchar(250),
	@email nvarchar(250)
)
as begin 
	update member set Password=@password where Email=@email
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdatePointMemberFE]
/*
	EDITED BY: LÝ KIẾN ĐỨC
	EDITED DATE: 25.01.2018
	DESCRIPTION: UPDATE JACKPOT POINT AwardMegaball
*/
(
	@Points numeric(18, 10),
	@MemberID int,
	@NumberID int,
	@TotalJackPot float
)
as begin 
	update Member set Points=@Points where MemberID=@MemberID;
	--IF(@@ROWCOUNT > 0)
	--BEGIN 
	--	--if query update success -> update jackpot AwardMegaball
	--	DECLARE @ISEXISTS INT;
	--	BEGIN
	--		SELECT @ISEXISTS = 1
	--		FROM AwardMegaball
	--		WHERE AwardMegaball.IsDelete = 0;
	--	END;
	--	IF(@ISEXISTS = 1)
	--		BEGIN
	--		    --IF TABLE AwardMegaball EXISTS LEAST 1 RECORD -> UPDATE JACKPOT
	--			UPDATE AwardMegaball
	--			SET AwardMegaball.JackPot = @TotalJackPot
	--			WHERE AwardMegaball.IsDelete = 0
	--			      AND EXISTS(SELECT 1
	--							FROM AwardMegaball AW
	--							WHERE AW.IsDelete = 0
	--								AND AwardMegaball.CreateDate >=AW.CreateDate
	--							)
	--		END
	--	ELSE 
	--		BEGIN
	--		    --IF TABLE AwardMegaball NOT EXISTS LEAST 1 RECORD -> INSERT 1 RECORD DEFAULT
	--			INSERT 
	--			INTO AWARDMEGABALL(CreateDate, IsDelete,ISACTIVE, JACKPOT)
	--				VALUES(SYSDATETIME(),0,0,@TotalJackPot)
	--		END;
		
	--END;
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointsMember]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePointsMember]
(
	@points float,
	@memberID int
)
as begin
		update member set Points=Points+@points where MemberID=@memberID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointsMember_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePointsMember_FE]
(
@memberID INT,
@points numeric(18, 10)
)
as begin 
	update member set Points=Points+@points where MemberID=@memberID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointsMemberFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdatePointsMemberFE]
(
@points numeric(18, 10),
@memberID int
)
as begin 
	update member set Points=Points+@points where MemberID=@memberID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePriorityTopic_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_UpdatePriorityTopic_CMS]
(
	@TopicID int,
	@Priority int
)
as 
begin
update topic
set [Priority]=@Priority
where topicid=@topicid;
end;

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusAdmin]
(
	@isDelete smallint,
	@adminID int
)
as begin
		update admin set IsDelete=@isDelete where AdminID=@adminID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBooking]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBooking]
(
	@status int,
	@bookingID int
)
as begin
		update booking set Status=@status where BookingID=@bookingID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBooking_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBooking_FE]
(
	@bookingID int,
	@status int
)
as begin 
	update bookingmegaball set Status=@status where BookingID=@bookingID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBookingFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBookingFE]
(
	@bookingID int,
	@status int
)
as begin 
	update booking set Status=@status where BookingID=@bookingID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBookingMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBookingMegaball]
(
	@status int,
	@bookingID int
)
as begin 
		update BookingMegaball set status =@status where BookingID=@bookingID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusExchangePoint]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusExchangePoint]
(
	@status int,
	@exchangeID int,
	@deleteUser nvarchar(250),
	@deleteDate datetime
)
as begin 
		update ExchangePoint set IsDelete =@status, DeleteUser=@deleteUser,DeleteDate=@deleteDate where ExchangeID=@exchangeID 
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusExchangeTicket]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateStatusExchangeTicket]
(
	@status int,
	@exchangeID int,
	@deleteUser nvarchar(250),
	@deleteDate datetime
)
as begin 
		update ExchangeTicket set IsDelete =@status, DeleteUser=@deleteUser,DeleteDate=@deleteDate where ExchangeID=@exchangeID 
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusGroupAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusGroupAdmin]
(
	@isActive smallint,
	@groupID int
)
as begin
			update groupadmin set IsActive=@isActive where GroupID=@groupID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusMember]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusMember]
(
	@memberID int
)
as begin 
	update member set IsActive=1 where MemberID=@memberID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusTransaction]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusTransaction]
(
	@status int,
	@transactionID int
)
as begin 
	update TransactionPackage set Status=@status where TransactionID=@transactionID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateUserNameAndPasswordAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateUserNameAndPasswordAdmin]
(
	@adminID int,
	@userName nvarchar(50),
	@password nvarchar(250)
)
as begin 
		update admin set UserName=@userName,Password=@password where AdminID=@adminID
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateUserNamedAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateUserNamedAdmin]
(
	@userName nvarchar(50),
	@adminID int
)
as begin
		update admin set UserName=@userName where AdminID=@adminID
end

GO
/****** Object:  StoredProcedure [dbo].[TransactionPackage_Insert]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPackage_Insert]   
    @MemberID int,
    @PackageID int,
    @Status int,
    @ExpireDate datetime,
    @CreateDate datetime,
    @TransactionCode nvarchar(50)
AS
BEGIN 
     SET NOCOUNT ON 
    insert into TransactionPackage(MemberID,PackageID,Status,ExpireDate,CreateDate,TransactionCode) values(@MemberID,@PackageID,@Status,@ExpireDate,@CreateDate,@TransactionCode) 
    
END

GO
/****** Object:  StoredProcedure [dbo].[TransactionPackage_Sel]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPackage_Sel]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select t.CreateDate, t.ExpireDate, t.MemberID, t.PackageID, t.Status, t.TransactionID, p.PackageName,t.TransactionCode from TransactionPackage t, Package p where t.PackageID = p.PackageID and MemberID = @MemberID order by CreateDate desc
END

GO
/****** Object:  StoredProcedure [dbo].[TransactionPackage_Sel_OrderByDate]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[TransactionPackage_Sel_OrderByDate]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select top 1 t.CreateDate, t.ExpireDate, t.MemberID, t.PackageID, t.Status, t.TransactionID, p.PackageName,t.TransactionCode from TransactionPackage t, Package p where t.PackageID = p.PackageID and MemberID = @MemberID and t.Status = 1 order by CreateDate desc
END

GO
/****** Object:  StoredProcedure [dbo].[TransactionPoint_Insert]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPoint_Insert]   
    @MemberID int,
    @Status int,
    @Points int,
    @CreateDate datetime,
    @TransactionCode nvarchar(50)
AS
BEGIN 
     SET NOCOUNT ON 
    insert into TransactionPoint(MemberID,Status,Points,CreateDate,TransactionCode) values(@MemberID,@Status,@Points,@CreateDate,@TransactionCode) 
    
END

GO
/****** Object:  StoredProcedure [dbo].[TransactionPoint_Sel]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPoint_Sel]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select t.MemberID, t.CreateDate, t.Points, t.Status, t.TransactionID, m.E_Wallet,t.TransactionCode from TransactionPoint t, Member m where t.MemberID = m.MemberID and m.MemberID = @MemberID order by CreateDate desc
END

GO
/****** Object:  StoredProcedure [dev].[SP_AwardMegaballLog_Add]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dev].[SP_AwardMegaballLog_Add]
 (
	@Email nvarchar(250),
	@Value decimal(18, 5),
	@CreatedDate datetime,
	@Id int out
 )
 as begin 
		Insert into AwardMegaballLog(Email, Value, CreatedDate)
		Values (@Email, @Value, @CreatedDate);

		select @Id = Scope_Identity();
 end
GO
/****** Object:  StoredProcedure [dev].[SP_BookingMegaball_AwardValue_Sel]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dev].[SP_BookingMegaball_AwardValue_Sel]
 (
	@DrawID int,
	@FirstNumber int,
	@SecondNumber int,
	@ThirdNumber int,
	@FourthNumber int, 
	@FivethNumber int,
	@ExtraNumber int
 )
 as begin 
		Select BM.MemberID,
		       BM.BookingID,
			   BM.DrawID,
			   BM.CreateDate BuyDate,
			   M.Email,
			   A.AwardValue,
			   A.AwardID,
			   A.NumberBallNormal,
			   A.NumberBallGold,
			   A.Priority,
			   A.AwardFee,
			   BM.FirstNumber FirstNumberBuy,
			   BM.SecondNumber SecondNumberBuy,
			   BM.ThirdNumber ThirdNumberBuy,
			   BM.FourthNumber FourthNumberBuy,
			   BM.FivethNumber FivethNumberBuy,
			   BM.ExtraNumber ExtraNumberBuy
		From (
				Select BookingMegaball.MemberID,
				       BookingMegaball.BookingID,
					   BookingMegaball.DrawID,
					   BookingMegaball.CreateDate,
					   BookingMegaball.FirstNumber,
					   BookingMegaball.SecondNumber,
					   BookingMegaball.ThirdNumber,
					   BookingMegaball.FourthNumber,
					   BookingMegaball.FivethNumber,
					   BookingMegaball.ExtraNumber,
					   Case
						  When BookingMegaball.FirstNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						  Else 0
					   End + 
					   Case
						  When BookingMegaball.SecondNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						  Else 0
					   End +
					   Case
						  When BookingMegaball.ThirdNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						  Else 0
					   End +
					   Case
						  When BookingMegaball.FourthNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						  Else 0
					   End +
					   Case
						  When BookingMegaball.FivethNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						  Else 0
					   End As NumNormal,
					   Case
						  When BookingMegaball.ExtraNumber = @ExtraNumber Then 1
						  Else 0
					   End as NumGold
				From BookingMegaball 
				Where BookingMegaball.DrawID = @DrawID
			) BM 
		join Award A on BM.NumNormal = A.NumberBallNormal AND BM.NumGold = A.NumberBallGold
		join Member M On M.MemberID = BM.MemberID;
 end
GO
/****** Object:  StoredProcedure [dev].[SP_CheckCoinID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE proc [dev].[SP_CheckCoinID]
 (
	@coinID nvarchar(250),
	@configID int
 )
 as begin 
		select CoinID 
		from TicketConfig 
		where CoinID=@coinID and IsActive=1
		and (@configID=0 or ConfigID!=@configID)
 end
GO
/****** Object:  StoredProcedure [dev].[SP_CheckConfigName]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create proc [dev].[SP_CheckConfigName]
  (
	@configName nvarchar(250),
	@configID int
 )
 as begin 
		select ConfigName 
		from TicketConfig 
		where ConfigName=@configName 
		and (@configID=0 or ConfigID!=@configID)
 end
GO
/****** Object:  StoredProcedure [dev].[SP_CheckCountryNameExits_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create proc [dev].[SP_CheckCountryNameExits_CMS]
 (@countryName nvarchar(250))
 as begin
	select CountryName from Country where CountryName=@countryName  and IsDeleted=0 and IsActive=1
 end
GO
/****** Object:  StoredProcedure [dev].[SP_CheckMobileExist]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_CheckMobileExist](@mobile char(15))
as begin 
	select Mobile from Member where Mobile=@mobile
end
GO
/****** Object:  StoredProcedure [dev].[SP_CheckMobileMemberExist]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create proc [dev].[SP_CheckMobileMemberExist](@mobile char(15), @email nvarchar(250))
 as begin 
	 select Mobile from Member where Mobile=@mobile and Email=@email
 end
GO
/****** Object:  StoredProcedure [dev].[SP_CheckOpenDateAwardMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_CheckOpenDateAwardMegaball]
  (
	@createDate datetime

  )
  as begin 
	select CreateDate from AwardMegaball where CreateDate=@createDate  
  end
GO
/****** Object:  StoredProcedure [dev].[SP_CheckPhoneZipCodeExists_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE proc [dev].[SP_CheckPhoneZipCodeExists_CMS]
 (@phoneZipCode char(10),@countryID int)
 as begin
	select PhoneZipCode from Country where PhoneZipCode=@phoneZipCode and IsDeleted=0 and IsActive=1 and CountryID!=@countryID
 end
GO
/****** Object:  StoredProcedure [dev].[SP_GetAllAwardWithDraw_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_GetAllAwardWithDraw_FE]
as begin 
	select *from AwardWithdraw where AwardWithdraw.IsDeleted=0
end
GO
/****** Object:  StoredProcedure [dev].[SP_GetListAwardDetail_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create proc [dev].[SP_GetListAwardDetail_CMS]
 (@awardID int)
 as begin
	select * from Award where AwardID=@awardID
 end
GO
/****** Object:  StoredProcedure [dev].[SP_GetListAwardMegaball_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_GetListAwardMegaball_FE]
as begin 
	select * from AwardMegaball where IsActive=1
end
GO
/****** Object:  StoredProcedure [dev].[SP_GetListCountry_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dev].[SP_GetListCountry_CMS]
(
		@start int,
		@end int
)
as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY c.CountryName Asc) as Row,SUM(1) OVER()AS TOTALROWS, c.* 
		from Country c where IsActive=1 and IsDeleted=0) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dev].[SP_GetListCountryBySearch_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_GetListCountryBySearch_CMS]
(
	@Keyword nvarchar(400),
	@start int,
	@end int
)
as begin
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY c.CreatedDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,c.* from Country c
where ((@Keyword is null) or (@Keyword='0') or c.CountryName like '%' + @Keyword + '%' ))
as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dev].[SP_GetListSystemConfig_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_GetListSystemConfig_CMS]
 as begin 
	select * from SystemConfig
 end
GO
/****** Object:  StoredProcedure [dev].[SP_GetTotalsSMSMember_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_GetTotalsSMSMember_FE](@email nvarchar(250),@totalsSMS int)
as begin 
	select * from Member where Email=@email and TotalSmsCodeInput=@totalsSMS
end
GO
/****** Object:  StoredProcedure [dev].[SP_InsertAwardWithdraw]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dev].[SP_InsertAwardWithdraw]
(
	@TransactionID char(50),
	@RequestWalletAddress nvarchar(250),
	@RequestMemberID nvarchar(250),
	@CoinIDWithdraw char(10),
	@RequestStatus int,
	@RequestDate datetime,
	@IsDeleted int,
	@AwardDate datetime,
	@ValuesWithdraw numeric(18, 8),
	@BookingID int	
)
as begin 
	insert into AwardWithdraw(TransactionID,RequestMemberID,RequestWalletAddress,RequestDate,RequestStatus,AwardDate,CoinIDWithdraw,ValuesWithdraw,AdminIDApprove,ApproveDate,ApproveNote,IsDeleted,UpdatedDate,UpdatedUser,DeletedDate,DeletedUser,BookingID)
	values(@TransactionID,@RequestMemberID,@RequestWalletAddress,@RequestDate,@RequestStatus,@AwardDate,@CoinIDWithdraw,@ValuesWithdraw,'','','',@IsDeleted,'','','','',@BookingID)
end
GO
/****** Object:  StoredProcedure [dev].[SP_InsertCountry_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_InsertCountry_CMS]
(
 	@CountryName nvarchar(250),
	@IsActive smallint,
	@IsDeleted smallint,
	@CreatedDate DateTime,
	@CreatedUser nvarchar(250),
	@PhoneZipCode varchar(50)
)
as begin 
	insert into Country(CountryName,PhoneZipCode,IsActive,IsDeleted,CreatedDate,CreatedUser,UpdatedDate,UpdatedUser,DeletedDate,DeletedUser) 
	values(@CountryName,@PhoneZipCode,@IsActive,@IsDeleted,@CreatedDate,@CreatedUser,'','','','')
end
GO
/****** Object:  StoredProcedure [dev].[SP_InsertTicketConfig_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_InsertTicketConfig_CMS]
(
 	@ConfigName nvarchar(250),
	@CreatedDate datetime,
	@NumberTicket int,
	@IsActive smallint,
	@IsDeleted smallint,
	@CoinID nvarchar(250),
	@CreatedUser nvarchar(250),
	@CoinValues numeric(18,10)
)
as begin 
		insert into TicketConfig(ConfigName,NumberTicket,CoinValues,CoinID,IsActive,IsDeleted,CreatedDate,CreatedUser,UpdatedDate,UpdatedUser,DeletedDate,DeletedUser)
		 values(@ConfigName,@NumberTicket,@CoinValues,@CoinID,@IsActive,@IsDeleted,@CreatedDate,@CreatedUser,'','','','')
end
GO
/****** Object:  StoredProcedure [dev].[SP_ListAllBookingMegaball_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_ListAllBookingMegaball_FE]
as begin
	select * from BookingMegaball
end
GO
/****** Object:  StoredProcedure [dev].[SP_ListBookingByDraw]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_ListBookingByDraw](@draw int)
as begin 
	select * from BookingMegaball where DrawID=@draw
end
GO
/****** Object:  StoredProcedure [dev].[SP_ListBookingByDraw_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_ListBookingByDraw_FE](@DrawID int)
as begin
	select * from BookingMegaball where DrawID=@DrawID
end
GO
/****** Object:  StoredProcedure [dev].[SP_ListBookingMegaballByDraw]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dev].[SP_ListBookingMegaballByDraw]
(@DrawID int,@start int, @end int)
as begin
    select * from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email
	 from bookingmegaball b left join member m on b.MemberID=m.MemberID and b.DrawID = @DrawID) as Products  where Row>=@start and Row<=@end
		 
end
GO
/****** Object:  StoredProcedure [dev].[SP_ListBookingWinningBySearch_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dev].[SP_ListBookingWinningBySearch_FE]
(
	@memberID int,
	@start int,
	@end int,
	@fromDate Datetime,
	@todate Datetime
)
as begin 
	select * from(SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,
	 b.*,m.Email,m.E_Wallet,
	 dbo.GetAwardWithdrawRequestStatus(m.Email,b.BookingID,b.OpenDate) as AwardWithdrawStatus
	  from BookingMegaball b left join member m on b.MemberID=m.MemberID
	  where b.MemberID=@memberID and b.Status = 1  and (@fromDate IS NULL or b.CreateDate between @fromDate and @toDate)
	 ) as product
	where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dev].[SP_ListTransactionCoinID]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_ListTransactionCoinID]
(
	@start int,
	@end int,
	@fromDate Datetime,
	@toDate Datetime
)
as begin 
select * from (
							SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,
							tc.* from TransactionCoin tc left join member m on tc.MemberID=m.MemberID
												left join Coin c on tc.CoinID=c.CoinID 
							where (@fromDate= null or m.CreateDate between @fromDate and @toDate)
							)
as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dev].[SP_ListTransactionWalletCoinFE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dev].[SP_ListTransactionWalletCoinFE]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime,
	@coinID nchar(10)
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,m.Email,m.E_Wallet 
from TransactionCoin tp left join Coin c on tp.CoinID= c.CoinID
						 left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and(tp.CoinID=@coinID)
and (@fromDate ='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
and status=0
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dev].[SP_LockAndLockConfigTicket_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE proc [dev].[SP_LockAndLockConfigTicket_CMS]
 (
	@configID int,
	@isActive smallint
 )
 as begin
		update TicketConfig set IsActive=@isActive where ConfigID=@configID
 end
 
GO
/****** Object:  StoredProcedure [dev].[SP_LockAndUnlockCountry_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create proc [dev].[SP_LockAndUnlockCountry_CMS](
	@CountryID int,
	@IsActive smallint
 )
 as begin
		update Country set IsActive=@IsActive where CountryID=@CountryID
 end
GO
/****** Object:  StoredProcedure [dev].[SP_TicketNumberTimeLog_Add]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dev].[SP_TicketNumberTimeLog_Add]
 (
	@Email nvarchar(250),
	@TicketNumber int,
	@CreatedDate datetime,
	@Id int out
 )
 as begin 
		Insert into TicketNumberTimeLog(Email, TicketNumber, CreatedDate)
		Values (@Email, @TicketNumber, @CreatedDate);

		select @Id = Scope_Identity();
 end
GO
/****** Object:  StoredProcedure [dev].[SP_TicketWinning_Add]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dev].[SP_TicketWinning_Add]
 (
	@BookingID int,
	@DrawID int,
	@BuyDate DateTime,
	@AwardID int,
	@FirstNumberBuy int,
	@SecondNumberBuy int,
	@ThirdNumberBuy int,
	@FourthNumberBuy int, 
	@FivethNumberBuy int,
	@ExtraNumberBuy int,
	@FirstNumberAward int,
	@SecondNumberAward int,
	@ThirdNumberAward int,
	@FourthNumberAward int, 
	@FivethNumberAward int,
	@ExtraNumberAward int,
	@NumberBallNormal int,
	@NumberBallGold int, 
	@AwardValue decimal(18, 5),
	@AwardDate DateTime,
	@MemberID int,
	@Priority int,
	@AwardFee decimal(18, 8),
	@TicketWinningID int out
 )
 as begin 
		Insert into TicketWinning(BookingID, 
									DrawID, 
									BuyDate, 
									AwardID, 
									FirstNumberBuy, 
									SecondNumberBuy, 
									ThirdNumberBuy, 
									FourthNumberBuy, 
									FivethNumberBuy, 
									ExtraNumberBuy,
									FirstNumberAward,
									SecondNumberAward,
									ThirdNumberAward,
									FourthNumberAward, 
									FivethNumberAward,
									ExtraNumberAward,
									NumberBallNormal,
									NumberBallGold, 
									AwardValue,
									AwardDate,
									MemberID,
									Priority,
									AwardFee)
		Values(
				@BookingID, 
				@DrawID, 
				@BuyDate, 
				@AwardID, 
				@FirstNumberBuy, 
				@SecondNumberBuy, 
				@ThirdNumberBuy, 
				@FourthNumberBuy, 
				@FivethNumberBuy, 
				@ExtraNumberBuy,
				@FirstNumberAward,
				@SecondNumberAward,
				@ThirdNumberAward,
				@FourthNumberAward, 
				@FivethNumberAward,
				@ExtraNumberAward,
				@NumberBallNormal,
				@NumberBallGold, 
				@AwardValue,
				@AwardDate,
				@MemberID,
				@Priority,
				@AwardFee
			  );

	Select @TicketWinningID = Scope_Identity(); 
 end
GO
/****** Object:  StoredProcedure [dev].[SP_UpdateCountry_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_UpdateCountry_CMS]
(
    @CountryID int,
 	@CountryName nvarchar(250),
	@IsActive smallint,
	@IsDeleted smallint,
 	@UpdatedDate DateTime,
	@UpdatedUser nvarchar(250),
	@PhoneZipCode varchar(50)
)
as begin 
	Update Country set CountryName=@CountryName, PhoneZipCode=@PhoneZipCode,IsActive=@IsActive,IsDeleted=@IsDeleted,UpdatedDate=@UpdatedDate,UpdatedUser=@UpdatedUser
	Where CountryID=@CountryID and IsActive=1 and IsDeleted=0
end
GO
/****** Object:  StoredProcedure [dev].[SP_UpdateInformationMember_FE]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dev].[SP_UpdateInformationMember_FE]
(
	@email nvarchar(250),
	@mobile char(15),
	@smsCode nvarchar(50),
	@isIndentifySms int,
	@expireSmSCode datetime,
	@totalSmsCodeInput int
)
as begin
	update Member set Mobile=@mobile, SmsCode=@smsCode,IsIndentifySms=@isIndentifySms,TotalSmsCodeInput=@totalSmsCodeInput,ExpireSmSCode=@expireSmSCode
	where Email=@email
end
GO
/****** Object:  StoredProcedure [dev].[SP_UpdateStatusActive_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_UpdateStatusActive_CMS]
(
	@isAutoLottery smallint,
	@configID int
)
as begin 
	update SystemConfig set IsAutoLottery=@isAutoLottery where ConfigID=@configID
end
GO
/****** Object:  StoredProcedure [dev].[SP_UpdateStatusAwardWithdraw_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dev].[SP_UpdateStatusAwardWithdraw_CMS]
(
	@TransactionID char(50),
	@RequestStatus smallint,
	@AdminIDApprove nvarchar(250),
	@UpdatedUser nvarchar(250)
)
as
begin
update AwardWithdraw
set RequestStatus=@RequestStatus,
AdminIDApprove=@AdminIDApprove,
UpdatedUser=@UpdatedUser,
UpdatedDate=GETDATE(),
ApproveDate=GETDATE()
where TransactionID=@TransactionID
and IsDeleted=0;
end;
GO
/****** Object:  StoredProcedure [dev].[SP_UpdateTicketConfig_CMS]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dev].[SP_UpdateTicketConfig_CMS]
  (
	@ConfigName nvarchar(250),
	@ConfigID int,
	@NumberTicket int,
	@CoinValues numeric(18,10),
	@CoinID nvarchar(250),
	@UpdateDate Datetime
 )
 as begin  
		update TicketConfig set CoinID=@CoinID,NumberTicket=@NumberTicket,CoinValues=@CoinValues,ConfigName=@ConfigName, UpdatedDate=@UpdateDate
		 where ConfigID=@ConfigID
 end
GO
/****** Object:  StoredProcedure [guest].[SP_CheckCoinIDExist]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_CheckCoinIDExist]
(
	@coinID nchar(10),
	@exchangeID int
)
as begin 
	select * from ExchangeTicket where CoinID=@coinID and (@exchangeID=0 or ExchangeID!=@exchangeID) and IsActive =1 and IsDelete =0
end

GO
/****** Object:  StoredProcedure [guest].[SP_CountAllAwardMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_CountAllAwardMegaball]
as begin 
	select COUNT(aw.AwardID) as TotalAllAward,
	guest.fc_CountNewAward() as TotalNewAward 
	from AwardMegaball aw
	where aw.IsActive=1 and aw.IsDelete=0;
end

GO
/****** Object:  StoredProcedure [guest].[SP_GetListAwardMegaballResultByDate]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_GetListAwardMegaballResultByDate]
 (
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,
		aw.*,a.AwardName,a.AwardNameEnglish from awardmegaball aw left join award a on aw.AwardID=a.AwardID 
		where aw.CreateDate between @fromDate and @toDate ) as Products  where Row>=@start and Row<=@end
 end

GO
/****** Object:  StoredProcedure [guest].[SP_ListAllAdminPaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_ListAllAdminPaging]
(   @start int,
	@end int)
as begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY a.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,a.*,g.GroupName
		 from Admin a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID)
		 as Products  where Row>=@start and Row<=@end

end

GO
/****** Object:  StoredProcedure [guest].[SP_ListAllCoin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_ListAllCoin]
as begin
	select * from Coin where IsActive =1
end

GO
/****** Object:  StoredProcedure [guest].[SP_SearchMemberByBookingMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [guest].[SP_SearchMemberByBookingMegaball]
(
	@Keyword nvarchar(400),
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
	)
as begin 
select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,b.*,m.Email 
	 from BookingMegaball b left join Member m on b.MemberID=m.MemberID
	-- where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	--) as product
	--where Row>=@start and Row<=@end
    where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)) as product
	where Row>=@start and Row<=@end
end

GO
/****** Object:  StoredProcedure [guest].[SP_SearchMemberNoPaging]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_SearchMemberNoPaging]
(
	@Keyword nvarchar(400))
as begin 
	select * from Member m

    where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
end

GO
/****** Object:  UserDefinedFunction [dbo].[CountNewBooking]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewBooking]()
returns int
as
begin
declare @numberBooking int
set @numberBooking=(select COUNT(b.BookingID) from Booking b
where DATEADD(DD, DATEDIFF(DD, 0, b.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberBooking);
end;

GO
/****** Object:  UserDefinedFunction [dbo].[CountNewBookingMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewBookingMegaball]()
returns int
as begin 
	declare @numberBookingMegaball int
	set @numberBookingMegaball=(select COUNT(b.BookingID) from BookingMegaball b
	where DATEADD(DD,DATEDIFF(DD,0,b.CreateDate),0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0));
	return(@numberBookingMegaball);
end;

GO
/****** Object:  UserDefinedFunction [dbo].[CountNewCoin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewCoin]()
returns int
as
begin
declare @numberCoin int
set @numberCoin=(select COUNT(c.TransactionID) from TransactionCoin c
where DATEADD(DD, DATEDIFF(DD, 0, c.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberCoin);
end;

GO
/****** Object:  UserDefinedFunction [dbo].[CountNewMember]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewMember]()
returns int
as
begin
declare @numberMember int
set @numberMember=(select COUNT(m.MemberID) from Member m
where DATEADD(DD, DATEDIFF(DD, 0, m.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberMember);
end;

GO
/****** Object:  UserDefinedFunction [dbo].[CountNewPackage]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewPackage]()
returns int
as
begin
declare @numberPackage int
set @numberPackage=(select COUNT(p.TransactionID) from TransactionPackage p
where DATEADD(DD, DATEDIFF(DD, 0, p.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberPackage);
end;

GO
/****** Object:  UserDefinedFunction [dbo].[CountNewPoint]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewPoint]()
returns int
as
begin
declare @numberPoints int
set @numberPoints=(select COUNT(p.TransactionID) from TransactionPoint p
where DATEADD(DD, DATEDIFF(DD, 0, p.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberPoints);
end;

GO
/****** Object:  UserDefinedFunction [dbo].[GetAwardWithdrawRequestStatus]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[GetAwardWithdrawRequestStatus]
(
	@MemberID nvarchar(250),
	@BookingID int,
	@AwardDate datetime
)
returns int
as
begin
declare @status int;
set @status = (select top 1 RequestStatus from AwardWithdraw
where convert(varchar(10), AwardDate, 101)=convert(varchar(10), @AwardDate, 101)
and RequestMemberID=@MemberID
and IsDeleted=0
and BookingID=@BookingID);
if @status is null 
begin
 set @status=-1
 end;
return @status;
end;



GO
/****** Object:  UserDefinedFunction [guest].[fc_CountNewAward]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [guest].[fc_CountNewAward]()
returns int
as begin
	declare @numberAward int
	set @numberAward=(select COUNT(aw.AwardID) from AwardMegaball aw
	where DATEADD(DD, DATEDIFF(DD, 0, aw.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) 
	); 
	return(@numberAward);
end;

GO
/****** Object:  Table [dbo].[AccessRight]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccessRight](
	[GroupID] [int] NOT NULL,
	[AdminID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Admin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Admin](
	[AdminID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](250) NULL,
	[Hashkey] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[FullName] [nvarchar](250) NULL,
	[Email] [nvarchar](250) NULL,
	[Mobile] [char](50) NULL,
	[Address] [nvarchar](250) NULL,
	[Avatar] [nvarchar](250) NULL,
	[IsDelete] [smallint] NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AuthenticateLink]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthenticateLink](
	[AuthenticateID] [int] IDENTITY(1,1) NOT NULL,
	[LinkAuthenticate] [nvarchar](1000) NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[Note] [nvarchar](3500) NULL,
	[CreateUser] [datetime] NULL,
 CONSTRAINT [PK_AuthenticateLink] PRIMARY KEY CLUSTERED 
(
	[AuthenticateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Award]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Award](
	[AwardID] [int] IDENTITY(1,1) NOT NULL,
	[AwardName] [nvarchar](250) NULL,
	[AwardValue] [decimal](18, 4) NULL,
	[CreateDate] [datetime] NULL,
	[Priority] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[AwardNameEnglish] [nvarchar](250) NULL,
	[NumberBallGold] [int] NULL,
	[NumberBallNormal] [int] NULL,
	[AwardFee] [decimal](18, 8) NULL,
	[AwardPercent] [float] NULL,
 CONSTRAINT [PK_Award] PRIMARY KEY CLUSTERED 
(
	[AwardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AwardMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AwardMegaball](
	[NumberID] [int] IDENTITY(1,1) NOT NULL,
	[AwardID] [int] NULL,
	[CreateDate] [datetime] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[FirstNumber] [int] NULL,
	[SecondNumber] [int] NULL,
	[ThirdNumber] [int] NULL,
	[FourthNumber] [int] NULL,
	[FivethNumber] [int] NULL,
	[ExtraNumber] [int] NULL,
	[Priority] [int] NULL,
	[GenCode] [nvarchar](500) NULL,
	[JackPot] [float] NULL,
	[JackPotWinner] [float] NULL,
	[DrawID] [int] NULL,
	[TotalWon] [decimal](18, 5) NULL,
 CONSTRAINT [PK_AwardMegaball] PRIMARY KEY CLUSTERED 
(
	[NumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AwardNumber]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AwardNumber](
	[NumberID] [int] IDENTITY(1,1) NOT NULL,
	[AwardID] [int] NULL,
	[NumberValue] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[Priority] [int] NULL,
	[StationName] [nvarchar](250) NULL,
	[StationNameEnglish] [nvarchar](250) NULL,
	[Gencode] [nvarchar](500) NULL,
 CONSTRAINT [PK_AwardNumber] PRIMARY KEY CLUSTERED 
(
	[NumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AwardWithdraw]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AwardWithdraw](
	[TransactionID] [char](50) NOT NULL,
	[RequestMemberID] [nvarchar](250) NULL,
	[RequestWalletAddress] [nvarchar](250) NULL,
	[RequestDate] [datetime] NULL,
	[RequestStatus] [smallint] NULL,
	[AwardDate] [datetime] NULL,
	[CoinIDWithdraw] [char](10) NULL,
	[ValuesWithdraw] [numeric](18, 8) NULL,
	[AdminIDApprove] [nvarchar](250) NULL,
	[ApproveDate] [datetime] NULL,
	[ApproveNote] [nvarchar](2000) NULL,
	[IsDeleted] [smallint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
	[BookingID] [int] NULL,
 CONSTRAINT [PK_AwardWithdraw] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Booking]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Booking](
	[BookingID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[NumberValue] [varchar](50) NULL,
	[Quantity] [int] NULL,
	[CreateDate] [datetime] NULL,
	[OpenDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](2500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
 CONSTRAINT [PK_Booking] PRIMARY KEY CLUSTERED 
(
	[BookingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BookingMegaball]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BookingMegaball](
	[BookingID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[CreateDate] [datetime] NULL,
	[OpenDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](2500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
	[FirstNumber] [int] NULL,
	[SecondNumber] [int] NULL,
	[ThirdNumber] [int] NULL,
	[FourthNumber] [int] NULL,
	[FivethNumber] [int] NULL,
	[ExtraNumber] [int] NULL,
	[Quantity] [int] NULL,
	[ValuePoint] [numeric](18, 10) NULL,
	[DrawID] [int] NULL,
	[IsFreeTicket] [smallint] NULL,
 CONSTRAINT [PK_BookingMegaball] PRIMARY KEY CLUSTERED 
(
	[BookingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Coin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coin](
	[CoinID] [char](10) NOT NULL,
	[CoinName] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[CoinSymbol] [varchar](50) NULL,
 CONSTRAINT [PK_Coin] PRIMARY KEY CLUSTERED 
(
	[CoinID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CompanyInfomation]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyInfomation](
	[MemberID] [int] NOT NULL,
	[CompanyName] [nvarchar](500) NULL,
	[Address] [nvarchar](500) NULL,
	[Phone] [char](50) NULL,
	[Email] [nvarchar](250) NULL,
	[Website] [nvarchar](500) NULL,
	[Description] [nvarchar](4000) NULL,
 CONSTRAINT [PK_CompanyInfomation] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Country]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[CountryID] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](250) NULL,
	[PhoneZipCode] [char](10) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Draw]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Draw](
	[DrawID] [int] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Draw] PRIMARY KEY CLUSTERED 
(
	[DrawID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExchangePoint]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExchangePoint](
	[ExchangeID] [int] IDENTITY(1,1) NOT NULL,
	[PointValue] [float] NULL,
	[BitcoinValue] [float] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[MemberID] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_ExchangePoint] PRIMARY KEY CLUSTERED 
(
	[ExchangeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExchangeTicket]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExchangeTicket](
	[ExchangeID] [int] IDENTITY(1,1) NOT NULL,
	[PointValue] [float] NULL,
	[TicketNumber] [int] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[MemberID] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [date] NULL,
	[DeleteUser] [nvarchar](250) NULL,
	[CoinID] [nchar](10) NULL,
 CONSTRAINT [PK_ExchangeTicket] PRIMARY KEY CLUSTERED 
(
	[ExchangeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FreeTicketDaily]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FreeTicketDaily](
	[FreeTicketID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[Total] [int] NULL,
	[IsActive] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_FreeTicket] PRIMARY KEY CLUSTERED 
(
	[FreeTicketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupAdmin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupAdmin](
	[GroupID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
 CONSTRAINT [PK_GroupAdmin] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IPConfig]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IPConfig](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](50) NULL,
	[ServiceDomain] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_IPConfig] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LinkResetPassword]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LinkResetPassword](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[LinkReset] [nvarchar](1000) NULL,
	[Status] [smallint] NULL,
	[NumberSend] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ExpireLink] [datetime] NULL,
	[IPAddress] [nvarchar](50) NULL,
 CONSTRAINT [PK_LinkResetPassword] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Member]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Member](
	[MemberID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NULL,
	[E_Wallet] [nvarchar](250) NULL,
	[Password] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[Points] [numeric](18, 10) NULL,
	[CreateDate] [datetime] NULL,
	[FullName] [nvarchar](250) NULL,
	[Mobile] [char](15) NULL,
	[Avatar] [nvarchar](250) NULL,
	[Gender] [smallint] NULL,
	[Birthday] [datetime] NULL,
	[ClientID] [int] NULL,
	[IsCompany] [smallint] NULL,
	[IsUserType] [smallint] NULL,
	[UserTypeID] [varchar](100) NULL,
	[MemberIDSync] [varchar](50) NULL,
	[LinkLogin] [nvarchar](500) NULL,
	[NumberTicketFree] [int] NULL,
	[LinkReference] [nvarchar](500) NULL,
	[SmsCode] [nvarchar](50) NULL,
	[IsIndentifySms] [smallint] NULL,
	[ExpireSmSCode] [datetime] NULL,
	[TotalSmsCodeInput] [int] NULL,
 CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Member_Wallet]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Member_Wallet](
	[WalletID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[IndexWallet] [int] NULL,
	[NumberCoin] [float] NULL,
	[IsActive] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [nvarchar](250) NULL,
	[IsDelete] [smallint] NULL,
	[WalletAddress] [nvarchar](250) NULL,
	[RippleAddress] [nvarchar](250) NULL,
	[SerectKeyRipple] [nvarchar](250) NULL,
 CONSTRAINT [PK_Member_Wallet] PRIMARY KEY CLUSTERED 
(
	[WalletID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MemberReference]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemberReference](
	[MemberID] [int] NOT NULL,
	[LinkReference] [nvarchar](250) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[MemberIDReference] [int] NOT NULL,
	[LockID] [nvarchar](250) NULL,
	[Status] [smallint] NULL,
	[Amount] [decimal](18, 5) NULL,
 CONSTRAINT [PK_MemberReference] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC,
	[MemberIDReference] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationSystem]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationSystem](
	[NotificationID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](4000) NULL,
	[SenderID] [int] NULL,
	[ReceiveMemberID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Status] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[TypeNotificationID] [int] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_NotificationSystem] PRIMARY KEY CLUSTERED 
(
	[NotificationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Package]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Package](
	[PackageID] [int] IDENTITY(1,1) NOT NULL,
	[PackageName] [nvarchar](250) NULL,
	[PackageValue] [float] NULL,
	[CreateDate] [datetime] NULL,
	[NumberExpire] [int] NULL,
	[IsActive] [smallint] NULL,
	[Priority] [int] NULL,
	[PackageNameEnglish] [nvarchar](250) NULL,
 CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED 
(
	[PackageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PackageCoin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackageCoin](
	[PackageID] [int] NOT NULL,
	[CoinID] [nchar](10) NOT NULL,
	[PackageValue] [numeric](18, 2) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StoreFreeTicket]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreFreeTicket](
	[StoreFreeTicketID] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [int] NULL,
 CONSTRAINT [PK_StoreFreeTicket] PRIMARY KEY CLUSTERED 
(
	[StoreFreeTicketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemConfig]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemConfig](
	[ConfigID] [int] IDENTITY(1,1) NOT NULL,
	[IsAutoLottery] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_SystemConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TicketConfig]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketConfig](
	[ConfigID] [int] IDENTITY(1,1) NOT NULL,
	[ConfigName] [nvarchar](250) NOT NULL,
	[NumberTicket] [int] NOT NULL,
	[CoinValues] [decimal](18, 10) NOT NULL,
	[CoinID] [nchar](10) NOT NULL,
	[IsActive] [smallint] NOT NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TicketConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TicketWinning]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketWinning](
	[TicketWinningID] [int] IDENTITY(1,1) NOT NULL,
	[BookingID] [int] NULL,
	[DrawID] [int] NULL,
	[BuyDate] [datetime] NULL,
	[Status] [int] NULL,
	[AwardID] [int] NULL,
	[FirstNumberBuy] [int] NULL,
	[SecondNumberBuy] [int] NULL,
	[ThirdNumberBuy] [int] NULL,
	[FourthNumberBuy] [int] NULL,
	[FivethNumberBuy] [int] NULL,
	[ExtraNumberBuy] [int] NULL,
	[FirstNumberAward] [int] NULL,
	[SecondNumberAward] [int] NULL,
	[ThirdNumberAward] [int] NULL,
	[FourthNumberAward] [int] NULL,
	[FivethNumberAward] [int] NULL,
	[ExtraNumberAward] [int] NULL,
	[NumberBallNormal] [int] NULL,
	[NumberBallGold] [int] NULL,
	[AwardValue] [decimal](18, 5) NULL,
	[AwardDate] [datetime] NULL,
	[MemberID] [int] NULL,
	[Priority] [int] NULL,
	[AwardFee] [decimal](18, 8) NULL,
 CONSTRAINT [PK_TicketWinning] PRIMARY KEY CLUSTERED 
(
	[TicketWinningID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TransactionCoin]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionCoin](
	[TransactionID] [char](150) NOT NULL,
	[WalletAddressID] [nvarchar](500) NOT NULL,
	[MemberID] [int] NOT NULL,
	[ValueTransaction] [numeric](18, 10) NOT NULL,
	[QRCode] [nvarchar](500) NULL,
	[CreateDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[Status] [smallint] NULL,
	[Note] [nvarchar](2500) NULL,
	[WalletID] [nvarchar](500) NULL,
	[TypeTransactionID] [int] NULL,
	[TransactionBitcoin] [nvarchar](500) NULL,
	[CoinID] [nchar](10) NULL,
 CONSTRAINT [PK_TransactionCoin] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransactionFee]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionFee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionTypeID] [int] NULL,
	[Fee] [numeric](18, 8) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TransactionFee] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TransactionPackage]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionPackage](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[PackageID] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](3500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
 CONSTRAINT [PK_TransactionPackage] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransactionPoint]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionPoint](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[Points] [int] NULL,
	[CreateDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](2500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
	[TransactionBitcoin] [nvarchar](500) NULL,
	[CoinID] [nchar](10) NULL,
 CONSTRAINT [PK_TransactionPoint] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TypeConfig]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeConfig](
	[TypeConfigID] [int] IDENTITY(1,1) NOT NULL,
	[TypeConfigName] [nvarchar](250) NOT NULL,
	[IsActive] [smallint] NOT NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TypeConfig] PRIMARY KEY CLUSTERED 
(
	[TypeConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TypeNotification]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeNotification](
	[TypeNotificationID] [int] IDENTITY(1,1) NOT NULL,
	[TypeNotificationName] [nvarchar](500) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TypeNotification] PRIMARY KEY CLUSTERED 
(
	[TypeNotificationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TypeTransaction]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeTransaction](
	[TypeTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[TypeTransactionName] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
 CONSTRAINT [PK_TypeTransaction] PRIMARY KEY CLUSTERED 
(
	[TypeTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Wallet_Address]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wallet_Address](
	[WalletAddressID] [int] IDENTITY(1,1) NOT NULL,
	[WalletID] [int] NOT NULL,
	[PrivateKey] [nvarchar](500) NULL,
	[IndexWallet] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_Wallet_Address] PRIMARY KEY CLUSTERED 
(
	[WalletAddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dev].[AwardMegaballLog]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dev].[AwardMegaballLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[Value] [decimal](18, 5) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AwardMegaballLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dev].[TicketNumberTimeLog]    Script Date: 7/27/2018 11:02:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dev].[TicketNumberTimeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[TicketNumber] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TicketNumberTimeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_NumberBallGold]  DEFAULT ((0)) FOR [NumberBallGold]
GO
ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_NumberBallNormal]  DEFAULT ((0)) FOR [NumberBallNormal]
GO
ALTER TABLE [dbo].[AwardMegaball] ADD  CONSTRAINT [DF_AwardMegaball_JackPotWinner]  DEFAULT ((0)) FOR [JackPotWinner]
GO
ALTER TABLE [dbo].[BookingMegaball] ADD  CONSTRAINT [DF_BookingMegaball_IsFreeTicket]  DEFAULT ((0)) FOR [IsFreeTicket]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[IPConfig] ADD  CONSTRAINT [DF_IPConfig_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Member] ADD  CONSTRAINT [DF_Member_IsLoginType]  DEFAULT ((0)) FOR [IsUserType]
GO
ALTER TABLE [dbo].[Member] ADD  CONSTRAINT [DF_Member_NumberTicketFree]  DEFAULT ((0)) FOR [NumberTicketFree]
GO
ALTER TABLE [dbo].[SystemConfig] ADD  CONSTRAINT [DF_SystemConfig_IsAutoLottery]  DEFAULT ((0)) FOR [IsAutoLottery]
GO
ALTER TABLE [dbo].[TicketConfig] ADD  CONSTRAINT [DF_TicketConfig_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TicketWinning] ADD  CONSTRAINT [DF_TicketWinning_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[TypeConfig] ADD  CONSTRAINT [DF_TypeConfig_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total JackPot previous won' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AwardMegaball', @level2type=N'COLUMN',@level2name=N'JackPotWinner'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Login normal, 1: Login facebook, 2: Login google+' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'IsUserType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FacebookID or GoogleID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'UserTypeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ma Giao dich coin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'TransactionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dia chi vi cua nguoi muon giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'WalletAddressID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nguoi giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gia tri giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'ValueTransaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'QR code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'QRCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ngay giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'CreateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Thoi gian het han giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'ExpireDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Trang thai giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ghi chu giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vi cua nguoi nhan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'WalletID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dia chi id cua vi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Wallet_Address', @level2type=N'COLUMN',@level2name=N'WalletAddressID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID cua vi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Wallet_Address', @level2type=N'COLUMN',@level2name=N'WalletID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ticket Number Of Times' , @level0type=N'SCHEMA',@level0name=N'dev', @level1type=N'TABLE',@level1name=N'AwardMegaballLog', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ticket Number Of Times' , @level0type=N'SCHEMA',@level0name=N'dev', @level1type=N'TABLE',@level1name=N'TicketNumberTimeLog', @level2type=N'COLUMN',@level2name=N'TicketNumber'
GO
