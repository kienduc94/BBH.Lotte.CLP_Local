﻿using BBC.Core.WebService;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBC.CWallet.ServiceListener.ServiceContract.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Wallet.Repository
{
    public class AddressRepository : WCFClient<IAddressServices>, IAddressServices
    {
        public AddressInfo Create(AddressCreationRequest objInput)
        {
            return Proxy.Create(objInput);
        }

        public AddressInfo Get(AddressInfoRequest objInput)
        {
            return Proxy.Get(objInput);
        }

        public CWalletList<AddressInfo> List(SystemFilterRequest objInput)
        {
            return Proxy.List(objInput);
        }

        public CWalletList<AddressEventInfo> ListEventsHistory(AddressEventFilterRequest objInput)
        {
            return Proxy.ListEventsHistory(objInput);
        }

        public AddressEventInfo Lock(AddressLockRequest objInput)
        {
            return Proxy.Lock(objInput);
        }

        public AddressEventInfo UnLock(AddressLockRequest objInput)
        {
            return Proxy.UnLock(objInput);
        }
    }
}
