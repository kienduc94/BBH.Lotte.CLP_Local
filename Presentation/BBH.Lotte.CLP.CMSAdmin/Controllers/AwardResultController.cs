﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BBH.Lotte.CLP.Repository;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Microsoft.Practices.Unity;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CMSAdmin.Models;
using System.Text.RegularExpressions;
using BBH.Lotte.CLP.Shared;
using BBC.Core.Database;
using BBC.Core.Common.Utils;
using BBH.Lotte.CLP.CMSAdmin.Models;
using System.Configuration;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class AwardResultController : Controller
    {
        //
        // GET: /AwardResult/

        [Dependency]
        protected IAwardServices repository { get; set; }

        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }

        int groupIDAdmin = 0;

        [Authorization]
        public ActionResult Index()
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            //if (Session[SessionKey.SESSION_USERNAME] == null)
            //{
            //    Response.Redirect("/login");
            //}

            int d = DateTime.Now.Day;
            int m = DateTime.Now.Month;
            int y = DateTime.Now.Year;
            string fromD = m + "/" + d + "/" + y + " 00:00:00";
            string toD = m + "/" + d + "/" + y + " 23:59:00";
            try
            {
                fromDate = DateTime.Parse(fromD);
                toDate = DateTime.Parse(toD);
            }
            catch
            {
                fromD = d + "/" + m + "/" + y + " 00:00:00";
                toD = d + "/" + m + "/" + y + " 23:59:00";
                fromDate = DateTime.Parse(fromD);
                toDate = DateTime.Parse(toD);
            }
            IEnumerable<AwardBO> lstAward = repository.GetListAward(1, 100);           
            ViewData[ViewDataKey.VIEWDATA_LISTAWARD] = lstAward;

            IEnumerable<AwardNumberBO> lstAwardNumber = repository.GetListAwardNumberByDate(1, 200, fromDate, toDate);
            ViewData[ViewDataKey.VIEWDATA_LISTAWARDNUMBER] = lstAwardNumber;

            IEnumerable<WinnerBO> lstWinnerAward = repository.GetListWinner(1, 10000, fromDate, toDate);
            ViewData[ViewDataKey.VIEWDATA_LISTWINERAWARD] = lstWinnerAward;

            return View();
        }
        [Authorization]
        [HttpPost]
        public string SaveAwardNumber(int numberID, int awardID, string numberValue)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            AwardNumberBO award = new AwardNumberBO();

            if (numberID > 0)
            {
                award.AwardID = awardID;
                award.NumberID = numberID;
                award.NumberValue = numberValue;
                award.StationName =string.Empty;
                award.StationNameEnglish = string.Empty;
                bool rs = repository.UpdateAwardNumber(award);
                if (rs)
                {
                    result = "success";
                }
            }
            else if (numberID == 0)
            {
                award.IsActive = 1;
                award.NumberID = numberID;
              
                    award.NumberValue = numberValue;
               
                award.IsDelete = 0;
                award.CreateDate = DateTime.Now;
                award.Priority = 1;
                award.AwardID = awardID;
                award.StationName = string.Empty;
                award.StationNameEnglish = string.Empty;
                bool rs = repository.InsertAwardNumber(award);
                if (rs)
                {
                    result = "success";
                }
            }
            return result;
        }
        [Authorization]
        [HttpPost]
        public string LockAndUnLockAwardNumber(int numberID, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                   
                    int statusNew = -1;
                    if (status == 1)
                    {
                        statusNew = 0;
                    }
                    else
                    {
                        statusNew = 1;
                    }
                    //bool rs = repository.LockAndUnlockAwardNumber(numberID, statusNew);

                    //if (rs)
                    //{
                    //    result = "success";
                    //}
                    AwardMegaballBO objAwardMegaball = new AwardMegaballBO();
                    objAwardMegaball.IsActive = statusNew;
                    objAwardMegaball.NumberID = numberID;
                    string strParameterObj = Algorithm.EncryptionObjectRSA(objAwardMegaball);
                    ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objRsa.UserName;

                    objResult = repository.LockAndUnlockAwardNumber(objRsa2);
                    if (!objResult.IsError)
                    {
                        result = "success";
                    }
                }
            }
            return result;
        }
        [Authorization]
        [HttpGet]
        public string SearchAwardByDate(string searchDay)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            if (searchDay != string.Empty)
            {
                string[] arr = searchDay.Split('/');
                if (arr != null && arr.Length > 0)
                {
                    string d = arr[0];
                    string m = arr[1];
                    string y = arr[2];
                    string fromD = m + "/" + d + "/" + y + " 00:00:00";
                    string toD = m + "/" + d + "/" + y + " 23:59:00";
                    try
                    {
                        fromDate = DateTime.Parse(fromD);
                        toDate = DateTime.Parse(toD);
                    }
                    catch
                    {
                        
                    }
                    IEnumerable<AwardNumberBO> lstAwardNumber = repository.GetListAwardNumberByDate(1, 200, fromDate, toDate);
                    if (lstAwardNumber != null && lstAwardNumber.Count() > 0)
                    {
                        foreach (AwardNumberBO award in lstAwardNumber)
                        {
                            string titleStatus = "Lock";
                            string status = "Active";
                            if (award.IsActive == 0)
                            {
                                status = "InActive";
                                titleStatus = "Unlock";
                            }


                            builder.Append("<tr id=\"trGroup_" + award.NumberID + "\" class=\"none-top-border\">");

                            builder.Append("<td><p>" + award.AwardID + "</p></td>");
                            builder.Append("<td><p>" + award.NumberValue + "</p></td>");
                            builder.Append("<td><p>" + award.AwardNameEnglish + "</p></td>");
                            builder.Append("<td><p>" + award.CreateDate.ToString("MM/dd/yyyy") + "</p></td>");
                            builder.Append("<td><p class=\"label-success label label-default\">" + status + "</p></td>");
                            builder.Append("<td>");

                            builder.Append("<a class=\"btn btn-info btn-sm\" href=\"javascript:void(0)\" onclick=\"ShowPopUpEditAwardNumber('" + award.NumberID + "','" + award.AwardID + "', this.title,'" + award.StationName + "','" + award.StationNameEnglish + "')\" data-toggle=\"modal\" data-target=\"#standardModal\" data-placement=\"top\" title=\"" + award.NumberValue + "\"><i class=\"glyphicon glyphicon-edit icon-white\"></i>Edit</a>");

                            builder.Append("<a class=\"btn btn-warning btn-sm\" href=\"javascript:void(0)\" onclick=\"LockAndUnlockAwardNumber('" + award.NumberID + "','" + award.IsActive + "')\" class=\"red-text\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + titleStatus + "\">");
                            if (award.IsActive == 1)
                            {
                                builder.Append("<i class=\"glyphicon glyphicon-ok\"></i>");
                            }
                            else
                            {
                                builder.Append("<i class=\"glyphicon glyphicon-remove\"></i>");

                            }
                            builder.Append("</a>");
                            builder.Append("</td>");
                            builder.Append("</tr>");
                        }
                    }
                    else
                    {
                        builder.Append("<tr><td colspan=\"5\"> <strong> No result found </strong></td></tr>");
                    }
                }
            }
            return builder.ToString();
        }
        [Authorization]
        [HttpGet]
        public string SearchWinnerAward(string searchDay)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            if (searchDay != string.Empty)
            {
                string[] arr = searchDay.Split('/');
                if (arr != null && arr.Length > 0)
                {
                    string d = arr[0];
                    string m = arr[1];
                    string y = arr[2];
                    string fromD = m + "/" + d + "/" + y + " 00:00:00";
                    string toD = m + "/" + d + "/" + y + " 23:59:00";
                    try
                    {
                        fromDate = DateTime.Parse(fromD);
                        toDate = DateTime.Parse(toD);
                    }
                    catch
                    {
                        
                    }
                    IEnumerable<WinnerBO> lstAwardNumber = repository.GetListWinner(1, 10000, fromDate, toDate);
                    if (lstAwardNumber != null && lstAwardNumber.Count() > 0)
                    {
                        foreach (WinnerBO award in lstAwardNumber)
                        {
                            int num1, num2, num3, num4, num5, extranum;
                            num1 = award.FirstNumber;
                            num2 = award.SecondNumber;
                            num3 = award.ThirdNumber;
                            num4 = award.FourthNumber;
                            num5 = award.FivethNumber;
                            extranum = award.ExtraNumber;

                            builder.Append("<tr id=\"trGroup_" + award.BookingID + "\" class=\"none-top-border\">");
                            builder.Append("<td><p>" + award.TransactionCode + "</p></td>");
                            builder.Append("<td><p>" + award.Email + "</p></td>");
                            builder.Append("<td><p>" + award.E_Wallet + "</p></td>");
                            //builder.Append("<td><p>" + award.NumberValue + "</p></td>");
                            builder.Append("<td class=\"center\">");
                            builder.Append("<span class=\"circle1\">" + num1 + "</span> ");
                            builder.Append("<span class=\"circle1\">" + num2 + "</span>");
                            builder.Append("<span class=\"circle1\">" + num3 + "</span>");
                            builder.Append("<span class=\"circle1\">" + num4 + "</span>");
                            builder.Append("<span class=\"circle1\">" + num5 + "</span> ");
                            builder.Append("&nbsp; &nbsp;<span class=\"circle_ball\">" + extranum + "</span></td>");
                            builder.Append("<td><p>" + award.OpenDate.ToString("MM/dd/yyyy") + "</p></td>");
                            builder.Append("</tr>");
                        }
                    }
                    else
                    {
                        builder.Append("<tr><td colspan=\"4\"> <strong> No result found </strong></td></tr>");
                    }
                }
            }
            return builder.ToString();
        }
        [Authorization]
        [HttpGet]
        public string ExportListWinnerAward(string searchDay)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            HSSFWorkbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("LIST WINNER");
            Row rowLogo = sheet.CreateRow(0);

            rowLogo.CreateCell(0).SetCellValue("LIST WINNER DATE " + searchDay + " - LOTTERY");
            rowLogo.HeightInPoints = 20;
            CellStyle styleLogo = workbook.CreateCellStyle();
            styleLogo.Alignment = HorizontalAlignment.CENTER;
            NPOI.SS.UserModel.Font font = workbook.CreateFont();
            font.FontHeightInPoints = 12;
            font.FontName = "Times New Roman";
            font.Boldweight = (short)FontBoldWeight.BOLD;

            styleLogo.SetFont(font);
            rowLogo.GetCell(0).CellStyle = styleLogo;

            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

            sheet.SetColumnWidth(0, 5 * 256);
            sheet.SetColumnWidth(1, 40 * 256);
            sheet.SetColumnWidth(2, 30 * 256);
            sheet.SetColumnWidth(3, 30 * 256);
            sheet.SetColumnWidth(4, 15 * 256);
            sheet.SetColumnWidth(5, 25 * 256);
            Row titleList = sheet.CreateRow(2);

            titleList.CreateCell(0).SetCellValue("STT");
            titleList.HeightInPoints = 22;
            CellStyle styleTitleList = workbook.CreateCellStyle();
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            NPOI.SS.UserModel.Font fontTitleList = workbook.CreateFont();
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            fontTitleList.Color = (short)NPOI.HSSF.Util.HSSFColor.WHITE.index;
            //NPOI.HSSF.UserModel.HSSFTextbox.LINESTYLE_SOLID
            styleTitleList.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;
            // styleTitleList.FillPattern = FillPatternType.THICK_VERT_BANDS;
            styleTitleList.FillPattern = FillPatternType.THIN_FORWARD_DIAG;
            styleTitleList.FillPattern = FillPatternType.THIN_BACKWARD_DIAG;
            styleTitleList.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;

            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(0).CellStyle = styleTitleList;

            titleList.CreateCell(1).SetCellValue("Transaction Code");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(1).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(1).CellStyle = styleTitleList;


            titleList.CreateCell(2).SetCellValue("Email");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(2).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(2).CellStyle = styleTitleList;



            titleList.CreateCell(3).SetCellValue("Bitcon Wallet");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(3).CellStyle = styleTitleList;

            titleList.CreateCell(4).SetCellValue("Ticket");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.CENTER;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(4).CellStyle = styleTitleList;


            titleList.CreateCell(5).SetCellValue("Open Date");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(5).CellStyle = styleTitleList;

            int rowNext = 3;

            int number = 1;

            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            if (searchDay != "")
            {
                string[] arr = searchDay.Split('/');
                if (arr != null && arr.Length > 0)
                {
                    string d = arr[0];
                    string m = arr[1];
                    string y = arr[2];
                    string fromD = m + "/" + d + "/" + y + " 00:00:00";
                    string toD = m + "/" + d + "/" + y + " 23:59:00";
                    try
                    {
                        fromDate = DateTime.Parse(fromD);
                        toDate = DateTime.Parse(toD);
                    }
                    catch
                    {
                        //fromD = d + "/" + m + "/" + y + " 00:00:00";
                        //toD = d + "/" + m + "/" + y + " 23:59:00";
                        //fromDate = DateTime.Parse(fromD);
                        //toDate = DateTime.Parse(toD);
                    }
                    IEnumerable<WinnerBO> lstAwardNumber = repository.GetListWinner(1, 10000, fromDate, toDate);
                    if (lstAwardNumber != null && lstAwardNumber.Count() > 0)
                    {
                        foreach (WinnerBO award in lstAwardNumber)
                        {
                            Row rowListInvoice = sheet.CreateRow(rowNext);

                            rowListInvoice.CreateCell(0).SetCellValue(number);
                            rowListInvoice.HeightInPoints = 20;
                            CellStyle styleNumber = workbook.CreateCellStyle();
                            styleNumber.Alignment = HorizontalAlignment.LEFT;
                            NPOI.SS.UserModel.Font fontNumber = workbook.CreateFont();
                            fontNumber.FontHeightInPoints = 12;
                            fontNumber.FontName = "Times New Roman";
                            styleNumber.SetFont(fontNumber);
                            rowListInvoice.GetCell(0).CellStyle = styleNumber;

                            rowListInvoice.CreateCell(1).SetCellValue(award.TransactionCode);
                            rowListInvoice.HeightInPoints = 20;
                            CellStyle styleDate = workbook.CreateCellStyle();
                            styleDate.Alignment = HorizontalAlignment.LEFT;
                            NPOI.SS.UserModel.Font fontDate = workbook.CreateFont();
                            fontDate.FontHeightInPoints = 12;
                            fontDate.FontName = "Times New Roman";
                            styleDate.SetFont(fontDate);
                            rowListInvoice.GetCell(1).CellStyle = styleDate;

                            rowListInvoice.CreateCell(2).SetCellValue(award.Email);
                            rowListInvoice.HeightInPoints = 20;
                            CellStyle styleDate2 = workbook.CreateCellStyle();
                            styleDate2.Alignment = HorizontalAlignment.LEFT;
                            NPOI.SS.UserModel.Font fontDate2 = workbook.CreateFont();
                            fontDate2.FontHeightInPoints = 12;
                            fontDate2.FontName = "Times New Roman";
                            styleDate.SetFont(fontDate2);
                            rowListInvoice.GetCell(2).CellStyle = styleDate2;

                            //Row rowDate = sheet.CreateRow(rowNext);
                            rowListInvoice.CreateCell(3).SetCellValue(award.E_Wallet);
                            rowListInvoice.HeightInPoints = 20;
                            CellStyle styleEmail = workbook.CreateCellStyle();
                            styleEmail.Alignment = HorizontalAlignment.LEFT;
                            NPOI.SS.UserModel.Font fontEmail = workbook.CreateFont();
                            fontEmail.FontHeightInPoints = 12;
                            fontEmail.FontName = "Times New Roman";
                            styleEmail.SetFont(fontEmail);
                            rowListInvoice.GetCell(3).CellStyle = styleEmail;


                            //Row rowOrderID = sheet.CreateRow(rowNext);
                            string strnumber = award.FirstNumber + " " + award.SecondNumber + " " + award.ThirdNumber + " " + award.FourthNumber + " " + award.FivethNumber + " " + award.ExtraNumber;

                            rowListInvoice.CreateCell(4).SetCellValue(strnumber);
                            rowListInvoice.HeightInPoints = 20;
                            CellStyle styleOrderID = workbook.CreateCellStyle();
                            styleOrderID.Alignment = HorizontalAlignment.LEFT;
                            NPOI.SS.UserModel.Font fontOrderID = workbook.CreateFont();
                            fontOrderID.FontHeightInPoints = 12;
                            fontOrderID.FontName = "Times New Roman";
                            styleOrderID.SetFont(fontOrderID);
                            rowListInvoice.GetCell(4).CellStyle = styleOrderID;

                            rowListInvoice.CreateCell(5).SetCellValue(award.OpenDate.ToString("MM/dd/yyyy"));
                            rowListInvoice.HeightInPoints = 20;
                            CellStyle styleEmail3 = workbook.CreateCellStyle();
                            styleEmail3.Alignment = HorizontalAlignment.LEFT;

                            NPOI.SS.UserModel.Font fontEmail3 = workbook.CreateFont();
                            fontEmail3.FontHeightInPoints = 12;
                            fontEmail3.FontName = "Times New Roman";

                            styleEmail.SetFont(fontEmail3);
                            rowListInvoice.GetCell(5).CellStyle = styleEmail3;

                            number++;
                            rowNext++;
                        }

                    }
                }
            }
            string fileNameExcel = "ListWinner_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + ".xls";
            string filename = Server.MapPath("~/FileExcels/AwardWinner/" + fileNameExcel);
            using (var fileData = new FileStream(filename, FileMode.Create))
            {
                workbook.Write(fileData);
            }
            return fileNameExcel;
        }

    }
}
