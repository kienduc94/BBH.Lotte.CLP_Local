﻿using BBC.Core.Common.Utils;
using BBC.Core.Common.Cache;
 using BBC.Core.Database;
using BBH.Lotte.CLP.CMSAdmin.Models;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain.ObjDomain;
using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CMSAdmin.Models;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
namespace BBH.Lotte.CLP.CMSAdmin.Controllers
{
    public class SystemConfigController : Controller
    {
        [Dependency]
        protected ISystemConfigServices repository { get; set; }
        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }
        RedisCache objRedisCache = new RedisCache();
        int groupIDAdmin = 0;
        [Models.Authorization]
        public ActionResult Index()
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            IEnumerable<SystemConfigBO> lstConfig = new List<SystemConfigBO>();
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                      lstConfig = repository.GetListSystemConfig();
                    ViewData["ListSystemConfig"] = lstConfig;
                }
            }

            return View();
        }

        [Models.Authorization]
        [System.Web.Http.HttpPost]
        public string UpdateStatusActive(int configID, int isAutoLottery)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int statusNew = -1;
                    if (isAutoLottery == 1)
                    {
                        statusNew = 0;
                    }
                    else
                    {
                        statusNew = 1;
                    }
                    SystemConfigBO objConfigSys = new SystemConfigBO();
                    objConfigSys.ConfigID = configID;
                    objConfigSys.IsAutoLottery = statusNew;
                    objConfigSys.UpdatedUser = objConfig.UserName;
                    string strParameterUpdate = Algorithm.EncryptionObjectRSA<SystemConfigBO>(objConfigSys);
                    ModelRSA objRsa2 = new ModelRSA(strParameterUpdate);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objConfig.UserName;
                   
                        objResult = repository.UpdateStatusActive(objRsa2);
                        if (!objResult.IsError)
                        {
                            objRedisCache.RemoveCache(Common.KeyTicketConfig);
                            result = ResultKey.RESULT_SUCCESS;
                        }
                    
                }
            }
            return result;
        }

        //[System.Web.Http.HttpPost]
        //public string SaveTicketConfig(int configID, string coinfigName, int numberTicket, decimal coinValue, string coinID)
        //{
        //    string result = string.Empty;
        //    IPConfigBO objConfig = new IPConfigBO();
        //    objConfig.IPAddress = Common.GetLocalIPAddress();
        //    objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
        //    objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

        //    string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
        //    ModelRSA objRsa = new ModelRSA(strRsaParameter1);
        //    objRsa.UserName = objConfig.UserName;
        //    ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
        //    if (!objResult.IsError)
        //    {
        //        objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
        //        if (objRsa.Token != null && objRsa.Token != string.Empty)
        //        {
        //            TicketConfigBO ticketConfig = new TicketConfigBO();
        //            if (coinfigName == string.Empty /*|| awardValue == string.Empty*/)
        //            {
        //                result = ResultKey.RESULT_FAILES;
        //            }
        //            else
        //            {
        //                if (configID > 0)
        //                {
        //                    try
        //                    {
        //                        //double value = 0;
        //                        //double.TryParse(awardValue, out value);
        //                        ticketConfig.ConfigID = configID;
        //                        ticketConfig.CoinValues = coinValue;
        //                        ticketConfig.ConfigName = coinfigName;
        //                        ticketConfig.NumberTicket = numberTicket;
        //                        ticketConfig.CoinID = coinID;
        //                        ticketConfig.UpdatedDate = DateTime.Now;

        //                        //bool CheckAwardNameEnglish = repository.CheckAwardNameEnglishExists(awardID, awardNameEnglish);
        //                        //if (!CheckAwardNameEnglish)
        //                        //{
        //                        string strParameterObj = Algorithm.EncryptionObjectRSA<TicketConfigBO>(ticketConfig);
        //                        ModelRSA objRsa2 = new ModelRSA(strParameterObj);
        //                        objRsa2.Token = objRsa.Token;
        //                        objRsa2.UserName = objConfig.UserName;
        //                        objResult = repository.UpdateTicketConfig(objRsa2);
        //                        if (!objResult.IsError)
        //                        {
        //                            objRedisCache.RemoveCache(Common.KeyTicketConfig);
        //                            result = ResultKey.RESULT_SUCCESS;
        //                        }
        //                        else
        //                        {
        //                            result = ResultKey.RESULT_FAILES;
        //                        }
        //                        //}
        //                        //else
        //                        //{
        //                        //    result = ResultKey.RESULT_AWARDNAMEENGLISHEXIST;
        //                        //}
        //                    }
        //                    catch { }
        //                }
        //                else if (configID == 0)
        //                {
        //                    try
        //                    {
        //                        //double value = 0;
        //                        //double.TryParse(awardValue, out value);
        //                        //award.AwardID = awardID;
        //                        ticketConfig.ConfigID = configID;
        //                        ticketConfig.CoinValues = coinValue;
        //                        ticketConfig.ConfigName = coinfigName;
        //                        ticketConfig.NumberTicket = numberTicket;
        //                        ticketConfig.CoinID = coinID;
        //                        ticketConfig.IsActive = 1;
        //                        ticketConfig.IsDeleted = 0;
        //                        ticketConfig.CreatedDate = DateTime.Now;
        //                        ticketConfig.CreatedUser = Session[Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME].ToString();
        //                        bool CheckCoinID = repository.CheckCoinID(configID, coinID);
        //                        if (CheckCoinID)
        //                        {
        //                            result = ResultKey.RESULT_CONFIGTICKETEXIST;
        //                        }
        //                        else
        //                        {
        //                            string strParameterObj = Algorithm.EncryptionObjectRSA<TicketConfigBO>(ticketConfig);
        //                            ModelRSA objRsa2 = new ModelRSA(strParameterObj);
        //                            objRsa2.Token = objRsa.Token;
        //                            objRsa2.UserName = objConfig.UserName;
        //                            objResult = repository.InsertTicketConfig(objRsa2);
        //                            if (!objResult.IsError)
        //                            {
        //                                objRedisCache.RemoveCache(Common.KeyTicketConfig);
        //                                result = ResultKey.RESULT_SUCCESS;
        //                            }
        //                            else
        //                            {
        //                                result = ResultKey.RESULT_FAILES;
        //                            }
        //                        }
        //                    }
        //                    catch { }
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

   
    }
}