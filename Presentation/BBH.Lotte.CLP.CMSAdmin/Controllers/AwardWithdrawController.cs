﻿using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBH.Lotte.CLP.CMSAdmin.Models;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain.ObjDomain;
using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CMSAdmin.Models;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

using System.Web.Mvc;


namespace BBH.Lotte.CLP.CMSAdmin.Controllers
{
    public class AwardWithdrawController : Controller
    {
        [Dependency]
        protected IAwardWithdrawServices repository { get; set; }

        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }

        [Dependency]
        protected IMemberServices repositoryMember { get; set; }

        int groupID = 0;

        [Models.Authorization]
        public ActionResult Index(string p)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupID"], out groupID);
            if ((int)Session["GroupIDAdmin"] !=groupID)
            {
                Response.Redirect("/");
            }
            ModelRSA objRsaResult = null;
            int totalRecord = 0;
            List<AwardWithdrawBO> lstAward = new List<AwardWithdrawBO>();
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
           
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int intPageSize = 10;
                    int start = 0, end = 10;
                    Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                    int page = 1;
                    try
                    {
                        if (p != null && p != string.Empty)
                        {
                            page = int.Parse(p);
                        }
                    }
                    catch
                    {
                    }
                    if (page > 1)
                    {
                        start = (page - 1) * intPageSize + 1;
                        end = (page * intPageSize);
                    }

                    DateTime fromDate = DateTime.Parse("01/01/1990");
                    DateTime toDate = DateTime.Now;
                    DateTime awardDate = DateTime.Parse("01/01/1990");
                    DateTime requestDate = DateTime.Parse("01/01/1990");
                    int requestStatus = -1;
                    string userName = string.Empty;
                    ObjSearchAwardWithdraw objSearch = new ObjSearchAwardWithdraw();
                    objSearch.AwardDate = awardDate;
                    objSearch.EndIndex = end;
                    objSearch.RequestFromDate = fromDate;
                    objSearch.RequestStatus = requestStatus;
                    objSearch.RequestToDate = toDate;
                    objSearch.RquestMemberID = userName;
                    objSearch.StartIndex = start;
                    objSearch.RequestDate = requestDate;



                    string strParameterSearch = Algorithm.EncryptionObjectRSA<ObjSearchAwardWithdraw>(objSearch);
                    ModelRSA objRsa2 = new ModelRSA(strParameterSearch);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objConfig.UserName;

                    objResult = repository.ListAwardWithdraw(objRsa2, ref objRsaResult);
                    if (objRsaResult != null && objRsaResult.ParamRSAFirst != "")
                    {
                      
                        lstAward = Algorithm.DecryptionObjectRSA<List<AwardWithdrawBO>>(objRsaResult.ParamRSAFirst);
                    }
                   
                    if (lstAward != null && lstAward.Count() > 0)
                    {
                        totalRecord = lstAward[0].TotalRecord;
                    }
                   
                }
            }

            ViewData["ListAwardWithdraw"] = lstAward;
            TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;

            IEnumerable<MemberBO> lstMember = repositoryMember.GetListMember(1, 10000);
            ViewData[ViewDataKey.VIEWDATA_LISTMEMBER] = lstMember;
            return View();
        }

        [Models.Authorization]
        [HttpPost]
        public string UpdateStatusAwardWithdraw(string transactionID, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupID"], out groupID);
            if ((int)Session["GroupIDAdmin"] != groupID)
            {
                Response.Redirect("/");
            }
            //string clientID = ConfigurationManager.AppSettings["ClientID"];
            //string systemID = ConfigurationManager.AppSettings["SystemID"];
            //string walletID = ConfigurationManager.AppSettings["WalletID"];
            //string requestBy = ConfigurationManager.AppSettings["LotteryCarcoin"];
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    AwardWithdrawBO objAward = new AwardWithdrawBO();
                    objAward.TransactionID = transactionID;
                    objAward.RequestStatus = status;
                    objAward.UpdatedUser = objConfig.UserName;
                    objAward.AdminIDApprove = objConfig.UserName;
                    objAward.ApproveDate = DateTime.Now;
                    
                    string strParameterUpdate = Algorithm.EncryptionObjectRSA<AwardWithdrawBO>(objAward);
                    ModelRSA objRsa2 = new ModelRSA(strParameterUpdate);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objConfig.UserName;
                    
                    if (status == 1)
                    {
                        //TransactionFilterRequest objRequest = new TransactionFilterRequest();

                        objResult = repository.UpdateStatusAwardWithdraw(objRsa2);
                        if (!objResult.IsError)
                        {
                            result = ResultKey.RESULT_SUCCESS;
                        }
                    }
                }
            }
            return result;
        }
        [Models.Authorization]
        [HttpGet]
        public string SearchAwardWithdraw(string memberID, string fromDate, string toDate, int page, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupID"], out groupID);
            if ((int)Session["GroupIDAdmin"] != groupID)
            {
                Response.Redirect("/");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            ModelRSA objRsaResult = null;
            int totalRecord = 0;
            List<AwardWithdrawBO> lstAward = new List<AwardWithdrawBO>();
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int intPageSize = 10;
                    int start = 0, end = 10;
                    Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

                    if (page > 1)
                    {
                        start = (page - 1) * intPageSize + 1;
                        end = (page * intPageSize);
                    }

                    DateTime fromD = DateTime.Parse("01/01/1990");
                    DateTime toD = DateTime.Now;
                    if (fromDate != null && fromDate != string.Empty && fromDate != "")
                    {
                        fromD = Common.ConvertDateTime(fromDate);
                        if (toD != null)
                        {
                            toD = Common.ConvertDateTime(toDate);
                        }
                        else
                        {
                            toD = DateTime.Now;
                        }
                    }
                    DateTime awardDate = DateTime.Parse("01/01/1990");
                    DateTime requestDate = DateTime.Parse("01/01/1990");
                    ObjSearchAwardWithdraw objSearch = new ObjSearchAwardWithdraw();
                    objSearch.AwardDate = awardDate;
                    objSearch.EndIndex = end;
                    objSearch.RequestFromDate = fromD;
                    objSearch.RequestStatus = status;
                    objSearch.RequestToDate = toD;
                    objSearch.RquestMemberID = memberID;
                    objSearch.StartIndex = start;
                    objSearch.RequestDate = requestDate;
                    string strParameterSearch = Algorithm.EncryptionObjectRSA<ObjSearchAwardWithdraw>(objSearch);
                    ModelRSA objRsa2 = new ModelRSA(strParameterSearch);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objConfig.UserName;

                    objResult = repository.ListAwardWithdraw(objRsa2, ref objRsaResult);
                    if (objRsaResult != null && objRsaResult.ParamRSAFirst != "")
                    {
                        lstAward = Algorithm.DecryptionObjectRSA<List<AwardWithdrawBO>>(objRsaResult.ParamRSAFirst);
                    }
                    if (lstAward != null && lstAward.Count() > 0)
                    {
                        totalRecord = lstAward[0].TotalRecord;
                        foreach (AwardWithdrawBO transaction in lstAward)
                        {
                            string strStatus = string.Empty;
                            if (transaction.RequestStatus == 0)
                            {
                                strStatus = "UnProcess";
                            }
                            else
                            {
                                strStatus = "Confirmed";
                            }
                            builder.Append("<tr id=\"trGroup_" + transaction.TransactionID.Trim() + "\" class=\"none-top-border\">");
                            builder.Append("<td class=\"center\">" + transaction.RequestMemberID + "</td>");
                            builder.Append("<td class=\"center\">" + transaction.RequestDate.ToString("dd/MM/yyyy hh:mm:ss") + "</td>");
                            builder.Append("<td class=\"center\" title=\"" + transaction.AwardDate + "\">" + transaction.AwardDate.ToString("dd/MM/yyyy hh:mm:ss") + "</td>");
                            builder.Append("<td class=\"center\">" + transaction.ValuesWithdraw + "</td>");
                            builder.Append("<td class=\"center\">");
                            builder.Append("<span class=\"label-success label label-default\">" + strStatus + "</span>");
                            builder.Append("</td>");
                            builder.Append("<td class=\"center\">");
                            if (transaction.RequestStatus == 0)
                            {
                                builder.Append("<img src =\"/Images/loading.gif\" id=\"imgLoading_" + transaction.TransactionID.Trim() + "\" style=\"position: absolute; display:none ; float: right; width: 30px; margin-left: 45px;\" >");
                                builder.Append("<a class=\"btn btn-info btn-sm\" href=\"javascript: void(0)\" onclick=\"ConfirmWithdrawAward('" + transaction.TransactionID.Trim() + "', '1')\" title=\"Confirm\">");
                                builder.Append("<i class=\"glyphicon glyphicon-edit icon-white\"></i>Confirm");
                                builder.Append("</a>");
                            }
                            builder.Append("</td>");
                            builder.Append("<td class=\"center\">");
                            builder.Append("<a class=\"btn btn-info btn-sm\" onclick=\"ShowAwardWithdrawDetail('" + transaction.TransactionID.Trim() + "','" + transaction.RequestStatus + "')\" data-toggle=\"modal\" data-target=\"#standardModal\" href=\"javacript: void(0)\">View detail</a>");
                            builder.Append("</td>");
                            builder.Append("</tr>");
                        }

                        if (totalRecord > 0)
                        {
                            int pagerank = 5;
                            int next = 10;
                            int prev = 1; int nextPage = 0, previewPage = 0;
                            int totalPage = totalRecord / intPageSize;
                            int balance = totalRecord % intPageSize;
                            if (balance != 0)
                            {
                                totalPage += 1;
                            }
                            if (page >= totalPage)
                            {
                                nextPage = totalPage;
                            }
                            else
                            {
                                nextPage = (page + 1);
                            }
                            if (page <= 1)
                            {
                                previewPage = 1;
                            }
                            else
                            {
                                previewPage = page - 1;
                            }
                            int currentPage = page;
                            var m = 1;
                            if (totalPage > 10)
                            {
                                if (page > pagerank + 1)
                                {
                                    next = (page + pagerank) - 1;
                                    m = page - pagerank;
                                }
                                if (page <= pagerank)
                                {
                                    prev = (page - pagerank);
                                    m = 1;
                                }
                                if (next > totalPage)
                                {
                                    next = totalPage;
                                }
                                if (prev < 1)
                                {
                                    prev = 1;
                                }
                            }
                            else
                            {
                                next = totalPage;
                                prev = 1;
                            }

                            if (totalPage > 1)
                            {
                                if (currentPage > 1)
                                {

                                    builderPaging.Append("<li >");
                                    builderPaging.Append("<a onclick=\"PagingSearchAwardWithdraw(" + previewPage + ")\" href=\"javascript:void(0)\" class=\"page-link waves-effect waves-effect\" aria-label=\"Previous\">Prev");
                                    //builderPaging.Append("<span aria-hidden=\"true\">");
                                    //builderPaging.Append("<i class=\"fa fa-angle-double-left\" area-hidden=\"hidden\"></i>");
                                    //builderPaging.Append("</span>");
                                    builderPaging.Append("</a>");
                                    builderPaging.Append(" </li>");
                                }
                            }

                            if (totalPage > 1)
                            {

                                for (; m <= next; m++)
                                {
                                    if (m == currentPage)
                                    {
                                        builderPaging.Append("<li class=\"page-item active\"><a class=\"page-link\">" + m + "</a></li>");
                                    }
                                    else
                                    {
                                        builderPaging.Append("<li onclick=\"PagingSearchAwardWithdraw(" + m + ")\" class=\"page-item\"><a href=\"javascript:void(0)\" class=\"page-link\">" + m + "</a></li>");
                                    }
                                }  
                            }

                            if (totalPage > 1)
                            {
                                if (currentPage < totalPage)
                                {
                                    builderPaging.Append("<li >");
                                    builderPaging.Append("<a onclick=\"PagingSearchAwardWithdraw(" + nextPage + ")\" class=\"page-link waves-effect waves-effect\" aria-label=\"Next\"> Next");
                                    //builderPaging.Append("<span aria-hidden=\"true\">");
                                    //builderPaging.Append("<i class=\"fa fa-angle-double-right\" area-hidden=\"hidden\"></i>");
                                    //builderPaging.Append("</span>");
                                    builderPaging.Append("</a>");
                                    builderPaging.Append("</li>");
                                }
                            }
                        }
                    }
                }
            }

            SearchObj obj = new SearchObj();
            obj.ContentResult = builder.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = totalRecord;
            string json = JsonConvert.SerializeObject(obj);
            return json;
        }

        [Models.Authorization]
        [HttpGet]
        public string GetAwardWithdrawDetail(string transactionID,int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupID"], out groupID);
            if ((int)Session["GroupIDAdmin"] != groupID)
            {
                Response.Redirect("/");
            }
            string json = string.Empty;
            ModelRSA objRsaResult = null;
            AwardWithdrawBO objAwardResult = null;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    AwardWithdrawBO obj = new AwardWithdrawBO();
                    obj.TransactionID = transactionID;
                    obj.RequestStatus = status;
               
                    TempData["RequestStatus"] = status;
                    string strParameterDetail = Algorithm.EncryptionObjectRSA<AwardWithdrawBO>(obj);
                    ModelRSA objRsa2 = new ModelRSA(strParameterDetail);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objConfig.UserName;

                    objResult = repository.GetAwardWithdrawDetail(objRsa2, ref objRsaResult);
                    if (objRsaResult != null && objRsaResult.ParamRSAFirst != "")
                    {
                        objAwardResult = Algorithm.DecryptionObjectRSA<AwardWithdrawBO>(objRsaResult.ParamRSAFirst);
                    }
                    
                }
            }
            if (objAwardResult != null)
            {
                ObjAwardWithdrawResult objJson = new ObjAwardWithdrawResult();
                objJson.AwardDate = objAwardResult.AwardDate.ToString("dd/MM/yyyy hh:mm:ss");
                objJson.RequestDate = objAwardResult.RequestDate.ToString("dd/MM/yyyy hh:mm:ss");
                objJson.TransactionID = objAwardResult.TransactionID;
                objJson.AwardValues = objAwardResult.ValuesWithdraw.ToString();
                objJson.UserName = objAwardResult.RequestMemberID;

                //TempData["ConfimDate"] = objAwardResult.ApproveDate;
                // TempData["UserName"] = objConfig.UserName;
                //TempData["RequestStatus"] = objAwardResult.RequestStatus;

                if (objAwardResult.RequestStatus == 0)
                {
                    objJson.RequestStatus = "UnProccess";
                }
                else
                {
                    objJson.RequestStatus = "Confirmed";
                }
                if (!string.IsNullOrEmpty(objAwardResult.AdminIDApprove))
                {
                    objJson.ConfirmUser = objAwardResult.AdminIDApprove;
                }
                else
                {
                    objJson.ConfirmUser = string.Empty;
                }
                if (objAwardResult.ApproveDate != null && objAwardResult.ApproveDate != DateTime.Parse("01/01/0001"))
                {
                    objJson.ConfirmDate = objAwardResult.ApproveDate.ToString("dd/MM/yyyy hh:mm:ss");
                }
                else
                {
                    objJson.ConfirmDate = string.Empty;
                }
                json = JsonConvert.SerializeObject(objJson);
            }
            return json;
        }

    }
}