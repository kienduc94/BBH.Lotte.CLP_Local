﻿using BBC.Core.Common.Cache;
using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBH.Lotte.CLP.CMSAdmin.Models;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain.ObjDomain;
using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CMSAdmin.Models;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.CMSAdmin.Controllers
{
    public class TicketConfigController : Controller
    {
        // GET api/<controller>
        [Dependency]
        protected ITicketConfigServices repository { get; set; }

        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }
        RedisCache objRedisCache = new RedisCache();

        int groupIDAdmin = 0;
        [Models.Authorization]
        public ActionResult Index(string p)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            int totalRecord = 0;
            IEnumerable<TicketConfigBO> lstTicketConfig = new List<TicketConfigBO>();
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);

            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int intPageSize = 10;
                    int start = 0, end = 10;
                    Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                    int page = 1;
                    try
                    {
                        if (p != null && p != string.Empty)
                        {
                            page = int.Parse(p);
                        }
                    }
                    catch
                    {
                    }
                    if (page > 1)
                    {
                        start = (page - 1) * intPageSize + 1;
                        end = (page * intPageSize);
                    }

                    lstTicketConfig = repository.ListTicketConfig();
                    //if (lstTicketConfig != null && lstTicketConfig.Count() > 0)
                    //{
                    //    totalRecord = lstTicketConfig[0].TotalRecord;
                    //}

                }
            }

            ViewData[ViewDataKey.VIEWDATA_LISTTICKETCONFIG] = lstTicketConfig;
            //TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;

          
            return View();
        }

        [Models.Authorization]
        [System.Web.Http.HttpPost]
        public string SaveTicketConfig(int configID, string coinfigName, int numberTicket, decimal coinValue,string coinID)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    TicketConfigBO ticketConfig = new TicketConfigBO();
                    if (coinfigName == string.Empty /*|| awardValue == string.Empty*/)
                    {
                        result = ResultKey.RESULT_FAILES;
                    }
                    else
                    {
                        if (configID > 0)
                        {
                            try
                            {
                                //double value = 0;
                                //double.TryParse(awardValue, out value);
                                ticketConfig.ConfigID = configID;
                                ticketConfig.CoinValues = coinValue;
                                ticketConfig.ConfigName = coinfigName;
                                ticketConfig.NumberTicket = numberTicket;
                                ticketConfig.CoinID = coinID;
                                ticketConfig.UpdatedDate=DateTime.Now;

                                //bool CheckAwardNameEnglish = repository.CheckAwardNameEnglishExists(awardID, awardNameEnglish);
                                //if (!CheckAwardNameEnglish)
                                //{
                                string strParameterObj = Algorithm.EncryptionObjectRSA<TicketConfigBO>(ticketConfig);
                                ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                objRsa2.Token = objRsa.Token;
                                objRsa2.UserName = objConfig.UserName;
                                objResult = repository.UpdateTicketConfig(objRsa2);
                                if (!objResult.IsError)
                                {
                                    objRedisCache.RemoveCache(Common.KeyTicketConfig);
                                    result = ResultKey.RESULT_SUCCESS;
                                }
                                else
                                {
                                    result = ResultKey.RESULT_FAILES;
                                }
                                //}
                                //else
                                //{
                                //    result = ResultKey.RESULT_AWARDNAMEENGLISHEXIST;
                                //}
                            }
                            catch { }
                        }
                        else if (configID == 0)
                        {
                            try
                            {
                                //double value = 0;
                                //double.TryParse(awardValue, out value);
                                //award.AwardID = awardID;
                                ticketConfig.ConfigID = configID;
                                ticketConfig.CoinValues = coinValue;
                                ticketConfig.ConfigName = coinfigName;
                                ticketConfig.NumberTicket = numberTicket;
                                ticketConfig.CoinID = coinID;
                                ticketConfig.IsActive = 1;
                                ticketConfig.IsDeleted = 0;
                                ticketConfig.CreatedDate = DateTime.Now;
                                ticketConfig.CreatedUser = Session[Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME].ToString();
                                bool CheckCoinID = repository.CheckCoinID(configID, coinID);
                                if (CheckCoinID)
                                {
                                    result = ResultKey.RESULT_CONFIGTICKETEXIST;
                                }
                                else
                                {
                                    string strParameterObj = Algorithm.EncryptionObjectRSA<TicketConfigBO>(ticketConfig);
                                    ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                    objRsa2.Token = objRsa.Token;
                                    objRsa2.UserName = objConfig.UserName;
                                    objResult = repository.InsertTicketConfig(objRsa2);    
                                    if (!objResult.IsError)
                                    {
                                        objRedisCache.RemoveCache(Common.KeyTicketConfig);
                                        result = ResultKey.RESULT_SUCCESS;
                                    }
                                    else
                                    {
                                        result = ResultKey.RESULT_FAILES;
                                    }
                                }
                            }
                            catch { }
                        }
                    }
                }
            }
            return result;
        }

        [Models.Authorization]
        [System.Web.Http.HttpPost]
        public string LockAndUnLockTicketConfig(int configID, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int statusNew = -1;
                    if (status == 1)
                    {
                        statusNew = 0;
                    }
                    else
                    {
                        statusNew = 1;
                    }
                    TicketConfigBO objTicket = new TicketConfigBO();
                    objTicket.IsActive = statusNew;
                    objTicket.ConfigID = configID;
                    string strParameterObj = Algorithm.EncryptionObjectRSA<TicketConfigBO>(objTicket);
                    ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objConfig.UserName;
                    objResult = repository.LockAndUnlockConfigTicket(objRsa2);
                    if (!objResult.IsError)
                    {
                        objRedisCache.RemoveCache(Common.KeyTicketConfig);
                        result = ResultKey.RESULT_SUCCESS;
                    }
                }
            }
            return result;
        }


    }
}