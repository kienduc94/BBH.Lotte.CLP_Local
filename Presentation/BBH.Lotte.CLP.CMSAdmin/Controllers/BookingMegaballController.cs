﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BBH.Lotte.CLP.Repository;
using Newtonsoft.Json;
using BBH.Lotte.CMSAdmin.Models;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Microsoft.Practices.Unity;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.CMSAdmin.Models;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class BookingMegaballController : Controller
    {
        //
        // GET: /BookingMegaball/

        [Dependency]
        protected IBookingMegaballServices repository { get; set; }
        [Dependency]
        protected IMemberServices repositoryMember { get; set; }

        int groupIDAdmin = 0;
        [Authorization]
        public ActionResult Index(string p)
        {

            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            int totalRecord = 0;
            int intPageSize = 10;
            int start = 0, end = 10;
            //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
            int page = 1;
            try
            {
                if (p != null && p != string.Empty)
                {
                    page = int.Parse(p);
                }
            }
            catch
            {

            }
            if (page > 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<BookingMegaballBO> lstBooking = repository.ListAllBookingPage(start, end);
            ViewData[ViewDataKey.VIEWDATA_LISTBOOKINGMEGABALL] = lstBooking;
            if (lstBooking != null && lstBooking.Count() > 0)
            {
                totalRecord = lstBooking.ElementAt(0).TotalRecord;
            }
            TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;

            IEnumerable<MemberBO> lstMember = repositoryMember.GetListMember(1, 10000);
            ViewData[ViewDataKey.VIEWDATA_LISTMEMBER] = lstMember;
            return View();
        }
        [Authorization]
        [HttpPost]
        public string UpdateStatusBookingMegaball(int bookingID, int status, int memberID)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;

            bool rs = repository.UpdateStatusBookingMegaball(bookingID, status);
            if (rs)
            {
                int points = 0;
                if (status == 1)
                {
                    points = -points;
                    repositoryMember.UpdatePointsMember(memberID, points);
                }
                result = "success";
            }
            return result;
        }
        [Authorization]
        [HttpGet]
        public string SearchBookingMegaball(int memberID, string fromDate, string toDate, int page, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/");
            }
            DateTime? fromD = null;
            DateTime? toD = null;
            //DateTime? fromD = DateTime.Now;
            //DateTime? toD = DateTime.Now;

            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int intPageSize = 50;
            int start = 0, end = 100;
            int totalRecord = 0;
            //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
            #region edit
            //if (fromDate == string.Empty && toDate == string.Empty)
            //{
            //    fromD = DateTime.Parse("01/01/1990");
            //    toD = DateTime.Now;

            //}
            //else if (fromDate == string.Empty)
            //{
            //    fromD = DateTime.Parse("01/01/1990");

            //    string[] arrTo = toDate.Split('/');
            //    if (arrTo != null && arrTo.Length > 0)
            //    {
            //        string m = arrTo[0];
            //        string d = arrTo[1];
            //        string y = arrTo[2];
            //        string dateTo = m + "/" + d + "/" + y + " 23:59:00";
            //        toD = DateTime.Parse(dateTo);
            //    }
            //}
            //else if (toDate == string.Empty)
            //{
            //    string[] arrFrom = fromDate.Split('/');
            //    if (arrFrom != null && arrFrom.Length > 0)
            //    {
            //        string m = arrFrom[0];
            //        string d = arrFrom[1];
            //        string y = arrFrom[2];
            //        string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
            //        fromD = DateTime.Parse(dateFrom);
            //    }

            //    toDate = toD.ToString();
            //    string[] arrTo = toDate.Split('/');
            //    if (arrTo != null && arrTo.Length > 0)
            //    {
            //        string m = arrTo[0];
            //        string d = arrTo[1];
            //        string y = arrTo[2];
            //        string dateTo = m + "/" + d + "/" + y;
            //        toD = DateTime.Parse(dateTo);
            //    }

            //}
            //else
            //{
            //    string[] arrFrom = fromDate.Split('/');
            //    if (arrFrom != null && arrFrom.Length > 0)
            //    {
            //        string m = arrFrom[0];
            //        string d = arrFrom[1];
            //        string y = arrFrom[2];
            //        string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
            //        //String dateFrom = fromD.ToString("MM/dd/yyyy");
            //        fromD = DateTime.Parse(dateFrom);
            //    }
            //    string[] arrTo = toDate.Split('/');
            //    if (arrTo != null && arrTo.Length > 0)
            //    {
            //        string m = arrTo[0];
            //        string d = arrTo[1];
            //        string y = arrTo[2];
            //        string dateTo = m + "/" + d + "/" + y + " 23:59:00";
            //        //String dateTo = toD.ToString("MM/dd/yyyy");
            //        toD = DateTime.Parse(dateTo);
            //    }
            //}
            #endregion
            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                fromD = DateTime.Parse("01/01/1990 12:01:01");
                toD = DateTime.Now;
            }
            else if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
            }
            else
            {
                if (string.IsNullOrEmpty(fromDate))
                {
                    if (string.IsNullOrEmpty(toDate))
                    {
                        //fromD = DateTime.Parse("");
                        toD = DateTime.Parse("01/01/1990");
                    }
                    else
                    {
                        fromD = DateTime.Parse("01/01/1990");
                        toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                    }
                }
                else
                {
                    fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                    toD = DateTime.Now;
                }
            }

            if (page >= 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<BookingMegaballBO> lstBooking = repository.ListAllBookingBySearch(memberID, fromD, toD, status, start, end);
            if (lstBooking != null && lstBooking.Count() > 0)
            {
                totalRecord = lstBooking.ElementAt(0).TotalRecord;
                foreach (BookingMegaballBO booking in lstBooking)
                {
                    string titleStatus = string.Empty;
                    string statusName = string.Empty;
                    if (booking.Status == 0)
                    {
                        statusName = "Not confirm";
                        titleStatus = "Confirm";
                    }
                    else if (booking.Status == 1)
                    {
                        statusName = "Confirmed";
                        titleStatus = "Actived";
                    }
                    else if (booking.Status == 2)
                    {
                        statusName = "Deleted";
                        titleStatus = "";
                    }
                    const int maxlenght = 20;
                    string transactionCode = booking.TransactionCode.ToString();
                    string dot = "...";
                    if (transactionCode.Length > maxlenght)
                    {
                        transactionCode = transactionCode.Substring(0, maxlenght) + "" + dot;
                    }

                    int num1, num2, num3, num4, num5, extranum;
                    num1 = booking.FirstNumber;
                    num2 = booking.SecondNumber;
                    num3 = booking.ThirdNumber;
                    num4 = booking.FourthNumber;
                    num5 = booking.FirstNumber;
                    extranum = booking.ExtraNumber;

                    builder.Append(" <tr id=\"trGroup_" + booking.BookingID + "\" class=\"none-top-border\">");
                    builder.Append("<td class=\"center\"><p>" + booking.BookingID + "</p> </td>");
                    builder.Append("<td>" + booking.Email + "</td>");
                    //builder.Append("<td>" + booking.NumberWin + "</td>");
                    builder.Append("<td class=\"center\" style=\"width:185px\">");
                    builder.Append("<span class=\"circle12\">" + booking.FirstNumber + "</span>");
                    builder.Append("<span class=\"circle12\">" + booking.SecondNumber + "</span>");
                    builder.Append("<span class=\"circle12\">" + booking.ThirdNumber + "</span>");
                    builder.Append("<span class=\"circle12\">" + booking.FourthNumber + "</span>");
                    builder.Append("<span class=\"circle12\">" + booking.FirstNumber + "</span>");
                    builder.Append("<span class=\"circle_ball1\">" + booking.ExtraNumber + "</span>");
                    builder.Append("</td>");

                    builder.Append("<td>" + booking.Quantity + "</td>");
                    builder.Append("<td class=\"center\" title=\"" + booking.TransactionCode + "\">" + transactionCode + "</td>");
                    builder.Append("<td class=\"center\">" + booking.CreateDate.ToString("MM/dd/yyyy HH:mm:ss") + "</td>");
                    builder.Append(" <td class=\"center\">" + booking.OpenDate.ToString(OpenDateResult.OPENDATERESULT) + "</td>");
                    builder.Append("<td class=\"center\"><span class=\"label-success label label-default\">" + statusName + "</span></td>");
                    builder.Append(" <td>");
                    if (booking.Status == 0)
                    {
                        builder.Append("<a class=\"btn btn-info btn-sm\" href=\"javascript:void(0)\" onclick=\"ConfirmBookingMegaball('" + booking.BookingID + "','1','" + booking.MemberID + "','" + booking.NumberWin + "')\" title=\"" + @titleStatus + "\"><i class=\"glyphicon glyphicon-edit icon-white\"></i>Confirm</a>");
                        builder.Append("<a class=\"btn btn-warning btn-sm\" href=\"javascript:void(0)\" onclick=\"ConfirmBookingMegaball('" + booking.BookingID + "','2','" + booking.MemberID + "','" + booking.NumberWin + "')\" title=\"Delete\"><i class=\"glyphicon glyphicon-trash icon-white\"></i>Delete</a>");
                    }
                    else if (booking.Status == 1)
                    {
                        builder.Append("<a class=\"btn btn-warning btn-sm\" href=\"javascript:void(0)\" onclick=\"ConfirmBookingMegaball('" + booking.BookingID + "','2','" + booking.MemberID + "','" + booking.NumberWin + "')\" title=\"Delete\"><i class=\"glyphicon glyphicon-trash icon-white\"></i>Delete</a>");
                    }
                    builder.Append("</td>");
                    builder.Append("</tr>");
                }

                int totalPage = totalRecord / intPageSize;
                int balance = totalRecord % intPageSize;
                if (balance != 0)
                {
                    totalPage += 1;
                }
                if (totalPage > 1)
                {
                    for (int m = 1; m <= totalPage; m++)
                    {

                        if (m == page)
                        {
                            builderPaging.Append("<li class=\"active\"><a href=\"javascript:void(0)\">" + m + "</a></li>");
                        }
                        else
                        {
                            builderPaging.Append("<li><a href=\"javascript:void(0)\" onclick=\"PagingSearchBookingMegaball('" + m + "')\">" + m + "</a></li>");
                        }
                    }
                }
            }
            else
            {
                builder.Append("<tr><td colspan=\"9\">No result found</td></tr>");
            }
            SearchObj obj = new SearchObj();
            obj.ContentResult = builder.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = totalRecord;
            string json = JsonConvert.SerializeObject(obj);

            return json;
        }
        [Authorization]
        [HttpGet]
        public string ExportListBookingMegaballTicket(int memberID, string fromDate, string toDate, int page, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            HSSFWorkbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("LIST BOOKING MEGABALL TICKET");
            Row rowLogo = sheet.CreateRow(0);

            rowLogo.CreateCell(0).SetCellValue("LIST BOOKING MEGABALL TICKET - LOTTERY");
            rowLogo.HeightInPoints = 20;
            CellStyle styleLogo = workbook.CreateCellStyle();
            styleLogo.Alignment = HorizontalAlignment.CENTER;
            NPOI.SS.UserModel.Font font = workbook.CreateFont();
            font.FontHeightInPoints = 12;
            font.FontName = "Times New Roman";
            font.Boldweight = (short)FontBoldWeight.BOLD;

            styleLogo.SetFont(font);
            rowLogo.GetCell(0).CellStyle = styleLogo;

            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

            sheet.SetColumnWidth(0, 5 * 256);
            sheet.SetColumnWidth(1, 40 * 256);
            sheet.SetColumnWidth(2, 35 * 256);
            sheet.SetColumnWidth(3, 15 * 256);
            sheet.SetColumnWidth(4, 15 * 256);
            sheet.SetColumnWidth(5, 25 * 256);
            sheet.SetColumnWidth(6, 25 * 256);
            sheet.SetColumnWidth(7, 15 * 256);
            Row titleList = sheet.CreateRow(2);

            titleList.CreateCell(0).SetCellValue("STT");
            titleList.HeightInPoints = 22;
            CellStyle styleTitleList = workbook.CreateCellStyle();
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            NPOI.SS.UserModel.Font fontTitleList = workbook.CreateFont();
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            fontTitleList.Color = (short)NPOI.HSSF.Util.HSSFColor.WHITE.index;
            //NPOI.HSSF.UserModel.HSSFTextbox.LINESTYLE_SOLID
            styleTitleList.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;
            // styleTitleList.FillPattern = FillPatternType.THICK_VERT_BANDS;
            styleTitleList.FillPattern = FillPatternType.THIN_FORWARD_DIAG;
            styleTitleList.FillPattern = FillPatternType.THIN_BACKWARD_DIAG;
            styleTitleList.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;

            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(0).CellStyle = styleTitleList;

            titleList.CreateCell(1).SetCellValue("Transaction Code");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(1).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(1).CellStyle = styleTitleList;


            titleList.CreateCell(2).SetCellValue("Email");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(2).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(2).CellStyle = styleTitleList;

            titleList.CreateCell(3).SetCellValue("Ticket");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.CENTER;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(3).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(3).CellStyle = styleTitleList;

            titleList.CreateCell(4).SetCellValue("Quantity");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.CENTER;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(4).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(4).CellStyle = styleTitleList;

            titleList.CreateCell(5).SetCellValue("Buy Date");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.CENTER;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(5).CellStyle = styleTitleList;

            titleList.CreateCell(6).SetCellValue("Open Date");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.CENTER;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(6).CellStyle = styleTitleList;


            titleList.CreateCell(7).SetCellValue("Status");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(7).CellStyle = styleTitleList;


            int rowNext = 3;
            int number = 1;

            DateTime? fromD = null;
            DateTime? toD = null;

            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int intPageSize = 300;
            int start = 0, end = 300;
            int totalRecord = 0;
            #region edit
            //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

            //if (fromDate == string.Empty)
            //{
            //    fromD = DateTime.Parse("01/01/1990");
            //    toD = DateTime.Parse("01/01/1990");
            //}
            //else if (toDate == string.Empty)
            //{
            //    string[] arrFrom = fromDate.Split('/');
            //    if (arrFrom != null && arrFrom.Length > 0)
            //    {
            //        string m = arrFrom[0];
            //        string d = arrFrom[1];
            //        string y = arrFrom[2];
            //        string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
            //        //String dateFrom = fromD.ToString("MM/dd/yyyy");
            //        fromD = DateTime.Parse(dateFrom);
            //    }

            //    toDate = toD.ToString();
            //    string[] arrTo = toDate.Split('/');
            //    if (arrTo != null && arrTo.Length > 0)
            //    {
            //        string m = arrTo[0];
            //        string d = arrTo[1];
            //        string y = arrTo[2];
            //        string dateTo = m + "/" + d + "/" + y;
            //        //String dateTo = toD.ToString("MM/dd/yyyy");
            //        toD = DateTime.Parse(dateTo);
            //    }
            //}
            //else
            //{
            //    string[] arrFrom = fromDate.Split('/');
            //    if (arrFrom != null && arrFrom.Length > 0)
            //    {
            //        string m = arrFrom[0];
            //        string d = arrFrom[1];
            //        string y = arrFrom[2];
            //        string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
            //        fromD = DateTime.Parse(dateFrom);

            //    }
            //    string[] arrTo = toDate.Split('/');
            //    if (arrTo != null && arrTo.Length > 0)
            //    {
            //        string m = arrTo[0];
            //        string d = arrTo[1];
            //        string y = arrTo[2];
            //        string dateTo = m + "/" + d + "/" + y + " 23:59:00";
            //        toD = DateTime.Parse(dateTo);

            //    }
            //}
            #endregion

            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                fromD = DateTime.Parse("01/01/1990 12:01:01");
                toD = DateTime.Now;
            }
            else if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
            }
            else
            {
                if (string.IsNullOrEmpty(fromDate))
                {
                    if (string.IsNullOrEmpty(toDate))
                    {
                        toD = DateTime.Parse("01/01/1990");
                    }
                    else
                    {
                        fromD = DateTime.Parse("01/01/1990");
                        toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                    }
                }
                else
                {
                    fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                    toD = DateTime.Now;
                }
            }
            if (page >= 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = repository.ListAllBookingBySearch(memberID, fromD, toD, status, start, end);
                if (lstBooking != null && lstBooking.Count() > 0)
                {
                    totalRecord = lstBooking.ElementAt(0).TotalRecord;
                    foreach (BookingMegaballBO booking in lstBooking)
                    {
                        string titleStatus = "";
                        string statusName = "";
                        if (booking.Status == 0)
                        {
                            statusName = "Not confirm";
                            titleStatus = "Confirm";
                        }
                        else if (booking.Status == 1)
                        {
                            statusName = "Confirmed";
                            titleStatus = "Actived";
                        }
                        else if (booking.Status == 2)
                        {
                            statusName = "Deleted";
                            titleStatus = "";
                        }
                        Row rowListInvoice = sheet.CreateRow(rowNext);

                        rowListInvoice.CreateCell(0).SetCellValue(number);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleNumber = workbook.CreateCellStyle();
                        styleNumber.Alignment = HorizontalAlignment.LEFT;
                        NPOI.SS.UserModel.Font fontNumber = workbook.CreateFont();
                        fontNumber.FontHeightInPoints = 12;
                        fontNumber.FontName = "Times New Roman";
                        styleNumber.SetFont(fontNumber);
                        rowListInvoice.GetCell(0).CellStyle = styleNumber;
                        try
                        {
                            rowListInvoice.CreateCell(1).SetCellValue(booking.TransactionCode);
                            rowListInvoice.HeightInPoints = 20;
                            CellStyle styleDate = workbook.CreateCellStyle();
                            styleDate.Alignment = HorizontalAlignment.LEFT;
                            NPOI.SS.UserModel.Font fontDate = workbook.CreateFont();
                            fontDate.FontHeightInPoints = 12;
                            fontDate.FontName = "Times New Roman";
                            styleDate.SetFont(fontDate);
                            rowListInvoice.GetCell(1).CellStyle = styleDate;
                        }
                        catch { }
                        try
                        {
                            rowListInvoice.CreateCell(2).SetCellValue(booking.Email);
                            rowListInvoice.HeightInPoints = 20;
                            CellStyle styleDate2 = workbook.CreateCellStyle();
                            styleDate2.Alignment = HorizontalAlignment.LEFT;
                            NPOI.SS.UserModel.Font fontDate2 = workbook.CreateFont();
                            fontDate2.FontHeightInPoints = 12;
                            fontDate2.FontName = "Times New Roman";
                            styleDate2.SetFont(fontDate2);
                            rowListInvoice.GetCell(2).CellStyle = styleDate2;
                        }
                        catch { }
                        try
                        {
                            string strnumber = booking.FirstNumber + " " + booking.SecondNumber + " " + booking.ThirdNumber + " " + booking.FourthNumber + " " + booking.FivethNumber + " " + booking.ExtraNumber;
                            rowListInvoice.CreateCell(3).SetCellValue(strnumber);
                            rowListInvoice.HeightInPoints = 20;
                            CellStyle styleDate4 = workbook.CreateCellStyle();
                            styleDate4.Alignment = HorizontalAlignment.CENTER;
                            NPOI.SS.UserModel.Font fontDate4 = workbook.CreateFont();
                            fontDate4.FontHeightInPoints = 12;
                            fontDate4.FontName = "Times New Roman";
                            styleDate4.SetFont(fontDate4);
                            rowListInvoice.GetCell(3).CellStyle = styleDate4;
                        }
                        catch { }
                        rowListInvoice.CreateCell(4).SetCellValue(booking.Quantity);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleDate5 = workbook.CreateCellStyle();
                        styleDate5.Alignment = HorizontalAlignment.CENTER;
                        NPOI.SS.UserModel.Font fontDate5 = workbook.CreateFont();
                        fontDate5.FontHeightInPoints = 12;
                        fontDate5.FontName = "Times New Roman";
                        styleDate5.SetFont(fontDate5);
                        rowListInvoice.GetCell(4).CellStyle = styleDate5;

                        //Row rowDate = sheet.CreateRow(rowNext);
                        rowListInvoice.CreateCell(5).SetCellValue(booking.CreateDate.ToString("MM/dd/yyyy HH:mm:ss"));
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleEmail = workbook.CreateCellStyle();
                        styleEmail.Alignment = HorizontalAlignment.CENTER;
                        NPOI.SS.UserModel.Font fontEmail = workbook.CreateFont();
                        fontEmail.FontHeightInPoints = 12;
                        fontEmail.FontName = "Times New Roman";
                        styleEmail.SetFont(fontEmail);
                        rowListInvoice.GetCell(5).CellStyle = styleEmail;


                        //Row rowOrderID = sheet.CreateRow(rowNext);
                        rowListInvoice.CreateCell(6).SetCellValue(booking.OpenDate.ToString(OpenDateResult.OPENDATERESULT));
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleOrderID = workbook.CreateCellStyle();
                        styleOrderID.Alignment = HorizontalAlignment.CENTER;
                        NPOI.SS.UserModel.Font fontOrderID = workbook.CreateFont();
                        fontOrderID.FontHeightInPoints = 12;
                        fontOrderID.FontName = "Times New Roman";
                        styleOrderID.SetFont(fontOrderID);
                        rowListInvoice.GetCell(6).CellStyle = styleOrderID;

                        rowListInvoice.CreateCell(7).SetCellValue(statusName);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleEmail3 = workbook.CreateCellStyle();
                        styleEmail3.Alignment = HorizontalAlignment.LEFT;
                        NPOI.SS.UserModel.Font fontEmail3 = workbook.CreateFont();
                        fontEmail3.FontHeightInPoints = 12;
                        fontEmail3.FontName = "Times New Roman";
                        styleEmail.SetFont(fontEmail3);
                        rowListInvoice.GetCell(7).CellStyle = styleEmail3;

                        number++;
                        rowNext++;
                    }

                }
            }
            catch { }
            string fileNameExcel = "Listbookingticket_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + ".xls";
            string filename = Server.MapPath("~/FileExcels/BookingMegaballTicket/" + fileNameExcel);
            using (var fileData = new FileStream(filename, FileMode.Create))
            {
                workbook.Write(fileData);
            }
            return fileNameExcel;
        }

    }
}
