﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BBH.Lotte.CLP.Repository;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain;
using System.Text;
using BBH.Lotte.CMSAdmin.Models;
using Newtonsoft.Json;
using BBH.Lotte.CLP.CMSAdmin.Models;
using System.Configuration;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class CompanyInfomationController : Controller
    {
        //
        // GET: /CompanyInfomation/
        [Dependency]
        protected ICompanyInformationServices repository { get; set; }

        [Dependency]
        protected IMemberServices repositoryMember { get; set; }

        int groupIDAdmin = 0;
        [Authorization]
        public ActionResult Index(string p)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            int totalRecord = 0;
            int intPageSize = 10;
            int start = 0, end = 10;
            //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

            int page = 1;
            try
            {
                if (p != null && p != string.Empty)
                {
                    page = int.Parse(p);
                }
            }
            catch
            {

            }
            if (page > 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<CompanyInformationBO> lstCompany = repository.ListAllCompanyPaging(start, end);
            ViewData[ViewDataKey.VIEWDATA_LISTCOMPANY] = lstCompany;

            if (lstCompany != null && lstCompany.Count() > 0)
            {
                totalRecord = lstCompany.ElementAt(0).TotalRecord;
            }
            TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;


            return View();
        }
        [Authorization]
        [HttpGet]
        public string SearchCompany(string keyword)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();

            int totalRecord = 0;
            int intPageSize = 10;
            int start = 0, end = 10;
            int page = 1;

            if (page >= 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<CompanyInformationBO> lstCompany = repository.GetListCompanyBySearch(keyword);
            if (lstCompany != null && lstCompany.Count() > 0)
            {
                totalRecord = lstCompany.ElementAt(0).TotalRecord;
                foreach (CompanyInformationBO company in lstCompany)
                {
                    const int maxlenght = 40;
                    string dot = "...";
                    string description = company.Description.ToString();
                    if (description.Length > maxlenght)
                    {
                        description = description.Substring(0, maxlenght) + "" + dot;
                    } 

                   builder.Append("<tr id=\"trGroup_@company.MemberID\" class=\"none-top-border\">");
                    builder.Append("<td class=\"center\">"+company.MemberID+"</td>");
                                                    
                    builder.Append("<td class=\"center\">"+company.CompanyName+"</td>");
                    builder.Append("<td class=\"center\">"+company.Address+"</td>");
                    builder.Append("<td class=\"center\">"+company.Phone+"</td>");
                    builder.Append("<td class=\"center\">"+company.Email+"</td>");
                    builder.Append("<td class=\"center\"><a href=\"#\">"+company.Website+"</a></td>");
                    builder.Append("<td class=\"center\" title=\""+company.Description+"\">" + description + "</td>");
                                                 
                    builder.Append("</tr>");
                }
                int totalPage = totalRecord / intPageSize;
                int balance = totalRecord % intPageSize;
                if (balance != 0)
                {
                    totalPage += 1;
                }

                if (totalPage > 1)
                {

                    for (int m = 1; m <= totalPage; m++)
                    {

                        if (m == page)
                        {
                            builderPaging.Append("<li class=\"active\"><a href=\"javascript:void(0)\" >" + m + "</a></li>");
                        }
                        else
                        {
                            builderPaging.Append("<li ><a href=\"javascript:void(0)\" onclick=\"PagingSearchCompany('" + m + "')\" >" + m + "</a></li>");

                        }
                    }
                }

            }
            else
            {
                builder.Append("<tr><td colspan=\"6\">No result found</td></tr>");
            }

            SearchObj obj = new SearchObj();
            obj.ContentResult = builder.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = totalRecord;
            string json = JsonConvert.SerializeObject(obj);

            return json;
        }
        [Authorization]
        [HttpGet]
        public string ListMemberByMemberID(int memberID)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            StringBuilder builder = new StringBuilder();
            IEnumerable<MemberBO> lstMember = repositoryMember.GetMemberByID(memberID);
            if (lstMember != null && lstMember.Count() > 0)
            {
                foreach (MemberBO member in lstMember)
                {                   
                    //builder.Append("<tr id=\"trGroup_"+member.MemberID+"\" class=\"none-top-border\">");
                    //builder.Append("<td class=\"center\">"<a onclick = "ShowPopupDetailCompany('@member.MemberID')" data - toggle = "modal" data - target = "#standardModal" > + member.MemberID +"</a></td>");
                    //builder.Append("<td class=\"center\">" + member.Email + "</td>");
                    //builder.Append("<td class=\"center\">" + member.FullName + "</td>");
                    //builder.Append("<td class=\"center\">" + member.CreateDate + "</td>");
                    //builder.Append("<td class=\"center\">" + member.Points + "</td>");
                   
                    //builder.Append("</tr>");
                }
            }
            else
            {
                builder.Append("<tr><td colspan=\"7\">CompanyInformation not found</td></tr>");
            }
            return builder.ToString();
        }
    }
}
