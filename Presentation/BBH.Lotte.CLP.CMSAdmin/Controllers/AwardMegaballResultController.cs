﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BBH.Lotte.CLP.Repository;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Microsoft.Practices.Unity;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CMSAdmin.Models;
using System.Configuration;
using BBH.Lotte.CLP.Shared;
using BBC.Core.Database;
using BBC.Core.Common.Utils;
using BBH.Lotte.CLP.CMSAdmin.Models;
using BBC.Core.Common.Log;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.Wallet.Repository;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class AwardMegaballResultController : Controller
    {
        enum Days { Saturday = 7, Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6 };

        string DayOpen = ConfigurationManager.AppSettings[KeyManager.DayOpen];

        string Number49 = ConfigurationManager.AppSettings[ViewDataKey.NUMBER49];
        string Number26 = ConfigurationManager.AppSettings[ViewDataKey.NUMBER26];

        int TimeClosedBuyTicket = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_CLOSED_COUNT_DOWN]);
        int TimeOpenBuyTicket = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_OPENED_COUNT_DOWN]);

        int PercentRevenueForFreeTicket = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForFreeTicket]);//2
        int PercentRevenueForAward = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForAward]);//40;
        int PercentRevenueForOper = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForOper]);//18;
        int PercentRevenueForBusinessActivity = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForBusinessActivity]);//20;
        int PercentRevenueForJacpot = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForJacpot]);//20;

        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];
        //sysAdmin
        string sysAdmin_UserID = ConfigurationManager.AppSettings[KeyManager.SysAdminID];
        string sysAdmin_Address = ConfigurationManager.AppSettings[KeyManager.SysAdminWalletAddress];
        //sysFreeTicket
        string sysFreeTicket_UserID = ConfigurationManager.AppSettings[KeyManager.FreeWalletID];
        string sysFreeTicket_Address = ConfigurationManager.AppSettings[KeyManager.FreeWalletAddress];
        //sysMgmt
        string sysMgmt_UserID = ConfigurationManager.AppSettings[KeyManager.sysMgmtID];
        string sysMgmt_Address = ConfigurationManager.AppSettings[KeyManager.SysMgmtWalletAddress];

        //sysProgressivePrize 
        string sysProgressivePrize_UserID = ConfigurationManager.AppSettings[KeyManager.ProgressivePrizeID];
        string sysProgressivePrize_Address = ConfigurationManager.AppSettings[KeyManager.ProgressivePrizeWalletAddress];
        //sysLoan
        string sysLoan_UserID = ConfigurationManager.AppSettings[KeyManager.LoanWalletID];
        string sysLoan_Address = ConfigurationManager.AppSettings[KeyManager.LoanWalletAddress];

        //sysAward
        string sysAward_UserID = ConfigurationManager.AppSettings[KeyManager.AwardWalletID];
        string sysAward_Address = ConfigurationManager.AppSettings[KeyManager.AwardWalletAddress];

        //sysJackpot
        string sysJackpot_UserID = ConfigurationManager.AppSettings[KeyManager.JackPotID];
        string sysJackpot_Address = ConfigurationManager.AppSettings[KeyManager.JackPotWalletAddress];

        //sysFeePrize
        string sysFeePrize_UserID = ConfigurationManager.AppSettings[KeyManager.FeePrizeID];
        string sysFeePrize_Address = ConfigurationManager.AppSettings[KeyManager.FeePrizeWalletAddress];

        //sysFeeTxn
        string sysFeeTxn_UserID = ConfigurationManager.AppSettings[KeyManager.SysFeeTxnID];
        string sysFeeTxn_Address = ConfigurationManager.AppSettings[KeyManager.SysFeeTxnWalletAddress];

        //[Dependency]
        //protected IAwardServices repository { get; set; }
        [Dependency]
        protected IAwardMegaballServices repository { get; set; }

        [Dependency]
        protected IAwardServices repositoryAward { get; set; }
        [Dependency]
        protected IMemberServices repositoryMember { get; set; }

        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }

        [Dependency]
        protected IDrawServices drawServices { get; set; }

        [Dependency]
        protected ITicketConfigServices ticketConfigServices { get; set; }
        [Dependency]
        protected IBookingMegaballServices bookingMegaball { get; set; }

        int groupIDAdmin = 0;
        [Authorization]
        public ActionResult Index(string p)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            int totalRecord = 0;
            int intPageSize = 10;
            int start = 1, end = 1000;

            int page = 1;
            try
            {
                if (p != null && p != string.Empty)
                {
                    page = int.Parse(p);
                }
            }
            catch
            {

            }
            if (page > 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;

            int d = DateTime.Now.Day;
            int m = DateTime.Now.Month;
            int y = DateTime.Now.Year;
            string fromD = m + "/" + d + "/" + y + " 00:00:00";
            string toD = m + "/" + d + "/" + y + " 23:59:00";
            try
            {
                fromDate = DateTime.Parse(fromD);
                toDate = DateTime.Parse(toD);
            }
            catch
            {
                fromD = d + "/" + m + "/" + y + " 00:00:00";
                toD = d + "/" + m + "/" + y + " 23:59:00";
                fromDate = DateTime.Parse(fromD);
                toDate = DateTime.Parse(toD);
            }
            IEnumerable<AwardBO> lstAward = repositoryAward.GetListAward(1, 100);
            ViewData[ViewDataKey.VIEWDATA_LISTAWARD] = lstAward;

            IEnumerable<AwardMegaballBO> lstAwardNumber = repository.GetListAwardNumberByDate(1, 200, fromDate, toDate);
            ViewData[ViewDataKey.VIEWDATA_LISTAWARDMEGABALL] = lstAwardNumber;

            IEnumerable<MemberBO> lstMember = repositoryMember.GetListMember(1, 100);
            ViewData[ViewDataKey.VIEWDATA_LISTMEMBER] = lstMember;

            return View();
        }
        [Authorization]
        [HttpPost]
        public string SaveAwardNumber(int numberID, int awardID, string strBlockHash, float jackPot)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {

                    string strHtml = string.Empty;
                    List<int> lstResultNumber = new List<int>();
                    try
                    {
                        char[] array = strBlockHash.ToCharArray();
                        if (array.Length > 0)
                        {
                            List<string> lst = new List<string>();
                            string str = string.Empty;
                            int k = 0;
                            for (int i = array.Length - 1; i >= 0; i--)
                            {
                                str = str + array[i];
                                k++;
                                if (k % 4 == 0)
                                {
                                    string number = ReverseNumber(str);
                                    k = 0;
                                    lst.Add(number);
                                    str = string.Empty;
                                }
                            }


                            if (lst != null && lst.Count > 0)
                            {
                                foreach (var item in lst)
                                {
                                    int test = Convert.ToInt32(item, 16);
                                    if (lstResultNumber.Count < 5)
                                    {
                                        test = test % int.Parse(Number49);
                                        if (!lstResultNumber.Contains(test))
                                        {
                                            if (test == 0)
                                            {
                                                test = int.Parse(Number49);
                                            }
                                            lstResultNumber.Add(test);
                                        }
                                    }
                                    else if (lstResultNumber.Count == 5)
                                    {
                                        test = test % int.Parse(Number26);
                                        if (test == 0)
                                        {
                                            test = int.Parse(Number26);
                                        }
                                        lstResultNumber.Add(test);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    catch { }
                    AwardMegaballBO awardMegaball = new AwardMegaballBO();
                    if (numberID > 0)
                    {
                        DateTime dtNow = DateTime.Now.ToUniversalTime();

                        if (lstResultNumber != null && lstResultNumber.Count() > 0)
                        {
                            awardMegaball.NumberID = numberID;
                            awardMegaball.AwardID = awardID;
                            awardMegaball.CreateDate = dtNow;
                            awardMegaball.IsActive = 1;
                            awardMegaball.IsDelete = 0;
                            awardMegaball.Priority = 1;
                            awardMegaball.FirstNumber = lstResultNumber[0];
                            awardMegaball.SecondNumber = lstResultNumber[1];
                            awardMegaball.ThirdNumber = lstResultNumber[2];
                            awardMegaball.FourthNumber = lstResultNumber[3];
                            awardMegaball.FivethNumber = lstResultNumber[4];
                            awardMegaball.ExtraNumber = lstResultNumber[5];
                            awardMegaball.GenCode = strBlockHash;
                            awardMegaball.JackPot = decimal.Parse(jackPot.ToString());
                            string strParameterObj = Algorithm.EncryptionObjectRSA(awardMegaball);
                            ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                            objRsa2.Token = objRsa.Token;
                            objRsa2.UserName = objRsa.UserName;

                            objResult = repository.UpdateAwardMegaball(objRsa2);
                            if (!objResult.IsError)
                            {
                                strHtml = "success";
                            }
                            else
                            {
                                strHtml = "faile";
                            }
                        }
                    }
                    else if (numberID == 0)
                    {

                        if (lstResultNumber != null && lstResultNumber.Count() > 0)
                        {
                            awardMegaball.NumberID = numberID;
                            awardMegaball.AwardID = awardID;
                            awardMegaball.CreateDate = DateTime.Now;
                            awardMegaball.IsActive = 1;
                            awardMegaball.IsDelete = 0;
                            awardMegaball.Priority = 1;
                            awardMegaball.FirstNumber = lstResultNumber[0];
                            awardMegaball.SecondNumber = lstResultNumber[1];
                            awardMegaball.ThirdNumber = lstResultNumber[2];
                            awardMegaball.FourthNumber = lstResultNumber[3];
                            awardMegaball.FivethNumber = lstResultNumber[4];
                            awardMegaball.ExtraNumber = lstResultNumber[5];
                            awardMegaball.GenCode = strBlockHash;
                            awardMegaball.JackPot = decimal.Parse(jackPot.ToString());
                            string strParameterObj = Algorithm.EncryptionObjectRSA(awardMegaball);
                            ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                            objRsa2.Token = objRsa.Token;
                            objRsa2.UserName = objRsa.UserName;
                            objResult = repository.InsertAwardMegaball(objRsa2);
                            if (!objResult.IsError)
                            {
                                strHtml = "success";
                            }
                            else
                            {
                                strHtml = "faile";
                            }
                        }
                    }
                }
            }
            return result;
        }
        [Authorization]
        private string ReverseNumber(string number)
        {
            string str = string.Empty;
            char[] array = number.ToCharArray();
            if (array.Length > 0)
            {
                for (int i = array.Length - 1; i >= 0; i--)
                {
                    str = str + array[i];
                }
            }
            return str;
        }
        [Authorization]
        [HttpPost]
        public string LockAndUnLockAwardNumber(int numberID, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {

                    int statusNew = -1;
                    if (status == 1)
                    {
                        statusNew = 0;
                    }
                    else
                    {
                        statusNew = 1;
                    }
                    AwardMegaballBO objAwardMegaball = new AwardMegaballBO();
                    objAwardMegaball.IsActive = statusNew;
                    objAwardMegaball.NumberID = numberID;
                    string strParameterObj = Algorithm.EncryptionObjectRSA(objAwardMegaball);
                    ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objRsa.UserName;

                    objResult = repositoryAward.LockAndUnlockAwardNumber(objRsa2);
                    if (!objResult.IsError)
                    {
                        result = "success";
                    }
                }
            }
            return result;
        }
        [Authorization]
        [HttpPost]
        public JsonResult SearchAwardByDate(string searchDay)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            StringBuilder strAward = new StringBuilder();
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    DateTime fromDate = DateTime.Now;
                    DateTime toDate = DateTime.Now;

                    // string strWinnerNumber = string.Empty;
                    try
                    {
                        if (!string.IsNullOrEmpty(searchDay))
                        {
                            string[] arr = searchDay.Split('/');
                            if (arr != null && arr.Length > 0)
                            {
                                string m = arr[0];
                                string d = arr[1];
                                string y = arr[2];
                                string fromD = m + "/" + d + "/" + y + " 00:00:00";
                                string toD = m + "/" + d + "/" + y + " 23:59:00";
                                try
                                {
                                    fromDate = DateTime.Parse(fromD);
                                    toDate = DateTime.Parse(toD);
                                }
                                catch
                                {
                                }
                                // IEnumerable<AwardMegaballBO> lstAwardMegaball = repository.ListAllAwardMegaballBySearch(fromDate, toDate, 1, 200);
                                IEnumerable<AwardMegaballBO> lstAwardMegaball = repository.GetListAwardNumberByDate(1, 200, fromDate, toDate);

                                if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                                {
                                    foreach (AwardMegaballBO awardMegaball in lstAwardMegaball)
                                    {
                                        string titleStatus = "Lock";
                                        string status = "Active";
                                        if (awardMegaball.IsActive == 0)
                                        {
                                            status = "InActive";
                                            titleStatus = "Unlock";
                                        }
                                        MemberBO member = new MemberBO();
                                        int num1, num2, num3, num4, num5, extranum;
                                        num1 = awardMegaball.FirstNumber;
                                        num2 = awardMegaball.SecondNumber;
                                        num3 = awardMegaball.ThirdNumber;
                                        num4 = awardMegaball.FourthNumber;
                                        num5 = awardMegaball.FivethNumber;
                                        extranum = awardMegaball.ExtraNumber;

                                        string NumberWiner = num1.ToString() + " " + num2.ToString() + " " + num3.ToString() + " " + num4.ToString() + " " + num5.ToString();

                                        strAward.Append("<tr id=\"trGroup_" + awardMegaball.NumberID + "\" class=\"none-top-border\">");
                                        strAward.Append("<td class=\"center\">" + awardMegaball.NumberID + "</td>");
                                        strAward.Append("<td class=\"center\">" + awardMegaball.AwardNameEnglish + "</td>");
                                        strAward.Append("<td class=\"center\">");
                                        strAward.Append("<span class=\"circle1\">" + num1 + "</span> ");
                                        strAward.Append("<span class=\"circle1\">" + num2 + "</span>");
                                        strAward.Append("<span class=\"circle1\">" + num3 + "</span>");
                                        strAward.Append("<span class=\"circle1\">" + num4 + "</span>");
                                        strAward.Append("<span class=\"circle1\">" + num5 + "</span> ");
                                        strAward.Append("<span class=\"circle_ball\">" + extranum + "</span></td>");
                                        strAward.Append("<td class=\"center\">" + awardMegaball.JackPot + "</td>");
                                        strAward.Append("<td class=\"center\">" + awardMegaball.CreateDate + "</td>");
                                        strAward.Append("<td class=\"center\">");
                                        strAward.Append("<span class=\"label-success label label-default\" title=\"'" + titleStatus + "'\">" + status + "</span>");
                                        strAward.Append("</td>");
                                        strAward.Append("<td class=\"center\">");
                                        strAward.Append("<a class='btn btn-info btn-sm' title='" + awardMegaball.NumberID + "' href='javascript:void(0)' onclick='getWinnerMemberByNumberID(" + awardMegaball.NumberID + ")' ><i class='glyphicon glyphicon-edit icon-white'></i>&nbsp;Winner</a>");
                                        strAward.Append("</td>");
                                        //strAward.Append("</a>");
                                        strAward.Append("</td>");
                                        strAward.Append("</tr>");

                                    }

                                    //Gen Html Winner Member
                                    //strWinnerNumber = GenHtmlWinnerMember(lstAwardMegaball.ToList()[0].ListWinnerMember);
                                }
                                else
                                {
                                    strAward.Append("<tr><td colspan=\"7\"> <strong> No result found </strong></td></tr>");
                                    //  strWinnerNumber = "<tr><td colspan=\"8\"> <strong> No result found </strong></td></tr>";
                                }
                            }
                        }



                    }
                    catch (Exception objEx)
                    {
                        return Json(new { isError = 1 }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { isError = 0, strAward = strAward.ToString() }, JsonRequestBehavior.AllowGet);//, strWinnerNumber = strWinnerNumber.ToString() 
        }
        [Authorization]
        public string GenHtmlWinnerMember(List<WinnerBO> lstAwardNumber)
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                if (lstAwardNumber != null && lstAwardNumber.Count > 0)
                {
                    foreach (WinnerBO award in lstAwardNumber)
                    {
                        int num1, num2, num3, num4, num5, extranum;
                        num1 = award.FirstNumber;
                        num2 = award.SecondNumber;
                        num3 = award.ThirdNumber;
                        num4 = award.FourthNumber;
                        num5 = award.FivethNumber;
                        extranum = award.ExtraNumber;

                        builder.Append("<tr id=\"trGroup_" + award.BookingID + "\" class=\"none-top-border\">");
                        builder.Append("<td><p>" + award.BookingID + "</p></td>");
                        builder.Append("<td><p>" + award.Email + "</p></td>");
                        builder.Append("<td><p>" + award.TransactionCode + "</p></td>");
                        builder.Append("<td class=\"center\">");
                        builder.Append("<span class=\"circle1\">" + num1 + "</span> ");
                        builder.Append("<span class=\"circle1\">" + num2 + "</span>");
                        builder.Append("<span class=\"circle1\">" + num3 + "</span>");
                        builder.Append("<span class=\"circle1\">" + num4 + "</span>");
                        builder.Append("<span class=\"circle1\">" + num5 + "</span> ");
                        builder.Append("<span class=\"circle_ball\">" + extranum + "</span></td>");
                        //<td class="center">1027</td>
                        builder.Append("<td class='center'>" + award.JackPot + "</td>");
                        builder.Append("<td><p>" + award.CreateDate.ToString("MM/dd/yyyy HH:mm:ss") + "</p></td>");
                        builder.Append("<td><p>" + award.OpenDate.ToString(OpenDateResult.OPENDATERESULT) + "</p></td>");
                        builder.Append("<td><p>" + award.AwardNameEnglish + "</p></td>");
                        builder.Append("</tr>");
                    }
                }
                else
                {
                    builder.Append("<tr><td colspan=\"7\"> <strong> No result found </strong></td></tr>");
                }
            }
            catch (Exception objEx)
            {

                throw;
            }

            return builder.ToString();
        }
        [Authorization]
        [HttpPost]
        public JsonResult SearchWinnerAward(int intNumberID)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            try
            {
                bool bolIsResult = false;
                IEnumerable<WinnerBO> lstAwardNumber = repository.GetListWinner(intNumberID);
                if (lstAwardNumber != null && lstAwardNumber.Count() > 0)
                {
                    foreach (WinnerBO award in lstAwardNumber)
                    {
                        int num1, num2, num3, num4, num5, extranum;
                        num1 = award.FirstNumber;
                        num2 = award.SecondNumber;
                        num3 = award.ThirdNumber;
                        num4 = award.FourthNumber;
                        num5 = award.FivethNumber;
                        extranum = award.ExtraNumber;

                        builder.Append("<tr id=\"trGroup_" + award.BookingID + "\" class=\"none-top-border\">");
                        builder.Append("<td><p>" + award.BookingID + "</p></td>");
                        builder.Append("<td><p>" + award.Email + "</p></td>");
                        builder.Append("<td><p>" + award.TransactionCode + "</p></td>");
                        builder.Append("<td class=\"center\">");
                        builder.Append("<span class=\"circle1\">" + num1 + "</span> ");
                        builder.Append("<span class=\"circle1\">" + num2 + "</span>");
                        builder.Append("<span class=\"circle1\">" + num3 + "</span>");
                        builder.Append("<span class=\"circle1\">" + num4 + "</span>");
                        builder.Append("<span class=\"circle1\">" + num5 + "</span> ");
                        builder.Append("<span class=\"circle_ball\">" + extranum + "</span></td>");
                        builder.Append("<td class='center'>" + award.JackPot + "</td>");
                        builder.Append("<td><p>" + award.CreateDate.ToString("MM/dd/yyyy HH:mm:ss") + "</p></td>");
                        builder.Append("<td><p>" + award.OpenDate.ToString(OpenDateResult.OPENDATERESULT) + "</p></td>");
                        builder.Append("<td><p>" + award.AwardNameEnglish + "</p></td>");
                        builder.Append("</tr>");
                    }

                    bolIsResult = true;
                }
                else
                {
                    builder.Append("<tr><td colspan=\"8\"> <strong> No result found </strong></td></tr>");
                }

                return Json(new { isError = (bolIsResult ? 0 : 2), builder = builder.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objEx)
            {
                return Json(new { isError = 1 }, JsonRequestBehavior.AllowGet);
            }
        }
        [Authorization]
        [HttpGet]
        public string ExportListWinnerAward(int intNumberID, string searchDay)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            HSSFWorkbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("LIST WINNER");
            Row rowLogo = sheet.CreateRow(0);

            rowLogo.CreateCell(0).SetCellValue("LIST WINNER DATE " + searchDay + " - LOTTERY");
            rowLogo.HeightInPoints = 20;
            CellStyle styleLogo = workbook.CreateCellStyle();
            styleLogo.Alignment = HorizontalAlignment.CENTER;
            NPOI.SS.UserModel.Font font = workbook.CreateFont();
            font.FontHeightInPoints = 12;
            font.FontName = "Times New Roman";
            font.Boldweight = (short)FontBoldWeight.BOLD;

            styleLogo.SetFont(font);
            rowLogo.GetCell(0).CellStyle = styleLogo;

            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

            sheet.SetColumnWidth(0, 5 * 256);
            sheet.SetColumnWidth(1, 15 * 256);
            sheet.SetColumnWidth(2, 40 * 256);
            sheet.SetColumnWidth(3, 40 * 256);
            sheet.SetColumnWidth(4, 25 * 256);
            sheet.SetColumnWidth(5, 15 * 256);
            sheet.SetColumnWidth(6, 25 * 256);
            sheet.SetColumnWidth(7, 25 * 256);
            sheet.SetColumnWidth(8, 25 * 256);

            Row titleList = sheet.CreateRow(2);

            titleList.CreateCell(0).SetCellValue("STT");
            titleList.HeightInPoints = 22;
            CellStyle styleTitleList = workbook.CreateCellStyle();
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            NPOI.SS.UserModel.Font fontTitleList = workbook.CreateFont();
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            fontTitleList.Color = (short)NPOI.HSSF.Util.HSSFColor.WHITE.index;
            //NPOI.HSSF.UserModel.HSSFTextbox.LINESTYLE_SOLID
            styleTitleList.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;
            // styleTitleList.FillPattern = FillPatternType.THICK_VERT_BANDS;
            styleTitleList.FillPattern = FillPatternType.THIN_FORWARD_DIAG;
            styleTitleList.FillPattern = FillPatternType.THIN_BACKWARD_DIAG;
            styleTitleList.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(0).CellStyle = styleTitleList;

            titleList.CreateCell(1).SetCellValue("BookingID");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(1).CellStyle = styleTitleList;

            titleList.CreateCell(2).SetCellValue("Email");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(2).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            //titleList.GetCell(2).CellStyle = styleTitleList;

            titleList.CreateCell(3).SetCellValue("Transaction Code");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(3).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            //titleList.GetCell(3).CellStyle = styleTitleList;            

            titleList.CreateCell(4).SetCellValue("Winner number");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.CENTER;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(4).CellStyle = styleTitleList;

            titleList.CreateCell(5).SetCellValue("JackPot(XRP)");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.CENTER;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(5).CellStyle = styleTitleList;

            titleList.CreateCell(6).SetCellValue("Create Date");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(6).CellStyle = styleTitleList;

            titleList.CreateCell(7).SetCellValue("Open Date");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(7).CellStyle = styleTitleList;

            titleList.CreateCell(8).SetCellValue("AwardName");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(8).CellStyle = styleTitleList;

            int rowNext = 3;
            int number = 1;

            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            if (searchDay != string.Empty)
            {
                string[] arr = searchDay.Split('/');
                if (arr != null && arr.Length > 0)
                {
                    string d = arr[0];
                    string m = arr[1];
                    string y = arr[2];
                    string fromD = m + "/" + d + "/" + y + " 00:00:00";
                    string toD = m + "/" + d + "/" + y + " 23:59:00";
                    try
                    {
                        fromDate = DateTime.Parse(fromD);
                        toDate = DateTime.Parse(toD);
                    }
                    catch
                    {
                        fromD = d + "/" + m + "/" + y + " 00:00:00";
                        toD = d + "/" + m + "/" + y + " 23:59:00";
                        fromDate = DateTime.Parse(fromD);
                        toDate = DateTime.Parse(toD);
                    }
                    try
                    {
                        List<WinnerBO> lstAwardNumber = repository.GetListWinner(intNumberID);
                        if (lstAwardNumber != null && lstAwardNumber.Count > 0)
                        {
                            foreach (WinnerBO award in lstAwardNumber)
                            {
                                Row rowListInvoice = sheet.CreateRow(rowNext);

                                rowListInvoice.CreateCell(0).SetCellValue(number);
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleNumber = workbook.CreateCellStyle();
                                styleNumber.Alignment = HorizontalAlignment.LEFT;
                                NPOI.SS.UserModel.Font fontNumber = workbook.CreateFont();
                                fontNumber.FontHeightInPoints = 12;
                                fontNumber.FontName = "Times New Roman";
                                styleNumber.SetFont(fontNumber);
                                rowListInvoice.GetCell(0).CellStyle = styleNumber;

                                rowListInvoice.CreateCell(1).SetCellValue(award.BookingID);
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleBookingID = workbook.CreateCellStyle();
                                styleBookingID.Alignment = HorizontalAlignment.LEFT;
                                NPOI.SS.UserModel.Font fontDate1 = workbook.CreateFont();
                                fontDate1.FontHeightInPoints = 12;
                                fontDate1.FontName = "Times New Roman";
                                styleBookingID.SetFont(fontDate1);
                                rowListInvoice.GetCell(1).CellStyle = styleBookingID;

                                rowListInvoice.CreateCell(2).SetCellValue(award.Email);
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleDate = workbook.CreateCellStyle();
                                styleDate.Alignment = HorizontalAlignment.LEFT;
                                NPOI.SS.UserModel.Font fontDate = workbook.CreateFont();
                                fontDate.FontHeightInPoints = 12;
                                fontDate.FontName = "Times New Roman";
                                styleDate.SetFont(fontDate);
                                rowListInvoice.GetCell(2).CellStyle = styleDate;

                                rowListInvoice.CreateCell(3).SetCellValue(award.TransactionCode);
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleDate2 = workbook.CreateCellStyle();
                                styleDate2.Alignment = HorizontalAlignment.LEFT;
                                NPOI.SS.UserModel.Font fontDate2 = workbook.CreateFont();
                                fontDate2.FontHeightInPoints = 12;
                                fontDate2.FontName = "Times New Roman";
                                styleDate.SetFont(fontDate2);
                                rowListInvoice.GetCell(3).CellStyle = styleDate2;


                                string strnumber = award.FirstNumber + " " + award.SecondNumber + " " + award.ThirdNumber + " " + award.FourthNumber + " " + award.FivethNumber + " " + award.ExtraNumber;
                                rowListInvoice.CreateCell(4).SetCellValue(strnumber);
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleOrderID = workbook.CreateCellStyle();
                                styleOrderID.Alignment = HorizontalAlignment.LEFT;
                                NPOI.SS.UserModel.Font fontOrderID = workbook.CreateFont();
                                fontOrderID.FontHeightInPoints = 12;
                                fontOrderID.FontName = "Times New Roman";
                                styleOrderID.SetFont(fontOrderID);
                                rowListInvoice.GetCell(4).CellStyle = styleOrderID;

                                rowListInvoice.CreateCell(5).SetCellValue(award.JackPot);
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleJackPot = workbook.CreateCellStyle();
                                styleJackPot.Alignment = HorizontalAlignment.LEFT;
                                NPOI.SS.UserModel.Font fontJackPot = workbook.CreateFont();
                                fontJackPot.FontHeightInPoints = 12;
                                fontJackPot.FontName = "Times New Roman";
                                styleDate.SetFont(fontJackPot);
                                rowListInvoice.GetCell(5).CellStyle = styleJackPot;

                                rowListInvoice.CreateCell(6).SetCellValue(award.CreateDate.ToString("MM/dd/yyyy HH:mm:ss"));
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleEmail3 = workbook.CreateCellStyle();
                                styleEmail3.Alignment = HorizontalAlignment.LEFT;
                                NPOI.SS.UserModel.Font fontEmail3 = workbook.CreateFont();
                                fontEmail3.FontHeightInPoints = 12;
                                fontEmail3.FontName = "Times New Roman";
                                styleEmail3.SetFont(fontEmail3);
                                rowListInvoice.GetCell(6).CellStyle = styleEmail3;

                                rowListInvoice.CreateCell(7).SetCellValue(award.OpenDate.ToString(OpenDateResult.OPENDATERESULT));
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleEmail4 = workbook.CreateCellStyle();
                                styleEmail4.Alignment = HorizontalAlignment.LEFT;
                                NPOI.SS.UserModel.Font fontEmail4 = workbook.CreateFont();
                                fontEmail4.FontHeightInPoints = 12;
                                fontEmail4.FontName = "Times New Roman";
                                styleEmail4.SetFont(fontEmail3);
                                rowListInvoice.GetCell(7).CellStyle = styleEmail4;

                                rowListInvoice.CreateCell(8).SetCellValue(award.AwardNameEnglish);
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleEmail5 = workbook.CreateCellStyle();
                                styleEmail5.Alignment = HorizontalAlignment.LEFT;
                                NPOI.SS.UserModel.Font fontEmail5 = workbook.CreateFont();
                                fontEmail5.FontHeightInPoints = 12;
                                fontEmail5.FontName = "Times New Roman";
                                styleEmail5.SetFont(fontEmail5);
                                rowListInvoice.GetCell(8).CellStyle = styleEmail5;

                                number++;
                                rowNext++;
                            }
                        }
                    }
                    catch { }
                }
            }
            string fileNameExcel = "ListWinner_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + ".xls";
            string filename = Server.MapPath("~/FileExcels/AwardWinner/" + fileNameExcel);
            using (var fileData = new FileStream(filename, FileMode.Create))
            {
                workbook.Write(fileData);
            }

            return fileNameExcel;
        }

    }
}