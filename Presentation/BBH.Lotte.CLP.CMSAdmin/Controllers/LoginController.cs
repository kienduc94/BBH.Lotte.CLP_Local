﻿using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Repository;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BBH.Lotte.CMSAdmin.Models;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Net;
using BBC.Core.Database;
using System.IO;
using BBH.Lotte.CLP.CMSAdmin.Models;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        string secretKey = ConfigurationManager.AppSettings["secretKey"];
       
        [Dependency]
        protected IAdminServices repository { get; set; }

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult MemberLogin(string strUsername, string strPassword, string strCaptcha)
        {
            try
            {
                string strResult = string.Empty;
           
                if (strUsername == string.Empty || strUsername.Trim().Length == 0)
                {
                    return Json(new { intTypeError = 1, result = "", username = strUsername, password = strPassword }, JsonRequestBehavior.AllowGet);
                }
                if (strPassword == string.Empty || strPassword.Trim().Length > 50)
                {
                    return Json(new { intTypeError = 2, result = "", username = strUsername, password = strPassword }, JsonRequestBehavior.AllowGet);
                }
                if (strCaptcha == null || strCaptcha.Trim().Length == 0)
                {
                    return Json(new { intTypeError = 3, result = "", username = strUsername, password = strPassword }, JsonRequestBehavior.AllowGet);
                }
                TempData["UserNameLogin"] = strUsername;
             
                var client = new WebClient();
                var response = strCaptcha;
                var result1 = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
                var obj = JObject.Parse(result1);
                var status = (bool)obj.SelectToken("success");
              
                if (status == false)
                {
                    strResult = "errorCaptcha";
                }
                else
                {
                    string passMd5 = Utility.MaHoaMD5(strPassword + "1234");
                    AdminBO admin = repository.LoginManagerAccount(strUsername, passMd5);
                  
                    if (admin == null)
                    {
                    
                        strResult = ResultKey.RESULT_LOGINFAILES;
                    }
                    else
                    {
                      
                        Session[SessionKey.SESSION_USERNAME] = strUsername;
                        Session[SessionKey.SESSION_EMAIL] = admin.Email;
                        Session["GroupIDAdmin"] = admin.GroupID;
                        strResult = ResultKey.RESULT_LOGINSUCCESS;
                    }
                }
                return Json(new { intTypeError = 0, result = strResult, username = strUsername, password = strPassword }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objEx) { return Json(new { intTypeError = 0, result = ResultKey.RESULT_LOGINFAILES, username = strUsername, password = strPassword, messageError = objEx.Message }, JsonRequestBehavior.AllowGet); }
        }
        #region edit
        //public string MemberLogin(string username, string password)
        //{
        //    string strResult = string.Empty;
        //    if (username == string.Empty || password == string.Empty)
        //    {
        //        strResult = ResultKey.RESULT_LOGINFAILES;
        //    }
        //    else
        //    {
        //        string passMd5 = Utility.MaHoaMD5(password + "1234");
        //        AdminBO admin = repository.LoginManagerAccount(username, passMd5);
        //        if (admin == null)
        //        {
        //            strResult = ResultKey.RESULT_LOGINFAILES;
        //        }
        //        else
        //        {
        //            Session[SessionKey.SESSION_USERNAME] = username;
        //            Session[SessionKey.SESSION_GROUPID] = admin.GroupID;
        //            strResult = ResultKey.RESULT_LOGINSUCCESS;
        //        }
        //    }
        //    return strResult;
        //}
        #endregion
        [HttpPost]
        public void LogoutMember()
        {
            Session[SessionKey.SESSION_USERNAME] = null;
        }
    }
}
