﻿using BBH.Lotte.CMSAdmin.Models;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BBH.Lotte.CLP.CMSAdmin.Models;
using System.Configuration;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class ExchangePointController : Controller
    {
        //
        // GET: /ExchangePoint/
        [Dependency]
        protected IExchangePointServices repository { get; set; }

        [Dependency]
        protected IAdminServices repositoryAdmin { get; set; }

        [Dependency]
        protected IMemberServices repositoryMember { get; set; }

        int groupIDAdmin = 0;

        [Authorization]
        public ActionResult Index(string p)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            int totalRecord = 0;
            int intPageSize = 10;
            int start = 0, end = 10;
            //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

            int page = 1;
            try
            {
                if (p != null && p != string.Empty)
                {
                    page = int.Parse(p);
                }
            }
            catch
            {

            }
            if (page > 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<ExchangePointBO> lstExtrangePoint = repository.ListAllExchangePointPaging(start, end);
            ViewData[ViewDataKey.VIEWDATA_LISTEXCHANGEPOINT] = lstExtrangePoint;
            if (lstExtrangePoint != null && lstExtrangePoint.Count() > 0)
            {
                totalRecord = lstExtrangePoint.ElementAt(0).TotalRecord;
            }
            TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;

            IEnumerable<AdminBO> lstAdmin = repositoryAdmin.ListAllAdmin();
            ViewData[ViewDataKey.VIEWDATA_LISTADMIN] = lstAdmin;

            IEnumerable<MemberBO> lstMember = repositoryMember.GetListMember(1, 10000);
            ViewData[ViewDataKey.VIEWDATA_LISTMEMBER] = lstMember;

            return View();
        }
        [Authorization]
        [HttpPost]
        public string LockAndUnlockExchangePoint(int exchangeID, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            int statusNew = -1;
            if (status == 1)
            {
                statusNew = 0;
            }
            else
            {
                statusNew = 1;
            }
            bool rs = repository.LockAndUnlockExchangePoint(exchangeID, statusNew);
            if (rs)
            {
                result = ResultKey.RESULT_SUCCESS;
            }
            return result;
        }
        [Authorization]
        [HttpPost]
        public string UpdateStatusExchangePoint(int exchangeID, int status, DateTime deleteDate, string deleteUser)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            ExchangePointBO exchangePoint = new ExchangePointBO();
            exchangePoint.DeleteUser = deleteUser;
            deleteUser = (string)Session[SessionKey.SESSION_USERNAME];           
            exchangePoint.DeleteDate = deleteDate;
            deleteDate = DateTime.Now;
            bool rs = repository.UpdateStatusExchangePoint(exchangeID, status, deleteDate, deleteUser);
            if (rs)
            {
                result = ResultKey.RESULT_SUCCESS;
            }
            return result;
        }
        [Authorization]
        [HttpPost]
        public string SaveExchangePoint(int exchangeID, string createUser, float pointValue, float bitcoinValue, string email)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            ExchangePointBO exchangePoint = new ExchangePointBO();
            if (pointValue == 0 || bitcoinValue ==0)
            {
                result = ResultKey.RESULT_FAILES;
            }
            else
            {
                if (exchangeID > 0)
                {
                    try
                    {
                        exchangePoint.ExChangeID = exchangeID;
                        exchangePoint.CreateUser = (string)Session[SessionKey.SESSION_USERNAME];
                        exchangePoint.PointValue = pointValue;
                        exchangePoint.BitcoinValue = bitcoinValue;
                        exchangePoint.DeleteUser = string.Empty;
                        exchangePoint.IsActive = 1;
                        exchangePoint.IsDelete = 0;
                        exchangePoint.MemberID = 0;
                        exchangePoint.UpdateDate = DateTime.Now;
                        exchangePoint.UpdateUser = (string)Session[SessionKey.SESSION_USERNAME];
                        exchangePoint.Email = email;

                        bool rs = repository.UpdateExchangePoint(exchangePoint);
                        if (rs)
                        {
                            result = ResultKey.RESULT_SUCCESS;
                        }
                        else
                        {
                            result = ResultKey.RESULT_FAILES;
                        }
                    }
                    catch { }
                }
                else if (exchangeID == 0)
                {
                    try
                    {
                        exchangePoint.ExChangeID = exchangeID;
                        exchangePoint.CreateUser = (string)Session[SessionKey.SESSION_USERNAME];
                        exchangePoint.PointValue = pointValue;
                        exchangePoint.BitcoinValue = bitcoinValue;
                        exchangePoint.CreateDate = DateTime.Now;
                        exchangePoint.DeleteDate = DateTime.Now;
                        exchangePoint.DeleteUser = string.Empty;
                        exchangePoint.IsActive = 1;
                        exchangePoint.IsDelete = 0;
                        exchangePoint.MemberID = 0;
                        exchangePoint.UpdateDate = DateTime.Now;
                        exchangePoint.UpdateUser = string.Empty;

                        bool rs = repository.InsertExchangePoint(exchangePoint);
                        if (rs)
                        {
                            result = ResultKey.RESULT_SUCCESS;
                        }
                        else
                        {
                            result = ResultKey.RESULT_FAILES;
                        }
                    }
                    catch { }
                }
            }
            return result;
        }
        [Authorization]
        [HttpGet]
        public string SearchExchangePoint(string fromDate, string toDate, int page)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            DateTime? toD = null;
            DateTime? fromD = null;

            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int intPageSize = 10;
            int start = 0, end = 10;
            int totalRecord = 0;
            //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
            #region edit
            if (fromDate == string.Empty)
            {
                fromD = DateTime.Parse("01/01/1990");
                toD = DateTime.Parse("01/01/2200");
            }
            else if (toDate == string.Empty)
            {
                string[] arrFrom = fromDate.Split('/');
                if (arrFrom != null && arrFrom.Length > 0)
                {
                    string m = arrFrom[0];
                    string d = arrFrom[1];
                    string y = arrFrom[2];
                    string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
                    fromD = DateTime.Parse(dateFrom);
                }

                toDate = toD.ToString();
                string[] arrTo = toDate.Split('/');
                if (arrTo != null && arrTo.Length > 0)
                {
                    string m = arrTo[0];
                    string d = arrTo[1];
                    string y = arrTo[2];
                    string dateTo = m + "/" + d + "/" + y;
                    toD = DateTime.Parse(dateTo);
                }
            }
            else
            {
                string[] arrFrom = fromDate.Split('/');
                if (arrFrom != null && arrFrom.Length > 0)
                {
                    string m = arrFrom[0];
                    string d = arrFrom[1];
                    string y = arrFrom[2];
                    string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
                    fromD = DateTime.Parse(dateFrom);

                }
                string[] arrTo = toDate.Split('/');
                if (arrTo != null && arrTo.Length > 0)
                {
                    string m = arrTo[0];
                    string d = arrTo[1];
                    string y = arrTo[2];
                    string dateTo = m + "/" + d + "/" + y + " 23:59:00";
                    toD = DateTime.Parse(dateTo);
                }
            }
            #endregion

            if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
            }
            else
            {
                if (string.IsNullOrEmpty(fromDate))
                {
                    if (string.IsNullOrEmpty(toDate))
                    {
                        //fromD = DateTime.Parse("");
                        toD = DateTime.Parse("01/01/1990");
                    }
                    else
                    {
                        fromD = DateTime.Parse("01/01/1990");
                        toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                    }
                }
                else
                {
                    fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                    toD = DateTime.Now;
                }
            }
            if (page >= 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<ExchangePointBO> lstExchangePoint = repository.ListExchangePointBySearch(fromD, toD, start, end);
            if (lstExchangePoint != null && lstExchangePoint.Count() > 0)
            {
                totalRecord = lstExchangePoint.ElementAt(0).TotalRecord;
                foreach (ExchangePointBO exchange in lstExchangePoint)
                {
                    string titleStatus = string.Empty;
                    string status = string.Empty;
                    if (exchange.IsDelete == 0)
                    {
                        status = "confirm";
                        titleStatus = "Active";
                    }
                    else if (exchange.IsDelete == 1)
                    {
                        status = "Deleted";
                        titleStatus = string.Empty;
                    }

                    builder.Append("<tr id=\"trGroup_" + exchange.ExChangeID + "\" class=\"none-top-border\">");
                    builder.Append("<td class=\"center\">" + exchange.ExChangeID + "</td>");
                    builder.Append("<td class=\"center\">" + exchange.BitcoinValue + "</td>");
                    builder.Append("<td class=\"center\">" + exchange.PointValue + "</td>");
                    builder.Append("<td class=\"center\">" + exchange.CreateDate + "</td>");
                    //builder.Append("<td class=\"center\">" + exchange.UpdateDate + "</td>");
                    builder.Append("<td class=\"center\">" + exchange.CreateUser + "</td>");
                    builder.Append("<td class=\"center\">");
                    builder.Append("<span class=\"label-success label label-default\" title=\"'" + titleStatus + "'\">" + status + "</span>");
                    builder.Append("</td>");
                    builder.Append("<td class=\"center\">");
                    builder.Append("<img src=\"/Images/loading.gif\" id=\"imgLoading_" + exchange.ExChangeID + "\" style=\"position: absolute; display:none ; float: right; width: 30px; margin-left: 45px; \">");

                    if (exchange.IsDelete == 0)
                    {
                        builder.Append("<a class=\"btn btn-info btn-sm\" title=\"" + exchange.ExChangeID + "\" href=\"javascript:void(0)\" onclick=\"ShowPopUpEditExchangeTicket('" + exchange.ExChangeID + "','" + exchange.CreateUser + "','" + exchange.PointValue + "','" + exchange.BitcoinValue + "')\" data-toggle=\"modal\" data-target=\"#standardModal\"><i class=\"glyphicon glyphicon-edit icon-white\"></i>Edit</a>");

                        builder.Append("<a class=\"btn btn-warning btn-sm\" href=\"javascript:void(0)\" onclick=\"ConfirmExchangeTicket('" + exchange.ExChangeID + "','1','" + exchange.DeleteDate + "','" + exchange.DeleteUser + "')\"><i class=\"glyphicon glyphicon-trash icon-white\"></i>Delete</a>");
                    }
                    else if (exchange.IsDelete == 1)
                    {
                    }
                    builder.Append("</a>");
                    builder.Append("</td>");
                    builder.Append("</tr>");
                }

                int totalPage = totalRecord / intPageSize;
                int balance = totalRecord % intPageSize;
                if (balance != 0)
                {
                    totalPage += 1;
                }
                if (totalPage > 1)
                {
                    for (int m = 1; m <= totalPage; m++)
                    {
                        if (m == page)
                        {
                            builderPaging.Append("<li class=\"active\"><a href=\"javascript:void(0)\" >" + m + "</a></li>");
                        }
                        else
                        {
                            builderPaging.Append("<li ><a href=\"javascript:void(0)\" onclick=\"PagingSearchExchangePoint('" + m + "')\" >" + m + "</a></li>");
                        }
                    }
                }
            }
            else
            {
                builder.Append("<tr><td colspan=\"7\">No result found</td></tr>");
            }

            SearchObj obj = new SearchObj();
            obj.ContentResult = builder.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = totalRecord;
            string json = JsonConvert.SerializeObject(obj);

            return json;
        }
    }
}
