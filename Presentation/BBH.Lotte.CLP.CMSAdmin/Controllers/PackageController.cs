﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using BBH.Lotte.CLP.Repository;

using Newtonsoft.Json;
using BBH.Lotte.CMSAdmin.Models;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Microsoft.Practices.Unity;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.CMSAdmin.Models;
//using System.Runtime.CompilerServices;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class PackageController : Controller
    {

        // MemberRepository repositoryMember = new MemberRepository();
        [Dependency]
        protected IMemberServices repositoryMember { get; set; }

         [Dependency]
         protected ITransactionPackageServices repository { get; set; }
        //TransactionPackageRepository repository = new TransactionPackageRepository();
        int groupIDAdmin = 0;

        [Authorization]
        public ActionResult Index(string p)
        {

            if (Session["UserName"] == null)
            {
                Response.Redirect("/login");
            }
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            int totalRecord = 0;
            int intPageSize = 10;
            int start = 0, end = 10;
            //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

            int page = 1;
            try
            {
                if (p != null && p != "")
                {
                    page = int.Parse(p);
                }
            }
            catch
            {

            }
            if (page > 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<TransactionPackageBO> lstTransaction = repository.ListAllTransactionPackage(start, end);
            ViewData["TransactionPackage"] = lstTransaction;
            if (lstTransaction != null && lstTransaction.Count() > 0)
            {
                totalRecord = lstTransaction.ElementAt(0).TotalRecord;
            }
            TempData["TotalRecord"] = totalRecord;

            IEnumerable<MemberBO> lstMember = repositoryMember.GetListMember(1, 10000);
            ViewData["ListMember"] = lstMember;
            return View();
        }

        [Authorization]
        [HttpPost]
        public string UpdateStatusTransaction(int transactionID, int status, int memberID, int packageID)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = "";
            //int statusNew = -1;
            //if (status == 1)
            //{
            //    statusNew = 0;
            //}
            //else
            //{
            //    statusNew = 1;
            //}
            bool rs = repository.UpdateStatusTransaction(transactionID, status);
            if (rs)
            {
                if (status == 1)
                {
                    double points = 0;
                    PackageBO package = repository.GetPackageDetail(packageID);
                    if (package != null)
                    {
                        points = package.PackageValue;
                    }
                    repositoryMember.UpdatePointsMember(memberID, (int)points);
                }
                result = "success";
            }
            return result;
        }

        [Authorization]
        [HttpGet]
        public string SearchPackage(int memberID, string fromDate, string toDate, int page, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            DateTime fromD = DateTime.Now;
            DateTime toD = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int totalRecord = 0;
            int intPageSize = 10;
            int start = 0, end = 5;
            //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

            if (fromDate == "")
            {
                fromD = DateTime.Parse("01/01/1990");
                toD = DateTime.Parse("01/01/1990");
            }
            else if (toDate == "")
            {
                fromD = DateTime.Parse("01/01/1990");
                toD = DateTime.Parse("01/01/1990");
            }
            else
            {
                string[] arrFrom = fromDate.Split('/');
                if (arrFrom != null && arrFrom.Length > 0)
                {
                    string d = arrFrom[0];
                    string m = arrFrom[1];
                    string y = arrFrom[2];
                    string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
                    fromD = DateTime.Parse(dateFrom);

                }
                string[] arrTo = toDate.Split('/');
                if (arrTo != null && arrTo.Length > 0)
                {
                    string d = arrTo[0];
                    string m = arrTo[1];
                    string y = arrTo[2];
                    string dateTo = m + "/" + d + "/" + y + " 23:59:00";
                    toD = DateTime.Parse(dateTo);

                }

            }

            if (page >= 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<TransactionPackageBO> lstTransaction = repository.ListAllTransactionPackageByMember(memberID, fromD, toD, status, start, end);
            if (lstTransaction != null && lstTransaction.Count() > 0)
            {
                totalRecord = lstTransaction.ElementAt(0).TotalRecord;
                foreach (TransactionPackageBO transaction in lstTransaction)
                {
                    string titleStatus = "";
                    string statusName = "";
                    if (transaction.Status == 0)
                    {
                        statusName = "Not confirm";
                        titleStatus = "Confirm";
                    }
                    else if (transaction.Status == 1)
                    {
                        statusName = "Confirmed";
                        titleStatus = "Actived";
                    }
                    else if (transaction.Status == 2)
                    {
                        statusName = "Deleted";
                        titleStatus = "";
                    }
                    string fullName = "", email = "", mobile = "", avatar = "";


                    builder.Append("<tr id=\"trGroup_" + transaction.TransactionID + "\" class=\"none-top-border\">");

                    builder.Append("<td class=\"center\">" + transaction.TransactionID + "</td>");
                    builder.Append("<td class=\"center\">" + transaction.Email + "</td>");
                    builder.Append("<td>" + transaction.PackageName + "</td>");
                    builder.Append("<td class=\"center\">" + transaction.CreateDate.ToString("dd/MM/yyyy") + "</td>");
                    builder.Append("<td class=\"center\">" + transaction.ExpireDate.ToString("dd/MM/yyyy") + "</td>");
                    builder.Append("<td class=\"center\"><span class=\"label-success label label-default\">" + statusName + "</span></td>");
                    if (transaction.Status == 0)
                    {
                        builder.Append("<td>");
                        builder.Append("<img src=\"/Images/loading.gif\" id=\"imgLoading_" + transaction.TransactionID + "\" style=\"position: absolute; display: none; float: right; width: 30px; margin-left: 45px;\">");
                        builder.Append("<a class=\"btn btn-info btn-sm\" href=\"javascript:void(0)\" onclick=\"ConfirmTransaction('" + transaction.TransactionID + "','1','" + transaction.MemberID + "','" + transaction.PackageID + "')\"  title=\"Xác nhận\"><i class=\"glyphicon glyphicon-edit icon-white\">Confirm</i></a>");
                        builder.Append("<a class=\"btn btn-warning btn-sm\" href=\"javascript:void(0)\" onclick=\"ConfirmTransaction('" + transaction.TransactionID + "','2','" + transaction.MemberID + "','" + transaction.PackageID + "')\"  title=\"Xóa\"><i class=\"glyphicon glyphicon-trash icon-white\"></i>Deleted</a>");

                        builder.Append("</td>");
                    }
                    builder.Append("</tr>");
                }
                int totalPage = totalRecord / intPageSize;
                int balance = totalRecord % intPageSize;
                if (balance != 0)
                {
                    totalPage += 1;
                }

                if (totalPage > 1)
                {

                    for (int m = 1; m <= totalPage; m++)
                    {

                        if (m == page)
                        {
                            builderPaging.Append("<li class=\"active\"><a href=\"javascript:void(0)\" >" + m + "</a></li>");
                        }
                        else
                        {
                            builderPaging.Append("<li ><a href=\"javascript:void(0)\" onclick=\"PagingSearchTransactionPackage('" + m + "')\" >" + m + "</a></li>");

                        }
                    }
                }

            }
            else
            {
                builder.Append("<tr><td colspan=\"7\">No result found</td></tr>");
            }
            SearchObj obj = new SearchObj();
            obj.ContentResult = builder.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = totalRecord;
            string json = JsonConvert.SerializeObject(obj);

            return json;
        }

        [Authorization]
        [HttpGet]
        public string ExportListTransactionPackage(int memberID, string fromDate, string toDate, int page, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            HSSFWorkbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("LIST TRANSACTION PACKAGE");
            Row rowLogo = sheet.CreateRow(0);

            rowLogo.CreateCell(0).SetCellValue("LIST TRANSACTION PACKAGE - LOTTERY");
            rowLogo.HeightInPoints = 20;
            CellStyle styleLogo = workbook.CreateCellStyle();
            styleLogo.Alignment = HorizontalAlignment.CENTER;
            NPOI.SS.UserModel.Font font = workbook.CreateFont();
            font.FontHeightInPoints = 12;
            font.FontName = "Times New Roman";
            font.Boldweight = (short)FontBoldWeight.BOLD;

            styleLogo.SetFont(font);
            rowLogo.GetCell(0).CellStyle = styleLogo;

            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

            sheet.SetColumnWidth(0, 5 * 256);
            sheet.SetColumnWidth(1, 40 * 256);
            sheet.SetColumnWidth(2, 30 * 256);
            sheet.SetColumnWidth(3, 25 * 256);
            sheet.SetColumnWidth(4, 25 * 256);
            sheet.SetColumnWidth(5, 25 * 256);
            sheet.SetColumnWidth(6, 15 * 256);
            Row titleList = sheet.CreateRow(2);

            titleList.CreateCell(0).SetCellValue("STT");
            titleList.HeightInPoints = 22;
            CellStyle styleTitleList = workbook.CreateCellStyle();
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            NPOI.SS.UserModel.Font fontTitleList = workbook.CreateFont();
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            fontTitleList.Color = (short)NPOI.HSSF.Util.HSSFColor.WHITE.index;
            //NPOI.HSSF.UserModel.HSSFTextbox.LINESTYLE_SOLID
            styleTitleList.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;
            // styleTitleList.FillPattern = FillPatternType.THICK_VERT_BANDS;
            styleTitleList.FillPattern = FillPatternType.THIN_FORWARD_DIAG;
            styleTitleList.FillPattern = FillPatternType.THIN_BACKWARD_DIAG;
            styleTitleList.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;

            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(0).CellStyle = styleTitleList;

            titleList.CreateCell(1).SetCellValue("Transaction Code");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(1).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(1).CellStyle = styleTitleList;


            titleList.CreateCell(2).SetCellValue("Email");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(2).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(2).CellStyle = styleTitleList;

            titleList.CreateCell(3).SetCellValue("Package name");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(3).CellStyle = styleTitleList;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(3).CellStyle = styleTitleList;



            titleList.CreateCell(4).SetCellValue("Buy Date");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.CENTER;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(4).CellStyle = styleTitleList;

            titleList.CreateCell(5).SetCellValue("Expire Date");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.CENTER;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(5).CellStyle = styleTitleList;


            titleList.CreateCell(6).SetCellValue("Status");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(6).CellStyle = styleTitleList;

            int rowNext = 3;

            int number = 1;

            DateTime fromD = DateTime.Now;
            DateTime toD = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int totalRecord = 0;
            int intPageSize = 10;
            int start = 0, end = 5;
            Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

            if (fromDate == "")
            {
                fromD = DateTime.Parse("01/01/1990");
                toD = DateTime.Parse("01/01/1990");
            }
            else if (toDate == "")
            {
                fromD = DateTime.Parse("01/01/1990");
                toD = DateTime.Parse("01/01/1990");
            }
            else
            {
                string[] arrFrom = fromDate.Split('/');
                if (arrFrom != null && arrFrom.Length > 0)
                {
                    string d = arrFrom[0];
                    string m = arrFrom[1];
                    string y = arrFrom[2];
                    string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
                    fromD = DateTime.Parse(dateFrom);

                }
                string[] arrTo = toDate.Split('/');
                if (arrTo != null && arrTo.Length > 0)
                {
                    string d = arrTo[0];
                    string m = arrTo[1];
                    string y = arrTo[2];
                    string dateTo = m + "/" + d + "/" + y + " 23:59:00";
                    toD = DateTime.Parse(dateTo);

                }

            }

            //if (page >= 1)
            //{
            //    start = (page - 1) * intPageSize + 1;
            //    end = (page * intPageSize);
            //}
            IEnumerable<TransactionPackageBO> lstTransaction = repository.ListAllTransactionPackageByMember(memberID, fromD, toD, status, 0, 10000);
            if (lstTransaction != null && lstTransaction.Count() > 0)
            {
                totalRecord = lstTransaction.ElementAt(0).TotalRecord;
                foreach (TransactionPackageBO transaction in lstTransaction)
                {
                    string titleStatus = "";
                    string statusName = "";
                    if (transaction.Status == 0)
                    {
                        statusName = "Not confirm";
                        titleStatus = "Confirm";
                    }
                    else if (transaction.Status == 1)
                    {
                        statusName = "Confirmed";
                        titleStatus = "Actived";
                    }
                    else if (transaction.Status == 2)
                    {
                        statusName = "Deleted";
                        titleStatus = "";
                    }


                    Row rowListInvoice = sheet.CreateRow(rowNext);

                    rowListInvoice.CreateCell(0).SetCellValue(number);
                    rowListInvoice.HeightInPoints = 20;
                    CellStyle styleNumber = workbook.CreateCellStyle();
                    styleNumber.Alignment = HorizontalAlignment.LEFT;
                    NPOI.SS.UserModel.Font fontNumber = workbook.CreateFont();
                    fontNumber.FontHeightInPoints = 12;
                    fontNumber.FontName = "Times New Roman";
                    styleNumber.SetFont(fontNumber);
                    rowListInvoice.GetCell(0).CellStyle = styleNumber;

                    rowListInvoice.CreateCell(1).SetCellValue(transaction.TransactionCode);
                    rowListInvoice.HeightInPoints = 20;
                    CellStyle styleDate = workbook.CreateCellStyle();
                    styleDate.Alignment = HorizontalAlignment.LEFT;
                    NPOI.SS.UserModel.Font fontDate = workbook.CreateFont();
                    fontDate.FontHeightInPoints = 12;
                    fontDate.FontName = "Times New Roman";
                    styleDate.SetFont(fontDate);
                    rowListInvoice.GetCell(1).CellStyle = styleDate;

                    rowListInvoice.CreateCell(2).SetCellValue(transaction.Email);
                    rowListInvoice.HeightInPoints = 20;
                    CellStyle styleDate2 = workbook.CreateCellStyle();
                    styleDate2.Alignment = HorizontalAlignment.LEFT;
                    NPOI.SS.UserModel.Font fontDate2 = workbook.CreateFont();
                    fontDate2.FontHeightInPoints = 12;
                    fontDate2.FontName = "Times New Roman";
                    styleDate.SetFont(fontDate2);
                    rowListInvoice.GetCell(2).CellStyle = styleDate2;

                    rowListInvoice.CreateCell(3).SetCellValue(transaction.PackageName);
                    rowListInvoice.HeightInPoints = 20;
                    CellStyle styleDate4 = workbook.CreateCellStyle();
                    styleDate4.Alignment = HorizontalAlignment.LEFT;
                    NPOI.SS.UserModel.Font fontDate4 = workbook.CreateFont();
                    fontDate4.FontHeightInPoints = 12;
                    fontDate4.FontName = "Times New Roman";
                    styleDate.SetFont(fontDate4);
                    rowListInvoice.GetCell(3).CellStyle = styleDate4;

                    //Row rowDate = sheet.CreateRow(rowNext);
                    rowListInvoice.CreateCell(4).SetCellValue(transaction.CreateDate.ToString("dd/MM/yyyy"));
                    rowListInvoice.HeightInPoints = 20;
                    CellStyle styleEmail = workbook.CreateCellStyle();
                    styleEmail.Alignment = HorizontalAlignment.CENTER;
                    NPOI.SS.UserModel.Font fontEmail = workbook.CreateFont();
                    fontEmail.FontHeightInPoints = 12;
                    fontEmail.FontName = "Times New Roman";
                    styleEmail.SetFont(fontEmail);
                    rowListInvoice.GetCell(4).CellStyle = styleEmail;


                    //Row rowOrderID = sheet.CreateRow(rowNext);
                    rowListInvoice.CreateCell(5).SetCellValue(transaction.ExpireDate.ToString("dd/MM/yyyy"));
                    rowListInvoice.HeightInPoints = 20;
                    CellStyle styleOrderID = workbook.CreateCellStyle();
                    styleOrderID.Alignment = HorizontalAlignment.CENTER;
                    NPOI.SS.UserModel.Font fontOrderID = workbook.CreateFont();
                    fontOrderID.FontHeightInPoints = 12;
                    fontOrderID.FontName = "Times New Roman";
                    styleOrderID.SetFont(fontOrderID);
                    rowListInvoice.GetCell(5).CellStyle = styleOrderID;

                    rowListInvoice.CreateCell(6).SetCellValue(statusName);
                    rowListInvoice.HeightInPoints = 20;
                    CellStyle styleEmail3 = workbook.CreateCellStyle();
                    styleEmail3.Alignment = HorizontalAlignment.LEFT;

                    NPOI.SS.UserModel.Font fontEmail3 = workbook.CreateFont();
                    fontEmail3.FontHeightInPoints = 12;
                    fontEmail3.FontName = "Times New Roman";

                    styleEmail.SetFont(fontEmail3);
                    rowListInvoice.GetCell(6).CellStyle = styleEmail3;

                    number++;
                    rowNext++;
                }

            }

            string fileNameExcel = "Listtransactionpackage_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xls";
            string filename = Server.MapPath("~/FileExcels/TransactionPackage/" + fileNameExcel);
            using (var fileData = new FileStream(filename, FileMode.Create))
            {
                workbook.Write(fileData);
            }
            return fileNameExcel;
        }

    }
}
