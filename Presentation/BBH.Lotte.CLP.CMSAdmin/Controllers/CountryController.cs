﻿using BBC.Core.Common.Cache;
using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBH.Lotte.CLP.CMSAdmin.Models;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CMSAdmin.Models;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
//using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.CMSAdmin.Controllers
{
    public class CountryController : Controller
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }
        [Dependency]
        protected ICountryServices repositoryCountry { get; set; }
        RedisCache objRedisCache = new RedisCache();

        int groupIDAdmin = 0;
        // GET: Country
        [Authorization]
        public ActionResult Index(string p)
        {
            if (Session[Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME] == null)
            {
                Response.Redirect("/login");
            }
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            IEnumerable<CountryBO> lstCountry = null;
            int totalRecord = 0;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int intPageSize = 10;
                    int start = 0, end = 10;
                    //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                    int page = 1;
                    try
                    {
                        if (p != null && p != string.Empty)
                        {
                            page = int.Parse(p);
                        }
                    }
                    catch
                    {
                    }
                    if (page > 1)
                    {
                        start = (page - 1) * intPageSize + 1;
                        end = (page * intPageSize);
                    }
                    //Utilitys.WriteLog(fileLog, "Exception CheckAwardNameEnglish :2 " + (repositoryCountry != null ? repositoryCountry.ToString() : "null") + ", start: " + start + ", end: " + end);
                    lstCountry = repositoryCountry.GetListCountry(start, end);
                    if (lstCountry != null && lstCountry.Count() > 0)
                    {
                        totalRecord = lstCountry.ElementAt(0).TotalRecord;
                    }
                    ViewData[ViewDataKey.VIEWDATA_LISTCOUNTRY] = lstCountry;
                }

            }

            TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;

            return View();
        }

        [Authorization]
        [HttpPost]
        public string SaveCountry(int countryID, string countryName, string phoneCode)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    CountryBO country = new CountryBO();
                    if (countryName == string.Empty /*|| awardValue == string.Empty*/)
                    {
                        result = ResultKey.RESULT_FAILES;
                    }
                    else
                    {

                        if (countryID > 0)
                        {
                            try
                            {
                                //double value = 0;
                                //double.TryParse(awardValue, out value);
                                country.CountryID = countryID;
                                country.CountryName = countryName;
                                country.IsActive = 1;
                                country.IsDeleted = 0;
                                country.PhoneZipCode = phoneCode;
                                country.UpdatedDate = DateTime.Now;
                                country.UpdatedUser = Session[Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME].ToString();
                                bool CheckPhoneCodeExist = repositoryCountry.CheckPhoneZipCodeExists(countryID, phoneCode);
                                if (!CheckPhoneCodeExist)
                                {
                                    string strParameterObj = Algorithm.EncryptionObjectRSA<CountryBO>(country);
                                    ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                    objRsa2.Token = objRsa.Token;
                                    objRsa2.UserName = objConfig.UserName;
                                    objResult = repositoryCountry.UpdateCountry(objRsa2);
                                    if (!objResult.IsError)
                                    {
                                        objRedisCache.RemoveCache(Common.KeyListAwardBO);
                                        result = ResultKey.RESULT_SUCCESS;
                                    }
                                    else
                                    {
                                        result = ResultKey.RESULT_FAILES;
                                    }
                                }
                                else
                                {
                                    result = ResultKey.RESULT_PHONECODEEXIST;
                                }
                            }
                            catch (Exception ex) { }
                        }
                        else if (countryID == 0)
                        {
                            try
                            {
                                country.CountryID = countryID;
                                country.CountryName = countryName;
                                country.CreatedDate = DateTime.Now;
                                country.CreatedUser = Session[Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME].ToString();
                                country.IsActive = 1;
                                country.IsDeleted = 0;
                                country.PhoneZipCode = phoneCode;

                                bool CheckCountryNameExist = repositoryCountry.CheckCountryNameExists(countryID, countryName);
                                if (CheckCountryNameExist)
                                {
                                    result = ResultKey.RESULT_COUNTRYNAMEEXIST;
                                }
                                else
                                {
                                    bool CheckPhoneCodeExist = repositoryCountry.CheckPhoneZipCodeExists(countryID, phoneCode);
                                    if (!CheckPhoneCodeExist)
                                    {
                                        string strParameterObj = Algorithm.EncryptionObjectRSA<CountryBO>(country);
                                        ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                        objRsa2.Token = objRsa.Token;
                                        objRsa2.UserName = objConfig.UserName;
                                        objResult = repositoryCountry.InsertCountry(objRsa2);
                                        if (!objResult.IsError)
                                        {
                                            objRedisCache.RemoveCache(Common.KeyListAwardBO);
                                            result = ResultKey.RESULT_SUCCESS;
                                        }
                                        else
                                        {
                                            result = ResultKey.RESULT_FAILES;
                                        }
                                    }
                                    else
                                    {
                                        result = ResultKey.RESULT_PHONECODEEXIST;
                                    }
                                }
                            }
                            catch (Exception ex) { }
                        }
                    }
                }
            }
            return result;
        }

        [Authorization]
        [HttpPost]
        public string LockAndUnLockCountry(int countryID, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int statusNew = -1;
                    if (status == 1)
                    {
                        statusNew = 0;
                    }
                    else
                    {
                        statusNew = 1;
                    }
                    CountryBO objCountry = new CountryBO();
                    objCountry.IsActive = statusNew;
                    objCountry.CountryID = countryID;
                    string strParameterObj = Algorithm.EncryptionObjectRSA<CountryBO>(objCountry);
                    ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objConfig.UserName;
                    objResult = repositoryCountry.LockAndUnlockCountry(objRsa2);
                    if (!objResult.IsError)
                    {
                        objRedisCache.RemoveCache(Common.KeyListAwardBO);
                        result = ResultKey.RESULT_SUCCESS;
                    }
                }
            }
            return result;
        }

        [Authorization]
        [HttpGet]
        public string SearchCountryName(string keyword, int page)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();

            int totalRecord = 0;
            int intPageSize = 10;
            int start = 0, end = 100;


            if (page >= 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<CountryBO> lstCountry = repositoryCountry.GetListCountryBySearch(keyword, start, end);
            if (lstCountry != null && lstCountry.Count() > 0)
            {
                totalRecord = lstCountry.ElementAt(0).TotalRecord;
                foreach (CountryBO country in lstCountry)
                {
                    string titleStatus = "Lock";
                    string status = "Active";
                    if (country.IsActive == 0)
                    {
                        status = "InActive";
                        titleStatus = "Unlock";
                    }

                    int CountryID = country.CountryID;
                    string CountryName = country.CountryName;
                    string PhoneCode = country.PhoneZipCode;

                    builder.Append("<tr id =\"trGroup_" + CountryID + "\" class=\"none-top-border\">");
                    builder.Append("<td class=\"text-center\">" + CountryID + " </td>");
                    builder.Append("<td class=\"text-center\">" + CountryName + " </td>");
                    builder.Append("<td class=\"text-center\">" + PhoneCode + " </td>");
                    builder.Append("<td class=\"text-center\">" + country.CreatedDate + "</td>");
                    builder.Append("<td class=\"text-center\">");
                    builder.Append("<span class=\"label-success label label-default\">" + status + "</span>");
                    builder.Append("</td>");
                    builder.Append("<td class=\"text-center\">");
                    builder.Append("<img src =\"/Images/loading.gif\" id=\"imgLoading_" + CountryID + "\" style=\"position: absolute; display:none ; float: right; width: 30px; margin-left: 45px; \">");
                    builder.Append("<a class=\"btn btn-info btn-sm\" style=\"margin-right: 10px;\" title=\"" + CountryName + "\" href=\"#\" onclick=\"ShowPopUpEditCountry('" + CountryID + "','" + CountryName + "','" + PhoneCode + "')\" data-toggle=\"modal\" data-target=\"#standardModal\"><i class=\"glyphicon glyphicon-edit icon-white\"></i>Edit</a>");
                    builder.Append("<a class=\"btn btn-warning btn-sm\" href=\"#\" onclick=\"LockAndUnlockCountry('" + CountryID + "','" + country.IsActive + "')\" title=\"" + titleStatus + "\">");
                    if (country.IsActive == 1)
                    {
                        builder.Append("<i class=\"glyphicon glyphicon-ok\"></i>");
                    }
                    else
                    {
                        builder.Append("<i class=\"glyphicon glyphicon-remove\"></i>");
                    }
                    builder.Append("</a>");
                    builder.Append("</td>");
                    builder.Append("</tr>");
                }
                int totalPage = totalRecord / intPageSize;
                int balance = totalRecord % intPageSize;
                if (balance != 0)
                {
                    totalPage += 1;
                }

                if (totalPage > 1)
                {
                    for (int m = 1; m <= totalPage; m++)
                    {
                        if (m == page)
                        {
                            builderPaging.Append("<li class=\"active\"><a href=\"javascript:void(0)\" >" + m + "</a></li>");
                        }
                        else
                        {
                            builderPaging.Append("<li><a href=\"javascript:void(0)\" onclick=\"PagingSearchCountry('" + m + "')\" >" + m + "</a></li>");
                        }
                    }
                }
            }
            else
            {
                builder.Append("<tr><td colspan=\"7\">No result found</td></tr>");
            }

            SearchObj obj = new SearchObj();
            obj.ContentResult = builder.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = totalRecord;
            string json = JsonConvert.SerializeObject(obj);

            return json;
        }
    }
}