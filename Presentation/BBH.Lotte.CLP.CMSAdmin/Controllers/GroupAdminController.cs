﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BBH.Lotte.CLP.Repository;
using Microsoft.Practices.Unity;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CMSAdmin.Models;
using BBH.Lotte.CLP.CMSAdmin.Models;
using System.Configuration;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class GroupAdminController : Controller
    {
        //
        // GET: /GroupAdmin/
        [Dependency]
        protected IGroupAdminServices repository { get; set; }

        int groupIDAdmin = 0;

        [Authorization]
        public ActionResult Index()
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }

            IEnumerable<GroupAdminBO> lstGroupAdmin = repository.ListGroupAdmin();
            ViewData[ViewDataKey.VIEWDATA_LISTGROUP] = lstGroupAdmin;

            return View();
        }

        [Authorization]
        [HttpPost]
        public string SaveGroupAdmin(int groupID, string groupName)
        {
            string result = string.Empty;
            GroupAdminBO groupAdmin = new GroupAdminBO();
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }

            if (groupName == null || groupName.Trim().Length == 0)
            {
                result = ResultKey.RESULT_GROUPNAMENULL;
            }
            else
            {
                if (groupID > 0)
                {
                    groupAdmin.GroupID = groupID;
                    groupAdmin.GroupName = groupName;
                    bool checkGroupName = repository.CheckUserGroupAdminNameExists(groupName);
                    if (checkGroupName)
                    {
                        result = ResultKey.RESULT_GROUPNAMEEXIST;
                    }
                    else
                    {
                        bool rs = repository.UpdateGroupAdmin(groupID, groupName);
                        if (rs)
                        {
                            result = ResultKey.RESULT_SUCCESS;
                        }
                        else
                        {
                            result = ResultKey.RESULT_FAILES;
                        }
                    }
                }
                else if (groupID == 0)
                {
                    groupAdmin.GroupID = groupID;
                    groupAdmin.GroupName = groupName;
                    groupAdmin.IsActive = 1;

                    bool checkGroupName = repository.CheckUserGroupAdminNameExists(groupName);
                    if (checkGroupName)
                    {
                        result = ResultKey.RESULT_GROUPNAMEEXIST;
                    }
                    else
                    {
                        bool rs = repository.InsertGroupAdmin(groupAdmin);
                        if (rs)
                        {
                            result = ResultKey.RESULT_SUCCESS;
                        }
                        else
                        {
                            result = ResultKey.RESULT_FAILES;
                        }
                    }
                }
            }
            return result;
        }
        [Authorization]
        [HttpPost]
        public string LockAndUnlockGroupAdmin(int groupID, int status)
        {
            string result = string.Empty;
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }

            int statusNew = -1;
            if (status == 1)
            {
                statusNew = 0;
            }
            else
            {
                statusNew = 1;
            }
            bool rs = repository.UpdateStatusGroupAdmin(groupID, statusNew);
            if (rs)
            {
                result = ResultKey.RESULT_SUCCESS;
            }

            return result;
        }

    }
}
