﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BBH.Lotte.CLP.Repository;
using System.Configuration;
using Microsoft.Practices.Unity;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CMSAdmin.Models;
using BBH.Lotte.CLP.Shared;
using BBC.Core.Database;
using BBC.Core.Common.Utils;
using BBH.Lotte.CLP.CMSAdmin.Models;
using BBC.Core.Common.Cache;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.Wallet.Repository;

namespace BBH.Lotte.CLP.CMSAdmin.Controllers
{
    public class AwardController : Controller
    {

        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];

        //sysJackpot
        string sysJackpot_UserID = ConfigurationManager.AppSettings[KeyManager.JackPotID];
        string sysJackpot_Address = ConfigurationManager.AppSettings[KeyManager.JackPotWalletAddress];

        // GET: /Award/
        [Dependency]
        protected IAwardServices repository { get; set; }

        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }
        RedisCache objRedisCache = new RedisCache();

        int groupIDAdmin = 0;
        [Authorization]
        public ActionResult Index(string p)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            IEnumerable<AwardBO> lstAward = null;
            int totalRecord = 0;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);

            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int intPageSize = 10;
                    int start = 0, end = 10;
                    //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                    int page = 1;
                    try
                    {
                        if (p != null && p != string.Empty)
                        {
                            page = int.Parse(p);
                        }
                    }
                    catch
                    {
                    }
                    if (page > 1)
                    {
                        start = (page - 1) * intPageSize + 1;
                        end = (page * intPageSize);
                    }

                    lstAward = repository.GetListAward(start, end);

                    if (lstAward != null && lstAward.Count() > 0)
                    {
                        totalRecord = lstAward.ElementAt(0).TotalRecord;
                    }
                }
            }

            //decimal jackpotValue = GetBalance(sysJackpot_UserID);

            //var obj = lstAward.FirstOrDefault(x => x.Priority == 1);
            //if (obj != null) obj.AwardValue = jackpotValue;

            TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;
            ViewData[ViewDataKey.VIEWDATA_LISTAWARD] = lstAward;
            return View();
        }
        [Authorization]
        [HttpPost]
        public string SaveAward(int awardID, string awardNameEnglish, decimal awardValue, float awardPercent)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    AwardBO award = new AwardBO();
                    if (awardNameEnglish == string.Empty /*|| awardValue == string.Empty*/)
                    {
                        result = ResultKey.RESULT_FAILES;
                    }
                    else
                    {
                        //Get jackpot
                        decimal jackpot = 0;
                        string strJackpotAddress = string.Empty;
                        //get address btc
                        AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                        objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                        objAddressInfoRequest.UserID = sysJackpot_UserID;
                        objAddressInfoRequest.ClientID = short.Parse(ClientID);
                        objAddressInfoRequest.SystemID = short.Parse(SystemID);
                        objAddressInfoRequest.IsCreateAddr = false;
                        objAddressInfoRequest.CreatedBy = sysJackpot_UserID;
                        objAddressInfoRequest.WalletId = short.Parse(WalletID);

                        AddressRepository objAddressRepository = new AddressRepository();
                        AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                        if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                        {
                            strJackpotAddress = objAddressInfo.AddressId;
                            jackpot = objAddressInfo.Available;
                        }

                        if (awardID > 0)
                        {
                            try
                            {
                                award = repository.GetListAwardDetail(awardID);
                                if (award.Priority == 1)
                                {
                                    award.AwardID = awardID;
                                    award.AwardValue = jackpot;
                                    award.AwardNameEnglish = awardNameEnglish;
                                    award.AwardName = awardNameEnglish;
                                    award.AwardPercent = awardPercent;
                                    award.AwardFee = ((jackpot * (decimal)awardPercent) / 100);

                                    bool CheckAwardNameEnglish = repository.CheckAwardNameEnglishExists(awardID, awardNameEnglish);
                                    if (!CheckAwardNameEnglish)
                                    {
                                        string strParameterObj = Algorithm.EncryptionObjectRSA<AwardBO>(award);
                                        ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                        objRsa2.Token = objRsa.Token;
                                        objRsa2.UserName = objConfig.UserName;
                                        objResult = repository.UpdateAward(objRsa2);
                                        if (!objResult.IsError)
                                        {
                                            objRedisCache.RemoveCache(Common.KeyListAwardBO);
                                            result = ResultKey.RESULT_SUCCESS;
                                        }
                                        else
                                        {
                                            result = ResultKey.RESULT_FAILES;
                                        }
                                    }
                                    else
                                    {
                                        result = ResultKey.RESULT_AWARDNAMEENGLISHEXIST;
                                    }
                                }
                                else
                                {
                                    award.AwardID = awardID;
                                    award.AwardValue = awardValue;
                                    award.AwardNameEnglish = awardNameEnglish;
                                    award.AwardName = awardNameEnglish;
                                    award.AwardPercent = awardPercent;
                                    award.AwardFee = ((awardValue * (decimal)awardPercent) / 100);

                                    bool CheckAwardNameEnglish = repository.CheckAwardNameEnglishExists(awardID, awardNameEnglish);
                                    if (!CheckAwardNameEnglish)
                                    {
                                        string strParameterObj = Algorithm.EncryptionObjectRSA<AwardBO>(award);
                                        ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                        objRsa2.Token = objRsa.Token;
                                        objRsa2.UserName = objConfig.UserName;
                                        objResult = repository.UpdateAward(objRsa2);
                                        if (!objResult.IsError)
                                        {
                                            objRedisCache.RemoveCache(Common.KeyListAwardBO);
                                            result = ResultKey.RESULT_SUCCESS;
                                        }
                                        else
                                        {
                                            result = ResultKey.RESULT_FAILES;
                                        }
                                    }
                                    else
                                    {
                                        result = ResultKey.RESULT_AWARDNAMEENGLISHEXIST;
                                    }
                                }
                            }
                            catch (Exception ex) { }
                        }
                        else if (awardID == 0)
                        {
                            try
                            {
                                if (awardNameEnglish == "Jackpot")
                                {
                                    award.AwardNameEnglish = awardNameEnglish;
                                    award.AwardValue = jackpot;
                                    award.CreateDate = DateTime.Now;
                                    award.IsActive = 1;
                                    award.IsDelete = 0;
                                    award.Priority = 1;
                                    award.AwardName = awardNameEnglish;
                                    award.AwardPercent = awardPercent;
                                    award.AwardFee = ((jackpot * (decimal)awardPercent) / 100);

                                    bool CheckAwardNameEnglish = repository.CheckAwardNameEnglishExists(awardID, awardNameEnglish);
                                    if (CheckAwardNameEnglish)
                                    {
                                        result = ResultKey.RESULT_AWARDNAMEENGLISHEXIST;
                                    }
                                    else
                                    {
                                        string strParameterObj = Algorithm.EncryptionObjectRSA<AwardBO>(award);
                                        ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                        objRsa2.Token = objRsa.Token;
                                        objRsa2.UserName = objConfig.UserName;
                                        objResult = repository.InsertAward(objRsa2);
                                        if (!objResult.IsError)
                                        {
                                            objRedisCache.RemoveCache(Common.KeyListAwardBO);
                                            result = ResultKey.RESULT_SUCCESS;
                                        }
                                        else
                                        {
                                            result = ResultKey.RESULT_FAILES;
                                        }
                                    }
                                }
                                else
                                {
                                    award.AwardNameEnglish = awardNameEnglish;
                                    award.AwardValue = awardValue;
                                    award.CreateDate = DateTime.Now;
                                    award.IsActive = 1;
                                    award.IsDelete = 0;
                                    award.Priority = 2;
                                    award.AwardName = awardNameEnglish;
                                    award.AwardPercent = awardPercent;
                                    award.AwardFee = ((awardValue * (decimal)awardPercent) / 100);

                                    bool CheckAwardNameEnglish = repository.CheckAwardNameEnglishExists(awardID, awardNameEnglish);
                                    if (CheckAwardNameEnglish)
                                    {
                                        result = ResultKey.RESULT_AWARDNAMEENGLISHEXIST;
                                    }
                                    else
                                    {
                                        string strParameterObj = Algorithm.EncryptionObjectRSA<AwardBO>(award);
                                        ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                        objRsa2.Token = objRsa.Token;
                                        objRsa2.UserName = objConfig.UserName;
                                        objResult = repository.InsertAward(objRsa2);
                                        if (!objResult.IsError)
                                        {
                                            objRedisCache.RemoveCache(Common.KeyListAwardBO);
                                            result = ResultKey.RESULT_SUCCESS;
                                        }
                                        else
                                        {
                                            result = ResultKey.RESULT_FAILES;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            return result;
        }
        [Authorization]
        [HttpPost]
        public string LockAndUnLockAward(int awardID, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int statusNew = -1;
                    if (status == 1)
                    {
                        statusNew = 0;
                    }
                    else
                    {
                        statusNew = 1;
                    }
                    AwardBO objAward = new AwardBO();
                    objAward.IsActive = statusNew;
                    objAward.AwardID = awardID;
                    string strParameterObj = Algorithm.EncryptionObjectRSA<AwardBO>(objAward);
                    ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objConfig.UserName;
                    objResult = repository.LockAndUnlockAward(objRsa2);
                    if (!objResult.IsError)
                    {
                        objRedisCache.RemoveCache(Common.KeyListAwardBO);
                        result = ResultKey.RESULT_SUCCESS;
                    }
                }
            }
            return result;
        }
        private decimal GetBalance(string addressID)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            decimal balance = 0;
            AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
            objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
            objAddressInfoRequest.UserID = addressID;
            objAddressInfoRequest.ClientID = short.Parse(ClientID);
            objAddressInfoRequest.SystemID = short.Parse(SystemID);
            objAddressInfoRequest.IsCreateAddr = false;
            objAddressInfoRequest.WalletId = short.Parse(WalletID);

            AddressRepository objAddressRepository = new AddressRepository();
            AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);
            if (objAddressInfo != null && !string.IsNullOrEmpty(objAddressInfo.AddressId))
            {
                balance = objAddressInfo.Available;
            }
            return balance;
        }
    }
}
