﻿using BBH.Lotte.CLP.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CMSAdmin.Models;
using BBH.Lotte.CLP.CMSAdmin.Models;
using System.Configuration;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        int groupIDAdmin = 0;

        [Dependency]
        protected IAdminServices repository { get; set; }
        //public ActionResult Index()
        //{          
        //    if (Session[SessionKey.SESSION_USERNAME] == null)
        //    {
        //        Response.Redirect("/login");
        //    }
        //    else
        //    {
        //        if ((int)Session[SessionKey.SESSION_GROUPID] != 1)
        //        {
        //            Response.Redirect("/");
        //        }
        //        IEnumerable<AdminBO> lstAdmin = repository.ListAllAdmin();
        //        if (lstAdmin != null && lstAdmin.Count() > 0)
        //        {
        //            string userName = lstAdmin.ElementAt(0).UserName;
        //        }
        //        ViewData[ViewDataKey.VIEWDATA_LISTADMIN] = lstAdmin;
        //        IEnumerable<GroupAdminBO> lstGroupAdmin = repository.ListGroupAdmin();
        //        ViewData[ViewDataKey.VIEWDATA_LISTGROUP] = lstGroupAdmin;                
        //    }
        //    return View();
        //}
        [Authorization]
        public ActionResult Index(string p)
        {
            //if (Session[SessionKey.SESSION_USERNAME] == null)
            //{
            //    Response.Redirect("/login");
            //}
            //else
            //{
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/");
            }

            int totalRecord = 0;
                int intPageSize = 10;
                int start = 0, end = 10;
                //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

                int page = 1;
                try
                {
                    if (p != null && p != string.Empty)
                    {
                        page = int.Parse(p);
                    }
                }
                catch
                {

                }
                if (page > 1)
                {
                    start = (page - 1) * intPageSize + 1;
                    end = (page * intPageSize);
                }
               
                IEnumerable<AdminBO> lstAdmin = repository.ListAllAdminPaging(start,end);
                ViewData[ViewDataKey.VIEWDATA_LISTADMIN] = lstAdmin;
                if (lstAdmin != null && lstAdmin.Count() > 0)
                {
                    totalRecord = lstAdmin.ElementAt(0).TotalRecord;
                }                      
                              
                TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;

                IEnumerable<GroupAdminBO> lstGroupAdmin = repository.ListGroupAdmin();
                ViewData[ViewDataKey.VIEWDATA_LISTGROUP] = lstGroupAdmin;
            //}
            return View();
        }

        [Authorization]
        [HttpPost]
        public string SaveAdmin(int adminID, string userName, string password, string fullName, string email, string mobile, string address, int groupID)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result = string.Empty;
            AdminBO admin = new AdminBO();
            if (Session[SessionKey.SESSION_USERNAME] == null)
            {
                Response.Redirect("/login");
            }
            else
            {
                //if ((int)Session["GroupID"] != 1)
                //{
                //    Response.Redirect("/");
                //}
                if (userName == string.Empty || password == string.Empty || fullName == string.Empty || email == string.Empty || mobile == string.Empty || address == string.Empty)
                {
                    result = ResultKey.RESULT_FAILES;
                }
                else
                {
                    if (adminID > 0)
                    {
                        admin.FullName = fullName;
                        admin.Email = email;
                        admin.Mobile = mobile;
                        admin.Address = address;

                        bool rs = repository.UpdateAdmin(adminID, admin);
                        if (rs)
                        {
                            bool rs2 = repository.DeleteAccessRight(adminID);
                            AccessRightBO access = new AccessRightBO();
                            access.AdminID = adminID;
                            access.GroupID = groupID;
                            bool rs3 = repository.InsertAccessRight(access);
                            result = ResultKey.RESULT_SUCCESS;
                            Session["GroupAccessID"] = access.GroupID;

                        }
                        else
                        {
                            result = ResultKey.RESULT_FAILES;
                        }
                    }
                    else if (adminID == 0)
                    {
                        admin.FullName = fullName;
                        admin.Email = email;
                        admin.Mobile = mobile;
                        admin.Address = address;
                        admin.UserName = userName;
                        string passMd5 = Utility.MaHoaMD5(password + "1234");
                        admin.Password = passMd5;
                        admin.IsActive = 1;
                        admin.IsDelete = 0;
                        admin.Hashkey = "1234";
                        admin.CreateDate = DateTime.Now;
                        admin.Avatar = string.Empty;

                        bool checkUserName = repository.CheckUserNameExists(userName);
                        if (checkUserName)
                        {
                            result = ResultKey.RESULT_EXISTUSERNAME;
                        }
                        else
                        {
                            bool checkEmail = repository.CheckEmailExists(email);
                            if (checkEmail)
                            {
                                result = ResultKey.RESULT_EXISTEMAIL;
                            }
                            else
                            {
                                int returnAdminID = repository.InsertAdmin(admin);
                                if (returnAdminID > 0)
                                {
                                    AccessRightBO access = new AccessRightBO();
                                    access.AdminID = returnAdminID;
                                    access.GroupID = groupID;
                                    bool rs = repository.InsertAccessRight(access);
                                    result = ResultKey.RESULT_SUCCESS;
                                    Session["GroupAccessID"] = access.GroupID;

                                }
                                else
                                {
                                    result = ResultKey.RESULT_FAILES;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }
        [Authorization]
        [HttpPost]
        public string LockAndUnlockAdmin(int adminID, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/awardwithdraw");
            }
            string result =string.Empty;            
            int statusNew = -1;
            if (status == 1)
            {
                statusNew = 0;
            }
            else
            {
                statusNew = 1;
            }
            bool rs = repository.UpdateStatusAdmin(adminID, statusNew);
            if (rs)
            {
                result = ResultKey.RESULT_SUCCESS;
            }
            return result;
        }
    }
}
