﻿function ConfirmTransaction(transactionID, status, memberID, points) {
    var textMessage = '';
    if (status == 1) {
        textMessage = 'Are you sure confirm this transaction?';
    }
    else if (status == 2) {
        textMessage = 'Are you sure lock this transaction?';
    }
    var confirmMessage = confirm(textMessage);
    if (!confirmMessage) {
        return false;
    }
    else {
        $('#imgLoading_' + transactionID).css("display", "");
        $.ajax({
            type: "post",
            async: false,
            url: "/TransactionPoints/UpdateStatusTransactionPoint",
            data: { transactionID: transactionID, status: status, memberID: memberID, points: points },
            beforeSend: function () {
                $('#imgLoading_' + transactionID).css("display", "");
            },
            success: function (d) {
                $('#imgLoading_' + transactionID).css("display", "none");
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    //alertify.success('Cập nhật thành công');
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                   // alertify.error('Không cập nhật được. Vui lòng liên hệ admin');
                    noty({ text: "Update Error. Please contact admin", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}


function SearchPoint() {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    var page = 1;
    //if (keyword == '') {
    //    noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    //}
    
    $.ajax({
        type: "get",
        async: true,
        url: "/TransactionPoints/SearchPoint",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListPoint').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function PagingSearchTransactionPoint(page) {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/TransactionPoints/SearchPoint",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListPoint').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function ExportExcelPoints()
{
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    var page = 1;
    if (toDate == '' && fromDate == '' && memberID == 0 && status == -1) {
        noty({ text: "Please choose keyword search", layout: "bottomRight", type: "error" });
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/TransactionPoints/ExportListTransactionPoints",
            data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                window.location.href = '/FileExcels/TransactionPoint/' + d;
            },
            error: function (e) {

            }
        });
    }
}