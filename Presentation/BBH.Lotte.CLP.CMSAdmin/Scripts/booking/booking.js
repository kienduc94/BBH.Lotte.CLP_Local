﻿function ConfirmBooking(bookingID, status, memberID, points) {
    var textMessage = '';
    if (status == 1) {
        textMessage = 'Are you sure confirm this transaction?';
    }
    else if (status == 2) {
        textMessage = 'Are you sure lock this transaction?';
    }
    var confirmMessage = confirm(textMessage);
    if (!confirmMessage) {
        return false;
    }
    else {
        $('#imgLoading_' + bookingID).css("display", "");
        $.ajax({
            type: "post",
            async: false,
            url: "/Booking/UpdateStatusBooking",
            data: { bookingID: bookingID, status: status, memberID: memberID, points: points },
            beforeSend: function () {
                $('#imgLoading_' + bookingID).css("display", "");
            },
            success: function (d) {
                $('#imgLoading_' + bookingID).css("display", "none");
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                    noty({ text: "Update Error. Please contact admin", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}

function SearchBooking() {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var page = 1;
    var status = $('#cbStatus').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Booking/SearchBooking",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListBooking').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }

        },
        error: function (e) {

        }
    });
}

function PagingSearchBooking(page) {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Booking/SearchBooking",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListBooking').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}


function ExportExcelBookingTicket()
{
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var page = 1;
    var status = $('#cbStatus').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Booking/ExportListBookingTicket",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")
            window.location.href = '/FileExcels/BookingTicket/' + d;
        },
        error: function (e) {

        }
    });
}