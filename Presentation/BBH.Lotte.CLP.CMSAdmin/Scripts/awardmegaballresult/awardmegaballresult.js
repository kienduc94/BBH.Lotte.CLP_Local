﻿function LockAndUnlockAwardNumber(numberID, status) {
    var confirmMessage = confirm('Are you sure lock this?');
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/AwardMegaballResult/LockAndUnLockAwardNumber",
            data: { numberID: numberID, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    window.location.reload();
                }
            },
            error: function (e) {
            }
        });
    }
}

//function PreventInput(event, value) {
//    var lenght = $('#txtNumberValue').val().length;
//    if (lenght > 16) {
//        event.preventDefault();
//    }
//}

function ShowPopUpEditAwardNumber(numberID, awardID, numberValue, stationNameEnglish) {
    $('#hdNumberID').val(numberID);
    $('#txtNumberValue').val(numberValue);
    //$('#txtStationName').val(stationName);
    //$('#txtStationNameEnglish').val(stationNameEnglish);
    $('#cbAward option[value=' + awardID + ']').prop("selected", true);
    $('#cbAward').trigger('chosen:updated');
}

function ShowPopupCreateNewAwardNumber() {
    $('#hdNumberID').val(0);
    $('#txtNumberValue').val('');
    //$('#txtStationName').val('');
    //$('#txtStationNameEnglish').val('');
    $('#cbAward option[value=0]').prop("selected", true);
}

function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}

function SaveAwardNumber() {
    var checkReg = true;
    var numberID = $('#hdNumberID').val();
    var strBlockHash = $('#txtNumberValue').val();
    var awardID = $('#cbAward').val();
    //var stationName = $('#txtStationName').val();
    //var stationNameEnglish = $('#txtStationNameEnglish').val();
    if (awardID == '0') {
        $('#lbErrorAward').text('Please select award');
        $('#lbErrorAward').css("display", "");
        checkReg = false;
    }
    if (strBlockHash == '') {
        $('#lbErrorNumberValue').text('Input Block Hash is required');
        $('#lbErrorNumberValue').css("display", "");
        checkReg = false;
    }

    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/AwardMegaballResult/SaveAwardNumber",
            data: { numberID: numberID, awardID: awardID, strBlockHash: strBlockHash },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "error" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d == 'isNumberError') {
                    $('#lbErrorNumberValue').text('Number Winner is incorect');
                    $('#lbErrorNumberValue').css("display", "");
                }
            },
            error: function (e) {

            }
        });
    }
}

function SearchAward() {
    var searchDay = $('#txtDatetimeResult').val();
    if (searchDay == '') {
        noty({ text: "Please select date search", layout: "bottomRight", type: "error" });
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/AwardMegaballResult/SearchAwardByDate",
            data: { searchDay: searchDay },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                if (typeof d.isError != 'undefined' && d.isError == 0) {
                    if (typeof d.strAward != 'undefined' && d.strAward.trim().length > 0 &&
                        typeof $('#tbodyListAwardMegaball') != 'undefined' && $('#tbodyListAwardMegaball').length > 0) {
                        $('#tbodyListAwardMegaball').html(d.strAward.trim());
                        if (typeof $('#tbodyWinnerAward') != 'undefined' && $('#tbodyWinnerAward').length > 0) {
                            $('#tbodyWinnerAward').html('<tr><td colspan="8"> <strong> No result found </strong></td></tr>');
                        }
                    }

                    if (typeof $('#divExportWinner') != 'undefined' && $('#divExportWinner').length > 0) {
                        $('#divExportWinner').removeClass('show');
                        $('#divExportWinner').addClass('hide');
                    }

                    if (typeof $('#spanAwardDate') != 'undefined' && $('#spanAwardDate').length > 0) {
                        $('#spanAwardDate').text(searchDay);
                    }

                    //if (typeof $('#spanWinnerDate') != 'undefined' && $('#spanWinnerDate').length > 0) {
                    //    $('#spanWinnerDate').text(searchDay);
                    //}
                }
                else {
                    //error
                    alertify.error('An error occurred! Please try again after.');
                }
                hideLoading();
            },
            error: function (e) {
                alertify.error('An error occurred! Please try again after.');
                hideLoading();
            }
        });
    }
}

function ExportExcelAwardWinner() {
    var intNumberID = 0;
    if (typeof $('#btnExportWinner') != 'undefined' && $('#btnExportWinner').length > 0) {
        intNumberID = $('#btnExportWinner').attr('data-numberid');
    }
    //
    var strDateWinner = "";
    if (typeof $('#spanWinnerDate') != 'undefined' && $('#spanWinnerDate').length > 0) {
        strDateWinner = $('#spanWinnerDate').html();
    }

    if (intNumberID == 0) {
        noty({ text: "Please choose award megaball", layout: "bottomRight", type: "error" });
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/AwardMegaballResult/ExportListWinnerAward",
            data: { intNumberID: intNumberID, searchDay: strDateWinner },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                $('#spanWinnerDate').text(strDateWinner);
                window.location.href = '/FileExcels/AwardWinner/' + d;
            },
            error: function (e) {
                hideLoading();
            }
        });
    }
}

function getWinnerMemberByNumberID(intNumberID) {
    //if (typeof $('#divWinnerMember') && $('#divWinnerMember').length > 0) {
    //if ($('#divWinnerMember').hasClass('hide')) {
    //    $('#divWinnerMember').addClass('show');
    //}
    //else {
    //    $('#divWinnerMember').addClass('hide');
    //}
    $.ajax({
        type: "post",
        async: true,
        url: "/AwardMegaballResult/SearchWinnerAward",
        data: { intNumberID: intNumberID },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            if (typeof d.isError != 'undefined') {
                if (d.isError == 0) {
                    if (typeof $('#btnExportWinner') != 'undefined' && $('#btnExportWinner').length > 0) {
                        $('#btnExportWinner').attr('data-numberid', intNumberID);
                    }
                    if (typeof $('#divExportWinner') != 'undefined' && $('#divExportWinner').length > 0) {
                        $('#divExportWinner').removeClass('hide');
                        $('#divExportWinner').addClass('show');
                    }

                  
                } else if (d.isError == 2) {
                    if (typeof $('#divExportWinner') != 'undefined' && $('#divExportWinner').length > 0) {
                        $('#divExportWinner').removeClass('show');
                        $('#divExportWinner').addClass('hide');
                    }
                }
                else {
                    alertify.error('An error occurred! Please try again after.');
                }

                $('#tbodyWinnerAward').html(d.builder);
            }
            else {
                alertify.error('An error occurred! Please try again after.');
            }
            hideLoading();
        },
        error: function (e) {
            alertify.error('An error occurred! Please try again after.');
            hideLoading();
        }
    });
    //}

}