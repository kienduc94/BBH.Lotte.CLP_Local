﻿function LockAndUnlockExchangePoint(exchangeID, status) {
    var confirmMessage = confirm('Are you sure lock this exchange point?');
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/ExchangePoint/LockAndUnlockExchangePoint",
            data: { exchangeID: exchangeID, status: status },
            beforeSend: function () {

            },
            success: function (d) {
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
            },
            error: function (e) {
            }
        });
    }
}

function ConfirmExchangePoint(exchangeID, status, deleteDate, deleteUser) {
    var textMessage = '';

    if (status == 0) {
        textMessage = 'Are you sure confirm this transaction?';
    }
    else if (status == 1) {
        textMessage = 'Are you sure lock this transaction?';
    }
    var confirmMessage = confirm(textMessage);
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: false,
            url: "/ExchangePoint/UpdateStatusExchangePoint",
            data: { exchangeID: exchangeID, status: status, deleteDate: deleteDate, deleteUser: deleteUser },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                    noty({ text: "Update Error.", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {
                noty({ text: "Update Error. Please contact admin", layout: "bottomRight", type: "error" });
            }
        });
    }
}


function ShowPopupCreateNewExchangePoint() {
    $('#hdExchangeID').val(0);
    $('#txtPointValue').val('');
    $('#txtBitcoinValue').val('');
    //$('#cbMember').val('');
}

function onlyNumbers(evt) {
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function ShowPopUpEditExchangePoint(exchangeID, createUser, pointValue, bitcoinValue,email) {
    $('#hdExchangeID').val(exchangeID);
    $('#txtPointValue').val(pointValue);
    $('#txtBitcoinValue').val(bitcoinValue);
    //$('#cbMember option[value=' + email + ']').attr("selected", true);
}
function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}

function SaveExchangePoint() {
    var checkReg = true;
    var exchangeID = $('#hdExchangeID').val();
    var pointValue = $('#txtPointValue').val();
    var bitcoinValue = $('#txtBitcoinValue').val();
    var createUser = $('txtCreateUser').val();
    //var memberID = $('#cbMember').val();

    if (pointValue == '') {
        $('#lbErrorPointValue').text('PointValue is required');
        $('#lbErrorPointValue').css('display', '');
        checkReg = false;
    }

    if (bitcoinValue == '') {
        $('#lbErrorBitcoinValue').text('BitCoinValue is required');
        $('#lbErrorBitcoinValue').css('display', '');
        checkReg = false;
    }

    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/ExchangePoint/SaveExchangePoint",
            data: { exchangeID: exchangeID, createUser: createUser, pointValue: pointValue, bitcoinValue: bitcoinValue },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d == 'failes')
                {
                    noty({ text: "Update Failes", layout: "bottomRight", type: "error" });
                }
              
            },
            error: function (e) {
            }
        });
    }
}

function SearchExchangePoint() {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var page = 1; 

    $.ajax({
        type: "get",
        async: true,
        url: "/ExchangePoint/SearchExchangePoint",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListExtrangePoint').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {
        }
    });
}

function PagingSearchExchangePoint(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();   

    $.ajax({
        type: "get",
        async: true,
        url: "/ExchangePoint/SearchExchangePoint",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListExtrangePoint').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}










