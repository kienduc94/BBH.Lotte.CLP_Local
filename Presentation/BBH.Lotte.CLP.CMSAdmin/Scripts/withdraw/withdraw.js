﻿function ConfirmWithdraw(transactionID) {//transactionID
    //var transactionID = $('#hdTransactionID').val();
    var status = 1;
    var textMessage = '';
    if (status == 1) {
        textMessage = 'Are you sure confirm this transaction?';
    }
    var confirmMessage = confirm(textMessage);
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/Withdraw/UpdateStatusWithdraw",
            data: { transactionID: transactionID },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d.success == 1) {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d.success == 2) {
                    noty({ text: ""+d.email+"'s BTC are not enought to withdraw!", layout: "bottomRight", type: "error" });
                }
                else {
                    noty({ text: "Update Error. Please contact admin", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {
                hideLoading();
            }
        });
    }

}

function SearchWithdraw() {
    //var strTransactionID = $('#hdTransactionID').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    var page = 1;
    //if (keyword == '') {
    //    noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    //}

    $.ajax({
        type: "get",
        async: true,
        url: "/Withdraw/SearchWithdraw",
        data: {/* strTransactionID: strTransactionID,*/ fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListWithdraw').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function PagingSearchWithdraw(page) {
    //var strTransactionID = $('#hdTransactionID').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Withdraw/SearchWithdraw",
        data: {/* strTransactionID: strTransactionID,*/ fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListWithdraw').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}