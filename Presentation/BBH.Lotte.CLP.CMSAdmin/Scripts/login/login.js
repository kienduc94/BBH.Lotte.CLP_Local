﻿function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};
function RemoveTagHtml(str) {
    return str.replace(/<\/?[^>]+(>|$)/g, "");
}
function checkspace(e) {
    var unicode = e.charCode ? e.charCode : e.keyCode
    if (unicode == 32) {
        return false
    }
}
function str_replace(search, replace, str) {
    var ra = replace instanceof Array, sa = str instanceof Array, l = (search = [].concat(search)).length, replace = [].concat(replace), i = (str = [].concat(str)).length;
    while (j = 0, i--)
        while (str[i] = str[i].split(search[j]).join(ra ? replace[j] || "" : replace[0]), ++j < l);
    return sa ? str : str[0];
}

function remove_vietnamese_accents(str) {
    accents_arr = new Array(
		"à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
		"ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
		"ế", "ệ", "ể", "ễ",
		"ì", "í", "ị", "ỉ", "ĩ",
		"ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
		"ờ", "ớ", "ợ", "ở", "ỡ",
		"ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
		"ỳ", "ý", "ỵ", "ỷ", "ỹ",
		"đ",
		"À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
		"Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
		"È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
		"Ì", "Í", "Ị", "Ỉ", "Ĩ",
		"Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
		"Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
		"Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
		"Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
		"Đ"
	);

    no_accents_arr = new Array(
		"a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
		"a", "a", "a", "a", "a", "a",
		"e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
		"i", "i", "i", "i", "i",
		"o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
		"o", "o", "o", "o", "o",
		"u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
		"y", "y", "y", "y", "y",
		"d",
		"A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
		"A", "A", "A", "A", "A",
		"E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
		"I", "I", "I", "I", "I",
		"O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
		"O", "O", "O", "O", "O",
		"U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
		"Y", "Y", "Y", "Y", "Y",
		"D"
	);

    return str_replace(accents_arr, no_accents_arr, str);
}
function isDoubleByte(str) {
    accents_arr = new Array(
		"à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
		"ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
		"ế", "ệ", "ể", "ễ",
		"ì", "í", "ị", "ỉ", "ĩ",
		"ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
		"ờ", "ớ", "ợ", "ở", "ỡ",
		"ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
		"ỳ", "ý", "ỵ", "ỷ", "ỹ",
		"đ",
		"À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
		"Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
		"È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
		"Ì", "Í", "Ị", "Ỉ", "Ĩ",
		"Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
		"Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
		"Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
		"Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
		"Đ"
	);
    for (var i = 0, n = str.length; i < n; i++) {
        if (str.charCodeAt(i) > 255) { return true; }
    }
    for (var j = 0; j < accents_arr.length; j++) {
        if (str.indexOf(accents_arr[j]) != -1) {
            return true;
        }
    }
    if (!CheckSpecialCharacter(str)) {
        return true;
    }
    return false;
}

function isDoubleByteUserName(str) {
    accents_arr = new Array(
		"à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
		"ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
		"ế", "ệ", "ể", "ễ",
		"ì", "í", "ị", "ỉ", "ĩ",
		"ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
		"ờ", "ớ", "ợ", "ở", "ỡ",
		"ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
		"ỳ", "ý", "ỵ", "ỷ", "ỹ",
		"đ",
		"À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
		"Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
		"È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
		"Ì", "Í", "Ị", "Ỉ", "Ĩ",
		"Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
		"Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
		"Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
		"Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
		"Đ"
	);
    for (var i = 0, n = str.length; i < n; i++) {
        if (str.charCodeAt(i) > 255) { return true; }
    }
    for (var j = 0; j < accents_arr.length; j++) {
        if (str.indexOf(accents_arr[j]) != -1) {
            return true;
        }
    }
    if (!CheckSpecialUserNameCharacter(str)) {
        return true;
    }
    return false;
}

function CheckPassword(str) {
    accents_arr = new Array(
    "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
    "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
    "ế", "ệ", "ể", "ễ",
    "ì", "í", "ị", "ỉ", "ĩ",
    "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
    "ờ", "ớ", "ợ", "ở", "ỡ",
    "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
    "ỳ", "ý", "ỵ", "ỷ", "ỹ",
    "đ",
    "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
    "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
    "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
    "Ì", "Í", "Ị", "Ỉ", "Ĩ",
    "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
    "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
    "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
    "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
    "Đ"
);
    //for (var i = 0, n = str.length; i < n; i++) {
    //    if (str.charCodeAt(i) > 255) { return true; }
    //}
    for (var j = 0; j < accents_arr.length; j++) {
        if (str.indexOf(accents_arr[j]) != -1) {
            return true;
        }
    }
    return false;
}

function CheckSpecialCharacter(value) {
    var checkReg = true;
    var count = 0;
    //value = remove_unicode(value);
    if (value != "") {
        var characterReg = new Array('@', '$', '%', '^', '&', '*', '(', ')', '{', '}', '#', '~', '|', '*', '!', '~', '-', '=', '+', ',', '/', '?', '\'', '\"', ';', ':', '[', ']', '\\', '_', '`', '>', '<', '`', '.');
        //var characterReg = '@$%^&*(){}#~|*';
        for (var i = 0; i < characterReg.length; i++) {
            if (parseInt(value.indexOf(characterReg[i])) > -1) {
                count++;
            }
        }
        if (count > 0) {
            checkReg = false;
        }
    }
    return checkReg;
}

function CheckSpecialUserNameCharacter(value) {
    var checkReg = true;
    var count = 0;
    //value = remove_unicode(value);
    if (value != "") {
        var characterReg = new Array('$', '%', '^', '&', '*', '(', ')', '{', '}', '#', '~', '|', '*', '!', '~', '-', '=', '+', ',', '/', '?', '\'', '\"', ';', ':', '[', ']', '\\', '`', '>', '<', '`');
        //var characterReg = '@$%^&*(){}#~|*';
        for (var i = 0; i < characterReg.length; i++) {
            if (parseInt(value.indexOf(characterReg[i])) > -1) {
                count++;
            }
        }
        if (count > 0) {
            checkReg = false;
        }
    }
    return checkReg;
}


function ResetForm(id) {
    switch (id) {
        case 1:
            {
                $('#lbEmail').text('');
                break;
            }
        case 2:
            {
                $('#lbE_Wallet').text('');
                break;
            }
        case 3:
            {
                $('#lbPass').text('');
                break;
            }
    }
}
//function MemberLogin() {
//    var username = $('#txtEmail').val();
//    //var ewallet = $('#txtE_Wallet').val();
//    var password = $('#txtPassword').val();
//    var checkReg = true;
//    var checkPassword = CheckPassword(password);
//    if (username == '') {
//        checkReg = false;
//        $('#divMessage').text('Please input username and password');
//    }

//    if (password == '') {
//        checkReg = false;
//        $('#divMessage').text('Please input username and password');
//    }
//    if (!checkReg) {
//        return false;
//    }
//    else {
//        $.ajax({
//            type: "post",
//            url: "/Login/MemberLogin",
//            async: true,
//            data: { username: username, password: password },
//            beforeSend: function () {
//                $('#imgLoading').css("display", "");
//            },
//            success: function (d) {
//                $('#imgLoading').css("display", "none");
//                if (d == 'loginfaile') {
//                    $('#divMessage').text("Username or password is wrong");
//                }
//                else if (d == 'loginSuccess') {
//                    window.location.href = '/';
//                }
//            },
//            error: function () {

//            }
//        });
//    }
//}
function MemberLogin() {
    var username = $('#txtEmail').val();
    var password = $('#txtPassword').val();
    var checkReg = true;
    if (username == '') {
        checkReg = false;
        $('#divMessage').text('Username and password is requied');
    }
    if (password == '') {
        checkReg = false;
        $('#divMessage').text('Username and password is requied');
    }
    else if (CheckPassword(password)) {
        checkReg = false;
        $('#divMessage').text('Password is incorect');
    }
    var strCaptcha = "";
    if (typeof $('#g-recaptcha-response') != 'undefined' && $('#g-recaptcha-response').length > 0) {
        strCaptcha = $('#g-recaptcha-response').val();
    }

    if (strCaptcha == null || strCaptcha.trim().length == 0) {
        if (typeof $('#lbrecaptcha') != 'undefined' && $('#lbrecaptcha').length > 0) {
            $('#lbrecaptcha').text('The captcha field is required!');
            $('#lbrecaptcha').css('display', '');
        }
        checkReg = false;
    }
    if (!checkReg) {
        return false;
    }
    else {
        var param = {};
        param.strUsername = username;
        param.strPassword = password;
        param.strCaptcha = strCaptcha;
        $.ajax({
            type: "post",
            url: "/Login/MemberLogin",
            async: true,
            data: param,
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (typeof d.intTypeError != 'undefined' && typeof d.result != 'undefined' && typeof d.username != 'undefined' && typeof d.password != 'undefined') {
                    LoginAdminSuccess(d.intTypeError, d.result, d.username, d.password);
                }
            },
            error: function () {
                noty({ text: "Login Error, Please contact to admin.", layout: "bottomRight", type: "error" });
            }
        });
    }
}
function LoginAdminSuccess(intTypeError, strResult, strUsername, strPassword) {
    if (intTypeError != 0) {
        if (intTypeError == 1) {
            if (typeof $('#divMessage') != 'undefined' && $('#divMessage').length > 0) {
                $('#divMessage').text('Email and Password required for login.');
                $('#divMessage').css('display', '');
            }
        }
        else if (intTypeError == 2) {
            if (typeof $('#divMessage') != 'undefined' && $('#divMessage').length > 0) {
                $('#divMessage').text('Email and Password required for login.');
                $('#divMessage').css('display', '');
            }
        }
        else if (intTypeError == 3) {
            if (typeof $('#lbrecaptcha') != 'undefined' && $('#lbrecaptcha').length > 0) {
                $('#lbrecaptcha').text('The captcha field is required!');
                $('#lbrecaptcha').css('display', '');
            }
        }
    }
    else {
        if (strResult == 'loginSuccess') {
            window.location.href = '/';
        }          
        else if (strResult == 'errorCaptcha') {
            ResetDataElement($('#hdUsername').val(), '', 'The captcha field is required!');            
        }
        else if (strResult == 'loginfaile') {
            ResetDataElement($('#hdUsername').val(), '', '');
            $('#divMessage').text("Username or password is wrong");          
        }
    }   
}
function ResetDataElement(strUsername, strPassword, strConfirmCaptcha) {
    if (typeof $('#txtEmail') != 'undefined' && $('#txtEmail').length > 0) {
        $('#txtEmail').val(strUsername);
    }
    if (typeof $('#txtPassword') != 'undefined' && $('#txtPassword').length > 0) {
        $('#txtPassword').val(strPassword);
    }
    if (typeof $('#g-recaptcha-response') != 'undefined' && $('#g-recaptcha-response').length > 0) {
        $('#g-recaptcha-response').val(strConfirmCaptcha);
    }
    grecaptcha.reset();


    if (typeof $('#divMessage') != 'undefined' && $('#divMessage').length > 0) {
        $('#divMessage').text('');
       /* $('#divMessage').hide(); */   }

    if (typeof $('#divMessage') != 'undefined' && $('#divMessage').length > 0) {
        $('#divMessage').text('');
        //$('#divMessage').hide();
    }
    if (typeof $('#lbrecaptcha') != 'undefined' && $('#lbrecaptcha').length > 0) {
        $('#lbrecaptcha').text(strConfirmCaptcha);
        if (strConfirmCaptcha.trim().length == 0) {
            $('#lbrecaptcha').hide();
        }
    }
}

function EnterLogin(event, value) {
    if (event.which == 13 || event.keyCode == 13) {
        var username = $('#txtEmail').val();
        var password = $('#txtPassword').val();
        var checkReg = true;
        if (username == '') {
            $('#divMessage').text('Username and password is requied');
            checkReg = false;
        }
        if (password == '') {
            $('#divMessage').text('Username and password is requied');
            checkReg = false;
        }
        else if (CheckPassword(password)) {
            $('#divMessage').text('Password is incorect');
            checkReg = false;
        }
        if (!checkReg) {
            return false;
        }
        else {
            $.ajax({
                type: "post",
                url: "/Login/MemberLogin",
                async: true,
                data: { username: username, password: password },
                beforeSend: function () {
                    showLoading();
                },
                success: function (d) {
                    hideLoading();
                    if (d == 'ResultKey.RESULT_LOGINFAILES') {
                        $('#divMessage').text("Username or password is wrong");
                    }
                    else if (d == 'ResultKey.RESULT_LOGINSUCCESS') {
                        window.location.href = '/';
                    }
                    else if (d == 'ResultKey.RESULT_USERNAMENULL') {
                        $('#divMessage').text('Username and password is requied');
                    }
                    else if (d == 'ResultKey.RESULT_PASSWORDNULL') {
                        $('#divMessage').text('Username and password is requied');
                    }
                },
                error: function () {

                }
            });
        }
    }

}
function ExistsMember() {
    $.ajax({
        type: "post",
        url: "/Login/LogoutMember",
        async: true,
        beforeSend: function () {
            
        },
        success: function (d) {
            window.location.href = '/';
        }
    });
}

function ToggleSubMenu() {
    $('.dropdown-menu').toggle();
}