﻿function LockAndUnlockAwardMegaball(drawID, status) {
    var confirmMessage = confirm('Are you sure lock this award megaball?');
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/AwardMegaball/LockAndUnlockAwardMegaball",
            data: { drawID: drawID, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {
            }
        });
    }
}
function ShowPopUpEditAwardMegaball(numberID, awardID, strBlockHash, awardNameEnglish, jackPot) {
    $('#hdNumberID').val(numberID);
    // $('#txtAwardID').val(awardID);  
    $('#txtBlockHash').val(strBlockHash);
    //$('#txtJackPot').val(jackPot);
    $('#cbAward option[value=' + awardID + ']').attr("selected", true);
    $('#cbAward').trigger('chosen:updated');
    setTimeout(function () {
        $("#cbAward").chosen({
            width: '200px'
        });
    }, 500);
}
function ShowPopupCreateNewAwardMegaball() {
    $('#hdNumberID').val(0);
    $('#txtAwardID').val('');
    //$('#txtJackPot').val('');
    $('#txtBlockHash').val('');
    $('#cbAward option[value=0]').attr("selected", true);
}

function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}

function SearchAwardMegaball() {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var page = 1;

    $.ajax({
        type: "get",
        async: true,
        url: "/AwardMegaball/SearchAwardMegaball",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListAward').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function PagingSearchAwardMegaball(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    if (fromDate == '' || toDate == '') {
        noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    }

    $.ajax({
        type: "get",
        async: true,
        url: "/AwardMegaball/SearchAwardMegaball",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListAward').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function SaveAwardMegaball() {
    var checkReg = true;
    var numberID = $('#hdNumberID').val();
    // var awardID = $('#txtAwardID').val();
    var firstNumber = $('#txtAwardMegaballFirstNumber').val();
    var secondNumber = $('#txtAwardMegaballSecondNumber').val();
    var thirdNumber = $('#txtAwardMegaballThirdNumber').val();
    var fourthNumber = $('#txtAwardMegaballFourthNumber').val();
    var fivethNumber = $('#txtAwardMegaballFivethNumber').val();
    var extraNumber = $('#txtAwardMegaballExtraNumber').val();
    var awardID = $('#cbMember').val();

    if (awardID == '') {
        $('#lbErrorAwardNameEnglish').text('Please choise AwardNameEnglish');
        $('#lbErrorAwardNameEnglish').css('display', '');
        checkReg = false;
    }

    if (firstNumber == '') {
        $('#lbErrorAwardMegaballFirstNumber').text('Please input first number');
        $('#lbErrorAwardMegaballFirstNumber').css('display', '');
        checkReg = false;
    }
    else {
        if (firstNumber.lenght > 2 || firstNumber > 50) {
            $('#lbErrorAwardMegaballFirstNumber').text('Enter up to 2 numbers and less than numbers 49');
            $('#lbErrorAwardMegaballFirstNumber').css('display', '');
            checkReg = false;
        }
    }

    if (secondNumber == '') {
        $('#lbErrorAwardMegaballSecondNumber').text('Please input second number');
        $('#lbErrorAwardMegaballSecondNumber').css('display', '');
        checkReg = false;
    }
    else {
        if (secondNumber.lenght > 2 || secondNumber > 50) {
            $('#lbErrorAwardMegaballSecondNumber').text('Enter up to 2 numbers and less than numbers 49');
            $('#lbErrorAwardMegaballSecondNumber').css('display', '');
            checkReg = false;
        }
    }

    if (thirdNumber == '') {
        $('#lbErrorAwardMegaballThirdNumber').text('Please input third number');
        $('#lbErrorAwardMegaballThirdNumber').css('display', '');
        checkReg = false;
    }
    else {
        if (thirdNumber.lenght > 2 || thirdNumber > 50) {
            $('#lbErrorAwardMegaballThirdNumber').text('Enter up to 2 numbers and less than numbers 49');
            $('#lbErrorAwardMegaballThirdNumber').css('display', '');
            checkReg = false;
        }
    }

    if (fourthNumber == '') {
        $('#lbErrorAwardMegaballFourthNumber').text('Please input fourth number');
        $('#lbErrorAwardMegaballFourthNumber').css('display', '');
        checkReg = false;
    }
    else {
        if (fourthNumber.lenght > 2 || fourthNumber > 50) {
            $('#lbErrorAwardMegaballFourthNumber').text('Enter up to 2 numbers and less than numbers 49');
            $('#lbErrorAwardMegaballFourthNumber').css('display', '');
            checkReg = false;
        }
    }

    if (fivethNumber == '') {
        $('#lbErrorAwardMegaballFivethNumber').text('Please input fiveth number');
        $('#lbErrorAwardMegaballFivethNumber').css('display', '');
        checkReg = false;
    }
    else {
        if (fivethNumber.lenght > 2 || fivethNumber > 50) {
            $('#lbErrorAwardMegaballFivethNumber').text('Enter up to 2 numbers and less than numbers 49');
            $('#lbErrorAwardMegaballFivethNumber').css('display', '');
            checkReg = false;
        }
    }

    if (extraNumber == '') {
        $('#lbErrorAwardMegaballExtraNumber').text('Please input extra number');
        $('#lbErrorAwardMegaballExtraNumber').css('display', '');
        checkReg = false;
    }
    else {
        if (extraNumber.lenght > 2 || extraNumber > 27) {
            $('#lbErrorAwardMegaballExtraNumber').text('Enter up to 2 numbers and less than numbers 26');
            $('#lbErrorAwardMegaballExtraNumber').css('display', '');
            checkReg = false;
        }
    }

    if (!checkReg) {
        return false;
    }
    else {
        $('#imgLoading').css("display", "");
        $.ajax({
            type: "post",
            async: true,
            url: "/AwardMegaball/SaveAwardMegaball",
            data: { numberID: numberID, awardID: awardID, firstNumber: firstNumber, secondNumber: secondNumber, thirdNumber: thirdNumber, fourthNumber: fourthNumber, fivethNumber: fivethNumber, extraNumber: extraNumber },
            beforeSend: function () {
                $('#imgLoading').css("display", "");
            },
            success: function (d) {
                $('#imgLoading').css("display", "none");
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d == 'NumberExit') {
                    //alert("Number Exit, please check again!!!");
                    noty({ text: "Number Exit, please check again!!!", layout: "bottomRight", type: "error" });

                }
                else if (d == 'CheckTimeOpenClose') {
                    noty({ text: "Lottery time is closed. Lottery time is from 8 - 8:30 on tuesday, thursday, saturday.", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}

function SubmitAward() {
    var check = true;
    var numberID = $('#hdNumberID').val();
    var awardID = $('#cbAward').val();
    var strBlockHash = $("#txtBlockHash").val();
    //var jackPot = $("#txtJackPot").val();

    if (strBlockHash == '') {
        $('#lbErrorHashText').text('BlockHash is required');
        $('#lbErrorHashText').css('display', '');
        check = false;
    }
    //if (jackPot == '') {
    //    $('#lbErrorJackPot').text('Draw is required');
    //    $('#lbErrorJackPot').css('display','');
    //}
    if (awardID == 0) {
        $('#lbErrorAwardNameEnglish').text('Choose AwardName');
        $('#lbErrorAwardNameEnglish').css('display', '');
        check = false;
    }
    if (!check) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            url: "/AwardMegaball/SaveAwardResultNumber",
            async: true,
            data: { numberID: numberID, awardID: awardID, strBlockHash: strBlockHash/*, jackPot: jackPot*/ },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d.isSuccess == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d.isSuccess == 'failes') {
                    //noty({ text: "String LockHash not incorrect !", layout: "bottomRight", type: "error" }); 
                    $('#lbErrorHashText').text('String LockHash not incorrect !');
                    $('#lbErrorHashText').css('display', '');
                }
                else if (d.isSuccess == 'CheckTimeOpenClose') {
                    noty({ text: "Lottery time is closed. Lottery time is from " + d.strTime + " on " + d.strDateOpen + ".", layout: "bottomRight", type: "error" });
                }
                else if (d.isSuccess == 'CreateDateExist') {
                    noty({ text: "Lottery Award Megaball Exist", layout: "bottomRight", type: "error" });
                }
                else if (d.isSuccess == 'CreateDate Award Megaball Exist') {
                    noty({ text: "Lottery Award Megaball Exist", layout: "bottomRight", type: "error" });
                }
            },
            error: function () {
                hideLoading();
                noty({ text: "Update failse. Please contact to Admin!", layout: "bottomRight", type: "error" });
            }
        });
    }
}
