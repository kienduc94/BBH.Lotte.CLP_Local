﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.CMSAdmin.Models
{
    public class ObjPrize
    {
        public int AwardID { get; set; }

        public string AwardName { get; set; }

        public double AwardValues { get; set; }
    }
}