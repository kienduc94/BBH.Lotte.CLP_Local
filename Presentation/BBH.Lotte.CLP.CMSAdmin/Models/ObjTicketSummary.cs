﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.CMSAdmin.Models
{
    public class ObjTicketSummary
    {
        public int Month { get; set; }

        public int TotalTicket { get; set; }

        public int TotalTicketFree { get; set; }
    }
}