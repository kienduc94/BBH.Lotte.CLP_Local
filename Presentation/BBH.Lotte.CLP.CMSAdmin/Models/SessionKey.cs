﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CMSAdmin.Models
{
    public class SessionKey
    {
        public const string SESSION_USERNAME = "UserName";
        public const string SESSION_GROUPID = "Group";
        public const string SESSION_EMAIL = "Email";
        public static string UserName
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session[SessionKey.SESSION_USERNAME]);
            }
        }
    }
}