﻿using BBH.Lotte.CMSAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.CMSAdmin.Models
{
    public class Authorization: ActionFilterAttribute
    {
        public static bool IsLogedIn()
        {
            if (System.Web.HttpContext.Current.Session[SessionKey.SESSION_USERNAME] == null)
                return false;
            return true;

        }

        /// <summary>
        /// Created date: 15.03.2018
        /// Description: call when refresh web page -> check if not login then redirect to page Login
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!IsLogedIn())
            {
                //if (string.IsNullOrEmpty(SignInUrl))
                string SignInUrl = "/login";
                context.HttpContext.Response.Redirect(SignInUrl, true);
                context.Result = new RedirectResult(SignInUrl, true);
            }
        }
    }
}