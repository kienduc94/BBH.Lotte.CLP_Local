﻿using BBH.Lotte.CLP.Shared;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;

using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;
using BBH.Lotte.CLP.Web.Models;
using Newtonsoft.Json;
using BBH.Lotte.CLP.Wallet.Repository;
using BBC.CWallet.ServiceListener.ServiceContract;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class CoinController : Controller
    {

        string pathLog = ConfigurationManager.AppSettings["PathLog"];
        string masterKeyWan2Lot = ConfigurationManager.AppSettings["KeyWan2Lot"];
        string ClientID = ConfigurationManager.AppSettings["ClientID"];
        string TimeExpired = ConfigurationManager.AppSettings["TimeExpired"];
        string Linktestnet = ConfigurationManager.AppSettings["LinkTestnet"];
        string RippleTag = ConfigurationManager.AppSettings["RippleTag"];
        public string Bitcoin = ConfigurationManager.AppSettings["Bitcoin"];
        public string Carcoin = ConfigurationManager.AppSettings["Carcoin"];

        public string WalletId = ConfigurationManager.AppSettings["WalletId"];
        public string ClientId = ConfigurationManager.AppSettings["ClientId"];
        public string SystemId = ConfigurationManager.AppSettings["SystemId"];
        public string Limit = ConfigurationManager.AppSettings["PageSize"];

        //sysFeeTxn
        string sysFeeTxn_UserID = ConfigurationManager.AppSettings[KeyManager.SysFeeTxnID];
        string sysFeeTxn_Address = ConfigurationManager.AppSettings[KeyManager.SysFeeTxnWalletAddress];

        TransactionRepository transaction = new TransactionRepository();

        [Dependency]
        protected IMemberServices objMemberRepository { get; set; }

        [Dependency]
        protected ICoinServices objCoinRepository { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TransactionBTCWallet(string p)
        {
            ViewBag.LinkTestnet = Linktestnet;
            ViewBag.Bitcoin = Bitcoin;
            int intPageSize = 10;
            int fromIndex = 0;
            int start = 0, end = 10;
            int flag = -1;
            if (System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] == null)
            {
                Response.Redirect("/login");
            }
            else
            {
                Int32.TryParse(ConfigurationManager.AppSettings["PageSize"], out intPageSize);
                try
                {
                    if (p != null && p != "")
                    {
                        fromIndex = int.Parse(p);
                    }
                }
                catch(Exception objEx)
                {
                    BBC.CWallet.Share.BBCLoggerManager.Error("CoinController -> TransactionBTCWallet -> fromIndex = int.Parse(p): " + objEx.ToJsonString());
                }
                start = (fromIndex - 1) * intPageSize + 1;
                end = (fromIndex * intPageSize);
                int intTimeClosedCountDown = int.Parse(ConfigurationManager.AppSettings["TimeClosedCountDown"]);
                int intTimeOpenCountDown = int.Parse(ConfigurationManager.AppSettings["TimeOpenCountDown"]);

                ViewData[KeyManager._HOUR_COUNT_DOWN] = intTimeClosedCountDown;
            }
            return View(GenHTMLBody_TransactionBTCWallet(CurrencyCode.BTC.ToString(), RDUser.MemberID, DateTime.Parse("01/01/1990"), DateTime.Now, flag, fromIndex, intPageSize));
        }

        public List<string> GenHTMLBody_TransactionBTCWallet(string coinID, int memberID, DateTime? dtbStart, DateTime? dtbEnd, int flag, int formIndex, int intPageSize)
        {
            List<string> lst = null;
            string strTotalRows = "0";
            int stt = 1;
            if (formIndex == 0)
            {
                stt = formIndex + stt;
            }
            else
            {
                stt = (formIndex * intPageSize) + 1;
            }
            try
            {
                TransactionFilterRequest transactionRequet = new TransactionFilterRequest();

                transactionRequet.ClientID = short.Parse(ClientId);
                transactionRequet.WalletId = short.Parse(WalletId);
                transactionRequet.SystemID = short.Parse(SystemId);
                transactionRequet.Limit = short.Parse(Limit);
                transactionRequet.UserId = (Session[SessionKey.USERNAME] != null ? Session[SessionKey.USERNAME].ToString() : null);
                transactionRequet.TransactionType = TransactionType.None;
                transactionRequet.FromDate = dtbStart;
                transactionRequet.ToDate = dtbEnd;
                transactionRequet.FromIndex = short.Parse(formIndex.ToString());
                transactionRequet.CurrencyCode = CurrencyCode.BTC;
               
                CWalletList<TransactionInfo> transactionInfo = transaction.List(transactionRequet);
                StringBuilder strBOBj = new StringBuilder();

                if (transactionInfo != null && transactionInfo.Data != null && transactionInfo.Data.Count > 0)
                {
                    int i = 0;
                    string color = "#eee";
                    strTotalRows = transactionInfo.TotalRecord.ToString();
                    foreach (TransactionInfo item in transactionInfo.Data)
                    {
                        if (item.ToUserID == null || item.ToUserID != sysFeeTxn_UserID)
                        {
                            string Link = item.Tx;
                            if (i % 2 != 0)
                            {
                                color = "#eee";
                            }
                            else
                            {
                                color = "#fff";
                            }
                            string dateTime = item.CreatedDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime);
                            string strTransactionID = item.TransactionId.ToString();
                            //string strValueTransaction = (item.Amount > 0 ? string.Format("{0:0.00000000}", item.Amount) : "0");
                            string strValueTransaction = item.Amount.ToString("G29");
                            strBOBj.Append("<tr class='info pointer'>");
                            strBOBj.Append("<td class='text-center'>" + stt + "</td>");
                            strBOBj.Append("<td class='text-center'>" + dateTime + " UTC </td>");
                            strBOBj.Append("<td class='text-center' onclick=\"ShowTransactionWalletDetail('" + strTransactionID + "','" + item.FromAddress + "','" + item.ToAddress + "','" + strValueTransaction + "','" + Link + "','" + item.CreatedDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "','" + coinID + "')\" data-toggle=\"modal\" data-target=\"#myModal_" + coinID + "\">" + strTransactionID + "</td>");
                            strBOBj.Append("<td class='text-center'>" + strValueTransaction + "</td>");
                            strBOBj.Append("<td class='text-center'><a href=\"" + Linktestnet + "" + item.Tx + "\" target =\"_blank\">" + item.Tx + "</a></td>");

                            strBOBj.Append("<td class='text-center'>" + item.TransactionType + "</td>");
                            strBOBj.Append("<td class='text-center'>" + item.Status + "</td>");
                            strBOBj.Append("</tr>");
                            stt++;
                        }
                    }
                }
                else
                {
                    strBOBj.Append("<tr><td colspan='7'>No Data</td></tr>");
                }
                lst = new List<string>();
                lst.Add(strBOBj.ToString());
                TempData["TotalRecord"] = int.Parse(strTotalRows);

                lst.Add(GenPaging(formIndex.ToString(), coinID));
                return lst;
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("CoinController -> GenHTMLBody_TransactionBTCWallet: " + objEx.ToJsonString());
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objEx: " + (objEx.Message));
                return null;
            }
        }


        public PartialViewResult TransactionCar(string p)
        {
            ViewBag.LinkTestnet = Linktestnet;
            ViewBag.Carcoin = Carcoin;
            int intPageSize = 10;
            int start = 0, end = 10;
            int flag = -1;
            int page = 0;
            if (System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] == null)
            {
                Response.Redirect("/login");
            }
            else
            {
                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);

                int intTimeClosedCountDown = int.Parse(ConfigurationManager.AppSettings["TimeClosedCountDown"]);
                int intTimeOpenCountDown = int.Parse(ConfigurationManager.AppSettings["TimeOpenCountDown"]);

                ViewData[KeyManager._HOUR_COUNT_DOWN] = intTimeClosedCountDown;

            }
            return PartialView(GenHTMLBody_TransactionCar(Carcoin, RDUser.MemberID, start, end, DateTime.Parse("01/01/1990"), DateTime.Now, flag, page, intPageSize));

        }

        public List<string> GenHTMLBody_TransactionCar(string coinID, int memberID, int start, int end, DateTime? dtbStart, DateTime? dtbEnd, int flag, int formIndex, int intPageSize)
        {
            List<string> lst = null;
            string strTotalRows = "0";
            int pageSize = 5, page = 1;
            try
            {
                pageSize = int.Parse(ConfigurationManager.AppSettings["NumberRecordPage"]);
            }
            catch(Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("CoinController -> GenHTMLBody_TransactionCar -> int.Parse(ConfigurationManager.AppSettings['NumberRecordPage']): " + objEx.ToJsonString());
            }
           
            try
            {
                StringBuilder strBOBj = new StringBuilder();
                TransactionFilterRequest transactionRequet = new TransactionFilterRequest();

                transactionRequet.ClientID = short.Parse(ClientId);
                transactionRequet.WalletId = short.Parse(WalletId);
                transactionRequet.SystemID = short.Parse(SystemId);
                transactionRequet.Limit = short.Parse(Limit);
                transactionRequet.UserId = Session[SessionKey.USERNAME].ToString();
                transactionRequet.TransactionType = TransactionType.None;
                transactionRequet.FromDate = dtbStart;
                transactionRequet.ToDate = dtbEnd;
                transactionRequet.FromIndex = short.Parse(formIndex.ToString());
                transactionRequet.CurrencyCode = CurrencyCode.Carcoin;

                CWalletList<TransactionInfo> transactionInfo = transaction.List(transactionRequet);
                if (transactionInfo != null && transactionInfo.Data != null&& transactionInfo.Data.Count > 0)
                {
                    strTotalRows = transactionInfo.TotalRecord.ToString().Trim();
                    int stt = 1;

                    if (formIndex == 0)
                    {
                        stt = formIndex + stt;
                    }
                    else
                    {
                        stt = (formIndex * intPageSize) + 1;
                    }
                    int i = 0;
                    string color = "#eee";
                    page = 0;
                    foreach (TransactionInfo item in transactionInfo.Data)
                    {
                       
                        string Link = item.Tx;
                        if (i % 2 != 0)
                        {
                            color = "#eee";
                        }
                        else
                        {
                            color = "#fff";
                        }
                        string strTransactionID = item.TransactionId.ToString();
                        string strValueTransaction = (item.Amount > 0 ? string.Format("{0:0.00000000}", item.Amount) : "0");


                        strBOBj.Append("<tr class='info pointer'>");
                        strBOBj.Append("<td class='text-center'>" + stt + "</td>");
                        strBOBj.Append("<td class='text-center'>" + item.CreatedDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "</td>");
                        strBOBj.Append("<td onclick=\"ShowTransactionWalletDetail('" + strTransactionID + "','" + item.FromAddress + "','"+item.ToAddress+"','" + strValueTransaction + "','" + Link + "','" + item.CreatedDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "','" + coinID + "')\" data-toggle=\"modal\" data-target=\"#myModal_" + coinID + "\">" + strTransactionID + "</td>");
                        strBOBj.Append("<td>" + strValueTransaction + "</td>");
                        strBOBj.Append("<td><a href=\"" + Linktestnet + "" + item.Tx + "\" target =\"_blank\">" + item.Tx + "</a></td>");

                        strBOBj.Append("<td>" + item.TransactionType + "</td>");
                        strBOBj.Append("</tr>");
                        stt++;
                    }
                }
                else
                {
                    strBOBj.Append("<tr><td colspan='6'> No Data </td></tr>");
                }
                lst = new List<string>();
                lst.Add(strBOBj.ToString());
                TempData["TotalRecord"] = int.Parse(strTotalRows);
                lst.Add(GenPaging(formIndex.ToString(), coinID));
                return lst;
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("CoinController -> GenHTMLBody_TransactionCar: " + objEx.ToJsonString());
                return null;
            }
        }

        #region public ActionResult TransactionCarWallet(string p)

        //public ActionResult TransactionCarWallet(string p)
        //{
        //    ViewBag.LinkTestnet = Linktestnet;

        //    //string memberID = "-1";
        //    //int totalRecord = 0;
        //    int intPageSize = 10;
        //    int start = 0, end = 10;
        //    int flag = -1;
        //    if (System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] == null)
        //    {
        //        Response.Redirect("/login");
        //    }
        //    else
        //    {
        //        Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
        //        int page = 1;
        //        try
        //        {
        //            if (p != null && p != "")
        //            {
        //                page = int.Parse(p);
        //            }
        //        }
        //        catch
        //        {

        //        }

        //        start = (page - 1) * intPageSize + 1;
        //        end = (page * intPageSize);

        //        //if (Session["memberid"] != null)
        //        //{
        //        //    memberID = Session["memberid"].ToString();
        //        //}
        //        /////////////
        //        //ViewData[KeyManager._CHECK_LOCK_BOOKING] = 0;
        //        int intTimeClosedCountDown = int.Parse(ConfigurationManager.AppSettings["TimeClosedCountDown"]);
        //        int intTimeOpenCountDown = int.Parse(ConfigurationManager.AppSettings["TimeOpenCountDown"]);

        //        ViewData[KeyManager._HOUR_COUNT_DOWN] = intTimeClosedCountDown;

        //        //if (DateTime.Now.Hour >= intTimeClosedCountDown && DateTime.Now.Hour <= intTimeOpenCountDown)
        //        //{
        //        //    ViewData[KeyManager._CHECK_LOCK_BOOKING] = 1;
        //        //}
        //    }
        //    /////////////
        //    //return View(GenHTMLBody_TransactionWallet(int.Parse(memberID), start, end, DateTime.Now, DateTime.Now, flag, p));
        //    return View(GenHTMLBody_TransactionCarWallet(Carcoin, RDUser.MemberID, start, end, DateTime.Parse("01/01/1990"), DateTime.Now, flag, p));
        //}
        #endregion

        #region public List<string> GenHTMLBody_TransactionCarWallet(string coinID, int memberID, int start, int end, DateTime? dtbStart, DateTime? dtbEnd, int flag, string p)
        //public List<string> GenHTMLBody_TransactionCarWallet(string coinID, int memberID, int start, int end, DateTime? dtbStart, DateTime? dtbEnd, int flag, string p)
        //{
        //    List<string> lst = null;
        //    string strTotalRows = "0";
        //    int pageSize = pagesize; int pageSize = 5, page = 1;
        //    int pageSize = 5, page = 1;
        //    try
        //    {
        //        if (p != null && p != "")
        //        {
        //            page = int.Parse(p);
        //        }
        //    }
        //    catch
        //    {

        //    }
        //    try
        //    {
        //        pageSize = int.Parse(ConfigurationManager.AppSettings["NumberRecordPage"]);
        //    }
        //    catch
        //    {
        //    }
        //    try
        //    {
        //        PointRepository objPointRepository = new PointRepository();
        //        IEnumerable<TransactionCoinBO> lstTransactionPointsBO = null;

        //        lstTransactionPointsBO = objCoinRepository.ListTransactionCoinWalletBySearch(Carcoin, memberID, dtbStart, dtbEnd, start, end);

        //        StringBuilder strBOBj = new StringBuilder();
        //        if (lstTransactionPointsBO != null)
        //        {
        //            int i = 0;
        //            string color = "#eee";
        //            page = page - 1;
        //            foreach (TransactionCoinBO item in lstTransactionPointsBO)
        //            {
        //                strTotalRows = item.TotalRecord.ToString().Trim();
        //                string Link = item.TransactionBitcoin;
        //                if (i % 2 != 0)
        //                {
        //                    color = "#eee";
        //                }
        //                else
        //                {
        //                    color = "#fff";
        //                }
        //                int number = pageSize * page + i + 1;
        //                string strTransactionID = item.TransactionID;
        //                string strValueTransaction = (item.ValueTransaction > 0 ? string.Format("{0:0.00000000}", item.ValueTransaction) : "0");

        //                const int maxlenght = 30;
        //                string transactionCoin = item.TransactionBitcoin.ToString();
        //                string dot = "...";
        //                if (transactionCoin.Length > maxlenght)
        //                {
        //                    transactionCoin = transactionCoin.Substring(0, maxlenght) + "" + dot;
        //                }
        //                strBOBj.Append("<tr class='info pointer' onclick=\"ShowTransactionWalletDetail('" + item.TransactionID + "','" + item.WalletAddressID + "','" + item.WalletID + "','" + strValueTransaction + "','" + Link + "','" + item.CreateDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "','" + item.ExpireDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "','" + coinID + "')\" data-toggle=\"modal\" data-target=\"#myModal\" >");
        //                strBOBj.Append("<td class='text-center'>" + item.CreateDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "</td>");
        //                strBOBj.Append("<td class='text-center'>" + strTransactionID + "</td>");
        //                strBOBj.Append("<td class='text-center'>" + strValueTransaction + "</td>");
        //                strBOBj.Append("<td class='text-center'><a href=\"" + Linktestnet + "" + item.TransactionBitcoin + "\" target =\"_blank\">" + transactionCoin + "</a></td>");

        //                strBOBj.Append("<td class='textAlignCenter'>" + item.CreateDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "</td>");
        //                strBOBj.Append("</tr>");
        //                i++;
        //            }
        //        }
        //        else
        //        {
        //            strBOBj.Append("<tr><td colspan='4'><div class='tfalert alert-warning'>Sorry, nothing found</div></td></tr>");
        //        }
        //        lst = new List<string>();
        //        lst.Add(strBOBj.ToString());
        //        TempData["TotalRecord"] = int.Parse(strTotalRows);
        //        lst.Add(GenPaging(p, coinID));
        //        lst.Add(pageSize.ToString());
        //        return lst;
        //    }
        //    catch (Exception objEx)
        //    {
        //        objResultMessage.IsError = true;
        //        objResultMessage.Message = objEx.Source;
        //        objResultMessage.MessageDetail = objEx.Message;
        //        new Library.Common.WriteErrorLog().WriteLog(objResultMessage, "GenHTMLBody_EFormTypeList ---> EOfficeMasterDataController");
        //        return null;
        //    }
        //}
        #endregion

        public string GenPaging(string p, string coinID)
        {
            StringBuilder builder = new StringBuilder();
            int pageSize = 5, page = 0;

            try
            {
                if (p != null && p != "")
                {
                    page = int.Parse(p);
                }
            }
            catch(Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("CoinController -> GenPaging -> page = int.Parse(p): " + objEx.ToJsonString());
            }
            try
            {
                pageSize = int.Parse(ConfigurationManager.AppSettings["NumberRecordPage"]);
            }
            catch(Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("CoinController -> GenPaging -> int.Parse(ConfigurationManager.AppSettings['NumberRecordPage']): " + objEx.ToJsonString());
            }
            int intPre = 0, intNext = 0;
            string linkPage = "", nextLink = "", previewLink = "", firstLink = "", lastLink = "";
            firstLink = "?p=1";
            if (page <= 0)
            {
                previewLink = "?p=0";
                intPre = 0;
            }
            else
            {
                previewLink = "?p=" + (page - 1).ToString();
                intPre = page - 1;
            }
            builder.Append("<nav class=\"text-center\">");
            builder.Append("<ul id=\"ulPaging_" + coinID + "\" class=\"pagination\">");

            int pagerank = 5;
            int next = 10;
            if (TempData["TotalRecord"] != null)
            {
                int totalRecore = (int)TempData["TotalRecord"];
                int totalPage = totalRecore / pageSize;
                int balance = totalRecore % pageSize;
                if (balance != 0)
                {
                    totalPage += 1;
                }
                if (page >= (totalPage - 1))
                {
                    nextLink = "?p=" + totalPage.ToString();
                    intNext = totalPage;
                }
                else
                {
                    nextLink = "?p=" + (page + 1).ToString();
                    intNext = page + 1;
                }
                int currentPage = page;
                var m = 0;
                if (totalPage > 10)
                {
                    if (page > pagerank + 1)
                    {
                        next = (page + pagerank) - 1;
                        m = page - pagerank;
                    }
                    if (page <= pagerank)
                    {
                        m = 0;
                    }
                    if (next > totalPage)
                    {
                        next = totalPage;
                    }
                }
                else
                {
                    next = totalPage;
                   // prev = 1;
                }
                lastLink = "?p=" + totalPage;
                if (totalPage > 1)
                {
                    if (currentPage > 0)
                    {
                        builder.Append("<li class=\"page-item\">");
                        builder.Append("<a style=\"cursor:pointer\" onclick=\"PagingSearchCoin('" + intPre + "','" + coinID + "')\" href=\"javascript:void(0)\" class=\"page-link\" aria-label=\"Previous\" data-index=\""+ intPre + "\">");
                        builder.Append("<span aria-hidden=\"true\">");
                        builder.Append("<i class=\"fa fa-angle-double-left\" area-hidden=\"hidden\"></i>");
                        builder.Append("</span>");
                        builder.Append("</a>");
                        builder.Append("</li>");
                    }
                }

                if (totalPage > 1)
                {
                    for (; m < next; m++)
                    {
                        linkPage = "?p=" + m;
                        if (m == currentPage)
                        {
                            builder.Append("<li class=\"page-item active\"><a class=\"page-link\">" + (m + 1) + "</a></li>");
                        }
                        else
                        {
                            builder.Append("<li class=\"page-item\"><a style=\"cursor:pointer\" onclick=\"PagingSearchCoin('" + m + "','" + coinID + "')\" href=\"javascript:void(0)\" class=\"page-link\">" + (m+1) + "</a></li>");
                        }
                    }
                }

                if (totalPage > 1)
                {
                    if (currentPage < totalPage)
                    {
                        builder.Append("<li class=\"page-item\">");
                        builder.Append("<a style=\"cursor:pointer\" onclick=\"PagingSearchCoin('" + intNext + "','" + coinID + "')\" href=\"javascript:void(0)\" class=\"page-link\" aria-label=\"Next\" data-index=\"" + intNext + "\">");
                        builder.Append("<span aria-hidden=\"true\">");
                        builder.Append("<i class=\"fa fa-angle-double-right\" area-hidden=\"hidden\"></i>");
                        builder.Append("</span>");
                        builder.Append("</a>");
                        builder.Append("</li>");
                    }
                }
            }
            builder.Append("</ul>");
            builder.Append("</nav>");
            return builder.ToString();
        }


        [System.Web.Http.HttpGet]
        public string SearchTransactionCoin(string fromDate, string toDate, int page, string coinID)
        {
            DateTime? fromD = DateTime.Now;
            DateTime? toD = DateTime.Now;
            StringBuilder strBOBj = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int intPageSize = 10;
            int start = 0, end = 5;
            int totalRecord = 0;
            string json = "";
            MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
            if (member != null)
            {
                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

                if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                    toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                }
                else
                {
                    if (string.IsNullOrEmpty(fromDate))
                    {
                        if (string.IsNullOrEmpty(toDate))
                        {
                            fromD = DateTime.Parse("01/01/1990");
                            toD = DateTime.Now;
                        }
                        else
                        {
                            fromD = DateTime.Parse("01/01/1990");
                            toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                        }
                    }
                    else
                    {
                        fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                        toD = DateTime.Now;
                    }
                }
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
                int stt = 1;
                if (page == 0)
                {
                    stt = page + stt;
                }
                else
                {
                    stt = (page * intPageSize) + 1;
                }
                TransactionFilterRequest transactionRequet = new TransactionFilterRequest();

                transactionRequet.ClientID = short.Parse(ClientId);
                transactionRequet.WalletId = short.Parse(WalletId);
                transactionRequet.SystemID = short.Parse(SystemId);
                transactionRequet.Limit = short.Parse(Limit);
                transactionRequet.UserId = Session[SessionKey.USERNAME].ToString();
                transactionRequet.TransactionType = TransactionType.None;
                transactionRequet.FromDate = fromD;
                transactionRequet.ToDate =toD;
                transactionRequet.FromIndex = short.Parse((page).ToString());
                if (coinID == Bitcoin)
                {
                    transactionRequet.CurrencyCode = CurrencyCode.BTC;
                }
                else if (coinID == Carcoin)
                {
                    transactionRequet.CurrencyCode = CurrencyCode.Carcoin;
                }
                CWalletList<TransactionInfo> transactionInfo = transaction.List(transactionRequet);

                if (transactionInfo != null && transactionInfo.Data != null&& transactionInfo.Data.Count > 0)
                {
                    totalRecord = int.Parse(transactionInfo.TotalRecord.ToString());
                    int i = 1;

                    foreach (TransactionInfo item in transactionInfo.Data)
                    {
                        string styleColor = "#fff";
                        if (i % 2 == 0)
                        {
                            styleColor = "#eee";
                        }
                        string strTransactionID = item.TransactionId.ToString();
                        string strValueTransaction = item.Amount.ToString("G29");
                        string Link = item.Tx;
                        string dateTime = item.CreatedDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime);
                        strBOBj.Append("<tr class='info pointer'>");
                        strBOBj.Append("<td class='text-center'>" + stt + "</td>");

                        strBOBj.Append("<td class='text-center'>" + dateTime + " UTC </td>");
                        strBOBj.Append("<td class='text-center' onclick=\"ShowTransactionWalletDetail('" + strTransactionID + "','" + item.FromAddress + "','" + strValueTransaction + "','" + Link + "','" + item.CreatedDate.ToUniversalTime() + "','" + coinID + "')\" data-toggle=\"modal\" data-target=\"#myModal_" + coinID + "\">" + strTransactionID + "</td>");
                        strBOBj.Append("<td class='text-center'>" + strValueTransaction + "</td>");
                        strBOBj.Append("<td class='text-center'><a href=\"" + Linktestnet + "" + item.Tx + "\" target =\"_blank\">" + item.Tx + "</a></td>");

                        strBOBj.Append("<td class='text-center'>" + item.TransactionType + "</td>");
                        strBOBj.Append("<td class='text-center'>" + item.Status + "</td>");
                        strBOBj.Append("</tr>");
                        stt++;
                    }
                    int totalPage = totalRecord / intPageSize;
                    int balance = totalRecord % intPageSize;
                    if (balance != 0)
                    {
                        totalPage += 1;
                    }
                    if (totalPage > 1)
                    {
                        for (int m = 0; m < totalPage; m++)
                        {
                            if (m == page)
                            {
                                builderPaging.Append("<li class=\"page-item active\"><a class=\"page-link\">" + (m+1) + "</a></li>");
                            }
                            else
                            {
                                builderPaging.Append("<li class=\"page-item\"><a onclick=\"PagingSearchCoin('" + m + "','" + coinID + "')\" class=\"page-link\">" + (m+1) + "</a></li>");
                            }
                        }
                    }
                }
                else
                {
                    strBOBj.Append("<tr><td colspan=\"7\">No Result</td></tr>");
                }
                SearchObj obj = new SearchObj();
                obj.ContentResult = strBOBj.ToString();
                obj.PagingResult = builderPaging.ToString();
                obj.Totalrecord = totalRecord;
                json = JsonConvert.SerializeObject(obj);
            }
            return json;
        }
    }
}
