﻿using BBH.Lotte.CLP.Shared;
using UtilityShared = BBH.Lotte.CLP.Shared.Utility;
using BBH.Lotte.CLP.Web.Models;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using BBH.LotteFE.CLP.Repository;
using Utility = BBH.LotteFE.CLP.Repository.Utility;
using Microsoft.Practices.Unity;
using NBitcoin;
using QBitNinja.Client;
using QBitNinja.Client.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using BBC.Core.Common.Cache;
using BBC.Core.Database;
using BBC.Core.Common.Utils;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.Wallet.Repository;
using BBC.Core.Common.Log;
using RestSharp;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class GameController : Controller
    {
        protected RestClient _client = new RestClient(ConfigurationManager.AppSettings["Blockchain_Info:apiUrlToBTC"]);

        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];
        string AdminID = ConfigurationManager.AppSettings[KeyManager.AdminID];
        string FreeWalletID = ConfigurationManager.AppSettings[KeyManager.FreeWalletID];
        string FreeWalletAddress = ConfigurationManager.AppSettings[KeyManager.FreeWalletAddress];
        string JackpotID = ConfigurationManager.AppSettings[KeyManager.JackPotID];

        string DayOpen = ConfigurationManager.AppSettings[KeyManager.DayOpen];
        string _TIME_CLOSED_COUNT_DOWN = ConfigurationManager.AppSettings[KeyManager._TIME_CLOSED_COUNT_DOWN];

        #region Properties Fields Key
        string masterKeyWan2Lot = ConfigurationManager.AppSettings["KeyWan2Lot"];
        string TimeExpired = ConfigurationManager.AppSettings["TimeExpired"];
        string linkExchange = ConfigurationManager.AppSettings["linkExchange"];

        string strPathLog = Convert.ToString(ConfigurationManager.AppSettings[KeyManager.PATH_LOG]);
        int intTimeClosed = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_CLOSED_COUNT_DOWN]);
        int intTimeOpened = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_OPENED_COUNT_DOWN]);

        RedisCache objRedisCache = new RedisCache();
        #endregion

        [Dependency]
        protected IPointsServices objPointRepository { get; set; }
        [Dependency]
        protected IBookingMegaballServices services { get; set; }
        [Dependency]
        protected IAwardMegaballServices awardServices { get; set; }
        [Dependency]
        protected IMemberServices memberServices { get; set; }
        [Dependency]
        protected ITicketConfigServices ticketConfigServices { get; set; }
        [Dependency]
        protected ICommonServices commonServices { get; set; }
        [Dependency]
        protected IDrawServices drawServices { get; set; }
        [Dependency]
        protected IFreeTicketServices freeTicketServices { get; set; }
        public ActionResult Index(string strLinkReference = null)
        {
            ViewBag.LinkReference = "-1";
            try
            {
                if (Request.Cookies["LotteryCookiesLogin"] != null)
                {
                    string Email = Request.Cookies["LotteryCookiesLogin"].Values["Email"];
                    string Password = Request.Cookies["LotteryCookiesLogin"].Values["Password"];
                    if (!string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(Password))
                    {
                        LoginByCookies(Email, Password);
                    }
                }
                if (!string.IsNullOrEmpty(strLinkReference))
                {
                    strLinkReference = strLinkReference.Trim(); /*strLinkRefernce.Replace(" ", "+").Replace("@", "/");*/
                    ViewBag.LinkReference = strLinkReference;
                }
                ViewBag.GetOpenDate = GetOpenDate().ToString("MM/dd/yyyy HH:mm:ss UTC");
                return View();
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("GameController -> Index: " + objEx.ToJsonString());
                Response.Redirect("/error");
            }
            return View();
        }
        private void SetSessionPoint(decimal dblPoint, int intNumberTicket)
        {
            System.Web.HttpContext.Current.Session[SessionKey.BTCPOINT] = dblPoint;
            System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = (int)dblPoint;
            System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKETFREE] = intNumberTicket;
        }

        private void SetViewDataCountDown(int intHours, int intMinutes, int intSeconds)
        {
            ViewData[KeyManager._HOUR_COUNT_DOWN] = intHours;
            ViewData[KeyManager._MINUTE_COUNT_DOWN] = intMinutes;
            ViewData[KeyManager._SECOND_COUNT_DOWN] = intSeconds;
        }

        public PartialViewResult Menu()
        {
            return PartialView();
        }

        public PartialViewResult MenuNew()
        {
            string intRawNumber = "0";
            try
            {
                IEnumerable<TicketConfigBO> lstTicketConfigBO = ticketConfigServices.GetListTicketConfig();
                string coinValues = "0";
                if (lstTicketConfigBO != null && lstTicketConfigBO.Count() > 0)
                {
                    foreach (var item in lstTicketConfigBO)
                    {
                        if (item.CoinID.Trim() == "BTC")
                        {
                            coinValues = (item.CoinValues / item.NumberTicket).ToString();
                        }
                    }
                }
                ViewBag.CoinValues = coinValues;
                //get address btc
                try
                {
                    FreeTicketBO objFreeTicket = freeTicketServices.GetFreeTicketActive();
                    if (objFreeTicket != null && objFreeTicket.Total > 0)
                    {
                        ViewBag.AmountTicketFree = objFreeTicket.Total;
                    }
                }
                catch (Exception objEx)
                {
                    BBC.CWallet.Share.BBCLoggerManager.Error("GameController -> MenuNew ->  ViewBag.AmountTicketFree : " + objEx.ToJsonString());
                }
                decimal numberCoinBTCAvailable = 0;
                ViewBag.IntNumberTicketFree = 0;
                ViewBag.NumberCoinBTC = 0;
                MemberBO obj = memberServices.GetMemberDetailByEmail(RDUser.Email);
                if (obj != null && obj.NumberTicketFree >= 0)
                {
                    ViewBag.IntNumberTicketFree = obj.NumberTicketFree;
                    System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKETFREE] = obj.NumberTicketFree;

                    AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                    objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                    objAddressInfoRequest.UserID = obj.Email;
                    objAddressInfoRequest.ClientID = short.Parse(ClientID);
                    objAddressInfoRequest.SystemID = short.Parse(SystemID);
                    objAddressInfoRequest.IsCreateAddr = true;
                    objAddressInfoRequest.CreatedBy = obj.Email;
                    objAddressInfoRequest.WalletId = short.Parse(WalletID);

                    AddressRepository objAddressRepository = new AddressRepository();
                    AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                    if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                    {
                        ViewBag.NumberCoinBTC = objAddressInfo.Available.ToString("G29");
                        numberCoinBTCAvailable = objAddressInfo.Available;
                    }
                    SetSessionPoint(numberCoinBTCAvailable, obj.NumberTicketFree);
                }


                DrawBO objDraw = drawServices.GetNewestDrawID();
                if (objDraw != null && objDraw.DrawID > 0)
                {
                    intRawNumber = objDraw.DrawID.ToString();
                }
                System.Web.HttpContext.Current.Session[SessionKey.RawNumber] = intRawNumber;
            }
            catch(Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("GameController -> MenuNew ->   System.Web.HttpContext.Current.Session[SessionKey.RawNumber]: " + objEx.ToJsonString());
            }
            ViewBag.RawNumber = intRawNumber;
            ViewBag.JackPot = 0;
            try
            {
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = JackpotID;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = false;
                objAddressInfoRequest.CreatedBy = JackpotID;
                objAddressInfoRequest.WalletId = short.Parse(WalletID);

                AddressRepository objAddressRepository = new AddressRepository();
                AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                {
                    try
                    {
                        ViewBag.JackPot = (Math.Truncate(objAddressInfo.Available * 100) / 100).ToString("N2");
                    }
                    catch
                    {
                        ViewBag.JackPot = objAddressInfo.Available.ToString("G29");
                    }
                }
                if (CheckTimeBooking())
                {
                    ViewData[KeyManager._DATEANDTIME] = DateTime.Now.ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss UTC");
                    ViewData[KeyManager._ENDDATE] = GetOpenDate().ToString("yyyy/MM/dd HH:mm:ss UTC");
                }
                else
                {
                    ViewData[KeyManager._DATEANDTIME] = DateTime.Now.ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss UTC");
                    ViewData[KeyManager._ENDDATE] = GetOpenDate().ToString("yyyy/MM/dd HH:mm:ss UTC");
                }
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("GameController -> MenuNew ->   ViewData[KeyManager._ENDDATE] = GetOpenDate(): " + objEx.ToJsonString());
            }
            return PartialView();
        }
        enum Days { Saturday = 7, Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6 };
        private DateTime GetOpenDate()
        {
            DateTime dtNow = DateTime.Now.ToUniversalTime();
            DrawBO objDrawID = drawServices.GetNewestDrawID();
            if(objDrawID != null)
            {
                dtNow = objDrawID.EndDate;
            }
            return dtNow;
        }
        public PartialViewResult Footer_Game()
        {
            return PartialView();
        }

        [HttpGet]
        public string GetListMemberBuyTicket()
        {
            StringBuilder builder = new StringBuilder();
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            int d = DateTime.Now.Day;
            int m = DateTime.Now.Month;
            int y = DateTime.Now.Year;
            string fromD = m + "/" + d + "/" + y + " 00:00:00";
            string toD = m + "/" + d + "/" + y + " 23:59:59";

            try
            {
                fromDate = DateTime.Parse(fromD);
                toDate = DateTime.Parse(toD);
            }
            catch(Exception objEx)
            {
                fromD = d + "/" + m + "/" + y + " 00:00:00";
                toD = d + "/" + m + "/" + y + " 23:59:59";
                fromDate = DateTime.Parse(fromD);
                toDate = DateTime.Parse(toD);

                BBC.CWallet.Share.BBCLoggerManager.Error("GameController -> GetListMemberBuyTicket ->  toDate = DateTime.Parse(toD): " + objEx.ToJsonString());
            }
            IEnumerable<BookingMegaball> lstBooking = null;
            lstBooking = (IEnumerable<BookingMegaball>)objRedisCache.GetCache<IEnumerable<BookingMegaball>>(Common.KeyListBooking);
            if (lstBooking == null)
            {
                lstBooking = services.ListTransactionBookingByDate(fromDate, toDate);
            }

            if (lstBooking != null && lstBooking.Count() > 0)
            {
                int i = 0;
                foreach (BookingMegaball book in lstBooking)
                {
                    builder.Append("<tr style='height:30px;'>");
                    builder.AppendFormat("<td style=\"font-size: 13px; font-weight: bold;\" class='cursorContext' title='{0}'>{0}</td>", book.TransactionCode);
                    builder.Append("<td style=\"font-size: 13px; font-weight:bold;padding-bottom: 10px;\"><span style=\"border-radius: 100%;background-color: #fff; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: red; margin-right:2px\">" + book.FirstNumber + "</span>&nbsp;<span style=\"border-radius: 100%;background-color: #fff; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: red; margin-right:2px\">" + book.SecondNumber + "</span>&nbsp;<span style=\"border-radius: 100%;background-color: #fff; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: red; margin-right:2px\">" + book.ThirdNumber + "</span>&nbsp;<span style=\"border-radius: 100%;background-color: #fff; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: red; margin-right:2px\">" + book.FourthNumber + "</span>&nbsp;<span style=\"border-radius: 100%;background-color: #fff; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: red; margin-right:2px\">" + book.FivethNumber + "</span>&nbsp;<span style=\"border-radius: 100%;background-color: #df9942; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: #fff; margin-right:2px\">" + book.ExtraNumber + "</span>&nbsp;</td>");
                    builder.Append("</tr>");
                    if (i < lstBooking.Count() - 1)
                    {
                        builder.Append("<tr class='space'>");
                        builder.Append("<td></td>");
                        builder.Append("<td></td>");
                        builder.Append("</tr>");
                    }
                    i++;
                }
                objRedisCache.SetCache<IEnumerable<BookingMegaball>>(Common.KeyListBooking, lstBooking);
            }

            return builder.ToString();
        }

        [HttpPost]
        public void SelectLanguageUser(string language)
        {
            Session["Language"] = language;
        }
        [HttpPost]
        public JsonResult GetListAwardOrderByDate1()
        {
            string strhtml = "-1";
            int intPageSize = 10;
            Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

            IEnumerable<AwardMegaball> lstAwardMegaball = null;
            lstAwardMegaball = (IEnumerable<AwardMegaball>)objRedisCache.GetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward);

            if (lstAwardMegaball == null)
            {
                lstAwardMegaball = awardServices.GetListAwardOrderByDate();
                if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                {
                    objRedisCache.SetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward, lstAwardMegaball);
                }
            }

            StringBuilder strBOBj = new StringBuilder();
            if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
            {
                int i = 0;
                foreach (var item in lstAwardMegaball)
                {
                    if (i == 0)
                    {
                        strBOBj.Append("<div class=\"num_1\"><h3 class=\"h3\">" + ConvetTwoNumberToString(item.FirstNumber) + "</h3></div>");
                        strBOBj.Append("<div class=\"num_1\"><h3 class=\"h3\">" + ConvetTwoNumberToString(item.SecondNumber) + "</h3></div>");

                        strBOBj.Append("<div class=\"num_2\"><h3 class=\"h3\">" + ConvetTwoNumberToString(item.ThirdNumber) + "</h3></div>");
                        strBOBj.Append("<div class=\"num_3\"><h3 class=\"h3\">" + ConvetTwoNumberToString(item.FourthNumber) + "</h3></div>");
                        strBOBj.Append("<div class=\"num_4\"><h3 class=\"h3\">" + ConvetTwoNumberToString(item.FivethNumber) + "</h3></div>");
                        strBOBj.Append("<div class=\"num_6\" style=\"background-color: #f6b559\"><h3 class=\"h3\">" + ConvetTwoNumberToString(item.ExtraNumber) + "</h3></div");

                        i++;
                    }

                }
                strhtml = strBOBj.ToString();

                objRedisCache.SetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward, lstAwardMegaball);
            }
            List<string> lst = new List<string>();
            lst.Add(strhtml);
            return Json(new { content = strhtml });
        }
        private string ConvetTwoNumberToString(int number)
        {
            if (number < 10)
                return "0" + number;
            else
                return number.ToString();
        }

        [HttpPost]
        public string BookingMegaball(string JsonArrListItem = "")
        {
            string[][] dict = new JavaScriptSerializer().Deserialize<string[][]>(JsonArrListItem);
            string result = "bookingFaile";
            MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
            if (member != null)
            {
                IPConfigBO objConfig = new IPConfigBO();
                objConfig.IPAddress = Common.GetLocalIPAddress();
                objConfig.ServiceDomain = Common.GetHostServices();
                objConfig.UserName = (string)Session[SessionKey.EMAIL];

                string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
                ModelRSA objRsa = new ModelRSA(strRsaParameter1);
                ObjResultMessage objResult = commonServices.CheckAuthenticate(objRsa);
                if (!objResult.IsError)
                {
                    objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                    if (objRsa.Token != null && objRsa.Token != string.Empty)
                    {
                        List<BookingMegaball> lstBookingMegaball = new List<BookingMegaball>();

                        //objBookingMegaball.ExtensionData = DateTime.Now;
                        //objBookingMegaball.TotalRecord = "";
                        if (dict != null && dict.Length > 0)
                        {
                            for (int i = 0; i < dict.Length; i++)
                            {
                                string strCode = Utility.GenCode();
                                string tick = DateTime.Now.Ticks.ToString();
                                string transactionCode = Utility.MaHoaMD5(strCode + tick + i * 2017);
                                BookingMegaball objBookingMegaball = new BookingMegaball();
                                objBookingMegaball.MemberID = member.MemberID;
                                objBookingMegaball.Note = "";
                                objBookingMegaball.Quantity = int.Parse("1");
                                objBookingMegaball.CreateDate = DateTime.Now;
                                objBookingMegaball.OpenDate = DateTime.Now;
                                objBookingMegaball.Status = 1;
                                objBookingMegaball.TransactionCode = transactionCode;
                                objBookingMegaball.NoteEnglish = "";
                                objBookingMegaball.Bitcoin_Wallet = member.E_Wallet;
                                objBookingMegaball.Email = member.Email;
                                objBookingMegaball.FirstNumber = int.Parse(dict[i][0]);
                                objBookingMegaball.SecondNumber = int.Parse(dict[i][1]);
                                objBookingMegaball.ThirdNumber = int.Parse(dict[i][2]);
                                objBookingMegaball.FourthNumber = int.Parse(dict[i][3]);
                                objBookingMegaball.FivethNumber = int.Parse(dict[i][4]);
                                objBookingMegaball.ExtraNumber = int.Parse(dict[i][5]);
                                lstBookingMegaball.Add(objBookingMegaball);
                            }
                        }
                        //Get ExchangeTickket
                        float PointValue = 1;
                        float TicketNumber = 1;
                        double totalPoint = 0;
                        double totalTicket = 0;
                        double Point = 0;
                        if (lstBookingMegaball != null && lstBookingMegaball.Count > 0)
                        {
                            foreach (var item in lstBookingMegaball)
                            {
                                totalTicket += item.Quantity;
                            }
                            totalPoint = (totalTicket * PointValue) / TicketNumber;
                        }
                        if (member.Points >= totalPoint)
                        {
                            Point = member.Points - totalPoint;
                            string strParameterObj = Algorithm.EncryptionObjectRSA(lstBookingMegaball);
                            ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                            objRsa2.Token = objRsa.Token;
                            objRsa2.UserName = objRsa.UserName;
                            objResult = services.InsertListBooking(objRsa2, Point);
                            if (!objResult.IsError)
                            {
                                if (Session["point"] != null)
                                {
                                    Session["point"] = Point.ToString();
                                }
                                result = "bookingSuccess";
                            }
                        }
                        else
                        {
                            result = "You do not get enough points to buy " + totalTicket + " ticket";
                        }
                    }
                }
            }
            else
            {
                result = "Login to buy ticket";
            }
            return result;
        }

        private static List<TransactionReceivedCoins> GetListTransaction(string strAddress)
        {

            List<TransactionReceivedCoins> list = new List<TransactionReceivedCoins>();
            try
            {
                QBitNinjaClient client = new QBitNinjaClient(Network.TestNet);
                BalanceModel myBalance = client.GetBalance(new BitcoinPubKeyAddress(strAddress), unspentOnly: true).Result;
                foreach (BalanceOperation op in myBalance.Operations)
                {
                    List<Coin> lstCoin = new List<Coin>();
                    var receivedCoins = op.ReceivedCoins;
                    foreach (Coin e in receivedCoins)
                    {
                        lstCoin.Add(e);
                    }
                    TransactionReceivedCoins objTransactionReceivedCoins = new TransactionReceivedCoins();
                    objTransactionReceivedCoins.ListCoins = lstCoin;
                    objTransactionReceivedCoins.TransactionID = op.TransactionId;
                    objTransactionReceivedCoins.Confirm = op.Confirmations;
                    list.Add(objTransactionReceivedCoins);
                }
            }
            catch (Exception objEx)
            {
                list = null;
                BBC.CWallet.Share.BBCLoggerManager.Error("GameController -> GetListTransaction: " + objEx.ToJsonString());
            }
            return list;

        }

        #region Get Exchange
        [HttpPost]
        public async Task<JsonResult> GetExchangeAsync(string strBTC = "0")
        {
            double dbExchangeUSD = 0;
            try
            {
                if (!string.IsNullOrEmpty(strBTC))
                {
                    //Tổng BTC của giải thưởng
                    double dbBTC = Double.Parse(strBTC);

                    List<Exchange> lstExchange = await UtilityShared.GetExchangeAsync(linkExchange, strPathLog);
                    if (lstExchange != null && lstExchange.Count > 0)
                    {
                        List<Exchange> lstBTC = lstExchange.Where(c => c.CoinName == "BTC").ToList();
                        if (lstBTC != null && lstBTC.Count > 0)
                        {
                            double dbSumValueExchange = 0;
                            for (int i = 0; i < lstBTC.Count; i++)
                            {
                                dbSumValueExchange += lstBTC[i].Values;
                            }

                            dbExchangeUSD = dbBTC * dbSumValueExchange;
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("GameController -> GetExchangeAsync: " + objEx.ToJsonString());
                UtilityShared.WriteLog(strPathLog, objEx.Message);
            }


            return Json(new { strUSD = Math.Round(dbExchangeUSD, 5) }, JsonRequestBehavior.AllowGet);
        }

        #endregion


        [HttpPost]
        public JsonResult LoadPoinBalance()
        {
            try
            {
                string strSecretAddress = string.Empty;
                MemberWalletBO objWallet = objPointRepository.LoadInfoPointWallet(RDUser.MemberID);
                if (objWallet != null)
                {
                    System.Web.HttpContext.Current.Session[SessionKey.BTCPOINT] = RDUser.BTCPoint;
                    System.Web.HttpContext.Current.Session[SessionKey.BTCADDRESS] = RDUser.BTCAddress;
                    System.Web.HttpContext.Current.Session[SessionKey.CARCOINADDRESS] = RDUser.CARCOINADDRESS;
                    strSecretAddress = objWallet.SerectKeyRipple;
                    System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = (int)objWallet.Points;
                }
                return Json(new { isError = 0, dbPoint = RDUser.BTCPoint, numberTicket = RDUser.NumberTickets, strWalletAddress = RDUser.E_Wallet, strRippleAddress = RDUser.BTCAddress, strSecretAddress = strSecretAddress }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("GameController -> LoadPoinBalance: " + objEx.ToJsonString());
                UtilityShared.WriteLog(strPathLog, objEx.Message);
                return Json(new { isError = 1, dbPoint = 0, numberTicket = 0, strWalletAddress = "", strRippleAddress = "", strSecretAddress = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult IndexV2()
        {
            int intHours = DateTime.Now.Hour;
            int intMinutes = 0;
            int intSecond = 0;

            if (intHours >= intTimeClosed && intHours < intTimeOpened)
            {
                ViewData[KeyManager._CHECK_LOCK_BOOKING] = 1;
            }
            else
            {
                ViewData[KeyManager._CHECK_LOCK_BOOKING] = 0;
            }
            intHours = 0;
            UtilityShared.GetTimeCountDown(intTimeClosed, ref intHours, ref intMinutes, ref intSecond);
            SetViewDataCountDown(intHours, intMinutes, intSecond);
            return View();
        }

        private void LoginByCookies(string strEmail, string strPassword)
        {
            MemberBO member = memberServices.LoginMember(strEmail, strPassword);
            if (member != null)
            {

                string strBTCAddress = string.Empty;
                string strCarcoinAddress = string.Empty;
                decimal numberCoinBTCAvailable = 0;
                decimal numberCarcoinAvailable = 0;
                //get address btc
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = member.Email;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = true;
                objAddressInfoRequest.CreatedBy = member.Email;
                objAddressInfoRequest.WalletId = short.Parse(WalletID);

                AddressRepository objAddressRepository = new AddressRepository();
                AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                {
                    strBTCAddress = objAddressInfo.AddressId;
                    numberCoinBTCAvailable = objAddressInfo.Available;
                }
                int isSetSession = -1;
                if (!string.IsNullOrEmpty(strBTCAddress) || !string.IsNullOrEmpty(strCarcoinAddress))
                {
                    SetSessionLogin(member, strBTCAddress, strCarcoinAddress, numberCoinBTCAvailable, numberCarcoinAvailable);
                }

            }
        }
        private int SetSessionLogin(MemberBO objMember, string strBTCAddress, string strCarcoinAddress, decimal douCoinBTCAvailable, decimal douCarcoinAvailable)
        {
            try
            {
                if (objMember != null)
                {
                    if (Session[SessionKey.USERNAME] == null)
                    {

                        Session[SessionKey.USERNAME] = objMember.Email;
                    }
                    System.Web.HttpContext.Current.Session[SessionKey.MEMBERID] = objMember.MemberID;
                    System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] = objMember;
                    System.Web.HttpContext.Current.Session[SessionKey.BTCPOINT] = douCoinBTCAvailable;
                    System.Web.HttpContext.Current.Session[SessionKey.CARCOINPOINT] = douCarcoinAvailable;
                    System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = douCarcoinAvailable;
                    System.Web.HttpContext.Current.Session[SessionKey.EMAIL] = objMember.Email;
                    System.Web.HttpContext.Current.Session[SessionKey.BTCADDRESS] = strBTCAddress;
                    System.Web.HttpContext.Current.Session[SessionKey.CARCOINADDRESS] = strCarcoinAddress;
                    System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKETFREE] = objMember.NumberTicketFree;
                    System.Web.HttpContext.Current.Session[SessionKey.CARCOINADDRESS] = strCarcoinAddress;
                    string checkIsEmptyPass = "true";
                    if (string.IsNullOrEmpty(objMember.Password))
                    {
                        checkIsEmptyPass = "false";
                    }
                    System.Web.HttpContext.Current.Session[SessionKey.ISPASSWORD] = checkIsEmptyPass;
                    System.Web.HttpContext.Current.Session[SessionKey.LinkReference] = objMember.Email;
                    return 0;
                }
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message);
                BBC.CWallet.Share.BBCLoggerManager.Debug("GameController -> SetSessionLogin: " + objEx.ToJsonString());
            }
            return 1;//isResult = 1: Error login fail
        }
        private bool CheckTimeBooking()
        {
            Type weekdays = typeof(Days);
            string dayofWeek = DateTime.Now.ToUniversalTime().DayOfWeek.ToString();
            int intDays = (int)Enum.Parse(weekdays, dayofWeek); ;
            string strDayCheck = DayOpen;//key config
            string[] arrDayCheck = strDayCheck.Split(',');
            int[] arrintDayCheck;
            if (arrDayCheck.Length > 0)
            {
                arrintDayCheck = new int[arrDayCheck.Length];
                for (int j = 0; j < arrDayCheck.Length; j++)
                {
                    arrintDayCheck[j] = (int)Enum.Parse(weekdays, arrDayCheck[j].ToString());
                }
                Array.Sort(arrintDayCheck);

                if (arrintDayCheck.Length > 0)
                {
                    for (int k = 0; k < arrintDayCheck.Length; k++)
                    {
                        if (intDays == arrintDayCheck[k])
                        {
                            int TimeClosedCountDown = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_CLOSED_COUNT_DOWN]);
                            int TimeOpenedCountDown = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_OPENED_COUNT_DOWN]);

                            DateTime dtBooking = DateTime.Now.ToUniversalTime();
                            DateTime dtOpenDate = DateTime.Now.ToUniversalTime().Date.Add(new TimeSpan(TimeOpenedCountDown, 0, 0));
                            DateTime dtClosedDate = DateTime.Now.ToUniversalTime().Date.Add(new TimeSpan(TimeClosedCountDown, 0, 0));

                            DateTime dtBookingTemp = new DateTime(dtBooking.Year, dtBooking.Month, dtBooking.Day, dtBooking.Hour, dtBooking.Minute, dtBooking.Second);

                            DateTime dtOpenDateTemp = new DateTime(dtOpenDate.Year, dtOpenDate.Month, dtOpenDate.Day, dtOpenDate.Hour, dtOpenDate.Minute, dtOpenDate.Second);
                            DateTime dtClosedDateTemp = new DateTime(dtClosedDate.Year, dtClosedDate.Month, dtClosedDate.Day, dtClosedDate.Hour, dtClosedDate.Minute, dtClosedDate.Second);

                            int resultOpen = DateTime.Compare(dtBookingTemp, dtOpenDateTemp);
                            int resultClosed = DateTime.Compare(dtBookingTemp, dtClosedDateTemp);
                            if (resultClosed >= 0 && resultOpen <= 0)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
        [HttpPost]
        public async Task<double> CurrencyToBtc()
        {
            try
            {
                var request = new RestRequest("tobtc?currency=USD&value=1", Method.GET);
                var response = await _client.ExecuteGetTaskAsync<double>(request);
                return double.Parse(response.Content);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
