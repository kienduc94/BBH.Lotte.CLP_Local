﻿using BBH.Lotte.CLP.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class CommonController : Controller
    {
        [System.Web.Mvc.HttpGet]
        public JsonResult RandomTicketNumber(int numberTicket)
        {
            StringBuilder builder = new StringBuilder();
            List<string> lstNumber = Utilitys.ListNumberRandom(numberTicket);
            if(lstNumber!=null&&lstNumber.Count()>0)
            {
                return Json(new { ListNumber = lstNumber }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { ListNumber = new List<string>() }, JsonRequestBehavior.AllowGet);
        }
    }
}
