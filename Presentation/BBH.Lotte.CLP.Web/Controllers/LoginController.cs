﻿using BBC.Core.Common.Log;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CLP.Wallet.Repository;
using BBH.Lotte.CLP.Web.Models;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class LoginController : Controller
    {
        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];
        string AdminID = ConfigurationManager.AppSettings[KeyManager.AdminID];
        string FreeWalletID = ConfigurationManager.AppSettings[KeyManager.FreeWalletID];
        string FreeWalletAddress = ConfigurationManager.AppSettings[KeyManager.FreeWalletAddress];
        string LoanWalletID = ConfigurationManager.AppSettings[KeyManager.LoanWalletID];
        string LoanWalletAddress = ConfigurationManager.AppSettings[KeyManager.LoanWalletAddress];

        string NumberTicketFree = ConfigurationManager.AppSettings[KeyManager.NumberTicketFree];
        //api carcoin
        string ApiMyCarCoin = ConfigurationManager.AppSettings[KeyManager.ApiMyCarCoin];
        string HashToken = ConfigurationManager.AppSettings[KeyManager.HashToken];
        //string HashSalt = ConfigurationManager.AppSettings[KeyManager.HashSalt];
        string EmailDefault = ConfigurationManager.AppSettings[KeyManager.EmailDefault];

        public string KeyCodeSHA = Convert.ToString(ConfigurationManager.AppSettings[KeyManager._KEYCODESHA]);

        [Dependency]
        protected IMemberServices services { get; set; }
        [Dependency]
        protected IPointsServices objPointRepository { get; set; }
        [Dependency]
        protected ITicketConfigServices ticketConfigServices { get; set; }
        [Dependency]
        protected IMemberServices memberServices { get; set; }

        public ActionResult Index(int intIsLoginType = 0)
        {
            try
            {
                if (!RDUser.IsLogedIn())
                {
                    //chưa login
                    return Redirect("/play-game");
                }
                string url = "";
                string redirectUrl = Request.QueryString["redirect"];
                if (redirectUrl == null || redirectUrl == "")
                {
                    url = "/";
                }
                else
                {
                    url = "/" + redirectUrl;
                }
                ViewData["UrlRedirect"] = url;
                ViewBag.intIsLoginType = intIsLoginType;
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message.Trim());
            }

            return View();
        }
        [HttpPost]
        public string MemberLogin(string email, string password)
        {
            string result = "";
            int TimeCookies = int.Parse(ConfigurationManager.AppSettings["TimeCookies"]);
            MemberBO member = services.LoginMember(email, Utilitys.MaHoaMD5(password));

            if (member != null)
            {
                string strBTCAddress = string.Empty;
                string strCarcoinAddress = string.Empty;
                decimal numberCoinBTCAvailable = 0;
                decimal numberCarcoinAvailable = 0;
                //get address btc
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = member.Email;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = true;
                objAddressInfoRequest.CreatedBy = member.Email;
                objAddressInfoRequest.WalletId = short.Parse(WalletID);

                AddressRepository objAddressRepository = new AddressRepository();
                AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                {
                    strBTCAddress = objAddressInfo.AddressId;
                    numberCoinBTCAvailable = objAddressInfo.Available;

                }
                int isSetSession = -1;
                if (!string.IsNullOrEmpty(strBTCAddress))
                {
                    isSetSession = SetSessionLogin(member, strBTCAddress, strCarcoinAddress, numberCoinBTCAvailable, numberCarcoinAvailable);
                }
                if (isSetSession == 0)
                {
                    result = "loginSuccess";
                }
                if (isSetSession == 1)
                {
                    result = "loginfaile";
                }
            }
            else
            {
                result = "loginfaile";
            }


            return result;
        }
        [HttpPost]
        public string LogoutMember(string str)
        {
            string result = "";
            Session[SessionKey.USERNAME] = null;
            System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] = null;
            if (Request.Cookies["Login"] != null)
            {
                Response.Cookies["Login"].Expires = DateTime.Now.AddDays(-1);
            }
            System.Web.HttpContext.Current.Session[SessionKey.BTCPOINT] = null;
            System.Web.HttpContext.Current.Session[SessionKey.CARCOINPOINT] = null;
            System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = null;
            System.Web.HttpContext.Current.Session[SessionKey.BTCADDRESS] = null;
            System.Web.HttpContext.Current.Session[SessionKey.CARCOINADDRESS] = null;
            System.Web.HttpContext.Current.Session[SessionKey.ISPASSWORD] = null;
            System.Web.HttpContext.Current.Session[SessionKey.LinkReference] = null;
            result = str;
            return result;
        }
        [HttpPost]
        public async Task<JsonResult> LoginSuccess(MemberBO objMember)
        {
            try
            {
                string strMemberObj = string.Empty;
                string strEmail = objMember.Email;

                string strCode = Utility.GenCode();
                string tick = DateTime.Now.Ticks.ToString();
                string keyCode = Utility.MaHoaMD5(strCode + tick);

                objMember.NumberTicketFree = 0;
                objMember.LinkReference = objMember.Email + "/" + Utility.MaHoaMD5(strCode + tick + objMember.Email);
                int intIsResult = services.InsertMemberAfterLoginFB(ref objMember);

                if (intIsResult == 0 || intIsResult == 3)
                {

                    if (intIsResult == 3)
                    {
                        AccountCreationRequest objAccountCreationRequest = new AccountCreationRequest();
                        objAccountCreationRequest.ClientID = short.Parse(ClientID);
                        objAccountCreationRequest.Email = objMember.Email;
                        objAccountCreationRequest.Name = objMember.Email;
                        objAccountCreationRequest.PhoneNumber = "";
                        objAccountCreationRequest.RequestedBy = RequestBy;
                        objAccountCreationRequest.SystemID = short.Parse(SystemID);
                        objAccountCreationRequest.UserID = objMember.Email;
                        objAccountCreationRequest.WalletID = long.Parse(WalletID);

                        AccountInfo objAccountInfo = new AccountInfo();
                        AccountRepository objAccountRepository = new AccountRepository();
                        objAccountInfo = objAccountRepository.Create(objAccountCreationRequest);

                        //Create add to BOS carcoin
                        try
                        {
                            var client = new RestClient(ApiMyCarCoin);
                            var request = new RestRequest("/create-user", Method.POST);
                            request.AddParameter("api_key", HashToken); 
                            request.AddParameter("referral_email", EmailDefault); 
                            request.AddParameter("email", objMember.Email);

                            IRestResponse<ContentResponse> response = client.Execute<ContentResponse>(request);
                            string result = response.Data.result;
                            if (result == "0")
                            {
                                WriteErrorLog.WriteLogsToFileText("Call api BOS :Error" + response.Data.error_msg);
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteErrorLog.WriteLogsToFileText("call api BOS :create_user" + ex.Message.Trim());
                        }
                    }
                    //login success
                    if (intIsResult == 0)
                    {
                        string strBTCAddress = string.Empty;
                        string strCarcoinAddress = string.Empty;
                        decimal numberCoinBTCAvailable = 0;
                        decimal numberCarcoinAvailable = 0;
                        //get address btc
                        AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                        objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                        objAddressInfoRequest.UserID = strEmail;
                        objAddressInfoRequest.ClientID = short.Parse(ClientID);
                        objAddressInfoRequest.SystemID = short.Parse(SystemID);
                        objAddressInfoRequest.IsCreateAddr = true;
                        objAddressInfoRequest.CreatedBy = strEmail;
                        objAddressInfoRequest.WalletId = short.Parse(WalletID);

                        AddressRepository objAddressRepository = new AddressRepository();
                        AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                        if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                        {
                            strBTCAddress = objAddressInfo.AddressId;
                            numberCoinBTCAvailable = objAddressInfo.Available;

                        }

                        if (!string.IsNullOrEmpty(strBTCAddress))
                        {
                            SetSessionLogin(objMember, strBTCAddress, strCarcoinAddress, numberCoinBTCAvailable, numberCarcoinAvailable);
                        }
                    }
                    else
                    {
                        strMemberObj = JsonConvert.SerializeObject(objMember);
                    }
                }
                return Json(new { isError = intIsResult, Email = objMember.Email, intIsUserType = (objMember != null ? objMember.IsUserType : 0) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message.Trim());
                return Json(new { isError = 1 }, JsonRequestBehavior.AllowGet);
            }
        }

        async Task<RippleAccount> GetRippleAddressAsync(string path)
        {

            string str = string.Empty;
            RippleAccount lst = new RippleAccount();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    using (var response = await client.GetAsync(path))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            str = await response.Content.ReadAsStringAsync();
                            lst = JsonConvert.DeserializeObject<RippleAccount>(str);
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message);
            }
            return lst;
        }

        [HttpPost]
        public string UpdatePassMember(string strPassword, string objMember)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(strPassword) && !string.IsNullOrEmpty(objMember))
            {
                MemberBO objMemberBO = JsonConvert.DeserializeObject<MemberBO>(objMember);
                string strEmail = objMemberBO.Email;
                result = strEmail;
                if (objMemberBO != null)
                {
                    string passmd5 = Utilitys.MaHoaMD5(strPassword);
                    bool rs = services.UpdatePasswordMember(objMemberBO.Email, passmd5);
                    if (rs)
                    {
                        objMemberBO.Password = passmd5;

                        string strBTCAddress = string.Empty;
                        string strCarcoinAddress = string.Empty;
                        decimal numberCoinBTCAvailable = 0;
                        decimal numberCarcoinAvailable = 0;
                        //get address btc
                        AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                        objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                        objAddressInfoRequest.UserID = strEmail;
                        objAddressInfoRequest.ClientID = short.Parse(ClientID);
                        objAddressInfoRequest.SystemID = short.Parse(SystemID);
                        objAddressInfoRequest.IsCreateAddr = true;
                        objAddressInfoRequest.CreatedBy = strEmail;
                        objAddressInfoRequest.WalletId = short.Parse(WalletID);

                        AddressRepository objAddressRepository = new AddressRepository();
                        AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);
                        if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                        {
                            strBTCAddress = objAddressInfo.AddressId;
                            numberCoinBTCAvailable = objAddressInfo.Available;

                        }


                        int isSetSession = -2;
                        if (!string.IsNullOrEmpty(strBTCAddress))
                        {
                            isSetSession = SetSessionLogin(objMemberBO, strBTCAddress, strCarcoinAddress, numberCoinBTCAvailable, numberCarcoinAvailable);
                        }
                        if (isSetSession == 0)
                        {
                            result = "UpdatePassSuccess";
                        }
                    }
                }
            }

            return result;
        }
        private int SetSessionLogin(MemberBO objMember, string strBTCAddress, string strCarcoinAddress, decimal douCoinBTCAvailable, decimal douCarcoinAvailable)
        {
            try
            {
                if (objMember != null)
                {
                    if (Session[SessionKey.USERNAME] == null)
                    {

                        Session[SessionKey.USERNAME] = objMember.Email;

                    }
                    System.Web.HttpContext.Current.Session[SessionKey.MEMBERID] = objMember.MemberID;
                    System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] = objMember;
                    System.Web.HttpContext.Current.Session[SessionKey.BTCPOINT] = douCoinBTCAvailable;
                    System.Web.HttpContext.Current.Session[SessionKey.CARCOINPOINT] = douCarcoinAvailable;
                    System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = douCarcoinAvailable;
                    System.Web.HttpContext.Current.Session[SessionKey.EMAIL] = objMember.Email;
                    System.Web.HttpContext.Current.Session[SessionKey.LinkReference] = objMember.Email;/*Utility.EncryptText(objMember.LinkReference, KeyCodeSHA);*/
                    System.Web.HttpContext.Current.Session[SessionKey.BTCADDRESS] = strBTCAddress;
                    System.Web.HttpContext.Current.Session[SessionKey.CARCOINADDRESS] = strCarcoinAddress;
                    string checkIsEmptyPass = "true";
                    if (string.IsNullOrEmpty(objMember.Password))
                    {
                        checkIsEmptyPass = "false";
                    }
                    System.Web.HttpContext.Current.Session[SessionKey.ISPASSWORD] = checkIsEmptyPass;
                    System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKETFREE] = objMember.NumberTicketFree;
                    return 0;
                }
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message);
            }
            return 1;//isResult = 1: Error login fail
        }

        public ActionResult UpdateIsActiveMember(string email, string key)
        {
            try
            {
                MemberBO objMemberBO = new MemberBO();
                objMemberBO = services.GetMemberDetailByEmail(email);
                if (objMemberBO != null)
                {
                    bool result = services.LockAndUnlockMember(objMemberBO.MemberID, 1);
                    if (result)
                    {
                        string strBTCAddress = string.Empty;
                        string strCarcoinAddress = string.Empty;
                        decimal numberCoinBTCAvailable = 0;
                        decimal numberCarcoinAvailable = 0;
                        //get address btc
                        AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                        objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                        objAddressInfoRequest.UserID = objMemberBO.Email;
                        objAddressInfoRequest.ClientID = short.Parse(ClientID);
                        objAddressInfoRequest.SystemID = short.Parse(SystemID);
                        objAddressInfoRequest.IsCreateAddr = true;
                        objAddressInfoRequest.CreatedBy = objMemberBO.Email;
                        objAddressInfoRequest.WalletId = short.Parse(WalletID);

                        AddressRepository objAddressRepository = new AddressRepository();
                        AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                        if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                        {
                            strBTCAddress = objAddressInfo.AddressId;
                            numberCoinBTCAvailable = objAddressInfo.Available;
                            //get coin for test 
                            Tranfer(LoanWalletID, LoanWalletAddress, objAddressInfo.UserId, 1);
                        }

                        ////Get price ticket
                        //IEnumerable<TicketConfigBO> lstTicketConfigBO = ticketConfigServices.GetListTicketConfig();
                        //double coinValues = 0;
                        //if (lstTicketConfigBO != null && lstTicketConfigBO.Count() > 0)
                        //{
                        //    foreach (var item in lstTicketConfigBO)
                        //    {
                        //        if (item.CoinID.Trim() == "BTC")
                        //        {
                        //            coinValues = item.CoinValues / item.NumberTicket;
                        //        }
                        //    }
                        //}
                        //double amountFreeTicket = 0;
                        //amountFreeTicket = int.Parse(NumberTicketFree) * coinValues;
                        ////Call block address wallet free for new member
                        //TransactionLockRequest objTransactionLockRequest = new TransactionLockRequest();
                        //objTransactionLockRequest.UserId = FreeWalletID;
                        //objTransactionLockRequest.ClientID = short.Parse(ClientID);
                        //objTransactionLockRequest.SystemID = short.Parse(SystemID);
                        //objTransactionLockRequest.FromAddress = FreeWalletAddress;
                        //objTransactionLockRequest.Amount = decimal.Parse(amountFreeTicket.ToString());
                        //objTransactionLockRequest.CurrencyCode = CurrencyCode.BTC;
                        //objTransactionLockRequest.Reason = "Lock for free ticket";
                        //objTransactionLockRequest.CreatedBy = RequestBy;

                        //TransactionInfo objTransactionInfo = new TransactionInfo();
                        //TransactionRepository objTransactionRepository = new TransactionRepository();

                        //objTransactionInfo = objTransactionRepository.Lock(objTransactionLockRequest);
                        //if (objTransactionInfo != null && !string.IsNullOrEmpty(objTransactionInfo.TransactionId.ToString()))
                        //{
                        //    try
                        //    {
                        //        services.UpdateFreeTicketMember(objMemberBO.MemberID, 1);
                        //        //insert member reference
                        //        MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                        //        objMemberReferenceBO.MemberID = objMemberBO.MemberID;
                        //        objMemberReferenceBO.MemberIDReference = objMemberBO.MemberID;
                        //        objMemberReferenceBO.LinkReference = objMemberBO.LinkReference;
                        //        objMemberReferenceBO.LockID = objTransactionInfo.TransactionId.ToString();
                        //        objMemberReferenceBO.Status = 0;
                        //        objMemberReferenceBO.Amount = decimal.Parse(amountFreeTicket.ToString());

                        //        services.InsertMemberReference(objMemberReferenceBO);
                        //    }
                        //    catch
                        //    {
                        //    }
                        //}
                        ////Call block address wallet free for  member reference
                        //List<MemberReferenceBO> lstMemberReferenceBO = services.GetDetailMemberReferenceByIDRef(objMemberBO.MemberID).ToList();
                        //lstMemberReferenceBO = lstMemberReferenceBO.Where(x => x.LockID == "").ToList();
                        //if (lstMemberReferenceBO != null && lstMemberReferenceBO.Count > 0)
                        //{
                        //    objTransactionLockRequest = new TransactionLockRequest();
                        //    objTransactionLockRequest.UserId = FreeWalletID;
                        //    objTransactionLockRequest.ClientID = short.Parse(ClientID);
                        //    objTransactionLockRequest.SystemID = short.Parse(SystemID);
                        //    objTransactionLockRequest.FromAddress = FreeWalletAddress;
                        //    objTransactionLockRequest.Amount = decimal.Parse(amountFreeTicket.ToString());
                        //    objTransactionLockRequest.CurrencyCode = CurrencyCode.BTC;
                        //    objTransactionLockRequest.Reason = "Lock for free ticket";
                        //    objTransactionLockRequest.CreatedBy = RequestBy;

                        //    objTransactionInfo = new TransactionInfo();
                        //    objTransactionRepository = new TransactionRepository();

                        //    objTransactionInfo = objTransactionRepository.Lock(objTransactionLockRequest);
                        //    if (objTransactionInfo != null && !string.IsNullOrEmpty(objTransactionInfo.TransactionId.ToString()))
                        //    {

                        //        int intmemberID = lstMemberReferenceBO[0].MemberID;
                        //        MemberBO objMemberBO_ = services.GetMemberDetailByID(intmemberID);
                        //        if (objMemberBO_ != null && objMemberBO_.MemberID != 0)
                        //        {
                        //            int number = objMemberBO_.NumberTicketFree + int.Parse(NumberTicketFree);
                        //            services.UpdateFreeTicketMember(intmemberID, number);
                        //        }

                        //        //update member reference
                        //        MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                        //        objMemberReferenceBO.MemberID = intmemberID;
                        //        objMemberReferenceBO.MemberIDReference = objMemberBO.MemberID;
                        //        objMemberReferenceBO.LockID = objTransactionInfo.TransactionId.ToString();
                        //        objMemberReferenceBO.Status = 0;

                        //        services.UpdateMemberReference(objMemberReferenceBO);
                        //    }
                        //}

                        if (!string.IsNullOrEmpty(strBTCAddress))
                        {
                            SetSessionLogin(objMemberBO, strBTCAddress, strCarcoinAddress, numberCoinBTCAvailable, numberCarcoinAvailable);
                        }

                        return Redirect("/play-game");//isResult = 1: Error login fail
                    }
                    else
                    {
                        return Redirect("/error");//isResult = 1: Error login fail
                    }
                }
            }
            catch (Exception ex)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText(ex.Message);
                return Redirect("/error");//isResult = 1: Error login fail
            }
            return View();
        }

        private bool Tranfer(string fromID, string fromAddress, string toID, decimal value)
        {
            try
            {
                TransferRequest objTransferRequest = new TransferRequest();
                objTransferRequest.UserId = fromID;
                objTransferRequest.FromAddress = fromAddress;
                objTransferRequest.ClientID = short.Parse(ClientID);
                objTransferRequest.SystemID = short.Parse(SystemID);
                objTransferRequest.Amount = value;
                objTransferRequest.Description = "Tranfer -> from: " + fromID + " to: " + toID;
                objTransferRequest.ToClientId = short.Parse(ClientID);
                objTransferRequest.ToSystemId = short.Parse(SystemID);
                objTransferRequest.ToUserId = toID;
                objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                objTransferRequest.CreatedBy = RequestBy;

                TransactionInfo objTransactionInfo = new TransactionInfo();
                TransactionRepository objTransactionRepository = new TransactionRepository();

                objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                if (objTransactionInfo != null)
                {
                    WriteErrorLog.WriteLogsToFileText("Success LoginController(Tranfer) transactionID: " + objTransactionInfo.TransactionId);
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Error LoginController(Tranfer) Message: " + ex.Message);
                return false;
            }
            return false;
        }
    }
}
