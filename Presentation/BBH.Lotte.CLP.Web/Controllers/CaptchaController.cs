﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class CaptchaController : Controller
    {
        // GET: Captcha
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SubmitCapcha()
        {
            var response = Request["g-recaptcha-response"];
            string secretKey = "6LfhJyUUAAAAAPKM6Hl87lD0mVKa-0zPKNR53W_j";
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");
            if (status == true)
            {
                ViewBag.Message = "Your Google reCaptcha validation success";
            }
            else
            {
                ViewBag.Message = "Your Google reCaptcha validation failed";
            }
            Session["CaptchaCode"] = response;

            return View("SubmitCapcha");
        }
    }
}