﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using BBH.Lotte.CLP.Shared;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class ErrorController : Controller
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ErrorLogs()
        {
            ViewBag.ErrorLogs = ReadErrorLogs();
            return View();
        }
        private String ReadErrorLogs()
        {
            String line = String.Empty;
            try
            {
                using (StreamReader sr = new StreamReader(fileLog))
                {
                    line = sr.ReadToEnd();
                }
            }
            catch (Exception objEx)
            {
                line = objEx.Message.ToString();
                BBC.CWallet.Share.BBCLoggerManager.Error("ErrorController -> ReadErrorLogs: " + objEx.ToJsonString());
            }
            return line;
        }
    }
}