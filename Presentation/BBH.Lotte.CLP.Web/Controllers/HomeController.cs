﻿using BBH.Lotte.CLP.Web.Models;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using BBH.LotteFE.CLP.Repository;
using Utility = BBH.LotteFE.CLP.Repository.Utility;
using Microsoft.Practices.Unity;
using NBitcoin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using QBitNinja.Client.Models;
using QBitNinja.Client;
using BBH.Lotte.CLP.Shared;
using UtilityShared = BBH.Lotte.CLP.Shared.Utility;
using BBC.Core.Common.Cache;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.Wallet.Repository;
using RestSharp;
using System.Threading.Tasks;
using System.IO;
using BBC.Core.Common.Log;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class HomeController : Controller
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));

        protected RestClient _client = new RestClient(ConfigurationManager.AppSettings["Blockchain_Info:apiUrlToBTC"]);
        string JackpotID = ConfigurationManager.AppSettings[KeyManager.JackPotID];
        enum Days { Saturday = 7, Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6 };
        string DayOpen = ConfigurationManager.AppSettings[KeyManager.DayOpen];

        string masterKeyWan2Lot = ConfigurationManager.AppSettings["KeyWan2Lot"];
        string TimeExpired = ConfigurationManager.AppSettings["TimeExpired"];
        int intTimeClosed = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_CLOSED_COUNT_DOWN]);
        int intTimeOpened = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_OPENED_COUNT_DOWN]);
        string strPathLog = Convert.ToString(ConfigurationManager.AppSettings[KeyManager.PATH_LOG]);

        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];
        RedisCache objRedisCache = new RedisCache();
        [Dependency]
        protected IAwardMegaballServices awardServices { get; set; }
        [Dependency]
        protected IMemberServices memberServices { get; set; }
        [Dependency]
        protected IBookingMegaballServices services { get; set; }
        [Dependency]
        protected IBookingMegaballServices bookingMegaballServices { get; set; }
        [Dependency]
        protected IPointsServices objPointRepository { get; set; }
        [Dependency]
        protected IDrawServices drawServices { get; set; }
        [Dependency]
        protected IAwardMegaballServices awardMegabalServices { get; set; }
        [Dependency]
        protected IFreeTicketServices freeTicketServices { get; set; }
        public ActionResult Index()
        {
            try
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("HomeController -> Index: 1");

                FreeTicketBO objFreeTicket = freeTicketServices.GetFreeTicketActive();
                BBC.CWallet.Share.BBCLoggerManager.Error("HomeController ->  Index -> GetFreeTicketActive: " + objFreeTicket.ToJsonString());
                if (objFreeTicket != null && objFreeTicket.FreeTicketID > 0)
                {
                    ViewBag.FreeTicketRemaning = GenHtmlFreeTicker(objFreeTicket);
                }

                BBC.CWallet.Share.BBCLoggerManager.Error("HomeController -> Index: ViewBag.FreeTicketRemaning: " + ViewBag.FreeTicketRemaning);

            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            return View();
        }

        public string GenHtmlFreeTicker(FreeTicketBO obj)
        {
            string strHtml = string.Empty;
            try
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("HomeController -> GenHtmlFreeTicker: " + obj.ToJsonString());

                if (obj != null)
                {
                    StringBuilder strBuilder = new StringBuilder();
                    strBuilder.Append("<div class=\"number-free\">");
                    strBuilder.Append("<div class=\"row row-xs\">");
                    for (int i = 0; i < obj.Total.ToString().Trim().Length; i++)
                    {
                        strBuilder.Append("<div class=\"col\">");
                        strBuilder.Append("<div class=\"ticket\">" + obj.Total.ToString().Substring(i, 1) + "</div>");
                        strBuilder.Append("</div>");
                    }
                    strBuilder.Append("</div>");
                    strBuilder.Append("<div class=\"text-ticket\">Free tickets remaining</div>");
                    strBuilder.Append("</div>");
                    strHtml = strBuilder.ToString();
                }

                BBC.CWallet.Share.BBCLoggerManager.Error("HomeController -> GenHtmlFreeTicker -> HTML: " + strHtml);
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            return strHtml;
        }

        public PartialViewResult Menu()
        {
            return PartialView();
        }

        public PartialViewResult Footer()
        {
            return PartialView();
        }

        public PartialViewResult About()
        {
            try
            {
                //Set ViewData DateTime Next Draw
                int intHours = 0;
                int intMinutes = 0;
                int intSecond = 0;
                UtilityShared.GetTimeCountDown(intTimeClosed, ref intHours, ref intMinutes, ref intSecond);
                SetViewDataCountDown(intHours, intMinutes, intSecond);
            }
            catch (Exception objEx)
            {
                UtilityShared.WriteLog(strPathLog, objEx.Message);
            }
            return PartialView();
        }

        public PartialViewResult Contact()
        {
            try
            {
                //Set ViewData DateTime Next Draw
                int intHours = 0;
                int intMinutes = 0;
                int intSecond = 0;
                UtilityShared.GetTimeCountDown(intTimeClosed, ref intHours, ref intMinutes, ref intSecond);
                SetViewDataCountDown(intHours, intMinutes, intSecond);
            }
            catch (Exception objEx)
            {
                UtilityShared.WriteLog(strPathLog, objEx.Message);
            }

            return PartialView();
        }

        public PartialViewResult WinningNumber()
        {
            try
            {
                string dayResult = string.Empty;
                string s1 = string.Empty, s2 = string.Empty, s3 = string.Empty, s4 = string.Empty, s5 = string.Empty, s6 = string.Empty;
                List<int> lstResult = new List<int>();

                DateTime fromDate = DateTime.Now;
                DateTime toDate = DateTime.Now;
                int d = DateTime.Now.Day;
                int m = DateTime.Now.Month;
                int y = DateTime.Now.Year;
                switch (m)
                {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        {
                            d = 31;
                            break;
                        }
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        {
                            d = 30;
                            break;
                        }
                    case 2:
                        {
                            if (y % 4 == 0 || y % 100 == 0 || y % 400 == 0)
                            {
                                d = 29;
                            }
                            else
                            {
                                d = 28;
                            }
                            break;
                        }
                }

                string fromD = m + "/01/" + y + " 00:00:00";
                string toD = m + "/" + d + "/" + y + " 23:59:00";

                try
                {
                    fromDate = DateTime.Parse(fromD);
                    toDate = DateTime.Parse(toD);
                }
                catch
                {
                    //fromD = d + "/" + m + "/" + y + " 00:00:00";
                    //toD = d + "/" + m + "/" + y + " 23:59:00";
                    //fromDate = DateTime.Parse(fromD);
                    //toDate = DateTime.Parse(toD);
                }
                IEnumerable<AwardMegaball> lstAward = awardServices.GetListAward(1, 200, fromDate, toDate);

                if (lstAward != null && lstAward.Count() > 0)
                {
                    if (lstAward.ElementAt(0).FirstNumber < 10)
                    {
                        s1 = "0" + lstAward.ElementAt(0).FirstNumber;
                    }
                    else
                    {
                        s1 = lstAward.ElementAt(0).FirstNumber.ToString();
                    }
                    if (lstAward.ElementAt(0).SecondNumber < 10)
                    {
                        s2 = "0" + lstAward.ElementAt(0).SecondNumber;
                    }
                    else
                    {
                        s2 = lstAward.ElementAt(0).SecondNumber.ToString();
                    }
                    if (lstAward.ElementAt(0).ThirdNumber < 10)
                    {
                        s3 = "0" + lstAward.ElementAt(0).ThirdNumber;
                    }
                    else
                    {
                        s3 = lstAward.ElementAt(0).ThirdNumber.ToString();
                    }
                    if (lstAward.ElementAt(0).FourthNumber < 10)
                    {
                        s4 = "0" + lstAward.ElementAt(0).FourthNumber;
                    }
                    else
                    {
                        s4 = lstAward.ElementAt(0).FourthNumber.ToString();
                    }
                    if (lstAward.ElementAt(0).FivethNumber < 10)
                    {
                        s5 = "0" + lstAward.ElementAt(0).FivethNumber;
                    }
                    else
                    {
                        s5 = lstAward.ElementAt(0).FivethNumber.ToString();
                    }
                    if (lstAward.ElementAt(0).ExtraNumber < 10)
                    {
                        s6 = "0" + lstAward.ElementAt(0).ExtraNumber;
                    }
                    else
                    {
                        s6 = lstAward.ElementAt(0).ExtraNumber.ToString();
                    }
                    lstResult.Add(lstAward.ElementAt(0).FirstNumber);
                    lstResult.Add(lstAward.ElementAt(0).SecondNumber);
                    lstResult.Add(lstAward.ElementAt(0).ThirdNumber);
                    lstResult.Add(lstAward.ElementAt(0).FourthNumber);
                    lstResult.Add(lstAward.ElementAt(0).FivethNumber);
                    lstResult.Add(lstAward.ElementAt(0).ExtraNumber);
                    ViewData["FirstNumber"] = s1;
                    ViewData["SecondNumber"] = s2;
                    ViewData["ThirdNumber"] = s3;
                    ViewData["FourthNumber"] = s4;
                    ViewData["FivethNumber"] = s5;
                    ViewData["ExtraNumber"] = s6;
                }
                dayResult = d + "/" + m + "/" + y;

                ViewData["ResultDate"] = dayResult;
                ViewData["ListNumberResult"] = lstResult;

                ViewData["ListAwardMegaball"] = lstAward;

                //Set ViewData DateTime Next Draw
                int intHours = 0;
                int intMinutes = 0;
                int intSecond = 0;
                UtilityShared.GetTimeCountDown(intTimeClosed, ref intHours, ref intMinutes, ref intSecond);
                SetViewDataCountDown(intHours, intMinutes, intSecond);
            }
            catch (Exception objEx)
            {
                UtilityShared.WriteLog(strPathLog, objEx.Message);
            }

            return PartialView();
        }

        [HttpPost]
        public string SendContact(string email, string name, string phone, string content)
        {
            string result = "";
            int portMail = 587;
            string domainMail = ConfigurationManager.AppSettings["DomainEmail"];
            Int32.TryParse(ConfigurationManager.AppSettings["PortMail"], out portMail);
            string emailSend = ConfigurationManager.AppSettings["EmailSend"];
            string emailRecieve = ConfigurationManager.AppSettings["EmailRecieve"];
            string passwordMail = ConfigurationManager.AppSettings["PassEmail"];

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            SmtpClient SmtpServer = new SmtpClient(domainMail);
            mail.From = new MailAddress(emailSend);
            mail.To.Add(emailRecieve);
            mail.Subject = "RippleLot - Gửi thông tin liên hệ";
            string contentBody = "<strong>Thông tin liên hệ</strong> <br/> <p> Emai : <strong>" + email + "</strong></p></br> <p> Họ tên : <strong>" + name + "</strong></p></br> <p> Điện thoại : <strong>" + phone + "</strong></p> </br> <p> Nội dung : <strong>" + content + "</strong></p>";
            mail.Body = contentBody;
            mail.IsBodyHtml = true;
            SmtpServer.Port = portMail;
            SmtpServer.Credentials = new System.Net.NetworkCredential(emailSend, passwordMail);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
            result = "sendsuccess";
            return result;
        }

        public PartialViewResult LoaddTransactionBookingOrderByDate()
        {
            int intPageSize = 3;
            Int32.TryParse(ConfigurationManager.AppSettings["NumberRecord"], out intPageSize);

            IEnumerable<BookingMegaball> lstBookingMegaball = null;//(IEnumerable<BookingMegaball>)objRedisCache.GetCache<IEnumerable<BookingMegaball>>(Common.KeyListBooking);
            DrawBO objDraw = drawServices.GetNewestDrawID();
            if (objDraw != null && objDraw.DrawID > 0)
            {
                int drawID = objDraw.DrawID;
                lstBookingMegaball = bookingMegaballServices.ListBookingByDraw(drawID, 0, intPageSize);
            }

            ViewData["lstBookingMegaball"] = lstBookingMegaball;

            return PartialView();
        }

        public PartialViewResult ListAwardMegaball()
        {
            int intPageSize = 3;
            Int32.TryParse(ConfigurationManager.AppSettings["NumberRecord"], out intPageSize);

            //lstAwardMegaball = null;
            //if (objRedisCache == null)
            //{
            //    objRedisCache = new RedisCache();
            //}
            //lstAwardMegaball = (IEnumerable<AwardMegaball>)objRedisCache.GetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward + "_" + 0 + intPageSize.ToString());
            //if (lstAwardMegaball == null)
            //{
            IEnumerable<AwardMegaball> lstAwardMegaball = awardMegabalServices.GetListAwardMegaball(1, intPageSize);

            //    if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
            //    {
            //        objRedisCache.SetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward + "_" + 0 + intPageSize.ToString(), lstAwardMegaball);
            //    }
            //}
            if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
            {
                //lstAwardMegaball = awardMegabalServices.GetListAwardMegaball(1, intPageSize);
                ViewData["lstAwardMegaball"] = lstAwardMegaball;
            }
            return PartialView();
        }

        public ActionResult StatisticNumber()
        {
            List<string> lstString = new List<string>();
            lstString.Add(GenHtmlStatisticNumber());

            return PartialView(lstString);
        }

        public string GenHtmlStatisticNumber()
        {
            StringBuilder strBuilder = new StringBuilder();
            int count = 0;
            try
            {
                for (int i = 1; i <= 39; i++)
                {
                    IEnumerable<AwardMegaball> lstBooking = awardServices.GetAllListAward();
                    if (lstBooking != null && lstBooking.Count() > 0)
                    {
                        List<AwardMegaball> lst1 = lstBooking.Where(c => c.FirstNumber == i || c.SecondNumber == i || c.ThirdNumber == i || c.FourthNumber == i || c.FivethNumber == i).ToList();
                        List<AwardMegaball> lst2 = lstBooking.Where(c => c.ExtraNumber == i).ToList();
                        if (lst1 != null && lst1.Count() > 0 || lst2 != null && lst2.Count() > 0)
                        {
                            count = lst1.Count + lst2.Count;
                            int countBooking = lstBooking.Count();

                            float pecent = ((float)(count * 100) / (float)countBooking);
                            if (pecent > 100)
                            {
                                pecent = 100;
                            }
                            strBuilder.Append("<div class=\"bar\">");
                            strBuilder.Append("<div class=\"title\">" + i + "</div>");
                            if (pecent > 50)
                            {
                                strBuilder.Append("<div class=\"value red\" title=\"" + pecent.ToString("##.00") + "%\" data-toggle=\"tooltip\" data-placement=\"top\" style=\"height:" + pecent.ToString("##.00") + "%; \"></div>");
                            }
                            else
                            {
                                strBuilder.Append("<div class=\"value\" title=\"" + pecent.ToString("##.00") + "%\" data-toggle=\"tooltip\" data-placement=\"top\" style=\"height:" + pecent.ToString("##.00") + "%;\"></div>");
                            }
                            strBuilder.Append("</div>");

                        }
                        else
                        {
                            count = lst1.Count + lst2.Count;
                            int countBooking = lstBooking.Count();

                            float pecent = ((float)(count * 100) / (float)countBooking);

                            strBuilder.Append("<div class=\"bar\">");
                            strBuilder.Append("<div class=\"title\">" + i + "</div>");

                            strBuilder.Append("<div class=\"value\" title=\"" + pecent.ToString("##.00") + "%\" data-toggle=\"tooltip\" data-placement=\"top\" style=\"height:" + pecent.ToString("##.00") + "%;\"></div>");

                            strBuilder.Append("</div>");
                        }
                    }
                    else
                    {
                        strBuilder.Append("<div class=\"bar\">");
                        strBuilder.Append("<div class=\"title\">" + i + "</div>");

                        strBuilder.Append("<div class=\"value\" title=\"" + 0 + "%\" data-toggle=\"tooltip\" data-placement=\"top\" style=\"height:" + 0 + "%;\"></div>");

                        strBuilder.Append("</div>");
                    }
                }

                return strBuilder.ToString();
            }
            catch (Exception e)
            {
                return "";
            }


        }

        [HttpPost]
        public JsonResult GetListAwardOrderByDate1()
        {
            string strhtml = "-1";
            int intPageSize = 10;
            Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
            //AwardMegaballRepository objAwardMegaballRepository = new AwardMegaballRepository();
            IEnumerable<AwardMegaball> lstAwardMegaball = (IEnumerable<AwardMegaball>)objRedisCache.GetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward);

            if (lstAwardMegaball == null || lstAwardMegaball.Count() == 0)
            {
                lstAwardMegaball = awardServices.GetListAwardOrderByDate();
            }
            StringBuilder strBOBj = new StringBuilder();
            if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
            {
                int i = 0;
                foreach (var item in lstAwardMegaball)
                {
                    if (i == 0)
                    {
                        strBOBj.Append(@"<li>" + ConvetTwoNumberToString(item.FirstNumber) + "</li>");
                        strBOBj.Append(@"<li>" + ConvetTwoNumberToString(item.SecondNumber) + "</li>");
                        strBOBj.Append(@"<li>" + ConvetTwoNumberToString(item.ThirdNumber) + "</li>");
                        strBOBj.Append(@"<li>" + ConvetTwoNumberToString(item.FourthNumber) + "</li>");
                        strBOBj.Append(@"<li>" + ConvetTwoNumberToString(item.FivethNumber) + "</li>");
                        strBOBj.Append(@"<li>" + ConvetTwoNumberToString(item.ExtraNumber) + "</li>");

                        i++;
                    }

                }
                strhtml = strBOBj.ToString();
                objRedisCache.SetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward, lstAwardMegaball);
            }
            List<string> lst = new List<string>();
            lst.Add(strhtml);
            return Json(new { content = strhtml });
        }

        private string ConvetTwoNumberToString(int number)
        {
            if (number < 10)
                return "0" + number;
            else
                return number.ToString();
        }

        #region SetViewDataCountDown
        /// <summary>

        /// Created date: 05.01.2018
        /// Description: Set ViewData CountDown
        /// </summary>
        /// <param name="intHours"></param>
        /// <param name="intMinutes"></param>
        /// <param name="intSeconds"></param>
        private void SetViewDataCountDown(int intHours, int intMinutes, int intSeconds)
        {
            ViewData[KeyManager._HOUR_COUNT_DOWN] = intHours;
            ViewData[KeyManager._MINUTE_COUNT_DOWN] = intMinutes;
            ViewData[KeyManager._SECOND_COUNT_DOWN] = intSeconds;
        }
        #endregion

        public ActionResult AboutUs()
        {
            return View();
        }
        public ActionResult ContactUs()
        {
            return View();
        }
        public ActionResult FAQ()
        {
            return View();
        }
        public ActionResult HowToPlay()
        {
            return View();
        }
        public ActionResult TermOfUse()
        {
            return View();
        }

        public ActionResult Policy()
        {
            return View();
        }
        public PartialViewResult MenuRight()
        {
            return PartialView();
        }
        public PartialViewResult Header()
        {
            try
            {
                decimal JackPot = 0;
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = JackpotID;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = false;
                objAddressInfoRequest.CreatedBy = JackpotID;
                objAddressInfoRequest.WalletId = short.Parse(WalletID);

                AddressRepository objAddressRepository = new AddressRepository();
                AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                {
                    JackPot = Math.Truncate(objAddressInfo.Available * 100) / 100;
                }
                ViewBag.GenJackPot = GenJackpotResult(JackPot);
            }
            catch (Exception ex)
            {
                Response.Redirect("/error");
            }
            return PartialView();
        }
        public PartialViewResult ContentLeft()
        {
            decimal JackPot = 0;
            AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
            objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
            objAddressInfoRequest.UserID = JackpotID;
            objAddressInfoRequest.ClientID = short.Parse(ClientID);
            objAddressInfoRequest.SystemID = short.Parse(SystemID);
            objAddressInfoRequest.IsCreateAddr = false;
            objAddressInfoRequest.CreatedBy = JackpotID;
            objAddressInfoRequest.WalletId = short.Parse(WalletID);

            AddressRepository objAddressRepository = new AddressRepository();
            AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

            if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
            {
                JackPot = Math.Truncate(objAddressInfo.Available * 100) / 100;
            }
            ViewBag.JackPot = JackPot;
            ViewBag.GenJackPot = GenJackpotResult(JackPot);
            if (CheckTimeBooking())
            {
                ViewData[KeyManager._DATEANDTIME] = DateTime.Now.ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss UTC");
                ViewData[KeyManager._ENDDATE] = GetOpenDate().ToString("yyyy/MM/dd HH:mm:ss UTC");
            }
            else
            {
                ViewData[KeyManager._DATEANDTIME] = DateTime.Now.ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss");
                ViewData[KeyManager._ENDDATE] = GetOpenDate().ToString("yyyy/MM/dd HH:mm:ss UTC");
            }
            return PartialView();
        }
        [HttpPost]
        public async Task<double> CurrencyToBtc()
        {
            try
            {
                var request = new RestRequest("tobtc?currency=USD&value=1", Method.GET);
                var response = await _client.ExecuteGetTaskAsync<double>(request);
                return double.Parse(response.Content);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public string GenJackpotResult(decimal jackpot)
        {
            string strHtml = string.Empty;
            string strJackPot = jackpot.ToString();
            int value1 = 0;
            int value2 = 0;
            StringBuilder strBuider = new StringBuilder();
            strBuider.Append("<div class=\"number-jackpot\">");
            if (strJackPot.IndexOf('.') != -1)
            {
                string[] arrNumber = strJackPot.Split('.');
                if (arrNumber != null && arrNumber.Length == 2)
                {
                    value1 = int.Parse(arrNumber[0]);
                    value2 = int.Parse(arrNumber[1]);
                    for (int i = 0; i < arrNumber[0].Length; i++)
                    {
                        if (arrNumber[0].Length == 1)
                        {
                            strBuider.Append("<span class=\"number\">0</span>");
                            strBuider.Append("<span class=\"number\">" + arrNumber[0] + "</span>");
                        }
                        else
                        {
                            strBuider.Append("<span class=\"number\">" + arrNumber[0].Substring(i, 1) + "</span>");
                        }
                    }
                    strBuider.Append("<span class=\"dot\">.</span>");
                    for (int j = 0; j < arrNumber[1].Length; j++)
                    {
                        if (arrNumber[1].Length == 1)
                        {
                            strBuider.Append("<span class=\"number\">" + arrNumber[1] + "</span>");
                            strBuider.Append("<span class=\"number\">0</span>");
                        }
                        else
                        {
                            strBuider.Append("<span class=\"number\">" + arrNumber[1].Substring(j, 1) + "</span>");
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < strJackPot.Length; i++)
                {
                    if (strJackPot.Length == 1)
                    {
                        strBuider.Append("<span class=\"number\">0</span>");
                        strBuider.Append("<span class=\"number\">" + jackpot + "</span>");
                    }
                    else
                    {
                        strBuider.Append("<span class=\"number\">" + strJackPot.Substring(i, 1) + "</span>");
                    }
                }
                strBuider.Append("<span class=\"dot\">.</span>");
                strBuider.Append("<span class=\"number\">0</span>");
                strBuider.Append("<span class=\"number\">0</span>");

            }
            strBuider.Append("<span class=\"text-btc\">BTC</span>");
            strBuider.Append("</div>");

            strHtml = strBuider.ToString();
            return strHtml;
        }
        private bool CheckTimeBooking()
        {
            Type weekdays = typeof(Days);
            string dayofWeek = DateTime.Now.ToUniversalTime().DayOfWeek.ToString();
            int intDays = (int)Enum.Parse(weekdays, dayofWeek); ;
            string strDayCheck = DayOpen;//key config
            string[] arrDayCheck = strDayCheck.Split(',');
            int[] arrintDayCheck;
            if (arrDayCheck.Length > 0)
            {
                arrintDayCheck = new int[arrDayCheck.Length];
                for (int j = 0; j < arrDayCheck.Length; j++)
                {
                    arrintDayCheck[j] = (int)Enum.Parse(weekdays, arrDayCheck[j].ToString());
                }
                Array.Sort(arrintDayCheck);

                if (arrintDayCheck.Length > 0)
                {
                    for (int k = 0; k < arrintDayCheck.Length; k++)
                    {
                        if (intDays == arrintDayCheck[k])
                        {
                            int TimeClosedCountDown = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_CLOSED_COUNT_DOWN]);
                            int TimeOpenedCountDown = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_OPENED_COUNT_DOWN]);

                            DateTime dtBooking = DateTime.Now.ToUniversalTime();
                            DateTime dtOpenDate = DateTime.Now.ToUniversalTime().Date.Add(new TimeSpan(TimeOpenedCountDown, 0, 0));
                            DateTime dtClosedDate = DateTime.Now.ToUniversalTime().Date.Add(new TimeSpan(TimeClosedCountDown, 0, 0));

                            DateTime dtBookingTemp = new DateTime(dtBooking.Year, dtBooking.Month, dtBooking.Day, dtBooking.Hour, dtBooking.Minute, dtBooking.Second);

                            DateTime dtOpenDateTemp = new DateTime(dtOpenDate.Year, dtOpenDate.Month, dtOpenDate.Day, dtOpenDate.Hour, dtOpenDate.Minute, dtOpenDate.Second);
                            DateTime dtClosedDateTemp = new DateTime(dtClosedDate.Year, dtClosedDate.Month, dtClosedDate.Day, dtClosedDate.Hour, dtClosedDate.Minute, dtClosedDate.Second);

                            int resultOpen = DateTime.Compare(dtBookingTemp, dtOpenDateTemp);
                            int resultClosed = DateTime.Compare(dtBookingTemp, dtClosedDateTemp);
                            if (resultClosed >= 0 && resultOpen <= 0)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
        private DateTime GetOpenDate()
        {
            DateTime dtNow = DateTime.Now.ToUniversalTime();
            DrawBO objDrawID = drawServices.GetNewestDrawID();
            if (objDrawID != null)
            {
                dtNow = objDrawID.EndDate;
            }
            return dtNow;
        }

        public PartialViewResult ModalRegistrationLogin()
        {
            return PartialView();
        }
    }
}
