﻿
using BBC.Core.Common.Log;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CLP.Wallet.Repository;
using BBH.Lotte.CLP.Web.Models;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using Microsoft.Practices.Unity;
using NBitcoin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class RegisterController : Controller
    {
        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];

        [Dependency]
        protected IMemberServices memberServices { get; set; }
        [Dependency]
        protected IPointsServices objPointRepository { get; set; }
        public ActionResult Index()
        {
            string url = "";
            string redirectUrl = Request.QueryString["redirect"];
            if (redirectUrl == null || redirectUrl == "")
            {
                url = "/";
            }
            else
            {
                url = redirectUrl;
            }
            ViewData["UrlRedirect"] = url;
            return View();
        }

        [HttpPost]
        public async Task<string> MemberRegister(string email, string password, string mobile="", string fullName="")
        {
            string result = "";
            string strCode = Utilitys.GenCode();
            string tick = DateTime.Now.Ticks.ToString();
            string keyCode = Utilitys.MaHoaMD5(strCode + tick);

            MemberBO member = new MemberBO();
            member.IsActive = 0;
            member.IsDelete = 0;
            member.Password = Utilitys.MaHoaMD5(password);
            member.Points = 0;
            member.CreateDate = DateTime.Now;
            member.Email = email;
            member.FullName = fullName;
            member.Gender = 1;
            member.Birthday = DateTime.Parse("01/01/1990");
            member.Avatar = string.Empty;
            member.Mobile = mobile;
            member.LinkLogin = email + "/" + keyCode;
            bool checkEmailExists = memberServices.CheckEmailExists(email);
            if (checkEmailExists)
            {
                result = "EmailExists";
            }
            else
            {
                //bool rs = true;
                int intMemberID = 0;
                bool rs = memberServices.InsertMember(member, ref intMemberID);
                if (rs)
                {
                    

                    //Create account wallet from wcf wallet
                    AccountCreationRequest objAccountCreationRequest = new AccountCreationRequest();
                    objAccountCreationRequest.ClientID = short.Parse(ClientID);
                    objAccountCreationRequest.Email = member.Email;
                    objAccountCreationRequest.Name = member.Email;
                    objAccountCreationRequest.PhoneNumber = "";
                    objAccountCreationRequest.RequestedBy = RequestBy;
                    objAccountCreationRequest.SystemID = short.Parse(SystemID);
                    objAccountCreationRequest.UserID = member.Email;
                    objAccountCreationRequest.WalletID = long.Parse(WalletID);
                    //objAccountCreationRequest.CurrencyCodes = lstCurrencyCode;

                    AccountInfo objAccountInfo = new AccountInfo();
                    AccountRepository objAccountRepository = new AccountRepository();
                    objAccountInfo = objAccountRepository.Create(objAccountCreationRequest);

                    if (objAccountInfo != null && objAccountInfo.WalletID != 0)
                    {
                        bool rsSendMail = SentMailServicesModels.WSSentMail.SendMailByLotteryCarcoin(email, keyCode);
                        //bool rsSendMail = SentMailServicesModels.WSSentMail.SentMailByCSCJacpot(email, keyCode);

                        result = "registerSuccess";
                    }
                    else
                    {
                        AccountInfoRequest objAccountInfoRequest = new AccountInfoRequest();
                        objAccountInfoRequest.ClientID = short.Parse(ClientID);
                        objAccountInfoRequest.SystemID = short.Parse(SystemID);
                        objAccountInfoRequest.UserID = member.Email;

                        objAccountRepository = new AccountRepository();
                        objAccountInfo = objAccountRepository.Get(objAccountInfoRequest);
                        if (objAccountInfo != null && objAccountInfo.WalletID != 0)
                        {
                            bool rsSendMail = SentMailServicesModels.WSSentMail.SendMailByLotteryCarcoin(email, keyCode);
                            result = "registerSuccess";
                        }
                    }
                }
                else
                {
                    result = "RegisterFaile";
                }

                if (result == "RegisterFaile")
                {
                    memberServices.DeleteMember(intMemberID);
                }
            }
            return result;
        }

        [HttpPost]
        public string ResfeshPoint()
        {
            int TimeCookies = int.Parse(ConfigurationManager.AppSettings["TimeCookies"]);
            string result = "";
            MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
            if (member != null)
            {
                double memeberPoints = memberServices.GetPointsMember(member.MemberID);
                System.Web.HttpContext.Current.Session[SessionKey.BTCPOINT] = RDUser.BTCPoint;
                System.Web.HttpContext.Current.Session[SessionKey.CARCOINPOINT] = RDUser.CarcoinPoint;
                System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = (int)memeberPoints;
                result = memeberPoints.ToString();
            }
            else
            {
                result = "resfeshpointFails";
            }
            return result;
        }

        async Task<RippleAccount> GetRippleAddressAsync(string path)
        {
            string str = string.Empty;
            RippleAccount lst = new RippleAccount();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    using (var response = await client.GetAsync(path))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            str = await response.Content.ReadAsStringAsync();
                            lst = JsonConvert.DeserializeObject<RippleAccount>(str);
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message.Trim());
            }
            return lst;
        }
    }
}
