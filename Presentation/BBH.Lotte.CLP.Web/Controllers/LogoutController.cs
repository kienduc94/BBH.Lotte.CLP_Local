﻿using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CLP.Web.Models;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class LogoutController : Controller
    {
        //
        // GET: /Logout/
        public ActionResult Index()
        {
            Session.RemoveAll();
            System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] = null;
            System.Web.HttpContext.Current.Session[SessionKey.EMAIL] = null;
            //Session["points"] = null;
            System.Web.HttpContext.Current.Session[SessionKey.BTCPOINT] = null;
            System.Web.HttpContext.Current.Session[SessionKey.CARCOINPOINT] = null;
            System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = null;
            System.Web.HttpContext.Current.Session[SessionKey.PASSWORD] = null;

            if (Request.Cookies["LotteryCookiesLogin"] != null)
            {
                var c = new HttpCookie("LotteryCookiesLogin");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }

            Response.Redirect("/play-game");
            return View();
        }

    }
}
