﻿using BBH.Lotte.CLP.Web.Models;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using BBH.LotteFE.CLP.Repository;
using Utility = BBH.LotteFE.CLP.Repository.Utility;
using Microsoft.Practices.Unity;
using NBitcoin;
using Newtonsoft.Json;
using QBitNinja.Client;
using QBitNinja.Client.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.Mvc;
using System.Linq;
using BBH.Lotte.CLP.Shared;
using BBC.Core.Common.Cache;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.Wallet.Repository;
using BBC.Core.Common.Log;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class PointController : Controller
    {
        //
        // GET: /Point/
        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];
        string AdminID = ConfigurationManager.AppSettings[KeyManager.AdminID];
        string FeeBTC = ConfigurationManager.AppSettings[KeyManager.FeeBTC];
        string FeeCarCoin = ConfigurationManager.AppSettings[KeyManager.FeeCarCoin];

        string pathLog = ConfigurationManager.AppSettings["PathLog"];
        string masterKeyWan2Lot = ConfigurationManager.AppSettings["KeyWan2Lot"];
        string TimeExpired = ConfigurationManager.AppSettings["TimeExpired"];
        string Linktestnet = ConfigurationManager.AppSettings["LinkTestnet"];
        string RippleTag = ConfigurationManager.AppSettings["RippleTag"];

        RedisCache objRedisCache = new RedisCache();

        [Dependency]
        protected IMemberServices objMemberRepository { get; set; }
        [Dependency]
        protected IPointsServices objPointRepository { get; set; }

        public ActionResult Index()
        {

            ViewBag.LinkTestnet = Linktestnet;
            ViewData[SessionKey.BTCADDRESS] = "";
            ViewData[SessionKey.CARCOINADDRESS] = "";
            if (!RDUser.IsLogedIn())
            {
                Response.Redirect("/login?redirect=balance-refill");
            }
            else
            {
                MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
                if (member != null)
                {
                    ViewData[SessionKey.BTCADDRESS] = RDUser.BTCAddress;
                    ViewData[SessionKey.CARCOINADDRESS] = RDUser.CARCOINADDRESS;
                }
            }
            return View();
        }

        private static List<Coin> GetCoin(string strAdrress)
        {
            List<Coin> list = new List<Coin>();
            try
            {
                QBitNinjaClient client = new QBitNinjaClient(Network.TestNet);
                BalanceModel myBalance = client.GetBalance(new BitcoinPubKeyAddress(strAdrress), unspentOnly: true).Result;
                foreach (BalanceOperation op in myBalance.Operations)
                {
                    var receivedCoins = op.ReceivedCoins;
                    foreach (Coin e in receivedCoins)
                    {
                        list.Add(e);
                    }
                }
            }
            catch (Exception ex)
            {
                list = null;
            }
            return list;

        }

        private static bool SendToNetwork(Transaction tx)
        {
            QBitNinjaClient client = new QBitNinjaClient(Network.TestNet);
            BroadcastResponse broadcastResponse = client.Broadcast(tx).Result;
            return broadcastResponse.Success;
        }

        [HttpPost]
        public JsonResult GetExchangePoint()
        {
            float PointValue = 1;
            float CoinValue = 1;
            ExchangeTicketBO objExchangePointBO = objPointRepository.ListExchangeTicketByMember(int.Parse(ClientID), "XRP");
            if (objExchangePointBO != null)
            {
                PointValue = objExchangePointBO.PointValue;
                CoinValue = objExchangePointBO.TicketNumber;
            }
            return Json(new { pointvalue = PointValue, coinvalue = CoinValue });
        }

        [HttpPost]
        public JsonResult GetExchangeTicket()
        {
            float PointValue = 1;
            float TicketNumber = 1;
            ExchangeTicketBO objExchangeTicketBO = objPointRepository.ListExchangeTicketByMember(int.Parse(ClientID), "XRP");//BTC
            if (objExchangeTicketBO != null)
            {
                PointValue = objExchangeTicketBO.PointValue;
                TicketNumber = objExchangeTicketBO.TicketNumber;
            }
            return Json(new { pointvalue = PointValue, ticketvalue = TicketNumber });
        }

       
        public ActionResult TransactionWallet(string p)
        {

            ViewBag.LinkTestnet = Linktestnet;
            int intPageSize = 10;
            int start = 0, end = 10;
            int flag = -1;
            if (System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] == null)
            {
                Response.Redirect("/login");
            }
            else
            {
                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                int page = 1;
                try
                {
                    if (p != null && p != "")
                    {
                        page = int.Parse(p);
                    }
                }
                catch
                {

                }
                end = (page * intPageSize);
                int intTimeClosedCountDown = int.Parse(ConfigurationManager.AppSettings["TimeClosedCountDown"]);
                int intTimeOpenCountDown = int.Parse(ConfigurationManager.AppSettings["TimeOpenCountDown"]);
                ViewData[KeyManager._HOUR_COUNT_DOWN] = intTimeClosedCountDown;
            }
            return View(GenHTMLBody_TransactionWallet(RDUser.MemberID, start, end, DateTime.Now, DateTime.Now, flag, p));
        }
        public List<string> GenHTMLBody_TransactionWallet(int memberID, int start, int end, DateTime? dtbStart, DateTime? dtbEnd, int flag, string p)
        {
            List<string> lst = null;
            string strTotalRows = "0";
            int pageSize = 5, page = 1;
            try
            {
                if (p != null && p != "")
                {
                    page = int.Parse(p);
                }
            }
            catch
            {

            }
            try
            {
                pageSize = int.Parse(ConfigurationManager.AppSettings["NumberRecordPage"]);
            }
            catch
            {

            }
            try
            {
                IEnumerable<TransactionCoinBO> lstTransactionPointsBO = null;
                if (flag == -1)//default 
                {
                    lstTransactionPointsBO = objPointRepository.ListTransactionWalletByMember(memberID, start, end);
                }
                else
                {
                    lstTransactionPointsBO = objPointRepository.ListTransactionWalletBySearch(memberID, dtbStart, dtbEnd, start, end);
                }
                StringBuilder strBOBj = new StringBuilder();
                if (lstTransactionPointsBO != null)
                {
                    int i = 0;
                    string color = "#eee";
                    page = page - 1;
                    foreach (TransactionCoinBO item in lstTransactionPointsBO)
                    {
                        strTotalRows = item.TotalRecord.ToString().Trim();
                        string Link = item.TransactionBitcoin;
                        if (i % 2 != 0)
                        {
                            color = "#eee";
                        }
                        else
                        {
                            color = "#fff";
                        }
                        int number = pageSize * page + i + 1;

                        string strValueTransaction = (item.ValueTransaction > 0 ? string.Format("{0:0.00000000}", item.ValueTransaction) : "0");
                        strBOBj.Append("<tr class='none-top-border' style='cursor:pointer; background-color:" + color + "' onclick=\"ShowTransactionWalletDetail('" + item.TransactionID + "','" + item.WalletAddressID + "','" + item.WalletID + "','" + strValueTransaction + "','" + Link + "','" + item.CreateDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "','" + item.ExpireDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "')\" data-toggle=\"modal\" data-target=\"#standardModal\">");
                        strBOBj.Append("<td class='textAlignCenter'>" + number + "</td>");
                        strBOBj.Append("<td class='textAlignCenter'>" + strValueTransaction + "</td>");
                        strBOBj.Append("<td class='textAlignCenter'>" + item.CreateDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "</td>");
                        strBOBj.Append("</tr>");
                        i++;
                    }
                }
                else
                {
                    strBOBj.Append("<tr><td colspan='3'><div class='tfalert alert-warning'>Sorry, nothing found</div></td></tr>");
                }
                lst = new List<string>();
                lst.Add(strBOBj.ToString());
                TempData["TotalRecord"] = int.Parse(strTotalRows);
                lst.Add(GenPaging(p));
                return lst;
            }
            catch (Exception objEx)
            {
                return null;
            }
        }
        public ActionResult DetailTransactionWallet(string strTransactionID)
        {
            return View();
        }

        [HttpGet]
        public string SearchTransactionPoint(string fromDate, string toDate, int page)
        {
            DateTime? fromD = DateTime.Now;
            DateTime? toD = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int intPageSize = 10;
            int start = 0, end = 5;
            int totalRecord = 0;
            string json = "";
            MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
            if (member != null)
            {
                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                    toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                }
                else
                {
                    if (string.IsNullOrEmpty(fromDate))
                    {
                        if (string.IsNullOrEmpty(toDate))
                        {
                            fromD = null;
                            toD = DateTime.Parse("01/01/1990");
                        }
                        else
                        {
                            fromD = DateTime.Parse("01/01/1990");
                            toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                        }
                    }
                    else
                    {
                        fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                        toD = DateTime.Now;
                    }
                }
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);

                IEnumerable<TransactionCoinBO> lstTransaction = null;

                if (lstTransaction == null || lstTransaction.Count() == 0)
                {
                    lstTransaction = objPointRepository.ListTransactionWalletBySearch(member.MemberID, fromD, toD, start, end);
                }

                if (lstTransaction != null && lstTransaction.Count() > 0)
                {
                    totalRecord = lstTransaction.ElementAt(0).TotalRecord;
                    int i = 1;
                    foreach (TransactionCoinBO item in lstTransaction)
                    {
                        string styleColor = "#fff";
                        if (i % 2 == 0)
                        {
                            styleColor = "#eee";
                        }
                        string strValueTransaction = (item.ValueTransaction > 0 ? string.Format("{0:0.00000000}", item.ValueTransaction) : "0");
                        builder.Append(@"<tr class='none-top-border' style='cursor:pointer; background-color:" + styleColor + "' onclick=\"ShowTransactionWalletDetail('" + item.TransactionID + "','" + item.WalletAddressID + "','" + item.WalletID + "','" + strValueTransaction + "','" + item.TransactionBitcoin + "','" + item.CreateDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "','" + item.ExpireDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "')\" data-toggle=\"modal\" data-target=\"#standardModal\">");
                        builder.Append(@"<td class='textAlignCenter'>" + i + "</td>");
                        builder.Append(@"<td class='textAlignCenter'>" + strValueTransaction + "</td>");
                        builder.Append(@"<td class='textAlignCenter'>" + item.CreateDate.ToString(BBH.Lotte.CLP.Shared.Utility.strFormmatDatetime) + "</td>");
                        builder.Append(@"</tr>");
                        i++;
                    }
                    int totalPage = totalRecord / intPageSize;
                    int balance = totalRecord % intPageSize;
                    if (balance != 0)
                    {
                        totalPage += 1;
                    }

                    if (totalPage > 1)
                    {
                        for (int m = 1; m <= totalPage; m++)
                        {

                            if (m == page)
                            {
                                builderPaging.Append("<li class=\"page-item active\"><a class=\"page-link waves-effect waves-effect\">" + m + "</a></li>");
                            }
                            else
                            {
                                builderPaging.Append("<li class=\"page-item\"><a onclick=\"PagingSearchPointCoin('" + m + "')\" class=\"page-link waves-effect waves-effect\">" + m + "</a></li>");

                            }
                        }
                    }
                }
                else
                {
                    builder.Append("<tr><td colspan=\"3\">No Result</td></tr>");
                }

                SearchObj obj = new SearchObj();
                obj.ContentResult = builder.ToString();
                obj.PagingResult = builderPaging.ToString();
                obj.Totalrecord = totalRecord;
                json = JsonConvert.SerializeObject(obj);
            }
            return json;
        }

        public string GenPaging(string p)
        {
            StringBuilder builder = new StringBuilder();
            int pageSize = 5, page = 1;
            try
            {
                if (p != null && p != "")
                {
                    page = int.Parse(p);
                }
            }
            catch
            {

            }
            try
            {
                pageSize = int.Parse(ConfigurationManager.AppSettings["NumberRecordPage"]);
            }
            catch
            {

            }
            string linkPage = "", nextLink = "", previewLink = "", firstLink = "", lastLink = "";
            firstLink = "?p=1";
            if (page <= 1)
            {
                previewLink = "?p=1";
            }
            else
            {
                previewLink = "?p=" + (page - 1).ToString();
            }
            builder.Append("<nav>");
            builder.Append("<ul id=\"ulPaging\" class=\"pagination pg-darkgrey wow fadeIn flex-center\" data-wow-delay=\"0.3s\" style=\"visibility: visible; animation-delay: 0.3s; animation-name: fadeIn;\">");
            int pagerank = 5;
            int next = 10;
            int prev = 1;
            if (TempData["TotalRecord"] != null)
            {
                int totalRecore = (int)TempData["TotalRecord"];
                int totalPage = totalRecore / pageSize;
                int balance = totalRecore % pageSize;
                if (balance != 0)
                {
                    totalPage += 1;
                }
                if (page >= totalPage)
                {
                    nextLink = "?p=" + totalPage.ToString();
                }
                else
                {
                    nextLink = "?p=" + (page + 1).ToString();
                }
                int currentPage = page;
                var m = 1;
                if (totalPage > 10)
                {
                    if (page > pagerank + 1)
                    {
                        next = (page + pagerank) - 1;
                        m = page - pagerank;
                    }
                    if (page <= pagerank)
                    {
                        prev = (page - pagerank);
                        m = 1;
                    }
                    if (next > totalPage)
                    {
                        next = totalPage;
                    }
                    if (prev < 1)
                    {
                        prev = 1;
                    }
                }
                else
                {
                    next = totalPage;
                    prev = 1;
                }
                lastLink = "?p=" + totalPage;
                if (totalPage > 1)
                {
                    if (currentPage > 1)
                    {
                        builder.Append("<li class=\"page-item\">");
                        builder.Append("<a href=\"" + previewLink + "\" class=\"page-link waves-effect waves-effect\" aria-label=\"Previous\">");
                        builder.Append("<span aria-hidden=\"true\">«</span>");
                        builder.Append("<span class=\"sr-only\">Previous</span>");
                        builder.Append("</a>");
                        builder.Append(" </li>");
                    }
                }
                if (totalPage > 1)
                {
                    for (; m <= next; m++)
                    {
                        linkPage = "?p=" + m;
                        if (m == currentPage)
                        {
                            builder.Append("<li class=\"page-item active\"><a href=\"" + linkPage + "\" class=\"page-link waves-effect waves-effect\">" + m + "</a></li>");
                        }
                        else
                        {
                            builder.Append("<li class=\"page-item\"><a href=\"" + linkPage + "\" class=\"page-link waves-effect waves-effect\">" + m + "</a></li>");
                        }
                    }
                }
                if (totalPage > 1)
                {
                    if (currentPage < totalPage)
                    {
                        builder.Append("<li class=\"page-item\">");
                        builder.Append("<a href=\"" + nextLink + "\" class=\"page-link waves-effect waves-effect\" aria-label=\"Next\">");
                        builder.Append("<span aria-hidden=\"true\">»</span>");
                        builder.Append("<span class=\"sr-only\">Next</span>");
                        builder.Append("</a>");
                        builder.Append("</li>");
                    }
                }
            }
            builder.Append("</ul>");
            builder.Append("</nav>");
            return builder.ToString();
        }
        [RDUser]
        public ActionResult WithDraw()
        {
            string strBTCAddress = string.Empty;
            string strCarcoinAddress = string.Empty;
            decimal numberCoinBTCAvailable = 0;
            decimal numberCarcoinAvailable = 0;
            //get address btc
            AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
            objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
            objAddressInfoRequest.UserID = RDUser.Email;
            objAddressInfoRequest.ClientID = short.Parse(ClientID);
            objAddressInfoRequest.SystemID = short.Parse(SystemID);
            objAddressInfoRequest.IsCreateAddr = false;// no check create new account
            objAddressInfoRequest.CreatedBy = RDUser.Email;

            AddressRepository objAddressRepository = new AddressRepository();
            AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

            if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
            {
                strBTCAddress = objAddressInfo.AddressId;
                numberCoinBTCAvailable = objAddressInfo.Available;

            }
            SetSessionBTC(strBTCAddress, strCarcoinAddress, numberCoinBTCAvailable, numberCarcoinAvailable);
            if (!string.IsNullOrEmpty(strBTCAddress) || !string.IsNullOrEmpty(strCarcoinAddress))
            {
                ViewBag.BTCAddress = strBTCAddress;
                ViewBag.CarcoinAddress = strCarcoinAddress;
                ViewBag.BTCNumberCoin = numberCoinBTCAvailable;
                ViewBag.CarNumberCoin = numberCarcoinAvailable;
                ViewBag.Fee = 0.0001;
            }
            return View();
        }
        public PartialViewResult _PartialWithDrawBTC()
        {
            WithDrawBTCModel objWithDrawBTCModel = new WithDrawBTCModel();
            try
            {
                string strBTCAddress = string.Empty;
                decimal numberCoinBTCAvailable = 0;
                //get address btc
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = RDUser.Email;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = false;// no check create new account
                objAddressInfoRequest.CreatedBy = RDUser.Email;

                AddressRepository objAddressRepository = new AddressRepository();
                AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                {
                    strBTCAddress = objAddressInfo.AddressId;
                    numberCoinBTCAvailable = objAddressInfo.Available;

                }
                ViewBag.BTCAddress = strBTCAddress;
                ViewBag.BTCNumberCoin = numberCoinBTCAvailable;
                ViewBag.FeeBTC = FeeBTC;
            }
            catch (Exception ex)
            {

            }
            return PartialView("_PartialWithDrawBTC", objWithDrawBTCModel);
        }

        public PartialViewResult _PartialWithDrawCarCoin()
        {
            WithDrawCarcoinModel objWithDrawCarcoinModel = new WithDrawCarcoinModel();
            try
            {
                string strCarcoinAddress = string.Empty;
                decimal numberCarcoinAvailable = 0;
                ViewBag.CarcoinAddress = strCarcoinAddress;
                ViewBag.CarNumberCoin = numberCarcoinAvailable;
                ViewBag.FeeCarCoin = FeeCarCoin;
            }
            catch (Exception ex)
            {
            }
            return PartialView("_PartialWithDrawCarCoin", objWithDrawCarcoinModel);
        }
        public JsonResult PartialWithDrawBTCSubmit(WithDrawBTCModel objWithDrawBTCModel)
        {
            bool isSuccess = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (RDUser.IsLogedIn())
                    {
                        string strBTCAddress = string.Empty;
                        decimal numberCoinBTCAvailable = 0;
                        //get address btc
                        AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                        objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                        objAddressInfoRequest.UserID = RDUser.Email;
                        objAddressInfoRequest.ClientID = short.Parse(ClientID);
                        objAddressInfoRequest.SystemID = short.Parse(SystemID);
                        objAddressInfoRequest.IsCreateAddr = false;// no check create new account
                        objAddressInfoRequest.CreatedBy = RDUser.Email;

                        AddressRepository objAddressRepository = new AddressRepository();
                        AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                        if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                        {
                            strBTCAddress = objAddressInfo.AddressId;
                            numberCoinBTCAvailable = objAddressInfo.Available;
                        }
                        if (strBTCAddress.Trim() == RDUser.BTCAddress.Trim() && (double)numberCoinBTCAvailable >= objWithDrawBTCModel.AmountBTC)
                        {
                            //Withdraw wallet sent BTC to address
                            WithdrawRequest objWithdrawRequest = new WithdrawRequest();
                            objWithdrawRequest.Amount = (decimal)objWithDrawBTCModel.AmountBTC;
                            objWithdrawRequest.ClientID = short.Parse(ClientID);
                            objWithdrawRequest.CreatedBy = RDUser.Email;
                            objWithdrawRequest.CurrencyCode = CurrencyCode.BTC;
                            objWithdrawRequest.Description = "Lottery With Draw";
                            objWithdrawRequest.FromAddress = RDUser.BTCAddress;
                            objWithdrawRequest.SystemID = short.Parse(SystemID);
                            objWithdrawRequest.ToAddress = objWithDrawBTCModel.AddrressBTC;
                            objWithdrawRequest.UserId = RDUser.Email;
                            objWithdrawRequest.WalletID = long.Parse(WalletID);
                            objWithdrawRequest.Fee = (decimal)(objWithDrawBTCModel.AmountBTC - objWithDrawBTCModel.TotalBTC);
                            objWithdrawRequest.Status = TransactionStatus.Pending;

                            TransactionInfo objTransactionInfo = new TransactionInfo();
                            TransactionRepository objTransactionRepository = new TransactionRepository();
                            objTransactionInfo = objTransactionRepository.Withdraw(objWithdrawRequest);
                            if (objTransactionInfo != null && !string.IsNullOrEmpty(objTransactionInfo.TransactionId.ToString()))
                            {
                                isSuccess = true;
                            }
                        }
                        else
                        {
                            isSuccess = false;
                        }
                    }
                    else
                    {
                        return Json(new { success = isSuccess, isLogin = false }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = isSuccess, isLogin = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("PointController-->PartialWithDrawBTCSubmit - Exception : " + objEx.Message);
                return Json(new { success = false, isLogin = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult PartialWithDrawCarcoinSubmit(WithDrawCarcoinModel objWithDrawCarcoinModel)
        {
            bool isSuccess = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (RDUser.IsLogedIn())
                    {
                        string strCarcoinAddress = string.Empty;
                        decimal numberCarcoinAvailable = 0;
                        //get address btc
                        AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                        objAddressInfoRequest.CurrencyCode = CurrencyCode.Carcoin;
                        objAddressInfoRequest.UserID = RDUser.Email;
                        objAddressInfoRequest.ClientID = short.Parse(ClientID);
                        objAddressInfoRequest.SystemID = short.Parse(SystemID);
                        objAddressInfoRequest.IsCreateAddr = false;// no check create new account
                        objAddressInfoRequest.CreatedBy = RDUser.Email;
                        AddressRepository objAddressRepository = new AddressRepository();
                        AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                        if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                        {
                            strCarcoinAddress = objAddressInfo.AddressId;
                            numberCarcoinAvailable = objAddressInfo.Available;
                        }
                        if (strCarcoinAddress.Trim() == RDUser.CARCOINADDRESS && (double)numberCarcoinAvailable >= objWithDrawCarcoinModel.AmountCarCoin)
                        {
                            //Withdraw wallet
                            WithdrawRequest objWithdrawRequest = new WithdrawRequest();
                            objWithdrawRequest.Amount = (decimal)objWithDrawCarcoinModel.AmountCarCoin;
                            objWithdrawRequest.ClientID = short.Parse(ClientID);
                            objWithdrawRequest.CreatedBy = RDUser.Email;
                            objWithdrawRequest.CurrencyCode = CurrencyCode.Carcoin;
                            objWithdrawRequest.Description = "Lottery With Draw";
                            objWithdrawRequest.FromAddress = RDUser.CARCOINADDRESS;
                            objWithdrawRequest.SystemID = short.Parse(SystemID);
                            objWithdrawRequest.ToAddress = objWithDrawCarcoinModel.AddrressCarCoin;
                            objWithdrawRequest.UserId = RDUser.Email;
                            objWithdrawRequest.WalletID = long.Parse(WalletID);
                            objWithdrawRequest.Fee = (decimal)(objWithDrawCarcoinModel.AmountCarCoin - objWithDrawCarcoinModel.TotalCarCoin);

                            TransactionInfo objTransactionInfo = new TransactionInfo();
                            TransactionRepository objTransactionRepository = new TransactionRepository();
                            objTransactionInfo = objTransactionRepository.Withdraw(objWithdrawRequest);
                            if (objTransactionInfo != null && !string.IsNullOrEmpty(objTransactionInfo.TransactionId.ToString()))
                            {
                                isSuccess = true;
                            }
                        }
                        else
                        {
                            isSuccess = false;
                        }
                    }
                    else
                    {
                        return Json(new { success = isSuccess, isLogin = false }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = isSuccess, isLogin = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("PointController-->PartialWithDrawBTCSubmit - Exception : " + objEx.Message);
                return Json(new { success = false, isLogin = false }, JsonRequestBehavior.AllowGet);
            }

        }
        private int SetSessionBTC(string strBTCAddress, string strCarcoinAddress, decimal douCoinBTCAvailable, decimal douCarcoinAvailable)
        {
            try
            {
                System.Web.HttpContext.Current.Session[SessionKey.BTCPOINT] = douCoinBTCAvailable;
                System.Web.HttpContext.Current.Session[SessionKey.CARCOINPOINT] = douCarcoinAvailable;
                System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = douCarcoinAvailable;
                System.Web.HttpContext.Current.Session[SessionKey.BTCADDRESS] = strBTCAddress;
                System.Web.HttpContext.Current.Session[SessionKey.CARCOINADDRESS] = strCarcoinAddress;
                return 0;
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message);
            }
            return 1;//isResult = 1: Error login fail
        }
    }
}
