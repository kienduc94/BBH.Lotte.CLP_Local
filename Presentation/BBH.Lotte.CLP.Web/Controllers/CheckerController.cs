﻿using BBC.Core.Common.Cache;
using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CLP.Web.Models;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class CheckerController : Controller
    {
        string secretKey = ConfigurationManager.AppSettings[KeyManager._SECREC_KEY_CAPTCHA];
        string Number49 = ConfigurationManager.AppSettings[SessionKey.NUMBER49];
        string Number26 = ConfigurationManager.AppSettings[SessionKey.NUMBER26];
        // GET: Checker
        [Dependency]
        protected IAwardMegaballServices awardServices { get; set; }
        RedisCache objRedisCache = new RedisCache();
        static String[] suffixes ={ "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th","th", "th", "th", "th",
                                    "th", "th", "th", "th", "th", "th","th", "st", "nd", "rd", "th","th", "th", "th",
                                    "th", "th","th", "st" };
        public ActionResult Index()
        {
            ViewBag.TitlePage = "Check number";
            ViewBag.Title = "Checker";
            return View();
        }
        public ActionResult CheckResult(string number1, string number2, string number3, string number4, string number5, string extranumber)
        {
            List<string> lstHtml = new List<string>();
            bool CheckResult = CheckNumber(number1, number2, number3, number4, number5, extranumber);
            if (!CheckResult)
            {
                Response.Redirect("/");
            }
            try
            {
                lstHtml.Add(GenHtmlYourNumber(number1, number2, number3, number4, number5, extranumber));
                lstHtml.Add(GenHtmlCheckingRusult(number1, number2, number3, number4, number5, extranumber));
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("CheckerController -> CheckResult: " + objEx.ToJsonString());
                Response.Redirect("/");
            }
            return View(lstHtml);
        }
        public string GenHtmlCheckingRusult(string number1, string number2, string number3, string number4, string number5, string extranumber)
        {
            string strHtml = "";
            try
            {
                StringBuilder strBuilder = new StringBuilder();
                IEnumerable<AwardMegaball> lstAwardMegaball = null;
                //lstAwardMegaball = (IEnumerable<AwardMegaball>)objRedisCache.GetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward);

                if (lstAwardMegaball == null || lstAwardMegaball.Count() == 0)
                {
                    lstAwardMegaball = awardServices.GetListAwardOrderByDate();
                    //objRedisCache.SetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward, lstAwardMegaball);
                }
                if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                {
                    int i = 0;
                    string strClass = "";
                    foreach (var item in lstAwardMegaball)
                    {
                        DateTime dtmCreatedDate = item.CreateDate.Value;
                        string strResult = "";
                        if (i % 2 == 0)
                        {
                            strClass = "alt";
                        }
                        else
                        {
                            strClass = "";
                        }
                        int[] arrNumber = new int[5];
                        arrNumber[0] = item.FirstNumber;
                        arrNumber[1] = item.SecondNumber;
                        arrNumber[2] = item.ThirdNumber;
                        arrNumber[3] = item.FourthNumber;
                        arrNumber[4] = item.FivethNumber;
                        Array.Sort(arrNumber);
                        strBuilder.Append("<tr class=\"" + strClass + " " + (CountNumberExist(arrNumber, item.ExtraNumber.ToString(), number1, number2, number3, number4, number5, extranumber, ref strResult) == 6 ? "won" : "") + "\">");
                        strBuilder.Append("<td class=\"vertical-middle\"><strong> " + dtmCreatedDate.DayOfWeek + " </strong> " + dtmCreatedDate.Day + "" + suffixes[dtmCreatedDate.Day] + " " + dtmCreatedDate.ToString("MMMM", CultureInfo.InvariantCulture) + " " + dtmCreatedDate.Year + " </t >");
                        strBuilder.Append("<td class=\"nowrap\" style=\"text-align: center;\">");
                        strBuilder.Append("<span class=\"result small ball " + (Checknumber(arrNumber[0], number1, number2, number3, number4, number5) == true ? "" : "greyed") + " " + (Checknumber(arrNumber[0], number1, number2, number3, number4, number5) == true ? "thunderball-ball" : "") + "\">" + arrNumber[0] + "" + (Checknumber(arrNumber[0], number1, number2, number3, number4, number5) == true ? "<img src=\"/Images/tick.png\" alt=\"Matched\" class=\"ballTick\">" : "") + "</span>");
                        strBuilder.Append("<span class=\"result small ball " + (Checknumber(arrNumber[1], number1, number2, number3, number4, number5) == true ? "" : "greyed") + " " + (Checknumber(arrNumber[1], number1, number2, number3, number4, number5) == true ? "thunderball-ball" : "") + "\">" + arrNumber[1] + "" + (Checknumber(arrNumber[1], number1, number2, number3, number4, number5) == true ? "<img src=\"/Images/tick.png\" alt=\"Matched\" class=\"ballTick\">" : "") + "</span>");
                        strBuilder.Append("<span class=\"result small ball " + (Checknumber(arrNumber[2], number1, number2, number3, number4, number5) == true ? "" : "greyed") + " " + (Checknumber(arrNumber[2], number1, number2, number3, number4, number5) == true ? "thunderball-ball" : "") + "\">" + arrNumber[2] + "" + (Checknumber(arrNumber[2], number1, number2, number3, number4, number5) == true ? "<img src=\"/Images/tick.png\" alt=\"Matched\" class=\"ballTick\">" : "") + "</span>");
                        strBuilder.Append("<span class=\"result small ball " + (Checknumber(arrNumber[3], number1, number2, number3, number4, number5) == true ? "" : "greyed") + " " + (Checknumber(arrNumber[3], number1, number2, number3, number4, number5) == true ? "thunderball-ball" : "") + "\">" + arrNumber[3] + "" + (Checknumber(arrNumber[3], number1, number2, number3, number4, number5) == true ? "<img src=\"/Images/tick.png\" alt=\"Matched\" class=\"ballTick\">" : "") + "</span>");
                        strBuilder.Append("<span class=\"result small ball " + (Checknumber(arrNumber[4], number1, number2, number3, number4, number5) == true ? "" : "greyed") + " " + (Checknumber(arrNumber[4], number1, number2, number3, number4, number5) == true ? "thunderball-ball" : "") + "\">" + arrNumber[4] + "" + (Checknumber(arrNumber[4], number1, number2, number3, number4, number5) == true ? "<img src=\"/Images/tick.png\" alt=\"Matched\" class=\"ballTick\">" : "") + "</span>");
                        strBuilder.Append("<span class=\"result small thunderball " + (extranumber == item.ExtraNumber.ToString().Trim() ? "" : "greyed") + " " + (extranumber == item.ExtraNumber.ToString().Trim() ? "thunderball-thunderball" : "") + "\">" + item.ExtraNumber + "" + (extranumber == item.ExtraNumber.ToString().Trim() ? "<img src=\"/Images/tick.png\" alt=\"Matched\" class=\"ballTick\">" : "") + "</span>");
                        strBuilder.Append("</td>");
                        strBuilder.Append("<td style =\"text-align: center;\">" + (CountNumberExist(arrNumber, item.ExtraNumber.ToString(), number1, number2, number3, number4, number5, extranumber, ref strResult) == 0 ? "-" : strResult + " Balls") + "</td>");
                        strBuilder.Append("</tr>");
                        i++;
                    }
                }
                else
                {
                    strBuilder.Append("<tr><td colspan=\"3\" class=\"\">No Result</td></tr>");
                }
                strHtml = strBuilder.ToString();
            }
            catch (Exception objEx)
            {
                strHtml = "";
                BBC.CWallet.Share.BBCLoggerManager.Error("CheckerController -> GenHtmlCheckingRusult: " + objEx.ToJsonString());
            }
            return strHtml;
        }
        public string GenHtmlYourNumber(string number1, string number2, string number3, string number4, string number5, string extranumber)
        {
            string strHtml = "";
            try
            {
                StringBuilder strBuilder = new StringBuilder();
                strBuilder.Append("<span class=\"result medium thunderball-ball\">" + number1 + "</span>");
                strBuilder.Append("<span class=\"result medium thunderball-ball\">" + number2 + "</span>");
                strBuilder.Append("<span class=\"result medium thunderball-ball\">" + number3 + "</span>");
                strBuilder.Append("<span class=\"result medium thunderball-ball\">" + number4 + "</span>");
                strBuilder.Append("<span class=\"result medium thunderball-ball\">" + number5 + "</span>");
                strBuilder.Append("<span class=\"result medium thunderball-thunderball\">" + extranumber + "</span>");
                strHtml = strBuilder.ToString();

            }
            catch(Exception objEx)
            {
                strHtml = "";
                BBC.CWallet.Share.BBCLoggerManager.Error("CheckerController -> GenHtmlYourNumber: " + objEx.ToJsonString());
            }
            return strHtml;
        }
        public bool Checknumber(int number, string number1, string number2, string number3, string number4, string number5)
        {
            bool check = false;
            if (number.ToString().Trim() == number1)
            {
                check = true;
            }
            if (number.ToString().Trim() == number2)
            {
                check = true;
            }
            if (number.ToString().Trim() == number3)
            {
                check = true;
            }
            if (number.ToString().Trim() == number4)
            {
                check = true;
            }
            if (number.ToString().Trim() == number5)
            {
                check = true;
            }
            return check;
        }
        public int CountNumberExist(int[] arrNumber, string extraNumberCheck, string number1, string number2, string number3, string number4, string number5, string extranumber, ref string strResult)
        {
            int[] arrArrayCheck = { int.Parse(number1), int.Parse(number2), int.Parse(number3), int.Parse(number4), int.Parse(number5) };
            int count = 0;
            for (int i = 0; i < arrNumber.Length; i++)
            {
                for (int j = 0; j < arrArrayCheck.Length; j++)
                {
                    if (arrNumber[i] == arrArrayCheck[j])
                    {
                        count++;
                    }
                }
            }
            if (extraNumberCheck.Trim() == extranumber.Trim())
            {
                count++;
            }
            strResult = count.ToString();
            return count;
        }
        public bool CheckNumber(string number1, string number2, string number3, string number4, string number5, string extranumber)
        {
            bool result = true;
            try
            {
                if (number1 == "" || int.Parse(number1) < 1 || int.Parse(number1) > 49)
                {
                    result = false;
                }
                if (number2 == "" || int.Parse(number2) < 1 || int.Parse(number2) > 49)
                {
                    result = false;
                }
                if (number3 == "" || int.Parse(number3) < 1 || int.Parse(number3) > 49)
                {
                    result = false;
                }
                if (number3 == "" || int.Parse(number3) < 1 || int.Parse(number3) > 49)
                {
                    result = false;
                }
                if (number4 == "" || int.Parse(number4) < 1 || int.Parse(number4) > 49)
                {
                    result = false;
                }
                if (number5 == "" || int.Parse(number5) < 1 || int.Parse(number5) > 49)
                {
                    result = false;
                }
                if (extranumber == "" || int.Parse(extranumber) < 1 || int.Parse(extranumber) > 26)
                {
                    result = false;
                }
            }
            catch(Exception objEx)
            {
                result = false;
                BBC.CWallet.Share.BBCLoggerManager.Error("CheckerController -> CheckNumber: " + objEx.ToJsonString());
            }
            return result;
        }
        public ActionResult CheckTransparency()
        {
            return View();
        }
        [HttpPost]
        public JsonResult CheckHashTransparency(string strHash, string strCaptcha)
        {
            try
            {
                string strHtml = string.Empty;
                //Hash error || null
                if (strHash == null || strHash.Trim().Length == 0)
                {
                    return Json(new { intTypeError = 1, result = "fails" }, JsonRequestBehavior.AllowGet);
                }
                //Captcha error || null
                if (strCaptcha == null || strCaptcha.Trim().Length == 0)
                {
                    return Json(new { intTypeError = 2, result = "fails" }, JsonRequestBehavior.AllowGet);
                }
                // string strCaptcha = Request["g-recaptcha-response"].ToString();
                var client = new WebClient();
                var response = strCaptcha;
                var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
                var obj = JObject.Parse(result);
                var status = (bool)obj.SelectToken("success");
                if (status == false)
                {
                    return Json(new { intTypeError = 2, result = "fails" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    strHtml = GenHtmlResultCheckHash(strHash);
                    return Json(new { intTypeError = 0, result = strHtml }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("CheckerController -> CheckHashTransparency: " + objEx.ToJsonString());
                return Json(new { intTypeError = 3, result = objEx.Message.ToString(), messageError = objEx.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public string GenHtmlResultCheckHash(string strBlockHash)
        {
            string result = string.Empty;
            StringBuilder strBuilder = new StringBuilder();
            List<int> lstResultNumber = new List<int>();
            try
            {
                char[] array = strBlockHash.ToCharArray();
                if (array.Length > 0)
                {
                    List<string> lst = new List<string>();
                    string str = string.Empty;
                    int k = 0;
                    for (int i = array.Length - 1; i >= 0; i--)
                    {
                        str = str + array[i];
                        k++;
                        if (k % 4 == 0)
                        {
                            string number = ReverseNumber(str);
                            k = 0;
                            lst.Add(number);
                            str = string.Empty;
                        }
                    }


                    if (lst != null && lst.Count > 0)
                    {
                        foreach (var item in lst)
                        {
                            int test = Convert.ToInt32(item, 16);
                            if (lstResultNumber.Count < 5)
                            {
                                test = test % int.Parse(Number49);
                                if (!lstResultNumber.Contains(test))
                                {
                                    if (test == 0)
                                    {
                                        test = int.Parse(Number49);
                                    }
                                    lstResultNumber.Add(test);
                                }
                            }
                            else if (lstResultNumber.Count == 5)
                            {
                                test = test % int.Parse(Number26);
                                if (test == 0)
                                {
                                    test = int.Parse(Number26);
                                }
                                lstResultNumber.Add(test);
                                break;
                            }
                        }
                    }
                }
                if (lstResultNumber != null && lstResultNumber.Count > 5)
                {

                    strBuilder.Append("<div class=\"modal-dialog modal-lg\">");
                    strBuilder.Append("<div class=\"modal-content\">");
                    strBuilder.Append("<div class=\"modal-header\">");
                    strBuilder.Append("<h4 class=\"modal-title text-center\">Winning number</h4>");
                    strBuilder.Append("</div>");
                    strBuilder.Append("<div class=\"modal-body plr-0\">");
                    strBuilder.Append("<div class=\"winning-number\">");
                    strBuilder.Append("<div class=\"balls\">");
                    strBuilder.Append("<span class=\"ticketNumber\">" + lstResultNumber[0] + "</span>");
                    strBuilder.Append("<span class=\"ticketNumber\">" + lstResultNumber[1] + "</span>");
                    strBuilder.Append("<span class=\"ticketNumber\">" + lstResultNumber[2] + "</span>");
                    strBuilder.Append("<span class=\"ticketNumber\">" + lstResultNumber[3] + "</span>");
                    strBuilder.Append("<span class=\"ticketNumber\">" + lstResultNumber[4] + "</span>");
                    strBuilder.Append("<span class=\"ticketNumber\">" + lstResultNumber[5] + "</span>");
                    strBuilder.Append("</div>");
                    strBuilder.Append("</div>");
                    strBuilder.Append("<div class=\"hash-number\">");
                    strBuilder.Append("Hash: " + strBlockHash + "");
                    strBuilder.Append("</div>");
                    strBuilder.Append("<div class=\"modal-footer\">");
                    strBuilder.Append("<button type = \"button\" class=\"btn btn-danger btn-sm\" data-dismiss=\"modal\">Close</button>");
                    strBuilder.Append("</div>");
                    strBuilder.Append("</div>");
                    strBuilder.Append("</div>");
                }
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("CheckerController -> GenHtmlResultCheckHash: " + objEx.ToJsonString());
            }
            return strBuilder.ToString();
        }

        private string ReverseNumber(string number)
        {
            string str = string.Empty;
            char[] array = number.ToCharArray();
            if (array.Length > 0)
            {
                for (int i = array.Length - 1; i >= 0; i--)
                {
                    str = str + array[i];
                }
            }
            return str;
        }

        [HttpPost]
        public string CheckResultNumber(string strNumber)
        {
            string strHtml = string.Empty;
            try
            {
                string firstNumber = string.Empty;
                string secondNumber = string.Empty;
                string thirdNumber = string.Empty;
                string fourNumber = string.Empty;
                string fiveNumber = string.Empty;
                string extraNumber = string.Empty;

                string[] arrNumber = strNumber.Split(',');

                if (arrNumber != null && arrNumber.Length > 0)
                {
                    firstNumber = arrNumber[0];
                    secondNumber = arrNumber[1];
                    thirdNumber = arrNumber[2];
                    fourNumber = arrNumber[3];
                    fiveNumber = arrNumber[4];
                    extraNumber = arrNumber[5];
                }
                strHtml = GenHtmlCheckingRusultNumber(firstNumber, secondNumber, thirdNumber, fourNumber, fiveNumber, extraNumber);
            }
            catch (Exception objEx)
            {
                strHtml = string.Empty;
                BBC.CWallet.Share.BBCLoggerManager.Error("CheckerController -> CheckResultNumber: " + objEx.ToJsonString());
            }
            return strHtml;
        }

        public string GenHtmlCheckingRusultNumber(string number1, string number2, string number3, string number4, string number5, string extranumber)
        {
            string strHtml = "";
            try
            {
                StringBuilder strBuilder = new StringBuilder();
                IEnumerable<AwardMegaball> lstAwardMegaball = null;
                //lstAwardMegaball = (IEnumerable<AwardMegaball>)objRedisCache.GetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward);

                if (lstAwardMegaball == null || lstAwardMegaball.Count() == 0)
                {
                    lstAwardMegaball = awardServices.GetListAwardOrderByDate();
                    //    objRedisCache.SetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward, lstAwardMegaball);
                }
                if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                {
                    int i = 0;
                    string strClass = "";
                    foreach (var item in lstAwardMegaball)
                    {
                        if (i < 30)
                        {
                            DateTime dtmCreatedDate = DateTime.SpecifyKind(item.CreateDate.Value, DateTimeKind.Utc);

                            string strResult = "";
                            if (i % 2 == 0)
                            {
                                strClass = "alt";
                            }
                            else
                            {
                                strClass = "";
                            }
                            int[] arrNumber = new int[5];
                            arrNumber[0] = item.FirstNumber;
                            arrNumber[1] = item.SecondNumber;
                            arrNumber[2] = item.ThirdNumber;
                            arrNumber[3] = item.FourthNumber;
                            arrNumber[4] = item.FivethNumber;
                            Array.Sort(arrNumber);
                            strBuilder.Append("<tr class=\"info\">");
                            strBuilder.Append("<td> " + dtmCreatedDate.ToString("MM/dd/yyyy HH:mm:ss UTC") + " </td> ");
                            strBuilder.Append("<td class=\"text-center balls\">");
                            strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(item.FirstNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + item.FirstNumber + "</span>");
                            strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(item.SecondNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + item.SecondNumber + "</span>");
                            strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(item.ThirdNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + item.ThirdNumber + "</span>");
                            strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(item.FourthNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + item.FourthNumber + "</span>");
                            strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(item.FivethNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + item.FivethNumber + "</span>");
                            strBuilder.Append("<span class=\"ticketNumber " + (extranumber == item.ExtraNumber.ToString().Trim() ? "" : "disable") + "\">" + item.ExtraNumber + "</span>");
                            strBuilder.Append("</td>");
                            strBuilder.Append("<td class=\"text-center\">" + (CountNumberExist(arrNumber, item.ExtraNumber.ToString(), number1, number2, number3, number4, number5, extranumber, ref strResult) == 0 ? "-" : strResult + " Balls") + "</td>");
                            strBuilder.Append("</tr>");
                            i++;
                        }
                    }
                }
                else
                {
                    strBuilder.Append("<tr><td colspan=\"3\" class=\"\">No Result</td></tr>");
                }
                strHtml = strBuilder.ToString();
            }
            catch(Exception objEx)
            {
                strHtml = "";
                BBC.CWallet.Share.BBCLoggerManager.Error("CheckerController -> GenHtmlCheckingRusultNumber: " + objEx.ToJsonString());
            }
            return strHtml;
        }
    }
}