﻿using BBC.Core.Common.Cache;
using BBC.Core.Common.Log;
using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CLP.Web.Models;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class AwardController : Controller
    {
        //
        // GET: /Award/
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        string Number49 = ConfigurationManager.AppSettings["Number49"];
        string Number26 = ConfigurationManager.AppSettings["Number26"];
        static String[] suffixes ={ "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th","th", "th", "th", "th",
                                    "th", "th", "th", "th", "th", "th","th", "st", "nd", "rd", "th","th", "th", "th",
                                    "th", "th","th", "st" };
        [Dependency]
        protected IAwardMegaballServices awardMegabalServices { get; set; }
        [Dependency]
        protected IAwardServices awardServices { get; set; }
        [Dependency]
        protected IBookingMegaballServices bookingMegaballServices { get; set; }
        RedisCache objRedisCache = new RedisCache();
        public ActionResult Index(string p)
        {

            List<string> lstString = new List<string>();
            int intPageSize = 5, page = 1;
            int start = 0, end = 10;
            ViewBag.TitlePage = "Lotto Result";
            Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
            try
            {
                if (p != null && p != "")
                {
                    page = int.Parse(p);
                }
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> Index: " + objEx.ToJsonString());
            }

            lstString.Add(GenHtmlLoterryResultAward(start, end));
            lstString.Add(GenPaging(p));
            return View(lstString);
        }
        [HttpPost]
        public string SearchAwardByDate(string searchDay)
        {
            string s1 = "", s2 = "", s3 = "", s4 = "", s5 = "", s6 = "";
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            string title = "";
            if (searchDay != "")
            {
                string[] arr = searchDay.Split('/');
                if (arr != null && arr.Length > 0)
                {
                    string d = arr[1];
                    string m = arr[0];
                    string y = arr[2];
                    string fromD = m + "/" + d + "/" + y + " 00:00:00";
                    string toD = m + "/" + d + "/" + y + " 23:59:59";
                    try
                    {
                        fromDate = DateTime.Parse(fromD);
                        toDate = DateTime.Parse(toD);
                    }
                    catch (Exception objEx)
                    {
                        fromD = d + "/" + m + "/" + y + " 00:00:00";
                        toD = d + "/" + m + "/" + y + " 23:59:59";
                        fromDate = DateTime.Parse(fromD);
                        toDate = DateTime.Parse(toD);

                        BBC.CWallet.Share.BBCLoggerManager.Error("SearchAwardByDate -> fromDate = DateTime.Parse: " + objEx);
                    }
                    IEnumerable<AwardMegaball> lstAward = null;
                    if (lstAward == null)
                    {
                        lstAward = awardMegabalServices.GetListAward(1, 100, fromDate, toDate);
                        title = "LOTTERY RESULT DATE " + d + "/" + m + "/" + y;

                        List<string> lstResult = new List<string>();
                        if (lstAward != null && lstAward.Count() > 0)
                        {
                            if (lstAward.ElementAt(0).FirstNumber < 10)
                            {
                                s1 = "0" + lstAward.ElementAt(0).FirstNumber;
                            }
                            else
                            {
                                s1 = lstAward.ElementAt(0).FirstNumber.ToString();
                            }
                            if (lstAward.ElementAt(0).SecondNumber < 10)
                            {
                                s2 = "0" + lstAward.ElementAt(0).SecondNumber;
                            }
                            else
                            {
                                s2 = lstAward.ElementAt(0).SecondNumber.ToString();
                            }
                            if (lstAward.ElementAt(0).ThirdNumber < 10)
                            {
                                s3 = "0" + lstAward.ElementAt(0).ThirdNumber;
                            }
                            else
                            {
                                s3 = lstAward.ElementAt(0).ThirdNumber.ToString();
                            }
                            if (lstAward.ElementAt(0).FourthNumber < 10)
                            {
                                s4 = "0" + lstAward.ElementAt(0).FourthNumber;
                            }
                            else
                            {
                                s4 = lstAward.ElementAt(0).FourthNumber.ToString();
                            }
                            if (lstAward.ElementAt(0).FivethNumber < 10)
                            {
                                s5 = "0" + lstAward.ElementAt(0).FivethNumber;
                            }
                            else
                            {
                                s5 = lstAward.ElementAt(0).FivethNumber.ToString();
                            }
                            if (lstAward.ElementAt(0).ExtraNumber < 10)
                            {
                                s6 = "0" + lstAward.ElementAt(0).ExtraNumber;
                            }
                            else
                            {
                                s6 = lstAward.ElementAt(0).ExtraNumber.ToString();
                            }
                            builder.Append("<li style=\"background-color: #fff; color: #d12f3e; font-size: 1.75rem; font-weight: normal; \">" + s1 + "</li>");
                            builder.Append("<li style=\"background-color: #fff; color: #d12f3e; font-size: 1.75rem; font-weight: normal; \">" + s2 + "</li>");
                            builder.Append("<li style=\"background-color: #fff; color: #d12f3e; font-size: 1.75rem; font-weight: normal; \">" + s3 + "</li>");
                            builder.Append("<li style=\"background-color: #fff; color: #d12f3e; font-size: 1.75rem; font-weight: normal; \">" + s4 + "</li>");
                            builder.Append("<li style=\"background-color: #fff; color: #d12f3e; font-size: 1.75rem; font-weight: normal; \">" + s5 + "</li>");
                            builder.Append("<li style=\"background-color: #f6b559; color: #fff; font-size: 1.75rem; font-weight: normal; \">" + s6 + "</li>");
                        }
                        else
                        {
                            builder.Append("<li style=\"width:100%; border-radius:0px\">No Result</li>");
                        }
                    }
                }
                else
                {
                    builder.Append("<li style=\"width:100%; border-radius:0px\">No Result</li>");
                }
            }
            SearchResultObj obj = new SearchResultObj();
            obj.TitleResult = title;
            obj.ContentResult = builder.ToString();
            string json = JsonConvert.SerializeObject(obj);
            return json;
        }
        [HttpPost]
        public JsonResult GetListAwardOrderByDate12()
        {
            string strhtml = "-1";
            int intPageSize = 10;
            Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
            StringBuilder strBOBj = new StringBuilder();

            //IEnumerable<AwardMegaball> lstAwardMegaball = (IEnumerable<AwardMegaball>)objRedisCache.GetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward);
            IEnumerable<AwardMegaball> lstAwardMegaball = null;
            if (lstAwardMegaball == null)
            {
                lstAwardMegaball = awardMegabalServices.GetListAwardOrderByDate();
                if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                {
                    int i = 0;
                    foreach (AwardMegaball item in lstAwardMegaball)
                    {
                        if (i == 0)
                        {
                            strBOBj.Append("<li style=\"background-color: #fff; color: #d12f3e; font-size: 1.75rem; font-weight: normal\">" + ConvetTwoNumberToString(item.FirstNumber) + "</li>");
                            strBOBj.Append("<li style=\"background-color: #fff; color: #d12f3e; font-size: 1.75rem; font-weight: normal\">" + ConvetTwoNumberToString(item.SecondNumber) + "</li>");
                            strBOBj.Append("<li style=\"background-color: #fff; color: #d12f3e; font-size: 1.75rem; font-weight: normal\">" + ConvetTwoNumberToString(item.ThirdNumber) + "</li>");
                            strBOBj.Append("<li style=\"background-color: #fff; color: #d12f3e; font-size: 1.75rem; font-weight: normal\">" + ConvetTwoNumberToString(item.FourthNumber) + "</li>");
                            strBOBj.Append("<li style=\"background-color: #fff; color: #d12f3e; font-size: 1.75rem; font-weight: normal\">" + ConvetTwoNumberToString(item.FivethNumber) + "</li>");
                            strBOBj.Append("<li style=\"background-color: #f6b559; color: #fff; font-size: 1.75rem; font-weight: normal\">" + ConvetTwoNumberToString(item.ExtraNumber) + "</li>");

                            i++;
                        }
                    }
                    strhtml = strBOBj.ToString();
                    //objRedisCache.SetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward, lstAwardMegaball);
                }
            }
            List<string> lst = new List<string>();
            lst.Add(strhtml);
            return Json(new { content = strhtml });
        }
        private string ConvetTwoNumberToString(int number)
        {
            if (number < 10)
                return "0" + number;
            else
                return number.ToString();
        }
        [HttpPost]
        public string CheckResultNumber(string strBlockHash)
        {
            string strHtml = "";
            strBlockHash = Request.Form["btchash"];
            string response = Request.Form["g-recaptcha-response"];
            string secretKey = ConfigurationManager.AppSettings[KeyManager._SECREC_KEY_CAPTCHA];// "6LfhJyUUAAAAAPKM6Hl87lD0mVKa-0zPKNR53W_j";
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");

            if (status == false)
            {
                strHtml = "errorCaptcha";
            }
            else
            {
                try
                {
                    char[] array = strBlockHash.ToCharArray();
                    if (array.Length > 0)
                    {
                        List<string> lst = new List<string>();
                        string str = "";
                        int k = 0;
                        for (int i = array.Length - 1; i >= 0; i--)
                        {
                            str = str + array[i];
                            k++;
                            if (k % 4 == 0)
                            {
                                string number = ReverseNumber(str);
                                k = 0;
                                lst.Add(number);
                                str = "";
                            }
                        }
                        List<int> lstResultNumber = new List<int>();

                        if (lst != null && lst.Count > 0)
                        {
                            foreach (var item in lst)
                            {
                                int test = Convert.ToInt32(item, 16);
                                if (lstResultNumber.Count < 5)
                                {
                                    test = test % int.Parse(Number49);
                                    if (!lstResultNumber.Contains(test))
                                    {
                                        if (test == 0)
                                        {
                                            test = int.Parse(Number49);
                                        }
                                        lstResultNumber.Add(test);
                                    }

                                }
                                else if (lstResultNumber.Count == 5)
                                {
                                    test = test % int.Parse(Number26);
                                    if (test == 0)
                                    {
                                        test = int.Parse(Number26);
                                    }
                                    lstResultNumber.Add(test);
                                    break;
                                }
                            }
                        }
                        foreach (var item3 in lstResultNumber)
                        {
                            strHtml += "<li>" + item3 + "</li>";
                        }
                    }
                }
                catch(Exception objEx)
                {
                    strHtml = "errorblock";

                    BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> CheckResultNumber: " + objEx.ToJsonString());
                }
            }
            Session["ResultNumber"] = strHtml;
            Response.Redirect("/lotte-result");
            return strHtml;
        }
        [HttpPost]
        public void SetTimeoutSession()
        {
            Session["ResultNumber"] = null;
        }
        private string ReverseNumber(string number)
        {
            string str = "";
            char[] array = number.ToCharArray();
            if (array.Length > 0)
            {
                for (int i = array.Length - 1; i >= 0; i--)
                {
                    str = str + array[i];
                }
            }
            return str;
        }
        public ActionResult PastResult()
        {
            List<string> lst = new List<string>();
            lst.Add(GenHtmlLoterryRusult());
            return View(lst);
        }
        public string GenHtmlLoterryRusult()
        {
            string strHtml = "";
            try
            {
                StringBuilder strBuilder = new StringBuilder();
                string strClass = "";
                //IEnumerable<AwardMegaball> lstAwardMegaball = (IEnumerable<AwardMegaball>)objRedisCache.GetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward);
                IEnumerable<AwardMegaball> lstAwardMegaball = null;
                if (lstAwardMegaball == null)
                {
                    lstAwardMegaball = awardMegabalServices.GetListAwardOrderByDate();
                    //if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                    //{
                    //    objRedisCache.SetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward, lstAwardMegaball);
                    //}
                }
                if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                {
                    int i = 0;
                    foreach (var item in lstAwardMegaball)
                    {
                        if (i % 2 == 0)
                        {
                            strClass = "alt";
                        }
                        else
                        {
                            strClass = "";
                        }
                        strBuilder.Append("<tr class= " + strClass + ">");
                        strBuilder.Append("<td style =\"text-align:center;\"><a data-action=\"flipstar_timeline\" data-drawhash=\"" + item.GenCode + "\" data-drawdate=\"" + String.Format("{0:MM/dd/yyyy}", item.CreateDate) + "\" class=\"timeline-link\">" + item.NumberID + "</a></td>");
                        strBuilder.Append("<td style =\"text-align:center;\">" + string.Format("{0:D}", item.CreateDate) + "</td>");
                        strBuilder.Append("<td style =\"text-align:center;white-space:nowrap;font-weight:bold; \"> Daily </td>");
                        strBuilder.Append("<td style =\"text-align:center;white-space:nowrap;\">");
                        strBuilder.Append("<div class=\"result small free-ball\">" + item.FirstNumber + "</div>");
                        strBuilder.Append("<div class=\"result small free-ball\">" + item.SecondNumber + "</div > ");
                        strBuilder.Append("<div class=\"result small free-ball\">" + item.ThirdNumber + "</div>");
                        strBuilder.Append("<div class=\"result small free-ball\">" + item.FourthNumber + "</div>");
                        strBuilder.Append("<div class=\"result small free-ball\">" + item.FivethNumber + "</div>");
                        strBuilder.Append("<div class=\"result small free-ball-last\">" + item.ExtraNumber + "</div>");
                        strBuilder.Append("</td>");
                        strBuilder.Append("</tr>");
                        i++;
                    }
                }
                strHtml = strBuilder.ToString();
            }
            catch(Exception objEx)
            {
                strHtml = "";

                BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> GenHtmlLoterryRusult: " + objEx.ToJsonString());
            }
            return strHtml;
        }
        public string GenHtmlLoterryRusultHome()
        {
            string strHtml = "";
            try
            {
                StringBuilder strBuilder = new StringBuilder();
                IEnumerable<AwardMegaball> lstAwardMegaball = null;
                //lstAwardMegaball = (IEnumerable<AwardMegaball>)objRedisCache.GetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward);

                if (lstAwardMegaball == null)
                {
                    lstAwardMegaball = awardMegabalServices.GetListAwardOrderByDate();
                    //if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                    //{
                    //    objRedisCache.SetCache<IEnumerable<AwardMegaball>>(Common.KeyListAward, lstAwardMegaball);
                    //}
                }
                if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                {
                    int i = 0;
                    foreach (var item in lstAwardMegaball)
                    {
                        if (i == 5)
                        {
                            break;
                        }
                        DateTime dtmCreatedDate = item.CreateDate.Value;
                        if (i == 0)
                        {
                            strBuilder.Append("<div class=\"resultBox\">");
                            strBuilder.Append("<div class=\"latestHeader free\">");
                            strBuilder.Append("" + dtmCreatedDate.DayOfWeek + "<span class=\"smallerHeading\">" + dtmCreatedDate.Day + "" + suffixes[dtmCreatedDate.Day] + " " + dtmCreatedDate.ToString("MMMM", CultureInfo.InvariantCulture) + " " + dtmCreatedDate.Year + "</span>");
                            strBuilder.Append("</div>");
                            strBuilder.Append("<div class=\"paddedLight_Result\">");
                            strBuilder.Append("<div class=\"calendar floatLeft\">");
                            strBuilder.Append("<div class=\"calMonth\">" + dtmCreatedDate.ToString("MMMM", CultureInfo.InvariantCulture) + "</div>");
                            strBuilder.Append("<div class=\"calDate\">" + dtmCreatedDate.Day + "<sup>" + suffixes[dtmCreatedDate.Day] + "</sup></div>");
                            strBuilder.Append("</div><br>");
                            strBuilder.Append("<div class=\"result free-ball floatLeft\">" + item.FirstNumber + "</div>");
                            strBuilder.Append("<div class=\"result free-ball floatLeft\">" + item.SecondNumber + "</div>");
                            strBuilder.Append("<div class=\"result free-ball floatLeft\">" + item.ThirdNumber + "</div>");
                            strBuilder.Append("<div class=\"result free-ball floatLeft\">" + item.FourthNumber + "</div>");
                            strBuilder.Append("<div class=\"result free-ball floatLeft\">" + item.FivethNumber + "</div>");
                            strBuilder.Append("<div class=\"result free-ball-last floatLeft\">" + item.ExtraNumber + "</div>");
                            strBuilder.Append("</div>");
                            strBuilder.Append("</div>");
                        }
                        strBuilder.Append("<div class=\"resultBox withSide lottoResults\">");
                        strBuilder.Append("<div class=\"sideHeader free floatLeft\" style=\"margin-right: 20px;\">");
                        strBuilder.Append("<div class=\"calendar small centred\">");
                        strBuilder.Append("<span class=\"calDate\">" + dtmCreatedDate.Day + "<sup>" + suffixes[dtmCreatedDate.Day] + "</sup></span>");
                        strBuilder.Append("</div>");
                        strBuilder.Append("Wednesday <br><span class=\"smallerHeading\">" + string.Format("{0:D}", dtmCreatedDate) + "</span>");
                        strBuilder.Append("</div>");
                        strBuilder.Append("<div class=\"padded\">");
                        strBuilder.Append("<div class=\"title\">Daily Draw</div>");
                        strBuilder.Append("<div class=\"result medium free-ball floatLeft\">" + item.FirstNumber + "</div>");
                        strBuilder.Append("<div class=\"result medium free-ball floatLeft\">" + item.SecondNumber + "</div>");
                        strBuilder.Append("<div class=\"result medium free-ball floatLeft\">" + item.ThirdNumber + "</div>");
                        strBuilder.Append("<div class=\"result medium free-ball floatLeft\">" + item.FourthNumber + "</div>");
                        strBuilder.Append("<div class=\"result medium free-ball floatLeft\">" + item.FivethNumber + "</div>");
                        strBuilder.Append("<div class=\"result medium free-ball-last floatLeft\">" + item.ExtraNumber + "</div>");
                        strBuilder.Append("<br style =\"clear: right;\">");
                        strBuilder.Append("<div class=\"floatLeft padded\"><strong></strong></div>");
                        strBuilder.Append("<div class=\"extra\">");
                        strBuilder.Append("<div class=\"draw-no\">Draw Number: <strong style = \"font-size: 16px;\"> " + item.NumberID + " </strong></div >");
                        strBuilder.Append("</div>");
                        strBuilder.Append("</div>");
                        strBuilder.Append("</div>");
                        i++;
                    }
                }
                strHtml = strBuilder.ToString();
            }
            catch (Exception objEx)
            {
                strHtml = "";
                BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> GenHtmlLoterryRusultHome: " + objEx.ToJsonString());

            }
            return strHtml;
        }
        public string GenHtmlLoterryResultAward(int start, int end)
        {
            string strHtml = "";
            try
            {
                StringBuilder strBuilder = new StringBuilder();
                IEnumerable<AwardMegaball> lstAwardMegaball = null;
                string strTotalRows = string.Empty;
                if (lstAwardMegaball == null)
                {
                    lstAwardMegaball = awardMegabalServices.GetListAwardMegaball(start, end);
                }
                if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                {
                    int i = 1;
                    foreach (var item in lstAwardMegaball)
                    {
                        DateTime dtmCreatedDate = DateTime.SpecifyKind(item.CreateDate.Value, DateTimeKind.Utc);
                        string strNumberTicket = item.FirstNumber + "," + item.SecondNumber + "," + item.ThirdNumber + "," + item.FourthNumber + "," + item.FivethNumber;
                        string strExtrarNumber = item.ExtraNumber.ToString();
                        strBuilder.Append("<tr onclick=\"ClickDetail(" + item.NumberID + ")\" class=\"info pointer\" data-toggle=\"modal\" data-target=\"#myModal\">");
                        strBuilder.Append("<td class=\"text-center\">" + item.DrawID + "</td>");
                        strBuilder.Append("<td class=\"text-center\">" + dtmCreatedDate.ToString("MM/dd/yyyy HH:mm:ss UTC") + "</td>");
                        strBuilder.Append("<td class=\"text-center\">");
                        strBuilder.Append("<ul class=\"number-winner\">");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.FirstNumber) + "</li>");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.SecondNumber) + "</li>");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.ThirdNumber) + "</li>");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.FourthNumber) + "</li>");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.FivethNumber) + "</li>");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.ExtraNumber) + "</li>");
                        strBuilder.Append("</ul>");
                        strBuilder.Append("</td>");
                        strBuilder.Append("<td class=\"text-center\">" + item.TotalWon.ToString("G29") + " BTC</td>");
                        strBuilder.Append("<td class=\"text-center\">" + item.JackPot + " BTC</td>");
                        strBuilder.Append("</tr>");
                        i++;
                        strTotalRows = item.TotalRecord.ToString();
                    }
                }
                else
                {
                    strTotalRows = "0";
                    strBuilder.Append("<tr><td colspan=\"5\" class=\"text-center\">No data</td></tr>");
                }

                TempData["TotalRecord"] = int.Parse(strTotalRows);
                strHtml = strBuilder.ToString();
            }
            catch (Exception objEx)
            {
                strHtml = "";

                BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> GenHtmlLoterryResultAward: " + objEx.ToJsonString());
            }
            return strHtml;
        }
        [HttpPost]
        public string GenHtmlModalResult(int intNumberID)
        {
            string strHtml = string.Empty;
            try
            {
                StringBuilder strBuilder = new StringBuilder();
                AwardMegaball objAwardMegaball = new AwardMegaball();
                objAwardMegaball = awardMegabalServices.GetAwardMegaballByID(intNumberID);
                string strNumberTicket = objAwardMegaball.FirstNumber + "," + objAwardMegaball.SecondNumber + "," + objAwardMegaball.ThirdNumber + "," + objAwardMegaball.FourthNumber + "," + objAwardMegaball.FivethNumber;
                string strExtrarNumber = objAwardMegaball.ExtraNumber.ToString();

                DateTime dtmCreatedDate = objAwardMegaball.CreateDate.Value;

                strBuilder.Append("<div class=\"modal-dialog modal-lg\">");
                strBuilder.Append("<div class=\"modal-content\">");
                strBuilder.Append("<div class=\"modal-header\">");
                strBuilder.Append("<h4 class=\"modal-title text-center\">Draw results №" + (objAwardMegaball != null ? objAwardMegaball.DrawID : 0) + " " + (objAwardMegaball != null ? dtmCreatedDate.ToString("MM/dd/yyyy HH:mm") : DateTime.Now.ToString("MM/dd/yyyy HH:mm")) + "</h4>");
                strBuilder.Append("</div>");
                strBuilder.Append("<div class=\"modal-body plr-0\">");
                strBuilder.Append("<div class=\"winning-number\">");
                strBuilder.Append("<div class=\"balls\">");
                strBuilder.Append("<span class=\"ticketNumber\">" + (objAwardMegaball != null ? objAwardMegaball.FirstNumber : 0) + "</span>");
                strBuilder.Append("<span class=\"ticketNumber\">" + (objAwardMegaball != null ? objAwardMegaball.SecondNumber : 0) + "</span>");
                strBuilder.Append("<span class=\"ticketNumber\">" + (objAwardMegaball != null ? objAwardMegaball.ThirdNumber : 0) + "</span>");
                strBuilder.Append("<span class=\"ticketNumber\">" + (objAwardMegaball != null ? objAwardMegaball.FourthNumber : 0) + "</span>");
                strBuilder.Append("<span class=\"ticketNumber\">" + (objAwardMegaball != null ? objAwardMegaball.FivethNumber : 0) + "</span>");
                strBuilder.Append("<span class=\"ticketNumber\">" + (objAwardMegaball != null ? objAwardMegaball.ExtraNumber : 0) + "</span>");
                strBuilder.Append("</div>");
                strBuilder.Append("</div>");
                strBuilder.Append("<div class=\"hash-number\">");
                strBuilder.Append("Hash: " + (objAwardMegaball != null ? objAwardMegaball.GenCode : "000000000000000000000000000000000000000000000000000000000") + "");
                strBuilder.Append("</div>");
                strBuilder.Append("<div class=\"overflow-x-auto\">");
                strBuilder.Append("<table class=\"table-winning-number\">");
                strBuilder.Append("<thead>");
                strBuilder.Append("<th>Guessed numbers</th>");
                strBuilder.Append("<th class=\"text-center\">Number of winners</th>");
                strBuilder.Append("<th class=\"text-center\">Prize</th>");
                strBuilder.Append("<th class=\"text-center\">Amount of payments</th>");
                strBuilder.Append("</thead>");
                strBuilder.Append("<tbody>");
                //IEnumerable<AwardBO> lstAward = (IEnumerable<AwardBO>)objRedisCache.GetCache<IEnumerable<AwardBO>>(Common.KeyListAwardBO);
                IEnumerable<AwardBO> lstAward = null;
                if (lstAward == null)
                {
                    lstAward = awardServices.GetAllListAward();
                    //if (lstAward != null && lstAward.Count() > 0)
                    //{
                    //    objRedisCache.SetCache<IEnumerable<AwardBO>>(Common.KeyListAwardBO, lstAward);
                    //}
                }
                if (lstAward != null && lstAward.Count() > 0)
                {
                    foreach (var item in lstAward)
                    {
                        strBuilder.Append("<tr>");
                        strBuilder.Append("<td>");
                        int totalBalls = 0;
                        if (item.NumberBallNormal > 0)
                        {
                            totalBalls += item.NumberBallNormal;
                        }
                        if (item.NumberBallGold > 0)
                        {
                            totalBalls += item.NumberBallGold;
                        }
                        if (totalBalls > 0)
                        {
                            for (int i = 1; i <= totalBalls; i++)
                            {
                                if (i > (totalBalls - item.NumberBallGold))
                                {
                                    strBuilder.Append("<i class=\"balls-icon orange\"></i>");
                                }
                                else
                                {
                                    strBuilder.Append("<i class=\"balls-icon\"></i>");
                                }
                            }
                        }
                        strBuilder.Append("</td>");
                        strBuilder.Append("<td class=\"text-center\">");
                        strBuilder.Append("" + CountWinner(item.NumberBallNormal, item.NumberBallGold, strNumberTicket, strExtrarNumber, objAwardMegaball.DrawID) + "");
                        strBuilder.Append("</td>");
                        strBuilder.Append("<td class=\"text-center\">" + item.AwardNameEnglish + "</td>");
                        strBuilder.Append("<td class=\"text-center\">" + (item.Priority == 2 ? (CountWinner(item.NumberBallNormal, item.NumberBallGold, strNumberTicket, strExtrarNumber, objAwardMegaball.DrawID) * item.AwardValue).ToString("G29") : CountWinner(item.NumberBallNormal, item.NumberBallGold, strNumberTicket, strExtrarNumber, objAwardMegaball.DrawID) == 0 ? 0.ToString("G29") : objAwardMegaball.JackPot.ToString("G29")) + "</td>");
                        strBuilder.Append("</tr>");
                    }
                }
                strBuilder.Append("</tbody>");
                strBuilder.Append("</table>");
                strBuilder.Append("</div>");
                strBuilder.Append("</div>");
                strBuilder.Append("<div class=\"modal-footer\">");
                strBuilder.Append("<button type = \"button\" class=\"btn btn-danger btn-sm\" data-dismiss=\"modal\">Close</button>");
                strBuilder.Append("</div>");
                strBuilder.Append("</div>");
                strBuilder.Append("</div>");
                strHtml = strBuilder.ToString();
            }
            catch (Exception objEx)
            {
                strHtml = string.Empty;
                WriteErrorLog.WriteLogsToFileText(objEx.Message);

                BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> GenHtmlModalResult: " + objEx.ToJsonString());
            }
            return strHtml;
        }
        public string GenPaging(string p)
        {
            StringBuilder builder = new StringBuilder();
            int pageSize = 5, page = 1;
            try
            {
                if (p != null && p != "")
                {
                    page = int.Parse(p);
                }
            }
            catch(Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> GenPaging -> page = int.Parse(p): " + objEx.ToJsonString());
            }

            try
            {
                pageSize = int.Parse(ConfigurationManager.AppSettings["NumberRecordPage"]);
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> GenPaging -> int.Parse(ConfigurationManager.AppSettings['NumberRecordPage']): " + objEx.ToJsonString());
            }
            string linkPage = "", nextLink = "", previewLink = "", firstLink = "", lastLink = "";
            firstLink = "?p=1";
            if (page <= 1)
            {
                previewLink = "?p=1";
            }
            else
            {
                previewLink = "?p=" + (page - 1).ToString();
            }

            builder.Append("<nav class=\"text-center\">");
            builder.Append("<ul id=\"ulPaging\" class=\"pagination\">");

            int pagerank = 5;
            int next = 10;
            int prev = 1;


            if (TempData["TotalRecord"] != null)
            {
                int totalRecore = (int)TempData["TotalRecord"];
                int totalPage = totalRecore / pageSize;
                int balance = totalRecore % pageSize;
                if (balance != 0)
                {
                    totalPage += 1;
                }
                if (page >= totalPage)
                {
                    nextLink = "?p=" + totalPage.ToString();
                }
                else
                {
                    nextLink = "?p=" + (page + 1).ToString();
                }
                int currentPage = page;
                var m = 1;
                if (totalPage > 10)
                {
                    if (page > pagerank + 1)
                    {
                        next = (page + pagerank) - 1;
                        m = page - pagerank;
                    }
                    if (page <= pagerank)
                    {
                        prev = (page - pagerank);
                        m = 1;
                    }
                    if (next > totalPage)
                    {
                        next = totalPage;
                    }
                    if (prev < 1)
                    {
                        prev = 1;
                    }
                }
                else
                {
                    next = totalPage;
                    prev = 1;
                }
                lastLink = "?p=" + totalPage;
                if (totalPage > 1)
                {
                    if (currentPage > 1)
                    {
                        builder.Append("<li class=\"page-item\">");
                        builder.Append("<a href=\"" + previewLink + "\" class=\"page-link\" aria-label=\"Previous\">");
                        builder.Append("<span aria-hidden=\"true\">");
                        builder.Append("<i class=\"fa fa-angle-double-left\" area-hidden=\"hidden\"></i>");
                        builder.Append("</span>");
                        builder.Append("</a>");
                        builder.Append(" </li>");
                    }
                }

                if (totalPage > 1)
                {

                    for (; m <= next; m++)
                    {
                        linkPage = "?p=" + m;
                        if (m == currentPage)
                        {
                            builder.Append("<li class=\"page-item active\"><a class=\"page-link\">" + m + "</a></li>");
                        }
                        else
                        {
                            builder.Append("<li class=\"page-item\"><a href=\"" + linkPage + "\" class=\"page-link\">" + m + "</a></li>");
                        }
                    }
                }

                if (totalPage > 1)
                {
                    if (currentPage < totalPage)
                    {
                        builder.Append("<li class=\"page-item\">");
                        builder.Append("<a href=\"" + nextLink + "\" class=\"page-link\" aria-label=\"Next\">");
                        builder.Append("<span aria-hidden=\"true\">");
                        builder.Append("<i class=\"fa fa-angle-double-right\" area-hidden=\"hidden\"></i>");
                        builder.Append("</span>");
                        builder.Append("</a>");
                        builder.Append("</li>");
                    }
                }
            }
            builder.Append("</ul>");
            builder.Append("</nav>");
            return builder.ToString();
        }
        private int CountWinner(int intNumberBallNormal, int intNumberBallGold, string strNumberCheck, string strExtraNumber, int intDrawID)
        {
            int intNumberWinner = 0;
            try
            {
                string[] arrNumber = strNumberCheck.Split(',');
                IEnumerable<BookingMegaball> lstBookingMegaball = new List<BookingMegaball>();
                lstBookingMegaball = null;

                if (lstBookingMegaball == null)
                {
                    lstBookingMegaball = bookingMegaballServices.ListAllBookingMegaballByDraw(intDrawID);
                }
                if (lstBookingMegaball != null || lstBookingMegaball.Count() > 0)
                {
                    List<int> lstTicketWinner = new List<int>();
                    foreach (var item in lstBookingMegaball)
                    {
                        int countBallNornal = 0;
                        int countBallGold = 0;
                        var lstTemp = lstTicketWinner.Where(x => x == item.BookingID);
                        if (lstTemp.Count() == 0)
                        {
                            for (int i = 0; i < arrNumber.Length; i++)
                            {
                                if (arrNumber[i].Trim() == item.FirstNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.SecondNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.ThirdNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.FourthNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.FivethNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                            }
                            if (strExtraNumber.Trim() == item.ExtraNumber.ToString())
                            {
                                countBallGold++;
                            }
                            if (intNumberBallGold == countBallGold && intNumberBallNormal == countBallNornal)
                            {
                                intNumberWinner++;
                                lstTicketWinner.Add(item.BookingID);
                            }
                        }
                    }

                }
            }
            catch (Exception objEx)
            {
                intNumberWinner = 0;
                WriteErrorLog.WriteLogsToFileText(objEx.Message);

                BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> CountWinner: " + objEx.ToJsonString());
            }
            return intNumberWinner;
        }
        private decimal SumTotalWon(string strNumberCheck, string strExtraNumber, int intDrawID, decimal jackpotValue)
        {
            decimal totalWon = 0;
            try
            {
                bool flag = false;
                string[] arrNumber = strNumberCheck.Split(',');
                IEnumerable<BookingMegaball> lstBookingMegaball = new List<BookingMegaball>();
                lstBookingMegaball = bookingMegaballServices.ListAllBookingMegaballByDraw(intDrawID);
                if (lstBookingMegaball != null || lstBookingMegaball.Count() > 0)
                {
                    List<int> lstTicketWinner = new List<int>();
                    foreach (var item in lstBookingMegaball)
                    {
                        int countBallNornal = 0;
                        int countBallGold = 0;
                        var lstTemp = lstTicketWinner.Where(x => x == item.BookingID);
                        if (lstTemp.Count() == 0)
                        {
                            for (int i = 0; i < arrNumber.Length; i++)
                            {
                                if (arrNumber[i].Trim() == item.FirstNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.SecondNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.ThirdNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.FourthNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.FivethNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                            }
                            if (strExtraNumber.Trim() == item.ExtraNumber.ToString())
                            {
                                countBallGold++;
                            }

                            IEnumerable<AwardBO> lstAward = (IEnumerable<AwardBO>)objRedisCache.GetCache<IEnumerable<AwardBO>>(Common.KeyListAwardBO);
                            //IEnumerable<AwardBO> lstAward = null;
                            lstAward = awardServices.GetAllListAward();
                            if (lstAward == null)
                            {
                                lstAward = awardServices.GetAllListAward();
                                if (lstAward != null && lstAward.Count() > 0)
                                {
                                    objRedisCache.SetCache<IEnumerable<AwardBO>>(Common.KeyListAwardBO, lstAward);
                                }
                            }

                            if (lstAward != null && lstAward.Count() > 0)
                            {
                                IEnumerable<AwardBO> lstTempAward = lstAward.Where(x => x.NumberBallNormal == countBallNornal && x.NumberBallGold == countBallGold).ToList();
                                if (lstTempAward != null && lstTempAward.Count() > 0)
                                {
                                    foreach (var itemTempAward in lstTempAward)
                                    {
                                        if (itemTempAward.Priority == 2)
                                        {
                                            totalWon += itemTempAward.AwardValue;
                                        }
                                        else
                                        {
                                            if (!flag)
                                            {
                                                totalWon += jackpotValue;
                                                flag = true;
                                            }
                                        }
                                    }
                                    lstTicketWinner.Add(item.BookingID);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                totalWon = 0;
                WriteErrorLog.WriteLogsToFileText(objEx.Message);

                BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> SumTotalWon: " + objEx.ToJsonString());
            }
            return totalWon;
        }
        [HttpPost]
        public string GenHtmlSearch(string fromDate, string toDate, int page)
        {
            if (fromDate == "")
            {
                fromDate = "01/01/" + (DateTime.Now.Year - 1).ToString();
            }
            if (toDate == "")
            {
                toDate = DateTime.Now.ToString("MM/dd/yyyy");
            }
            DateTime? fromD = DateTime.Now;
            DateTime? toD = DateTime.Now;
            StringBuilder strBuilder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int intPageSize = 10;
            int start = 0, end = 5;
            int totalRecord = 0;
            string json = "";
            Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
            try
            {
                if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                    toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                }
                else
                {
                    if (string.IsNullOrEmpty(fromDate))
                    {
                        if (string.IsNullOrEmpty(toDate))
                        {
                            fromD = null;
                            toD = DateTime.Parse("01/01/1990");
                        }
                        else
                        {
                            fromD = DateTime.Parse("01/01/1990");
                            toD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                        }
                    }
                    else
                    {
                        fromD = BBH.Lotte.CLP.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                        toD = DateTime.Now;
                    }
                }
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
                IEnumerable<AwardMegaball> lstAwardMegaball = null;

                string strTotalRows = string.Empty;

                if (lstAwardMegaball == null || lstAwardMegaball.Count() == 0)
                {
                    lstAwardMegaball = awardMegabalServices.GetListAward(start, end, (DateTime)fromD, (DateTime)toD);
                }
                if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                {

                    int i = 1;
                    foreach (var item in lstAwardMegaball)
                    {
                        //decimal jackpotValue = Math.Truncate(item.JackPot / 100) * 100;
                        DateTime dtmCreatedDate = DateTime.SpecifyKind(item.CreateDate.Value, DateTimeKind.Utc);

                        string strNumberTicket = item.FirstNumber + "," + item.SecondNumber + "," + item.ThirdNumber + "," + item.FourthNumber + "," + item.FivethNumber;
                        string strExtrarNumber = item.ExtraNumber.ToString();

                        strBuilder.Append("<tr onclick=\"ClickDetail(" + item.NumberID + ")\" class=\"info pointer\" data-toggle=\"modal\" data-target=\"#myModal\">");
                        strBuilder.Append("<td class=\"text-center\">" + item.DrawID + "</td>");
                        strBuilder.Append("<td class=\"text-center\">" + dtmCreatedDate.ToString("MM/dd/yyyy HH:mm:ss UTC") + "</td>");
                        strBuilder.Append("<td class=\"text-center\">");
                        strBuilder.Append("<ul class=\"number-winner\">");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.FirstNumber) + "</li>");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.SecondNumber) + "</li>");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.ThirdNumber) + "</li>");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.FourthNumber) + "</li>");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.FivethNumber) + "</li>");
                        strBuilder.Append("<li>" + ConvertNumberToString(item.ExtraNumber) + "</li>");
                        strBuilder.Append("</ul>");
                        strBuilder.Append("</td>");
                        strBuilder.Append("<td class=\"text-center\">" + item.TotalWon.ToString("G29") + " BTC</td>");
                        strBuilder.Append("<td class=\"text-center\">" + item.JackPot.ToString("G29") + "</td>");
                        strBuilder.Append("</tr>");
                        i++;
                        strTotalRows = item.TotalRecord.ToString();
                        totalRecord = item.TotalRecord;
                    }

                    TempData["TotalRecord"] = int.Parse(strTotalRows);

                    int totalPage = totalRecord / intPageSize;
                    int balance = totalRecord % intPageSize;
                    if (balance != 0)
                    {
                        totalPage += 1;
                    }
                    if (totalPage > 1)
                    {
                        for (int m = 1; m <= totalPage; m++)
                        {
                            if (m == page)
                            {
                                builderPaging.Append("<li class=\"page-item active\"><a class=\"page-link\">" + m + "</a></li>");
                            }
                            else
                            {
                                builderPaging.Append("<li class=\"page-item\"><a onclick=\"PagingSearchAwardCoin('" + m + "')\" class=\"page-link\">" + m + "</a></li>");

                            }
                        }
                    }
                }
                else
                {
                    strBuilder.Append("<tr><td colspan=\"5\" class=\"text-center\">No Result</td></tr>");
                }
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message);
                strBuilder.Append("<tr><td colspan=\"3\">No Result</td></tr>");

                BBC.CWallet.Share.BBCLoggerManager.Error("AwardController -> GenHtmlSearch: " + objEx.ToJsonString());

            }
            SearchObj obj = new SearchObj();
            obj.ContentResult = strBuilder.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = totalRecord;
            json = JsonConvert.SerializeObject(obj);
            return json;
        }
        private string ConvertNumberToString(int number)
        {
            if (number < 10)
            {
                return "0" + number.ToString();
            }
            return number.ToString();
        }
    }


}
