﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NBitcoin;
using BBH.Lotte.CLP.Shared;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class BalanceRefillController : Controller
    {
        public ActionResult Index()
        {
            int intTimeClosedCountDown = int.Parse(ConfigurationManager.AppSettings["TimeClosedCountDown"]);
            int intTimeOpenCountDown = int.Parse(ConfigurationManager.AppSettings["TimeOpenCountDown"]);
            ViewData["h"] = intTimeClosedCountDown;
            RandomUtils.Random = new UnsecureRandom();
            string strClientExtKey = "xprv9s21ZrQH143K3NuVwYwwRL5L8Qn3Ryn9Y5bmMLLRY37QmJyUd3wMYeSoQvgtGpttbfs3aJmyS3wgvjeHtoB99ywgJoYAvVSa4fKyHzsZDBJ";
            ExtKey masterPubKey = new BitcoinExtKey(strClientExtKey, Network.Main);
            Console.WriteLine(masterPubKey.PrivateKey.PubKey.GetAddress(Network.Main));
            ExtKey pubkey = masterPubKey.Derive(int.Parse(DateTime.Now.Minute.ToString()), hardened: true);
            string wallet = pubkey.PrivateKey.PubKey.GetAddress(Network.Main).ToString();
            ViewBag.Wallet = wallet;
            return View();
        }

    }
}
