﻿using BBH.Lotte.CLP.Shared;
using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.Web.Controllers
{
    public class VerifyController : Controller
    {
        public string KeyCodeSHA = Convert.ToString(ConfigurationManager.AppSettings[KeyManager._KEYCODESHA]);
        public string HostSiteSHA = Convert.ToString(ConfigurationManager.AppSettings[KeyManager._HOSTSITESHA]);
        // GET: Verify
        public ActionResult Index(string strValue)
        {
            string key = KeyCodeSHA;
            string hostVerify = HostSiteSHA;
            string linkAction = strValue.Replace(hostVerify, "");
            linkAction = strValue.Replace("_", "+").Replace("@", "/");
            string linkDecrypt = Utility.DecryptText(linkAction, key);
            string linkVerify = hostVerify + "/Login/UpdateIsActiveMember?" + linkDecrypt;
            Response.Redirect(linkVerify);
            return View();
        }

        public ActionResult ResetPassword(string strValue)
        {
            string key = KeyCodeSHA;
            string hostVerify = HostSiteSHA;
            string linkAction = strValue.Replace(hostVerify, "");
            linkAction = strValue.Replace("_", "+").Replace("@", "/");
            string linkDecrypt = Utility.DecryptText(linkAction, key);
            string linkVerify = hostVerify + "/Member/RessetPassword?" + linkDecrypt;
            Response.Redirect(linkVerify);
            return View();
        }
    }
}
