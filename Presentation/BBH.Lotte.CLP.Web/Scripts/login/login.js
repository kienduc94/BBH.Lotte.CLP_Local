﻿var loginMemberFB = function (strName, strEmail, intGender, strBirthDay, intIsUserType, strUserTypeID) {
    var bolIsError = false;
    var strMessageError = "";

    if (strUserTypeID == null || strUserTypeID.trim().length == 0) {
        if (intIsUserType == 1) {
            //Login FB fails
            alertify.error('Login Facebook failed! Please try again after.');
        }
        else {
            //Login GG fails
            alertify.error('Login Google+ failed! Please try again after.');
        }

        return false;
    }

    if (strEmail == null || strEmail.trim().length == 0) {
        //email is null
        strMessageError += "<p>- Please input email</p>";
        bolIsError = true;
    }
    else if (!isValidEmailAddress(strEmail)) {
        //email valid format
        strMessageError += "<p>- Email is not correct format</p>";
        bolIsError = true;
    }

    if (bolIsError) {
        alertify.error(strMessageError);
        return false;
    }
    else {
        var objMember = {};
        objMember.Email = strEmail;
        objMember.FullName = strName;
        objMember.Gender = (intGender == "male" ? 0 : 1);
        if (typeof strBirthDay != 'undefined') {
            objMember.Birthday = strBirthDay;
        }
        objMember.IsUserType = intIsUserType;
        objMember.UserTypeID = strUserTypeID;

        $.ajax({
            type: "post",
            url: "/Login/LoginSuccess",
            //async: true,
            data: JSON.stringify({ objMember: objMember }),
            contentType: 'application/json',
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (typeof data.isError != 'undefined') {
                    var strTypeMessage = "Facebook";
                    if (intIsUserType == 2) {
                        strTypeMessage = "Google+";
                    }

                    if (data.isError == 0) {
                        alertify.success('Congratulations! Login ' + strTypeMessage + ' succeed.');
                        setTimeout(function () { window.location.href = '/play-game'; hideLoading(); }, 500);
                    }
                    else if (data.isError == 2) {
                        alertify.error('Login ' + strTypeMessage + ' failed! Please try again after.');
                        hideLoading();
                    }
                    else if (data.isError == 3) {
                        
                        $('#login').removeClass('active');
                        $('#update-pass').addClass('active');
                        $('#EmailUpdatePass').val(data.Email.trim());
                        hideLoading();
                    }
                    else if (data.isError == 4) {
                        alert('Register member first fails! Please contact with administrator');
                        hideLoading();
                    }
                    else {
                        alertify.error('Error Login. Please contact with administrator');
                        hideLoading();
                    }
                }
                else {
                    alertify.error('Error Login. Please contact with administrator');
                    hideLoading();
                }
            },
            error: function () {
                alertify.error('Error Login. Please contact with administrator');
                hideLoading();
            }
        });
    }
}
function ChangePassWord() {
    var language = $('#hdLanguage').val();
    var strMessage = "";

    var strPassword = $('#txtNewPassword').val();
    var strRePassword = $('#txtConfirmNewPassword').val();

    if (undefined !== strPassword && (strPassword == '' || strPassword.length < 8)) {
        strMessage += "<p> - Password at least 8 character</p>";
    }
    if (strPassword != strRePassword) {
        strMessage += "<p> - Password confirm wrong</p>";
    }

    if (strMessage.trim().length > 0) {
        alertify.error(strMessage);
        return false;
    }
    else {
        var objMember = "";
        if (typeof $("#divChangePassword") != 'undefined' && $("#divChangePassword").length > 0) {
            objMember = $("#divChangePassword").attr('data-member').trim();
        }

        $.ajax({
            type: "post",
            url: "/Login/UpdatePassMember",
            async: true,
            data: { strPassword: strPassword, objMember: objMember },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                if (d == 'UpdatePassSuccess') {
                    $('#txtNewPassword').val('');
                    $('#txtConfirmNewPassword').val('');
                    alertify.success('Congratulations! You have successfully registered!');
                    goToPlayGame();
                    //showMessageAlert('Congratulations! You have successfully changed.', goToPlayGame)
                }
                else {
                    alertify.error('Update error. Please contact with administrator');
                    hideLoading();
                }
            },
            error: function () {
                alertify.error('An error occurred! Please try again after.');
                hideLoading();
            }
        });
    }
}



var goToPlayGame = function () {
    setTimeout(function () { window.location.href = '/play-game' }, 1500);
}



function statusChangeCallback(response) {

    if (response.status === 'connected' &&
        (typeof $('#hdIsLoginOther') != 'undefined' && $('#hdIsLoginOther').val() == 1)) {
        showLoading();
        FB.api('/me', { locale: 'en_US', fields: 'name, email, gender, birthday' },
            function (response) {
                loginMemberFB(response.name, response.email, response.gender, response.birthday, 1, response.id);//login hệ thống RippleLot bằng thông tin facebook (call xuống db de insert member)
            }
        );
    }

    else if (response.status === 'not_authorized' || (typeof $('#hdIsLoginOther') != 'undefined' && $('#hdIsLoginOther').val() != 1)) {
    }
    else {
    }
}
function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function (googleUser) {
            if (googleUser.isSignedIn() == true) {
                //googleUser.isSignedIn() = true: is signed in
                $.ajax({
                    url: 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + googleUser.Zi.access_token,
                    data: null,
                    success: function (response) {
                        //get user info by token google
                        loginMemberFB(response.name, response.email, response.gender, response.birthday, 2, response.id);//login hệ thống RippleLot bằng thông tin facebook (call xuống db de insert member)
                    },
                    dataType: "jsonp"
                });
            }
            else {
                alertify.error('Login Google+ failed! Please try again after.');
            }
        }, function (error) {
            alertify.error('Login Google+ failed! Please try again after.');
        });
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};
function RemoveTagHtml(str) {
    return str.replace(/<\/?[^>]+(>|$)/g, "");
}
function checkspace(e) {
    var unicode = e.charCode ? e.charCode : e.keyCode
    if (unicode == 32) {
        return false
    }
}
function str_replace(search, replace, str) {
    var ra = replace instanceof Array, sa = str instanceof Array, l = (search = [].concat(search)).length, replace = [].concat(replace), i = (str = [].concat(str)).length;
    while (j = 0, i--)
        while (str[i] = str[i].split(search[j]).join(ra ? replace[j] || "" : replace[0]), ++j < l);
    return sa ? str : str[0];
}

function remove_vietnamese_accents(str) {
    accents_arr = new Array(
        "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
        "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
        "ế", "ệ", "ể", "ễ",
        "ì", "í", "ị", "ỉ", "ĩ",
        "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
        "ờ", "ớ", "ợ", "ở", "ỡ",
        "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
        "ỳ", "ý", "ỵ", "ỷ", "ỹ",
        "đ",
        "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
        "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
        "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
        "Ì", "Í", "Ị", "Ỉ", "Ĩ",
        "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
        "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
        "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
        "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
        "Đ"
    );

    no_accents_arr = new Array(
        "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
        "a", "a", "a", "a", "a", "a",
        "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
        "i", "i", "i", "i", "i",
        "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
        "o", "o", "o", "o", "o",
        "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
        "y", "y", "y", "y", "y",
        "d",
        "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
        "A", "A", "A", "A", "A",
        "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
        "I", "I", "I", "I", "I",
        "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
        "O", "O", "O", "O", "O",
        "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
        "Y", "Y", "Y", "Y", "Y",
        "D"
    );

    return str_replace(accents_arr, no_accents_arr, str);
}
function isDoubleByte(str) {
    accents_arr = new Array(
        "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
        "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
        "ế", "ệ", "ể", "ễ",
        "ì", "í", "ị", "ỉ", "ĩ",
        "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
        "ờ", "ớ", "ợ", "ở", "ỡ",
        "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
        "ỳ", "ý", "ỵ", "ỷ", "ỹ",
        "đ",
        "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
        "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
        "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
        "Ì", "Í", "Ị", "Ỉ", "Ĩ",
        "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
        "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
        "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
        "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
        "Đ"
    );
    for (var i = 0, n = str.length; i < n; i++) {
        if (str.charCodeAt(i) > 255) { return true; }
    }
    for (var j = 0; j < accents_arr.length; j++) {
        if (str.indexOf(accents_arr[j]) != -1) {
            return true;
        }
    }
    if (!CheckSpecialCharacter(str)) {
        return true;
    }
    return false;
}

function isDoubleByteUserName(str) {
    accents_arr = new Array(
        "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
        "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
        "ế", "ệ", "ể", "ễ",
        "ì", "í", "ị", "ỉ", "ĩ",
        "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
        "ờ", "ớ", "ợ", "ở", "ỡ",
        "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
        "ỳ", "ý", "ỵ", "ỷ", "ỹ",
        "đ",
        "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
        "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
        "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
        "Ì", "Í", "Ị", "Ỉ", "Ĩ",
        "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
        "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
        "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
        "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
        "Đ"
    );
    for (var i = 0, n = str.length; i < n; i++) {
        if (str.charCodeAt(i) > 255) { return true; }
    }
    for (var j = 0; j < accents_arr.length; j++) {
        if (str.indexOf(accents_arr[j]) != -1) {
            return true;
        }
    }
    if (!CheckSpecialUserNameCharacter(str)) {
        return true;
    }
    return false;
}

function CheckPassword(str) {
    accents_arr = new Array(
        "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
        "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
        "ế", "ệ", "ể", "ễ",
        "ì", "í", "ị", "ỉ", "ĩ",
        "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
        "ờ", "ớ", "ợ", "ở", "ỡ",
        "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
        "ỳ", "ý", "ỵ", "ỷ", "ỹ",
        "đ",
        "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
        "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
        "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
        "Ì", "Í", "Ị", "Ỉ", "Ĩ",
        "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
        "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
        "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
        "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
        "Đ"
    );
    for (var j = 0; j < accents_arr.length; j++) {
        if (str.indexOf(accents_arr[j]) != -1) {
            return true;
        }
    }
    return false;
}

function CheckSpecialCharacter(value) {
    var checkReg = true;
    var count = 0;
    if (value != "") {
        var characterReg = new Array('@', '$', '%', '^', '&', '*', '(', ')', '{', '}', '#', '~', '|', '*', '!', '~', '-', '=', '+', ',', '/', '?', '\'', '\"', ';', ':', '[', ']', '\\', '_', '`', '>', '<', '`', '.');
        for (var i = 0; i < characterReg.length; i++) {
            if (parseInt(value.indexOf(characterReg[i])) > -1) {
                count++;
            }
        }
        if (count > 0) {
            checkReg = false;
        }
    }
    return checkReg;
}

function CheckSpecialUserNameCharacter(value) {
    var checkReg = true;
    var count = 0;
    if (value != "") {
        var characterReg = new Array('$', '%', '^', '&', '*', '(', ')', '{', '}', '#', '~', '|', '*', '!', '~', '-', '=', '+', ',', '/', '?', '\'', '\"', ';', ':', '[', ']', '\\', '`', '>', '<', '`');
        for (var i = 0; i < characterReg.length; i++) {
            if (parseInt(value.indexOf(characterReg[i])) > -1) {
                count++;
            }
        }
        if (count > 0) {
            checkReg = false;
        }
    }
    return checkReg;
}


function ResetForm(id) {
    switch (id) {
        case 1:
            {
                $('#lbEmail').text('');
                break;
            }
        case 2:
            {
                $('#lbE_Wallet').text('');
                break;
            }
        case 3:
            {
                $('#lbPass').text('');
                break;
            }
    }
}

function LoginMember() {
    var language = $('#hdLanguage').val();
    var urlRedirect = $('#hdRedirect').val();
    var email = $('#txtEmail').val();
    var password = $('#txtPassword').val();
    var checkReg = true;
    var checkPassword = CheckPassword(password);
    if (email == '') {
        checkReg = false;
        $('#lbEmail').text('Please input email');
        $('#lbEmail').css('display', '');
    }
    else {
        if (!isValidEmailAddress(email)) {
            checkReg = false;
            $('#lbEmail').text('Email is not correct format');
            $('#lbEmail').css('display', '');
        }
    }
    if (password == '') {
        checkReg = false;
        $('#lbPass').text('Please input password');
        $('#lbPass').css('display', '');
    }
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            url: "/Login/MemberLogin",
            async: true,
            data: { email: email, password: password },
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (data == 'loginfaile') {
                    alertify.error("Email or password is wrongly");
                }
                else if (data == 'loginSuccess') {
                    window.location.href = '/play-game';
                }
                hideLoading();
            },
            error: function () {
                alertify.error('Error Login. Please contact with administrator');
                hideLoading();
            }
        });
    }
}

function EnterLogin(event, value) {
    var urlRedirect = $('#hdRedirect').val();
    if (event.which == 13 || event.keyCode == 13) {
        var email = $('#txtEmail').val();
        var password = $('#txtPassword').val();
        var checkReg = true;
        var checkPassword = CheckPassword(password);
        if (email == '') {
            checkReg = false;
            if (language == 'EN') {
                $('#lbEmail').text('Please input email');
            }
            else {
                $('#lbEmail').text('Please input email');
            }
        }
        else {
            if (!isValidEmailAddress(email)) {
                checkReg = false;
                if (language == 'EN') {
                    $('#lbEmail').text('Email is not correct format');
                }
                else {
                    $('#lbEmail').text('Email is not valid');
                }
            }
        }
        if (password == '') {
            checkReg = false;
            if (language == 'EN') {
                $('#lbPass').text('Please input password');
            }
            else {
                $('#lbPass').text('Please input password');
            }
        }
        if (!checkReg) {
            return false;
        }
        else {
            $('#imgLoading').css("display", "");
            $.ajax({
                type: "post",
                url: "/Login/MemberLogin",
                async: true,
                data: { email: email, password: password },
                beforeSend: function () {
                    showLoading();
                },
                success: function (d) {
                    hideLoading();
                    if (d == 'loginfaile') {
                        alertify.error("Email or password is wrongly");
                    }
                    else if (d == 'loginSuccess') {
                        if (urlRedirect == '' || urlRedirect == '/') {
                            urlRedirect = "/play-game";
                        }
                        window.location.href = urlRedirect;
                    }
                },
                error: function () {

                }
            });
        }
    }
}