﻿function ResetField(id) {
    if (id == 1) {
        $('#lbPassword').text('');

    }
    if (id == 2) {
        $('#lbRePass').text('');

    }
}
function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}
function onlyNumber(evt) {
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function SentMobile() {
    var mobile = $('#txtMobile').val();
    var email = $('#hdEmail').val();
    var phonecode = $('#cbPhonecode').val();
    var checkReg = true;
    if (mobile == '') {

        $('#lbMobile').css("display", "");
        $('#lbMobile').text('Mobile is required');
        checkReg = false;
    }
    else if (mobile.length < 9) {
        $('#lbMobile').text('Invalid mobile format');
        $('#lbMobile').css("display", "");

        checkReg = false;
    }
    else if (phonecode == '0') {
        $('#lbPhonecode').text('Area code is required');
        $('#lbPhonecode').css("display", "");

        checkReg = false;
    }
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/Member/SentMobile",
            data: { email: email, mobile: mobile, phonecode: phonecode },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (d) {
                hideLoadingNew();
                if (d.isSuccess == 'UpdateSuccess') {
                    //alertify.alert('Sent Success.')
                    //setTimeout(function () { window.location.reload(); }, 2000);
                    $('#txtMobile').val('');
                    $('#input-mobile').modal('hide');
                    $('#get-free-ticket').modal('show');
                }
                else if (d.isSuccess == 'Failse') {
                    alertify.alert('Sent Failse.');
                }
                else if (d.isSuccess == 'GetFreeTicketToRecived') {
                    $('#lbMobile').css("display", "");
                    $('#lbMobile').text('Your account received free ticket!');
                    window.scrollTo(100, 100);
                }
                else if (d.isSuccess == 'FullNumberSents') {
                    alertify.alert('Your mobile sent over the allow times of system');
                }
                else if (d.isSuccess = 'ExitMobile') {
                    $('#lbMobile').css("display", "");
                    $('#lbMobile').text('Mobile is registered');
                }
            },
            error: function (e) {
                alertify.alert("Update Error, Please contact to admin.");
                hideLoadingNew();
            }
        });
    }
}


function GetFreeTicket() {
    var smsCode = $('#txtfreeticket').val();
    var email = $('#hdEmail').val();

    var checkReg = true;
    if (smsCode == '') {

        $('#lbfreeticlet').css("display", "");
        $('#lbfreeticlet').text('Code is required');
        checkReg = false;
    }

    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/Member/GetFreeTicket",
            data: { email: email, smsCode: smsCode },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (data) {
                hideLoadingNew();
                if (!data.FreeTickIsZero) {
                    if (data.isSuccess == 'UpdateSuccess') {
                        //alertify.alert('Get Free Ticket Success.')
                        //setTimeout(function () { window.location.reload(); }, 2000);
                        window.location.reload();
                    }
                    else if (data.isSuccess == 'Failse') {
                        alertify.alert('Sent Failse.');
                    }
                    else if (data.isSuccess == 'incorrect') {
                        $('#lbfreeticlet').css("display", "");
                        $('#lbfreeticlet').text('Code incorrect');
                    }
                    else if (data.isSuccess == 'ExpireDate') {
                        alertify.alert('ExpireTime get code,please contact Admin.')
                    }
                    else if (data.isSuccess == 'Getcodebefore') {
                        alertify.alert('You got the code before.')
                    }
                }
                else {
                    $("#txtfreeticket").val('');
                    $("#get-free-ticket").modal('hide');
                    alertify.alert('Sorry ! The free ticket has been given away today. We apologize for the inconvenience.')
                }
            },
            error: function (e) {
                alertify.alert("Update Error, Please contact to admin.");
                hideLoadingNew();
            }
        });
    }
}



var maxItemNotification = 1;
function ChangePassWord() {
    var language = $('#hdLanguage').val();
    var checkReg = true;
    var password = $('#txtNewPass').val();
    var rePassword = $('#txtRePass').val();
    if (password == '' || password.length < 8) {
        $('#lbPassword').text('Password at least 8 character');

        checkReg = false;
    }
    if (password != rePassword) {
        $('#lbRePass').text('Password confirm wrong');

        checkReg = false;
    }
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            url: "/Member/UpdatePassMember",
            async: true,
            data: { password: password },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (d) {
                if (d == 'UpdatePassSuccess') {
                    $('#txtNewPass').val('');
                    $('#txtRePass').val('');
                    showMessageAlert('Congratulations! You have successfully updated.', backToPagePrev)

                }

                else {

                    alertify.error('Update error. Please contact with administrator');

                }
                hideLoadingNew();
            },
            error: function () {
                alertify.error('An error occurred! Please try again after.');
                hideLoadingNew();
            }
        });
    }
}

function backToPagePrev() {
    setTimeout(function () { window.history.back() }, 1000);
}

function ShowBookingDetail(bookingID, transactionCode, valuePoints, firstNumber, secondNumber, thirdNumber, fourthNumber, fivethNumber, extraNumber, quantity, bookingDate, openDate, statusName, status) {
    if (typeof $('#standardModal') != 'undefined' && $('#standardModal').length > 0) {
        $('#standardModal').show();
    }
    // $('#standardModal').css("display", "block");
    $('#hdBookingID').val(bookingID);
    $('#hdQuantity').val(quantity);
    if (status == '0') {
        $('#2lvlModal-learnMore').css('display', '');

    }
    else {
        $('#2lvlModal-learnMore').css('display', 'none');
    }
    var strTicket = '<span class="ticketNumber">' + firstNumber + '</span>&nbsp;<span class="ticketNumber">' + secondNumber + '</span>&nbsp;<span class="ticketNumber">' + thirdNumber + '</span>&nbsp;<span class="ticketNumber">' + fourthNumber + '</span>&nbsp;<span class="ticketNumber">' + fivethNumber + '</span>&nbsp;<span class="ticketNumber oneMore">' + extraNumber + '</span>';
    $('#txtBookingID').val(transactionCode);
    $('#txtPoints').val(valuePoints);
    $('#spanTicket').html(strTicket);
    $('#txtQuantity').val(quantity);
    $('#txtBookingDate').val(bookingDate);
    $('#txtOpenDate').val(openDate);
    //$('#txtStatus').val(statusName);
}

function ShowTransactionPackageDetail(transactionCode, ewallet, packageName, bookingDate, expireDate, statusName) {
    //$('#txtBookingID').val(bookingID);
    $('#standardModal').css("display", "block");
    $('#txtEwallet').val(ewallet);
    $('#txtPackageName').val(packageName);
    $('#txtBookingDate').val(bookingDate);
    $('#txtExpireDate').val(expireDate);
    $('#txtStatus').val(statusName);
    $('#txtTransaction').val(transactionCode);
}

function ShowTransactionPointDetail(transactionCode, ewallet, points, bookingDate, statusName) {
    $('#standardModal').css("display", "block");
    $('#txtEwallet').val(ewallet);
    $('#txtPoints').val(points);
    $('#txtBookingDate').val(bookingDate);
    $('#txtStatus').val(statusName);
    $('#txtTransaction').val(transactionCode);
}

function ConfirmBooking() {
    var bookingID = $('#hdBookingID').val();
    var quantity = $('#hdQuantity').val();
    $('#imgLoadingBooking').css("display", "");
    $.ajax({
        type: "post",
        url: "/Member/ConfirmBooking",
        async: true,
        data: { bookingID: bookingID, quantity: quantity },
        beforeSend: function () {
            showLoadingNew();
        },
        success: function (d) {
            hideLoadingNew();
            if (d == 'BookingSuccess') {

                alertify.alert('Buy Success.')
                setTimeout(function () { window.location.reload(); }, 3000);
            }

            else {
                alertify.error('Buy error. Please contact with administrator');
            }

        },
        error: function () {

        }
    });
}


///Edited date: 19.01.2018
///Description:  $('#standardModal').hide();
function CloseModal() {
    if (typeof $('#standardModal') != 'undefined' && $('#standardModal').length > 0) {
        $('#standardModal').hide();
    }
    $('#standardModal').css("display", "none");
    $('.modal-backdrop.fade.show').css('opacity', '0');
    $('.modal-backdrop.fade.show').css('display', 'none');
}

function SearchBooking() {
    //var page = 1;
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var fromYears = fromDate.split('/')[2];
    var toYears = toDate.split('/')[2];
    var fromMounth = fromDate.split('/')[0];
    var toMounth = toDate.split('/')[0];

    if (fromDate == "" && toDate == "") {
        alertify.error("Please input From date or To date!");
        checkMaxShowAlertNoti(maxItemNotification);
    }
    else {
        if (toDate.trim().length == 0) {
            toDate = Date.toDate;
        }
        var page = 1;
        if (fromDate > toDate && fromMounth > toMounth && fromYears > toYears) {
            alertify.error("From date less than or equal To date");
            checkMaxShowAlertNoti(maxItemNotification);
        }
        $.ajax({
            type: "get",
            async: true,
            url: "/Member/SearchTransactionBooking",
            data: { fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (d) {

                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListBooking').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }
                hideLoadingNew();
            },
            error: function (e) {
                alertify.error('An error occurred! Please try again after.');
                checkMaxShowAlertNoti(maxItemNotification);
                hideLoadingNew();
            }
        });
    }
}

function PagingSearchBooking(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var fromYears = fromDate.split('/')[2];
    var toYears = toDate.split('/')[2];
    var fromMounth = fromDate.split('/')[0];
    var toMounth = toDate.split('/')[0];
    if (fromDate == '' && toDate == '') {
        alertify.alert("Please input From date or To date!");
        checkMaxShowAlertNoti(maxItemNotification);
    }
    else {
        if (toDate.trim().length == 0) {
            toDate = Date.toDate;
        }
        //var page = 1;
        if (fromDate > toDate && fromMounth > toMounth && fromYears > toYears) {
            alertify.error("From date less than or equal To date");
            checkMaxShowAlertNoti(maxItemNotification);
        }
        $.ajax({
            type: "get",
            async: true,
            url: "/Member/SearchTransactionBooking",
            data: { fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (d) {
                hideLoadingNew();
                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListBooking').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }

            },
            error: function (e) {

            }
        });
    }
}


function SearchPoint() {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();

    var page = 1;
    if (fromDate == '' && toDate == '') {
        alertify.error("Please input From date or To date!");
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/Member/SearchTransactionPoint",
            data: { fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (d) {
                hideLoadingNew();

                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListPoint').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }
            },
            error: function (e) {

            }
        });
    }
}

function PagingSearchPoint(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    if (fromDate == '' && toDate == '') {
        alertify.error("Please input From date or To date!");
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/Member/SearchTransactionPoint",
            data: { fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (d) {
                hideLoadingNew();

                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListPoint').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }
            },
            error: function (e) {

            }
        });
    }
}


function SearchPackage() {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();

    var page = 1;
    if (fromDate == '' && toDate == '') {
        alertify.error("Please input From date or To date!");
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/Member/SearchTransactionPackage",
            data: { fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (d) {
                hideLoadingNew();

                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListPackage').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }
            },
            error: function (e) {

            }
        });
    }
}


function PagingSearchPackage(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    if (fromDate == '' && toDate == '') {
        alertify.error("Please input From date or To date!");
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/Member/SearchTransactionPackage",
            data: { fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (d) {
                hideLoadingNew();

                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListPackage').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }
            },
            error: function (e) {

            }
        });
    }
}

function SearchBookingWinnig() {
    //var page = 1;
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var fromMounth = fromDate.split('/')[0];
    var toMounth = toDate.split('/')[0];
    var fromYears = fromDate.split('/')[2];
    var toYears = toDate.split('/')[2];

    if (fromDate == "" && toDate == "") {
        alertify.error("Please input From date or To date!");
        checkMaxShowAlertNoti(maxItemNotification);
    }
    else {
        if (toDate.trim().length == 0) {
            toDate = Date.toDate;
        }
        var page = 1;
        if (fromDate > toDate && fromMounth > toMounth && fromYears > toYears) {
            alertify.error("From date less than or equal To date");
            checkMaxShowAlertNoti(maxItemNotification);
        }
        $.ajax({
            type: "get",
            async: true,
            //url: "/Member/SearchTransactionBookingWinning",
            url: "/Member/GenHtmlSearchTicketWinning",
            data: { fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (d) {
                hideLoadingNew();
                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListBookingWining').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }
                hideLoadingNew();
            },
            error: function (e) {
                alertify.error('An error occurred! Please try again after.');
                checkMaxShowAlertNoti(maxItemNotification);
                hideLoadingNew();
            }
        });
    }
}
function SearchTransactionBookingWinning(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var fromMounth = fromDate.split('/')[0];
    var toMounth = toDate.split('/')[0];
    var fromYears = fromDate.split('/')[2];
    var toYears = toDate.split('/')[2];

    if (fromDate == '' && toDate == '') {
        alertify.alert("Please input From date or To date!");
        checkMaxShowAlertNoti(maxItemNotification);
    }
    else {
        if (toDate.trim().length == 0) {
            toDate = Date.toDate;
        }
        //var page = 1;
        if (fromDate > toDate && fromMounth > toMounth && fromYears > toYears) {
            alertify.error("From date less than or equal To date");
            checkMaxShowAlertNoti(maxItemNotification);
        }
        $.ajax({
            type: "get",
            async: true,
            url: "/Member/GenHtmlSearchTicketWinning",
            data: { fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (d) {
                hideLoadingNew();
                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListBookingWining').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }

            },
            error: function (e) {

            }
        });
    }
}
$('[data-dismiss=modal]').on('click', function (e) {
    var $t = $(this),
        target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

    $(target)
        .find("input,textarea")
        .val('')
        .end()
        .find("input[type=checkbox], input[type=radio]")
        .prop("checked", "")
        .end();
})
