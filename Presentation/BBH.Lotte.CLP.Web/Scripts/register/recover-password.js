﻿$(document).ready(function () {
    $(function () {
        var $formRecoverPassword = $('#frRecoverPassword');
        $('#registration-login').change(
            function () {
                $formRecoverPassword.removeData('validator');
                $formRecoverPassword.removeData('unobtrusiveValidation');
                $.validator.unobtrusive.parse($formRecoverPassword);
            });
        $formRecoverPassword.unbind('submit').submit(function () {
            if ($formRecoverPassword.valid()) {
                $.ajax({
                    url: "/Member/SubmitRecoverPassword",
                    async: true,
                    type: 'POST',
                    data: $(this).serialize(),
                    beforeSend: function (xhr, opts) {
                        showLoadingNew();
                    },
                    complete: function () {
                    },
                    success: function (data) {
                        if (data.exists) {
                            if (data.success) {
                                alertify.success('You have successfully recover your password. Please check your email.');
                                hideLoadingNew();
                                setTimeout(function () {
                                    window.location.href = '/play-game';
                                }, 3000);
                            }
                            else {
                                if (data.ismaxsendmail != 0) {
                                    alertify.error('Max of send your password: ' + data.ismaxsendmail + ' times/day.');
                                }
                                else {
                                    alertify.error('Recover your password fails! Please try again.');
                                }
                                hideLoadingNew();
                            }
                        }
                        else {
                            alertify.error('Email does not exist in the system.');
                            hideLoadingNew();
                        }
                    },
                    error: function (data) {
                        alertify.error('An error occurred! Please try again after.');
                        hideLoadingNew();
                    }
                });
            }
            return false;
        });
    });
});