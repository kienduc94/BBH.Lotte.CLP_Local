﻿$(document).ready(function () {
    chooseTickets();
})
function ListMemberByTicket() {
    $.ajax({
        type: "get",
        url: "/Game/GetListMemberBuyTicket",
        async: true,
        beforeSend: function () {
            showLoadingNew();
        },
        success: function (d) {
            $('#tbListMember').html(d);
            hideLoadingNew();
        }
    });
}

function RandomNumberTicket() {
    $('.profile-ticket__item').find('.box-ticket').find('.box-ticket__body').find('ul:eq(0)').find('li').each(function (e) {
        var num = 0; var count = 0;
        $(this).on('click', function (c) {
            var checkActive = $(this).hasClass('active');
            var idParent = $(this).parent().parent().parent().parent().attr('id');
            num = idParent.replace('divTicket_', '');
            var countNumber = 0; var arrNumber = new Array();

            $('#' + idParent).find('.box-ticket').find('.box-ticket__body').find('ul').find('li').each(function (b) {

                if ($(this).hasClass('active')) {
                    countNumber++;
                    var value = $(this).text();
                    arrNumber.push(value);
                }
                if (countNumber == 0) {
                    countNumber++;
                    var value = c.toElement.innerText;
                    arrNumber.push(value);
                }

                if (countNumber > 0) {
                    if (checkActive) {
                        var pos = -1;
                        for (var m = 0; m < arrNumber.length; m++) {
                            if ($(this).text() == arrNumber[m]) {
                                pos = m;
                                break;
                            }
                        }
                        arrNumber.splice(pos, 1);
                    }
                }
            });
            if (countNumber == 5) {
                var strTicketNumber = '';
                if (arrNumber != null && arrNumber.length > 0) {
                    for (var i = arrNumber.length - 1; i >= 0; i--) {
                        strTicketNumber += "<span style='border-radius: 100%;background-color: #fff; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: #d12f3e; margin-right:2px'>" + arrNumber[i] + "</span>&nbsp";
                    }
                }
                var strTicket = '<tr id="trTicket_' + num + '">'
                    + '<td id="tdTicket_' + num + '" style="color: #000; width:80%; font-size: 13px; font-weight:bold">' + strTicketNumber + '</td>'
                    + '<td style="color: #fff; width:20%; font-size: 13px; font-weight:bold; text-align:left">1</td>'
                    + '</tr>';
                if ($('#trTicket_' + num).length == 0) {
                    $('#tbListTicket').append(strTicket);
                }
                else {
                    $('#tdTicket_' + num).html(strTicketNumber);
                }
            }
        })
    })
    $('.profile-ticket__item').find('.box-ticket').find('.box-ticket__body').find('ul:eq(1)').find('li').each(function (e) {
        var num = 0; var count = 0;
        $(this).on('click', function (c) {
            var idParent = $(this).parent().parent().parent().parent().attr('id');

            num = idParent.replace('divTicket_', '');

            $('#ulExtra_' + num).find('li').each(function (v) {
                if ($(this) != $(c)) {
                    $(this).removeClass('active');
                }

            })
            setTimeout(function () {

                $('#spanExtra_' + num).remove();
                var value = c.toElement.innerText;

                var strTicketNumber = '<span id=\"spanExtra_' + num + "\" style=\"border-radius: 100%;background-color: #df9942; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: #d12f3e; margin-right:2px\">" + value + "</span>";
                $('#tdTicket_' + num).append(strTicketNumber);
            }, 100);
            $(c).addClass('active');
        })
    });

}

function UpdateClickTicketByID(id) {

    $('#divTicket_' + id).find('.box-ticket').find('.box-ticket__body').find('ul:eq(0)').find('li').each(function (e) {
        var num = 0; var count = 0;
        $(this).on('click', function (c) {
            var t = $(this);
            var checkActive = $(this).hasClass('active');
            var idParent = $(this).parent().parent().parent().parent().attr('id');
            num = idParent.replace('divTicket_', '');

            var countNumber = 0; var arrNumber = new Array();
            $('#' + idParent).find('.box-ticket').find('.box-ticket__body').find('ul').find('li').each(function (b) {
                if ($(this).hasClass('active')) {
                    countNumber++;
                    var value = $(this).text();
                    arrNumber.push(value);
                }

                if (countNumber == 0) {
                    countNumber++;
                    var value = c.toElement.innerText;
                    arrNumber.push(value);

                }

                if (countNumber > 0) {
                    if (checkActive) {
                        var pos = -1;
                        for (var m = 0; m < arrNumber.length; m++) {
                            if ($(this).text() == arrNumber[m]) {
                                pos = m;
                                break;
                            }
                        }
                        arrNumber.splice(pos, 1);
                    }
                }
            })

            if (countNumber == 5) {
                var strTicketNumber = '';
                if (arrNumber != null && arrNumber.length > 0) {
                    for (var i = arrNumber.length - 1; i >= 0; i--) {
                        strTicketNumber += "<span style='border-radius: 100%;background-color: #fff; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: #d12f3e; margin-right:2px'>" + arrNumber[i] + "</span>&nbsp";
                    }
                }
                var strTicket = '<tr id="trTicket_' + num + '">'
                    + '<td id="tdTicket_' + num + '" style="color: #000; width:80%; font-size: 13px; font-weight:bold">' + strTicketNumber + '</td>'
                    + '<td style="color: #fff; width:20%; font-size: 13px; font-weight:bold; text-align:left">1</td>'
                    + '</tr>';
                if ($('#trTicket_' + num).length == 0) {
                    $('#tbListTicket').append(strTicket);
                }
                else {
                    $('#tdTicket_' + num).html(strTicketNumber);
                }
            }
            t.addClass('active');
        })

    })
}

function RandomeTicket(number) {
    setTimeout(function () {
        var countNumber = 0; var arrNumber = new Array();
        $('#divTicket_' + number).find('.box-ticket').find('.box-ticket__body').find('ul').find('li').each(function (b) {
            if ($(this).hasClass('active')) {
                var value = $(this).text();
                arrNumber.push(value);
            }
        })
        var strTicketNumber = '';
        if (arrNumber != null && arrNumber.length > 0) {

            for (var i = 0; i < arrNumber.length; i++) {
                if (i < arrNumber.length - 1) {
                    strTicketNumber += "<span style='border-radius: 100%;background-color: #fff; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: #d12f3e; margin-right:2px'>" + arrNumber[i] + "</span>&nbsp";
                }
                else {
                    strTicketNumber += "<span id='spanExtra_1' style='border-radius: 100%;background-color: #df9942; height: 25px;position: relative;float: left;text-align:center; width: 25px;color: #d12f3e; margin-right:2px'>" + arrNumber[i] + "</span>&nbsp";
                }

            }
        }
        var strTicket = '<tr id="trTicket_' + number + '">'
            + '<td id="tdTicket_' + number + '" style="color: #000; width:80%; font-size: 13px; font-weight:bold">' + strTicketNumber + '</td>'
            + '<td style="color: #fff; width:20%; font-size: 13px; font-weight:bold; text-align:center">1</td>'
            + '</tr>';
        if ($('#trTicket_' + number).length == 0) {
            $('#tbListTicket').append(strTicket);
        }
        else {
            $('#tdTicket_' + number).html(strTicketNumber);
        }
    }, 1000);
}

function ClearTicket(num) {
    var trID = '#trTicket_' + num;
    $(trID).remove();
}

function AddMoreTicketMegaball() {
    var k = Math.floor(Math.random() * 99999) + 10000;

    var strTicket = ' <div class="profile-ticket__item " id="divTicket_' + k + '" data-id="' + k + '">'
        + '<div class="box-ticket">'
        + '<div class="box-ticket__header">'
        + "<a id=\"autoBox_" + k + "\" href=\"javascript:void(0)\" onclick=\"autoProfileAnimated(" + k + ",$(this).closest('.profile-ticket__item'));\" class=\"btn btn-info btn-sm mr-4\">Auto</a>"
        + "<a href=\"javascript:void(0)\" onclick=\"clearTicketIsActived(this," + k + ");\" class=\"btn btn-info btn-sm ticket-header__btn-clear js-ticket-clear\">Clear</a>"
        + "<a onclick=\"closeTicket(this," + k + ");\" class=\"pull-right btn btn-danger btn-round btn-fab btn-fab-mini ticket-header__btn-del js-ticket-hidden\"><i class=\"material-icons\">close</i></a>"
        + '</div>'
        + '<div class="box-ticket__body">'
        + '<p class="text-cen-up">Select 5 numbers</p>'
        + '<ul class="num-list first-49" data-max="5">'
        + '<li class="num-list__item">1</li>'
        + '<li class="num-list__item">2</li>'
        + '<li class="num-list__item">3</li>'
        + '<li class="num-list__item">4</li>'
        + '<li class="num-list__item">5</li>'
        + '<li class="num-list__item">6</li>'
        + '<li class="num-list__item">7</li>'
        + '<li class="num-list__item">8</li>'
        + '<li class="num-list__item">9</li>'
        + '<li class="num-list__item">10</li>'
        + '<li class="num-list__item">11</li>'
        + '<li class="num-list__item">12</li>'
        + '<li class="num-list__item">13</li>'
        + '<li class="num-list__item">14</li>'
        + '<li class="num-list__item">15</li>'
        + '<li class="num-list__item">16</li>'
        + '<li class="num-list__item">17</li>'
        + '<li class="num-list__item">18</li>'
        + '<li class="num-list__item">19</li>'
        + '<li class="num-list__item">20</li>'
        + '<li class="num-list__item">21</li>'
        + '<li class="num-list__item">22</li>'
        + '<li class="num-list__item">23</li>'
        + '<li class="num-list__item">24</li>'
        + '<li class="num-list__item">25</li>'
        + '<li class="num-list__item">26</li>'
        + '<li class="num-list__item">27</li>'
        + '<li class="num-list__item">28</li>'
        + '<li class="num-list__item">29</li>'
        + '<li class="num-list__item">30</li>'
        + '<li class="num-list__item">31</li>'
        + '<li class="num-list__item">32</li>'
        + '<li class="num-list__item">33</li>'
        + '<li class="num-list__item">34</li>'
        + '<li class="num-list__item">35</li>'
        + '<li class="num-list__item">36</li>'
        + '<li class="num-list__item">37</li>'
        + '<li class="num-list__item">38</li>'
        + '<li class="num-list__item">39</li>'
        + '</ul>'
        + '<p class="text-cen-up">And one more</p>'
        + '<ul class="num-list num-list_yellow" id="ulExtra_' + k + '" data-max="1">'
        + '<li class="num-list__item">1</li>'
        + '<li class="num-list__item">2</li>'
        + '<li class="num-list__item">3</li>'
        + '<li class="num-list__item">4</li>'
        + '<li class="num-list__item">5</li>'
        + '<li class="num-list__item">6</li>'
        + '<li class="num-list__item">7</li>'
        + '<li class="num-list__item">8</li>'
        + '<li class="num-list__item">9</li>'
        + '<li class="num-list__item">10</li>'
        + '<li class="num-list__item">11</li>'
        + '<li class="num-list__item">12</li>'
        + '<li class="num-list__item">13</li>'
        + '<li class="num-list__item">14</li>'
        + '<li class="num-list__item">15</li>'
        + '<li class="num-list__item">16</li>'
        + '<li class="num-list__item">17</li>'
        + '<li class="num-list__item">18</li>'
        + '</ul>'
        + '</div>'
        + '</div>';
    $('#divBoxTicket').append(strTicket);
    chooseTickets();
}

function SubmitTicket_CloseTime(intTimeOpened) {
    alertify.alert("Close Time, please again after " + intTimeOpened + " o'clock.");
}
function SubmitTicket() {
    var createdate = Date.now;
    var totalquantity = 0;
    var lstTicket = '';
    var checkReg = true;
    var num = 0;
    if (lstTickets.length == 0 || !checkList()) {
        alertify.error('Please, fill out your tickets!');
        checkMaxShowAlertNoti(maxItemNotification);
        checkReg = false;
    }
    if (lstTickets.length > 0) {
        totalquantity = lstTickets.length;
        for (var i = 0; i < lstTickets.length; i++) {
            if (lstTickets[i].arrNumber.length > 0) {
                for (var j = 0; j < lstTickets[i].arrNumber.length; j++) {
                    lstTicket += lstTickets[i].arrNumber[j] + ";";
                }
            }
            lstTicket += ',';
        }
    }
    if (!checkReg) {
        return false;
    }
    else {
        var intNumberMegaballID = 0;
        //if (typeof $('#hdNumberMegaballID') != 'undefined' && $('#hdNumberMegaballID').length > 0) {
        //    intNumberMegaballID = $('#hdNumberMegaballID').val();
        //}
        var dblJackPot = 0;
        //if (typeof $('#hdJackPot') != 'undefined' && $('#hdJackPot').length > 0) {
        //    dblJackPot = $('#hdJackPot').val();
        //}
        $.ajax({
            type: "post",
            url: "/Booking/BookingListTicketMember",
            async: true,
            data: { lstTicket: lstTicket, intNumberMegaballID: intNumberMegaballID, dblJackPot: dblJackPot },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (data) {
                if (!data.isRedirect) {
                    if (typeof data.isError != 'undefined') {
                        if (data.isError == 0) {
                            alertify.success('Buy tickets successfully.');
                            checkMaxShowAlertNoti(maxItemNotification);
                            setTimeout(function () {
                                location.reload();
                            }, 1500);
                        }
                        else if (data.isError == 1) {
                            if (typeof data.intTimeOpened != 'undefined') {
                                showMessageAlert('The system is being the lottery. Please come back after' + data.intTimeOpened + ':00:00', null)
                            }
                            else {
                                alertify.error('An error occurred! Please try again after.');
                                checkMaxShowAlertNoti(maxItemNotification);
                            }
                        }
                        else if (data.isError == 2) {
                            window.location.href = "/login?redirect=";
                        }
                        else if (data.isError == 3) {
                            var PriceTicket = $('#hdPriceTicket').val();
                            var PointBTC = $('#hdPointBTC').val();
                            var FreeTicket = $('#hdFreeTicketBTC').val();
                            alertify.alert('Your points are not enough to buy <span style="color:red;">' + totalquantity + ' tickets</span>!<br/> Please input points to buy!');
                            checkMaxShowAlertNoti(maxItemNotification);
                        }
                        else if (data.isError == 4) {
                            alertify.error('An error occurred! Please try again after.');
                            checkMaxShowAlertNoti(maxItemNotification);
                        }
                        else if (data.isError == 5) {
                            alertify.error('Please, fill out your tickets!!');
                            checkMaxShowAlertNoti(maxItemNotification);
                        }
                    }
                    else {
                        alertify.error('An error occurred! Please try again after.');
                        checkMaxShowAlertNoti(maxItemNotification);
                    }
                }
                else {
                    if (data.redirectUrl == window.location.pathname) {

                        alertify.confirm('Confirm Title', 'Please, login to buy tickets!', function () { $("#registration-login").modal(); }
                            , function () { });
                    }
                    else {
                        window.location.href = data.redirectUrl;
                    }
                }
                hideLoadingNew();
            },
            error: function () {
                alertify.error('An error occurred! Please try again after.');
                checkMaxShowAlertNoti(maxItemNotification);
                hideLoadingNew();
            }
        });
    }
    return false;
}

function GetListAwardOrderByDate1() {
    $.ajax({
        type: "post",
        url: "/Game/GetListAwardOrderByDate1",
        async: true,
        beforeSend: function () {
        },
        success: function (d) {
            if (d == '-1') {
                $("#ulLotteryResult").html('');
            }
            else {
                $("#ulLotteryResult").html(d.content);
            }
        },
        error: function () {
            $("#ulLotteryResult").html('');
        }
    });
}

var lstTickets = [];//list tickets
var arrTicketTemp = [];//array ticket temp (Auto Random)
var addTicketToList = function (id, intNumberNew, intIndex) {
    if (lstTickets != null) {
        intNumberNew = parseInt(intNumberNew);

        var lst = $.grep(lstTickets, function (n) {
            return n.ID == id;
        });

        if (lst == null || lst.length == 0) {
            var obj = {};
            obj.ID = id;
            obj.arrNumber = new Array();
            if (intIndex > 0) {
                for (var i = 0; i < (intIndex - 1); i++) {
                    obj.arrNumber.push(0);
                }
            }
            obj.arrNumber.push(intNumberNew);
            lstTickets.push(obj);


        }
        else {
            if (lst[0].arrNumber == null || lst[0].arrNumber.length == 0) {
                lst[0].arrNumber = new Array();
                lst[0].arrNumber.push(intNumberNew);
            }
            else {
                var intFlag = 0;
                for (var i = 0; i < lst[0].arrNumber.length; i++) {
                    if (lst[0].arrNumber[i] == 0 && (intIndex < 0 || i == (intIndex - 1))) {
                        lst[0].arrNumber[i] = intNumberNew;
                        intFlag = 1;
                        break;
                    }
                }

                if (intFlag == 0) {
                    if (lst[0].arrNumber.length < intIndex) {
                        var intTotalTicketCur = intIndex - lst[0].arrNumber.length - 1;
                        for (var i = 0; i < intTotalTicketCur; i++) {
                            lst[0].arrNumber.push(0);
                        }
                    }
                    lst[0].arrNumber.push(intNumberNew);
                }
            }
        }
    }
}

var removeNumberTicket = function (id, intNumberCur, intIndex) {
    if (lstTickets != null && lstTickets.length > 0) {
        if (intIndex == -1) {
            var j = -1;
            for (var i = 0; i < lstTickets.length; i++) {
                if (lstTickets[i].ID == id) {
                    for (var k = 0; k < lstTickets[i].arrNumber.length; k++) {
                        if (lstTickets[i].arrNumber[k] == intNumberCur) {
                            j = k;
                            break;
                        }

                    }
                    lstTickets[i].arrNumber.splice(j, 1, 0);
                }
            }
        }
        else {
            if (intNumberCur != 0) {
                for (var i = 0; i < lstTickets.length; i++) {
                    if (lstTickets[i].ID == id) {
                        intIndex = intIndex - 1;
                        lstTickets[i].arrNumber.splice(intIndex, 1);
                        break;
                    }
                }
            }
        }
    }
}
var clearListNumberTicket = function (id) {
    for (var i = 0; i < lstTickets.length; i++) {
        if (lstTickets[i].ID == id) {
            lstTickets.splice(i, 1);
            break;
        }
    }
}

var chooseTickets = function () {
    if (typeof $('.box-ticket__body .num-list.first-49 li') != 'undefined' && $('.box-ticket__body .num-list.first-49 li').length > 0) {
        $('.box-ticket__body .num-list.first-49 li').unbind().click(function () {
            var _this = $(this);
            var _intMaxCount = getMaxTicket(_this.parent());
            var _countIsActived = _this.parent().find('li.active').length;
            var _intID = (typeof _this.parent().parent().parent().parent().attr('data-id') != 'undefined' ? _this.parent().parent().parent().parent().attr('data-id') : 0);
            if (!_this.hasClass('active')) {
                if (_countIsActived < _intMaxCount) {
                    _this.addClass('active');
                    if ((_countIsActived + 1) == _intMaxCount) {
                        _this.parent().addClass('ready-number-set');
                    }
                    addTicketToList(_intID, _this.html().trim(), -1);
                }
            }
            else {
                _this.removeClass('active');
                _this.parent().removeClass('ready-number-set');
                removeNumberTicket(_intID, _this.html().trim(), -1);
            }
            _intMaxCount += getMaxTicket(_this.parent().parent().find('.num-list.num-list_yellow'));
            genHtmlListTicket(_intMaxCount);
        });
    }
    if (typeof $('.box-ticket__body .num-list.num-list_yellow li') != 'undefined' && $('.box-ticket__body .num-list.num-list_yellow li').length > 0) {
        $('.box-ticket__body .num-list.num-list_yellow li').unbind().click(function () {
            var _this = $(this);
            var _intMaxCount = getMaxTicket(_this.parent());
            _intMaxCount += getMaxTicket(_this.parent().parent().find('.num-list.first-49'));
            var _countIsActived = _this.parent().find('li.active').length;
            var _intID = (typeof _this.parent().parent().parent().parent().attr('data-id') != 'undefined' ? _this.parent().parent().parent().parent().attr('data-id') : 0);
            var intValueCur = (_countIsActived > 0 ? _this.parent().find('li.active').html().trim() : 0);
            var intValueNew = _this.html().trim();
            removeNumberTicket(_intID, intValueCur, _intMaxCount);
            _this.parent().find('li.active').removeClass('active');
            _this.addClass('active');
            addTicketToList(_intID, intValueNew, _intMaxCount);
            genHtmlListTicket(_intMaxCount);
        });
    }
}
var clearTicketIsActived = function (_this, _id) {
    var _parent = $(_this).parent().parent().parent();
    var _parent_ul = _parent.find('.box-ticket__body .num-list.first-49');
    if (typeof _parent_ul != 'undefined' && _parent_ul.length > 0) {
        if (typeof _parent_ul.find('li.active') != 'undefined' && _parent_ul.find('li.active').length > 0) {
            _parent_ul.find('li.active').removeClass('active');
            _parent_ul.removeClass('ready-number-set');
        }
    }
    if (typeof _parent.find('.box-ticket__body .num-list.num-list_yellow li.active') != 'undefined' && _parent.find('.box-ticket__body .num-list.num-list_yellow li.active').length > 0) {
        _parent.find('.box-ticket__body .num-list.num-list_yellow li.active').removeClass('active');
    }
    clearListNumberTicket(_id);
    var _intMaxCount = getMaxTicket(_parent_ul);
    var _parent_ul_OneMore = _parent.find('.box-ticket__body .num-list.num-list_yellow');
    _intMaxCount += getMaxTicket(_parent_ul_OneMore);
    genHtmlListTicket(_intMaxCount);
    setNumberTicket();
}

var closeTicket = function (_this, _id) {
    $(_this).closest('.profile-ticket__item').remove();

    clearTicketIsActived($(_this).parent().prev(), _id);
}

var autoProfileAnimated = function (_id, elm) {
    var intTotal = 50;
    for (var i = 0; i <= intTotal; i++) {
        setTimeout(function () {
            arrTicketTemp = autoProfile(elm);
        }, 500 + i * 10);
    }

    var internalListener = setTimeout(function () { addListTicketAfterRandom(_id, elm) }, 500 + intTotal * 10);

}
var autoProfile = function (elm) {
    $(elm).find('.num-list__item').removeClass('active');
    var arr = [];
    var arrTempTotal = [];
    var _intMaxCount = getMaxTicket($(elm).find('.first-49'));
    while (arr.length < _intMaxCount) {
        var randomnumber = Math.ceil(Math.random() * 39)
        if (arr.indexOf(randomnumber) == -1 && arr.length != _intMaxCount) {
            arr.push(randomnumber);
        }
    }
    var arra = [];
    _intMaxCount = getMaxTicket($(elm).find('.num-list_yellow'));
    while (arra.length < _intMaxCount) {
        var randomnumber = Math.ceil(Math.random() * 18)
        if (arra.indexOf(randomnumber) == -1 && arra.length == 0) {
            arra.push(randomnumber);
        }
    }
    arr.forEach(function (item, i, arr) {
        $(elm).find('.first-49 li:eq(' + (item - 1) + ')').addClass('active');
        arrTempTotal.push(item);
    });
    arra.forEach(function (item, i, array) {
        $(elm).find('.num-list_yellow li:eq(' + (item - 1) + ')').addClass('active');
        arrTempTotal.push(item);
    });
    $(elm).find('.first-49').attr("data-ready", true);
    $(elm).find('.first-49').addClass('ready-number-set')
    $(elm).find('.num-list_yellow').attr("data-ready", true);
    $(elm).find('.num-list_yellow').addClass('ready-number-set');

    return arrTempTotal;
}
var addListTicketAfterRandom = function (_id, elm) {
    if (arrTicketTemp != null && arrTicketTemp.length > 0) {
        var _intMaxCount = getMaxTicket($(elm).find('.first-49'));
        _intMaxCount += getMaxTicket($(elm).find('.num-list_yellow'));
        clearListNumberTicket(_id);
        for (var i = 0; i < arrTicketTemp.length; i++) {
            if (i < (arrTicketTemp.length - 1)) {
                addTicketToList(_id, arrTicketTemp[i], -1);
            }
            else {
                addTicketToList(_id, arrTicketTemp[i], i);
            }
        }
        genHtmlListTicket(_intMaxCount);
    }
}

var getMaxTicket = function (_this) {
    var intMaxTicket = 0;
    if (typeof $(_this) != 'undefined' && $(_this).length > 0 &&
        typeof $(_this).attr('data-max') != 'undefined') {
        intMaxTicket = parseInt($(_this).attr('data-max'));
    }

    return intMaxTicket;
}

var genHtmlListTicket = function (intMaxTicket) {
    var strHtml = "";
    if (lstTickets != null && lstTickets.length > 0) {
        for (var i = 0; i < lstTickets.length; i++) {
            if (lstTickets[i].arrNumber != null && lstTickets[i].arrNumber.length == intMaxTicket &&
                lstTickets[i].arrNumber.indexOf(0) < 0) {
                strHtml += "<tr id='trTicket_" + lstTickets[i].ID + "'>";
                strHtml += "<td id='tdTicket_" + lstTickets[i].ID + "'>";
                for (var k = 0; k < lstTickets[i].arrNumber.length; k++) {
                    strHtml += "<span class='ticketNumber " + ((k == (lstTickets[i].arrNumber.length - 1)) ? "oneMore" : "") + "'>" + lstTickets[i].arrNumber[k] + "</span>";
                }
                strHtml += "</td>";
                strHtml += "<td class='text-center fw-400'>1</td>";
                strHtml += "</tr>";
            }

        }
        if (typeof $('#tbListTicket') != 'undefined' && $('#tbListTicket').length > 0) {
            $('#tbListTicket').html(strHtml);
        }
    }
    setNumberTicket();
}
function setNumberTicket() {
    var count = 0;
    var priceTicket = $('#hdPriceTicket').val();
    if (lstTickets != null && lstTickets.length > 0) {
        for (var i = 0; i < lstTickets.length; i++) {
            if (lstTickets[i].arrNumber != null) {
                count++;
            }
        }
    }
    $('#spanNumberTicket').text(count);
    $('#spanTotal').text((priceTicket * count).toFixed(4));
}

function ClickTab(value) {
    if (value == 1) {
        $("#idTotalManual").addClass('activepage');
        $("#idTotalmultibet").removeClass('activepage');

    }
    if (value == 2) {
        $("#idTotalManual").removeClass('activepage');
        $("#idTotalmultibet").addClass('activepage');
    }
}
var ListNumberAutomatic = [];
function RandomNumberAutomaticTicket() {
    var numberTicket = $('#sliderOutput1').val();
    $.ajax({
        type: "get",
        url: "/Common/RandomTicketNumber",
        async: true,
        data: { numberTicket: numberTicket },
        beforeSend: function () {
        },
        success: function (d) {

            if (d.ListNumber != null) {
                $("#divListRandom").empty();
                ListNumberAutomatic = [];
                ListNumberAutomatic = d.ListNumber;
                var strHtml = "";
                for (var i = 0; i < ListNumberAutomatic.length; i++) {
                    var arrNum = new Array();
                    var strListNumber = ListNumberAutomatic[i];
                    arrNum = strListNumber.split(',');
                    if (arrNum.length > 0) {
                        strHtml += "<div class=\"col-md-4 col-sm-12\">";
                        strHtml += "<div class=\"number-list\">";
                        for (var j = 0; j < arrNum.length; j++) {

                            var cssClass = "";
                            if (j == arrNum.length - 1) {
                                cssClass = "last-ball";
                            }
                            strHtml += "<span class = " + cssClass + ">" + arrNum[j] + "</span>";

                        }
                        strHtml += "</div>";
                        strHtml += "</div>";
                    }
                }
                    $("#divListRandom").append(strHtml);
                    var Price = $("#hdPriceTicket").val();
                    var numberTicket = $("#sliderOutput1").val();
                    $("#spanNumberTicketMultibet").text(numberTicket);
                    $("#spanTotalMultibet").text((Price * numberTicket).toFixed(4));

            }
        }
    });
    return false;
}
function SubmitTicketAutomaticMultibet() {
    var createdate = Date.now;
    var totalquantity = 0;
    var lstTicket = '';
    var checkReg = true;
    var num = 0;

    if (ListNumberAutomatic.length == 0) {
        alertify.error('Please, fill out your tickets!');
        checkMaxShowAlertNoti(maxItemNotification);
        checkReg = false;
    }

    if (ListNumberAutomatic.length > 0) {
        totalquantity = ListNumberAutomatic.length;
        for (var i = 0; i < ListNumberAutomatic.length; i++) {
            var arrNum = new Array();
            var strListNumber = ListNumberAutomatic[i];
            arrNum = strListNumber.split(',');
            if (arrNum.length > 0) {
                for (var j = 0; j < arrNum.length; j++) {
                    lstTicket += arrNum[j] + ";";
                }
            }
            lstTicket += ',';
        }
    }

    if (!checkReg) {
        return false;
    }
    else {
        var intNumberMegaballID = 0;
        //if (typeof $('#hdNumberMegaballID') != 'undefined' && $('#hdNumberMegaballID').length > 0) {
        //    intNumberMegaballID = $('#hdNumberMegaballID').val();
        //}

        var dblJackPot = 0;
        //if (typeof $('#hdJackPot') != 'undefined' && $('#hdJackPot').length > 0) {
        //    dblJackPot = $('#hdJackPot').val();
        //}

        $.ajax({
            type: "post",
            url: "/Booking/BookingListTicketMember",
            async: true,
            data: { lstTicket: lstTicket, intNumberMegaballID: intNumberMegaballID, dblJackPot: dblJackPot },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (data) {
                hideLoadingNew();
                if (!data.isRedirect) {
                    if (typeof data.isError != 'undefined') {
                        if (data.isError == 0) {
                            alertify.success('Buy ticket successfully.');
                            checkMaxShowAlertNoti(maxItemNotification);
                            setTimeout(function () {
                                location.reload();
                            }, 1500);
                        }
                        else if (data.isError == 1) {
                            if (typeof data.intTimeOpened != 'undefined') {
                                showMessageAlert('Buying Tickets time is closed. Please come back after' + data.intTimeOpened + ':00:00', null)
                            }
                            else {
                                alertify.error('An error occurred! Please try again after.');
                                checkMaxShowAlertNoti(maxItemNotification);
                            }
                        }
                        else if (data.isError == 2) {
                            window.location.href = "/login?redirect=";
                        }
                        else if (data.isError == 3) {
                            alertify.alert('Your points are not enough to buy <span style="color:red;">' + totalquantity + ' tickets</span>!<br/> Please input points to buy!');
                            checkMaxShowAlertNoti(maxItemNotification);
                        }
                        else if (data.isError == 4) {
                            alertify.error('An error occurred! Please try again after.');
                            checkMaxShowAlertNoti(maxItemNotification);
                        }
                        else if (data.isError == 5) {
                            alertify.error('Please, fill out your tickets!!');
                            checkMaxShowAlertNoti(maxItemNotification);
                        }
                    }
                    else {
                        alertify.error('An error occurred! Please try again after.');
                        checkMaxShowAlertNoti(maxItemNotification);
                    }
                }
                else {
                    if (data.redirectUrl == window.location.pathname) {

                        alertify.confirm('Confirm Title', 'Please, login to buy tickets!', function () { $("#registration-login").modal(); }
                            , function () { });

                    }
                    else {
                        window.location.href = data.redirectUrl;
                    }
                }

            },
            error: function () {
                alertify.error('An error occurred! Please try again after.');
                checkMaxShowAlertNoti(maxItemNotification);
                hideLoadingNew();
            }
        });
    }
    return false;
}
function checkList() {
    var rs = true;
    if (lstTickets.length > 0) {
        for (var i = 0; i < lstTickets.length; i++) {
            if (lstTickets[i].arrNumber.length == 6) {
                for (var j = 0; j < lstTickets[i].arrNumber.length; j++) {
                    if (lstTickets[i].arrNumber[j] == 0) {
                        rs = false;

                    }
                }
            }
            else {
                rs = false;

            }
        }
    }
    return rs;
}