﻿
function showMessageAlert(strMessage, alert123) {
    //grab the dialog instance using its parameter-less constructor then set multiple settings at once.
    alertify.alert()
        .setting({
            'label': 'OK',
            'message': strMessage,
            'onok': function () {
                if (alert123 != null) {
                    showLoading();
                    alert123();
                }
            }
        }).show();
}
function showLoading() {
    if (typeof $('#divLoading') != 'undefined' && $('#divLoading').length > 0) {
        $('#divLoading').css("display", "block");
    }
}

function showLoadingNew() {
    if (typeof $('#loading') != 'undefined' && $('#loading').length > 0) {
        $('.body-loader').css("display", "block");
    }
}

function hideLoading() {
    if (typeof $('#divLoading') != 'undefined' && $('#divLoading').length > 0) {
        $('#divLoading').css("display", "none");
    }
}
function hideLoadingNew() {
    if (typeof $('#loading') != 'undefined' && $('#loading').length > 0) {
        $('.body-loader').css("display", "none");
    }
}

var showDropDownMenu = function (_this) {
    if (typeof _this != 'undefined' && _this.length > 0) {
        if (!_this.hasClass('show')) {
            _this.addClass('show');
        }
        else {
            _this.removeClass('show');
        }
    }
}

var loadWalletPoints = function (_this, e) {
    if (e.target !== e.currentTarget && ($(_this).find('label#lblPoint').length > 0 && e.target !== $(_this).find('label#lblPoint')[0])) {
        return;
    }
    if (!_this.hasClass('show')) {
        $.ajax({
            type: "post",
            url: "/Game/LoadPoinBalance",
            async: true,
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (typeof data.isError != 'undefined') {
                    if (data.isError == 0) {
                        if (typeof $('#dropPointID') != 'undefined' && $('#dropPointID').length > 0) {
                            $('#dropPointID').html(data.dbPoint);
                        }

                        if (typeof $('#spTicketID') != 'undefined' && $('#spTicketID').length > 0) {
                            $('#spTicketID').html(data.numberTicket);
                        }

                        if (typeof $('#pointID') != 'undefined' && $('#pointID').length > 0) {
                            $('#pointID').html(data.dbPoint);
                        }

                        if (typeof $('#spanTicketID') != 'undefined' && $('#spanTicketID').length > 0) {
                            $('#spanTicketID').html(data.numberTicket);
                        }

                        if (typeof $('#lblPoint') != 'undefined' && $('#lblPoint').length > 0) {
                            $('#lblPoint').html(data.dbPoint + '&nbsp;&nbsp;Point');
                        }

                        if (typeof $('#strInfoPoint') != 'undefined' && $('#strInfoPoint').length > 0) {
                            var dbPoint = data.dbPoint;
                            var intTickets = data.numberTicket;
                            $('#strInfoPoint').attr('title', dbPoint + ' ~ ' + intTickets + ' tickets');
                        }
                        if (typeof $('#idBTC') != 'undefined' && $('#idBTC').length > 0) {
                            $('#idBTC').html(data.strWalletAddress);
                        }


                        if (typeof $('#idXRP') != 'undefined' && $('#idXRP').length > 0) {
                            $('#idXRP').html(data.strRippleAddress);
                        }
                    }
                    else {
                        alertify.error('An error occurred! Please try again after.');
                    }
                }
                else {
                    alertify.error('An error occurred! Please try again after.');
                }
                hideLoading();
            },
            error: function () {
                alertify.error('An error occurred! Please try again after.');
                hideLoading();
            }
        });
    }
    showDropDownMenu(_this);
}

var backPagePrev = function () {
    window.history.back();
}

var isValidEmailAddress = function (emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
}

var goToLogin = function () {
    window.location.href = '/login';
}

var goToSignup = function () {
    window.location.href = '/sign-up';
}


function Empty(n) {
    return n == null || n == undefined || n == "" || n.length == 0 || jQuery.trim(n).length == 0 ? !0 : !1
}

var checkMaxShowAlertNoti = function (maxItemNotifier) {
    if (!Empty($('.alertify-notifier .ajs-message.ajs-error'))) {
        var i = 1;
        $('.alertify-notifier .ajs-message.ajs-error').each(function () {
            if (!Empty($(this).parent().find('.ajs-message.ajs-error:not(.ajs-visible)'))) {
                $(this).remove();
                i--;
            }
            if (i <= maxItemNotifier) {
                $(this).show();
            }
            else {
                $(this).remove();
            }
            i++;
        });
    }
    if (!Empty($('.alertify-notifier .ajs-message.ajs-success'))) {
        var i = 1;
        $('.alertify-notifier .ajs-message.ajs-success').each(function () {
            if (!Empty($(this).parent().find('.ajs-message.ajs-success:not(.ajs-visible)'))) {
                $(this).remove();
                i--;
            }
            if (i <= maxItemNotifier) {
                $(this).show();
            }
            else {
                $(this).remove();
            }
            i++;
        });
    }
}

var gotoContactPage = function () {
    window.location.href = '/contact';
}

var ShowPopup = function (titleMesseage, messeage, position, type) {
    $("#titleMesseage").text(titleMesseage);
    $("#messeage").text(messeage);
    $("#popupAlert").addClass('myAlert-' + position);
    $("#popupAlert").addClass('alert-' + type);
    $("#popupAlert").show();
    setTimeout(function () {
        $("#popupalert").hide();
    }, 10000);
}

var ClosePopup = function () {
    $("#popupAlert").hide();
}
function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function (googleUser) {
            if (googleUser.isSignedIn() == true) {
                //googleUser.isSignedIn() = true: is signed in
                $.ajax({
                    url: 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + googleUser.Zi.access_token,
                    data: null,
                    success: function (response) {
                        //get user info by token google
                        loginMemberFB(response.name, response.email, response.gender, response.birthday, 2, response.id);//login hệ thống RippleLot bằng thông tin facebook (call xuống db de insert member)
                    },
                    dataType: "jsonp"
                });
            }
            else {
                alertify.error('Login Google+ failed! Please try again after.');
            }
        }, function (error) {
            alertify.error('Login Google+ failed! Please try again after.');
        });
}
var loginMemberFB = function (strName, strEmail, intGender, strBirthDay, intIsUserType, strUserTypeID) {
    var bolIsError = false;
    var strMessageError = "";

    if (strUserTypeID == null || strUserTypeID.trim().length == 0) {
        if (intIsUserType == 1) {
            //Login FB fails
            alertify.error('Login Facebook failed! Please try again after.');
        }
        else {
            //Login GG fails
            alertify.error('Login Google+ failed! Please try again after.');
        }

        return false;
    }

    if (strEmail == null || strEmail.trim().length == 0) {
        //email is null
        strMessageError += "<p>- Please input email</p>";
        bolIsError = true;
    }
    else if (!isValidEmailAddress(strEmail)) {
        //email valid format
        strMessageError += "<p>- Email is not correct format</p>";
        bolIsError = true;
    }

    if (bolIsError) {
        alertify.error(strMessageError);
        return false;
    }
    else {
        var objMember = {};
        objMember.Email = strEmail;
        objMember.FullName = strName;
        objMember.Gender = (intGender == "male" ? 0 : 1);
        if (typeof strBirthDay != 'undefined') {
            objMember.Birthday = strBirthDay;
        }
        objMember.IsUserType = intIsUserType;
        objMember.UserTypeID = strUserTypeID;

        $.ajax({
            type: "post",
            url: "/Login/LoginSuccess",
            //async: true,
            data: JSON.stringify({ objMember: objMember }),
            contentType: 'application/json',
            beforeSend: function () {
                showLoading();
            },
            success: function (data) {
                if (typeof data.isError != 'undefined') {
                    var strTypeMessage = "Facebook";
                    if (intIsUserType == 2) {
                        strTypeMessage = "Google+";
                    }

                    if (data.isError == 0) {
                        alertify.success('Congratulations! Login ' + strTypeMessage + ' succeed.');
                        setTimeout(function () { window.location.href = '/play-game'; hideLoading(); }, 500);
                    }
                    else if (data.isError == 2) {
                        alertify.error('Login ' + strTypeMessage + ' failed! Please try again after.');
                        hideLoading();
                    }
                    else if (data.isError == 3) {

                        $('#login').removeClass('active');
                        $('#update-pass').addClass('active');
                        $('#EmailUpdatePass').val(data.Email.trim());
                        hideLoading();
                    }
                    else if (data.isError == 4) {
                        alert('Register member first fails! Please contact with administrator');
                        hideLoading();
                    }
                    else {
                        alertify.error('Error Login. Please contact with administrator');
                        hideLoading();
                    }
                }
                else {
                    alertify.error('Error Login. Please contact with administrator');
                    hideLoading();
                }
            },
            error: function () {
                alertify.error('Error Login. Please contact with administrator');
                hideLoading();
            }
        });
    }
}