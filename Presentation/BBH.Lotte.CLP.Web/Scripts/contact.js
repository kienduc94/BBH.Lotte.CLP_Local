﻿
///Created Date: 01/03/2018
///Description: init script
$(document).ready(function () {
    if (typeof $('#btnSubscribe') != 'undefined' && $('#btnSubscribe').length > 0) {
        $('#btnSubscribe').on('click', function () {
            sendSubscribe();
        })
    }
});
function SendContact() {
    var checkReg = true;
    var email = $('#email').val();
    var name = $('#name').val();
    var phone = $('#phone').val();
    var content = $('#content').val();
    if (email == '') {
        $('#spanEmail').text('Vui lòng nhập email');
        checkReg = false;
    }
    if (name == '') {
        $('#spanName').text('Vui lòng nhập tên');
        checkReg = false;
    }
    if (phone == '') {
        $('#spanPhone').text('Vui lòng nhập Số điện thoại');
        checkReg = false;
    }
    if (content == '') {
        $('#spanContent').text('Vui lòng nhập nội dung');
        checkReg = false;
    }
    else if (content.length > 2000) {
        $('#spanContent').text('Nội dung không quá 2000 ký tự');
        checkReg = false;
    }
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/Home/SendContact",
            data: { email: email, name: name, phone: phone, content: content },
            success: function (d) {
                if (d == 'sendsuccess') {
                    alertify.alert('Cám ơn bạn đã quan tâm. Chúng tôi sẽ liên hệ bạn trong thời gian sớm nhất');
                }
                else {
                    alertify.error('Không gửi liên hệ được. Vui lòng liên hệ admin');
                }
            }
        })
    }
}

function ResetField(id) {
    switch (id) {
        case 1:
            {
                $('#spanName').text('');
                break;
            }
        case 2:
            {
                $('#spanEmail').text('');
                break;
            }
        case 3:
            {
                $('#spanPhone').text('');
                break;
            }
        case 4:
            {
                $('#spanContent').text('');
                break;
            }

    }
}


///Created Date: 26/02/2018
///Description: send subscribe home page
var sendSubscribe = function () {
    var strFullName = "";
    if (typeof $('#txtFullName') != 'undefined' && $('#txtFullName').length > 0) {
        strFullName = $('#txtFullName').val().trim();
    }

    var strEmail = "";
    if (typeof $('#txtEmail') != 'undefined' && $('#txtEmail').length > 0) {
        strEmail = $('#txtEmail').val().trim();
    }

    var strMessage = "";
    if (typeof $('#txtMessage') != 'undefined' && $('#txtMessage').length > 0) {
        strMessage = $('#txtMessage').val().trim();
    }

    var strMessageError = checkValidateChar(strFullName, "Your full name", 250, 2, "txtFullName").trim();
    strMessageError += checkValidateChar(strEmail, "Your email", 250, 1, "txtEmail").trim();
    strMessageError += checkValidateChar(strMessage, "Your message", 3500, 2, "txtMessage").trim();

    if (strMessageError.trim().length > 0) {
        alertify.error(strMessageError.trim());
        checkMaxShowAlertNoti(maxItemNotification);
    }
    else {
        $.ajax({
            type: "POST",
            url: "http://mvcservices.bigbrothers.gold/insert-subscribe-mail/",
            data: {
                strSubscribeEmail: strEmail, strSubscribeName: strFullName, strSubscribeWebsite: window.location.href, intWebsiteID: 0, strSubscribeTitle: "Subscribe mail Wan2Lot", strSubscribeContent: strMessage.trim()
            },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                alertify.success('Contact is sent successfully!');
                checkMaxShowAlertNoti(maxItemNotification);
                if (typeof $('#txtFullName') != 'undefined' && $('#txtFullName').length > 0) {
                    $('#txtFullName').val('');
                }
                if (typeof $('#txtEmail') != 'undefined' && $('#txtEmail').length > 0) {
                    $('#txtEmail').val('');
                }
                if (typeof $('#txtMessage') != 'undefined' && $('#txtMessage').length > 0) {
                    $('#txtMessage').val('');
                }
                if (typeof grecaptcha !== 'undefined' && grecaptcha && grecaptcha.reset) {
                    grecaptcha.reset();
                }
            },
            error: function (msg) {
                alertify.error('Your question is sent failed! Please try again.');
                checkMaxShowAlertNoti(maxItemNotification);
            }
        });
    }
}


///Created Date: 26/02/2018
///Description: check validate Character
///strContent: content value element
///strElementName: Field Name
///intMaxLength: max length field element
///intTypeFormat: 0: others, 1: email, 2: format not allow html...
var checkValidateChar = function (strContent, strElementName, intMaxLength, intTypeFormat, strElementID) {
    var strMessage = "";
    if (strContent.trim().length == 0) {
        strMessage = "- Please enter <b>" + strElementName.trim() + "</b>!<br/>";
    }
    else if (strContent.trim().length > intMaxLength) {
        strMessage = "- Please enter <b>" + strElementName.trim() + "</b> less than " + intMaxLength + " character!<br/>";
    }
    else if (intTypeFormat == 1 && !isValidEmailAddress(strContent)) {
        //isValidEmailAddress: method isvalid email
        strMessage = "- Please enter <b>" + strElementName + "</b> correct format e-mail again!<br/>";
    }
    else if (intTypeFormat == 2 && /<(br|basefont|hr|input|source|frame|param|area|meta|!--|col|link|option|base|img|wbr|!DOCTYPE).*?>|<(a|abbr|acronym|address|applet|article|aside|audio|b|bdi|bdo|big|blockquote|body|button|canvas|caption|center|cite|code|colgroup|command|datalist|dd|del|details|dfn|dialog|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frameset|head|header|hgroup|h1|h2|h3|h4|h5|h6|html|i|iframe|ins|kbd|keygen|label|legend|li|map|mark|menu|meter|nav|noframes|noscript|object|ol|optgroup|output|p|pre|progress|q|rp|rt|ruby|s|samp|script|section|select|small|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video).*?<\/\2>/i.test(strContent.trim())) {
        strMessage = "- Please enter <b>" + strElementName + "</b> not contains character html or script again!<br/>";
    }

    if (!Empty($('#' + strElementID))) {
        if (strMessage.trim().length > 0) {
            $('#' + strElementID).addClass('has-error');
        }
        else {
            $('#' + strElementID).removeClass('has-error');
        }
    }
    return strMessage;
}
