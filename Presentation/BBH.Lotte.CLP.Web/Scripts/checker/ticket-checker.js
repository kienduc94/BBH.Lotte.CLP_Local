﻿$(document).ready(function () {
    $("#tbList").hide();
});
var arrNumber = [];
var maxItemNotification = 1;
$('#ulnum49 li').click(function (e) {
    var flag = true;
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        flag = false;
    }
    if (CheckClickNumber49() && flag) {
        $(this).addClass('active');
    }
    ListNumberSelected();
    addClassReady();
});

$('#ulExtra_1 li').click(function (e) {
    $('#ulExtra_1 li').each(function (e) {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active')
        }
    });
    $(this).addClass('active');
    ListNumberSelected();
});
function CheckClickNumber49() {
    var rs = true;
    var count = 0;
    $('#ulnum49 li').each(function (e) {
        if ($(this).hasClass('active')) {
            count++;
        }
    });
    if (count == 5) {
        rs = false;
    }
    
    return rs;
}
function CheckClickNumber26() {
    var rs = true;
    var count = 0;
    $('#ulExtra_1 li').each(function (e) {
        if ($(this).hasClass('active')) {
            count++;
        }
    });
    if (count == 1) {
        rs = false;
    }
    return rs;
}
function ListNumberSelected() {
    arrNumber = [];
    $('#ulnum49 li').each(function (e) {
        if ($(this).hasClass('active')) {
            arrNumber.push($(this).text());
        }
    });
    $('#ulExtra_1 li').each(function (e) {
        if ($(this).hasClass('active')) {
            arrNumber.push($(this).text());
        }
    });
}
function CheckNumber() {
    var checkReg = true;

    if (arrNumber.length != 6) {
        alertify.error('Please, fill out your tickets!');
        checkMaxShowAlertNoti(maxItemNotification);
        checkReg = false;
    }

    if (checkReg) {
        var strNumber = '';
        for (var i = 0; i < arrNumber.length; i++) {
            strNumber += arrNumber[i] + ',';
        }

        $.ajax({
            type: "post",
            url: "/Checker/CheckResultNumber",
            async: true,
            data: { strNumber: strNumber },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (data) {
                if (typeof data != 'undefined') {
                    $("#tbList").show();
                    $("#tbListAwardNumber").empty();
                    $("#tbListAwardNumber").append(data);
                }
                else {
                    alertify.error('An error occurred! Please try again after.');
                    checkMaxShowAlertNoti(maxItemNotification);
                }
                hideLoadingNew();
            },
            error: function () {
                alertify.error('An error occurred! Please try again after.');
                checkMaxShowAlertNoti(maxItemNotification);
                hideLoadingNew();
            }
        });
    }
}
function addClassReady() {
    var count = 0;
    $('#ulnum49 li').each(function (e) {
        if ($(this).hasClass('active')) {
            count++;
        }
    });
    if (count == 5) {
        $('#ulnum49').addClass('ready-number-set');
    }
    else {
        $('#ulnum49').removeClass('ready-number-set');
    }
}
function clearTicket() {
    $('#ulnum49 li').each(function (e) {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        }
    });
    $('#ulExtra_1 li').each(function (e) {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        }
    });
    arrNumber = [];
}