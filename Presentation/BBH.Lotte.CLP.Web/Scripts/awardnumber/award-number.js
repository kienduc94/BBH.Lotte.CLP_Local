﻿var maxItemNotification = 1;
function ClickDetail(value) {
    $.ajax({
        type: "post",
        url: "/Award/GenHtmlModalResult",
        async: true,
        data: { intNumberID: value },
        beforeSend: function () {
            showLoadingNew();
        },
        success: function (data) {
            if (typeof data != 'undefined') {
                $("#myModal").empty();
                $("#myModal").append(data);
            }
            else {
                //truong hop error
                alertify.error('An error occurred! Please try again after.');
                checkMaxShowAlertNoti(maxItemNotification);
            }
            hideLoadingNew();
        },
        error: function () {
            alertify.error('An error occurred! Please try again after.');
            checkMaxShowAlertNoti(maxItemNotification);
            hideLoadingNew();
        }
    });
}

function SearchAward() {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var page = 1;
    var checkReg = true;
    if (fromDate == '' && toDate == '') {
        checkReg = false;
        alertify.error('Please input from date or to date !');
        checkMaxShowAlertNoti(maxItemNotification);
    }
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            url: "/Award/GenHtmlSearch",
            async: true,
            data: { fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (data) {
                if (typeof data != 'undefined') {

                    var json = JSON.parse(data);
                    if (json != "") {
                        $('#tbodyList').empty();
                        $('#tbodyList').append(json.ContentResult);
                        if ($('#ulPaging') && $('#ulPaging').length > 0) {
                            $('#ulPaging').html(json.PagingResult);
                        }
                    }
                    hideLoadingNew();
                }
                else {
                    //truong hop error
                    alertify.error('An error occurred! Please try again after.');
                    checkMaxShowAlertNoti(maxItemNotification);
                }
                hideLoadingNew();
            },
            error: function () {
                alertify.error('An error occurred! Please try again after.');
                checkMaxShowAlertNoti(maxItemNotification);
                hideLoadingNew();
            }
        });
    }
}
function PagingSearchAwardCoin(page) {
    showLoadingNew();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var checkReg = true;
    if (fromDate == '' && toDate == '') {
        checkReg = false;
        alertify.error('Please input from date or to date!.');
    }
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            url: "/Award/GenHtmlSearch",
            async: true,
            data: { fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoadingNew();
            },
            success: function (data) {
                if (typeof data != 'undefined') {
                    hideLoadingNew();
                    var json = JSON.parse(data);
                    if (json != "") {
                        $('#tbodyList').empty();
                        $('#tbodyList').append(json.ContentResult);
                        if ($('#ulPaging') && $('#ulPaging').length > 0) {
                            $('#ulPaging').html(json.PagingResult);
                        }
                    }
                }
                else {
                    hideLoadingNew();
                    alertify.error('An error occurred! Please try again after.');
                    checkMaxShowAlertNoti(maxItemNotification);
                }
               
            },
            error: function () {
                hideLoadingNew();
                alertify.error('An error occurred! Please try again after.');
                checkMaxShowAlertNoti(maxItemNotification);
               
            }
        });
    }
}