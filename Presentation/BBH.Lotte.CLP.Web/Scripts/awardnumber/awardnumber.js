﻿
$(document).ready(function () {
    GetListAwardOrderByDate();
    var lstCheckResult = $('#hdResultNumber').val();
    if (lstCheckResult != null && lstCheckResult != "") {
        $("#divLotteryResult").show();
        $("#over").show();
        $("#show_lottery_results").html(lstCheckResult);
        //window.location.href='/xo-so';
    }
    else if (lstCheckResult == "")
    {
        $("#divLotteryResult").hide();
        $("#over").hide();
        $("#show_lottery_results").html('');
        //window.location.href = '/xo-so';
    }
    if (lstCheckResult == 'errorCaptcha')
    {
        $("#divLotteryResult").hide();
        $("#over").hide();
        $("#show_lottery_results").html('');
        //var checkReg = true;
        //var strBlockHash = $("#btchash").val();
        //if (strBlockHash == '') {
        //    alertify.error('Please input string LockHash !');
        //    checkReg = false;
        //}
        alertify.error('Please confirm captcha !');
        //window.location.href = '/xo-so';
    }
   
    setTimeout(function () {
        $.ajax({
            type: "post",
            async: true,
            url: "/Award/SetTimeoutSession",
            success: function () {
            }
        })
    }, 10);
});

function SearchAward() {
    var language = $('#hdLanguage').val();
    var searchDay = $('#txtDatetime').val();
    if (searchDay == '') {
        if (language == 'EN') {
            alertify.error('Please select date to search');
        }
        else
        {
            alertify.error('Vui lòng chọn ngày tìm kiếm');
        }
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/Award/SearchAwardByDate",
            data: { searchDay: searchDay },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                if (d != null && d != "")
                {
                    var obj = JSON.parse(d);
                    if(obj!=null&&obj!="")
                    {
                        $('#hResultNumber').text(obj.TitleResult);
                        $('#ulLotteryResult1').html(obj.ContentResult);
                    }
                }
                hideLoading();
            },
            error: function (e) {
                alertify.error('An error occurred! Please try again after.');
                hideLoading();
            }
        });
    }
}
function GetListAwardOrderByDate() {
    $.ajax({
        type: "post",
        url: "/Award/GetListAwardOrderByDate12",
        async: true,
        beforeSend: function () {
        },
        success: function (d) {
            if (d == '-1') {
                $("#ulLotteryResult1").html('');
            }
            else {
                $("#ulLotteryResult1").html(d.content);
            }
        },
        error: function () {
            $("#ulLotteryResult1").html('');
        }
    });
}
function SubmitCapcha() {
    var checkReg = true;
    var strBlockHash = $("#btchash").val();
    if (strBlockHash == '') {
        alertify.error('Please input string LockHash !');
        checkReg = false;
    }
    if (!checkReg) {
        return false;
    }
    else {
        $.post("/Award/SubmitCapcha", function (data) {
            if(data == 'errorCaptcha') {
                alertify.error('Security code not incorrect !');
            }
            else {
                $("#divLotteryResult").show();
                $("#over").show();
                $("#show_lottery_results").html(d);
            }
        });
    }
}

function CheckBlock() {
    var checkReg = true;
    var strBlockHash = $("#btchash").val();
    //var response = $('#captcha').val();
    if (strBlockHash == '') {
        alertify.error('Please input string LockHash !');
        checkReg = false;
    }
    
    if (!checkReg) {
        return false;
    }
    else {
        return true;
       
    }
}

function CheckResultNumber() {
    var checkReg = true;
    var strBlockHash = $("#btchash").val();
    //var response = $('#captcha').val();
    if (strBlockHash == '')
    {
        alertify.error('Please input string LockHash !');       
        checkReg = false;
    }
    //if (catcha == '') {
    //    checkReg = false;
    //    $('#ErrCaptcha').css("display", "");
    //    $('#ErrCaptcha').text('Please input Security code');
    //}
    if (!checkReg) {
        return false;
    }
    else {
        //return true;
        $.ajax({
            type: "post",
            url:"/Award/CheckResultNumber",
            async: true,
            data: { strBlockHash: strBlockHash/*, response: response*/},
            beforeSend: function () {
            },
            success: function (d) {
                //alert(d);
                if (d == 'errorCaptcha') {
                    alertify.error('Security code not incorrect !');
                    //RefreshCaptchaCheckResult();
                }
                else {
                    $("#divLotteryResult").show();
                    $("#over").show();

                    $("#show_lottery_results").html(d);
                }
            },
            error: function () {
                alertify.error('String LockHash not incorrect !');
            }
        });
    }
}
function CloseLotteryResult() {
    $("#divLotteryResult").hide();
    $("#over").hide();
}

//function RefreshCaptchaCheckResult()
//{
//    var idImage = 'imgCaptchaSignup';
//    var imgCatpchaVote = document.getElementById(idImage);
//    imgCatpchaVote.src = "/AjaxHandler/WriteImageCaptchaCheckResultLotte.aspx?query=" + Math.random();
//}

//function ResetCaptcha()
//{
//    $('#spanCatcha').css("display", "none");
//    $('#spanCatcha').text('');
//}