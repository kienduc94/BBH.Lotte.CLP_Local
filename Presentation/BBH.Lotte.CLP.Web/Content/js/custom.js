/* Toastr */
!function(a){a(["jquery"],function(a){return function(){function h(a,b,c){return u({type:e.error,iconClass:v().iconClasses.error,message:a,optionsOverride:c,title:b})}function i(c,d){return c||(c=v()),b=a("#"+c.containerId),b.length?b:(d&&(b=r(c)),b)}function j(a,b,c){return u({type:e.info,iconClass:v().iconClasses.info,message:a,optionsOverride:c,title:b})}function k(a){c=a}function l(a,b,c){return u({type:e.success,iconClass:v().iconClasses.success,message:a,optionsOverride:c,title:b})}function m(a,b,c){return u({type:e.warning,iconClass:v().iconClasses.warning,message:a,optionsOverride:c,title:b})}function n(a){var c=v();b||i(c),q(a,c)||p(c)}function o(c){var d=v();return b||i(d),c&&0===a(":focus",c).length?void w(c):void(b.children().length&&b.remove())}function p(c){for(var d=b.children(),e=d.length-1;e>=0;e--)q(a(d[e]),c)}function q(b,c){return!(!b||0!==a(":focus",b).length)&&(b[c.hideMethod]({duration:c.hideDuration,easing:c.hideEasing,complete:function(){w(b)}}),!0)}function r(c){return b=a("<div/>").attr("id",c.containerId).addClass(c.positionClass).attr("aria-live","polite").attr("role","alert"),b.appendTo(a(c.target)),b}function s(){return{tapToDismiss:!0,toastClass:"toast",containerId:"toast-container",debug:!1,showMethod:"fadeIn",showDuration:300,showEasing:"swing",onShown:void 0,hideMethod:"fadeOut",hideDuration:1e3,hideEasing:"swing",onHidden:void 0,extendedTimeOut:1e3,iconClasses:{error:"toast-error",info:"toast-info",success:"toast-success",warning:"toast-warning"},iconClass:"toast-info",positionClass:"toast-top-right",timeOut:5e3,titleClass:"toast-title",messageClass:"toast-message",target:"body",closeHtml:'<button type="button">&times;</button>',newestOnTop:!0,preventDuplicates:!1,progressBar:!1}}function t(a){c&&c(a)}function u(c){function q(b){if(!a(":focus",j).length||b)return clearTimeout(o.intervalId),j[e.hideMethod]({duration:e.hideDuration,easing:e.hideEasing,complete:function(){w(j),e.onHidden&&"hidden"!==p.state&&e.onHidden(),p.state="hidden",p.endTime=new Date,t(p)}})}function r(){(e.timeOut>0||e.extendedTimeOut>0)&&(h=setTimeout(q,e.extendedTimeOut),o.maxHideTime=parseFloat(e.extendedTimeOut),o.hideEta=(new Date).getTime()+o.maxHideTime)}function s(){clearTimeout(h),o.hideEta=0,j.stop(!0,!0)[e.showMethod]({duration:e.showDuration,easing:e.showEasing})}function u(){var a=(o.hideEta-(new Date).getTime())/o.maxHideTime*100;m.width(a+"%")}var e=v(),f=c.iconClass||e.iconClass;if("undefined"!=typeof c.optionsOverride&&(e=a.extend(e,c.optionsOverride),f=c.optionsOverride.iconClass||f),e.preventDuplicates){if(c.message===g)return;g=c.message}d++,b=i(e,!0);var h=null,j=a("<div/>"),k=a("<div/>"),l=a("<div/>"),m=a("<div/>"),n=a(e.closeHtml),o={intervalId:null,hideEta:null,maxHideTime:null},p={toastId:d,state:"visible",startTime:new Date,options:e,map:c};return c.iconClass&&j.addClass(e.toastClass).addClass(f),c.title&&(k.append(c.title).addClass(e.titleClass),j.append(k)),c.message&&(l.append(c.message).addClass(e.messageClass),j.append(l)),e.closeButton&&(n.addClass("toast-close-button").attr("role","button"),j.prepend(n)),e.progressBar&&(m.addClass("toast-progress"),j.prepend(m)),j.hide(),e.newestOnTop?b.prepend(j):b.append(j),j[e.showMethod]({duration:e.showDuration,easing:e.showEasing,complete:e.onShown}),e.timeOut>0&&(h=setTimeout(q,e.timeOut),o.maxHideTime=parseFloat(e.timeOut),o.hideEta=(new Date).getTime()+o.maxHideTime,e.progressBar&&(o.intervalId=setInterval(u,10))),j.hover(s,r),!e.onclick&&e.tapToDismiss&&j.click(q),e.closeButton&&n&&n.click(function(a){a.stopPropagation?a.stopPropagation():void 0!==a.cancelBubble&&a.cancelBubble!==!0&&(a.cancelBubble=!0),q(!0)}),e.onclick&&j.click(function(){e.onclick(),q()}),t(p),e.debug&&console&&console.log(p),j}function v(){return a.extend({},s(),f.options)}function w(a){b||(b=i()),a.is(":visible")||(a.remove(),a=null,0===b.children().length&&(b.remove(),g=void 0))}var b,c,g,d=0,e={error:"error",info:"info",success:"success",warning:"warning"},f={clear:n,remove:o,error:h,getContainer:i,info:j,options:{},subscribe:k,success:l,version:"2.1.0",warning:m};return f}()})}("function"==typeof define&&define.amd?define:function(a,b){"undefined"!=typeof module&&module.exports?module.exports=b(require("jquery")):window.toastr=b(window.jQuery)});

$(window).load(function(){    
    $('.box-graphic-wrap .hide-tab').css({'dispaly':'none'}); 
});
$(function() {
    var ww = $(window).width();
    $('.piggybank-drawdate').each(function () {
       var drawdate = moment($(this).text());
        drawdate.locale('ru');
        $(this).text(drawdate.format('LL Đ² LT'));
    });

    /*--------------- loader ---------------*/
    $(window).load(function() {
        $('body').removeClass('loaded');
    });
    /*--------------- loader ---------------*/

    /*--------------- countdown init ---------------*/
    //if ($.fn.countdown) {
    //    console.log('1');
    //    $('.js-countdown').countdown({
    //        date: nextDate,
    //        text: 'Đ Đ¾Đ·Ñ‹Đ³Ñ€Ñ‹Ñˆ Đ·Đ°Đ²ĐµÑ€ÑˆĐµĐ½.'
    //    });
    //}else {
    //    console.log('2');
    //}
    //if ($(document).find('.js-countdown-ico').length) {
    //    tflip.methods.countdown_ico('.js-countdown-ico',{date: ICOend,text: '-'});
    //}
    /*--------------- countdown init ---------------*/

    /*--------------- scroll slider ---------------*/
    //if ($('.about-lottery').length) {
    //    function scrollSlider() {
    //        var $parent = $('.about-lottery'),
    //            $parentTop = $parent.offset().top,
    //            $image = $('.js-about-lottery-slider'),
    //            $slide = $('.js-about-lottery-slider .item .img-wrap'),
    //            $text = $('.about-lottery .info'),
    //            wh = $(window).height(),
    //            lastScrollTop = 0,
    //            textInitHeight = $text.outerHeight(),
    //            headerHeight = $('.header').outerHeight(),
    //            ww = $(window).width();

    //        $text.css({
    //            'min-height': textInitHeight + wh/2
    //        });

    //        $image.css({
    //           'height': wh - headerHeight
    //        });

    //        if (ww < 1023) {
    //            setTimeout(function () {
    //                $text.each(function (el) {
    //                    var slideHeight = $($text[el]).outerHeight();
    //                    $($slide[el]).css({
    //                        'height': slideHeight
    //                    });
    //                    console.log('slideHight ' + slideHeight);
    //                });
    //            },100);
    //        }

    //        var $parentHeight = $parent.outerHeight();

    //        var curBlock = 0;

    //        $(window).scroll(function() {
    //            var windscroll = $(window).scrollTop();

    //            if (lastScrollTop > windscroll) {
    //                if (windscroll > $parentTop && windscroll < $parentHeight - wh) {
    //                    $image.css({
    //                        'top': headerHeight,
    //                        'position': 'fixed',
    //                        'bottom': 'auto'
    //                    })
    //                } else if (windscroll < $parentTop) {
    //                    $image.css({
    //                        'top': 0,
    //                        'bottom': 'auto',
    //                        'position': 'absolute'
    //                    });
    //                } else if (windscroll < $parentTop + $parentHeight - wh) {
    //                    $image.css({
    //                        'top': headerHeight,
    //                        'position': 'fixed',
    //                        'bottom': 'auto'
    //                    })
    //                }
    //                $text.each(function(el) {
    //                    var blockTop = $($text[el]).offset().top;

    //                    if (windscroll >= blockTop - wh) {
    //                        curBlock = el;
    //                    }
    //                });
    //            } else if (lastScrollTop < windscroll) {
    //                if (windscroll > $parentTop && windscroll < $parentHeight - wh) {
    //                    $image.css({
    //                        'top': headerHeight,
    //                        'position': 'fixed',
    //                        'bottom': 'auto'
    //                    })
    //                } else if (windscroll < $parentTop) {
    //                    $image.css({
    //                        'top': 0,
    //                        'bottom': 'auto',
    //                        'position': 'absolute'
    //                    });
    //                } else if (windscroll > $parentTop + $parentHeight - wh) {
    //                    $image.css({
    //                        'top': 'auto',
    //                        'bottom': 0,
    //                        'position': 'absolute'
    //                    });
    //                }
    //                $text.each(function(el) {
    //                    var blockTop = $($text[el]).offset().top;

    //                    if (windscroll > blockTop - wh) {
    //                        curBlock = el;
    //                    }
    //                });

    //            }
    //            lastScrollTop = windscroll;

    //            var currentSlide = $lotterySlider.slick('slickCurrentSlide');
    //            if (currentSlide !== curBlock) {
    //                $lotterySlider.slick('slickGoTo', parseInt(curBlock));
    //            }
    //        });

    //        setTimeout(function() {
    //            $('.slick-dots li').on('click', function() {
    //                var index = $('.slick-dots li').index(this);
    //                var pos = $($text[index]).offset().top;
    //                $('body, html').animate({
    //                    scrollTop: pos - 180
    //                }, 500);
    //            });
    //        }, 10);

    //    }
    //    scrollSlider();
    //}
    /*--------------- scroll slider ---------------*/

    /*--------------- slick init ---------------*/
    if ($.fn.slick) {
        var $lotterySlider = $('.js-about-lottery-slider').slick({
            arrows: false,
            dots: true,
            fade: true,
            speed: 500,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    vertical: true,
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    fade: false,
                    draggable: false
                }
            }]
        });

        $('.js-advantages-slider').slick({
            arrows: false,
            dots: true,
            draggable: false,
            rows: 2,
            slidesPerRow: 3,
            responsive: [{
                breakpoint: 640,
                settings: {
                    rows: 1,
                    slidesPerRow: 1
                }
            }]

        });
    }
    /*--------------- slick init ---------------*/

    /*--------------- fancybox init ---------------*/


    if ($('.js-tickets-slider').length) {
        $('.js-tickets-slider').slick({
            arrows: true,
            dots: true,
            draggable: false,
            slidesToShow: 1,
        });
    };
    
    $(".js-modal-slick-but").on("click", function() {

        

        $("body").addClass("special-fancy");
        setTimeout(function() {
            if ($('.js-tickets-slider').is('.slick-initialized')) {
                $('.js-tickets-slider').slick('unslick');
            }
            $('.js-tickets-slider').slick({
                arrows: true,
                dots: true,
                draggable: false,
                slidesToShow: 1,
            });
            $(".js-tickets-slider").find(".slick-dots").appendTo(".fancybox-wrap");
            $("#modal-play_3").parents(".fancybox-wrap").append("<a href='#' class='modal-arrow modal-arrow_left js-modal-arrow_left'><span class='mark__cont'><span class='mark-cont__title'>â„–2</span><span class='mark__icon'></span></span></a><a href='#' class='modal-arrow modal-arrow_right js-modal-arrow_right'><span class='mark__cont'><span class='mark-cont__title'>â„–4</span><span class='mark__icon'></span></span></a>");
            $('.js-modal-arrow_left').click(function() {
                $(this).parent().find('.slick-prev').click();
                return false;
            });
            $('.js-modal-arrow_right').click(function() {
                $(this).parent().find('.slick-next').click();
                return false;
            });

            $(".js-ticket-del").on('click', function(e) {
                e.preventDefault();
                $(this).parents(".fancybox-skin").append('<span class="box-sure show">Đ’Ñ‹ Đ´ĐµĐ¹ÑÑ‚Đ²Đ¸Ñ‚ĐµĐ»ÑŒĐ½Đ¾ Ñ…Đ¾Ñ‚Đ¸Ñ‚Đµ Đ¿Đ¾Đ»Đ½Đ¾ÑÑ‚ÑŒÑ ÑƒĐ´Đ°Đ»Đ¸Ñ‚ÑŒ ÑÑ‚Đ¾Ñ‚ Đ±Đ¸Đ»ĐµÑ‚ Đ¸Đ· ÑĐ¿Đ¸ÑĐºĐ°?<span class="sure-buttons"><span class="sure-btn-wrap"><a href="#" class="btn-empty js-remove-slide">Đ´Đ°</a></span><span class="sure-btn-wrap"><a href="#" class="btn-empty js-ticket-no">Đ¾Ñ‚Đ¼ĐµĐ½Đ°</a></span></span></span>');
                $(".js-ticket-no").on('click', function(e) {
                    e.preventDefault();
                    $(this).parents(".box-sure").detach();
                });
                $(".box-ticket__body").on('click', function(e) {
                    e.preventDefault();
                    $(".box-sure").detach();
                });


            });
            

            
            $(".num-list__item").click(function () {
        var activeItems = $(this).parent().find(".active").length;
        var activeItemsMax = $(this).parent().attr("data-max");
        if ((activeItems + 1) >= activeItemsMax) {
            $(this).parent().data("ready", true);
        } else {
            $(this).parent().data("ready", false);
        }

        if (activeItems < activeItemsMax) {
            $(this).toggleClass("active");
            return false;
        } else {
            $(this).removeClass("active");
            return false;
        }
    });
    
        }, 100);
        

    });

    /*------------------ ticket counters ------------------*/
    setInterval(function () {
        /*var ready_count = 0;
        $(document).find('.box-ticket').each(function () {
            var ready = false;
            $(this).find('.num-list').each(function (index) {
                if (index == 0) {
                    ready = $(this).data('ready');
                }
                ready &= $(this).data('ready');
            });
            if(ready) {
                ready_count++;
                $(this).data('ready', true);
            }
        });
        $('.price-inf__check-ticket_number').text(ready_count);
        $('.price-inf__summa-number').text((ready_count * 0.001));*/
    }, 300);
    /*------------------ ticket counters ------------------*/

    if ($.fn.fancybox) {
        $('.fancybox').fancybox({
            padding: 0,
            helpers: {
                overlay: {
                    locked: false
                }
            },
            tpl: {
                closeBtn: '<div title="Đ—Đ°ĐºÑ€Ñ‹Ñ‚ÑŒ" class="fancybox-item fancybox-close"></div>'
            },
            afterShow: function() {
                if ($('.js-menu-wrap').hasClass('open')) {
                    $('.js-menu-wrap').fadeOut().removeClass('open');
                    $('body').removeClass('body-overflow');
                }
            },
            afterClose: function() {
                $('body').removeClass("special-fancy");

            },
            margin: 10,
            fixed: true
        });
        $('.fancybox2').fancybox({
            padding: 0,
            helpers: {
                overlay: {
                    locked: false
                }
            },
            tpl: {
                closeBtn: '<div title="Đ—Đ°ĐºÑ€Ñ‹Ñ‚ÑŒ" class="fancybox-item fancybox-close"></div>'
            },
            afterShow: function() {
                $('body').addClass("special-fancy");
            },
            afterClose: function() {
                $('body').removeClass("special-fancy");

            },
            margin: 10,
            fixed: true
        });
    }
    /*--------------- fancybox init ---------------*/

    /*--------------- change modal content ---------------*/
    $('.js-modal-change').on('click', function(e) {
        e.preventDefault();
        var target = $(this).attr('href');
        $('.js-modal-in').removeClass('active');
        $(target).addClass('active');
        if (target == '#register1') {
            if (typeof(rcReg) == "undefined" && $(document).find('#rcreg').length) {
                rcReg = grecaptcha.render('rcreg', {
                  'sitekey' : '6LfqiBsUAAAAAAW0UOWA5YFptRFh7p9gpwOk2SOe',
                  'theme' : 'light',
                  'size': 'normal'
                });
            }else {
                if (typeof (grecaptch) != "undefined") {
                    grecaptcha.reset(rcReg);
                }
            }
        }
        $("body").addClass("special-fancy");



    });

    /*--------------- custom-scroll init ---------------*/
    if ($.fn.mCustomScrollbar) {
        if (ww >= 640) {
            $('.js-custom-scroll').mCustomScrollbar({
                theme: "orange"
            });
        }
    }

    /*--------------- accordion ---------------*/
    $('.js-accordion-link').on('click', function(e) {
        e.preventDefault();

        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).next().slideUp();
            return false;
        } else {
            $(this).addClass('open');
            $(this).next().slideDown();
        }
    });
    /*--------------- accordion ---------------*/



    /*---------------fixed aside-------------------*/
    if ($('.js-wrapper-fix').length) {

        var leftTop = $('.js-wrapper-fix'),
            scrollBlock = $('.js-aside-fix');

        function stikyLeftSide(rightSide, leftCol) {
            var scrollPos = $(window).scrollTop();
            var leftTop = leftCol.offset().top;
            var width = $('.box-fixed').width();
            
            
            var rightTop = $(".js-static").outerHeight();
            var asideHeight = $('.js-aside-fix').outerHeight(),
                headerHeight = $('header').outerHeight(),
                asideCont = $('.profile-cont_center').outerHeight() - headerHeight;
            /*
            console.log('scrollPos:'+scrollPos+';');
            console.log('leftTop:'+leftTop+';');
            console.log('width:'+width+';');
            console.log('rightTop:'+rightTop+';');
            console.log('asideHeight:'+asideHeight+';');
            console.log('headerHeight:'+headerHeight+';');
            console.log('asideCont:'+asideCont+';');
            console.log('--------------');*/
            if (scrollPos > (leftTop + rightTop) && scrollPos < (leftTop + leftCol.height() - rightSide.outerHeight() - headerHeight)) {
                rightSide.addClass('fixed').css('width', width).removeClass('bottom');
            } else if (scrollPos < (leftTop + rightTop)) {
                rightSide.removeClass('fixed').css('width', '100%');
            } else if (scrollPos > leftTop && (scrollPos) > (leftTop + leftCol.height() - rightSide.outerHeight() - headerHeight)) {
                if (rightSide.hasClass('profile_box_right')) {
                    rightSide.removeClass('fixed').addClass('bottom');    
                }else {
                    rightSide.removeClass('fixed');//.addClass('bottom');
                }
            }
        }
        stikyLeftSide(scrollBlock, leftTop);
        $(window).on('scroll', function() {
            stikyLeftSide(scrollBlock, leftTop);
        });
        $(window).resize(function() {
            stikyLeftSide(scrollBlock, leftTop);
        });
    };
    /*---------------fixed aside-------------------*/

    /*--------------- add class to header with scroll ---------------*/
    //$(window).scroll(function() {
    //    var windscroll = $(window).scrollTop();
    //    if (windscroll > 0) {
    //        $('.header').addClass('scroll');
    //    } else {
    //        $('.header').removeClass('scroll');
    //    }
    //});
    /*--------------- add class to header with scroll ---------------*/

    /*--------------- set device height to block ---------------*/
    function setSize() {
        var winHeight = $(window).height(),
            headerHeight = $('.header').outerHeight(),
            footerHeight = $('.footer').outerHeight(),
            contentHeight = winHeight - headerHeight - footerHeight;
        $('.js-content').css({
            'min-height': contentHeight,
            'padding-top': headerHeight,
            'padding-bottom': footerHeight
        });
        $('.content-wrapper').css({
            'min-height': winHeight
        })
    }
    setSize();
    $(window).resize(setSize);
    /*--------------- set device height to block ---------------*/

    /*--------------- open menu ---------------*/
    $('body').bind('click', function(e) {
        if (/ip(hone|od)|ipad/i.test(navigator.userAgent)) {
            $('body').css("cursor", "pointer");
        }
        if ($(e.target).closest('.js-menu-btn').length !== 0) {
            $('.js-menu-wrap').fadeIn().addClass('open');
            $('body').addClass('body-overflow');
        } else if ($(e.target).closest('.js-close-menu').length !== 0) {
            $('.js-menu-wrap').fadeOut().removeClass('open');
            $('body').removeClass('body-overflow');
        } else if ($(e.target).closest('.js-menu').length === 0) {
            $('.js-menu-wrap').fadeOut().removeClass('open');
            $('body').removeClass('body-overflow');
        }
    });
    /*--------------- open menu ---------------*/


    /*--------------- open menu in page profile---------------*/
    $('.js-menu-profile-btn').click(function() {
        $('.menu-wrap').addClass('menu-profile-open');
        $('html').addClass('body-overflow');
    });

    $('.close-menu').click(function() {
        $('.menu-wrap').removeClass('menu-profile-open');
        $('html').removeClass('body-overflow');
    });
    $('.menu-wrap').click(function() {
        $('.menu-wrap').removeClass('menu-profile-open')
        $('html').removeClass('body-overflow');
    });
    /*--------------- open menu in page profile ---------------*/


    /*--------------- hide block ticket ihf ---------------*/
    var box_profile_height = $('.box-profile-ticket').height();
    if ($('.box-profile-ticket').length) {
        if (box_profile_height <= 300) {
            $(".price-inf__bottom").addClass("jd");
        }
    };

    var handler = function() {

    }
    $(window).bind('load', handler);
    $(window).bind('resize', handler);
    /*--------------- hide block ticket ihf ---------------*/



    /*--------------- show more langs ---------------*/
    $('body').on('click', function(e) {
        if ($(e.target).closest('.js-lang-more').length === 0) {
            $('.js-lang-sublist').fadeOut();
        } else if ($(e.target).closest('.js-lang-sublist li').length !== 0) {
            $('.js-lang-sublist').fadeOut();
        } else if ($(e.target).closest('.js-lang-more').length !== 0) {
            $('.js-lang-sublist').fadeToggle();
        }
    });
    /*--------------- show more langs ---------------*/

    /*--------------- mobile functions ---------------*/
    $(document).on('touchstart', function() {
        documentClick = true;
    });
    $(document).on('touchmove', function() {
        documentClick = false;
    });
    /*--------------- mobile functions ---------------*/

    $(document).on('click', function(event) {
        if (event.type == "click") documentClick = true;
        if (documentClick) {
            var target = $(event.target);

            /*--------------- show more langs ---------------*/
            if (target.is('.js-select-lang-btn')) {
                event.preventDefault();
                $('.js-lang-list').fadeToggle();
            } else {
                $('.js-lang-list.mob').fadeOut();
            }
            /*--------------- show more langs ---------------*/

            /*--------------- select lang ---------------*/
            if (target.is('.js-lang.mob')) {
                var lang = $(target).attr('data-lang'),
                    text = $(target).text();
                $('.js-select-lang-btn').attr('data-lang', lang).text(text);
                $('.js-lang-list').fadeOut();
            }
            /*--------------- select lang ---------------*/
        }
    });
    /*--------------- open steps in mob ---------------*/
    $('.js-more-link').on('click', function(e) {
        $('.js-more-block-wrap').find('.js-hide-mob').fadeIn();
        $(this).hide();
        return false;
    });
    /*--------------- open steps in mob ---------------*/

    /*--------------- open usermenu ---------------*/
    $('.js-user').on('click', function(e) {
        $(this).toggleClass('open-usermenu');
        $('.usermenu').slideToggle();
        $('.hide-popup').toggleClass('visibl');
        $('.balance-dropdown').removeClass('open-dropdown');
        return false;
    });

    /*--------------- open usermenu ---------------*/

    /*--------------- hide usermenu ---------------*/
    $(".hide-popup").click(function() {
        $('.js-user').removeClass('open-usermenu');
        $('.usermenu').slideUp();
        $('.js-balance').removeClass('open-balance');
        $('.balance-dropdown').removeClass('open-dropdown');
        $(this).removeClass('visibl');
    });
    /*--------------- hide usermenu ---------------*/

    /*--------------- hidden ticket ---------------*/
    $('.js-ticket-hidden').on('click', function(e) {
        $(this).parents('.profile-ticket__item').addClass('ticket-hidden');
        return false;
    });
    /*--------------- hidden ticket ---------------*/

    /*--------------- hidden active in ticket ---------------*/
    $('.js-ticket-clear').on('click', function(e) {
        $(this).parents('.profile-ticket__item').find('.num-list__item').removeClass('active');
        return false;
    });

    /*--------------- hidden active in ticket ---------------*/

    /*--------------- open balance-dropdown ---------------*/
    $('.js-balance').on('click', function(e) {
        $(this).toggleClass('open-balance');
        $('.balance-dropdown').toggleClass('open-dropdown');
        $('.js-user').removeClass('open-usermenu');
        $('.hide-popup').toggleClass('visibl');
        $('.usermenu').slideUp();
        return false;
    });
    /*--------------- open balance-dropdown ---------------*/

    /*--------------- show video ---------------*/
    $(document).on('click','.js-modal-video',function() {
        var srcVideo = $(this).attr('data-src');
        $(this).parent().addClass("hide-video");
        $(this).parent().find('iframe').attr('src', srcVideo);
        return false;
    });
    /*--------------- show video ---------------*/

    /*--------------- fancybox with video ---------------*/
    if ($('.fancybox-video').length) {
        $('.fancybox-video').fancybox({
            padding: 0,
            wrapCSS: "styled_close",
            helpers: {
                overlay: {
                    locked: false
                }
            },
            tpl: {
                closeBtn: '<div title="Đ—Đ°ĐºÑ€Ñ‹Ñ‚ÑŒ" class="fancybox-item fancybox-close"></div>'
            },
            beforeClose: function() {
                $('.modal-timeline-video').find('iframe').removeAttr('src');
                $('.modal-timeline-video').removeClass('hide-video');
            },
            beforeShow: function(){
                $("body").css({'overflow-y':'hidden'});
            },
            afterClose: function(){
                $("body").css({'overflow-y':'visible'});
            },
            margin: 0
        });
    }
    /*--------------- fancybox with video ---------------*/


    /*--------------- tabs in transactions ---------------*/
    $('.js-switcher').change(function() {
        var el = $(this),
            parents = el.parents('.switcher'),
            id;
        parents.toggleClass('off');
        if (el.is(':checked')) {
            id = parents.find('.f_off').attr('data-href');

        } else {
            id = parents.find('.f_on').attr('data-href');
        }
        $('.js-switcher-tab').removeClass('active');
        $('#' + id + '').addClass('active');
        return false;
    });
    /*--------------- tabs in transactions ---------------*/
    $(".mark__item").addClass('hide');
    $(".modal-play_4 .mark__item").removeClass('hide');
    $(".js-tickets-num").click(function() {
        $(".js-tickets-num").removeClass("active");
        $(this).toggleClass("active");
        $(".box-mark").addClass("show");
        $('.js-mark-item').addClass("hide");
        var tickets_num = $(this).text();
        var ticket = $(".js-mark-item");

        for (i = 0; i < tickets_num; i++) {
            /*$(".js-mark-item")[i].removeClass('hide');*/

            ticket.eq(i).removeClass('hide');

        };
    });

    /*------------------ ticket numbers ------------------*/
    $(".num-list__item").click(function () {
        var activeItems = $(this).parent().find(".active").length;
        var activeItemsMax = $(this).parent().attr("data-max");
        if ((activeItems + 1) >= activeItemsMax) {
            $(this).parent().data("ready", true);
        } else {
            $(this).parent().data("ready", false);
        }

        if (activeItems < activeItemsMax) {
            $(this).toggleClass("active");
            return false;
        } else {
            $(this).removeClass("active");
            return false;
        }
    });
    /*------------------ ticket numbers ------------------*/



    /*----------------------- tabs -----------------------*/
    $('.tabs li a').click(function() {
        $(this).parents('.tab-wrap').find('.tab-cont').addClass('hide-tab');
        $(this).parent().siblings().removeClass('active');
        var id = $(this).attr('href');
        $(id).removeClass('hide-tab');
        $(this).parent().addClass('active');
        if (typeof($(this).attr('data-needed')) != "undefined" && $(this).attr('data-needed').length!=0) {
            tflip.methods.switch_receipts($(this).attr('data-needed'));
        }
        return false;
    });
    /*----------------------- tabs -----------------------*/

    $(".js-update-modal").click(function() {
        $.fancybox.update();
    });

    /*---------------- custom select ---------------------*/
    if ($('.styled').length) {
        $('.styled').styler();
    };
    /*---------------- custom select ---------------------*/
    /*---------------- periodpicker ---------------------*/
    if ($('#periodpickerstart').length) {
        $('#periodpickerstart').periodpicker({
            end: '#periodpickerend',
            lang:current_language,
            yearsLine: false,
            resizeButton: false,
            fullsizeButton: false,
            closeButton: false,
            formatDate: 'DD/MM/YYYY'
        });
    };
    /*---------------- periodpicker ---------------------*/
    /*---------------hidden-rofile-item------------------*/
    $('.js-hidden-rofile-item').click(function() {
        $(this).parents('.box-profile-item').addClass('hidden-rofile-item');
        return false;
    });
    /*--------------show-select --------------------*/ 
     $('.js-show-select').click(function() {
        $(this).parents('.list-check__item').toggleClass('show-select');
        $(this).parent().siblings().removeClass('show-select');
        return false;
    });
    /*--------------show-select --------------------*/ 

    /*--------------tabs page investor------------------*/
    $('.tabs li .js-inves__link').click(function() {
        var id = $(this).attr('data-href');
        $(id).removeClass('hidden-rofile-item');
        //$('html, body').animate({scrollTop: $(id).offset().top - 65}, 1000);
        return false;
    });
    /*---------------tabs page investor ------------------*/
    /*--------------tabs winners------------------*/
    $('.winners-list li a').click(function() {
        $(this).parents('.box-winners-list__row').find('.winners-history').addClass('hide-tab');
        var id = $(this).attr('href');
        $(id).removeClass('hide-tab');
        var elem_id = $(this).parent().parent().attr('id');
        var story_id = '\#'+elem_id;
        text = $(this).parent().find('.ftext').html();
        won = $(this).parent().find('.fwon').html();
        withdrew = $(this).parent().find('.fwithdrew').html();
        unm = $(this).parent().find('.winners-list__title').html();
        $('#full_text').html(text);
        $('.winners-history__title span').html(won);
        $('.winners-history__user').html(unm);
        $('.winners-history__withdrew span').html(withdrew);
        $(id).find('.block-copy a').attr('href','https://trueflip.io/info/winners?lang='+current_language+story_id);
        $(id).find('.profile-tab-item').removeClass('hidden-rofile-item');
        $('html, body').animate({scrollTop: $(id).offset().top - 65}, 1000);
        return false;
    });
    /*---------------tabs winners ------------------*/

     /*----------------------- tabs achive-----------------------*/
    $('.archive-table-title__left a').click(function() {
        $('.archive-tab__link').addClass('active');
        $('.archive-table-title__right').addClass('hide-tab');
        $(this).parents('.archive-table-title').addClass('active-tab');
        $('.archive-table').addClass('hide-tab');
        var id = $(this).attr('data-href');
        $(id).removeClass('hide-tab');
        $(this).removeClass('active');
        return false;
    });
    /*----------------------- tabs archive-----------------------*/
    /*--------------- archive-check ------------------*/
    $('.js-archive-check').click(function() {
        $(this).parent().toggleClass('active');
        return false;
    });
    /*-------------- archive-check --------------------*/

    /*---------------hidden-winners-history------------------*/
    $('.js-hidden-rofile-item').click(function() {
        $(this).parents('.winners-history').addClass('hide-tab');
        return false;
    });
    /*--------------hidden-winners-history --------------------*/
    /*---------------counter investor ------------------*/
    $('.counter__link-prev').click(function () {
        var $input = $(this).parents('.box-counter').find('input');
        if (isNaN(parseInt($input.val()))) {
            $input.val(1);
            $('.inves-tab__spec_header span').html(2);
            $input.change();
            return false;
        }else {
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $('.inves-tab__spec_header span').html(2*count);
            $input.change();
            return false;
        }
     });
     $('.counter__link-next').click(function () {
         var $input = $(this).parents('.box-counter').find('input');
         if (isNaN(parseInt($input.val()))) {
            $input.val(1);
            $('.inves-tab__spec_header span').html(2);
            $input.change();
            return false;
        }else {
            var count = parseInt($input.val()) + 1;
            count = count > ($input.attr("maxlength")) ? ($input.attr("maxlength")) : count;
            $input.val(count);
            $('.inves-tab__spec_header span').html(2*count);
            $input.change();
            return false;
        }
     });
    /*---------------counter investor ------------------*/
    /*---------------hidden-block-video------------------*/
    /*$('.js-hidden-video').click(function() {
        $(this).parents('.box-inves-video').hide();
        $(this).parents('.tab-wrap-with-video').find('.nav-tab-list-wrap').addClass('nav-tab-list-wrap-no-vid');
        
        
    });*/
    /*--------------hidden-block-video --------------------*/

    /*--------------hidden-block-video --------------------*/ 
    /*--------------graphic invest --------------------*/
    if ($('#graphic').length && typeof(TAB5_INFO) != "undefined") {
        $('#graphic').highcharts({
            chart: {
                renderTo: 'graphic',
                zoomType: 'xy',
                type: 'spline',
                backgroundColor: 'transparent',
                marginTop: 20
            },
            title: {
                text: ' '
            },
            subtitle: {
                text: ' '
            },
            xAxis: [{
                labels:{
                    rotation:0,
                }, 
                tickColor: 'tranasparent',             
                categories: TAB5_INFO.categories
            }],
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
                yAxis: [{ // Primary yAxis
                   title: {
                    text: 'Đ’Đ°Ñˆ Đ´Đ¾Ñ…Đ¾Đ´, USD',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '16px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                    y: 0,
                    x: 0,
                    offset: 12
                },
                labels: {
                    style: {
                        color: '#999',
                        fontSize: '14px'
                    }
                },
                title: {
                    text: ' ',
                    style: {
                        color: '#000'
                    }
                },
                opposite: true,
                showEmpty: false,
                min:0,
                /*tickInterval:2*/
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                },
                gridLineColor: '#eeeeee',
                labels: {
                    formatter: function() {
                        return this.value +'  ';
                    },
                    style: {
                        color: '#999',
                        fontSize: '14px'
                    }
                },
                min:0,
                /*tickInterval:250000,*/
                showEmpty: false

            }, { // Tertiary yAxis
                title: {
                    text: ' ',
                    style: {
                        color: '#999'
                    }
                }
            }],
            tooltip: {
                enabled: false
            },
            plotOptions: {
                lineWidth: 2,
                series: {
                    shadow: false
                },          
                spline: {
                    lineWidth: 2,
                    borderRadius: 0,
                    shadow : false,
                    marker: {
                        enabled: false
                    },
                    column: {
                        borderWidth: 0,
                        shadow: false
                    }
                }
            },
            series: [{
                name: '2013',
                color: '#dcd3ec',
                type: 'column',
                yAxis: 1,
                borderWidth: 0,                             
                data: TAB5_INFO.counts

            },
            {
                name: '2015',
                color: '#5d4c84',
                type: 'spline',            
                data: TAB5_INFO.profit
            }
            ]
        });
    };
    if ($('#graphic2').length && typeof(TAB6_INFO)!= "undefined") {

        $('#graphic2').highcharts({
            chart: {
                renderTo: 'graphic',
                zoomType: 'xy',
                type: 'spline',
                backgroundColor: 'transparent',
                marginTop: 40
            },
            title: {
                text: ' '
            },
            subtitle: {
                text: ' '
            },
            xAxis: [{
                labels:{
                    rotation:0,
                }, 
                tickColor: 'tranasparent',             
                categories: TAB6_INFO.categories
            }],
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
                yAxis: [{ // Primary yAxis
                   title: {
                    text: 'Đ’Đ°Ñˆ Đ´Đ¾Ñ…Đ¾Đ´, USD',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                    y: 0,
                    x: 0,
                    offset: 12
                },
                labels: {
                    style: {
                        color: '#999'
                    }
                },
                title: {
                    text: ' ',
                    style: {
                        color: '#000'
                    }
                },
                opposite: true,
                showEmpty: false,
                min:0
                /*tickInterval:2*/
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                },
                gridLineColor: '#eeeeee',
                labels: {
                    formatter: function() {
                        return this.value +'  ';
                    },
                    style: {
                        color: '#999'
                    }
                },
                min:0,
                /*tickInterval:250000,*/
                showEmpty: false

            }, { // Tertiary yAxis
                title: {
                    text: ' ',
                    style: {
                        color: '#999'
                    }
                }
            }],
            tooltip: {
                enabled: false
            },
            plotOptions: {
                lineWidth: 2,
                series: {
                    shadow: false
                },          
                spline: {
                    lineWidth: 2,
                    borderRadius: 0,
                    shadow : false,
                    marker: {
                        enabled: false
                    },
                    column: {
                        borderWidth: 0,
                        shadow: false
                    }
                }
            },
            series: [{
                name: '2013',
                color: '#dcd3ec',
                type: 'column',
                yAxis: 1,
                borderWidth: 0,                             
                data: TAB6_INFO.counts

            },
            {
                name: '2015',
                color: '#5d4c84',
                type: 'spline',            
                data: TAB6_INFO.profit
            }
            ]
        });
    };
    if ($('#graphic3').length && typeof(TAB7_INFO) != "undefined") {
        $('#graphic3').highcharts({
            chart: {
                renderTo: 'graphic',
                zoomType: 'xy',
                type: 'spline',
                backgroundColor: 'transparent',
                marginTop: 40
            },
            title: {
                text: ' '
            },
            subtitle: {
                text: ' '
            },
            xAxis: [{
                labels:{
                    rotation:0,
                }, 
                tickColor: 'tranasparent',             
                categories: TAB7_INFO.categories
            }],
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
                yAxis: [{ // Primary yAxis
                   title: {
                    text: 'Đ’Đ°Ñˆ Đ´Đ¾Ñ…Đ¾Đ´, USD',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                    y: 0,
                    x: 0,
                    offset: 12
                },
                labels: {
                    style: {
                        color: '#999'
                    }
                },
                title: {
                    text: ' ',
                    style: {
                        color: '#000'
                    }
                },
                opposite: true,
                showEmpty: false,
                min:0,
                /*tickInterval:2*/
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                },
                gridLineColor: '#eeeeee',
                labels: {
                    formatter: function() {
                        return this.value +'  ';
                    },
                    style: {
                        color: '#999'
                    }
                },
                min:0,
                /*tickInterval:250000,*/
                showEmpty: false

            }, { // Tertiary yAxis
                title: {
                    text: ' ',
                    style: {
                        color: '#999'
                    }
                }
            }],
            tooltip: {
                enabled: false
            },
            plotOptions: {
                lineWidth: 2,
                series: {
                    shadow: false
                },          
                spline: {
                    lineWidth: 2,
                    borderRadius: 0,
                    shadow : false,
                    marker: {
                        enabled: false
                    },
                    column: {
                        borderWidth: 0,
                        shadow: false
                    }
                }
            },
            series: [{
                name: '2013',
                color: '#dcd3ec',
                type: 'column',
                yAxis: 1,
                borderWidth: 0,                             
                data: TAB7_INFO.counts

            },
            {
                name: '2015',
                color: '#5d4c84',
                type: 'spline',            
                data: TAB7_INFO.profit
            }
            ]
        });
    };
    if ($('#graphic4').length) {
        $('#graphic4').highcharts({
            chart: {
                renderTo: 'graphic',
                zoomType: 'xy',
                type: 'spline',
                backgroundColor: 'transparent',
                marginTop: 40
            },
            title: {
                text: ' '
            },
            subtitle: {
                text: ' '
            },
            xAxis: [{
                labels:{
                    rotation:0,
                }, 
                tickColor: 'tranasparent',             
                categories: ['Đ”Đ•Đ 16', 'Đ¯ĐĐ’ 17', 'Đ¤Đ•Đ’ 17', 'ĐœĐĐ  17', 'ĐĐŸĐ  17', 'ĐœĐĐ™ 17',
                'Đ˜Đ®Đ 17', 'Đ˜Đ®Đ› 17', 'ĐĐ’Đ“ 17', 'Đ¡Đ•Đ 17', 'ĐĐĐ¢ 17', 'ĐĐĐ¯ 17', ]
            }],
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
                yAxis: [{ // Primary yAxis
                   title: {
                    text: 'Đ’Đ°Ñˆ Đ´Đ¾Ñ…Đ¾Đ´, USD',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                    y: 0,
                    x: 0,
                    offset: 12
                },
                labels: {
                    style: {
                        color: '#999'
                    }
                },
                title: {
                    text: ' ',
                    style: {
                        color: '#000'
                    }
                },
                opposite: true,
                showEmpty: false,
                min:0,
                tickInterval:2
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                },
                gridLineColor: '#eeeeee',
                labels: {
                    formatter: function() {
                        return this.value +'  ';
                    },
                    style: {
                        color: '#999'
                    }
                },
                min:0,
                tickInterval:250000,
                showEmpty: false

            }, { // Tertiary yAxis
                title: {
                    text: ' ',
                    style: {
                        color: '#999'
                    }
                }
            }],
            tooltip: {
                enabled: false
            },
            plotOptions: {
                lineWidth: 2,
                series: {
                    shadow: false
                },          
                spline: {
                    lineWidth: 2,
                    borderRadius: 0,
                    shadow : false,
                    marker: {
                        enabled: false
                    },
                    column: {
                        borderWidth: 0,
                        shadow: false
                    }
                }
            },
            series: [{
                name: '2013',
                color: '#dcd3ec',
                type: 'column',
                yAxis: 1,
                borderWidth: 0,                             
                data: [500000, 110000, 200000, 200000, 200000, 1200000, 1510000, 1200000, 1900000, 200000,220000, 6200000, ]

            },
            {
                name: '2015',
                color: '#5d4c84',
                type: 'spline',            
                data: [1.5, 2, 6.2, 5.5, 8.6, 5, 6.2, 3.5, 5.6, 5.6,8.8, 8.1, ]
            }
            ]
        });
    };

    /*--------------graphic invest --------------------*/
    /*-------------- profile-table- combination --------------------*/
    $(".js-more-comb").click(function() {
        $(this).parent().find(".js-more-comb-list").toggleClass('comb-visib');
        $(this).toggleClass('active');
        $(this).parent().parent().parent().find('.woncombination').toggle(300);
        $(this).parents('.profile-table-list__item').toggleClass('comb-visib');
        
        return false;
    });
    /*-------------- profile-table- combination --------------------*/

    $(".js-chesk").click(function() {
        $(this).parent().find(".js-check-num").slideToggle();
        return false;
    });

    /*--------------marketing plan show details --------------------*/
    $('.js-marketing-plan-link').on('click', function (e) {
        e.preventDefault();

        var index = $(this).parent().parent().index(),
            block = $('.js-marketing-plan-details').not('.mob');

        var blockToShow = $(".js-marketing-plan-details[data-id='" + index +"']").not('.mob');

        if($(blockToShow).hasClass('active')){
            $(blockToShow).removeClass('active').slideUp();
            $(this).removeClass('open');
        } else {
            $(block).slideUp().removeClass('active');
            $(blockToShow).addClass('active').slideDown();
            $(this).addClass('open');
        }
    });

    function deleteClassMob(){
        var winWidth = $(window).width();

        if (winWidth < 640) {
            if ($('.js-marketing-plan-details').hasClass('mob')) {
                $('.js-marketing-plan-details').removeClass('mob').addClass('mob-active');
            }
        } else if (winWidth >= 640) {
            if ($('.js-marketing-plan-details').hasClass('mob-active')) {
                $('.js-marketing-plan-details').removeClass('mob-active').addClass('mob');
            }
        }
    }
    deleteClassMob();
    $(window).resize(deleteClassMob);
    /*-------------- marketing plan show details --------------------*/

    if($.fn.slider){
        /*-------------- offering-range-slider --------------------*/
        //$("#offering-range-slider").slider({
        //    ticks: [0, 25, 50, 75, 100],
        //    ticks_labels: ['0', '25 BTC', '50 BTC', '75 BTC', '100 BTC'],
        //    ticks_snap_bounds: 30,
        //    tooltip: 'hide',
        //    enabled: false,
        //    value: soldTokens
        //});
        /*-------------- offering-range-slider --------------------*/
        /*-------------- profit-range-slider --------------------*/
        var profits = {
            0: {
                'monthly expences': '$ 99 873',
                'number tickets month': '324 000',
                'revenue': '$ 87 888',
                'monthly profit': '$ 1.05',
                'tockens': '10',
                'year profit': '$ 10.5'
            },
            50000: {
                'monthly expences': '$ 332 910',
                'number tickets month': '1 080 000',
                'revenue': '$ 292 961',
                'monthly profit': '$ 3.52',
                'tockens': '10',
                'year profit': '$ 35.20'
            },
            100000: {
                'monthly expences': '$ 665 820',
                'number tickets month': '2 160 000',
                'revenue': '$ 585 992',
                'monthly profit': '$ 7.03',
                'tockens': '10',
                'year profit': '$ 70.3'
            },
            250000: {
                'monthly expences': '$ 1 664 550',
                'number tickets month': '5 400 000',
                'revenue': '$ 1 464 804',
                'monthly profit': '$ 17.58',
                'tockens': '10',
                'year profit': '$ 175.8'
            },
            500000: {
                'monthly expences': '$ 3 329 100',
                'number tickets month': '10 800 000',
                'revenue': '$ 2 929 608',
                'monthly profit': '$ 35.16',
                'tockens': '10',
                'year profit': '$ 351.6'
            }
        };
       
        ///,
           // 1000000: {
             //   'monthly expences': '$ 26 632 800',
               // 'number tickets month': '21 600 000',
                //'revenue': '$ 3 302 467',
                //'monthly profit': '$ 3.30',
                //'tockens': '10',
                //'year profit': '$ 475.56'
            //}*/
        $("#profit-range-slider").slider({
            ticks: [0, 50000, 100000, 250000],//, 500000],// 1000000],
            //ticks_labels: ['100 000', '500 000', '1 000 000'],
            ticks_labels: ['15 000', '50 000','100 000','250 000'],//, '500 000'],
            ticks_snap_bounds: 30,
            tooltip: 'hide',
            value: 100000,
            //step: 50000
        });
        $("#profit-range-slider").on('slideStop', function (slideEvt) {
            var currentVal = slideEvt.value;
            if (currentVal<25000) {
                currentVal = 0;
            }else if (currentVal <=50000 && currentVal > 25000) {
                currentVal = 50000;
            }else if (currentVal<75000 && currentVal > 50000) {
                currentVal = 50000;
            }else if (currentVal <=100000 && currentVal > 75000) {
                currentVal = 100000;
            }else if (currentVal <175000 && currentVal > 100000) {
                currentVal = 100000;
            }else if (currentVal <=250000 && currentVal > 175000) {
                currentVal = 250000;
            }else if (currentVal < 375000 && currentVal > 250000) {
                currentVal = 250000;
            }else {
                currentVal = 500000;
            }
            setCalcResults(currentVal);
        });
        $('#js-profit-select-styler li').click(function () {

            var selectVal = $('#js-profit-select').val();

            setCalcResults(selectVal);
        });
        function getInitValue(){
            var initValue = $("#profit-range-slider").slider('getValue');

            setCalcResults(initValue);
        }
        getInitValue();
        function setCalcResults(val){
            if ($(document).find('.js-monthly-expences').length) {
                $('.js-monthly-expences').text(profits[val]["monthly expences"]);
                $('.js-number-tickets-month').text(profits[val]["number tickets month"]);
                $('.js-revenue').text(profits[val]["revenue"]);
                $('.js-monthly-profit').text(profits[val]["monthly profit"]);
                $('.js-tockens').text(profits[val]["tockens"]);
                $('.js-year-profit').text(profits[val]["year profit"]);
            }
        }
        /*-------------- profit-range-slider --------------------*/
    }

    function get_ticket(selector) {
        var ticket = {};
        selector.find(".num-list__item.active").each(function (index) {
            ticket['n' + (index + 1)] = $(this).text();
            ticket['address'] = $('.balance-dropdown__text').eq(0).text();
            ticket['payout_address'] = $('.balance-dropdown__text').eq(0).text();
            ticket['amount'] = 0.001;
        });
        ticket['id'] = selector.parent().attr('id');
        return ticket;
    }
    
    //$('body').on('click touchend','#pay_for_tickets',function () {
    //    var tickets = [];
    //    $(document).find('.box-ticket').each(function () {
    //        if ($(this).parent().hasClass('ready_to_pay')) {
                
    //        }else {
    //            if($(this).attr('data-ready')) {
    //                tickets.push(get_ticket($(this)));
    //            }
    //        }
    //    });
    //    console.dir(tickets);
    //    tflip.methods.loading.show();
    //    var btn = $(this);
    //    setTimeout(function(){
    //        $.ajax({
    //            url: "/ajax/tickets",
    //            method: "POST",
    //            data: {'tickets':tickets},
    //            dataType:'json',
    //            cache: false,
    //            success: function (msg) {
    //                tflip.methods.loading.hide();
    //                if (typeof(msg.ok) != "undefined") {
    //                    var success_tik = msg.ok;
    //                    console.log(success_tik);
    //                    for (var i=0; i<success_tik.length; i++) {
    //                        $(document).find('#'+success_tik[i]).remove();
    //                        if (typeof yaCounter44108249 != "undefined") {
    //                            yaCounter44108249.reachGoal('ticket_purchased');
    //                        }
    //                        if (typeof ga != "undefined") {
    //                            ga('send', 'event', 'profile', 'purchase', 'ticket');
    //                        }
    //                    }
    //                }
    //                if (msg.ans==1) {
                        
    //                    if (typeof(msg.draw_id) != 'undefined') {
    //                        window.location.href = '/profile/receipts/'+msg.draw_id;
    //                    }else {
    //                        window.location.href = "/profile";
    //                    }
    //                }else {
    //                    if (typeof(msg.err)!="undefined") {
    //                        tflip.methods.notification(msg.err,'error');
    //                    }else {
    //                        window.location.href = "/profile";
    //                    }
    //                }

    //            },
    //            error: function(msg){
    //                tflip.methods.loading.hide();
    //                tflip.methods.notification(msg.err,'error');

    //            }
    //        });
    //    },300);
    //    return false;
    //});
    //$('body').on('click touchend','#reserve_tickets',function () {
    //    var tickets = [];
    //    $('.box-ticket').each(function () {
    //        if ($(this).parent().hasClass('ready_to_pay')) {
                
    //        }else {
    //            if($(this).data('ready')) {
    //                tickets.push(get_ticket($(this)));
    //            }
    //        }
    //    });
    //    var btn = $(this);
    //    tflip.methods.loading.show();
    //    setTimeout(function(){
    //        $.ajax({
    //            url: "/ajax/reserve_tickets",
    //            method: "POST",
    //            data: {'tickets':tickets},
    //            dataType:'json',
    //            cache: false,
    //            success: function (msg) {
    //                tflip.methods.loading.hide();
    //                if (typeof(msg.ok) != "undefined") {
    //                    var success_tik = msg.ok;
    //                    console.log(success_tik);
    //                    for (var i=0; i<success_tik.length; i++) {
    //                        $(document).find('#'+success_tik[i]).remove();
    //                    }
    //                }
    //                if (msg.ans==1) {
    //                    if (typeof(msg.success_msg) != "undefined") {
    //                        tflip.methods.notification(msg.success_msg,'success');
    //                    }
    //                    setTimeout(function(){
    //                        window.location.href = "/profile";
    //                    },2000);
    //                }else {
    //                    tflip.methods.notification(msg.err,'error');
    //                }
    //            },
    //            error: function(msg){
    //                tflip.methods.loading.hide();
    //                tflip.methods.notification(msg.err,'error');
    //            }
    //        });
    //    });
    //    return false;
    //});
    /*----------------------- ticket buy buttons -----------------------*/

    $(".js-update-modal").click(function() {
        $.fancybox.update();
    });

    /*---------------- custom select ---------------------*/
    if ($('.styled').length) {
        $('.styled').styler();
    };
    /*---------------- custom select ---------------------*/

    $(".js-chesk").click(function() {
        $(this).parent().find(".js-check-num").slideToggle();
        return false;
    });

    

    $('.js-logout').click(function () {
        $('#logout_form').submit();
        return false;
    });



    /* Transactions */
    $('.period_picker_show ').click(function () {
        tflip.methods.loading.show();
        $('#filtered_by_date').html('');    
        $.ajax({
             async: true,
             url: '/ajax/tickets_by_date',
             type:  'POST',
             cache: false,
             data: {'from':$('#periodpickerstart').val(),'to':$('#periodpickerend').val()},
             success:  function(msg){ 
                 tflip.methods.loading.hide();
                 $('#filtered_by_date').html(msg);
             },
             error:function() {
                 tflip.methods.loading.hide();
             }
        });
    });
});

var handler = function() {
    var height_win = $(window).height();
        
    

   
       
    

};

$(window).bind('load', handler);
$(window).bind('resize', handler);