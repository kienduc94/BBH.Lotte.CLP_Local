/*! clipboard.js v1.6.0 Licensed MIT Â© Zeno Rocha */
!function(e){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{var t;t="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,t.Clipboard=e()}}(function(){var e,t,n;return function e(t,n,o){function i(a,c){if(!n[a]){if(!t[a]){var l="function"==typeof require&&require;if(!c&&l)return l(a,!0);if(r)return r(a,!0);var u=new Error("Cannot find module '"+a+"'");throw u.code="MODULE_NOT_FOUND",u}var s=n[a]={exports:{}};t[a][0].call(s.exports,function(e){var n=t[a][1][e];return i(n?n:e)},s,s.exports,e,t,n,o)}return n[a].exports}for(var r="function"==typeof require&&require,a=0;a<o.length;a++)i(o[a]);return i}({1:[function(e,t,n){function o(e,t){for(;e&&e.nodeType!==i;){if(e.matches(t))return e;e=e.parentNode}}var i=9;if(Element&&!Element.prototype.matches){var r=Element.prototype;r.matches=r.matchesSelector||r.mozMatchesSelector||r.msMatchesSelector||r.oMatchesSelector||r.webkitMatchesSelector}t.exports=o},{}],2:[function(e,t,n){function o(e,t,n,o,r){var a=i.apply(this,arguments);return e.addEventListener(n,a,r),{destroy:function(){e.removeEventListener(n,a,r)}}}function i(e,t,n,o){return function(n){n.delegateTarget=r(n.target,t),n.delegateTarget&&o.call(e,n)}}var r=e("./closest");t.exports=o},{"./closest":1}],3:[function(e,t,n){n.node=function(e){return void 0!==e&&e instanceof HTMLElement&&1===e.nodeType},n.nodeList=function(e){var t=Object.prototype.toString.call(e);return void 0!==e&&("[object NodeList]"===t||"[object HTMLCollection]"===t)&&"length"in e&&(0===e.length||n.node(e[0]))},n.string=function(e){return"string"==typeof e||e instanceof String},n.fn=function(e){var t=Object.prototype.toString.call(e);return"[object Function]"===t}},{}],4:[function(e,t,n){function o(e,t,n){if(!e&&!t&&!n)throw new Error("Missing required arguments");if(!c.string(t))throw new TypeError("Second argument must be a String");if(!c.fn(n))throw new TypeError("Third argument must be a Function");if(c.node(e))return i(e,t,n);if(c.nodeList(e))return r(e,t,n);if(c.string(e))return a(e,t,n);throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList")}function i(e,t,n){return e.addEventListener(t,n),{destroy:function(){e.removeEventListener(t,n)}}}function r(e,t,n){return Array.prototype.forEach.call(e,function(e){e.addEventListener(t,n)}),{destroy:function(){Array.prototype.forEach.call(e,function(e){e.removeEventListener(t,n)})}}}function a(e,t,n){return l(document.body,e,t,n)}var c=e("./is"),l=e("delegate");t.exports=o},{"./is":3,delegate:2}],5:[function(e,t,n){function o(e){var t;if("SELECT"===e.nodeName)e.focus(),t=e.value;else if("INPUT"===e.nodeName||"TEXTAREA"===e.nodeName){var n=e.hasAttribute("readonly");n||e.setAttribute("readonly",""),e.select(),e.setSelectionRange(0,e.value.length),n||e.removeAttribute("readonly"),t=e.value}else{e.hasAttribute("contenteditable")&&e.focus();var o=window.getSelection(),i=document.createRange();i.selectNodeContents(e),o.removeAllRanges(),o.addRange(i),t=o.toString()}return t}t.exports=o},{}],6:[function(e,t,n){function o(){}o.prototype={on:function(e,t,n){var o=this.e||(this.e={});return(o[e]||(o[e]=[])).push({fn:t,ctx:n}),this},once:function(e,t,n){function o(){i.off(e,o),t.apply(n,arguments)}var i=this;return o._=t,this.on(e,o,n)},emit:function(e){var t=[].slice.call(arguments,1),n=((this.e||(this.e={}))[e]||[]).slice(),o=0,i=n.length;for(o;o<i;o++)n[o].fn.apply(n[o].ctx,t);return this},off:function(e,t){var n=this.e||(this.e={}),o=n[e],i=[];if(o&&t)for(var r=0,a=o.length;r<a;r++)o[r].fn!==t&&o[r].fn._!==t&&i.push(o[r]);return i.length?n[e]=i:delete n[e],this}},t.exports=o},{}],7:[function(t,n,o){!function(i,r){if("function"==typeof e&&e.amd)e(["module","select"],r);else if("undefined"!=typeof o)r(n,t("select"));else{var a={exports:{}};r(a,i.select),i.clipboardAction=a.exports}}(this,function(e,t){"use strict";function n(e){return e&&e.__esModule?e:{default:e}}function o(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}var i=n(t),r="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},a=function(){function e(e,t){for(var n=0;n<t.length;n++){var o=t[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(e,o.key,o)}}return function(t,n,o){return n&&e(t.prototype,n),o&&e(t,o),t}}(),c=function(){function e(t){o(this,e),this.resolveOptions(t),this.initSelection()}return a(e,[{key:"resolveOptions",value:function e(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};this.action=t.action,this.emitter=t.emitter,this.target=t.target,this.text=t.text,this.trigger=t.trigger,this.selectedText=""}},{key:"initSelection",value:function e(){this.text?this.selectFake():this.target&&this.selectTarget()}},{key:"selectFake",value:function e(){var t=this,n="rtl"==document.documentElement.getAttribute("dir");this.removeFake(),this.fakeHandlerCallback=function(){return t.removeFake()},this.fakeHandler=document.body.addEventListener("click",this.fakeHandlerCallback)||!0,this.fakeElem=document.createElement("textarea"),this.fakeElem.style.fontSize="12pt",this.fakeElem.style.border="0",this.fakeElem.style.padding="0",this.fakeElem.style.margin="0",this.fakeElem.style.position="absolute",this.fakeElem.style[n?"right":"left"]="-9999px";var o=window.pageYOffset||document.documentElement.scrollTop;this.fakeElem.style.top=o+"px",this.fakeElem.setAttribute("readonly",""),this.fakeElem.value=this.text,document.body.appendChild(this.fakeElem),this.selectedText=(0,i.default)(this.fakeElem),this.copyText()}},{key:"removeFake",value:function e(){this.fakeHandler&&(document.body.removeEventListener("click",this.fakeHandlerCallback),this.fakeHandler=null,this.fakeHandlerCallback=null),this.fakeElem&&(document.body.removeChild(this.fakeElem),this.fakeElem=null)}},{key:"selectTarget",value:function e(){this.selectedText=(0,i.default)(this.target),this.copyText()}},{key:"copyText",value:function e(){var t=void 0;try{t=document.execCommand(this.action)}catch(e){t=!1}this.handleResult(t)}},{key:"handleResult",value:function e(t){this.emitter.emit(t?"success":"error",{action:this.action,text:this.selectedText,trigger:this.trigger,clearSelection:this.clearSelection.bind(this)})}},{key:"clearSelection",value:function e(){this.target&&this.target.blur(),window.getSelection().removeAllRanges()}},{key:"destroy",value:function e(){this.removeFake()}},{key:"action",set:function e(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"copy";if(this._action=t,"copy"!==this._action&&"cut"!==this._action)throw new Error('Invalid "action" value, use either "copy" or "cut"')},get:function e(){return this._action}},{key:"target",set:function e(t){if(void 0!==t){if(!t||"object"!==("undefined"==typeof t?"undefined":r(t))||1!==t.nodeType)throw new Error('Invalid "target" value, use a valid Element');if("copy"===this.action&&t.hasAttribute("disabled"))throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');if("cut"===this.action&&(t.hasAttribute("readonly")||t.hasAttribute("disabled")))throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');this._target=t}},get:function e(){return this._target}}]),e}();e.exports=c})},{select:5}],8:[function(t,n,o){!function(i,r){if("function"==typeof e&&e.amd)e(["module","./clipboard-action","tiny-emitter","good-listener"],r);else if("undefined"!=typeof o)r(n,t("./clipboard-action"),t("tiny-emitter"),t("good-listener"));else{var a={exports:{}};r(a,i.clipboardAction,i.tinyEmitter,i.goodListener),i.clipboard=a.exports}}(this,function(e,t,n,o){"use strict";function i(e){return e&&e.__esModule?e:{default:e}}function r(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function a(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function c(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}function l(e,t){var n="data-clipboard-"+e;if(t.hasAttribute(n))return t.getAttribute(n)}var u=i(t),s=i(n),f=i(o),d=function(){function e(e,t){for(var n=0;n<t.length;n++){var o=t[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(e,o.key,o)}}return function(t,n,o){return n&&e(t.prototype,n),o&&e(t,o),t}}(),h=function(e){function t(e,n){r(this,t);var o=a(this,(t.__proto__||Object.getPrototypeOf(t)).call(this));return o.resolveOptions(n),o.listenClick(e),o}return c(t,e),d(t,[{key:"resolveOptions",value:function e(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};this.action="function"==typeof t.action?t.action:this.defaultAction,this.target="function"==typeof t.target?t.target:this.defaultTarget,this.text="function"==typeof t.text?t.text:this.defaultText}},{key:"listenClick",value:function e(t){var n=this;this.listener=(0,f.default)(t,"click",function(e){return n.onClick(e)})}},{key:"onClick",value:function e(t){var n=t.delegateTarget||t.currentTarget;this.clipboardAction&&(this.clipboardAction=null),this.clipboardAction=new u.default({action:this.action(n),target:this.target(n),text:this.text(n),trigger:n,emitter:this})}},{key:"defaultAction",value:function e(t){return l("action",t)}},{key:"defaultTarget",value:function e(t){var n=l("target",t);if(n)return document.querySelector(n)}},{key:"defaultText",value:function e(t){return l("text",t)}},{key:"destroy",value:function e(){this.listener.destroy(),this.clipboardAction&&(this.clipboardAction.destroy(),this.clipboardAction=null)}}],[{key:"isSupported",value:function e(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:["copy","cut"],n="string"==typeof t?[t]:t,o=!!document.queryCommandSupported;return n.forEach(function(e){o=o&&!!document.queryCommandSupported(e)}),o}}]),t}(s.default);e.exports=h})},{"./clipboard-action":7,"good-listener":4,"tiny-emitter":6}]},{},[8])(8)});

/** TF App Methods **/
Array.prototype.clone = function() {
    //return this.slice(0);
};

if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(searchElement, fromIndex) {
    var k;
    if (this == null) {
      throw new TypeError('"this" is null or not defined');
    }

    var O = Object(this);
    var len = O.length >>> 0;

    if (len === 0) {
      return -1;
    }
    var n = +fromIndex || 0;
    if (Math.abs(n) === Infinity) {
      n = 0;
    }
    if (n >= len) {
      return -1;
    }
    k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
    while (k < len) {
      if (k in O && O[k] === searchElement) {
        return k;
      }
      k++;
    }
    return -1;
  };
}
var tflip = {};
var typingTimer;
var typingTimer_repeat;
tflip.lang = {
    ru: {
        errors : {
            'copied_denied' : 'Đ¡Đ¸ÑÑ‚ĐµĐ¼Đ° Đ±ĐµĐ·Đ¾Đ¿Đ°ÑĐ½Đ¾ÑÑ‚Đ¸ Đ²Đ°ÑˆĐµĐ³Đ¾ Đ±Ñ€Đ°ÑƒĐ·ĐµÑ€Đ° Đ½Đµ Đ´Đ°ĐµÑ‚ Đ´Đ¾ÑÑ‚ÑƒĐ¿ Đº Đ±ÑƒÑ„ĐµÑ€Ñƒ Đ¾Đ±Đ¼ĐµĐ½Đ°. ĐŸĐ¾Đ¿Ñ€Đ¾Đ±ÑƒĐ¹Ñ‚Đµ Đ²Ñ‹Đ´ĐµĐ»Đ¸Ñ‚ÑŒ Ñ‚ĐµĐºÑÑ‚ Đ¸ ÑĐºĐ¾Đ¿Đ¸Ñ€Đ¾Đ²Đ°Ñ‚ÑŒ ĐµĐ³Đ¾ Đ¾Đ±Ñ‹Ñ‡Đ½Ñ‹Đ¼ Đ¼ĐµÑ‚Đ¾Đ´Đ¾Đ¼.',
            'try_later': 'ĐŸÑ€Đ¾Đ¸Đ·Đ¾ÑˆĐ»Đ° Đ¾ÑˆĐ¸Đ±ĐºĐ°. ĐŸĐ¾Đ²Ñ‚Đ¾Ñ€Đ¸Ñ‚Đµ Đ’Đ°ÑˆÑƒ Đ¿Đ¾Đ¿Ñ‹Ñ‚ĐºÑƒ Đ¿Đ¾Đ·Đ¶Đµ',
            'server_error': 'ĐŸÑ€Đ¾Đ¸Đ·Đ¾ÑˆĐµĐ» ÑĐ±Đ¾Đ¹ Đ¿Ñ€Đ¸ Đ¾Đ±Ñ€Đ°Ñ‰ĐµĐ½Đ¸Đ¸ Đº ÑĐµÑ€Đ²ĐµÑ€Ñƒ. ĐŸĐ¾Đ²Ñ‚Đ¾Ñ€Đ¸Ñ‚Đµ Đ’Đ°ÑˆÑƒ Đ¿Đ¾Đ¿Ñ‹Ñ‚ĐºÑƒ Đ¿Đ¾Đ·Đ¶Đµ',
            'chp_too_short' : 'Đ¡Đ»Đ¸ÑˆĐºĐ¾Đ¼ ĐºĐ¾Ñ€Đ¾Ñ‚ĐºĐ¸Đ¹ Đ½Đ¾Đ²Ñ‹Đ¹ Đ¿Đ°Ñ€Đ¾Đ»ÑŒ',
            'chp_old_new_diff' : 'Đ¡Ñ‚Đ°Ñ€Ñ‹Đ¹ Đ¸ Đ½Đ¾Đ²Ñ‹Đ¹ Đ¿Đ°Ñ€Đ¾Đ»Đ¸ Đ½Đµ Đ´Đ¾Đ»Đ¶Đ½Ñ‹ ÑĐ¾Đ²Đ¿Đ°Đ´Đ°Ñ‚ÑŒ',
            'chp_pass_not_eq' : 'ĐĐ¾Đ²Ñ‹Đ¹ Đ¿Đ°Ñ€Đ¾Đ»ÑŒ Đ½Đµ ÑĐ¾Đ²Đ¿Đ°Đ´Đ°ĐµÑ‚ Ñ Đ¿Đ¾Đ²Ñ‚Đ¾Ñ€Đ½Đ¾ Đ²Đ²ĐµĐ´ĐµĐ½Đ½Ñ‹Đ¼',
            'chp_repeat_new' : 'ĐŸĐ¾Đ²Ñ‚Đ¾Ñ€Đ¸Ñ‚Đµ Đ½Đ¾Đ²Ñ‹Đ¹ Đ¿Đ°Ñ€Đ¾Đ»ÑŒ',
            'chp_enter_new' :'Đ’Đ²ĐµĐ´Đ¸Ñ‚Đµ Đ½Đ¾Đ²Ñ‹Đ¹ Đ¿Đ°Ñ€Đ¾Đ»ÑŒ',
            'chp_enter_old' : 'Đ’Đ²ĐµĐ´Đ¸Ñ‚Đµ ÑÑ‚Đ°Ñ€Ñ‹Đ¹ Đ¿Đ°Ñ€Đ¾Đ»ÑŒ',
            'wd_min' : 'Đ¡ Ñ†ĐµĐ»ÑŒÑ Đ¼Đ¸Đ½Đ¸Đ¼Đ¸Đ·Đ°Ñ†Đ¸Đ¸ ĐºĐ¾Đ¼Đ¸ÑÑĐ¸Đ¸ Đ·Đ° Ñ‚Ñ€Đ°Đ½Đ·Đ°ĐºÑ†Đ¸Ñ Đ¼Đ¸Đ½Đ¸Đ¼Đ°Đ»ÑŒĐ½Đ°Ñ ÑÑƒĐ¼Đ¼Đ° Đ´Đ»Ñ ÑĐ½ÑÑ‚Đ¸Ñ ÑĐ¾ÑÑ‚Đ°Đ²Đ»ÑĐµÑ‚ 0.01 BTC.'
        },
        success: {
            'copied_successfully': 'Đ¡ĐºĐ¾Đ¿Đ¸Ñ€Đ¾Đ²Đ°Đ½Đ¾ ÑƒÑĐ¿ĐµÑˆĐ½Đ¾!',
            'subscribed_successfully': 'ĐŸÑ€Đ¾Đ²ĐµÑ€ÑŒÑ‚Đµ Đ¿Đ¾Ñ‡Ñ‚Ñƒ, Đ¼Ñ‹ Đ¾Ñ‚Đ¿Ñ€Đ°Đ²Đ¸Đ»Đ¸ Đ²Đ°Đ¼ Đ¿Đ¸ÑÑŒĐ¼Đ¾ Đ´Đ»Ñ Đ¿Đ¾Đ´Ñ‚Đ²ĐµÑ€Đ¶Đ´ĐµĐ½Đ¸Ñ Đ¾Ñ„Đ¾Ñ€Đ¼Đ»ĐµĐ½Đ¸Ñ Đ¿Đ¾Đ´Đ¿Đ¸ÑĐºĐ¸'
        }
    },
    en: {
        errors : {
            'copied_denied' : 'Security system of your browser blocks the access to the clipboard. Try highlighting the text and copying it directly.',
            'try_later': 'An error occurred. Please try again later.',
            'server_error': 'An error occurred when applying to the server. Please try again later.',
            'chp_too_short' : 'New password is too short',
            'chp_old_new_diff' : 'New password must differ from the old one',
            'chp_pass_not_eq' : 'New password does not match password confirmation',
            'chp_repeat_new' : 'Repeat new password',
            'chp_enter_new' :'Specify new password',
            'chp_enter_old' : 'Specify old password',
            'wd_min' : 'In purpose to minimize transaction fees, the minimum amount to withdraw is 0.01 btc.'
        },
        success: {
            'copied_successfully': 'Copying successful!',
            'subscribed_successfully': 'Check your email, we sent you a letter to confirm your subscription'
        }
    },
    es: {
        errors : {
            'copied_denied' : 'El sistema de seguridad de su navegador no permite el acceso al portapapeles. Trate de seleccionar texto y copiarlo en el mĂ©todo habitual.',
            'try_later': 'Se ha producido un error. Repita su intentarlo mĂ¡s tarde',
            'server_error': 'Se produjo un error al acceder al servidor. Repita su intentarlo mĂ¡s tarde',
            'chp_too_short' : 'Demasiado corta su nueva contraseĂ±a',
            'chp_old_new_diff' : 'ContraseĂ±a vieja y la nueva no coinciden',
            'chp_pass_not_eq' : 'La nueva contraseĂ±a no coincide con la reintroducciĂ³n',
            'chp_repeat_new' : 'Repete su nueva contraseĂ±a',
            'chp_enter_new' :'Introduzca una nueva contraseĂ±a',
            'chp_enter_old' : 'Introduzca su contraseĂ±a vieja',
            'wd_min' : 'Con el propĂ³sito de minimizar los honorarios de transacciĂ³n, la cantidad mĂ­nima a retirar es 0.01 btc.'
        },
        success: {
            'copied_successfully': 'Â¡Copiado con Ă©xito!',
            'subscribed_successfully': 'Revise su correo electrĂ³nico, le enviamos una carta para confirmar su suscripciĂ³n'
        }
    },
    fr: {
        errors : {
            'copied_denied' : "Le systĂ¨me de sĂ©curitĂ© de votre navigateur bloque l'accĂ¨s au presse-papier. Essayez de mettre en surbrillance le texte et de le copier directement.",
            'try_later': "Une erreur s'est produite. Veuillez rĂ©essayer plus tard.",
            'server_error': "Une erreur s'est produite lors de l'application sur le serveur. Veuillez rĂ©essayer plus tard.",
            'chp_too_short' : 'Le nouveau mot de passe est trop court',
            'chp_old_new_diff' : "Le nouveau mot de passe doit diffĂ©rer de l'ancien",
            'chp_pass_not_eq' : 'Le nouveau mot de passe ne correspond pas Ă  la confirmation du mot de passe',
            'chp_repeat_new' : 'RĂ©pĂ©tĂ© le nouveau mot de passe',
            'chp_enter_new' :'SpĂ©cifiez un nouveau mot de passe',
            'chp_enter_old' : "PrĂ©cisez l'ancien mot de passe",
            'wd_min' : 'Afin de minimiser les frais de transaction, le montant minimal Ă  retirer est de 0,01 btc.'
        },
        success: {
            'copied_successfully': 'Copie rĂ©ussie!',
            'subscribed_successfully': 'VĂ©rifiez votre courrier Ă©lectronique, nous vous avons envoyĂ© une lettre pour confirmer votre abonnement'
        }
    },
    de: {
        errors : {
            'copied_denied' : 'Sicherheitssystem Ihres Browsers blockiert den Zugriff auf die Zwischenablage. Versuche, den Text hervorzuheben und direkt zu kopieren.',
            'try_later': 'Ein Fehler ist aufgetreten. Bitte versuchen Sie es spĂ¤ter noch einmal.',
            'server_error': 'Beim Anwenden auf den Server ist ein Fehler aufgetreten. Bitte versuchen Sie es spĂ¤ter noch einmal.',
            'chp_too_short' : 'Neues Passwort ist zu kurz',
            'chp_old_new_diff' : 'Neues Passwort muss von der alten abweichen',
            'chp_pass_not_eq' : 'Neues Passwort stimmt nicht mit der PasswortbestĂ¤tigung Ă¼berein',
            'chp_repeat_new' : 'Wiederhole das neue Passwort',
            'chp_enter_new' :'Neues Passwort angeben',
            'chp_enter_old' : 'Geben Sie das alte Passwort an',
            'wd_min' : 'Um die TransaktionsgebĂ¼hren zu minimieren, betrĂ¤gt der Mindestbetrag 0,01 btc.'
        },
        success: {
            'copied_successfully': 'Kopieren erfolgreich!',
            'subscribed_successfully': 'ĂœberprĂ¼fen Sie Ihre E-Mail, wir haben Ihnen einen Brief zugesandt, um Ihr Abonnement zu bestĂ¤tigen'
        }
    },
    'pt-br': {
        errors : {
            'copied_denied' : 'O sistema de seguranĂ§a do seu navegador bloqueia o acesso Ă  Ă¡rea de transferĂªncia. Tente realĂ§ar o texto e copiĂ¡-lo diretamente.',
            'try_later': 'Um erro ocorreu. Por favor, tente novamente mais tarde.',
            'server_error': 'Ocorreu um erro ao aplicar ao servidor. Por favor, tente novamente mais tarde.',
            'chp_too_short' : 'Nova senha Ă© muito curta',
            'chp_old_new_diff' : 'A nova senha deve ser diferente da antiga',
            'chp_pass_not_eq' : 'Nova senha nĂ£o corresponde Ă  confirmaĂ§Ă£o da senha',
            'chp_repeat_new' : 'Repita a nova senha',
            'chp_enter_new' :'Especifique uma nova senha',
            'chp_enter_old' : 'Especifique a senha antiga',
            'wd_min' : 'Com o objetivo de minimizar as taxas de transaĂ§Ă£o, o valor mĂ­nimo para retirar Ă© 0,01 btc.'
        },
        success: {
            'copied_successfully': 'Copiando com sucesso!',
            'subscribed_successfully': 'Confira seu e-mail, enviamos uma carta para confirmar sua assinatura'
        }
    },
    cn: {
        errors : {
            'copied_denied' : 'ç€è¦½å™¨ç„å®‰å…¨ç³»çµ±æœƒé˜»æ­¢å°å‰ªè²¼æ¿ç„è¨ªå•ă€‚ å˜—è©¦çªå‡ºé¡¯ç¤ºæ–‡æœ¬ä¸¦ç›´æ¥è¤‡è£½ă€‚',
            'try_later': 'ç™¼ç”ŸéŒ¯èª¤ è«‹ç¨å¾Œå†è©¦ă€‚',
            'server_error': 'æ‡‰ç”¨æ–¼æœå‹™å™¨æ™‚ç™¼ç”ŸéŒ¯èª¤ă€‚ è«‹ç¨å¾Œå†è©¦ă€‚',
            'chp_too_short' : 'æ–°å¯†ç¢¼å¤ªçŸ­',
            'chp_old_new_diff' : 'æ–°å¯†ç¢¼å¿…é ˆèˆ‡èˆå¯†ç¢¼ä¸åŒ',
            'chp_pass_not_eq' : 'æ–°å¯†ç¢¼èˆ‡å¯†ç¢¼ç¢ºèªä¸ç¬¦',
            'chp_repeat_new' : 'é‡è¤‡æ–°å¯†ç¢¼',
            'chp_enter_new' :'æŒ‡å®æ–°å¯†ç¢¼',
            'chp_enter_old' : 'æŒ‡å®èˆå¯†ç¢¼',
            'wd_min' : 'ç‚ºäº†ç›¡é‡æ¸›å°‘äº¤æ˜“è²»ç”¨ï¼Œæœ€ä½é€€æ¬¾é‡‘é¡ç‚º0.01 BTCă€‚'
        },
        success: {
            'copied_successfully': 'è¤‡è£½æˆåŸï¼',
            'subscribed_successfully': 'æª¢æŸ¥æ‚¨ç„é›»å­éƒµä»¶ï¼Œæˆ‘å€‘ç™¼çµ¦æ‚¨ä¸€å°ä¿¡ä»¥ç¢ºèªæ‚¨ç„è¨‚é–±'
        }
    }
};
tflip.settings = {
    doneTypingInterval: 1000
};
tflip.init= function(e){
    set_watch = function(){
        $(document).ready(function(){
            tflip.methods.copyToClipboard_handler();
            tflip.methods.btk_recharge_converter();
            tflip.methods.btk_inda_converter();
            tflip.methods.click_tick();
            tflip.methods.change_pass_handlers();
            tflip.methods.text_count_down();
            tflip.methods.bitcoin_to_usd_main_page();
            tflip.methods.slider_index();
            tflip.methods.iprofile_buy_calc();
            tflip.methods.ico_input_change();
            tflip.methods.ico_init_price();
            tflip.methods.switcher();
            tflip.methods.wd_calculate();
            tflip.methods.referral_height();
            tflip.methods.show_pre_ico_banner();
            tflip.methods.show_free_ticket_banner();
            tflip.methods.show_draw_in_process();
            tflip.methods.icobannersrotation();
            tflip.methods.close_alerts();
            tflip.methods.showStory();
            if ($(document).find('.ico_starts_in').length) {
                tflip.methods.preico_countdown();
            }
            if ($(document).find('#modal-play_1112').length) {
                tflip.methods.openFancyBox('#modal-play_1112');
            }
            if (tflip.methods.check_for_cookies()!=true) {
                document.location.href='/nocookies';
            }
            
            setTimeout(function(){
                tflip.methods.checkhash();
                tflip.methods.show_whatsHelp();                
            },1000);
            $(document).on('click','#form_ask_question',function(){
               if (typeof grecaptcha != "undefined") {
                   if (typeof rcAsk == "undefined") {
                       if ($(document).find('#rcask').length) {
                            rcAsk = grecaptcha.render('rcask', {
                              'sitekey' : '6LfqiBsUAAAAAAW0UOWA5YFptRFh7p9gpwOk2SOe',
                              'theme' : 'light',
                              'size': 'normal'
                            });
                        }
                   }
               }
            });
            if (typeof need_to_show_preopened != "undefined" && need_to_show_preopened == 1) {
                tflip.methods.add_ticket_profile();
                setTimeout(function(){
                    tflip.methods.add_ticket_profile();
                    if (window.innerWidth > 1660) {
                        setTimeout(function(){
                            tflip.methods.add_ticket_profile();
                        },300);
                    }
                },300);
                
            }
        });
        $(window).resize(function() {
            setTimeout(function(){
                tflip.methods.referral_height();
            },500);
        }); 
    }
    set_watch();
}
tflip.methods = {
    icobannersrotation:function(){
        if ($(document).find('.ico_banner').length==2) {
            function togglebanners() {
                $('.ico_banner').toggle('medium');
            }
            setInterval(togglebanners,10000);
        }     
        
    },
    showStory:function(){
        if (document.location.pathname=="/info/winners" && document.location.hash!="") {
            var element_id = document.location.hash;
            if (element_id != '#' && $(document).find(element_id).length) {
                $(element_id).find('a').trigger('click');
            }
        }
    },
    checkhash:function(){
        if ($(document).find('#register1').length) {
            if (document.location.hash=='#reg') {
                
                $('.js-modal-in').removeClass('active');
                $('#register1').addClass('active');
                if (typeof(rcReg) == "undefined" && $(document).find('#rcreg').length && typeof (grecaptcha) != "undefined") {
                    rcReg = grecaptcha.render('rcreg', {
                      'sitekey' : '6LfqiBsUAAAAAAW0UOWA5YFptRFh7p9gpwOk2SOe',
                      'theme' : 'light',
                      'size': 'normal'
                    });
                }else {
                    if (typeof (grecaptch) != "undefined") {
                        grecaptcha.reset(rcReg);
                    }
                }
                 $("body").addClass("special-fancy");
                tflip.methods.openFancyBox('#modal-login');
            }
        }
    },
    show_whatsHelp: function() {        
        /*var options = {
            //facebook: "1220442671409808",//"1714413388787568", // Facebook page ID
            //telegram: "TrueFlipBot", // Telegram bot username
            //vkontakte: "trueflip", // VKontakte page name
            email: "info@trueflip.io", // Email
            company_logo_url: "//trueflip.io/img/tfl_logo.jpg", // URL of company logo (png, jpg, gif)
            greeting_message: "Hello, how may we help you? Just send us a message now to get assistance.", // Text of greeting message
            call_to_action: "Message us", // Call to action
            button_color: "#FF6550", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,telegram,vkontakte,email" // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
          */  
    },    
    show_timeline: function(id) {
        tflip.methods.loading.show();
        $.ajax({
            async: true,
            url: '/ajax/draw_timeline',
            type:  'GET',
            cache: false,  
            data: {'draw_id':id},
            success:  function(msg){ 
                tflip.methods.loading.hide();
                if (typeof msg != "undefined" && msg.length) {
                    $('#incomingblock').html(msg);
                    setTimeout(function(){
                        if ($(document).find('#win-rezult__timeline').length) {
                            tflip.methods.openFancyBox('#win-rezult__timeline');
                        }
                    },300);
                }
            },
            error:function() {
                tflip.methods.loading.hide();                    
            } 
        });
    },
    close_alerts: function(){
        $(document).on('touchend click','.tfalert i',function(){
           $(this).parent().slideUp(300);
           setTimeout(function(){ $(this).parent().remove(); },300);
        });
    },
    share: {
        vk: function(url) {
            var link = 'https://vk.com/share.php?url='+url;
            tflip.methods.share.popup(link);
        },
        fb: function(url) {
            var link = 'https://www.facebook.com/sharer/sharer.php?u='+url;
            tflip.methods.share.popup(link);
        },
        tw: function(url) {
            var txt = 'Trueflip - International anonymous blockchain lottery ';
            var link = 'https://twitter.com/intent/tweet?text='+txt+url;
            tflip.methods.share.popup(link);
        },
        gplus: function(url) {
            var link = 'https://plus.google.com/share?url='+url;
            tflip.methods.share.popup(link);
        },
        ok: function(url) {
            var link = 'https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl='+url;
            tflip.methods.share.popup(link);
        },
        popup: function(link) {   
            if (link.replace('info/winners','').length == link.length) {
                tflip.methods.metrik_events_trigger('ref_link_shared_in_SN');
            }
            window.open(link,'','toolbar=0,status=0,width=626,height=436');
            return false;
        }
    },
    referral_height: function(){
        if ($(document).find('.advantages-ref__item').length) {
            $('.advantages-ref__item').removeAttr('style');
            setTimeout(function(){
                var height = 0;
                $('.advantages-ref__item').each(function(){
                    if ($(this).height()>height) {
                        height = $(this).height();
                    }
                });
                if (window.innerWidth>=1370) {
                    $('.advantages-ref__item').css({'height':(height+40)+'px'});                
                }else {
                    $('.advantages-ref__item').css({'height':(height+30)+'px'});                
                }
            },300);
        }
    },
    reset_grecaptch: function(){
        /*if ($(document).find('.g-recaptcha').length && typeof grecaptcha != "undefined") {
            grecaptcha.reset();
        }*/
        if (typeof rcWin != "undefined") {
            grecaptcha.reset(rcWin);
        }
        if (typeof rcPopup != "undefined") {
            grecaptcha.reset(rcPopup);
        }
        if (typeof rcAsk != "undefined") {
            grecaptcha.reset(rcAsk);
        }
        if (typeof rcFeedback != "undefined") {
            grecaptcha.reset(rcFeedback);
        }
        if (typeof rcReg != "undefined") {
            grecaptcha.reset(rcReg);
        }
        if (typeof rcCheck != "undefined") {
            grecaptcha.reset(rcCheck);
        }
    },
    follow_currency_exchanger_link: function(){
        tflip.methods.metrik_events_trigger('follow_currency_exchanger_link');
        
    },
    publishstory: function(){
        $('#story').removeClass('err');
        if ($('#story').val().length>20) {
            tflip.methods.loading.show();
            $.ajax({
                async: true,
                url: '/ajax/publishstory',
                type:  'POST',
                cache: false,  
                dataType: 'json',
                data: $('#winnerstory_form').serialize(),
                success:  function(msg){ 
                    tflip.methods.loading.hide();
                    tflip.methods.reset_grecaptch();
                    if (msg.ans == 1) {
                        $('#winnerstory_form').hide(300); 
                        tflip.methods.notification(msg.success,'success');
                        setTimeout(function(){
                            document.location.href='/profile';
                        },3000);
                    }else {
                        tflip.methods.notification(msg.err,'error');
                    }
                },
                error:function() {
                    tflip.methods.loading.hide();
                    tflip.methods.reset_grecaptch();
                } 
            });
        }else {
            $('#story').addClass('err');
        }
    },
    checkfairness: function(){
        if ($(document).find('#bhash').length) {
            $('#bhash').removeClass('err');
            if ($(document).find('#bhash').val()=='') {
                $('#bhash').addClass('err');
            }else {
                tflip.methods.loading.show();
                $.ajax({
                    async: true,
                    url: '/ajax/checkfairness',
                    type:  'POST',
                    cache: false,  
                    dataType: 'json',
                    data: $('#checkfair').serialize(),
                    success:  function(msg){ 
                        tflip.methods.loading.hide();
                        tflip.methods.reset_grecaptch();
                        if (msg.ans == 1) {
                            if (typeof(msg.results) != "undefined") {
                                $('#show_lottery_results').html(msg.results);
                                $('#bhash').val('');
                                tflip.methods.openFancyBox('#modal-play_8');
                            }
                        }else {
                            tflip.methods.notification(msg.err,'error');
                        }
                    },
                    error:function() {
                        tflip.methods.loading.hide();
                        tflip.methods.reset_grecaptch();
                    } 
                });
            }
        }
        
    },
    filter_profile:function(){
        if ($(document).find('#tr_type').length && $(document).find('#tr_qty').length && $(document).find('#tr_period').length) {
            tflip.methods.loading.show();
            $.ajax({
                async: true,
                url: '/ajax/filter_tf_transactions',
                type:  'POST',
                cache: false,    
                data: {'tr_type':$('#tr_type').val(),'tr_qty':$('#tr_qty').val(),'tr_period':$('#tr_period').val()},
                success:  function(msg){ 
                    tflip.methods.loading.hide();
                    $('#transactions_in_trueflip').html(''+msg+'');
                },
                error:function() {
                    tflip.methods.loading.hide();
                } 
            });
        }
    },
    withdraw: function(){
        if ($(document).find('#wd_amount').length && $(document).find('#wadr').length) {
            $('#wd_amount, #wadr').removeClass('err');
            if ($(document).find('#wd_pass').length) {
                $('#wd_pass').removeClass('err');
            }
            var err = 0;
            if ($('#wd_amount').val()=='') {
                $('#wd_amount').addClass('err');
                err++;
            }else {
                if ($('#wd_amount').val() < 0.01) {
                    $('#wd_amount').addClass('err');
                    tflip.methods.notification(tflip.lang[current_language].errors.wd_min,'error');
                    err++;
                }
            }
            if ($('#wadr').val()=='') {
                $('#wadr').addClass('err');
                err++;
            }
            if ($(document).find('#wd_pass').length) {
                if ($('#wd_pass').val()=='') {
                    $('#wd_pass').addClass('err');
                    err++;
                }
            }
            valid_addr = tflip.methods.validate_BTC_WA($('#wadr').val());
            if (valid_addr == false) {
                err++;
            }
            
            if (err == 0) {
                tflip.methods.loading.show();
                if ($(document).find('#wd_pass').length) {
                    var dt = {'amount':$('#wd_amount').val(),'destination':$('#wadr').val(),'pass':$('#wd_pass').val()};
                }else {
                    var dt = {'amount':$('#wd_amount').val(),'destination':$('#wadr').val()};
                }
                $.ajax({
                    async: true,
                    url: '/ajax/withdraw',
                    type:  'POST',
                    cache: false,    
                    dataType:'json',    
                    data: dt,
                    success:  function(msg){ 
                        tflip.methods.loading.hide();
                        if (typeof (msg.ans) != "undefined" && msg.ans == 1 )
                        {
                            $('#wd_amount, #wadr, #wd_pass').val('');
                            tflip.methods.notification(msg.msg,'success');
                            setTimeout(function(){
                                document.location.reload();
                            },1500);
                        }else {
                            if (typeof (msg.err)!="undefined") {                                    
                                tflip.methods.notification(msg.err,'error');
                            }
                            if(typeof (msg.selector)!="undefined") {
                                
                                $(msg.selector).removeClass('success');
                                $(msg.selector).addClass('err');
                            }
                            if (typeof (msg.err) === "undefined" && typeof (msg.selectors) === "undefined") {
                                tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
                            }
                        }
                    },
                    error:function() {
                        tflip.methods.loading.hide();
                        tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
                    } 
                });
            }
        }
    },
    wd_calculate: function() {
        if ($(document).find('#wd_amount').length) {
            $('body').on('keyup change','#wd_amount',function(){
                tflip.methods.wd_calculator($(this));
            });
        }
    },
    wd_calculator: function(elm) {
        if (elm.val()!='') {
            var value = elm.val();
            var maximum = elm.data('max');
            var perc_maximum = ((maximum * 4/ 100) * 1000000 / 1000000);
            if (value > maximum) {
                var max = Math.round(((maximum * 1000 ) / 1000) * 1000000) / 1000000;
                elm.val(max);
                var res = Math.round(((usd_to_btc * maximum * 1000 ) / 1000) * 100) / 100;
            }else {                        
                var res = Math.round(((usd_to_btc * value * 1000 ) / 1000) * 100) / 100;
            }
            var perc = (res * 4 / 100);
            $('#wd_usd_amount').html('= $'+res+' <span>+ $'+perc+' comission</span>');
        }
    },
    validate_BTC_WA: function(address) {
        $('#wadr').removeClass('success');
        $('#wadr').removeClass('err');
        var retval = true;
        if (typeof address != "undefined" && address != '') {
            $.ajax({
                async: false,
                url: 'https://btc.blockr.io/api/v1/address/info/'+address,
                type:  'GET',
                cache: false,    
                dataType:'json',            
                success:  function(msg){ 
                    if (typeof (msg.status) != "undefined" && msg.status == "success") {
                        //if (msg.data.is_unknown == false) {
                            $('#wadr').addClass('success');
                            $('#wadr').removeClass('err');
                            retval = true;
                        //}else {
                          //  $('#wadr').removeClass('success');
                           // $('#wadr').addClass('err');
                            //retval = false;
                        //}
                    }else {
                        $('#wadr').removeClass('success');
                        $('#wadr').addClass('err');
                        retval = false;
                    }
                },
                error:function() {
                    $('#wadr').removeClass('success');
                    $('#wadr').removeClass('err');
                    retval = false;
                } 
            });
        }else {
            $('#wadr').removeClass('success');
            $('#wadr').addClass('err');
            retval = false;
        }
        return retval;
    },
    subscribe:function() {
        if ($('#subscription_email').length) {
            $('#subscription_email').removeClass('err');
            if ($('#subscription_email').val()!='') {
                var email = $('#subscription_email').val();
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (re.test(email)) {
                    tflip.methods.loading.show();
                    $.ajax({
                       async: true,
                        url: '/ajax/subscribe_user',
                        type:  'POST',
                        cache: false,    
                        dataType:'json',
                        data: {'email':email},
                        success:  function(msg){ 
                            tflip.methods.loading.hide();
                            if (typeof msg != "undefined" && msg.ans == 1) {
                                tflip.methods.metrik_events_trigger('form_subscribe_ico');
                                
                                $('#subscription_email').val('');
                                tflip.methods.notification(tflip.lang[current_language].success.subscribed_successfully,'success');
                            }else if (typeof (msg.err) != "undefined") {
                                tflip.methods.notification(msg.err,'error');
                            }
                        },
                        error:function() {
                            tflip.methods.loading.hide();
                            tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
                        } 
                    });
                }else {
                    $('#subscription_email').addClass('err');
                }
            }else {
                $('#subscription_email').addClass('err');
            }
        }
    },
    move_interest_to_balance: function(){
        tflip.methods.loading.show();
        $.ajax({
            async: true,
            url: '/ajax/move_interest_to_balance',
            type:  'POST',
            cache: false,    
            dataType:'json',
            success:  function(msg){ 
                tflip.methods.loading.hide();
                if (typeof msg != "undefined" && msg.ans == 1) {
                    document.location.reload();
                }else if (typeof (msg.err) != "undefined") {
                    tflip.methods.notification(msg.err,'error');
                }
            },
            error:function() {
                tflip.methods.loading.hide();
                tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
            }
        });
    },
    validate_refill_form:function(){        
        $('#refill_amount').removeClass('err');
        if (!isNaN(parseFloat($('#refill_amount').val()))) {
            return true;
        }else {
            $('#refill_amount').addClass('err');
            return false;
        }        
    },
    validate_refill_card_form:function(){
        $('#refill_card_amount').removeClass('err');
        if (!isNaN(parseFloat($('#refill_card_amount').val()))) {
            if ($('#refill_card_amount').val()<15) {
                $('#refill_card_amount').val(15);
                return true;    
            }else {
                return true;
            }
        }else {
            $('#refill_card_amount').val(15);            
            return true;
        }
    },
    refill_popup:function(){
        if ($(document).find('#refill_popup_form').length) {
            tflip.methods.loading.show();
            $.ajax({
                async: true,
                url: '/ajax/filter_refill_popup',
                type:  'POST',
                cache: false,
                data: $('#refill_popup_form').serialize(),
                success:  function(msg){ 
                    tflip.methods.loading.hide();
                    if (typeof msg != "undefined") {
                        $('#refill_ways').html('');
                        $('#refill_ways').html(msg);
                    }
                },
                error:function() {
                    tflip.methods.loading.hide();
                }
            });
        }
    },
    refill_filter:function(){
        if ($(document).find('#refill_form').length) {
            if (tflip.methods.validate_refill_form()==true) {
                tflip.methods.loading.show();
                $.ajax({
                    async: true,
                    url: '/ajax/filter_refill',
                    type:  'POST',
                    cache: false,
                    data: $('#refill_form').serialize(),
                    success:  function(msg){ 
                        tflip.methods.loading.hide();
                        if (typeof msg != "undefined") {
                            $('.refill_table__row').remove();
                            $('.refill_table__head').after(msg);                            
                        }
                    },
                    error:function() {
                        tflip.methods.loading.hide();
                    }
                });
            }
        }
    },
    set_site_currency:function(currency){
        if (currency == 'usd') {
            $(document).find('*[data-valbtc]').each(function(){
               var btc = $(this).data('valbtc');
               var in_usd = (btc * usd_to_btc * 1000) /1000;
               if (!isNaN(in_usd)) {
                   console.log(in_usd);
                   $(this).find('.currency_value').html('$ '+in_usd.toFixed(2));
                   $(this).find('.currency_selected').html('');
               }else {
                   console.log($(this).data('valbtc'));
               }
            });
        }else {
            $(document).find('*[data-valBTC]').each(function(){
                var in_btc = $(this).data('valbtc');
                console.log(in_btc);
                $(this).find('.currency_value').html(in_btc);
                $(this).find('.currency_selected').html('BTC');               
            });
        }
        tflip.methods.ico_init_price();
    },
    switcher:function(){
        if ($(document).find('.btc2usdswitcher').length) {
            $('.btc2usdswitcher').find('span').on('click touchend',function(){
               if ($(this).hasClass('b2u')) {
                   if ($('.btc2usdswitcher span[data-swcurrency="btc"]').hasClass('active')) {
                       $('.btc2usdswitcher span[data-swcurrency="btc"]').removeClass('active');
                       $('.btc2usdswitcher span[data-swcurrency="usd"]').addClass('active');
                       $(this).removeClass('btc_active').addClass('usd_active');
                       tflip.methods.set_site_currency('usd');
                   }else {
                       $('.btc2usdswitcher span[data-swcurrency="usd"]').removeClass('active');
                       $('.btc2usdswitcher span[data-swcurrency="btc"]').addClass('active');
                       $(this).removeClass('usd_active').addClass('btc_active');
                       tflip.methods.set_site_currency('btc');
                   }
               }else {
                   $('.btc2usdswitcher span').removeClass('active');
                   $(this).addClass('active');
                   if ($(this).data('swcurrency')=='btc') {
                       $('.btc2usdswitcher span.b2u').removeClass('usd_active').addClass('btc_active');
                       tflip.methods.set_site_currency('btc');
                   }else {
                       $('.btc2usdswitcher span.b2u').removeClass('btc_active').addClass('usd_active');
                       tflip.methods.set_site_currency('usd');
                   }
               }
            });
        }
    },
    ico_init_price: function() { 
        $(document).find('.box-counter').removeClass('err');
        if ($(document).find('.inves-tab__price').length) {
            var v = $(document).find('.box-counter .counter__input input').val();
            var count_tfl = $('#profit-range-slider').val();
            if (count_tfl<25000) {
                count_tfl = 0;
            }else if (count_tfl <=50000 && count_tfl > 25000) {
                count_tfl = 50000;
            }else if (count_tfl<75000 && count_tfl > 50000) {
                count_tfl = 50000;
            }else if (count_tfl <=100000 && count_tfl > 75000) {
                count_tfl = 100000;
            }else if (count_tfl <175000 && count_tfl > 100000) {
                count_tfl = 100000;
            }else if (count_tfl <=250000 && count_tfl > 175000) {
                currentVal = 250000;
            }else if (count_tfl < 375000 && count_tfl > 250000) {
                count_tfl = 250000;
            }else {
                count_tfl = 500000;
            }
            var profit_single_year = {
                '0': Math.round(((1.05 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '50000': Math.round(((3.52 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '100000': Math.round(((7.03 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '250000': Math.round(((17.58 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '500000': Math.round(((35.16 / usd_to_btc * 10000) / 10000) * 10000) / 10000
            };
            var profit_single_month = {
                '0': Math.round(((0.088 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '50000': Math.round(((0.29 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '100000': Math.round(((0.59 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '250000': Math.round(((1.46 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '500000': Math.round(((2.93 / usd_to_btc * 10000) / 10000) * 10000) / 10000                
            };
            if (!isNaN(parseInt(v))) {
                
                if ($(document).find('.btc2usdswitcher .b2u').length && $('.btc2usdswitcher .b2u').hasClass('usd_active')) {
                    var res = (parseInt(v) * (ICO.tokenprice * usd_to_btc * 1000)/1000);
                    res = Math.round(res * 100)/100;
                    $(document).find('.inves-tab__price').html(res +' USD');
                    var profit_anual = Math.round(((parseInt(v) * profit_single_year[count_tfl+''] * usd_to_btc * 1000) / 1000)* 100) / 100;
                    var profit_month = Math.round(((parseInt(v) * profit_single_month[count_tfl] * usd_to_btc * 1000) / 1000) * 100 ) / 100;
                    $('#forecasted_profit').html(profit_anual+' USD');
                    $('.js-monthly-profit').text(profit_month+' USD');            
                    $('.js-year-profit').text(profit_anual+' USD');
                }else {
                    var res = (parseInt(v) * (ICO.tokenprice * 1000)/1000);                    
                    $(document).find('.inves-tab__price').html(res+' BTC');
                    var profit_anual = Math.round(parseInt(v) * (profit_single_year[count_tfl+''] * 10000 ))/10000;
                    var profit_month = Math.round(parseInt(v) * (profit_single_month[count_tfl+''] * 10000 ))/ 10000;                    
                    $('#forecasted_profit').html(profit_anual+' BTC');
                    $('.js-monthly-profit').text(profit_month+' BTC');            
                    $('.js-year-profit').text(profit_anual+' BTC');
                }
            }
        }        
    },
    ico_input_change: function(){
        if ($(document).find('.box-counter .counter__input input').length) {
            $(document).find('.box-counter .counter__input input').on('change',function(){
                tflip.methods.ico_init_price();
            });
        }
    },
    iprofile_init_calc: function(){
      /*-------------- profit-range-slider --------------------*/
        var profits = {
            0: {
                'monthly expences': '$ 99 873',
                'number tickets month': '324 000',
                'revenue': '$ 87 888',
                'monthly profit': '$ 1.05',
                'tockens': '10',
                'year profit': '$ 10.5'
            },
            50000: {
                'monthly expences': '$ 332 910',
                'number tickets month': '1 080 000',
                'revenue': '$ 292 961',
                'monthly profit': '$ 3.52',
                'tockens': '10',
                'year profit': '$ 35.20'
            },
            100000: {
                'monthly expences': '$ 665 820',
                'number tickets month': '2 160 000',
                'revenue': '$ 585 992',
                'monthly profit': '$ 7.03',
                'tockens': '10',
                'year profit': '$ 70.3'
            },
            250000: {
                'monthly expences': '$ 1 664 550',
                'number tickets month': '5 400 000',
                'revenue': '$ 1 464 804',
                'monthly profit': '$ 17.58',
                'tockens': '10',
                'year profit': '$ 175.8'
            },
            500000: {
                'monthly expences': '$ 3 329 100',
                'number tickets month': '10 800 000',
                'revenue': '$ 2 929 608',
                'monthly profit': '$ 35.16',
                'tockens': '10',
                'year profit': '$ 351.6'
            }
        };
        $("#profit-range-slider").slider({
            ticks: [0, 50000, 100000, 250000],//, 500000],// 1000000],
            //ticks_labels: ['100 000', '500 000', '1 000 000'],
            ticks_labels: ['15 000', '50 000','100 000','250 000'],// '500 000'],
            ticks_snap_bounds: 30,
            tooltip: 'hide',
            value: 100000,
        }).on('slideStop', function (slideEvt) {
            var currentVal = slideEvt.value;
            if (currentVal<25000) {
                currentVal = 0;
            }else if (currentVal <=50000 && currentVal > 25000) {
                currentVal = 50000;
            }else if (currentVal<75000 && currentVal > 50000) {
                currentVal = 50000;
            }else if (currentVal <=100000 && currentVal > 75000) {
                currentVal = 100000;
            }else if (currentVal <175000 && currentVal > 100000) {
                currentVal = 100000;
            }else if (currentVal <=250000 && currentVal > 175000) {
                currentVal = 250000;
            }else if (currentVal < 375000 && currentVal > 250000) {
                currentVal = 250000;
            }else {
                currentVal = 500000;
            }
            setCalcResults(currentVal);
        });
        $('#js-profit-select-styler li').click(function () {
            var selectVal = $('#js-profit-select').val();
            var currentVal = selectVal;
            if (currentVal<25000) {
                currentVal = 0;
            }else if (currentVal <=50000 && currentVal > 25000) {
                currentVal = 50000;
            }else if (currentVal<75000 && currentVal > 50000) {
                currentVal = 50000;
            }else if (currentVal <=100000 && currentVal > 75000) {
                currentVal = 100000;
            }else if (currentVal <175000 && currentVal > 100000) {
                currentVal = 100000;
            }else if (currentVal <=250000 && currentVal > 175000) {
                currentVal = 250000;
            }else if (currentVal < 375000 && currentVal > 250000) {
                currentVal = 250000;
            }else {
                currentVal = 500000;
            }
            setCalcResults(currentVal);
        });
        function getInitValue(){
            var initValue = $("#profit-range-slider").slider('getValue');
            var currentVal = initValue;
            if (currentVal<25000) {
                currentVal = 0;
            }else if (currentVal <=50000 && currentVal > 25000) {
                currentVal = 50000;
            }else if (currentVal<75000 && currentVal > 50000) {
                currentVal = 50000;
            }else if (currentVal <=100000 && currentVal > 75000) {
                currentVal = 100000;
            }else if (currentVal <175000 && currentVal > 100000) {
                currentVal = 100000;
            }else if (currentVal <=250000 && currentVal > 175000) {
                currentVal = 250000;
            }else if (currentVal < 375000 && currentVal > 250000) {
                currentVal = 250000;
            }else {
                currentVal = 500000;
            }
            setCalcResults(currentVal);
        }
        getInitValue();
        function setCalcResults(val){            
            var v = $(document).find('.box-counter .counter__input input').val();
            var count_tfl = val+'';
            
            var profit_single_year = {
                '0': Math.round(((1.05 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '50000': Math.round(((3.52 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '100000': Math.round(((7.03 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '250000': Math.round(((17.58 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '500000': Math.round(((35.16 / usd_to_btc * 10000) / 10000) * 10000) / 10000
            };
            var profit_single_month = {
                '0': Math.round(((0.088 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '50000': Math.round(((0.29 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '100000': Math.round(((0.59 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '250000': Math.round(((1.46 / usd_to_btc * 10000) / 10000) * 10000) / 10000,
                '500000': Math.round(((2.93 / usd_to_btc * 10000) / 10000) * 10000) / 10000                
            };            
            $('.js-monthly-expences').text(profits[val]["monthly expences"]);
            $('.js-number-tickets-month').text(profits[val]["number tickets month"]);
            $('.js-revenue').text(profits[val]["revenue"]);            
            if (!isNaN(parseInt(v))) {
                if ($(document).find('.btc2usdswitcher .b2u').length && $('.btc2usdswitcher .b2u').hasClass('usd_active')) {
                    var res = (parseInt(v) * (ICO.tokenprice * usd_to_btc * 1000)/1000);
                    res = Math.round(res * 100)/100;
                    var profit_anual = Math.round(((parseInt(v) * profit_single_year[count_tfl+''] * usd_to_btc * 1000) / 1000)* 100) / 100;
                    var profit_month = Math.round(((parseInt(v) * profit_single_month[count_tfl] * usd_to_btc * 1000) / 1000) * 100 ) / 100;
                    profit_anual += ' USD';
                    profit_month += ' USD';
                }else {
                    var res = (parseInt(v) * (ICO.tokenprice * 1000)/1000);
                    var profit_anual = Math.round(parseInt(v) * (profit_single_year[count_tfl+''] * 10000 ))/10000;
                    var profit_month = Math.round(parseInt(v) * (profit_single_month[count_tfl+''] * 10000 ))/ 10000;                    
                    profit_anual += ' BTC';
                    profit_month += ' BTC';
                }
                $('.js-monthly-profit').text(profit_month);            
                $('.js-year-profit').text(profit_anual);
            }
        }  
    },
    iprofile_buy_calc: function(){
        if ($(document).find('.toggle_profit').length) {
            $('.profit_investor').hide();
            $('.toggle_profit').on('click touchend',function(){
                if ($(this).next().is(':visible')) {
                    $(this).removeClass('opened');
                    $(this).next().fadeOut(300);
                }else {
                    $(this).addClass('opened');
                    $(this).next().fadeIn(300);
                    setTimeout(function(){
                        tflip.methods.iprofile_init_calc();
                    },400);
                }                
            });
        }
    },
    slider_index: function(){
        if ($('.about-lottery').length) {
            //scrollSlider();
        }
    },
    scroll_to:function(selector) {
        console.log(selector);
        if ($(selector).length != 0) {
            $('body,html').animate({scrollTop: $(selector).offset().top-30}, 600);
        }
        return false;
    },
    close_investor_about: function() {
        var notification_cookie_date = new Date;
        notification_cookie_date.setDate(notification_cookie_date.getDate()+360);
        document.cookie='investor_about=1;expires='+notification_cookie_date;
        $('.box-inves-video').hide();
        $('.tab-wrap-with-video').find('.nav-tab-list-wrap').addClass('nav-tab-list-wrap-no-vid');
        return false;
    },
    close_last_result:function(draw_id) {
        var notification_cookie_date = new Date;
        notification_cookie_date.setDate(notification_cookie_date.getDate()+360);
        document.cookie='draw_'+draw_id+'=1;expires='+notification_cookie_date;
        $('.last_draw_'+draw_id).fadeOut(300);
    },
    bitcoin_to_usd_main_page: function(){
        if ($(document).find('.jackpot-val').length) {
            $(document).find('.jackpot-val').html(jackpot_btc+' <sup>BTC</sup>');
        }
        if ($(document).find('.single_ticket_cost').length) {
            $(document).find('.single_ticket_cost').html(ticket_price);
        }
        if ($(document).find('.ticket_cost_in_usd').length) {
            $(document).find('.ticket_cost_in_usd').html((ticket_price*usd_to_btc).toFixed(2));
        }
        if ($(document).find('*[data-convertCUR="btc2usd"]').length) {
            $(document).find('*[data-convertCUR="btc2usd"]').each(function(){
               var res = ($(this).data('btcamount')*usd_to_btc).toFixed(2);
               $(this).html(res);
            });
        }
    },
    countdown_ico: function(selector, options){
        var monthArr1 = ['ÑÐ½Ð²Ð°Ñ€Ñ', 'Ñ„ÐµÐ²Ñ€Ð°Ð»Ñ', 'Ð¼Ð°Ñ€Ñ‚Ð°', 'Ð°Ð¿Ñ€ÐµÐ»Ñ', 'Ð¼Ð°Ñ', 'Ð¸ÑŽÐ½Ñ', 'Ð¸ÑŽÐ»Ñ', 'Ð°Ð²Ð³ÑƒÑÑ‚Ð°', 'ÑÐµÐ½Ñ‚ÑÐ±Ñ€Ñ', 'Ð¾ÐºÑ‚ÑÐ±Ñ€Ñ', 'Ð½Ð¾ÑÐ±Ñ€Ñ', 'Ð´ÐµÐºÐ°Ð±Ñ€Ñ'];
        var settings = { 'date': null };
        var expired = false;
        if (options) {
            $.extend(settings, options)
        }
        this_sel = $(selector);
        tempDate = new Date(settings['date']);
        tempMonth = monthArr1[tempDate.getMonth()];
        tempDay = tempDate.getDate();
        tempHours = tempDate.getHours();
        tempMins = tempDate.getMinutes();        
        function count_ecec_ico() {
            eventDate = Date.parse(settings['date']) / 1000;
            currentDate = Math.floor( $.now() / 1000 );

            if (eventDate <= currentDate ) {
                expired = true;
                //$('.js-lottery-date').html(settings['text']);
            }
            seconds = eventDate - currentDate;
            if (this_sel.find('.years-ico').length > 0) {
                years = Math.floor( seconds / ( 60 * 60 * 24 * 365 ) );
                seconds -= years * 60 * 60 * 24 * 365  ;
            }
            if (this_sel.find('.days-ico').length > 0) {
                days = Math.floor( seconds / ( 60 * 60 * 24 ) );
                seconds -= days * 60 * 60 * 24 ;
            }
            if (this_sel.find('.hours-ico').length > 0) {
                hours = Math.floor( seconds / ( 60 * 60) );
                seconds -= hours * 60 * 60 ;
            }
            if (this_sel.find('.mins-ico').length > 0) {
                minutes =  Math.floor( seconds / 60 );
                seconds -= minutes * 60 ;
            }
            if (this_sel.find('.years-ico').length > 0) {
                years = (String(years).length < 2 ? '0'  + years : years);
            }

            if (this_sel.find('.days-ico').length > 0) {
                //days = (String(days).length < 2 ? '0'  + days : days);
                if (expired) {
                    days = 0;
                }
            }
            if (this_sel.find('.hours-ico').length > 0) {
                if (expired) {
                    hours = '00';
                }
                hours = (String(hours).length !==2 ? '0'  + hours : hours);
            }
            if (this_sel.find('.mins-ico').length > 0) {
                if (expired) {
                    minutes = '00';
                }
                minutes = (String(minutes).length !==2 ? '0'  + minutes : minutes);
            }
            if (this_sel.find('.secs-ico').length > 0) {
                if (expired) {
                    seconds = '00';
                }
                seconds = (String(seconds).length !==2 ? '0'  + seconds : seconds);
            }
            if (!isNaN(eventDate)) {
                if (this_sel.find('.years-ico').length > 0) {
                    this_sel.find('.years-ico').text(years);
                }
                if (this_sel.find('.days-ico').length > 0) {
                    this_sel.find('.days-ico').text(days);
                    if (days == 0) {
                        this_sel.find('.days-ico').parent().addClass('vis_hidden');
                    }
                }
                if (this_sel.find('.hours-ico').length > 0) {
                    this_sel.find('.hours-ico').text(hours);
                }
                if (this_sel.find('.mins-ico').length > 0) {
                    this_sel.find('.mins-ico').text(minutes);
                }
                this_sel.find('.secs-ico').text(seconds);
                //setTimeout(tflip.methods.count_ecec_ico(this_sel,settings,expired),300);                
            }else {

            }
        }
        count_ecec_ico();
        interval1 = setInterval(count_ecec_ico, 1000);
        //setTimeout(tflip.methods.count_ecec_ico(this_sel,settings,expired), 300);
    },
    text_count_down:function() {
        if ($(document).find('.about-lottery-countdown').length) {
            $('.about-lottery-countdown').find('.js-countdown div').html($('.countdown-wrap').find('.js-countdown div').html());
        }
    },
    send_popup_quest:function(){
        tflip.methods.make_question_request('#popup_ask_question','popup');
    },
    send_question:function() {
        tflip.methods.make_question_request('#form_ask_question','normal');
    },
    make_question_request:function(selector,type){
        tflip.methods.loading.show();
        $.ajax({
            async: true,
            url: '/ajax/ask_question',
            type:  'POST',
            cache: false,
            data: $(selector).serialize(),
            dataType:'json',
            success:  function(msg){ 
                tflip.methods.loading.hide();
                tflip.methods.reset_grecaptch();
                if (typeof(msg.ans) != 'undefined' && msg.ans == 1) {
                    tflip.methods.metrik_events_trigger('form_feedback');
                    
                    tflip.methods.notification(msg.txt,'success');
                    if (type == 'normal') {
                        $(selector).fadeOut(300);
                    }else {
                        $('.ask-btn').fadeOut(300);
                        $('.fancybox-item.fancybox-close').trigger('click');
                        $(selector).fadeOut(300);
                    }
                }else {
                    tflip.methods.notification(msg.err,'error');
                    tflip.methods.reset_grecaptch();
                }
            },
            error:function() {
                tflip.methods.loading.hide();
                tflip.methods.reset_grecaptch();
            }
        });
    },
    modal_or_profile: function() {
        if ($(document).find('#modal-login').length) {
            $('.login-btn').trigger('click');
        }else {
            document.location.href = '/profile';
        }
        return false;
    },
    restore_password: function() {
        if ($('#new_pass').val().length >= 8 && ($('#repeat_pass').val()==$('#new_pass').val())) {
            tflip.methods.loading.show();
            setTimeout(function(){
                $.ajax({
                    async: true,
                    url: '/ajax/restore_pass',
                    type:  'POST',
                    data: $('#pass_change_form').serialize(),
                    dataType:'json',
                    cache: false,
                    success:  function(msg){ 
                        tflip.methods.loading.hide();
                        if (typeof(msg.ans) != 'undefined' && msg.ans==1) {
                            if (typeof(msg.success_msg)!="undefined") {
                                tflip.methods.notification(msg.success_msg,'success');
                            }
                            $('#new_pass, #repeat_pass').val('');
                            setTimeout(function(){
                                document.location.href='/profile';
                            },3000)
                        }else {
                            if (typeof(msg.err) != "undefined") {
                                tflip.methods.notification(msg.err,'error');
                            }else {
                                tflip.methods.notification(tflip.lang[current_language].errors.try_later,'error');
                            }
                        }
                    },
                    error:function() {
                        tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
                        tflip.methods.loading.hide();
                    }
                });
            },300);
        }else {
            var err = [];            
            if ($('#new_pass').val().length) {
                if ($('#new_pass').val().length < 8) {
                    err.push(tflip.lang[current_language].errors.chp_too_short);
                }
                if ($('#repeat_pass').val().length) {
                    if ($('#repeat_pass').val()!=$('#new_pass').val()) {
                        err.push(tflip.lang[current_language].errors.chp_pass_not_eq);
                    }
                }else {
                    err.push(tflip.lang[current_language].errors.chp_repeat_new);
                }
            }else {
                err.push(tflip.lang[current_language].errors.chp_enter_new);
            }
            tflip.methods.notification(err.join('<br>'),'error');
        }
    },
    change_password: function(){
        if ($('#old_pass').val().length != 0 && $('#new_pass').val().length >= 8 
                && ($('#repeat_pass').val()==$('#new_pass').val()) 
                && ($('#new_pass').val()!=$('#old_pass').val())) {
            tflip.methods.loading.show();
            setTimeout(function(){
                $.ajax({
                    async: true,
                    url: '/ajax/change_pass',
                    type:  'POST',
                    data: $('#pass_change_form').serialize(),
                    dataType:'json',
                    cache: false,
                    success:  function(msg){ 
                        tflip.methods.loading.hide();
                        if (typeof(msg.ans) != 'undefined' && msg.ans==1) {
                            if (typeof(msg.success_msg)!="undefined") {
                                tflip.methods.notification(msg.success_msg,'success');
                            }
                            $('#old_pass, #new_pass, #repeat_pass').val('');
                            setTimeout(function(){
                                document.location.href='/profile';
                            },3000)
                        }else {
                            if (typeof(msg.err) != "undefined") {
                                tflip.methods.notification(msg.err,'error');
                            }else {
                                tflip.methods.notification(tflip.lang[current_language].errors.try_later,'error');
                            }
                        }
                    },
                    error:function() {
                        tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
                        tflip.methods.loading.hide();
                    }
                });
            },300);
        }else {
            var err = [];
            if ($('#old_pass').val().length) {
                if ($('#new_pass').val().length) {
                    if ($('#new_pass').val().length < 8) {
                        err.push(tflip.lang[current_language].errors.chp_too_short);
                    }
                    if ($('#new_pass').val()==$('#old_pass').val()) {
                        err.push(tflip.lang[current_language].errors.chp_old_new_diff);
                    }
                    if ($('#repeat_pass').val().length) {
                        if ($('#repeat_pass').val()!=$('#new_pass').val()) {
                            err.push(tflip.lang[current_language].errors.chp_pass_not_eq);
                        }
                    }else {
                        err.push(tflip.lang[current_language].errors.chp_repeat_new);
                    }
                }else {
                    err.push(tflip.lang[current_language].errors.chp_enter_new);
                }
            }else {
                err.push(tflip.lang[current_language].errors.chp_enter_old);
            }
            tflip.methods.notification(err.join('<br>'),'error');
        }
    },
    change_password_cancel: function(){
        $('#old_pass, #new_pass, #repeat_pass').val('');
        tflip.methods.check_passwords();
    },
    check_passwords: function() {
        var old = $(document).find('#old_pass')
            newest = $(document).find('#new_pass'),
            repeated = $(document).find('#repeat_pass');
        var err = '';
        if (old.val().length) {
            if (newest.val().length) {
                if (newest.val().length<8) {
                    if (err!='') { err+='<br>';}
                    err += 'Ð¡Ð»Ð¸ÑˆÐºÐ¾Ð¼ ÐºÐ¾Ñ€Ð¾Ñ‚ÐºÐ¸Ð¹ Ð½Ð¾Ð²Ñ‹Ð¹ Ð¿Ð°Ñ€Ð¾Ð»ÑŒ';
                }
                if (old.val() == newest.val()) {
                    if (err!='') { err+='<br>';}
                    err += 'Ð¡Ñ‚Ð°Ñ€Ñ‹Ð¹ Ð¸ Ð½Ð¾Ð²Ñ‹Ðµ Ð¿Ð°Ñ€Ð¾Ð»Ð¸ Ð½Ðµ Ð´Ð¾Ð»Ð¶Ð½Ñ‹ ÑÐ¾Ð²Ð¿Ð°Ð´Ð°Ñ‚ÑŒ';
                }
                if (repeated.val().length) {
                    if (repeated.val()!= newest.val()) {
                        if (err!='') { err+='<br>';}
                        err += 'ÐŸÐ¾Ð²Ñ‚Ð¾Ñ€Ð½Ð¾ Ð²Ð²ÐµÐ´ÐµÐ½Ñ‹Ð¹ Ð¿Ð°Ñ€Ð¾Ð»ÑŒ Ð¸ Ð½Ð¾Ð²Ñ‹Ð¹ Ð¿Ð°Ñ€Ð¾Ð»Ð¸ Ð½Ðµ ÑÐ¾Ð²Ð¿Ð°Ð´Ð°ÑŽÑ‚';
                    }
                }
            }
        }
        $('#error_handler').html(err);
    },
    change_pass_handlers: function(){
        if ($(document).find('#pass_change_form').length) {
            $(document).on('blur','#new_pass',function(e){
                tflip.methods.check_passwords();
            });
            $(document).on('blur','#old_pass',function(e){
                tflip.methods.check_passwords();
            });
            
            $(document).on('keyup', '#new_pass', function(e){
                clearTimeout(typingTimer);
                typingTimer = setTimeout(tflip.methods.doneTyping($('#new_pass'),'new'), tflip.settings.doneTypingInterval);
            });
            $(document).on('keydown', '#new_pass', function(e){
                clearTimeout(typingTimer);
            });
            
            $(document).on('keyup','#repeat_pass',function(e){
                var inter = (tflip.settings.doneTypingInterval+5000);
                typingTimer_repeat = setTimeout(tflip.methods.doneTyping($('#repeat_pass'),'repeat'), inter);
                
            });
            $(document).on('keydown', '#repeat_pass', function(e){
                clearTimeout(typingTimer_repeat);
            });
        }
    },
    doneTyping: function(elm,type) {
        if (type == 'repeat') {
            setTimeout(function(){
                tflip.methods.check_passwords();
                clearTimeout(typingTimer_repeat);
            },1000);
        }
        if (type == 'new') {
            if (elm.val().length) {
                var str = tflip.methods.pass_strength(elm.val());
                $('.box-level-password').fadeIn(300);
                $('.level-password__text').attr('class','level-password__text');
                $('.level-password').attr('class','level-password');
                $('.level-password__text').addClass('level-password__text-'+str);
                $('.level-password').addClass('level-password-'+str);
                $('.level-password__text').html($('.level-password__line').data(''+str));
                $('.level-password__text').show();
            }else {
                $('.level-password__text').attr('class','level-password__text');
                $('.level-password').attr('class','level-password');
                $('.level-password__text').html('');
            }
        }
    },
    pass_strength:function(pass) {
        var options = {
            minChar: 6, // too short while less than this
            passIndex: 2, // Weak
            verdicts: ['red','red','orange','yellow','green'],
            scores: [10,15,25,45],
            bannedPasswords: ['123456','12345','123456789',
                'password','iloveyou','princess','rockyou','1234567','12345678','abc123','nicole','daniel',
                'babygirl','monkey','jessica','lovely','michael','ashley','654321','qwerty','password1',
                'welcome','welcome1','password2','password01','password3','p@ssw0rd','passw0rd','password4',
                'password123','summer09','password6','password7','password9','password8','welcome2',
                'welcome01','winter12','spring2012','summer12','summer2012'
            ],
        };
        var checks = [
            { re: /[a-z]/, score: 20 },
            { re: /[A-Z]/, score: 20 },
            { re: /([a-z].*[A-Z])|([A-Z].*[a-z])/, score: 30 },
            { re: /(.*[0-9].*[0-9].*[0-9])/, score: 30},
            { re: /.[!@#$%^&*?_~]/, score: 30 },
            { re: /(.*[!@#$%^&*?_~].*[!@#$%^&*?_~])/, score: 30 },
            { re: /([a-zA-Z0-9].*[!@#$%^&*?_~])|([!@#$%^&*?_~].*[a-zA-Z0-9])/, score: 40 },
            { re: /(.)\1+$/, score: 20 }
        ];
        function runPassword(pass,options,checks) {
            var score = checkPassword(pass,options,checks);
            if (Array.prototype.indexOf(options.bannedPasswords, pass.toLowerCase()) !== -1){
                return options.verdicts[0];
            } else {
                if (score < 20 ) {
                    return options.verdicts[0];
                }else if (score < 40) {
                    return options.verdicts[1];
                }else if (score < 60) {
                    return options.verdicts[2];
                }else if (score < 80) {
                    return options.verdicts[3];
                }else {
                    return options.verdicts[4];
                }
            }
        }
        
        function checkPassword(pass,options,checks) {
            var score = 0,
                minChar = options.minChar,
                len = pass.length,
                diff = len - minChar;
            (diff < 0 && (score -= 100)) || (diff >= 5 && (score += 18)) || (diff >= 3 && (score += 12)) || (diff === 2 && (score += 12));
            checks.forEach(function(entry, index,checks){
                var reg = checks[index].re;
                    reg.test(pass) && (score += checks[index].score);
            });
            // bonus for length per char
            score && (score += len);
            return score;
        }
        var ret = runPassword(pass,options,checks);
        console.log('Return:'+ret);
        return ret;
    },
    switch_receipts: function(classname) {
        if ($(document).find('.rec_filter').length) {
            $(document).find('.rec_filter').fadeOut(0);
            if ($(document).find('.'+classname).length) {
                $(document).find('.'+classname).fadeIn(300);
            }
        }
    },
    checkif_ready:function(elm){
        var activeItems = $(elm).parent().find(".active").length;
        var activeItemsMax = $(elm).parent().attr("data-max");
        if ((activeItems) >= activeItemsMax) {
            $(elm).parent().attr("data-ready", true);
            $(elm).parent().addClass('ready-number-set');
        } else {
            $(elm).parent().attr("data-ready", false);
            $(elm).parent().removeClass('ready-number-set');
        }            
        setTimeout(function(){
            //tflip.methods.update_total_summ();
        },250);
    },
    click_tick : function(){
        $('body').on('click touchend',".num-list__item",function() {
            var activeItems = $(this).parent().find(".active").length;
            var activeItemsMax = $(this).parent().attr("data-max");
            console.log('active:'+activeItems+'; active-max:'+activeItemsMax);
            if ((activeItems + 1) >= activeItemsMax) {
                $(this).parent().attr("data-ready", true);
                $(this).parent().addClass('ready-number-set');
            } else {
                $(this).parent().attr("data-ready", false);
                $(this).parent().removeClass('ready-number-set');
            }            
            setTimeout(function(){
                console.log('calling update method');
                //tflip.methods.update_total_summ();
            },250);
            if ($(this).parent().hasClass('num-list_yellow')) {
                $(this).parent().find('.num-list__item').removeClass('active');
                $(this).toggleClass('active');
                tflip.methods.checkif_ready($(this));
                return false;
            }else {
                if (activeItems < activeItemsMax) {
                    $(this).toggleClass("active");                
                    tflip.methods.checkif_ready($(this));
                    return false;
                } else {
                    $(this).removeClass("active");
                    tflip.methods.checkif_ready($(this));
                    return false;
                }
            }            
        });
    },
    loading: {
        pleaseWaitDiv: $('<div id="pleaseWaitDialog"><div></div></div>').hide().fadeIn(300),
        show: function() {
          if (!$(document).find('#pleaseWaitDialog').is(':visible') || !$(document).find('#pleaseWaitDialog').length) {
            $('.main-wrapper').prepend(tflip.methods.loading.pleaseWaitDiv);
          }
        },
        hide: function () {
          $(document).find('#pleaseWaitDialog').remove();          
        }
    },
    buytfl:function(){
        if (document.getElementById('agreementTFL').checked) {
            $(document).find('.box-counter').removeClass('err');
            $(document).find('.tfl_agreement').removeClass('err');
            var v = $(document).find('.box-counter .counter__input input').val();
            if (!isNaN(parseInt(v))) {
                var plurals = [
                    $('#howManyPlural').data('pl1'),
                    $('#howManyPlural').data('pl2'),
                    $('#howManyPlural').data('pl3')
                ];
                var pl = tflip.methods.get_plural(v,plurals);
                var in_btc = (parseInt(v) * ICO.tokenprice * 10000) / 10000;

                if (typeof myBalance != "undefined" && myBalance < in_btc) {
                    var cnt_want = $('.counter__input input').val();
                    var how_much_needed = ICO.tokenprice * cnt_want;
                    var diff = how_much_needed - myBalance;
                    if (!isNaN(diff)) {
                        $('#lacking_part').html(diff);
                        $('#need_to_refill').html(diff);
                        setTimeout(function(){
                            tflip.methods.refill_popup();                                                
                        },100);
                    }
                    tflip.methods.openFancyBox('#modal-not_enough');                
                }else {
                    $('#howManyToBy').html(v);
                    $('#howManyPlural').html(pl);
                    $('#howManyBTC').html(in_btc+' BTC');            
                    tflip.methods.openFancyBox('#modal-play_8');
                }
            }else {
                $(document).find('.box-counter').addClass('err');            
            }
        }else {
            $(document).find('.tfl_agreement').addClass('err');
        }
    },
    closePopupForever:function(id) {
        var notification_cookie_date = new Date;
        notification_cookie_date.setDate(notification_cookie_date.getDate()+360);
        document.cookie=id+'=1;expires='+notification_cookie_date;
        parent.$.fancybox.close();
    },
    show_pre_ico_banner: function(){
        if ($(document).find('#notification_popup_banner').length && document.location.hash!='#reg') {
            setTimeout(function(){
                tflip.methods.openFancyBox('#notification_popup_banner','preicobanner');
                tflip.methods.preico_countdown();            
            },1500);
        }
    },
    show_draw_in_process: function(){
        if ($(document).find('#inprocess__dr').length) {
            setTimeout(function(){
                if ($.fn.fancybox) {
                    $.fancybox({
                        padding: 0,
                        closeClick  : false,
                        wrapCSS : 'draw_processing',
                        helpers: {
                            overlay: {locked: false,closeClick: false}
                        },
                        keys : {
                            close  : null
                        },
                        tpl: {closeBtn: '<div title="Ð—Ð°ÐºÑ€Ñ‹Ñ‚ÑŒ" class="fancybox-item fancybox-close"></div>'},
                        afterShow: function() {
                            if ($('.js-menu-wrap').hasClass('open')) {
                                $('.js-menu-wrap').fadeOut().removeClass('open');
                                $('body').removeClass('body-overflow');
                            }
                        },
                        afterClose: function() {
                            $('body').removeClass("special-fancy");
                        },
                        margin: 10,
                        fixed: true,
                        href: '#inprocess__dr'
                    });
                    $('.main-wrapper').addClass('blured');
                    tflip.methods.check_process();
                }
                
            },2500);
        }
    },
    show_free_ticket_banner: function() {
        if ($(document).find('#free_popup_banner').length && document.location.hash!='#reg') {
            setTimeout(function(){
                tflip.methods.openFancyBox('#free_popup_banner','freetickban');
                tflip.methods.preico_countdown();            
            },1500);
        }
    },
    check_process: function(){
        function checkproc() {
            $.ajax({
                async: true,
                url: '/ajax/drawinprocess',
                type:  'GET',
                dataType:'json',
                cache: false,
                success:  function(msg){
                    if (typeof (msg.in_process) != "undefined") {
                        if (msg.in_process==0) {
                            $.fancybox.close();
                            document.location.reload();
                            $('.main-wrapper').removeClass('blured');
                        }
                    }
                }
            });
            //
        }
        checkproc();        
        interval = setInterval(checkproc, 120000);
    },
    preico_countdown:function() {
        var datest = new Date('2017-06-28 12:00:01 GMT+0');
        var tempdate = datest.getDate();
        var temphours = datest.getHours();
        var tempminutes = datest.getMinutes();
        var expired = false;
        function countSec() {
            eventDate = Date.parse('2017-06-28 12:00:01 GMT+0') / 1000;
            currentDate = Math.floor( $.now() / 1000 );
            if (eventDate <= currentDate ) {
                expired = true;                
            }
            seconds = eventDate - currentDate;
            if ($(document).find('.daysleft').length > 0) {
                days = Math.floor( seconds / ( 60 * 60 * 24 ) );
                seconds -= days * 60 * 60 * 24 ;
            }
            if ($(document).find('.hoursleft').length > 0) {
                hours = Math.floor( seconds / ( 60 * 60) );
                seconds -= hours * 60 * 60 ;
            }
            if ($(document).find('.minutesleft').length > 0) {
                minutes =  Math.floor( seconds / 60 );
                seconds -= minutes * 60 ;
            }
            if ($(document).find('.daysleft').length > 0) {
                if (expired) {
                    days = 0;
                }
            }
            if ($(document).find('.hoursleft').length > 0) {
                if (expired) {
                    hours = '00';
                }
                hours = (String(hours).length !==2 ? '0'  + hours : hours);
            }
            if ($(document).find('.minutesleft').length > 0) {
                if (expired) {
                    minutes = '00';
                }
                minutes = (String(minutes).length !==2 ? '0'  + minutes : minutes);
            }
            if ($(document).find('.secondsleft').length > 0) {
                if (expired) {
                    seconds = '00';
                }
                seconds = (String(seconds).length !==2 ? '0'  + seconds : seconds);
            }
            if (!isNaN(eventDate)) {
                if ($(document).find('.daysleft').length > 0) {
                    $(document).find('.daysleft').text(days);
                    if (days == 0) {
                        $(document).find('.daysleft').parent().addClass('vis_hidden');
                    }
                }
                if ($(document).find('.hoursleft').length > 0) {
                    $(document).find('.hoursleft').text(hours);
                }
                if ($(document).find('.minutesleft').length > 0) {
                    $(document).find('.minutesleft').text(minutes);
                }
                $(document).find('.secondsleft').text(seconds);
            }
        }
        countSec();
        interval = setInterval(countSec, 300);
        //text lottery date under countown in slider main page
        if (expired) {
            $('.daysleft, .hoursleft, .minutesleft, .secondsleft').html('0');
        } else {
            $('.daysleft').text(tempDay);
            $('.hoursleft').text(tempHours);
            $('.minutesleft').text(tempMins);
        }        
    },
    PurchaseTFL:function(){
        var v = $(document).find('.box-counter .counter__input input').val();
        if (!isNaN(parseInt(v))) {
            var in_btc = (parseInt(v) * ICO.tokenprice * 10000) / 10000;            
            if (typeof myBalance != "undefined" && myBalance < in_btc) {
                var cnt_want = $('.counter__input input').val();
                var how_much_needed = ICO.tokenprice * cnt_want;
                var diff = how_much_needed - myBalance;
                if (!isNaN(diff)) {
                    $('#lacking_part').html(diff);
                    $('#need_to_refill').html(diff);
                    setTimeout(function(){
                        tflip.methods.refill_popup();                                                
                    },100);
                }
                tflip.methods.openFancyBox('#modal-not_enough');
            }else {
                $('#successfullyB').html(v);
                var plurals = [
                    $('#successfullyBC').data('pl1'),
                    $('#successfullyBC').data('pl2'),
                    $('#successfullyBC').data('pl3')
                ];
                var pl = tflip.methods.get_plural(v,plurals);
                $('#successfullyBC').html(pl);
                tflip.methods.loading.show();
                $.ajax({
                    async: true,
                    url: '/ajax/purchasetfl',
                    type:  'POST',
                    data: {'howMany':v},
                    dataType:'json',
                    cache: false,
                    success:  function(msg){ 
                        tflip.methods.loading.hide();
                        if (typeof msg != 'undefined') {
                            if (typeof(msg.ans) != "undefined") {
                                if (msg.ans == 1) {
                                    parent.$.fancybox.close();
                                    tflip.methods.openFancyBox('#modal-play_12');
                                    tflip.methods.metrik_events_trigger('TFL_purchased');                                    
                                    setTimeout(function(){
                                        parent.$.fancybox.close();
                                        document.location.href = document.location.href
                                    },3000);
                                }else {
                                    parent.$.fancybox.close();
                                    if (typeof(msg.res) != "undefined" && msg.res == 'balance') {
                                        var cnt_want = $('.counter__input input').val();
                                        var how_much_needed = ICO.tokenprice * cnt_want;
                                        var diff = how_much_needed - myBalance;
                                        if (!isNaN(diff)) {
                                            $('#lacking_part').html(diff);
                                            $('#need_to_refill').html(diff);
                                            setTimeout(function(){
                                                tflip.methods.refill_popup();                                                
                                            },100);
                                        }
                                        tflip.methods.openFancyBox('#modal-not_enough');
                                    }else {
                                        tflip.methods.notification(msg.err,'error');
                                    }
                                }
                            }else {
                                parent.$.fancybox.close();
                                tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
                            }
                        }else {
                            parent.$.fancybox.close();
                            tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
                        }
                    },
                    error:function() {
                        parent.$.fancybox.close();
                        tflip.methods.loading.hide();
                        tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');                        
                    }
                });
            }
        }else {
            parent.$.fancybox.close();
            $(document).find('.box-counter').addClass('err');
        }
    },
    openFancyBox:function(id,cl) {
        var classWr = '';
        if (typeof cl != "undefined") {
            classWr = cl;
        }
        if ($.fn.fancybox) {
            $.fancybox({
                padding: 0,
                wrapCSS : classWr,
                helpers: {
                    overlay: {locked: false}
                },
                tpl: {closeBtn: '<div title="Ð—Ð°ÐºÑ€Ñ‹Ñ‚ÑŒ" class="fancybox-item fancybox-close"></div>'},
                afterShow: function() {
                    if ($('.js-menu-wrap').hasClass('open')) {
                        $('.js-menu-wrap').fadeOut().removeClass('open');
                        $('body').removeClass('body-overflow');
                    }
                },
                afterClose: function() {
                    $('body').removeClass("special-fancy");
                },
                margin: 10,
                fixed: true,
                href: id
            });
        }
    },
    get_plural:function(n,plural_array) {
        var plural;
        if (n%10==1 && n%100!=11) {
            plural = plural_array[0];
        }else if (n%10>=2 && n%10 <=4 && (n %100 <10 || n%100>=20)) {
            plural = plural_array[1];
        }else {
            plural = plural_array[2];
        }
        if (n == 0) {
            return plural_array[2];
        }else {
            return plural;
        }        
    },
    createTransaction: function(){
        tflip.methods.validate_refill_card_form();
        tflip.methods.loading.show();
        setTimeout(function(){
            $.ajax({
                async: true,
                url: '/ajax/ind_createTransaction',
                type:  'POST',            
                cache: false,
                data: {'from_currency':$('#inda_currency').val(),'amount':$('#refill_card_amount').val()},
                dataType:'json',
                success:  function(msg){ 
                    tflip.methods.loading.hide();
                    if (msg.ans==1) {
                        if (typeof(msg.url)!="undefined") {
                            document.location.href=msg.url;
                        }
                    }else {
                        tflip.methods.notification(msg.err,'error');
                    }
                },
                error:function() {
                    tflip.methods.loading.hide();
                }
            });
        },150);
    },
    btk_inda_convert: function(){
        var plural_arr = [$('.ind_tcount_w').data('pl1'),$('.ind_tcount_w').data('pl2'),$('.ind_tcount_w').data('pl3')];
        tflip.methods.validate_refill_card_form();
        tflip.methods.loading.show();
        setTimeout(function(){
            $.ajax({
                async: true,
                url: '/ajax/ind_refill_calculate',
                type:  'POST',            
                cache: false,
                data: {'from_currency':$('#inda_currency').val(),'amount':$('#refill_card_amount').val()},
                dataType:'json',
                success:  function(msg){ 
                    tflip.methods.loading.hide();
                    if (msg.ans==1 && typeof(msg.amountBtc) != "undefined") {
                        var btcAmount = msg.amountBtc;
                        var tc = (btcAmount/ticket_price).toFixed(0);
                        var tc_w = tflip.methods.get_plural(tc,plural_arr);
                        $('.ind_tcount').html(tc);
                        $('.ind_tcount_w').html(tc_w);
                        $('#ind_btc').html('= '+btcAmount+' BTC');
                    }else {
                        tflip.methods.notification(msg.err,'error');
                    }
                },
                error:function() {
                    tflip.methods.loading.hide();
                }
            });
        },150);
    },
    btk_inda_converter: function(){
        if ($(document).find('#refill_card_amount').length) {            
            $('#refill_card_amount').on('change',function(){
                tflip.methods.btk_inda_convert();
            });
        }
    },
    btk_recharge_converter: function() {
        if ($(document).find('#recharge_input').length) {            
            //var usd_to_btc = 1049.92;
            var default_btc = (ticket_price*10*usd_to_btc).toFixed(2);
            var plural_arr = [$('.rb_tcount_w').data('pl1'),$('.rb_tcount_w').data('pl2'),$('.rb_tcount_w').data('pl3')];
            $('#recharge_input').on('keyup',function(){
                var btk_url = 'https://chart.googleapis.com/chart?chs=195x195&cht=qr&chl=bitcoin:'+$('#recharge_qr').data('wallet')+'?amount=';
                if ($(this).val()!='' && $(document).find('#recharge_summ').length) {
                    var amount = parseFloat($(this).val());
                    var result = amount * usd_to_btc;
                    if (isNaN(result)) {
                        console.log('here');
                        $('#recharge_summ').html('= '+default_btc+' $');                        
                        var tv = $(this).val();
                        var tc = (tv/ticket_price).toFixed(0);
                        var tc_w = tflip.methods.get_plural(tc,plural_arr);
                        $('.rb_tcount_w').html(tc_w);
                        $('.rb_tcount').html(tc);
                        $('#recharge_qr').attr('src',btk_url+default_btc);
                    }else {
                        $('#recharge_summ').html('= '+result.toFixed(2)+' $');
                        $('#recharge_qr').attr('src',btk_url+''+amount);
                        var tv = $(this).val()+'';
                        
                        var tc = parseInt(tv/ticket_price);
                        var tc_w = tflip.methods.get_plural(tc,plural_arr);
                        $('.rb_tcount_w').html(tc_w);
                        $('.rb_tcount').html(tc);
                    }
                }else {
                    $('#recharge_summ').html('= '+default_btc+' $');
                    $('#recharge_qr').attr('src',btk_url+default_btc);
                    var tc = (default_btc/ticket_price).toFixed(0);
                    var tc_w = tflip.methods.get_plural(tc,plural_arr);
                    $('.rb_tcount_w').html(tc_w);
                    $('.rb_tcount').html(tc);
                }
            });
        }
    },
    add_ticket_profile:function() {
        tflip.methods.loading.show();
        var free_tickets = $(document).find('.profile-ticket__item.free-tickets').length;
        $.ajax({
            async: false,
            url: '/ajax/add_ticket_profile?alreadyfree='+free_tickets,
            type:  'GET',            
            cache: false,
            success:  function(msg){ 
                tflip.methods.loading.hide();
                if (typeof msg != 'undefined') {
                    var it = $(msg);
                    $('.profile-ticket__list').append(it);                    
                }
            },
            error:function() {
                tflip.methods.loading.hide();
            }
        });
    },
    autoProfileAnimated: function(elm) {
        for(var i = 0; i<=50; i++) {
            setTimeout(function(){tflip.methods.autoProfile(elm);},500+i*10);
        }
    },
    autoProfile: function(elm) {
        $(elm).find('.num-list__item').removeClass('active');
        var arr = [];
        while(arr.length < 5){
            var randomnumber = Math.ceil(Math.random()*49)
            if(arr.indexOf(randomnumber) == -1 && arr.length != 5) {
                arr.push(randomnumber);
            }
        }
        var arra = [];
        while(arra.length < 1){
            var randomnumber = Math.ceil(Math.random()*26)
            if(arra.indexOf(randomnumber) == -1 && arra.length == 0 ) {
                arra.push(randomnumber);
            }            
        }        
        arr.forEach(function(item, i, arr) {
            $(elm).find('.first-49 li:eq('+(item-1)+')').addClass('active');
        });        
        arra.forEach(function(item, i, array) {
            $(elm).find('.num-list_yellow li:eq('+(item-1)+')').addClass('active');
        });
        $(elm).find('.first-49').attr("data-ready", true);
        $(elm).find('.first-49').addClass('ready-number-set')
        $(elm).find('.num-list_yellow').attr("data-ready", true);
        $(elm).find('.num-list_yellow').addClass('ready-number-set');
        //tflip.methods.update_total_summ();
        
    },
    clearTicketProfile: function(elm) {
        $(elm).find('.num-list__item').removeClass('active');
        $(elm).find('.first-49').attr("data-ready", false);
        $(elm).find('.num-list_yellow').attr("data-ready", false);
        $(elm).find('.first-49').removeClass('ready-number-set')
        $(elm).find('.num-list_yellow').removeClass('ready-number-set');
        //tflip.methods.update_total_summ();
    },
    //update_total_summ: function () {
    //    var ready_count = 0;
    //    $(document).find('.box-ticket').each(function () {
    //        var ready = 0;
    //        $(this).find('.num-list').each(function () {                
    //            if ($(this).attr('data-ready')=='true') { ready++;}
    //        });
    //        if(ready==2) {
    //            ready_count++;
    //            $(this).attr('data-ready', true);
    //        }
    //    });
    //    console.log(ready_count+' - Ready count');
    //    $('.price-inf__check-ticket_number').text(ready_count);
    //    var free = $(document).find('.profile-ticket__item.free-tickets').length;
    //    var readyPrice = ((ready_count-free) *  (ticket_price * 1000 ) / 1000);
    //    if (readyPrice < 0 ) {
    //        readyPrice = 0;
    //    }
    //    $('.price-inf__summa-number').text(readyPrice);
    //},
    submit_tickets: function(){
        
    },
    reserve_tickets: function(){
        
    },
    pay_for_single_ticket: function(ticket_id) {
      tflip.methods.loading.show();
        var btn = $(this);
        setTimeout(function(){
            $.ajax({
                url: "/ajax/ticket",
                method: "POST",
                data: {'ticket_id':ticket_id},
                dataType:'json',
                cache: false,
                success: function (msg) {
                    tflip.methods.loading.hide();
                    if (msg.ans==1) {
                        $('#reserved_ticket_'+ticket_id).hide(300);
                        tflip.methods.metrik_events_trigger('ticket_purchased');                        
                        setTimeout(function(){
                            if (typeof(msg.draw_id) != 'undefined') {
                                window.location.href = '/profile/receipts/'+msg.draw_id;
                            }else {
                                window.location.href = "/profile";
                            }
                        },100);
                    }else {
                        if (typeof (msg.err) != "undefined") {
                            tflip.methods.notification(msg.err,'error');
                        }else {
                            window.location.href = "/profile";
                        }
                    }
                },
                error: function(msg){
                    tflip.methods.loading.hide();
                    tflip.methods.notification(msg.err,'error');

                }
            });
        },300);
        return false;  
    },
    notification:function(text,type) {
        if (typeof toastr != "undefined") {
        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-center",
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }        
        opts.timeOut = 5000;
        opts.progressBar = true;
        
        toastr.options = opts;
        toastr[type](text);
      }
    },
    signin_form: function() {  
        tflip.methods.loading.show();
        $.ajax({
            async: true,
            url: '/auth/signin',
            type:  'POST',
            data: $('#login1 #auth_form').serialize(),
            dataType:'json',
            cache: false,
            success:  function(msg){  
                tflip.methods.loading.hide();
                if (msg.ans == 1) {
                    if (document.location.pathname == '/ico') {
                        document.location.href='/profile/investor';
                    }else {
                        document.location.href='/profile';                    
                    }
                }else {
                    tflip.methods.notification(msg.err,'error');
                }
            },
            error: function() {
                tflip.methods.loading.hide();
                tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
            }
        });
        return false;
    },
    reset_pass_form: function(){
        tflip.methods.loading.show();
        $.ajax({
            async: true,
            url: '/auth/forgot_password',
            type:  'POST',
            data: $('#restore1 #reset_pass_form').serialize(),
            dataType:'json',
            cache: false,
            success:  function(msg){  
                tflip.methods.loading.hide();
                if (msg.ans == 1) {
                    if (typeof(msg.success_msg)!="undefined") {
                        tflip.methods.notification(msg.success_msg,'success');
                    }
                    $('.fancybox-close').trigger('click');
                }else {
                    tflip.methods.notification(msg.err,'error');
                }
            },
            error: function() {
                tflip.methods.loading.hide();
                tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
            }
        });
        return false;
    },
    check_for_cookies: function () {
        if (navigator.cookieEnabled) return true;
        // set and read cookie
        document.cookie = "cookietest=1";
        var ret = document.cookie.indexOf("cookietest=") != -1;

        // delete cookie
        document.cookie = "cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT";

        return ret;
    },
    signup_form: function(){
        tflip.methods.loading.show();
        $.ajax({
            async: true,
            url: '/auth/signup',
            type:  'POST',
            data: $('#register1 #register_form').serialize(),
            dataType:'json',
            cache: false,
            success:  function(msg){ 
                tflip.methods.loading.hide();
                if (msg.ans == 1) {
                    tflip.methods.metrik_events_trigger('registration');
                    setTimeout(function(){ 
                        if (document.location.pathname == '/ico') {
                            document.location.href='/profile/investor';
                        }else {
                            document.location.href='/profile'; 
                        }
                    },150);
                }else {
                    tflip.methods.reset_grecaptch();
                    tflip.methods.notification(msg.err,'error');
                }
            },
            error: function() {
                tflip.methods.loading.hide();
                tflip.methods.notification(tflip.lang[current_language].errors.server_error,'error');
            }
        });
        return false;
    },
    copyToClipboard_handler: function() {
        if ($(document).find('.copy_wallet_address').length) {
            if (typeof Clipboard != "undefined") {
                var a = new Clipboard('.copy_wallet_address');
                a.on('success', function(e) {
                    tflip.methods.notification(tflip.lang[current_language].success.copied_successfully,'success')
                    e.clearSelection();
                });
                a.on('error', function(e) {
                    tflip.methods.notification(tflip.lang[current_language].errors.copied_denied,'error');
                });
            }else {
                console.log('Clipboard is undefined');
            }
            /*$('.copy_wallet_address').on("click", function(e) {
                e.preventDefault();
               // var ss = tflip.methods.copyToClipboard(this);
               return false;
            });*/
        }
    },
    copyToClipboard: function(elem) {
        var targetId = "_hiddenCopyText_";
        var origSelectionStart, origSelectionEnd;
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.getAttribute('data-tocopy');
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);
        // copy the selection
        var succeed;
        try {
              succeed = document.execCommand("copy");
        } catch(e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }

        target.textContent = "";       
        return succeed;
    },
    metrik_events_trigger: function(event){
        if (typeof event != "undefined") {
            console.log('Metriks event: '+event+' fired');
            switch (event) {
                case 'copied_referral_link':
                   if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('referall_link_was_copied');
                    }  
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'referral', 'copy', 'link');
                    }                    
                    break;
                case 'ref_link_shared_in_SN':
                    if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('share_referral_link_in_sm');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'referral', 'share', 'link');
                    }                    
                    break;
                case 'refill_tab_btc':
                    if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('refill_balance_tab_btc');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'refill', 'click', 'btc');
                    }                    
                    break;
                case 'refill_tab_card':
                    if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('refill_balance_tab_card');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'refill', 'click', 'card');
                    }                    
                    break;
                case 'refill_tab_exchange':
                    if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('refill_balance_tab_exchange');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'refill', 'click', 'exchange');
                    }                    
                    break;
                case 'invest_btn_clicked':
                    if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('invest_btn_clicked');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'ico', 'click', 'invest');
                    }                    
                    break;
                case 'follow_currency_exchanger_link':
                    if (typeof yaCounter44108249!='undefined') { 
                        yaCounter44108249.reachGoal('follow_currency_exchanger_link');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'refill', 'follow', 'link');
                    }                  
                    break;
                case 'form_subscribe_ico':
                    if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('form_subscribe_ico');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'ico', 'subscribed', 'form');
                    }                
                    break;
                case 'form_feedback':
                    if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('form_feedback');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'forms', 'send', 'feedback');
                    }                
                    break;
                case 'TFL_purchased':
                    if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('TFL_purchased');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'investor', 'purchase', 'TFL');
                    }             
                    break;
                case 'ticket_purchased':
                    if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('ticket_purchased');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'profile', 'purchase', 'ticket');
                    }        
                    break;
                case 'registration':
                    if (typeof yaCounter44108249 != "undefined") {
                        yaCounter44108249.reachGoal('registration');
                    }
                    if (typeof ga != "undefined") {
                        ga('send', 'event', 'users', 'complete', 'registration');
                    }        
                    break;
                case 'wallet_address_copied':
                    if (typeof yaCounter44108249!='undefined') { 
                        yaCounter44108249.reachGoal('wallet_address_copied'); 
                    }
                    if (typeof ga != 'undefined') { 
                        ga('send', 'event', 'refill', 'copy', 'wallet_address'); 
                    }
                    break;
                case 'profile_investor_buy':
                    if (typeof yaCounter44108249!='undefined') {
                        yaCounter44108249.reachGoal('profile_investor_buy');
                    }
                    break;
                default:
                    console.log('Unknown event');
                    return false;
            }
        }
    }
}
tflip.init();