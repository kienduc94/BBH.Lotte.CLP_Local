function GetXmlHttp(){var e=null;try{e=new XMLHttpRequest}catch(t){try{e=new ActiveXObject("Msxml2.XMLHTTP")}catch(t){e=new ActiveXObject("Microsoft.XMLHTTP")}}return e}


	var FinishText = "Click 'Check Results'";


var Selections = new Array();
var MustSelect = new Array();
var BallPool = new Array();
var BallNames = new Array();
var BallClasses = new Array();


		Selections[0] = 0;
		Selections[1] = 0;
		MustSelect[0] = 5;
		MustSelect[1] = 1;
		BallPool[0] = 49;
		BallPool[1] = 26;
		BallNames[0] = "number";
		BallNames[1] = "Thunderball";
		BallClasses[0] = "thunderball-ball";
		BallClasses[1] = "thunderball-thunderball";


//#####################################
//# CLEAR THE CHECKER FORM
//#####################################

function ClearForm() {
	for (x = 0; x < MustSelect.length; x++) {
		for (i = 1; i <= BallPool[x]; i++) {
			document.getElementById("B" + x + "ID_" + i).className = "checkerNumber";
			if (x > 0) {document.getElementById("B"+x+"ID_" + i).className += " type2";}
		}
		for (i = 1; i <= MustSelect[x]; i++) {
			document.getElementById("B" + x + "_" + i).value = "";
			document.getElementById("empty" + x + i).innerHTML = "";
		}
	}
	Selections[0] = Selections[1] = 0;
	document.getElementById("checkerStatus").innerHTML = "Select " + MustSelect[0] + " " +  BallNames[0] + "s";
	if (BallNames.length > 1) {
		document.getElementById("checkerStatus").innerHTML += " and " + MustSelect[1] + " " + BallNames[1]
		if (MustSelect[1] > 1) {document.getElementById("checkerStatus").innerHTML += "s"}
	}
	document.getElementById("submit_checker").className = "button-blue disabled fluid";
	
	
	//var xmlHttp = GetXmlHttp();
	//xmlHttp.open("GET","/checker/_clear-cookies.asp?Lottery=thunderball",true);
	//xmlHttp.send(null);
}



//#####################################
//# NUMERICALLY SORT NUMBERS ARRAY
//#####################################

function SortNumbers() {
	debugger
	var temp;
	for (x = 0; x < MustSelect.length; x++) {
		for (i = 1; i <= MustSelect[x]; i++) {
			for (j = i+1; j <= MustSelect[x]; j++) {
				if (parseInt(document.getElementById("B"+x+"_"+j).value) < parseInt(document.getElementById("B"+x+"_"+i).value) || document.getElementById("B"+x+"_"+i).value == '') {
					temp = document.getElementById("B" + x + "_"+i).value;
					document.getElementById("B" + x + "_"+i).value = document.getElementById("B" + x + "_"+j).value
					document.getElementById("B" + x + "_"+j).value = temp;
				}
			}
		}
		for (i = 1; i <= MustSelect[x]; i++) {
			document.getElementById("empty" + x + i).innerHTML = "";
		}
		for (i = 1; i <= Selections[x]; i++) {
			document.getElementById("empty" + x + i).innerHTML = "<div class='result " + BallClasses[x] + " small'>" + document.getElementById("B" + x + "_"+i).value + "</div>";
		}
	}
};


//#####################################
//# SELECT NUMBER
//#####################################

function Select(n, s) {
    debugger
	if (document.getElementById("B"+s+"ID_" + n).className.indexOf('selected') != -1 || Selections[s] < MustSelect[s]) {
		for (b = 1; b <= BallPool[s]; b++) {
			if (n == b && document.getElementById("B"+s+"ID_" + n).className.indexOf('selected') != -1) {
				document.getElementById("B"+s+"ID_" + n).className -= " selected";
				for (i = 1; i <= MustSelect[s]; i++) {
					if (document.getElementById("B"+s+"_"+i).value == n) {
						document.getElementById("B"+s+"_"+i).value = '';
						Selections[s]--;
						break;
					};
				}
				SortNumbers();
				break;
			} else if (n == b) {
				Selections[s]++;
				document.getElementById("B"+s+"ID_" + n).className += " selected";
				document.getElementById("B"+s+"_"+Selections[s]).value = n;
				SortNumbers();
				break;
			}
		}
	} else {
		var AlertMsg = "You already have " + MustSelect[s] + " " + BallNames[s];
		if (MustSelect[s] > 1) {AlertMsg += "s"}
		AlertMsg += " selected. Please deselect one to continue."
		alert(AlertMsg)
	}
	SelectedNumbers(0);
}



//#####################################
//# NUMBER CLASSES AND STATUS MESSAGE
//#####################################

function SelectedNumbers(onload) {
	debugger
	if (onload == 1) {
		for (x = 0; x < MustSelect.length; x++) {
			for (i = 1; i <= MustSelect[x]; i++) {
				if (document.getElementById("B"+x+"_" + i).value !== "") {
					document.getElementById("empty"+x+i).innerHTML = "<div class='result " + BallClasses[x] + " small'>" + document.getElementById("B"+x+"_" + i).value + "</div>";
					document.getElementById("B"+x+"ID_" + document.getElementById("B"+x+"_" + i).value).className = "checkerNumber selected";
					Selections[x]++;
				}
			}
		}
		SortNumbers();
	}
	
	for (x = 0; x < MustSelect.length; x++) {
		if (Selections[x] >= MustSelect[x]) {
			for (b = 1; b <= BallPool[x]; b++) {
				if (document.getElementById("B"+x+"ID_" + b).className.indexOf('selected') == -1) {
					document.getElementById("B"+x+"ID_" + b).className = "checkerNumber disabled";
				}
			}
		} else {
			for (b = 1; b <= BallPool[x]; b++) {
				if (document.getElementById("B"+x+"ID_" + b).className.indexOf('selected') == -1) {
					document.getElementById("B"+x+"ID_" + b).className = "checkerNumber";
					if (x > 0) {document.getElementById("B"+x+"ID_" + b).className += " type2";}
				}
			}
		}
	}
	
	var Message = "";
	for (x = 0; x < MustSelect.length; x++) {
		var NumbersLeft = MustSelect[x] - Selections[x];
		if (NumbersLeft > 0 && Message == "") {
			Message += "Select " + NumbersLeft + " " + BallNames[x];
			if (NumbersLeft > 1) {Message += "s"}
		} else if (NumbersLeft > 0) {
			Message += " and " + NumbersLeft + " " + BallNames[x];
			if (NumbersLeft > 1) {Message += "s"}
		}
	}
	if (Message == "") {
		Message = FinishText;
		document.getElementById("submit_checker").className = "button-blue fluid";
	} else {
		document.getElementById("submit_checker").className = "button-blue disabled fluid";
	}
    document.getElementById("checkerStatus").innerHTML = Message;
}