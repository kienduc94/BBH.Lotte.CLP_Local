﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BBH.Lotte.CLP.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("favicon.ico");

            routes.MapRoute("RandomTicketNumber", "Common/RandomTicketNumber", new { controller = "Common", action = "RandomTicketNumber" });
            

            //********************************Index***********************************
            routes.MapRoute("AboutIndex", "about", new { controller = "Home", action = "About" });
            routes.MapRoute("ContactIndex", "contact", new { controller = "Home", action = "Contact" });
            routes.MapRoute("WinningNumberIndex", "winning-number", new { controller = "Home", action = "WinningNumber" });
            routes.MapRoute("GameIndex", "play-game", new { controller = "Game", action = "index" });
            //********************************Index***********************************


            routes.MapRoute("RegisterMember", "sign-up", new { controller = "Register", action = "Index" });
            routes.MapRoute("LoginMember", "login", new { controller = "Login", action = "Index" });
            routes.MapRoute("PackageMember", "package", new { controller = "Package", action = "Index" });
            routes.MapRoute("AwardResult", "lotte-result", new { controller = "Award", action = "Index" });
            routes.MapRoute("BookingTicket", "booking-tickets", new { controller = "Booking", action = "Index" });

            routes.MapRoute("PointMember", "top-up", new { controller = "Point", action = "Index" });
            routes.MapRoute("UpdatePass", "change-password", new { controller = "Member", action = "ChangePassword" });
            routes.MapRoute("TransactionPoint", "points-history", new { controller = "Member", action = "TransactionPointMember" });
            routes.MapRoute("TransactionPackage", "packages-history", new { controller = "Member", action = "TransactionPackageMember" });
            routes.MapRoute("TransactionBooking", "tickets-history", new { controller = "Member", action = "TransactionBookingTicketMember" }); 
            routes.MapRoute("TicketWinning", "tickets-winning-history", new { controller = "Member", action = "TicketWinning" });//TransactionTicketWinning
            //routes.MapRoute("TransactionWallet", "wallets-history", new { controller = "Point", action = "TransactionWallet" });
            //routes.MapRoute("TransactionWallet", "wallets-history", new { controller = "Coin", action = "TransactionBTCWallet" });
            routes.MapRoute("TransactionBitcoinWallet", "my-transaction", new { controller = "Coin", action = "TransactionBTCWallet" });
            routes.MapRoute("TransactionCarcoinWallet", "carcoin-wallets-history", new { controller = "Coin", action = "TransactionCarWallet" });

            routes.MapRoute("CheckTransparency", "check-transparency", new { controller = "Checker", action = "CheckTransparency" });
            routes.MapRoute("CheckResult", "checker/check-result", new { controller = "Checker", action = "CheckResult" });
            routes.MapRoute("WithDraw", "with-drawal", new { controller = "Point", action = "WithDraw" });
            routes.MapRoute("LinkExpired", "link-expired", new { controller = "Member", action = "LinkExpired" });
            routes.MapRoute("AboutUs", "aboutus", new { controller = "Home", action = "AboutUs" });
            routes.MapRoute("FAQ", "faq", new { controller = "Home", action = "FAQ" });
            routes.MapRoute("ContactUs", "contactus", new { controller = "Home", action = "ContactUs" });
            routes.MapRoute("HowToPlay", "howtoplay", new { controller = "Home", action = "HowToPlay" });
            routes.MapRoute("TermOfUse", "termofuse", new { controller = "Home", action = "TermOfUse" });
            routes.MapRoute("Policy", "policy", new { controller = "Home", action = "Policy" });
            //routes.MapRoute("Error", "error", new { controller = "Error", action = "Index" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}