﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class RippleAccount
    {
        public string address { get; set; }
        public string secret { get; set; }
    }
}