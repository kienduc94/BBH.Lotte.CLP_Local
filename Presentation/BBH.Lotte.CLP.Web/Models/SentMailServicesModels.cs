﻿using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CLP.Web.SendMailSvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class SentMailServicesModels
    {
        const string WSPath = "SendMailSvc.svc";
        static SendMailSvcClient objWSSentMail = null;
        public static SendMailSvcClient WSSentMail
        {
            get
            {
                //Khởi tạo object Services Mail
                if (objWSSentMail == null)
                {
                    objWSSentMail = new SendMailSvcClient();
                    objWSSentMail.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings[KeyManager._WSSENTMAILSERVICES] + "/" + WSPath);
                }

                return objWSSentMail;
            }
        }
    }
}