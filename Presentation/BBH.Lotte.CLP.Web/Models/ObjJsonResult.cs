﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class ObjJsonResult
    {
        public List<string> LstString { get; set; }

        public string StrValue { get; set; }
    }
}