﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class Outcome
    {
        public string result { get; set; }
        public string timestamp { get; set; }
        public double fee { get; set; }
        public Object balanceChanges { get; set; }
        public Object orderbookChanges { get; set; }
        public double ledgerVersion { get; set; }
        public double indexInLedger { get; set; }
    }
}