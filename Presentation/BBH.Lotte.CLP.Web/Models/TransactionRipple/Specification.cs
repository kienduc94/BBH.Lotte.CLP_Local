﻿using BBH.Lotte.CLP.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class Specification
    {
        public Source source { get; set; }
        public Destination destination { get; set; }
        public string paths { get; set; }
        public bool allowPartialPayment { get; set; }
        public bool noDirectRipple { get; set; }
        public bool limitQuality { get; set; }
        public string direction { get; set; }
        public Object quantity { get; set; }
        public Object totalPrice { get; set; }
    }
}