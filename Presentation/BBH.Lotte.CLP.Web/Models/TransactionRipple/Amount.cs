﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class Amount
    {
        public string currency { get; set; }
        public double value { get; set; }
        public string counterparty { get; set; }
    }
}