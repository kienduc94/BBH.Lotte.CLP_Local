﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class Source
    {
        public string address { get; set; }
        public MaxAmount maxAmount { get; set; }
    }
}