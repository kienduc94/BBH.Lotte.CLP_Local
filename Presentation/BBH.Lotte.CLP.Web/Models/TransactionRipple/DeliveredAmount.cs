﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class DeliveredAmount
    {
        public string currency { get; set; }
        public string value { get; set; }
        public string counterparty { get; set; }
    }
}