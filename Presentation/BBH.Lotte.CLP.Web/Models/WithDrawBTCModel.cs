﻿using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static BBH.Lotte.CLP.Web.Models.Validation;

namespace BBH.Lotte.CLP.Web.Models
{
    public class WithDrawBTCModel
    {

        [Required(ErrorMessage = "Enter the correct wallet address.")]
        public string AddrressBTC { get; set; }
        [Required(ErrorMessage = "Cannot be empty.")]
        [RegularExpression(@"^\d*\.?\d*$", ErrorMessage = "Amount need to be present and less or equal to available balance!")]
        [Range(0.001, double.MaxValue, ErrorMessage = "Minimum withdrawing amount: 0.001.")]
        [Remote("CheckAvailableBTC", "Validate")]
        public double AmountBTC { get; set;}
        //[Required(ErrorMessage = "This field is required.")]
        //[RegularExpression(@"^\d*\.?\d*$")]
        //[Remote("CheckTotalBTCWithdraw", "Validate")]
        public double TotalBTC { get; set; }
    }
}