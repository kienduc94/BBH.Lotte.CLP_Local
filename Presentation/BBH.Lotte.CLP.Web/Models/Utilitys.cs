﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class Utilitys
    {
        public static void WriteLog(string path, string message)
        {

            if (!File.Exists(path))
            {
                using (StreamWriter w = File.CreateText(path))
                {
                    Log(message, w);
                }
            }
            else
            {
                using (StreamWriter w = File.AppendText(path))
                {
                    Log(message, w);
                }
            }
        }

        private static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\n" + DateTime.Now.ToString());
            w.WriteLine(" {0}", logMessage);
            w.WriteLine("-----------------------------------");
        }

        public static string EncodeString(string value)
        {
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(value);
            encodedBytes = md5.ComputeHash(originalBytes);
            return BitConverter.ToString(encodedBytes);
        }
        public static string MaHoaMD5(string pass)
        {
            try
            {

                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] data = Encoding.UTF8.GetBytes(pass);
                data = md5.ComputeHash(data);
                StringBuilder buider = new StringBuilder();
                foreach (byte b in data)
                {
                    buider.Append(b.ToString("x2"));
                }
                return buider.ToString();
            }
            catch
            {
                return pass;
            }
        }
        public static string GenCode()
        {
            Random objRandom = new Random();
            string combination = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder captchaCode = new StringBuilder();

            for (int i = 0; i < 8; i++)
            {
                captchaCode.Append(combination[objRandom.Next(combination.Length)]);
            }
            return captchaCode.ToString();
        }



        public static List<string> ListNumberRandom(int numberTicket)
        {
            Random r = new Random();
            List<string> lstNumber = new List<string>();
            try
            {
                for (int j = 0; j < numberTicket; j++)
                {
                    string strNumber = string.Empty;
                    List<int> lst = new List<int>();
                    int maxNumber = 5;
                    for (int i = 0; i < maxNumber; i++)
                    {
                        
                        int number = r.Next(1, 40);
                        if (lst == null || lst.Count() == 0)
                        {
                            lst.Add(number);
                        }
                        else
                        {
                            if (!lst.Contains(number))
                            {
                                lst.Add(number);
                            }
                            else
                            {
                                maxNumber += 1;
                            }
                        }
                    }
                    lst.Sort();
                    int extraNumber = r.Next(1, 19);
                    if (lst != null && lst.Count() > 0)
                    {
                        int i = 0;
                        foreach (int n in lst)
                        {
                            strNumber += n;
                            if (i < lst.Count - 1)
                            {
                                strNumber += ",";
                            }
                            i++;
                        }
                    }
                    strNumber += "," + extraNumber;
                    if (lstNumber == null || lst.Count() == 0)
                    {
                        lstNumber.Add(strNumber);
                    }
                    else
                    {
                        if (!lstNumber.Contains(strNumber))
                        {
                            lstNumber.Add(strNumber);
                        }
                    }
                }
            }
            catch
            {

            }
            return lstNumber;
        }

        public static DateTime OpenDate(DateTime date)
        {
            string hour = ConfigurationManager.AppSettings["OpenHour"];
            DateTime openDate = DateTime.Now;
            int day = date.Day + 1;
            int month = date.Month;
            int year = date.Year;
            switch (month)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    {
                        if (day > 31)
                        {
                            day = 1;
                            month += 1;
                        }
                        if (month > 12)
                        {
                            month = 1;
                            year += 1;
                        }
                        break;
                    }
                case 4:
                case 6:
                case 9:
                case 11:
                    {
                        if (day > 30)
                        {
                            day = 1;
                            month += 1;
                        }
                        if (month > 12)
                        {
                            month = 1;
                            year += 1;
                        }
                        break;
                    }
            }
            string dayOpen = month + "/" + day + "/" + year + " " + hour;
            try
            {
                openDate = DateTime.Parse(dayOpen);
            }
            catch
            {
                dayOpen = day + "/" + month + "/" + year + " " + hour;
                openDate = DateTime.Parse(dayOpen);
            }
            return openDate;
        }
    }
}