﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class TicketObj
    {
        public string NumberTicket { get; set; }
        public int Quantity { get; set; }

        public int FirstNumber { get; set; }
        public int SecondNumber { get; set; }
        public int ThirdNumber { get; set; }
        public int FourthNumber { get; set; }
        public int FivethNumber { get; set; }
        public int ExtraNumber { get; set; }
    }
}