﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.Web.Models
{
    public class WithDrawCarcoinModel
    {
        [Required(ErrorMessage = "Enter the correct wallet address.")]
        public string AddrressCarCoin { get; set; }
        [Required(ErrorMessage = "Cannot be empty.")]
        [RegularExpression(@"^\d*\.?\d*$", ErrorMessage = "Amount need to be present and less or equal to available balance!")]
        [Range(10, double.MaxValue, ErrorMessage = "Minimum withdrawing amount: 10.")]
        [Remote("CheckAvailableCarCoin", "Validate")]
        public double AmountCarCoin { get; set; }
        //[Required(ErrorMessage = "This field is required.")]
        //[RegularExpression(@"^\d*\.?\d*$")]
        //[Remote("CheckTotalCarCoinWithdraw", "Validate")]
        public double TotalCarCoin { get; set; }
    }
}