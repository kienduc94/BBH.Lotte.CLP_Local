﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string EmailLogin { get; set; }

        [Required(ErrorMessage = "Please input your password")]
        public string PasswordLogin { get; set; }
        [Required(ErrorMessage = "Please input your password")]
        public bool IsRemeberme { get; set; }
    }
}