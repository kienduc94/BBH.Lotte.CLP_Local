﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class RecoverPasswordViewModel
    {
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        [MinLength(8, ErrorMessage = "Invalid email address")]
        public string EmailRecoverassword { get; set; }
    }
}