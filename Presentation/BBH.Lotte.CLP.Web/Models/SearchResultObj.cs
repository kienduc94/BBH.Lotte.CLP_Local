﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class SearchResultObj
    {
        public string TitleResult { get; set; }
        public string ContentResult { get; set; }
    }
}