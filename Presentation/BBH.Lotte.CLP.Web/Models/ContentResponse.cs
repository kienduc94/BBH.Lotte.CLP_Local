﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class ContentResponse
    {
        public string result { get; set; }
        public string email { get; set; }
        public string error_msg { get; set; }
    }

    public class ApiUpdateTicketResponse
    {
        public string result { get; set; }
        public List<object> sucess_list { get; set; }
    }

    public class TicketObjInput
    {
        public int id { get; set; }
        public string email { get; set; }
        public string ticket { get; set; }
        public string date_time { get; set; }
    }
}