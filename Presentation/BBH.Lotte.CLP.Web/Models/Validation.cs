﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.Web.Models
{
    public class Validation
    {
        public class MinValueAttribute : ValidationAttribute, IClientValidatable
        {
            private readonly double _minValue;

            public MinValueAttribute(double minValue)
            {
                _minValue = minValue;
            }

            public override bool IsValid(object value)
            {
                return (double)value >= _minValue;
            }

            public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
            {
                yield return new ModelClientValidationRule
                {
                    ErrorMessage = this.ErrorMessage
                };
            }

            
        }
    }
}