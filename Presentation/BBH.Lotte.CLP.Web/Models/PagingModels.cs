﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
namespace BBH.Lotte.CLP.Web.Models
{
    public class PagingModels
    {
        public static string GenPaging(string p, int intTotalRecord)
        {
            StringBuilder builder = new StringBuilder();
            int pageSize = 5, page = 1;

            try
            {
                if (p != null && p != "")
                {
                    page = int.Parse(p);
                }
            }
            catch
            {

            }
            try
            {
                pageSize = int.Parse(ConfigurationManager.AppSettings["NumberRecordPage"]);
            }
            catch
            {

            }
            string linkPage = "", nextLink = "", previewLink = "", firstLink = "", lastLink = "";
            firstLink = "?p=1";
            if (page <= 1)
            {
                previewLink = "?p=1";
            }
            else
            {
                previewLink = "?p=" + (page - 1).ToString();
            }

            builder.Append("<nav class=\"text-center\">");
            builder.Append("<ul id=\"ulPaging\" class=\"pagination\">");

            int pagerank = 5;
            int next = 10;
            int prev = 1;

            int totalRecore = intTotalRecord;
            int totalPage = totalRecore / pageSize;
            int balance = totalRecore % pageSize;
            if (balance != 0)
            {
                totalPage += 1;
            }
            if (page >= totalPage)
            {
                nextLink = "?p=" + totalPage.ToString();
            }
            else
            {
                nextLink = "?p=" + (page + 1).ToString();
            }
            int currentPage = page;
            var m = 1;
            if (totalPage > 10)
            {
                if (page > pagerank + 1)
                {
                    next = (page + pagerank) - 1;
                    m = page - pagerank;
                }
                if (page <= pagerank)
                {
                    prev = (page - pagerank);
                    m = 1;
                }
                if (next > totalPage)
                {
                    next = totalPage;
                }
                if (prev < 1)
                {
                    prev = 1;
                }
            }
            else
            {
                next = totalPage;
                prev = 1;
            }
            lastLink = "?p=" + totalPage;
            if (totalPage > 1)
            {
                if (currentPage > 1)
                {
                    builder.Append("<li class=\"page-item\">");
                    builder.Append("<a href=\"" + previewLink + "\" class=\"page-link\" aria-label=\"Previous\">");
                    builder.Append("<span aria-hidden=\"true\">");
                    builder.Append("<i class=\"fa fa-angle-double-left\" area-hidden=\"hidden\"></i>");
                    builder.Append("</span>");
                    builder.Append("</a>");
                    builder.Append("</li>");
                }
            }

            if (totalPage > 1)
            {

                for (; m <= next; m++)
                {
                    linkPage = "?p=" + m;
                    if (m == currentPage)
                    {
                        builder.Append("<li class=\"page-item active\"><a class=\"page-link\">" + m + "</a></li>");
                    }
                    else
                    {
                        builder.Append("<li class=\"page-item\"><a href=\"" + linkPage + "\" class=\"page-link\">" + m + "</a></li>");
                    }
                }
            }

            if (totalPage > 1)
            {
                if (currentPage < totalPage)
                {
                    builder.Append("<li class=\"page-item\">");
                    builder.Append("<a href=\"" + nextLink + "\" class=\"page-link\" aria-label=\"Next\">");
                    builder.Append("<span aria-hidden=\"true\">");
                    builder.Append("<i class=\"fa fa-angle-double-right\" area-hidden=\"hidden\"></i>");
                    builder.Append("</span>");
                    builder.Append("</a>");
                    builder.Append("</li>");
                }
            }


            builder.Append("</ul>");
            builder.Append("</nav>");
            return builder.ToString();
        }

    }
}