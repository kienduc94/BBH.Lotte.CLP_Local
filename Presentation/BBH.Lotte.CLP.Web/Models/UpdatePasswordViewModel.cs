﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.Web.Models
{
    public class UpdatePasswordViewModel
    {
        [Required(ErrorMessage = "Please input your password")]
        [DataType(DataType.Password)]
        [StringLength(250, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [Display(Name = "Password")]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string PasswordUpdate { get; set; }

        [DataType(DataType.Password)]
        [Compare("PasswordUpdate", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string ConfirmPasswordUpdate { get; set; }

        public string EmailUpdatePass { get; set; }

        public string LinkReferenceUpdatePassword { get; set; }
    }
}