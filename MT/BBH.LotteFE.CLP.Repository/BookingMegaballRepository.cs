﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Shared;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace BBH.LotteFE.CLP.Repository
{
    public class BookingMegaballRepository : WCFClient<IBookingMegaballServices>, IBookingMegaballServices
    {
        //BookingMegaballServicesSvc.BookingMegaballServicesClient services = new BookingMegaballServicesSvc.BookingMegaballServicesClient();
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public ObjResultMessage InsertBooking(BookingMegaball booking)
        {
            return Proxy.InsertBooking(booking);
        }

        public IEnumerable<BookingMegaball> ListTransactionBookingByMember(int memberID, int start, int end)
        {
            return Proxy.ListTransactionBookingByMember(memberID,start,end);
        }

        public IEnumerable<BookingMegaball> ListTransactionBookingByMemberPaging(int memberID, int start, int end)
        {
            return Proxy.ListTransactionBookingByMemberPaging(memberID, start, end);
        }

        /// <summary>
        
        /// Edited date: 12.01.2018
        /// Description: Thay param DateTime -> DateTime? (doi voi truong hop mac dinh la null)
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<BookingMegaball> ListTransactionBookingBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            return Proxy.ListTransactionBookingBySearch(memberID, fromDate, toDate, start, end);
        }

        public IEnumerable<BookingMegaball> ListTransactionBookingByDate(DateTime fromDate, DateTime toDate)
        {
            return Proxy.ListTransactionBookingByDate(fromDate, toDate);
        }

        public bool UpdateStatusBooking(int bookingID, int status)
        {
            return Proxy.UpdateStatusBooking(bookingID, status);
        }

        public int CountMemberBuyNumber(int memberID, int firstNumber, int secondNumber, int thirdNumber, int fourthNumber, int fivethNumber, DateTime fromDate, DateTime toDate)
        {
            return Proxy.CountMemberBuyNumber(memberID, firstNumber, secondNumber, thirdNumber, fourthNumber, fivethNumber, fromDate, toDate);
        }
        public IEnumerable<BookingMegaball> ListTransactionBookingOrderByDate(int start, int end)
        {
            return Proxy.ListTransactionBookingOrderByDate(start, end);
        }

        public ObjResultMessage InsertListBooking(ModelRSA objRsa, double doublePoint, int intNumberID = 0, double dblTotalJackPot = 0)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            //string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
               
                if (isCheckToken)
                {
                    objResult = Proxy.InsertListBooking(objRsa, doublePoint, intNumberID, dblTotalJackPot);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "BookingMegaballRepository -> InsertListBooking -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "BookingMegaballRepository -> InsertListBooking : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
            }

            return objResult;
        }
      
        public float SumValuePoint()
        {
            return Proxy.SumValuePoint();
        }
        public IEnumerable<BookingMegaball> ListBookingByAwardDate(int intAwardNumber)
        {
            return Proxy.ListBookingByAwardDate(intAwardNumber);
        }

        public IEnumerable<BookingMegaball> ListBookingMegaballByDate(DateTime dtmDate)
        {
            return Proxy.ListBookingMegaballByDate(dtmDate);
        }

        public List<BookingMegaball> ListTransactionBookingWinnigByMemberSearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            return Proxy.ListTransactionBookingWinnigByMemberSearch(memberID,fromDate,toDate,start,end);
        }

        public IEnumerable<BookingMegaball> ListAllBookingMegaballByDraw(int intDrawID)
        {
            return Proxy.ListAllBookingMegaballByDraw(intDrawID);
        }
        public IEnumerable<BookingMegaball> ListBookingByDraw(int intDrawID, int start, int end)
        {
            return Proxy.ListBookingByDraw(intDrawID,start,end);
        }
        public List<BookingMegaball> ListAllBooking()
        {
            return Proxy.ListAllBooking();
        }
        public IEnumerable<AwardBO> GetAllListAward()
        {
            return Proxy.GetAllListAward();
        }
        public IEnumerable<BookingMegaball> SP_ListBookingByDraw_FE(int intDrawID)
        {
            return Proxy.SP_ListBookingByDraw_FE(intDrawID);
        }


        public int InsertTicketNumberTimeLog(string strEmail, int intTicketNumber, DateTime dtmCreatedDate)
        {
            return Proxy.InsertTicketNumberTimeLog(strEmail, intTicketNumber, dtmCreatedDate);
        }
    }
}
