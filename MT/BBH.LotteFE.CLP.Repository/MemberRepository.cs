﻿using BBC.Core.WebService;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
namespace BBH.LotteFE.CLP.Repository
{

    public class MemberRepository : WCFClient<IMemberServices>, IMemberServices
    {
        //MemberServicesClient service = new MemberServicesClient();

        public bool InsertMember(MemberBO objMemberBO, ref int intMemberID)
        {
            try
            {
                return Proxy.InsertMember(objMemberBO, ref intMemberID);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdatePasswordMember(string email, string newpassword)
        {
            try
            {
                return Proxy.UpdatePasswordMember(email, newpassword);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateNewPasswordMember(string email, string oldpassword, string newpassword)
        {
            try
            {
                return Proxy.UpdateNewPasswordMember(email, oldpassword, newpassword);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateInformationMember(MemberBO member, string email)
        {
            try
            {
                return Proxy.UpdateInformationMember(member, email);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool LockAndUnlockMember(int memberID, int isActive)
        {
            return Proxy.LockAndUnlockMember(memberID, isActive);
        }

        public MemberBO GetMemberDetailByID(int memberID)
        {
            return Proxy.GetMemberDetailByID(memberID);
        }

        public MemberBO GetMemberDetailByEmail(string strEmail)
        {
            return Proxy.GetMemberDetailByEmail(strEmail);
        }

        public double GetPointsMember(int memberID)
        {
            return Proxy.GetPointsMember(memberID);
        }
        public int GetTotalSSNSMember(string email, int totalsSMS)
        {
            return Proxy.GetTotalSSNSMember(email, totalsSMS);
        }
        public MemberBO LoginMember(string userName, string password)
        {
            MemberBO member = null;
            try
            {
                member = Proxy.LoginMember(userName, password);
                return member;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public bool CheckUserNameExists(string strUserName)
        {
            try
            {
                return Proxy.CheckUserNameExists(strUserName);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CheckMobileExists(string mobile)
        {
            try
            {
                return Proxy.CheckMobileExists(mobile);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool CheckMobileMemberExists(string mobile, string email)
        {
            try
            {
                return Proxy.CheckMobileMemberExists(mobile,email);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool CheckE_WalletExists(string strE_Wallet)
        {
            try
            {
                return Proxy.CheckE_WalletExists(strE_Wallet);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CheckEmailExists(string strEmail)
        {
            try
            {
                return Proxy.CheckEmailExists(strEmail);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<MemberInfomationBO> GetListMember()
        {

            return Proxy.GetListMember();
        }

        public IEnumerable<MemberInfomationBO> GetListMemberInfomation(int start, int end)
        {
            return Proxy.GetListMemberInfomation(start, end);
        }
        public IEnumerable<CountryBO> GetListCountry(int start, int end)
        {
            return Proxy.GetListCountry(start, end);
        }
        public bool UpdatePointsMember(int memberID, int points)
        {
            return Proxy.UpdatePointsMember(memberID, points);
        }

        public int CountAllMember()
        {
            return Proxy.CountAllMember();
        }
        public MemberInfomationBO GetMemberDetail(string strEmail)
        {
            return Proxy.GetMemberDetail(strEmail);
        }
        public bool InsertMemberWallet(MemberWalletBO objMemberWalletBO)
        {
            try
            {
                return Proxy.InsertMemberWallet(objMemberWalletBO);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdatePointsMemberFE(int memberID, double points)
        {
            return Proxy.UpdatePointsMemberFE(memberID, points);
        }
        public int InsertMemberAfterLoginFB(ref MemberBO objMember)
        {
            return Proxy.InsertMemberAfterLoginFB(ref objMember);
        }
        public bool DeleteMember(int intMmberID)
        {
            return Proxy.DeleteMember(intMmberID);
        }
        public bool ResetPasswordMember(string strEmail)
        {
            return Proxy.ResetPasswordMember(strEmail);
        }

        public IEnumerable<LinkResetPassword> GetListLinkResetPassword(DateTime dtCreatedDate, string strEmail)
        {
            return Proxy.GetListLinkResetPassword(dtCreatedDate, strEmail);
        }

        public bool InsertLinkResetPassword(LinkResetPassword objLinkResetPassword)
        {
            return Proxy.InsertLinkResetPassword(objLinkResetPassword);
        }

        public LinkResetPassword GetDetailLinkResetPassword(string strLinkReset)
        {
            return Proxy.GetDetailLinkResetPassword(strLinkReset);
        }

        public bool InsertMemberReference(MemberReferenceBO objMemberReferenceBO)
        {
            return Proxy.InsertMemberReference(objMemberReferenceBO);
        }

        public bool UpdateFreeTicketMember(int intMemberID, int intNumberTicket)
        {
            return Proxy.UpdateFreeTicketMember(intMemberID, intNumberTicket);
        }

        public IEnumerable<MemberReferenceBO> GetListMemberReferenceByMemberID(int intMemberID)
        {
            return Proxy.GetListMemberReferenceByMemberID(intMemberID);
        }

        public IEnumerable<MemberReferenceBO> GetDetailMemberReferenceByIDRef(int intMemberIDRef)
        {
            return Proxy.GetDetailMemberReferenceByIDRef(intMemberIDRef);
        }

        public bool UpdateMemberReference(MemberReferenceBO objMemberReferenceBO)
        {
            return Proxy.UpdateMemberReference(objMemberReferenceBO);
        }
        public MemberReferenceBO GetMemberReferenceByMemberID(int intMemberID)
        {
            return Proxy.GetMemberReferenceByMemberID(intMemberID);
        }
    }
}
