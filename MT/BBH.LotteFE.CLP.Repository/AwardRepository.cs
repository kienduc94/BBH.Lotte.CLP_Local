﻿
using BBC.Core.WebService;

using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.LotteFE.CLP.Repository
{

    public class AwardRepository : WCFClient<IAwardServices>, IAwardServices
    {
        
        public bool InsertAward(AwardBO award)
        {
            return Proxy.InsertAward(award);
        }

        
        public bool UpdateAward(AwardBO award)
        {
            return Proxy.UpdateAward(award);
        }

        
        public bool LockAndUnlockAward(int awardID, int isActive)
        {
            return Proxy.LockAndUnlockAward(awardID, isActive);
        }

       
        public IEnumerable<AwardBO> GetListAward(int start, int end)
        {
            return Proxy.GetListAward(start, end);
        }

       
        public bool InsertAwardNumber(AwardNumberBO award)
        {
            return Proxy.InsertAwardNumber(award);
        }

       
        public IEnumerable<AwardNumberBO> GetListAwardByDate(int start, int end, DateTime fromDate, DateTime toDate)
        {
            return Proxy.GetListAwardByDate(start, end, fromDate, toDate);
        }


        public IEnumerable<AwardBO> GetAllListAward()
        {
            return Proxy.GetAllListAward();
        }
    }
}
