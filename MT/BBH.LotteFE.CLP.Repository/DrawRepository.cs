﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Repository
{
    public class DrawRepository : WCFClient<IDrawServices>, IDrawServices
    {
        public DrawBO GetDrawIDByDate(DateTime dtTime)
        {
            try
            {
                return Proxy.GetDrawIDByDate(dtTime);
            }
            catch
            {
                return null;
            }
        }

        public ObjResultMessage InsertDraw(DrawBO objDraw)
        {
            try
            {
                return Proxy.InsertDraw(objDraw);
            }
            catch
            {
                return null;
            }
        }
        public DrawBO GetNewestDrawID()
        {
            try
            {
                return Proxy.GetNewestDrawID();
            }
            catch
            {
                return null;
            }
        }
    }
}
