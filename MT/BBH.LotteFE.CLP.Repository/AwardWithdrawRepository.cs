﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Shared;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Repository
{
    public class AwardWithdrawRepository : WCFClient<IAwardWithdrawServices>, IAwardWithdrawServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public ObjResultMessage InsertAwardWithdraw(AwardWithdrawBO awardWithdraw)
        {
            return Proxy.InsertAwardWithdraw(awardWithdraw);
        }

        public ObjResultMessage GetAwardWithdrawDetail(string requestMemberID, DateTime awardDate,int bookingID, ref AwardWithdrawBO objAwardWithdraw)
        {
            return Proxy.GetAwardWithdrawDetail(requestMemberID, awardDate, bookingID, ref objAwardWithdraw);
        }
        public IEnumerable<AwardWithdrawBO> GetAllListAward()
        {
            return Proxy.GetAllListAward();
        }

        public ObjResultMessage UpdateStatusAwardWithdraw(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.UpdateStatusAwardWithdraw(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardWithdrawRepository -> UpdateStatusAwardWithdraw -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardWithdrawRepository -> UpdateStatusAwardWithdraw : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(pathLog, ex.Message);
            }
            return objResult;
        }
    }
}
