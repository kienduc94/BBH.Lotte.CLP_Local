﻿using BBC.Core.WebService;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Repository
{
    public class FreeTicketRepository : WCFClient<IFreeTicketServices>, IFreeTicketServices
    {
        public FreeTicketBO GetFreeTicketActive()
        {
            try
            {
                return Proxy.GetFreeTicketActive();
            }
            catch
            {
                return null;
            }
        }
    }
}
