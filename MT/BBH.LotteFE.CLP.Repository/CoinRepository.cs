﻿using BBC.Core.WebService;
using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.CLP.Repository
{
    public class CoinRepository : WCFClient<ICoinServices>, ICoinServices
    {
        public IEnumerable<TransactionCoinBO> ListTransactionCoinPaging(string coinID, int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            try
            {
                IEnumerable<TransactionCoinBO> lstTransactionCoin = Proxy.ListTransactionCoinPaging(coinID, memberID, fromDate, toDate, start, end);
                return lstTransactionCoin;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<TransactionCoinBO> ListTransactionCoinWalletBySearch(string coinID, int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            try
            {
                IEnumerable<TransactionCoinBO> lstTransactionCoin = Proxy.ListTransactionCoinWalletBySearch(coinID, memberID, fromDate, toDate, start, end);
                return lstTransactionCoin;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
