﻿using BBC.Core.WebService;

using BBH.LotteFE.CLP.Domain;
using BBH.LotteFE.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.LotteFE.CLP.Repository
{
    public class AwardMegaballRepository : WCFClient<IAwardMegaballServices>, IAwardMegaballServices
    {
        // AwardMegaballServicesSvc.AwardMegaballServicesClient services = new AwardMegaballServicesSvc.AwardMegaballServicesClient();
        public IEnumerable<AwardMegaball> GetListAward(int start, int end, DateTime fromDate, DateTime toDate)
        {
            return Proxy.GetListAward(start, end, fromDate, toDate);
        }
        public IEnumerable<AwardMegaball> GetListAwardOrderByDate()
        {
            return Proxy.GetListAwardOrderByDate();
        }

        public IEnumerable<AwardMegaball> GetListAwardByDay(DateTime fromDate, DateTime toDate)
        {
            return Proxy.GetListAwardByDay(fromDate, toDate);
        }

        /// <summary>
        
        /// Created date: 15.01.2018
        /// Description: Load thông tin xổ số gần nhất
        /// </summary>
        /// <returns></returns>
        public AwardMegaball GetLotteryResult(int intMemberID, ref double dblPoint)
        {
            return Proxy.GetLotteryResult(intMemberID, ref dblPoint);
        }
        public IEnumerable<AwardMegaball> GetAllListAward()
        {
            return Proxy.GetAllListAward();

        }
        public IEnumerable<AwardMegaball> GetListAwardByDate(DateTime Date)
        {
            return Proxy.GetListAwardByDate(Date);
        }

        public IEnumerable<AwardMegaball> GetListAwardMegaball(int start, int end)
        {
            try
            {
                return Proxy.GetListAwardMegaball(start, end);
            }
            catch(Exception ex)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("AwardMegaballRepository -> GetListAwardMegaball : " + ex.Message);
                return null;
            }
        }

        public AwardMegaball GetAwardMegaballByID(int intNumberID)
        {
            return Proxy.GetAwardMegaballByID(intNumberID);
        }
        public AwardMegaball GetAwardMegabalByDrawID(int intDrawID)
        {
            return Proxy.GetAwardMegabalByDrawID(intDrawID);
        }
    }
}
