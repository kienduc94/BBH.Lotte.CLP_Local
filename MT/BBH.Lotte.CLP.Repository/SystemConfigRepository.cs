﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Repository
{
    public class SystemConfigRepository : WCFClient<ISystemConfigServices>, ISystemConfigServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public ObjResultMessage UpdateStatusActive(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.UpdateStatusActive(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "SystemConfigRepository -> InsertAwardMegaball -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "SystemConfigRepository -> InsertAwardMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public IEnumerable<SystemConfigBO> GetListSystemConfig()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<SystemConfigBO> lstSystemConfig = Proxy.GetListSystemConfig();
                return lstSystemConfig;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
    }
}
