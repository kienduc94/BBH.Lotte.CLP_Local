﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Repository
{
    public class CountryRepository : WCFClient<ICountryServices>, ICountryServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public ObjResultMessage InsertCountry(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.InsertCountry(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "CountryRepository -> InsertCountry -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "CountryRepository -> InsertCountry : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public IEnumerable<CountryBO> GetListCountry(int start, int end)
        {

            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<CountryBO> lstCountry = Proxy.GetListCountry(start, end);
                return lstCountry;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }

        }

        public ObjResultMessage UpdateCountry(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.UpdateCountry(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "CountryRepository -> UpdateCountry -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "CountryRepository -> UpdateCountry : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public ObjResultMessage LockAndUnlockCountry(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.LockAndUnlockCountry(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "CountryRepository -> LockAndUnlockCountry -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "CountryRepository -> LockAndUnlockCountry : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public bool CheckCountryNameExists(int countryID, string countryName)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.CheckCountryNameExists(countryID,countryName);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool CheckPhoneZipCodeExists(int countryID, string phoneZipCode)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.CheckPhoneZipCodeExists(countryID, phoneZipCode);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }
        public IEnumerable<CountryBO> GetListCountryBySearch(string keyword, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<CountryBO> lstCountry = Proxy.GetListCountryBySearch(keyword, start, end);

                return lstCountry;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

    }
}
