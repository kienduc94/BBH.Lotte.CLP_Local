﻿using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Repository
{
    public class ExchangePointRepository : WCFClient<IExchangePointServices>, IExchangePointServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public IEnumerable<ExchangePointBO> ListAllExchangePoint()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<ExchangePointBO> lstExchangePoint = Proxy.ListAllExchangePoint();
                return lstExchangePoint;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<ExchangePointBO> ListAllExchangePointPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<ExchangePointBO> lstExchangePoint = Proxy.ListAllExchangePointPaging(start, end);
                return lstExchangePoint;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<ExchangePointBO> ListExchangePointBySearch(DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<ExchangePointBO> lstExchangePoint = Proxy.ListExchangePointBySearch(fromDate, toDate, start, end);
                return lstExchangePoint;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public bool LockAndUnlockExchangePoint(int exchangeID, int isActive)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            bool kq = false;
            try
            {
                kq = Proxy.LockAndUnlockExchangePoint(exchangeID, isActive);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateStatusExchangePoint(int exchangeID, int status, DateTime deleteDate, string deleteUser)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            bool kq = false;
            try
            {
                kq = Proxy.UpdateStatusExchangePoint(exchangeID, status, deleteDate, deleteUser);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }
        public bool InsertExchangePoint(ExchangePointBO exchangePoint)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            bool kq = false;
            try
            {
                kq = Proxy.InsertExchangePoint(exchangePoint);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateExchangePoint(ExchangePointBO exchangePoint)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            bool kq = false;
            try
            {
                kq = Proxy.UpdateExchangePoint(exchangePoint);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }
    }
}
