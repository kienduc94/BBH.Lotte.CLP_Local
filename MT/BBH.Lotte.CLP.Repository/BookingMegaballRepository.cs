﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Repository
{
    public class BookingMegaballRepository : WCFClient<IBookingMegaballServices>, IBookingMegaballServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public IEnumerable<BookingMegaballBO> ListAllBookingMegaball()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingMegaball();
                return lstBooking;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingPage(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingPage(start, end);
                return lstBooking;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int status, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingBySearch(memberID, fromDate, toDate, status, start, end);
                return lstBooking;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
        public IEnumerable<BookingMegaballBO> GetListMemberBySearch(string keyword, DateTime fromDate, DateTime toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.GetListMemberBySearch(keyword, fromDate, toDate, start, end);
                return lstBooking;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public bool UpdateStatusBookingMegaball(int bookingID, int status)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateStatusBookingMegaball(bookingID, status);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public ObjResultMessage CountAllBookingMegaball(ModelRSA objRsa, ref BookingMegaballBO objBooking)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.CountAllBookingMegaball(objRsa, ref objBooking);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "BookingMegaballRepository -> CountAllBookingMegaball -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "BookingMegaballRepository -> CountAllBookingMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public System.Threading.Tasks.Task<BookingMegaballBO[]> ListAllBookingMegaballAsync()
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task<BookingMegaballBO[]> ListAllBookingPageAsync(int start, int end)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task<BookingMegaballBO[]> ListAllBookingBySearchAsync(int memberID, DateTime fromDate, DateTime toDate, int status, int start, int end)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task<bool> UpdateStatusBookingMegaballAsync(int bookingID, int status)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingMegaballByDate(DateTime dtDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingMegaballByDate(dtDate);
                return lstBooking;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingMegaballByDraw(int intDrawID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingMegaballByDraw(intDrawID);
                return lstBooking;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public decimal GetAwardNotJackpot(string strNumberCheck, string strExtraNumber, int intDrawID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            decimal value = 0;
            try
            {
                value = Proxy.GetAwardNotJackpot(strNumberCheck, strExtraNumber, intDrawID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                value = 0;
            }
            return value;
        }
    }
}
