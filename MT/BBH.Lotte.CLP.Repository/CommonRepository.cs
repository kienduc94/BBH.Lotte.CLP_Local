﻿using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Repository
{
    public class CommonRepository : WCFClient<ICommonServices>, ICommonServices
    {
        public ObjResultMessage CheckAuthenticate(ModelRSA objRsa)
        {
            //if (!Utility.CheckToken(objRsa.UserName, objRsa.Token))
            //{
                ObjResultMessage objResult = Proxy.CheckAuthenticate(objRsa);
                if (!objResult.IsError)
                {
                    string strToken = Algorithm.EncryptTextSHA(DateTime.Now.Ticks.ToString());

                    System.Web.HttpContext.Current.Session[InstanceKeys.TokenSession + objRsa.UserName] = strToken;

                    objResult.ObjResultData = strToken;
                }
           // }

            return objResult;
        }
    }
}
