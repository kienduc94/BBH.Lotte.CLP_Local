﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BBH.Lotte.CLP.Repository;
using System.Configuration;
using System.IO;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain;

namespace BBH.Lotte.CLP.Repository
{
    public class GroupAdminRepository : WCFClient<IGroupAdminServices>, IGroupAdminServices
    {

        //GroupAdminServicesSvc.GroupAdminSvcClient service = new GroupAdminServicesSvc.GroupAdminSvcClient();
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public bool InsertGroupAdmin(GroupAdminBO admin)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.InsertGroupAdmin(admin);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public IEnumerable<GroupAdminBO> ListGroupAdmin()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<GroupAdminBO> lstGroupAdmin = Proxy.ListGroupAdmin();
                return lstGroupAdmin;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public GroupAdminBO GetGroupAdminDetail(int groupID)
        {
            GroupAdminBO services = null;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                services = Proxy.GetGroupAdminDetail(groupID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return services;
        }

        public bool UpdateStatusGroupAdmin(int groupID, int isActive)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateStatusGroupAdmin(groupID, isActive);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateGroupAdmin(int groupID, string groupName)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateGroupAdmin(groupID, groupName);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

       public bool CheckUserGroupAdminNameExists(string groupName)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.CheckUserGroupAdminNameExists(groupName);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }
    }
}
