﻿using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Repository
{
    public class AdminRepository : WCFClient<IAdminServices>, IAdminServices
    {
        //AdminServicesSvc.AdminSvcClient service = new AdminServicesSvc.AdminSvcClient();
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public AdminBO LoginManagerAccount(string username, string password)
        {
            AdminBO admin = null;
            string fileLog=Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                admin = Proxy.LoginManagerAccount(username, password);
            }
            catch(Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return admin;
        }

        public IEnumerable<AdminBO> ListAllAdmin()
        {
            
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog,"Logs"));
            try
            {
                IEnumerable<AdminBO> lstAdmin = Proxy.ListAllAdmin();
                return lstAdmin;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
           
        }

        public AdminBO GerAdminDetailByUserName(string username)
        {
            AdminBO admin = null;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                admin = Proxy.GerAdminDetailByUserName(username);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return admin;
        }

        public int InsertAdmin(AdminBO admin)
        {
            int row = 0;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                row = Proxy.InsertAdmin(admin);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return row;
        }

        public AdminBO GerAdminDetailByID(int adminID)
        {
            AdminBO admin = null;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                admin = Proxy.GerAdminDetailByID(adminID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return admin;
        }

        public int GetManagerIDNewest()
        {
            int row=0;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                row = Proxy.GetManagerIDNewest();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return row;
        }

        public bool UpdateStatusAdmin(int adminID, int visible)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateStatusAdmin(adminID, visible);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateUserNameAndPasswordAdmin(int adminID, string userName, string password)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateUserNameAndPasswordAdmin(adminID, userName, password);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateUserNamedAdmin(int adminID, string userName)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateUserNamedAdmin(adminID, userName);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateAdmin(int adminID, AdminBO admin)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateAdmin(adminID, admin);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool InsertAccessRight(AccessRightBO access)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.InsertAccessRight(access);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool DeleteAccessRight(int adminID)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.DeleteAccessRight(adminID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public IEnumerable<AccessRightBO> ListAccessRightByManagerID(int adminID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<AccessRightBO> lstAccessRight = Proxy.ListAccessRightByManagerID(adminID);
                return lstAccessRight;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<GroupAdminBO> ListGroupAdmin()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<GroupAdminBO> lstGroupAdmin = Proxy.ListGroupAdmin();
                return lstGroupAdmin;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
           
        }

        public IEnumerable<AdminBO> ListAllAdminPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<AdminBO> lstAdmin = Proxy.ListAllAdminPaging(start,end);
                return lstAdmin;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public GroupAdminBO GetGroupAdminDetail(int groupID)
        {
            GroupAdminBO admin = null;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                admin = Proxy.GetGroupAdminDetail(groupID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return admin;
        }

        public bool InsertAdminfomation(AdminInfomationBO member)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.InsertAdminfomation(member);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateAdminInfomation(AdminInfomationBO member, int adminID)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateAdminInfomation(member, adminID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public  bool CheckEmailExists(string email)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {

                kq = Proxy.CheckEmailExists(email);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public  bool CheckUserNameExists(string userName)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.CheckUserNameExists(userName);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }
    }
}
