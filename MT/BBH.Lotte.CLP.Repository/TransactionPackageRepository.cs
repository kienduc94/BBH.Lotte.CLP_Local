﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BBH.Lotte.CLP.Repository;
using System.Configuration;
using System.IO;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain;

namespace BBH.Lotte.CLP.Repository
{
    public class TransactionPackageRepository : WCFClient<ITransactionPackageServices>, ITransactionPackageServices
    {
        //TransactionPackageServicesSvc.TransactionPackageSvcClient service = new TransactionPackageServicesSvc.TransactionPackageSvcClient();
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public IEnumerable<TransactionPackageBO> ListAllTransactionPackage(int start, int end)
        {

            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            try
            {
                IEnumerable<TransactionPackageBO> lstTransactionPackage = Proxy.ListAllTransactionPackage(start, end);
                return lstTransactionPackage;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }

        }
        public IEnumerable<TransactionPackageBO> ListAllTransactionPackageByMember(int memberID, DateTime fromDate, DateTime toDate, int status, int start, int end)
        {

            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            try
            {
                IEnumerable<TransactionPackageBO> lstTransactionPackage = Proxy.ListAllTransactionPackageByMember(memberID, fromDate, toDate, status, start, end);
                return lstTransactionPackage;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }

        }

        public bool UpdateStatusTransaction(int transactionID, int status)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateStatusTransaction(transactionID, status);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public PackageBO GetPackageDetail(int packageID)
        {
            PackageBO Package = null;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                Package = Proxy.GetPackageDetail(packageID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return Package;
        }

        public TransactionPackageBO CountAllPackage()
        {
            TransactionPackageBO Package = null;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                Package = Proxy.CountAllPackage();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return Package;
        }
    }
}
