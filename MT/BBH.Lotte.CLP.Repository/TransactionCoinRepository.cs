﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Repository
{
    public class TransactionCoinRepository : WCFClient<ITransactionCoinServices>, ITransactionCoinServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public IEnumerable<TransactionCoinBO> ListTransactionCoin()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<TransactionCoinBO> lstTransactionCoin = Proxy.ListTransactionCoin();
                return lstTransactionCoin;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<TransactionCoinBO> ListTransactionCoinPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<TransactionCoinBO> lstTransactionCoin = Proxy.ListTransactionCoinPaging(start, end);
                return lstTransactionCoin;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<TransactionCoinBO> ListTransactionCoinBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int status, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<TransactionCoinBO> lstTransactionCoin = Proxy.ListTransactionCoinBySearch(memberID, fromDate, toDate, status, start, end);
                return lstTransactionCoin;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public bool UpdateStatusTransaction(string transactionID, int status)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateStatusTransaction(transactionID, status);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public ObjResultMessage CountAllCoin(ModelRSA objRsa, ref TransactionCoinBO objTransaction)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.CountAllCoin(objRsa, ref objTransaction);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "TransactionCoinRepository -> CountAllCoin -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "TransactionCoinRepository -> CountAllCoin : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }
    }
}
