﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Repository;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Repository
{
    public class AwardRepository : WCFClient<IAwardServices>, IAwardServices
    {
        //AwardServicesSvc.AwardSvcClient service = new AwardServicesSvc.AwardSvcClient();
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public ObjResultMessage InsertAward(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.InsertAward(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardRepository -> InsertAward -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardRepository -> InsertAward : "+ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public ObjResultMessage UpdateAward(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.UpdateAward(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardRepository -> UpdateAward -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardRepository -> UpdateAward : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public ObjResultMessage LockAndUnlockAward(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.LockAndUnlockAward(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardRepository -> LockAndUnlockAward -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardRepository -> LockAndUnlockAward : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public  bool CheckAwardNameEnglishExists(int awardID, string awardNameEnglish)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.CheckAwardNameEnglishExists(awardID, awardNameEnglish);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public IEnumerable<AwardBO> GetListAward(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<AwardBO> lstAward = Proxy.GetListAward(start, end);
                return lstAward;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<AwardNumberBO> GetListAwardNumber(int start, int end)
        {
            
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<AwardNumberBO> lstAwardNum = Proxy.GetListAwardNumber(start, end);
                return lstAwardNum;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
            
        }

        public bool InsertAwardNumber(AwardNumberBO award)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.InsertAwardNumber(award);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateAwardNumber(AwardNumberBO award)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateAwardNumber(award);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public IEnumerable<AwardNumberBO> GetListAwardNumberByDate(int start, int end, DateTime fromDate, DateTime toDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<AwardNumberBO> lstAwardNumberBO = Proxy.GetListAwardNumberByDate(start, end, fromDate, toDate);
                return lstAwardNumberBO;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
            
        }

        public ObjResultMessage LockAndUnlockAwardNumber(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
               
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.LockAndUnlockAwardNumber(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardRepository -> LockAndUnlockAwardNumber -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardRepository -> LockAndUnlockAwardNumber : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public IEnumerable<WinnerBO> GetListWinner(int start, int end, DateTime fromDate, DateTime toDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<WinnerBO> lstWinner = Proxy.GetListWinner(start, end, fromDate, toDate);
                return lstWinner;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
       public AwardBO GetListAwardDetail(int awardID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                AwardBO lstWinner = Proxy.GetListAwardDetail(awardID);
                return lstWinner;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
    }
}
