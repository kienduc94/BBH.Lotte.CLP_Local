﻿using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Repository
{
    public class SummaryRepository: WCFClient<IBookingSummaryServices>, IBookingSummaryServices
    {
        public IEnumerable<BookingSummaryBO> ListTicketSummaryByPoint(int year, int isFreeTicket)
        {
            return Proxy.ListTicketSummaryByPoint(year, isFreeTicket);
        }

        public IEnumerable<BookingSummaryBO> ListTicketSummaryByFree(int year, int isFreeTicket)
        {
            return Proxy.ListTicketSummaryByFree(year, isFreeTicket);
        }

        public IEnumerable<PrizeBO> ListPrize()
        {
            return Proxy.ListPrize();
        }
    }
}
