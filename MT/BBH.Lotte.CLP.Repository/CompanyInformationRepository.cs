﻿using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Repository
{

    public class CompanyInformationRepository : WCFClient<ICompanyInformationServices>, ICompanyInformationServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public IEnumerable<CompanyInformationBO> ListAllCompanyPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<CompanyInformationBO> lstCompany = Proxy.ListAllCompanyPaging(start, end);

                return lstCompany;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<CompanyInformationBO> GetListCompanyBySearch(string keyword)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<CompanyInformationBO> lstCompany = Proxy.GetListCompanyBySearch(keyword);

                return lstCompany;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
        public IEnumerable<CompanyInformationBO> GetCompanyByMemberID(int memberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<CompanyInformationBO> company = Proxy.GetCompanyByMemberID(memberID);
                return company;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
        public IEnumerable<MemberBO> GetMemberByMemberID(int memberID)
        {
             string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<MemberBO> company = Proxy.GetMemberByMemberID(memberID);
                return company;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
    }
}
