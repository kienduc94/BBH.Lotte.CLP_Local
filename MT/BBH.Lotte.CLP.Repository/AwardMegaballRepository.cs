﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using BBH.Lotte.CLP.Wallet.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace BBH.Lotte.CLP.Repository
{
    public class AwardMegaballRepository : WCFClient<IAwardMegaballServices>, IAwardMegaballServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public ObjResultMessage InsertAwardMegaball(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.InsertAwardMegaball(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardMegaballRepository -> InsertAwardMegaball -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardMegaballRepository -> InsertAwardMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public ObjResultMessage UpdateAwardMegaball(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.UpdateAwardMegaball(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardMegaballRepository -> UpdateAwardMegaball -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardMegaballRepository -> UpdateAwardMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public ObjResultMessage LockAndUnlockAwardMegaball(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.LockAndUnlockAwardMegaball(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardMegaballRepository -> LockAndUnlockAwardMegaball -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardMegaballRepository -> LockAndUnlockAwardMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public IEnumerable<AwardMegaballBO> GetListAwardMegaball(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<AwardMegaballBO> lstAwardMegaball = Proxy.GetListAwardMegaball(start, end);
                return lstAwardMegaball;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
        public IEnumerable<AwardMegaballBO> GetListAwardNumberByDate(int start, int end, DateTime fromDate, DateTime toDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<AwardMegaballBO> lstAwardMegaball = Proxy.GetListAwardNumberByDate(start, end, fromDate, toDate);
                return lstAwardMegaball;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<AwardMegaballBO> ListAllAwardMegaballBySearch(DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<AwardMegaballBO> lstAwardMegaball = Proxy.ListAllAwardMegaballBySearch(fromDate, toDate, start, end);
                return lstAwardMegaball;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public ObjResultMessage CountAllAwardMegaball(ModelRSA objRsa, ref AwardMegaballBO objAwardMegaball)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {

                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.CountAllAwardMegaball(objRsa, ref objAwardMegaball);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardMegaballRepository -> CountAllAwardMegaball -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }

            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardMegaballRepository -> CountAllAwardMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);

            }
            return objResult;
        }

        public List<WinnerBO> GetListWinner(int intNumberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                List<WinnerBO> lstWinner = Proxy.GetListWinner(intNumberID);
                return lstWinner;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public bool CheckCreateDateExists(DateTime createDate)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.CheckCreateDateExists(createDate);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public AwardMegaballBO GetAwardMegabalByDrawID(int intDrawID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                AwardMegaballBO objAwardMegaballBO = Proxy.GetAwardMegabalByDrawID(intDrawID);
                return objAwardMegaballBO;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public ObjResultMessage UpdateJackpotAwardMegaball(decimal decJackpot, int intDrawID, decimal totalwon)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                objResult = Proxy.UpdateJackpotAwardMegaball(decJackpot, intDrawID, totalwon);
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardMegaballRepository -> UpdateJackpotAwardMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        public ObjResultMessage InsertWinningNumber(TicketWinningBO objTicket, DateTime dtmCreatedDate, ref int intWinnerJackPotCount)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));

            ObjResultMessage objResult = new ObjResultMessage(true, MessageResult.Error, "InsertWinningNumber: ''", DateTime.Now);
            try
            {
                //get address btc
                //update Jackpot in table AwardMegaball
                objTicket.JackpotValue = GetJackPotValue(objTicket);
                
                objResult = Proxy.InsertWinningNumber(objTicket, dtmCreatedDate, ref intWinnerJackPotCount);

                if (objResult != null && (!objResult.IsError || (objResult.IsError && string.IsNullOrEmpty(objResult.MessageDetail))))
                {
                    //BBC.CWallet.Share.BBCLoggerManager.Debug("InsertWinningNumber: " + objResult.ToJsonString());

                    //transfer from sysJackPot to sysJackPotWinning
                    TransactionInfo objTransactionInfo = null;
                    if (objTicket.JackpotValue > 0 && intWinnerJackPotCount > 0)
                    {
                        //have to be at least 1 winner jackpot (intWinnerJackPotCount > 0) and JackPot value > 0 
                        //transfer from sysJackPot to sysJackPotWinning
                        TransferJackpotToAward(objTicket);
                    }

                    BBC.CWallet.Share.BBCLoggerManager.Debug("objTransactionInfo: " + objTransactionInfo.ToJsonString());
                    BBC.CWallet.Share.BBCLoggerManager.Debug("dcJackPot: " + objTicket.JackpotValue + ", intWinnerJackPotCount: " + intWinnerJackPotCount);
                }
            }
            catch (Exception ex)
            {
                objResult.MessageDetail = "AwardMegaballRepository -> InsertWinningNumber: " + ex.Message;

                Utility.WriteLog(fileLog, ex.Message);
            }
            return objResult;
        }

        private decimal GetJackPotValue(TicketWinningBO objTicket)
        {
            decimal dcJackPot = 0;
            try
            {
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = objTicket.SysJackpot_UserID;
                objAddressInfoRequest.ClientID = objTicket.ClientID;
                objAddressInfoRequest.SystemID = objTicket.SystemID;
                objAddressInfoRequest.IsCreateAddr = false;
                objAddressInfoRequest.CreatedBy = RDUserAdmin.UserName;
                objAddressInfoRequest.WalletId = objTicket.WalletID;

                //get sysJackPot address available
                AddressInfo objAddressInfo = new AddressRepository().Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.UserId == objAddressInfoRequest.UserID)
                {
                    dcJackPot = objAddressInfo.Available;
                }
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("GetJackPotValue: " + objEx);
            }
            return dcJackPot;
        }

        private void TransferJackpotToAward(TicketWinningBO objTicket)
        {
            try
            {
                TransferRequest objTransferRequest = new TransferRequest();
                objTransferRequest.UserId = objTicket.SysJackpot_UserID;
                objTransferRequest.FromAddress = objTicket.SysJackpot_Address;
                objTransferRequest.ClientID = objTicket.ClientID;
                objTransferRequest.SystemID = objTicket.SystemID;
                objTransferRequest.Amount = objTicket.JackpotValue;
                objTransferRequest.Description = "Transfer From sysJackPot To sysAward_UserID";
                objTransferRequest.ToClientId = objTicket.ClientID;
                objTransferRequest.ToSystemId = objTicket.SystemID;
                objTransferRequest.ToUserId = objTicket.SysAward_UserID;
                objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                objTransferRequest.CreatedBy = RDUserAdmin.UserName;

                TransactionRepository objTransactionRepository = new TransactionRepository();

                var obj = objTransactionRepository.Transfer(objTransferRequest);

                if(obj != null)
                {
                    TransferJackpotDefault(objTicket.SysJackpot_UserID, objTicket.SysLoan_UserID, objTicket.SysLoan_Address, objTicket.SysGuaranteeFund_UserID, objTicket.SysGuaranteeFund_Address, objTicket.ClientID, objTicket.SystemID, objTicket.WalletID);
                    //get address btc
                    //update Jackpot in table AwardMegaball
                    objTicket.JackpotValue = GetJackPotValue(objTicket);

                    Proxy.UpdateAwardValue(objTicket.JackpotValue, objTicket.DrawID);
                }
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("TransferJackpotToWinning: " + objEx);
            }
        }

        public int UpdateAwardValue(decimal dcJackPotValue, int intDrawID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            int intQuery = 0;
            try
            {
                intQuery = Proxy.UpdateAwardValue(dcJackPotValue, intDrawID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return intQuery;
        }
        private void TransferJackpotDefault(string SysJackpot_UserID, string SysLoan_UserID, string SysLoan_Address, string sysGuaranteeFund_UserID, string sysGuaranteeFund_Address, short clientID, short systemID, int walletID)
        {
            try
            {
                decimal jackPotDefault = decimal.Parse(ConfigurationManager.AppSettings[KeyManager.JackPotDefault]);
                decimal amountSysGuaranteeFund = 0;
                decimal amouuntPlus = 0;
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = sysGuaranteeFund_UserID;
                objAddressInfoRequest.ClientID = clientID;
                objAddressInfoRequest.SystemID = systemID;
                objAddressInfoRequest.IsCreateAddr = false;
                objAddressInfoRequest.CreatedBy = RDUserAdmin.UserName;
                objAddressInfoRequest.WalletId = walletID;

                //get sysJackPot address available
                AddressInfo objAddressInfo = new AddressRepository().Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.UserId == objAddressInfoRequest.UserID)
                {
                    amountSysGuaranteeFund = objAddressInfo.Available;
                }

                if (amountSysGuaranteeFund > 0)
                {
                    if(amountSysGuaranteeFund < jackPotDefault)
                    {
                        amouuntPlus = jackPotDefault - amountSysGuaranteeFund;

                        //tranfer fund to sysjackpot (amountSysGuaranteeFund)
                        TransferRequest objTransferRequest = new TransferRequest();
                        objTransferRequest.UserId = sysGuaranteeFund_UserID;
                        objTransferRequest.FromAddress = sysGuaranteeFund_Address;
                        objTransferRequest.ClientID = clientID;
                        objTransferRequest.SystemID = systemID;
                        objTransferRequest.Amount = amountSysGuaranteeFund;
                        objTransferRequest.Description = "TransferJackpotDefault(sysGuaranteeFund)";
                        objTransferRequest.ToClientId = clientID;
                        objTransferRequest.ToSystemId = systemID;
                        objTransferRequest.ToUserId = SysJackpot_UserID;
                        objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                        objTransferRequest.CreatedBy = RDUserAdmin.UserName;
                        
                        var obj = new TransactionRepository().Transfer(objTransferRequest);

                        //tranfer loan to sysjackpot (amouuntPlus)
                        objTransferRequest = new TransferRequest();
                        objTransferRequest.UserId = SysLoan_UserID;
                        objTransferRequest.FromAddress = SysLoan_Address;
                        objTransferRequest.ClientID = clientID;
                        objTransferRequest.SystemID = systemID;
                        objTransferRequest.Amount = amouuntPlus;
                        objTransferRequest.Description = "TransferJackpotDefault(SysLoan)";
                        objTransferRequest.ToClientId = clientID;
                        objTransferRequest.ToSystemId = systemID;
                        objTransferRequest.ToUserId = SysJackpot_UserID;
                        objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                        objTransferRequest.CreatedBy = RDUserAdmin.UserName;
                        
                        var obj2 = new TransactionRepository().Transfer(objTransferRequest);
                    }
                    else
                    {
                        //tranfer fund to sysjackpot (jackPotDefault)
                        TransferRequest objTransferRequest = new TransferRequest();
                        objTransferRequest.UserId = sysGuaranteeFund_UserID;
                        objTransferRequest.FromAddress = sysGuaranteeFund_Address;
                        objTransferRequest.ClientID = clientID;
                        objTransferRequest.SystemID = systemID;
                        objTransferRequest.Amount = jackPotDefault;
                        objTransferRequest.Description = "TransferJackpotDefault(sysGuaranteeFund)";
                        objTransferRequest.ToClientId = clientID;
                        objTransferRequest.ToSystemId = systemID;
                        objTransferRequest.ToUserId = SysJackpot_UserID;
                        objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                        objTransferRequest.CreatedBy = RDUserAdmin.UserName;

                        TransactionRepository objTransactionRepository = new TransactionRepository();

                        var obj = objTransactionRepository.Transfer(objTransferRequest);
                    }
                }
                else
                {
                    //tranfer loan to sysjackpot (jackPotDefault)
                    TransferRequest objTransferRequest = new TransferRequest();
                    objTransferRequest.UserId = SysLoan_UserID;
                    objTransferRequest.FromAddress = SysLoan_Address;
                    objTransferRequest.ClientID = clientID;
                    objTransferRequest.SystemID = systemID;
                    objTransferRequest.Amount = jackPotDefault;
                    objTransferRequest.Description = "Transfer sysLoan -> JackpotDefault(SysLoan)";
                    objTransferRequest.ToClientId = clientID;
                    objTransferRequest.ToSystemId = systemID;
                    objTransferRequest.ToUserId = SysJackpot_UserID;
                    objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                    objTransferRequest.CreatedBy = RDUserAdmin.UserName;

                    var obj2 = new TransactionRepository().Transfer(objTransferRequest);
                }
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("TransferJackpotToWinning: " + objEx);
            }
        }

        public int CheckJackPotWinnerByDrawID(TicketWinningBO objTicket)
        {
            try
            {
                return Proxy.CheckJackPotWinnerByDrawID(objTicket);
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Error("TransferJackpotToWinning: " + objEx);
                return 0;
            }
        }
    }
}
