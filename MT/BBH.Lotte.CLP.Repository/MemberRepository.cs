﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Repository;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Repository
{
    public class MemberRepository : WCFClient<IMemberServices>, IMemberServices
    {
         
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];     

        public bool UpdatePasswordMember(string userName, string password)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdatePasswordMember(userName, password);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateMemberInfomation(MemberInfomationBO member, int memberID)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateMemberInfomation(member, memberID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool InsertMemberInfomation(MemberInfomationBO member)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.InsertMemberInfomation(member);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdatePointsMember(int memberID, int points)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdatePointsMember(memberID, points);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool LockAndUnlockMember(int memberID, int isActive)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.LockAndUnlockMember(memberID, isActive);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        //public MemberInfomationBO GetMemberDetailByID(int memberID)
        //{
        //    MemberInfomationBO member = null;
        //    string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
        //    try
        //    {
        //        member = service.MemberInfomationBO(memberID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Utility.WriteLog(fileLog, ex.Message);
        //    }
        //    return member;
        //}

        //public MemberInfomationBO GetMemberDetail(string email)
        //{
        //    MemberInfomationBO member = null;
        //    string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
        //    try
        //    {
        //        member = service.MemberInfomationBO(memberID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Utility.WriteLog(fileLog, ex.Message);
        //    }
        //    return member;
        //}

        public bool CheckUserNameExists(string userName)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.CheckUserNameExists(userName);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool CheckEmailExists(string email)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.CheckEmailExists(email);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public IEnumerable<MemberBO> GetListMember(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<MemberBO> lstMember = Proxy.GetListMember(start, end);
              
                return lstMember;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
      
        public IEnumerable<MemberBO> ListAllMember()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<MemberBO> lstMember = Proxy.ListAllMember();

                return lstMember;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<MemberBO> GetListMemberBySearch(string keyword,DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<MemberBO> lstMember = Proxy.GetListMemberBySearch(keyword,fromDate,toDate,start,end);
                return lstMember;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public ObjResultMessage CountAllMember(ModelRSA objRsa, ref MemberBO objMember)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.CountAllMember(objRsa, ref objMember);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "MemberRepository -> CountAllMember -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "MemberRepository -> CountAllMember : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(fileLog, ex.Message);
                
            }
            return objResult;
        }

        public IEnumerable<CompanyInformationBO> GetCompanyByMemberID(int memberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<CompanyInformationBO> company = Proxy.GetCompanyByMemberID(memberID);
                return company;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public int CountMemberBySearch(string keyword)
        {
            int count = 0;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                count = Proxy.CountMemberBySearch(keyword);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return count;
        }

        public MemberInfomationBO GetMemberDetailByID(int memberID)
        {
            MemberInfomationBO member = null;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                member = Proxy.GetMemberDetailByID(memberID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
            return member;
        }

        public MemberInfomationBO GetMemberDetail(string email)
        {
            MemberInfomationBO member = null;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                member = Proxy.GetMemberDetail(email);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
            return member;
        }
        public IEnumerable<MemberBO> GetMemberByID(int memberID)
        {           
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<MemberBO> member = Proxy.GetMemberByID(memberID);
                return member;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
    }
}
