﻿using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Repository
{
    class FreeTicketRepository : WCFClient<IFreeTicketServices>, IFreeTicketServices
    {
        public bool InsertFreeTicket(int intNumberTicket)
        {
            try
            {
                return Proxy.InsertFreeTicket(intNumberTicket);
            }
            catch
            {
                return false;
            }
        }
    }
}
