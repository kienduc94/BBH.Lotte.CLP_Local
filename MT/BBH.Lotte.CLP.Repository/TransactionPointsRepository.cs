﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BBH.Lotte.CLP.Repository;
using System.Configuration;
using System.IO;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain;

namespace BBH.Lotte.CLP.Repository
{

    public class TransactionPointsRepository : WCFClient<ITransactionPointsServices>, ITransactionPointsServices
    {
        //TransactionPointsServicesSvc.TransactionPointsSvcClient service = new TransactionPointsServicesSvc.TransactionPointsSvcClient();
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public IEnumerable<TransactionPointsBO> ListTransactionPoint()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<TransactionPointsBO> lstTransactionPoints = Proxy.ListTransactionPoint();
                return lstTransactionPoints;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<TransactionPointsBO> ListTransactionPointPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<TransactionPointsBO> lstTransactionPoints = Proxy.ListTransactionPointPaging(start, end);
                return lstTransactionPoints;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<TransactionPointsBO> ListTransactionPointBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int status, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<TransactionPointsBO> lstTransactionPoints = Proxy.ListTransactionPointBySearch(memberID, fromDate, toDate, status, start, end);
                return lstTransactionPoints;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public bool UpdateStatusTransaction(int transactionID, int status)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateStatusTransaction(transactionID, status);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public TransactionPointsBO CountAllPoints()
        {
            TransactionPointsBO services = null;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                services = Proxy.CountAllPoints();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return services;
        }

    }
}
