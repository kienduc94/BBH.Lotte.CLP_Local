﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BBH.Lotte.CLP.Repository;
using System.Configuration;
using System.IO;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Domain;

namespace BBH.Lotte.CLP.Repository
{
    public class NumberGenerralRepository : WCFClient<INumberGenerralServices>, INumberGenerralServices
    {
        //NumberGenerralServicesSvc.NumberGenerralSvcClient service = new NumberGenerralServicesSvc.NumberGenerralSvcClient();
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public bool InsertNumberGeneral(NumberGeneralBO number)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.InsertNumberGeneral(number);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateNumberGeneral(string numberValue, int quantity, int numberID)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateNumberGeneral(numberValue, quantity, numberID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateQuantityNumberGeneral(string numberValue, int quantity)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.UpdateQuantityNumberGeneral(numberValue, quantity);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool LockAndUnlockNumberGeneral(int numberID, int isActive)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.LockAndUnlockNumberGeneral(numberID, isActive);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool CheckNumberExists(string numberValue)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.CheckNumberExists(numberValue);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }
        public NumberGeneralBO GetNumberGeneralDetail(int numberID)
        {
            NumberGeneralBO services = null;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            try
            {
                services = Proxy.GetNumberGeneralDetail(numberID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return services;
        }

        public IEnumerable<NumberGeneralBO> GetListNumberGeneralPaging(int start, int end)
        {

            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            try
            {
                IEnumerable<NumberGeneralBO> lstNumberGeneral = Proxy.GetListNumberGeneralPaging(start, end);
                return lstNumberGeneral;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }

        }

        public IEnumerable<NumberGeneralBO> GetListNumberGeneral(int start, int end, DateTime fromDate, DateTime toDate)
        {

            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            try
            {
                IEnumerable<NumberGeneralBO> lstNumberGeneral = Proxy.GetListNumberGeneral(start, end, fromDate, toDate);
                return lstNumberGeneral;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }

        }
    }
}
