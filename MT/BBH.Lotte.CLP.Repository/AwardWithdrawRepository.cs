﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Repository
{
    public class AwardWithdrawRepository : WCFClient<IAwardWithdrawServices>, IAwardWithdrawServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public ObjResultMessage ListAwardWithdraw(ModelRSA objRsa, ref ModelRSA objRsaReturn)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.ListAwardWithdraw(objRsa, ref objRsaReturn);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardWithdrawRepository -> ListAwardWithdraw -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardWithdrawRepository -> ListAwardWithdraw : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(pathLog, ex.Message);
            }
            return objResult;

        }

        public ObjResultMessage GetAwardWithdrawDetail(ModelRSA objRsa, ref ModelRSA objRsaReturn)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.GetAwardWithdrawDetail(objRsa, ref objRsaReturn);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardWithdrawRepository -> GetAwardWithdrawDetail -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardWithdrawRepository -> GetAwardWithdrawDetail : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(pathLog, ex.Message);
            }
            return objResult;
        }

        public ObjResultMessage UpdateStatusAwardWithdraw(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.UpdateStatusAwardWithdraw(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "AwardWithdrawRepository -> UpdateStatusAwardWithdraw -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "AwardWithdrawRepository -> UpdateStatusAwardWithdraw : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(pathLog, ex.Message);
            }
            return objResult;
        }
    }
}
