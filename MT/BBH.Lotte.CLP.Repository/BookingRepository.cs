﻿using BBC.Core.WebService;
using BBH.Lotte.CLP.Domain;
using BBH.Lotte.CLP.Domain.Interfaces;
using BBH.Lotte.CLP.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace BBH.Lotte.CLP.Repository
{
    public class BookingRepository : WCFClient<IBookingServices>, IBookingServices
    {
       //BookingServicesSvc.BookingSvcClient service = new BookingServicesSvc.BookingSvcClient();
       static string pathLog = ConfigurationManager.AppSettings["PathLog"];

       public IEnumerable<BookingBO> ListAllBooking()
       {
           string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
           try
           {
               IEnumerable<BookingBO> lstBooking = Proxy.ListAllBooking();
               return lstBooking;
           }
           catch (Exception ex)
           {
               Utility.WriteLog(fileLog, ex.Message);
               return null;
           }
       }

       public IEnumerable<BookingBO> ListAllBookingPaging(int start, int end)
       {
           string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
           try
           {
               IEnumerable<BookingBO> lstBooking = Proxy.ListAllBookingPaging(start, end);
               return lstBooking;
           }
           catch (Exception ex)
           {
               Utility.WriteLog(fileLog, ex.Message);
               return null;
           }
       }

       public IEnumerable<BookingBO> ListAllBookingBySearch(int memberID, DateTime fromDate, DateTime toDate, int status, int start, int end)
       {
           string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
           try
           {
               IEnumerable<BookingBO> lstBooking = Proxy.ListAllBookingBySearch(memberID, fromDate, toDate, status, start, end);
               return lstBooking;
           }
           catch (Exception ex)
           {
               Utility.WriteLog(fileLog, ex.Message);
               return null;
           }
       }

       public bool UpdateStatusBooking(int bookingID, int status)
       {
           bool kq = false;
           string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
           try
           {
               kq = Proxy.UpdateStatusBooking(bookingID, status);
           }
           catch (Exception ex)
           {
               Utility.WriteLog(fileLog, ex.Message);
           }
           return kq;
       }

       public BookingBO CountAllBooking()
       {
           BookingBO booking = null;
           string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
           try
           {
               booking = Proxy.CountAllBooking();
           }
           catch (Exception ex)
           {
               Utility.WriteLog(fileLog, ex.Message);
           }
           return booking;
       }
    }
}
