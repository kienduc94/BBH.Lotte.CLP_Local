﻿using BBC.Core.Common.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace BBH.Lotte.CLP.Repository
{
    public class Utility
    {
        public static void WriteLog(string path, string message)
        {
            try
            {
                if (!File.Exists(path))
                {
                    using (StreamWriter w = File.CreateText(path))
                    {
                        Log(message, w);
                    }
                }
                else
                {
                    using (StreamWriter w = File.AppendText(path))
                    {
                        Log(message, w);
                    }
                }
            }
            catch { }
        }
        private static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\n" + DateTime.Now.ToString());
            w.WriteLine(" {0}", logMessage);
            w.WriteLine("-----------------------------------");
        }

        public static string EncodeString(string value)
        {
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(value);
            encodedBytes = md5.ComputeHash(originalBytes);
            return BitConverter.ToString(encodedBytes);
        }
        public static string MaHoaMD5(string pass)
        {
            try
            {

                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] data = Encoding.UTF8.GetBytes(pass);
                data = md5.ComputeHash(data);
                StringBuilder buider = new StringBuilder();
                foreach (byte b in data)
                {
                    buider.Append(b.ToString("x2"));
                }
                return buider.ToString();
            }
            catch
            {
                return pass;
            }
        }

        public static bool CheckToken(string userName, string token)
        {
            bool isResult = false;
            if(System.Web.HttpContext.Current.Session[InstanceKeys.TokenSession + userName]!=null&&(string)System.Web.HttpContext.Current.Session[InstanceKeys.TokenSession + userName]==token)
            {
                isResult = true;
            }
            return isResult;
        }
    }
}
