﻿using BBH.Lotte.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace BBH.Lotte.CLP.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AdminSvc" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AdminSvc.svc or AdminSvc.svc.cs at the Solution Explorer and start debugging.
    public class AdminSvc : IAdminSvc
    {
         public AdminBO LoginManagerAccount(string username, string password)
        {
            return BBH.Lotte.Data.AdminBusiness.LoginManagerAccount(username, password);
        }

        public IEnumerable<AdminBO> ListAllAdmin()
         {
             return BBH.Lotte.Data.AdminBusiness.ListAllAdmin();
         }

        public AdminBO GerAdminDetailByUserName(string username)
        {

            return BBH.Lotte.Data.AdminBusiness.GerAdminDetail(username);
        }


        public int InsertAdmin(AdminBO admin)
        {
            return BBH.Lotte.Data.AdminBusiness.InsertAdmin(admin);
        }

        public AdminBO GerAdminDetailByID(int adminID)
        {
            return BBH.Lotte.Data.AdminBusiness.GerAdminDetail(adminID);
        }

        public int GetManagerIDNewest()
        {
            return BBH.Lotte.Data.AdminBusiness.GetManagerIDNewest();
        }

        public bool UpdateStatusAdmin(int adminID, int visible)
        {
            return BBH.Lotte.Data.AdminBusiness.UpdateStatusAdmin(adminID, visible);
        }

        public bool UpdateUserNameAndPasswordAdmin(int adminID, string userName, string password)
        {
            return BBH.Lotte.Data.AdminBusiness.UpdateUserNameAndPasswordAdmin(adminID, userName, password);
        }

        public bool UpdateUserNamedAdmin(int adminID, string userName)
        {
            return BBH.Lotte.Data.AdminBusiness.UpdateUserNamedAdmin(adminID, userName);
        }

        public bool UpdateAdmin(int adminID, AdminBO admin)
        {
            return BBH.Lotte.Data.AdminBusiness.UpdateAdmin(adminID, admin);
        }

        public bool InsertAccessRight(AccessRightBO access)
        {
            return BBH.Lotte.Data.AdminBusiness.InsertAccessRight(access);
        }

        public bool DeleteAccessRight(int adminID)
        {
            return BBH.Lotte.Data.AdminBusiness.DeleteAccessRight(adminID);
        }

        public IEnumerable<AccessRightBO> ListAccessRightByManagerID(int adminID)
        {
            return BBH.Lotte.Data.AdminBusiness.ListAccessRightByManagerID(adminID);
        }

        public IEnumerable<GroupAdminBO> ListGroupAdmin()
        {
            return BBH.Lotte.Data.AdminBusiness.ListGroupAdmin();
        }

        public GroupAdminBO GetGroupAdminDetail(int groupID)
        {
            return BBH.Lotte.Data.AdminBusiness.GetGroupAdminDetail(groupID);
        }

        public bool InsertAdminfomation(AdminInfomationBO member)
        {
            return BBH.Lotte.Data.AdminBusiness.InsertAdminfomation(member);
        }

        public bool UpdateAdminInfomation(AdminInfomationBO member, int adminID)
        {
            return BBH.Lotte.Data.AdminBusiness.UpdateAdminInfomation(member, adminID);
        }


        public bool CheckUserNameExists(string userName)
        {
            return BBH.Lotte.Data.AdminBusiness.CheckUserNameExists(userName);
        }

        public bool CheckEmailExists(string email)
        {
            return BBH.Lotte.Data.AdminBusiness.CheckEmailExists(email);
        }
    }
}
