﻿using System;
using BBH.Lotte.CLP.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AwardMegaballSvc" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AwardMegaballSvc.svc or AwardMegaballSvc.svc.cs at the Solution Explorer and start debugging.
    public class AwardMegaballSvc : IAwardMegaballSvc
    {

        public bool InsertAwardMegaball(AwardMegaballBO awardMegaball)
        {
            return BBH.Lotte.CLP.Data.AwardMegaballBusiness.InsertAwardMegaball(awardMegaball);
        }

        public bool UpdateAwardMegaball(AwardMegaballBO awardMegaball)
        {
            return BBH.Lotte.CLP.Data.AwardMegaballBusiness.UpdateAwardMegaball(awardMegaball);
        }

        public bool LockAndUnlockAwardMegaball(int numberID, int isActive)
        {
            return BBH.Lotte.CLP.Data.AwardMegaballBusiness.LockAndUnlockAwardMegaball(numberID, isActive);
        }

        public IEnumerable<AwardMegaballBO> GetListAwardMegaball(int start, int end)
        {
            return BBH.Lotte.CLP.Data.AwardMegaballBusiness.GetListAwardMegaball(start, end);
        }


        public IEnumerable<AwardMegaballBO> ListAllAwardMegaballBySearch(DateTime fromDate,DateTime toDate, int start, int end)
        {
            return BBH.Lotte.CLP.Data.AwardMegaballBusiness.ListAllAwardMegaballBySearch(fromDate, toDate, start, end);
        }
    }
}
